# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python
import sys
import argparse
from MDsimv2.tools.lifetimes import fit_and_write


 
parser = argparse.ArgumentParser(description='fitLifetimes routine as shipped with MDsimv2')
parser.add_argument('folder', 
                    help = 'Folder holding the simulations output. Must contain energy files.',
                    type = str)
parser.add_argument('--fitfunc',
                    dest = 'fitfunc',
                    help = 'Whether to do an exponential or a logarithmic fit.',
                    type = str,
                    choices = ['exp', 'log'],
                    default = 'log')

parser.add_argument('--show',
                    dest = 'show',
                    action = 'store_true',
                    help = 'Show individual fits graphically.',
                    default = False)

if __name__ == '__main__':
    args = parser.parse_args()
    fit_and_write(MDfolder = args.folder,
                  fitfunc = args.fitfunc,
                  filename = 'LIFETIMES__{}.dat'.format('-'.join([i for i in args.folder.split('/') if (i != 'output' and i != '')])),
                  show = args.show)

