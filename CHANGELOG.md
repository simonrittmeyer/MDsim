# Change Log
All notable changes to this project will be documented in this file from May 2016 on.
This project adheres to [gitlab/MDsim](https://gitlab.com/simonrittmeyer/MDsim).

## [3.5.2] --unreleased
### Added
### Changed
- Observable castepTKv2 is renamed to ehSpec
### Fixed

## [3.5.1] -- 2017-07-22
### Fixed
- io bug repeating system info with every call to ``__str__``
- plenty further bug fixes to allow for a stable usage on SuperMUC

## [3.5.0] -- 2016-11-25
### Added
- Fixed Atoms and fixed coordinates constraints
- Mainroutine for "simple" AIMD+EF without embedding
- support for different CODATA versions
- unit compatibility with ASE
- tests for numerical integrator stability
- diffusive trajectories for castepTKv2
- Routines to extract forced oscillator model (FOM) spectra
- Tensorial friction support (for constant friction tensors that are non-diagonal)

### Changed
- ASE trajectory handler never pushes the system state upon initialization. This has to be done manually in accordance with all other handlers.

### Fixed
- momenta conversion for ASE-related systems
- definition of Epot in QM/Me to yield a proper Hamiltonian
- velocity intialization in QM/Me system



## [3.5.0] -- 2016-11-25
### Added
- Fixed Atoms and fixed coordinates constraints
- Mainroutine for "simple" AIMD+EF without embedding
- support for different CODATA versions
- unit compatibility with ASE
- tests for numerical integrator stability
- diffusive trajectories for castepTKv2
- Routines to extract forced oscillator model (FOM) spectra
- Tensorial friction support (for constant friction tensors that are non-diagonal)

### Changed
- ASE trajectory handler never pushes the system state upon initialization. This has to be done manually in accordance with all other handlers.

### Fixed
- momenta conversion for ASE-related systems
- definition of Epot in QM/Me to yield a proper Hamiltonian
- velocity intialization in QM/Me system


## [3.4.3] -- 2016-08-26
### Added
- Support for non-interacting many particle systems (easier statistics)
- ISF can be evaluated via accumulated DSF (``isf_via_dsf``) or accumulated ISF (``isf``)
- stand-alone tool FitAlphas to fit a bunch of simulations automatically
- normalmode energy decomposition variable
- fit lifetime routines
- stand-alone script ShowVibCool to visualize vibrational damping
- vibdamp main reproduces PRL numbers on vibrational lifetimes exactly
- ConvertFriction stand alone utility
- Version used to produce the numbers in the upcoming surface diffusion publication

### Changed
- Indiviual ISFs are no longer normalized as this ruins statistical averaging afaik.
- Decay rate fitting routine has been revised.
- Naming conventions for surface directions aligned with Kolasinski, Surface Science, 3rd Ed.
- castepTK main routine also write energy files for comparison


### Fixed
- detection of unknown input parameters for diffusion main
- dK-alpha signatures for interacting particles
- several minor fixes

## [3.4.2] -- 2016-05-03
### Added
- support for incoherent scattering simulations
- support for several dK values at once 
