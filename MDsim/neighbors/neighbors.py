# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that allows for high-performance neighbor list evaluation. There is also
a fallback numpy-implemention as well as a plain python reference
implementation (incredibly slow).

The main intention is to provide a uniform interface to all of the provided
high-performance implementations.

There are to date no cell linked-lists algorithm implmented. If the neighbor
list creation is abottleneck, I will come back to this.
"""
from __future__ import print_function
import numpy as np

from MDsim import tools

from MDsim.speedup import plainpython

try:
    from MDsim.speedup import f90support
    _f90 = True
except ImportError:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    _numba = True
except ImportError:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    _cython = True
except ImportError:
    _cython = False

try:
    import scipy.spatial.distance
    _scipy = True
except ImportError:
    _scipy = False


class Neighbors(object):
    """
    Class to evaluate neighbor lists.

    Initialization
    --------------
    unit_cell : (Ndim, Ndim) array, optional (default = None)
        The unit cell. Currently only orthorhombic cells are supported. If you
        do not need periodicity, pass <None>. Note that there is no unit
        conversion, this means that the unit_cell units should be the same as
        the positions units.
    """
    _implemented_support = ['fortran',
                            'cython',
                            'numba',
                            'fallback']

    if _f90:
        _support = 'fortran'
    elif _cython:
        _support = 'cython'
    elif _numba:
        _support = 'numba'
    else:
        _support = 'fallback'


    def __init__(self, unit_cell=None):
        if unit_cell is None:
            self.has_unit_cell = False
        else:
            self._set_unit_cell(unit_cell)


    def _set_unit_cell(self, unit_cell):
        """
        Set a system-wide unit cell.

        Parameters
        ----------
        unit_cell : (Ndim, Ndim) array, optional (default = None)
            The unit cell. Currently only orthorhombic cells are supported. If
            you do not need periodicity, pass <None>. Note that there is no
            unit conversion, this means that the unit_cell units should be the
            same as the positions units.
        """
        self.unit_cell = np.asarray(unit_cell, dtype=float, order='C')
        tools.periodic.check_orthorhombic_cell(self.unit_cell)
        self._unit_cell_diag = np.diag(self.unit_cell)
        self.has_unit_cell = True


    def _change_support(self, support):
        """
        Change the applied speedup support.

        Parameters
        ----------
        support : string
            One of 'fortran', 'cython', 'numba', 'fallback'
        """
        if support not in self._implemented_support:
            raise NotImplementedError('Support "{}" not implemented'.format(support))

        self._support = support

    def construct_neighbor_lists(self, positions, list_cutoff, mic=True):
        """
        Construct neighbor lists bases on a given array of positions. If
        available, a high-performance F90 implementation will be used.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        mic : boolean, optional (default=True)
            Apply the minimum image convention which means that distances are
            evaluated respecting periodic boundary conditions.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        if mic and not self.has_unit_cell:
            raise ValueError('Set unit cell first when using periodic boudary conditions.')

        if self._support == 'fortran':
            return self._construct_neighbor_lists_fortran(positions, list_cutoff, mic)
        elif self._support == 'cython':
            return self._construct_neighbor_lists_cython(positions, list_cutoff, mic)
        elif self._support == 'numba':
            return self._construct_neighbor_lists_cython(positions, list_cutoff, mic)
        elif self._support == 'fallback':
            return self._construct_neighbor_lists_numpy(positions, list_cutoff, mic)


    def _construct_neighbor_lists_fortran(self, positions, list_cutoff, mic=True):
        """
        Construct neighbor lists bases on a given array of positions. A
        high-performance F90 implementation will be used.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        mic : boolean, optional (default=True)
            Apply the minimum image convention which means that distances are
            evaluated respecting periodic boundary conditions.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        Natoms = positions.shape[0]

        # copies are cheap as compared to the evaluation...
        neighbor_lists_F90 = np.empty((Natoms, Natoms), dtype=np.int32, order='F')
        num_neighbors_F90 = np.empty(Natoms, dtype=np.int32)

        if mic:
            f90support.neighbors.construct_neighbor_lists_mic(positions=positions.T,
                                                              unit_cell_diag=self._unit_cell_diag,
                                                              list_cutoff=list_cutoff,
                                                              neighbor_lists=neighbor_lists_F90,
                                                              num_neighbors=num_neighbors_F90)
        else:
            f90support.neighbors.construct_neighbor_lists(positions=positions.T,
                                                          list_cutoff=list_cutoff,
                                                          neighbor_lists=neighbor_lists_F90,
                                                          num_neighbors=num_neighbors_F90)

        # python indexing is returned, do not forget to transpose (contigiousness)
        return (np.asarray(neighbor_lists_F90.T, dtype=np.int32, order='C'),
                np.asarray(num_neighbors_F90, dtype=np.int32, order='C'))


    def _construct_neighbor_lists_python(self, positions, list_cutoff, mic=True):
        """
        Construct neighbor lists bases on a given array of positions. A
        very slow plain python reference implementation is used.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        mic : boolean, optional (default=True)
            Apply the minimum image convention which means that distances are
            evaluated respecting periodic boundary conditions.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        if mic:
            return plainpython.neighbors.construct_neighbor_lists_mic(positions=positions,
                                                                      unit_cell_diag=self._unit_cell_diag,
                                                                      list_cutoff=list_cutoff)
        else:
            return plainpython.neighbors.construct_neighbor_lists(positions=positions,
                                                                  list_cutoff=list_cutoff)

    def _construct_neighbor_lists_numba(self, positions, list_cutoff, mic=True):
        """
        Construct neighbor lists bases on a given array of positions. Numba's
        JIT compilation is made use of.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        mic : boolean, optional (default=True)
            Apply the minimum image convention which means that distances are
            evaluated respecting periodic boundary conditions.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        if mic:
            return numbasupport.neighbors.construct_neighbor_lists_mic(positions=positions,
                                                                       unit_cell_diag=self._unit_cell_diag,
                                                                       list_cutoff=list_cutoff)
        else:
            return numbasupport.neighbors.construct_neighbor_lists(positions=positions,
                                                                   list_cutoff=list_cutoff)

    def _construct_neighbor_lists_cython(self, positions, list_cutoff, mic=True):
        """
        Construct neighbor lists bases on a given array of positions. A
        high-performance cython-based C implementation will be used.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        mic = boolean, optional (default=True)
            Apply the minimum image convention which means that distances are
            evaluated respecting periodic boundary conditions.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        if mic:
            return cythonsupport.neighbors.construct_neighbor_lists_mic(positions=positions,
                                                                        unit_cell_diag=self._unit_cell_diag,
                                                                        list_cutoff=list_cutoff)
        else:
            return cythonsupport.neighbors.construct_neighbor_lists(positions=positions,
                                                                    list_cutoff=list_cutoff)

    def _construct_neighbor_lists_numpy(self, positions, list_cutoff, mic=True):
        """
        Construct neighbor lists bases on a given array of positions. Numpy's
        broadcasting abilities are used where possible. Note that this may
        require a substantial amount of memory. If no periodicity is invoked,
        then me can make use of scipy's pdist module

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        mic = boolean, optional (default=True)
            Apply the minimum image convention which means that distances are
            evaluated respecting periodic boundary conditions.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        # do it once
        list_cutoff = list_cutoff**2

        # this allows for convenient mic application
        if mic:
            # scipy does not allow for periodicity, so go for numpy here
            _dist = positions[:, None]-positions[None, :]
            _dist -= (self._unit_cell_diag[None, None, :] * np.round(_dist / self._unit_cell_diag[None, None, :]))
            distances = (_dist**2).sum(-1)
        else:
            if _scipy:
                Natoms = positions.shape[0]
                # these will be disregarded anyways
                distances = np.ones((Natoms, Natoms), dtype=np.float64, order='C') * (list_cutoff+1)
                # k=1 excludes the diagonal
                distances[np.triu_indices(Natoms, k=1)] = scipy.spatial.distance.pdist(positions, 'sqeuclidean')[:]
            else:
                _dist = positions[:, None]-positions[None, :]
                distances = (_dist**2).sum(-1)

        # in case of scipy it's a pseudo mask
        neighbor_masks = np.array((distances < list_cutoff), dtype=bool)

        # zero distance is a neighbor... but not in our understanding
        np.fill_diagonal(neighbor_masks, val=False)
        return self.convert_python_masks_to_neighbor_list(neighbor_masks)

    @staticmethod
    def convert_python_masks_to_neighbor_list(neighbor_masks):
        """
        Convert a neighbor mask (elements are either <True> or <False> for a
        tuple of atom indices i,j) to an actual neighbor list containing
        indices of neighbor atoms (and -1 for invalid entries).

        Unfortunately, the conversion requires a pythonic loop over all atoms.
        This may be replaced with a cython/F90 implementation if required.

        Parameters
        ----------
        neighbor_masks : (Natoms, Natoms) array
            The neighbor masks. If atoms i and j are neighbors, then
            neighbor_masks[i,j] = <True>, else <False>.

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.
        """
        Natoms = neighbor_masks.shape[0]
        neighbor_lists = np.ones((Natoms, Natoms), dtype=np.int32, order='C')*-1
        num_neighbors = np.zeros(Natoms, dtype=np.int32, order='C')

        # we do not care about the lower triangle
        neighbor_masks[np.tril_indices_from(neighbor_masks)] = False

        # is there any way to avoid this loop?
        for i in range(Natoms):
            nz = np.count_nonzero(neighbor_masks[i])
            neighbor_lists[i, :nz] = np.nonzero(neighbor_masks[i])[0]
            num_neighbors[i] = nz
        return neighbor_lists, num_neighbors

    @staticmethod
    def convert_neighbor_lists_to_python_masks(neighbor_lists, num_neighbors):
        """
        Convert a neighbor list containing neighbor indices to a neighbor mask
        (elements are either <True> or <False> for a tuple of atom indices i,j)
        to an actual neighbor list containing indices of neighbor atoms (and -1
        for invalid entries).

        Parameters
        ----------
        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists. Note that although these are (Natom x Natom)
            arrays, only some entries are actually meaningful. This is
            controlled by "num_neighbors", ie. the next output parameter.
            Invalid entries are marked with "-1". Note that indices apply to
            pythonic notation, ie. starting with "0".

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        neighbor_masks : (Natoms, Natoms) array
            The neighbor masks. If atoms i and j are neighbors, then
            neighbor_masks[i,j] = <True>, else <False>.
        """
        Natoms = neighbor_lists.shape[0]
        neighbor_masks = np.zeros(neighbor_lists.shape, dtype=bool)

        for atom_idx in range(Natoms):
            for j in range(num_neighbors[atom_idx]):
                neighbor_idx = neighbor_lists[atom_idx, j]
                neighbor_masks[atom_idx, neighbor_idx] = True
                neighbor_masks[neighbor_idx, atom_idx] = True
        return neighbor_masks



    def _create_neighbor_list_plot(self, positions, list_cutoff, iatom=None):
        """
        Visualization of the neighbor list creation. Currently only available
        for 2D systems.

        Note that PBC are required, ie. you have to set a unit cell in advance.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        iatom : integer, optional (default = None)
            Atom for which the neighbor list is visualized. If none is given, a
            random choice will be made.

        Returns
        -------
        details: dictionary
            A dictionary with the following keys:
            "fig" : matplotlib figure object
                The figure object.

            "ax" : matplotlib axes object
                The actual axes.

            "iatom" : int
                The atom index for which the neighbor list is drawn.
        """

        if not self.has_unit_cell:
            raise ValueError('Set a unit_cell first.')

        Natoms, Ndim = positions.shape
        if not Ndim == 2:
            raise NotImplementedError('Sorry, visualization is implemented for 2D only')

        if iatom is None:
            iatom = np.random.randint(Natoms)

        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        # corners of the unit cell
        corners = np.array([np.zeros(2),
                            self.unit_cell[0],
                            self.unit_cell[1],
                            self.unit_cell[0] + self.unit_cell[1]])

        # draw the unit cell
        for connected in ([0, 1], [0, 2], [2, 3], [1, 3]):
            ax.plot(*corners[connected].T, color='red')

        pos = tools.periodic.wrap_positions_orthorhombic(positions, self.unit_cell)
        # plot content of the unit cell
        for p in pos:
            ax.plot(*p,
                    marker='o',
                    #markeredgecolor=None,
                    color='black')

        # plot the nearest images
        positions_extpbc = tools.periodic.extend_positions_pbc_orthorhombic(pos, self.unit_cell)

        for p in positions_extpbc[Natoms:, :]:
            ax.plot(*p,
                    marker='o',
                    #markeredgecolor='none',
                    color='lightgrey')

        # the selected particle in red
        ax.plot(*pos[iatom],
                marker='o',
                #markeredgecolor='none',
                color='red')

        # the list cutoff
        lcirc = plt.Circle(pos[iatom],
                           radius=list_cutoff,
                           fill=True,
                           color='green',
                           alpha=0.2)

        # the neighbor list for the pbc_extended system
        _neighbor_lists, _num_neighbors = self.construct_neighbor_lists(positions_extpbc,
                                                                        list_cutoff,
                                                                        mic=False)


        neighbor_lists_pbc = self.convert_neighbor_lists_to_python_masks(_neighbor_lists, _num_neighbors)

        # the neighbors of particle #i across pbc borders
        for p in positions_extpbc[neighbor_lists_pbc[iatom]]:
            ax.plot(*p,
                    marker='o',
                    #markeredgecolor='none',
                    color='blue')

        # the neighbors in the unit cell
        neighbor_lists = self.convert_neighbor_lists_to_python_masks(*self.construct_neighbor_lists(positions,
                                                                                                    list_cutoff,
                                                                                                    mic=True))
        for p in pos[neighbor_lists[iatom]]:
            ax.plot(*p,
                    marker='o',
                    #markeredgecolor='none',
                    color='orange')

        # add the cutoff circles
        ax.add_artist(lcirc)

        # show it
        ax.set_aspect('equal')

        ax.set_xlabel(r'$x$ / a.u.')
        ax.set_ylabel(r'$y$ / a.u.')

        details = {'fig': fig,
                   'ax' : ax,
                   'iatom' : iatom}
        return details


    def show_neighbor_list(self, *args, **kwargs):
        """
        Visualization of the neighbor list creation. Currently only available
        for 2D systems.

        Note that PBC are required, ie. you have to set a unit cell in advance.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions to which the neighbor lists should refer.

        list_cutoff : float
            The maximum distance that separates neighbors from non-neighbors.

        iatom : integer, optional (default = None)
            Atom for which the neighbor list is visualized. If none is given, a
            random choice will be made.
        """
        import matplotlib.pyplot as plt
        self._create_neighbor_list_plot(*args, **kwargs)
        plt.show()
