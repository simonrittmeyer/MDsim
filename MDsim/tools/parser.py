# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import ConfigParser
import argparse

def parse_inputfile(inputfile, options_dict, verbose=True):
    """
    Function that parses an inputfile using the ConfigParser module. This
    means, the infile is to be written in INI format. This is easily
    understandable and rather straightforward.

    This parser will only accept a key-value pair if it is present in the
    options_dict.

    Parameters
    ----------
    inputfile : str
        The inputfile to be parsed

    options_dict : dictionary
        The options dictionary to be updated.
    """

    config = ConfigParser.SafeConfigParser()

    if verbose:
        print('Parsing inputfile "{}"'.format(inputfile))
    config.read(inputfile)

    for category in config.sections():
        category = category.lower()

        if not category in options_dict.keys():
            msg = 'Unknown category "{}"'.format(category)
            raise ValueError(msg)
        else:
            for option in config.options(category):
                option = option.lower()
                if not option in options_dict[category].keys():
                    msg = 'Unknown option "{}" in category "{}"'.format(option, category)
                else:
                    options_dict[category][option].val = config.get(category, option)
                    if verbose:
                        print('\tRecognized option "{}.{}"'.format(category, option))

    return options_dict


def create_argparser(options_dict, description=None):
    """
    Helper function that creates a command line parser based on the options
    defined in the `options` dictionary.

    There is no type conversion nor a choices check from the command line
    parser. All of this is done in den custom tools/parser.py module.

    Parameters
    ----------
    options_dict : dict
        The available options

    description : str, optional (default=None)
        Parser description.

    Returns
    -------
    parser : argparser instance
        The readily defined parser.
    """
    if description is None:
        description = 'MDsim : MD simulations including electronic friction.'

    parser = argparse.ArgumentParser(description=description)

    for category in options_dict.keys():
        for name, option in options_dict[category].items():
            # inactive options are not supported on command line
            if option._active:
                parser.add_argument('--{}.{}'.format(category,name),
                                    type = str,
                                    default = argparse.SUPPRESS,
                                    help = option.info)

    # infile is not a real internal option
    parser.add_argument('-i', '--infile', type = str,
                        default = argparse.SUPPRESS,
                        help = 'Infile specifying further options (same keywords as from commandline)')

    parser.add_argument('-p', '--progressbar',
                        action = 'store_true',
                        help = 'Show a progressbar during the simulation')
    return parser



def parse_cmdline(*args, **kwargs):
    """
    Function that parses command line arguments.

    Parameters
    ----------
    options_dict : dict
        The available options

    description : str, optional (default=None)
        Parser description.

    Returns
    -------
    output : dict
        Dictionary that can be expanded in the main routine(s). Contains the
        following keys:

        infile : str
            The infile (if specified)

        cmd_args : dict
            The command line arguments.
    """

    parser = create_argparser(*args, **kwargs)
    args = vars(parser.parse_args())

    infile = args.pop('infile', None)
    progressbar = args.pop('progressbar', False)

    output = {'infile' : infile,
              'cmd_args' : args,
              'progressbar' : progressbar}

    return output
