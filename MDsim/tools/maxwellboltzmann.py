# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy.constants import k

from MDsim import units

def draw_velocities(masses, Ndim, temperature, convert=True, seed=None):
    """
    Routine to draw velocities from a Maxwell-Boltzmann distribution.

    Parameters
    ----------
    masses : (Natoms x 1) array
        Masses of the respective atoms.

    Ndim : integer
        Cartesian dimensionality of the system.

    temperature : float
        Temperature.

    convert : boolean, optional (default = True)
        Whether to convert Kelvin and AMU to a.u. or not. If <False> then input
        in a.u.

    seed : integer, optional (default = None)
        The seed for the random number generator

    Returns
    -------
    velocities : (Natoms x Ndim) array
    """

    kT = temperature

    if convert:
        # converts directly to thermal energy
        kT *= units.KELVIN_TO_AU
        masses *= units.AMU_TO_AU

    # the variance of our gaussians
    sigmas = np.sqrt(kT / masses)

    Natoms = len(masses)

    np.random.seed(seed)

    velocities = np.random.standard_normal(size = (Natoms, Ndim))
    # scaling the normal distribution
    velocities *= sigmas[:,np.newaxis]

    if convert:
        velocities *= units.AU_TO_ANGSTROM_PER_FS

    return velocities

class MaxwellBoltzmann(object):
    """
    Class that symbolizes a continuous Maxwell-Boltzmann distribution.
    Mainly used for plotting purposes
    """
    def __init__(self, mass, T, Ndim, convert = True):
        self.mass = mass
        self.kT = T
        self.Ndim = Ndim

        if convert:
            self.mass *= units.AMU_TO_AU
            self.kT *= units.KELVIN_TO_AU

        self._expfactor = self.mass/(2.*self.kT)

        if self.Ndim == 1:
            self._prefactor = 2* np.sqrt(self._expfactor / np.pi)
        elif self.Ndim == 2:
            self._prefactor = 2.*np.pi*(self._expfactor / np.pi)
        elif self.Ndim == 3:
            self._prefactor = 4.*np.pi*(self._expfactor / np.pi)**1.5
        else:
            print('Error : unsupported dimensionality Ndim = "{}"'.format(self.Ndim))
            raise ValueError

    def _1d(self, v):
        return self._prefactor * np.exp(-self._expfactor*v**2)

    def _2d(self, v):
        return self._prefactor * v * np.exp(-self._expfactor*v**2)

    def _3d(self, v):
        return self._prefactor * v**2 * np.exp(-self._expfactor*v**2)

    def __call__(self, v):
        if self.Ndim == 1:
            return self._1d(v)
        elif self.Ndim == 2:
            return self._2d(v)
        elif self.Ndim == 3:
            return self._3d(v)



