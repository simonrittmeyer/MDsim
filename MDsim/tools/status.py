# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

class colors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Status(object):
    OK = "[OK]"
    FAIL = "[FAIL]"
    try:
        import platform
        if not platform.system() == 'Windows':
            OK = colors.OKGREEN + OK + colors.ENDC
            FAIL = colors.FAIL + FAIL + colors.ENDC
    except StandardError:
        pass

    def __init__(self):
        self.ok = True

    def __str__(self):
        if self.ok:
            return self.OK
        else:
            return self.FAIL

    def __repr__(self):
        if self.ok:
            return self.OK
        else:
            return self.FAIL

