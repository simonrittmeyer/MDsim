# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.tools.generic import is_square_matrix
from MDsim.tools.generic import is_diagonal_matrix
from MDsim.tools.generic import is_orthogonal_matrix

# efficient distance evluations
from MDsim.speedup import plainpython

try:
    from MDsim.speedup import f90support
    _f90 = True
except (ImportError, ValueError) as e:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    _numba = True
except (ImportError, ValueError) as e:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    _cython = True
except (ImportError, ValueError) as e:
    _cython = False


def check_orthorhombic_cell(cell):
    """
    Check whether a cell is actually orthorhombic

    Parameters
    ----------
    cell : (Ndim, Ndim) array
        The cell to be checked.

    Returns
    -------
    <True> is cell is orthorhombic, raises NotImplementedError elsewise.
    """
    # first dimension are cell vectors, second dimension cartesian coordinates
    if not is_square_matrix(cell):
        raise ValueError('Unit cell is not a square matrix')

    # check if our cell is diagonal...
    if not is_diagonal_matrix(cell):
        if not is_orthogonal_matrix(cell):
            raise NotImplementedError('Unit cell is not orthorhombic.')
        else:
            raise NotImplementedError('Unit cell is orthorhombic but not diagonal. '+
                                      'Consider transforming via rotations.')
    return True

def wrap_positions_orthorhombic(positions, cell):
    """
    Fold positions back into the unit cell
    """
    # get fractional positions, and wrap back into cell
    cell_diag = np.diagonal(cell)[None,:]
    return ((positions / cell_diag)%1) * cell_diag


def extend_positions_pbc_orthorhombic(positions, cell):
    """
    Extend a positions array by backfolding into the unit cell and adding
    periodic images of the the next neighbor cells
    """

    # check for the cell
    check_orthorhombic_cell(cell)

    # wrap positions back
    positions = wrap_positions_orthorhombic(positions, cell)

    # translations to nearest periodic images
    pbc_multiples = [0, 1, -1]

    # create the translations to obtain the next periodically repeated cells
    Natoms, Ndim = positions.shape
    Ncells_pbc = 3**Ndim

    # positions in unit cell and nearest neighbour cells
    positions_extpbc = np.empty((Ncells_pbc*Natoms, Ndim))

    # create the translation vectors to the nearest neighbours
    translations = []

    if Ndim == 1:
        for i in pbc_multiples:
            translations.append(cell[0]*i)

    elif Ndim == 2:
        for i in pbc_multiples:
            for j in pbc_multiples:
                translations.append(cell[0]*i + cell[1]*j)

    elif Ndim == 3:
        for i in pbc_multiples:
            for j in pbc_multiples:
                for k in pbc_multiples:
                    translations.append(cell[0]*i + cell[1]*j + cell[2]*k)

    translations = np.asarray(translations, order = 'C', dtype = float)

    for i in range(Ncells_pbc):
        positions_extpbc[i*Natoms:(i+1)*Natoms,:] = positions + translations[i]

    return positions_extpbc


def wrap_positions_generic_cell(positions, cell):
    """
    Fold positions back into the unit cell, works for every cell.
    If it was always the same cell you could also use the FractionalCoordinates
    object for performance gain.
    """
    # get fractional positions
    positions_frac = np.linalg.solve(cell.T, positions.T).T

    # wrap back to unit cell
    positions_frac = positions_frac%1
    return np.dot(positions_frac, cell)


class Distances(object):
    """
    Class that provides a high-level wrapper to Fortran, C, and numba
    extensions that evaluate pairwise distances in a system, also including
    periodic boundary conditions in terms of the minimum image convention
    (MIC).
    """

    _implemented_support = ['fortran',
                            'cython',
                            'numba',
                            'fallback']

    if _f90:
        _support = 'fortran'
    elif _cython:
        _support = 'cython'
    elif _numba:
        _support = 'numba'
    else:
        _support = 'fallback'

    def _change_support(self, support):
        """
        Change the applied speedup support.

        Parameters
        ----------
        support : string
            One of 'fortran', 'cython', 'numba', 'fallback'
        """
        if support not in self._implemented_support:
            raise NotImplementedError('Support "{}" not implemented'.format(support))

        self._support = support


    def calc_distances_mic(self, positions, unit_cell=None):
        """
        High level routine to evaluate distances using the minimum image
        convention (MIC). Note that unit cell and positions must be in same
        unit system, which then also defines the units of the output.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        unit_cell : (Ndim, Ndim) array
            The unit cell. This far, there is only support for orthorhombic
            cells.

        Returns
        -------
        distances : (Natoms, Natoms) array
            The respective distances between atom i and j.
        """
        unit_cell = np.asarray(unit_cell, dtype=np.float64, order='C')
        check_orthorhombic_cell(unit_cell)
        unit_cell_diag = np.diag(unit_cell)

        if not unit_cell.shape[0] == positions.shape[1]:
            raise ValueError('Dimensionality mismatch!')

        if self._support == 'fortran':
            return self._calc_distances_mic_fortran(positions, unit_cell_diag)
        elif self._support == 'cython':
            return self._calc_distances_mic_cython(positions, unit_cell_diag)
        elif self._support == 'numba':
            return self._calc_distances_mic_numba(positions, unit_cell_diag)
        elif self._support == 'fallback':
            return self._calc_distances_mic_python(positions, unit_cell_diag)
        else:
            raise ValueError('Unknown support "{}"'.format(self._support))


    def _calc_distances_mic_python(self, positions, unit_cell_diag):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses a plain
        python implementation.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        unit_cell_diag : (Ndim) array
            Lengths of the orthorhombic unit cell.

        Returns
        -------
        distances : (Natoms, Natoms) array
            The respective distances between atom i and j.
        """

        return plainpython.distances.calc_distances_mic(positions=positions,
                                                        unit_cell_diag=unit_cell_diag)


    def _calc_distances_mic_numba(self, positions, unit_cell_diag):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses numba's JIT
        compilation features.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        unit_cell_diag : (Ndim) array
            Lengths of the orthorhombic unit cell.

        Returns
        -------
        distances : (Natoms, Natoms) array
            The respective distances between atom i and j.
        """

        return numbasupport.distances.calc_distances_mic(positions=positions,
                                                         unit_cell_diag=unit_cell_diag)

    def _calc_distances_mic_cython(self, positions, unit_cell_diag):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses cython
        extensions.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        unit_cell_diag : (Ndim) array
            Lengths of the orthorhombic unit cell.

        Returns
        -------
        distances : (Natoms, Natoms) array
            The respective distances between atom i and j.
        """

        return cythonsupport.distances.calc_distances_mic(positions=positions,
                                                          unit_cell_diag=unit_cell_diag)


    def _calc_distances_mic_fortran(self, positions, unit_cell_diag):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses cython
        extensions.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        unit_cell_diag : (Ndim) array
            Lengths of the orthorhombic unit cell.

        Returns
        -------
        distances : (Natoms, Natoms) array
            The respective distances between atom i and j.
        """
        Natoms = positions.shape[0]
        _distances_F90 = np.zeros((Natoms, Natoms), dtype=np.float64, order='F')
        f90support.distances.calc_distances_mic(positions=positions.T,
                                                unit_cell_diag=unit_cell_diag,
                                                distances=_distances_F90)
        return np.asarray(_distances_F90, dtype=np.float64, order='C')
