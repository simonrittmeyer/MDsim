# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import re
import numpy as np
import os
import sys

import netCDF4

from time import strftime
from MDsim import units

from MDsim.tools.netcdf import load_netcdf
from MDsim.analysis.isf import calc_isf
from MDsim.analysis.fitting import tune_cutoff

import matplotlib.pyplot as plt

def fit_alphas(base_dir,
               file_pattern,
               tend=None, # in ps!
               stepsize=1,
               max_cutoff=None, # in ps!
               fitfunc='exponential_plus_constant',
               result_folder=None,
               log_fits=True,
               show_results=True,
               show_fit=False,
               ):
    """
    Automatic fitting of decay rates for a bunch of simulations.
    Will yield one file per simulation.

    Proper documentation to follow.
    """

    if result_folder is None:
        result_folder = 'decay_rates_from_{}'.format(base_dir)

    if log_fits:
        log_folder = os.path.join(result_folder, 'logfiles')

    # create those folders
    try:
        os.makedirs(result_folder)
    except OSError:
        pass

    if log_fits:
        try:
            os.makedirs(log_folder)
        except OSError:
            pass

    # loop over the base_dir
    for dirpath, dirnames, filenames in os.walk(base_dir):
        for f in filenames:
            match = re.search(file_pattern, f)
            if not match:
                continue

            print(f)
            fname = os.path.join(dirpath, f)
            data = load_netcdf(fname).T

            # read the delta K info
            ncfile = netCDF4.Dataset(fname, 'r')
            kinfo = ncfile.observable
            ncfile.close()

            kpts = []
            for line in kinfo.split('\n'):
                if 'Delta-K_' in line:
                    kpts.append(float(line.split()[-2]))

            # go to pico seconds
            time = data[0] / 1000.
            isf = data[1:]
            Nkpts, Ntimes = isf.shape

            assert len(kpts) == Nkpts

            alphas = np.zeros(Nkpts)*np.nan

            print('-'*80)
            print('Tuning cutoff ({})'.format(fname))
            print('-'*80)


            for k in range(0, Nkpts):
                if log_fits:
                    logfile = os.path.join(log_folder,
                            'fit__{:s}__Kpt_{:03d}.log'.format(f.replace('.nc', ''), k))
                else:
                    logfile=None

                print('-'*80)
                print('{} / {}'.format(k+1, Nkpts))
                print('-'*80)

                alphas[k] = tune_cutoff(times=time,
                                        signal=isf[k],
                                        verbose=True,
                                        fitfunc=fitfunc,
                                        show=show_fit,
                                        stepsize=stepsize,
                                        max_cutoff=max_cutoff,
                                        end=tend,
                                        logfile=logfile)


            with open('{}/alphas_{}.dat'.format(result_folder, f.replace('.nc','')),'w') as f:
                f.write('# File created: {}\n# Decay rates fitted to: {}\n#'.format(strftime('%c'), fname))
                f.write('\n# {:<18s}{:<20s}'.format('|dK| (A^-1)', 'alpha (ps^-1)'))
                for k, a in zip(kpts, alphas):
                    f.write('\n{:<20.9f}{:<20.9f}'.format(k,a))
            if show_results:
                plt.plot(kpts, alphas, ls='', marker='o')
                plt.xlabel('dK  (A^-1)')
                plt.ylabel('alpha (ps^-1)')
                plt.show()

