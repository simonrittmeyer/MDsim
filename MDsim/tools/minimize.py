# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import sys
import StringIO

from ase.optimize import BFGS

from MDsim.tools import check_kwargs
from MDsim.tools.createobjects import create_customCalculator

def minimize_atoms(atomsEq, potential_obj, **kwargs):
    """
    Optimize structure on interpolated potential
    """
    param = {'fmax'     : 1e-6,
             'forces'     : 'numerical',
             'forces_stepwidth' : 0.01}
     
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    print 'Optimizing atoms geometry on interpolated potential'
    
    # shut up, guys, i.e. suppress minimizer IO to stdout
    stdout = sys.stdout
    sys.stdout = StringIO.StringIO()
    
    atoms = atomsEq.get_full_atoms_obj()
    old_pos = atomsEq.get_positions_eq()

    calc = create_customCalculator(atomsEq = atomsEq,
                                   potential_obj = potential_obj, 
                                   forces = param['forces'],
                                   forces_stepwidth = param['forces_stepwidth'])
    
    atoms.set_calculator(calc)
    BFGS(atoms).run(fmax = param['fmax'])
    
    # ok, now you may talk again
    sys.stdout = stdout
    
    atomsEq._set_atoms(atoms)
    new_pos = atomsEq.get_positions_eq()
        
    offset = new_pos - old_pos
    
    print '\tobtained an offset of:'
    for i, o in enumerate(offset.flatten()):
        print '\t\tpos_{0:d} : {1:12.9f}'.format(i, o)

    atomsEq.set_offset(offset)

    return atomsEq

