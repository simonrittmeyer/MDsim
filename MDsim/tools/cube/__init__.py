# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

from MDsim import units

def read_cube(fileobj, convert=False, program=None, verbose=False):
    """
    This is a tweaked version of the cubefile reader as found in the
    ase.io.cube module, since the latter cannot deal with the different ways to
    handle periodic boundaries conditions, different programs apply. See `Notes`
    below for details. Yet, we try to avoid unnecessary calls to ase routines
    and variables in this context.

    Parameters
    ----------
    fileobj : string
        Location to the cubefile.

    convert : boolean, optional (default = False)
        If set true, the electronic density is returned in units of
        e/Angstrom**3 and the voxel dimensions are returned in Angstrom.
        If set False, there will be no conversion whatsoever of the
        input data, i.e. e/voxel.

    program : string, optional ({'castep', 'FHI-aims'})
        Program that wrote the actual cube file. this helps to avoid for instance
        issues with the way PBC are handled within the cube file, or how to treat
        the `origin` of the cube_datae in FHI-aims. If None is given, the routine
        tries to catch the program type from the comment line in the cube file.

        Choosing `castep` follows the PBC conventions that first and last voxel
        along a direction are mirror images, thus the last voxel is to be removed.
        However, the routine tries to catch castep files from the second comment
        line.

    verbose : boolean, optional (default = True)
        Print some more information to stdout.

    Returns
    -------
    output : dictionary
        Dictionary holding the entire output. The respective keys are
            * 'cell'      : The unit cell, or the coundaries of the cube file.
            * 'cube_data' : (Nx x Ny x Nz) array containg the electronic density
                            values (for units see convert argument) from the cube
                            file, where Nx, Ny, and Nz are the number of voxels in
                            the respective direction.
            * 'voxdim'    : (3 x 3 x 3) array containg the length of each voxel
                            along each direction of the coordinate system in
                            Angstrom.
            * 'origin'    : (3x1) array, specifying the cube_data origin. May be
                            different to zero for some programs (e.g. FHI-aims).
    Notes
    -----

    Periodic boundary conditions (PBC) and cube files are somewhat difficult to
    handle. They were not made to handle PBC, however, if you want to visualize
    periodic volumetric data, you need to incorporate it. See e.g. this link:

        http://www.xcrysden.org/doc/XSF.html#__toc__11

    To my experience, most codes apply a *general grid* to write cube files.
    However, CASTEP for instance uses a *periodic grid* in this lingo. This
    means, that every last entry along an axis is redundant.

    ---
    Simon P. Rittmeyer, 2015
    simon.rittmeyer(at)tum.de
    """

    programs = ['FHI-aims', 'castep', 'ase', None]
    if not program in programs:
        program = None

    # assign second variable as we might need to change it...
    convert_to_angstrom = units.AU_TO_ANGSTROM
    convert_voxel_dimensions = units.AU_TO_ANGSTROM

    _close = False

    if isinstance(fileobj, str):
        fname = fileobj.lower()
        if fname.endswith('.gz'):
            import gzip
            _close = True
            fileobj = gzip.open(fileobj)
        elif fname.endswith('.bz2'):
            import bz2
            fileobj = bz2.BZ2File(fileobj)
        else:
            _close = True
            fileobj = open(fileobj)

    readline = fileobj.readline

    # the first comment line
    line = readline()

    # AIMS cubes are identified via the first line
    if 'CUBE FILE written by FHI-AIMS' in line:
        program = 'FHI-aims'
    elif 'cube file from ase' in line:
        program = 'ase'

    # the second comment line
    line = readline()

    # The second comment line *CAN* contain information on the axes
    # But this is by far not the case for all programs
    axes = []
    if 'OUTER LOOP' in line.upper():
        axes = ['XYZ'.index(s[0]) for s in line.upper().split()[2::3]]
    if not axes:
        axes = [0, 1, 2]

    # castep2cube files have a specific comment in the second line...
    if 'castep' in line:
        program = 'castep'

    if not program is None:
        if verbose:
            print('read_cube identified program: {}'.format(program))

    # Third line contains actual system information
    line = readline().split()
    natoms = int(line[0])

    # origin around which the volumetric data is centered (at least in FHI aims)
    origin = np.array([float(x)*units.AU_TO_ANGSTROM for x in line[1::]])

    # additionally we want the voxel dimensions stored and returned in Angstrom
    voxdim = np.empty((3, 3))
    cell = np.empty((3, 3))
    shape = []

    # the upcoming three lines contain the cell information
    for i in range(3):
        n, x, y, z = [float(s) for s in readline().split()]

        # if n is negative, then units are Angstrom, else they are Bohr!
        # see: http://paulbourke.net/dataformats/cube/
        if n < 0:
            convert_to_angstrom = 1.

        shape.append(int(n))

        # different PBC treatment in castep, basically the last voxel row is
        # identical to the first one
        if program == 'castep':
            cell[i] = (n-1) * convert_to_angstrom * np.array([x, y, z])
        else:
            cell[i] = (n) * convert_to_angstrom * np.array([x, y, z])

        # voxel dimensions in Angstrom!
        voxdim[i] = np.array([x,y,z]) * convert_to_angstrom

    numbers = np.empty(natoms, int)
    positions = np.empty((natoms, 3))
    for i in range(natoms):
        line = readline().split()
        numbers[i] = int(line[0])
        positions[i] = [float(s) for s in line[2:]]

    # As far as I understood http://paulbourke.net/dataformats/cube/
    # positions are always in Bohr
    positions *= units.AU_TO_ANGSTROM

    # voxel volume is the triple product of the lattice vectors (in Angstrom**3)
    voxvol=abs(np.dot(voxdim[0], np.cross(voxdim[1],voxdim[2])))

    # prepare the output
    # reshape the data
    data = np.array([float(s) for s in fileobj.read().split()]).reshape(shape)
    if axes != [0, 1, 2]:
        data = data.transpose(axes).copy()

    # close the file if necessary
    if _close:
        fileobj.close()

    # convert values from e/voxel to e/A**3
    if convert:
        if not program == 'FHI-aims':
            # FHI aims is already in Angstrom**-3
            data *= 1./voxvol

    if program == 'castep':
        # Due to the PBC applied in castep2cube, the last entry along each
        # dimension equals the very first one.
        # In addtion to the cube data and the atoms object, return the voxel
        # dimensions (elsewhise the data object is more or less useless).
        data = data[:-1,:-1,:-1]

    output = {'cell'      : cell,
              'cube_data' : data,
              'voxdim'    : voxdim,
              'origin'    : origin}

    return output

