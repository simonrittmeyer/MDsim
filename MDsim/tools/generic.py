# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import re
import timeit

def is_square_matrix(matrix):
    """
    Check if a matrix is a square matrix

    Parameters
    ----------
    matrix : (N, M) array
        The matrix to be checked.

    Returns
    -------
    <True> matrix is of square form, <False> if not.
    """
    matrix = np.asarray(matrix)
    return np.all([i == matrix.shape[0] for i in matrix.shape])


def is_diagonal_matrix(matrix, eps = 1e-15):
    """
    Check if a matrix is diagonal, ie. no off-diagonal elements.

    Parameters
    ----------
    matrix : (N, M) array
        The matrix to be checked.

    eps : float, optional (default=1e-15)
        The absolute tolerance to accept "numerical zeros".

    Returns
    -------
    <True> if there are solely zero valued off-diagonals, <False> if not.
    """
    m = matrix.copy()
    # fill diagonal with zeros
    np.fill_diagonal(m, 0.)
    return np.allclose(m, 0., atol=eps)


def is_orthogonal_matrix(matrix, eps = 1e-15):
    """
    Check if a matrix is orthogonal, i.e. A*A^T = E

    Parameters
    ----------
    matrix : (N, M) array
        The matrix to be checked.

    eps : float, optional (default=1e-15)
        The absolute tolerance to accept "numerical zeros".

    Returns
    -------
    <True> if matrix is orthogonal, <False> if not.
    """
    m = np.dot(matrix, matrix)
    return is_diagonal_matrix(m, eps)


def extend_to_Ndim(vector, Ndim):
    """
    Extend a given (Natoms,) vector to a (Natomx, Ndim) matrix by repeating
    the vector Ndim times.

    Parameters
    ----------
    vector : (Natoms,) array
        The vector to be extended in dimensionality.

    Ndim : int
        The dimensionality that is to be added.

    Returns
    -------
    matrix (Natoms, Ndim) array
        The input vector, repeated along the second axis for Ndim times.
    """
    matrix = np.tile(vector, [Ndim,1]).transpose()
    return matrix


def species_to_masses(species):
    """
    Routine to map species to atomic masses in a.m.u.

    Relies on ase.

    Parameters
    ----------
    species : (Natoms,) array or list
        Strings that give the species chemical symbols.

    Returns
    -------
    masses : (Natoms,) array
        The respective masses in atomic mass units.
    """
    import ase
    masses = np.asarray([ase.atom.atomic_masses[ase.atom.atomic_numbers[s]] for s in species])
    return masses


def check_kwargs(kwargs, param):
    """
    Cross-check a given dictionary of arguments against a reference dictionary
    with standard key-value pairs.

    Parameters
    ----------
    kwargs : dict
        The dictionary to be checked.

    param : dict
        The reference parameters

    Returns
    -------
    <True> if all keys in "kwargs" are found in "param", <False> if not.
    """
    ok = True
    for key in kwargs.iterkeys():
      if not key in param:
        raise ValueError('Unknown argument "{}"'.format(key))
        ok = False

    return ok

def randomize_seed():
    """
    Yield a random number between 0 and 4294967295 (largest seed to initialize
    the numpy RNG).
    """
    return np.random.randint(0,4294967295)


def normalize_str(string, pattern = r'[\s\-_\./]*'):
    """
    Normalize a given string. By removing all whitespaces, dots (.),
    underscores (_) and hyphens (-), and consequently converting to lower
    case letters. This allows to compare strings on a more reliable basis.

    Arguments
    ---------
    ''string''
        string
        String that is to be normalized.

    ''pattern''
        raw-string, optional (default : r'[\s\-_\./]*')
        Regex pattern that specifies every character that is to be deleted
        from the original string.

    Returns
    -------
    ''string''
        string
        Normalized string.
    """
    return re.sub(pattern, '', string).lower()

def which(program):
    """
    Small routine that mimicks the unix utility "which" (available only at
    python 3.3 via shutil, but well....)

    Routine is taken from: http://stackoverflow.com/a/377028
    """
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    # get rid of any mpi-related commands things (may be improved via appropriate regex)
    program = program.split()[-1]
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def timeit_auto(stmt="pass", setup="pass", repeat=3):
    """
    http://stackoverflow.com/q/19062202/190597 (endolith)
    Imitate default behavior when timeit is run as a script.

    Runs enough loops so that total execution time is greater than 0.2 sec,
    and then repeats that 3 times and keeps the lowest value.

    Returns the number of loops and the time for each loop in microseconds
    """
    t = timeit.Timer(stmt, setup)

    # determine number so that 0.2 <= total time < 2.0
    for i in range(1, 10):
        number = 10**i
        x = t.timeit(number) # seconds
        if x >= 0.2:
            break
    r = t.repeat(repeat, number)
    best = min(r)
    usec = best * 1e6 / number
    return number, usec


def get_all_combinations(arrays, out=None):
    """
    Generate a cartesian product of input arrays.

    Adapted from
            https://gist.github.com/hernamesbarbara/68d073f551565de02ac5

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.
    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])
    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in range(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out



def convert_walltime(walltime):
    """
    Converts a walltime string/float such that we have a proper float that
    corresponds to seconds!

    If a float is given, it will be interpreted as seconds, same for a string
    that can be converted to float. Alternatively, strings can be passed in
    format "hh:mm:ss" and will be converted.
    """
    if not walltime:
        return None
    elif isinstance(walltime, (float, int)):
        if walltime is True:
            raise ValueError('Walltime must not be <True>')
        return float(walltime)
    elif isinstance(walltime, str):
        try:
            return float(walltime)
        except ValueError:
            try:
                h, m, s = [float(i) for i in walltime.split(':')]
                return (h*60*60 + 60*m + s)
            except ValueError:
                msg = 'Invalid walltime format ("{}")'.format(walltime)
                raise ValueError(msg)

    else:
        msg = 'Wrong walltime type "{}"'.format(type(walltime))
        raise ValueError(msg)
