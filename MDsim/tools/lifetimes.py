# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import re
import numpy as np
import os
import sys

import netCDF4

from time import strftime
from MDsim import units

from MDsim.tools.netcdf import load_netcdf
from MDsim.analysis.fitting import fit_lifetime

import matplotlib.pyplot as plt

def fit_lifetimes(base_dir,
                  file_pattern,
                  tend=None, # in ps!
                  fitfunc='exponential_plus_constant',
                  result_folder=None,
                  log_fits=True,
                  show=False,
                  ):
    """
    Automatic fitting of decay rates for a bunch of simulations.
    Will yield one file per simulation.

    Proper documentation to follow.
    """

    if result_folder is None:
        result_folder = 'lifetimes_from_{}'.format(base_dir)

    if log_fits:
        log_folder = os.path.join(result_folder, 'logfiles')

    # create those folders
    try:
        os.makedirs(result_folder)
    except OSError:
        pass

    if log_fits:
        try:
            os.makedirs(log_folder)
        except OSError:
            pass

    results = {}

    # loop over the base_dir
    for dirpath, dirnames, filenames in os.walk(base_dir):
        for f in filenames:
            match = re.search(file_pattern, f)
            if not match:
                continue

            print(f)

            fname = os.path.join(dirpath, f)
            results[fname] = {}
            data = load_netcdf(fname).T

            if log_fits:
                logfile = os.path.join(log_folder,
                        'fit__{:s}.log'.format(f.replace('.nc', '')))
            else:
                logfile=None


            lifetime = fit_lifetime(times=data[0],
                                    signal=data[-1],
                                    verbose=False,
                                    fitfunc=fitfunc,
                                    end=tend,
                                    logfile=logfile,
                                    show=show)


            # extract information on the exited quanta from the *.info file
            with open(fname.replace('.nc', '.info'), 'r') as infofile:
                quanta = []
                read = 0
                for line in infofile:
                    if read == 2:
                        line_split = line.split()
                        if len(line_split) != 5:
                            break
                        else:
                            quanta.append(float(line_split[-2]))
                    elif read == 1:
                        read += 1

                    elif read == 0:
                        if line.startswith('#     Mode #'):
                            read +=1

            results[fname]['lifetime'] = lifetime
            results[fname]['quanta'] = np.array(quanta)

    with open(os.path.join(result_folder, 'extracted_lifetimes.dat'), 'w') as f:
        f.write('# File created: {}\n# Decay rates fitted to:'.format(strftime('%c')))
        for fname in sorted(results.keys()):
            f.write('\n#\t{}'.format(fname))
        f.write('\n#')

        head = ''

        for i in range(len(quanta)):
            head += '{:>30s}'.format('excited quanta mode #{}'.format(i))
        head += '{:>20s}'.format('lifetime (ps)')

        f.write('\n#' + '-'*len(head))
        f.write('\n#' + head)
        f.write('\n#' + '-'*len(head))

        for k in sorted(results.keys()):
            v = results[k]
            line = '\n '
            for q in v['quanta']:
                line += '{:>30.6f}'.format(q)
            # remember that we want pico seconds
            line += '{:>20.9f}'.format(v['lifetime']/1000)

            f.write(line)
    print()
    with open(os.path.join(result_folder, 'extracted_lifetimes.dat'), 'r') as f:
        for line in f:
            print(line.strip('\n'))

    return results
        # with open('{}/alphas_{}.dat'.format(result_folder, f.replace('.nc','')),'w') as f:
            # f.write('# File created: {}\n# Decay rates fitted to: {}\n#'.format(strftime('%c'), fname))
            # f.write('\n# {:<18s}{:<20s}'.format('|dK| (A^-1)', 'alpha (ps^-1)'))
            # for k, a in zip(kpts, alphas):
                # f.write('\n{:<20.9f}{:<20.9f}'.format(k,a))
