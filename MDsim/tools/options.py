# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module implementing the options class.
"""

from __future__ import print_function

import os
import copy
import re
import numpy as np
from distutils.util import strtobool

from MDsim.observables.implemented import on_the_fly as implemented_observables_otf

class Option(object):
    """
    Template for any option processed by the code.

    Initialization
    --------------
    name : string
        The name of the option, ie. the key via which it will be identified. It
        must consist of lower case letters only! No numbers, no special
        characters, whitespaces,... only exception is the underscore(_). This
        means, the <key> has to match the regex pattern r'[a-z_]+' and is
        checked for it. If it matches '[a-zA-Z_]+' it will be converted,
        otherwise an error is raised. Note that also the default key raises an
        error (for good reason).

    category : string, optional (default = 'misc')
        Category of this option. This helps to have some form of ordering at
        least. categories in this sense are section in the *.cfg file.

    val : any type, optional (default = None)
        The default value of this option. If you require user input or if this
        variable is not essential, set it to `None`.

    fmt : string, optional (default = 'str')
        Internal format of the keyword. One of {'str', 'float', 'int',
        'bool'} or a '*_array'/'*_list' with the respective format appended.
        Required to properly parse user input.

    choices : list, optional (default = [])
        If your option has a limited amount of choices for "val", add the
        respective list here. If the list evaluates to False (ie. empty list or
        False), any "val" is possible. Note that the choices will be converted
        to strings in order to allow comparison already at the file i/o level.

    multvals : bool, optional (default = False)
        If your can have more than one "val", set True. Of course, "val" must
        then be a list of something.

    case_sensitive : bool, optional (default = False)
        In case `val` is a string. Case sensitivity is important e.g. for
        filenames.

    info : string, optional (default = 'no info available')
        Short user info. Will be used in the creation of the argparser and may
        probably help at some time to create an internal help routine.

    alias : list of strings, optional (default = [])
        Alternative names for the option. `name` will be added in any case.

    _active : bool, optional (default = True)
        Whether option is actively supported within the code. Set False if
        disabled.
    """
    # a class-wide categories set
    _categories = set()

    _basis_formats = ['int', 'bool', 'float', 'str']
    _array_formats = ['{}_array'.format(b) for b in _basis_formats]
    _list_formats = ['{}_list'.format(b) for b in _basis_formats]

    _formats = _basis_formats + _array_formats + _list_formats

    # global verbosity switch
    _verbose = False

    def __init__(self,
                 name,
                 category='misc',
                 fmt='str',
                 val=None,
                 choices=[],
                 multvals=False,
                 case_sensitive=False,
                 alias=[],
                 info='no info available',
                 _active=True
                 ):
        # check the name first
        self.name = self._check_name(name)

        # check the category and add to the class-wide set
        self.category = self._check_category(category)
        self._categories.add(self.category)

        # check the format
        self.fmt = self._check_fmt(fmt)
        self.alias = self._check_alias(alias)

        # the remaining options do not need certain care
        self.multvals = multvals
        self.case_sensitive = case_sensitive
        self.info = info
        self._active = _active

        # checking choices requires the case-sensitive flag
        self.choices = self._check_choices(choices)

        self._help_str = self._create_help_string()

        # declare values as property
        self.val = val



    def help(self):
        print(self._help_str)
        return self._help_str

    def __str__(self):
        return self._help_str

    def _create_help_string(self):
        # internally generated help string
        help_str = '"{}"'.format(self.name) + '\n\n'
        help_str += self.info + '\n'
        help_str += '\nDetails'
        help_str += '\n-------'
        help_str += '\n* category        : {}'.format(self.category)
        help_str += '\n* format          : {}'.format(self.fmt)
        help_str += '\n* case sensitive  : {}'.format(self.case_sensitive)
        help_str += '\n* multiple values : {}'.format(self.multvals)

        if self.choices:
            help_str += '\n\nValid choices'
            help_str += '\n-------------'
            for c in self.choices:
                help_str += '\n* {}'.format(c)
        return help_str

    def _check_name(self, name):
        """
        Check if the name option is valid
        """
        if re.match(r'[a-z_]+', name):
            return name
        elif re.match(r'[a-zA-Z_]+', name):
            return name.lower()
        else:
            raise ValueError('Invalid option name "{}" does not match r"[a-z_]"'.format(name))


    def _check_category(self, category):
        """
        Check if the category option is valid
        """
        if re.match(r'[a-z_]+', category):
            return category
        elif re.match(r'[a-zA-Z_]+', category):
            return category.lower()
        else:
            raise ValueError('Invalid category name "{}" does not match r"[a-z_]"'.format(category))


    def _check_fmt(self, fmt):
        """
        Check if the fmt option is valid
        """
        if not isinstance(fmt, str):
            raise TypeError('Format specifier must be string')
        fmt = fmt.lower()
        if not fmt in self._formats:
            raise ValueError('Invalid format specifier "{}"'.format(fmt))
        else:
            return fmt


    def _check_alias(self, alias):
        """
        Check if the alias option is valid and add name if not in there.
        """
        # check the alias
        _alias = []
        for a in alias:
            _alias.append(self._check_name(a))
        # add the name to the alias...
        if not self.name in _alias:
            _alias.append(self.name)
        return _alias


    def _check_choices(self, choices):
        """
        Check if the choices option is valid.
        """
        # treat the choices the same way as the values
        _choices=[]
        if choices:
            if not self.fmt.startswith('str'):
                raise ValueError('Choices are only implemented for str-type options')
            for c in choices:
                _choices.append(self._format_to_str(c))
        return _choices


    def __str__(self):
        return 'Option object'


    def _format(self, val):
        """
        Generic format routine
        """
        if isinstance(val, str):
            # strip quotes
            val = re.sub(r'[\"\'\s]+', '', val)

        if self.fmt == 'str':
            val = self._format_to_str(val)
            self._validate_choice(val)

        elif self.fmt == 'int':
            val = self._format_to_int(val)

        elif self.fmt == 'float':
            val = self._format_to_float(val)

        elif self.fmt == 'bool':
            val =  self._format_to_bool(val)
        else:
            raise ValueError('Cannot format value')
        return val


    def _format_to_str(self, val, delete_pattern = r'[\s]+'):
        """
        Normalize a given string. By removing all whitespaces, and consequently
        converting to lower case letters (if required). This allows to compare
        strings on a more reliable basis.
        """
        if not isinstance(val, str):
            val = str(val)

        val = re.sub(delete_pattern, '', val)

        if self.case_sensitive:
            return val
        else:
            return val.lower()


    def _format_to_int(self, val):
        """
        Convert to an integer.
        """
        try:
            return int(val)
        except ValueError:
            err_msg = 'Only integer input accepted for option "{}"'.format(self.name)
            try:
                if self._verbose:
                    print(err_msg)
                    print('\t--> rounded value "{}" to {}'.format(val, int(float(val))))
                return int(float(val))
            except ValueError:
                err_msg += ' -- unable to convert value "{}"'.format(val)
                raise ValueError(err_msg)


    def _format_to_float(self, val):
        """
        Convert to a float.
        """
        try:
            return float(val)
        except ValueError:
            err_msg = 'Only integer/float input accepted for option "{}"'.format(self.name)
            err_msg += ' -- unable to convert value "{}"'.format(val)
            raise ValueError(err_msg)


    def _format_to_bool(self, val):
        """
        Convert to a boolean.
        """
        if isinstance(val, bool):
            return val
        try:
            val = str(val)
            return bool(strtobool(val.strip()))
        except ValueError:
            err_msg = 'Invalid input for option "{}"'.format(self.name)
            err_msg += ' -- unable to convert value "{}" to type bool'.format(val)
            raise ValueError(err_msg)


    @staticmethod
    def get_categories():
        return Option._categories


    def _validate_choice(self, val):
        """
        Validate a single value
        """
        if self.choices:
            val = self._format_to_str(val)
            if val not in self.choices:
                err_msg   = 'Invalid value "{}" for option "{}"'.format(val, self.name)
                valid_choices  = '\tvalid choices are:'
                for i in self.choices:
                    valid_choices += '\n\t\t"{}"'.format(i)
                if self._verbose:
                    print(err_msg)
                    print(valid_choices)
                raise ValueError(err_msg)
        return val


    def _validate_choices(self, vals):
        """
        Validate a list of values
        """
        _vals = []
        for val in vals:
            _vals.append(self._validate_choice(val))
        return _vals


    def get_value(self):
        """
        Getter function for the val property
        """
        return self._value


    def set_value(self, val):
        """
        Setter function for the val property

        Yes, it is a mess but functional...
        """

        if val is None:
            pass

        # if we have the basis types, ok
        elif type(val).__name__ == self.fmt:
            if isinstance(val, str) and val.lower().strip() == 'none':
                val = None
            else:
                val = self._format(val)

        # if we have an array and want an array
        elif isinstance(val, np.ndarray) and self.fmt.endswith('array'):
            dtype_str = self.fmt.split('_')[0]
            val = np.asarray(val, dtype=dtype_str)
            if dtype_str =='str':
                val = np.asarray(self._validate_choices(val.tolist()), dtype=dtype_str)

        # if we have an array but want a list
        elif isinstance(val, np.ndarray) and self.fmt.endswith('list'):
            dtype_str = self.fmt.split('_')[0]
            if dtype_str =='str':
                val = self._validate_choices(np.asarray(val, dtype=dtype_str).tolist())
            else:
                val = np.asarray(val, dtype=dtype_str).tolist()

        # if we have a list and want a list
        elif isinstance(val, list) and self.fmt.endswith('list'):
            # crude but functional : unify type via numpy
            val = np.asarray(val, dtype = self.fmt.split('_')[0]).tolist()
            if self.fmt.split('_')[0] =='str':
                val = self._validate_choices(val)

        # if we have a list but want an array
        elif isinstance(val, list) and self.fmt.endswith('array'):
            val = np.asarray(val, dtype=self.fmt.split('_')[0])
            if self.fmt.split('_')[0] =='str':
                val = np.asarray(self._validate_choices(val.tolist()))

        # if we have the element of what is supposed to be a list/array (not a
        # string!)
        elif (type(val).__name__ == self.fmt.split('_')[0]
              and (self.fmt.endswith('array') or self.fmt.endswith('list')
              and not isinstance(val, str))
              # strings are treatd differently
              and self.fmt.split('_')[0] !='str'):
            fmt, t = self.fmt.split('_')
            if t == 'array':
                val = np.asarray(var)
            elif t == 'list':
                val = [val]

        # fallback mode, convert to string if necessary and try again
        else:
            if not isinstance(val, str):
                val = str(val)
            # set the none also via strings
            if val.lower().strip() == 'none':
                val = None

            elif self.fmt in self._basis_formats:
                val = self._format(val)

            # this may not be crash-free...
            elif self.fmt in self._array_formats:
                dtype_str = self.fmt.split('_')[0]
                if ',' in val:
                    delim = r','
                elif ';' in val:
                    delim = r';'
                else:
                    delim ='\s'
                val = np.loadtxt(val.split('\n'), delimiter=delim, dtype=dtype_str)
                if not val.shape:
                    val.shape = (1,)
                if dtype_str =='str':
                    val = np.asarray(self._validate_choices(val.tolist()), dtype=dtype_str)

            elif self.fmt in self._list_formats:
                dtype_str = self.fmt.split('_')[0]
                if ',' in val:
                    delim = r','
                elif ';' in val:
                    delim = r';'
                else:
                    delim ='\s'
                val = np.loadtxt(val.split('\n'), delimiter=delim, dtype=dtype_str)
                # stupid loadtxt won't create single-element lists
                if val.size == 1:
                    val = [val.tolist()]
                else:
                    val=val.tolist()
                if dtype_str =='str':
                    val = self._validate_choices(val)
            else:
                raise ValueError('Could not convert value "{}"'.format(val))


        # finall we set the value
        self._value = val

    val = property(get_value, set_value)


def create_options_dict(options):
    """
    Convert an options list to an options dictionry that can be conveniently
    parsed.
    """
    options_dict = {}

    for o in options:
        if not o.category in options_dict.keys():
            options_dict[o.category]={}
        options_dict[o.category][o.name] = o

    return options_dict


def update_options_values(args, options_dict):
    """
    Update a given options dict. Note that this does not add any new options
    but rather updates values only. The format of args is plain with
    <category>.<option> as key.
    """
    for item in args.keys():
        try:
            category, option = item.lower().split('.')
        except ValueError:
            msg='Irregular option format {}'.format(item)
            raise ValueError(msg)
        if not category in options_dict.keys():
            msg = 'Unknown category "{}"'.format(category)
            raise ValueError(msg)
        else:
            if not option in options_dict[category].keys():
                msg = 'Unknown option "{}" in category "{}"'.format(option, category)
            else:
                options_dict[category][option].val = args[item]
    return options_dict


def create_options_set(options):
    """
    Create a set of available options from the options list. Note that we will
    have a <category>.<name> pattern. Convenient to check if input options are
    valid at all. If passed a list, a dictionary will be created first.
    """
    options_set = set()
    if isinstance(options, list):
        options = create_options_dict(options)
    for category in options.keys():
        for name in options[category].keys():
            options_set.add("{}.{}".format(category, name))
    return options_set


def create_args_dict(options_dict):
    """
    Convert the options dict to a plain dictionary holding the values
    """
    args = {}
    for category in options_dict.keys():
        for option_name in options_dict[category].keys():
            option = options_dict[category][option_name]
            args['{}.{}'.format(category, option_name)] = option.val
    return args

def check_args_dict_unknown(args, options_set):
    """
    Check the elements of the args dictionary for unknown parameters.
    """
    msg='Unknown option(s):'
    valid=True
    for key in args.keys():
        if key not in options_set:
            msg += '\n\t* {}'.format(key)
            valid=False
    if not valid:
        print(msg)

    return valid

def check_options_dict_unknown(options, options_set):
    """
    Check an options dictionary for unknown parameters.
    """
    return check_args_dict_unknown(create_args_dict(options), options_set)

# here comes a list of generic options one may use in several applications.
_generic_options = [

    # integrator related keywords
    Option(name='integrator',
           fmt='str',
           category='propagation',
           val='bussi parrinello',
           choices=['liouville',
                    'velocity verlet',
                    'bussi parrinello'],
           info='Time stepping algorithm used.'),

    Option(name='timestep',
           fmt='float',
           category='propagation',
           val=None,
           info='Integration time step in fs.'),

    Option(name='simulation_time',
           fmt='float',
           category='propagation',
           val=None,
           info='Duration of the simulation in fs.'),

    # IO-related keywords
    Option(name='seed',
           fmt='str',
           category='io',
           val='MDsim',
           case_sensitive=True,
           info='Common seed for all output files'),

    Option(name='outpath',
           fmt='str',
           category='io',
           val=os.path.join(os.getcwd(), 'output'),
           case_sensitive=True,
           info='Path to the directory for all output files. By default <cwd>/output.'),

    Option(name='observables',
           fmt='str_list',
           category='io',
           val=None,
           multvals=True,
           choices=implemented_observables_otf,
           info='Observables for which there will be an output.'),

    Option(name='iatom',
           fmt='str_array',
           category='io',
           val=None,
           info='Atom-ID for atomic observables.'),

    Option(name='datatype',
           fmt='str',
           category='io',
           val='f8',
           choices=['f4','f8','i4','i2','i8','i1','u1','u2','u4','u8'],
           info='Data type within the output files. Only affects netCDF4 files and in-memory handling.'),

    Option(name='fileformat',
           fmt='str',
           category='io',
           val='ascii',
           choices=['ascii', 'netcdf4', 'netcdf', 'netcdf4_inmemory'],
           info='Format of the output files. NetCDF4 is much more efficient but requires external libs.'),

    Option(name='compress',
           fmt='bool',
           category='io',
           val=False,
           info='Compress output files via gzip or zlib for ASCII or netCDF4 files, respectively.'),

    Option(name='debug',
           val=False,
           category='io',
           fmt='bool',
           case_sensitive=False,
           info='Write more information to outfiles.'),

    Option(name='iostep',
           val=10,
           category='io',
           fmt='int',
           info='Multiple of integration steps at which we write info to the outfiles.'),

    # miscellaneous settings
    Option(name='random_seed',
           val=None,
           category='misc',
           fmt='int',
           info='Seed for the random number generator.'),

    # miscellaneous settings
    Option(name='walltime',
           val=None,
           category='misc',
           fmt='int',
           info='Walltime in seconds'),

    # miscellaneous settings
    Option(name='show_system',
           val=False,
           category='misc',
           fmt='bool',
           info='Show the initial state of the system.'),

    # A fake, inactive infile Option for the command line parser
    # This allows us to use the same routines to check for completeness
    # etc. for input file and command line args.
    Option(name='infile',
           val='MDsim.in',
           fmt='str',
           case_sensitive=True,
           info='Input file to control MDsim settings',
           _active=False),

    ]



# convenient imports
generic_options_dict = create_options_dict(_generic_options)
generic_options_set = create_options_set(_generic_options)
