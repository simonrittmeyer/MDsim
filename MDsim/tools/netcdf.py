# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from time import strftime
import netCDF4

"""
This is an extratool in order not to interfere with the regular tools because
of import issues
"""

def load_netcdf(fname, usecols=None, unpack=False):
    """
    Equivalent to numpy's loadtxt() routine. Load a netCDF4 file written by the
    MDsim NetCDFHandler into memory. Datatype is determined by the content of
    the netcdf file.
    """
    ncfile = netCDF4.Dataset(fname, 'r')
    if usecols is None:
        data = ncfile.variables['data'][:]
    else:
        usecols = list(usecols)
        data = nfile.variables['data'][:,usecols]

    if unpack:
        data = data.T

    return data


def write_netcdf(fname, array, dtype='f8', compression=False, header=None):
    """
    More or less equivalent to numpy's savetxt() routine. Dump a numpy array to
    a netCDF4 file.

    Parameters
    ----------
    fname : string
        The name of the netCDF4 file.

    array : numpy array
        The numpy array to be dumped. Must be 2D for the moment.

    dtype : string, optional (default = 'f8')
        Datatype of the dumped numerical information. Available are
            'f4' : 32-bit floating point
            'f8' : 64-bit floating point
            'i4' : 32-bit signed integer
            'i2' : 16-bit signed integer
            'i8' : 64-bit singed integer
            'i1' : 8-bit signed integer
            'u1' : 8-bit unsigned integer
            'u2' : 16-bit unsigned integer
            'u4' : 32-bit unsigned integer
            'u8' : 64-bit unsigned integer

    compression : boolean, optional (default = False)
        Use zlib compression.

    header : string, optional (default=None)
        Additional information written to the file.
    """
    if not len(array.shape) == 2:
        raise NotImplementedError('Only 2D arrays are supported')

    # create the netCDF4 file
    _dtypes = ['f4',
               'f8',
               'i4',
               'i2',
               'i8',
               'i1',
               'u1',
               'u2',
               'u4',
               'u8'
              ]

    dtype = dtype.lower()
    if not dtype in _dtypes:
        raise ValueError('Unknown data type "{}"'.format(dtype))

    ncfile = netCDF4.Dataset(fname, 'w', format='NETCDF4')

    # all the axes
    ncfile.createDimension('rows', array.shape[0])
    ncfile.createDimension('cols', array.shape[1])

    data = ncfile.createVariable('data',
                                 dtype,
                                 dimensions=('rows', 'cols'),
                                 zlib=compression)

    ncfile.created = 'File created : {}'.format(strftime('%c'))
    if header is not None:
        ncfile.header = header

    # last but not least, write the data
    data[:,:] = array
    ncfile.close()

# aliasing
def savenc(*args, **kwargs):
    write_netcdf(*args, **kwargs)

def loadnc(*args, **kwargs):
    return load_netcdf(*args, **kwargs)
