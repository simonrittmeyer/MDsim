# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim.tools import check_kwargs
from MDsim.tools import randomize_seed
from MDsim.tools import normalize_str

def create_integrator(**kwargs):
    """
    Create an integrator instance

    Parameters
    ----------
    method : string, optional ({*'velocityverlet'*, 'liouville', 'vec', 'bbk', 'gjf'})
        The integrator method that shall be used.

    dt : float, optional (default = 0.1)
        The time-step in femto seconds.

    seed : integer, optional (default = None)
        Seed for the random number generator for "tuckerman" and "bbk".

    Returns
    -------
    integrator : Integrator instance.
    """

    param = {'method'  : 'velocityverlet', # works in any case!
             'dt'      : 0.1,         # femtoseconds!
             'convert' : True,
             'seed'    : None,
             'tensorial_friction' : False,
             'verbose' : True}        # random seed

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']
    method = normalize_str(param['method'])

    if param['seed'] is not None:
        seed = param['seed']
    else:
        seed = randomize_seed()

    msg = 'Creating integrator instance (method = "{}")'

    if method in ['velocityverlet', 'vv']:
        from MDsim.integrator.velocityverlet import VelocityVerlet
        integrator = VelocityVerlet(dt = param['dt'],
                                    convert = param['convert'])
        msg = msg.format('Velocity Verlet')

    elif method in ['liouville']:
        if param['tensorial_friction']:
            from MDsim.integrator.langevin.frictiontensor.liouville import LiouvilleFrictionTensor as Liouville
            msg = msg.format('Liouville (for tensorial friction)')
        else:
            from MDsim.integrator.liouville import Liouville
            msg = msg.format('Liouville')
        integrator = Liouville(dt = param['dt'],
                               convert = param['convert'])

    elif method in ['vec', 'vandeneijndenciccotti']:
        if param['tensorial_friction']:
            raise NotImplementedError('Tensorial friction not supported for method "{}"'.format(method))
        from MDsim.integrator.langevin.vandeneijndenciccotti import LangevinVEC
        integrator = LangevinVEC(dt = param['dt'],
                                 _random_seed = seed,
                                 convert = param['convert'])
        msg = msg.format('Vanden-Eijnden/Cicotti')

    elif method in ['bbk', 'bruengerbrookskarplus', 'brungerbrookskarplus']:
        if param['tensorial_friction']:
            raise NotImplementedError('Tensorial friction not supported for method "{}"'.format(method))
        from MDsim.integrator.langevin.bruengerbrookskarplus import LangevinBBK
        integrator = LangevinBBK(dt = param['dt'],
                                 _random_seed = seed,
                                 convert = param['convert'])

        msg = msg.format('Bruenger/Brooks/Carplus')
        raise ValueError('Implementation of the {} integrator is seemingly broken')

    elif method in ['gjf', 'gronbechjensenfarago']:
        if param['tensorial_friction']:
            raise NotImplementedError('Tensorial friction not supported for method "{}"'.format(method))
        from MDsim.integrator.langevin.gronbechjensenfarago import LangevinGJF
        integrator = LangevinGJF(dt = param['dt'],
                                 _random_seed = seed,
                                 convert = param['convert'])
        msg = msg.format('Gronbech-Jensen/Farago')

    elif method in ['bp', 'bussiparrinello']:
        if param['tensorial_friction']:
            from MDsim.integrator.langevin.frictiontensor.bussiparrinello import LangevinBPFrictionTensor as LangevinBP
            msg = msg.format('Bussi/Parrinello (for tensorial friction)')
        else:
            from MDsim.integrator.langevin.bussiparrinello import LangevinBP
            msg = msg.format('Bussi/Parrinello')
        integrator = LangevinBP(dt = param['dt'],
                                 _random_seed = seed,
                                 convert = param['convert'])

    else:
        msg = 'Error : Integrator "{}" not implemented'.format(method)
        raise NotImplementedError(msg)

    if verbose:
        print(msg)

    return integrator


