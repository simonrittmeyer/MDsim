# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim.tools import check_kwargs
from MDsim.bindings.ase.mdsimtoase import TrajectoryHandler

def create_trajectoryhandler(system_inst, **kwargs):
    """
    Create an ase trajectory handler instance.

    Parameters
    ----------
    system_inst : MDsim system
        The actual syste to be observed.

    seed : string, optional (default = 'MDsim')
        The seed that will be prepended to the output file name.

    outfolder : string, optional (default = 'output')
        The folder in which the trajectory file will be stored.

    system_to_atoms : function, optional (default = None)
        Function that converts the system's positions to an ase atoms object.
        If <None> is given, then the converter from
        MDsim.bindings.ase.mdsimtoase.convert is used. See also there for
        details on the expected function behavior.

    system_to_calc : function, optional (default = None)
        Function that creates an ase calculatur out of the MDsim system. If
        <None> is given, then the converter from
        MDsim.bindings.ase.mdsimtoase.convert is used. See also there for
        details on the expected function behavior.

    _traj_class : string, optional ({*'trajectory'*, 'bundletrajectory'})
        Which kind of ase trajectory to generate.

    properties : list of strings, optional (default = ['energy', 'forces'])
        Which properties to be stored in the trajectory file besides the atomic
        positions and momenta.


    Returns
    ------
    TrajectoryHandler instance from MDsim.
    """
    param = {'seed' : 'MDsim',
             'outfolder' : 'output',
             'verbose' : True,
             'system_to_atoms' : None,
             'system_to_calc' : None,
             '_traj_class' : 'trajectory',
             'properties' : ['energy', 'forces']
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param.pop('verbose')

    if verbose:
        print('Creating ASE trajectory handler')
        print('\tobserved calculator properties in trajectory file:')
        for p in param['properties']:
            print('\t\t"{}"'.format(p))

    handler = TrajectoryHandler(system_inst, **param)

    return handler
