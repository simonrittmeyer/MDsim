# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

from MDsim import units
from MDsim import tools

def create_kvectors(surface_facet,
                    surface_direction,
                    Nkpts=20,
                    min_amplitude=0,
                    max_amplitude=4.,
                    supercell=None,
                    convert=True,
                    verbose=True):
    """
    Create the dK values that are also in agreement with the simulation cell
    you are using (a scattering condition of the supercell). Otherwise you will
    not see any interactions in the dK-alpha lines.

    Parameters
    ----------
    surface_facet : str
        The actual surface facet. Until now there is only "fcc111"
        implemented.

    surface_direction : str
        The respective surface direction. See
        create_real_space_surface_direction from
        MDsim.coordinates.diffusion.fcc111 for available directions.

    Nkpts : int
        The number of vectors (or k-points if you wish). They will be evenly
        distributed from one segement above 0 to max_amplitude.

    min_amplitude : float, optional (default=0.)
        The minimum dK-amplitude.

    max_amplitude : float, optional (default=4.)
        The maximum dK-amplitude.

    supercell : (2,2) array, optional (default=None)
        The supercell in case of interacting particles. One can only "see"
        interactions if the resepctive dK values are sum_i=1^2(n[i] * 2*pi /
        supercell lattice[i]), ie., if they fulfill the scattering conditions
        of the super cell. Otherwise, one loses all phase information of
        particles that are *not* within the unit cell.

    convert : bool, optional (default=True)
        Convert min/max_amplitude and supercell from Angstrom^-1 to au.

    verbose : bool, optional (default=True)
        Print information to stdout.

    Returns
    -------
    kvectors : (Nkpts, 2) array
        The k vectors. Note that all the calc_scattering_amplitudes() require
        the transpose of this matrix.
    """

    if surface_facet.lower() in ['fcc111', 'fcc100']:
        from MDsim.coordinates.diffusion import create_surface_direction
        direction = create_surface_direction(surface_facet=surface_facet.lower(),
                                             direction=surface_direction)

    else:
        err = 'Surface facet "{}" not implemented'.format(surface_facet)
        raise NotImplementedError(err)

    # no one needs the de-facto constant dK=0 contribution
    segments = np.linspace(min_amplitude, max_amplitude, Nkpts, dtype=np.float64)

    if convert:
        unit = 'au'
        segments /= units.ANGSTROM_TO_AU
    else:
        unit = 'A^-1'

    msg = 'Creating dK vectors for {} direction on {} surface'.format(surface_direction,
                                                                      surface_facet)
    msg += '\n\tdemanded {} vectors from {} to {} {}'.format(Nkpts, segments[0], segments[-1], unit)

    if verbose:
        print(msg)

    requested_dKs = segments[:,None]*direction

    if supercell is not None:
        # here comes the matching to the super cell vectors

        # check for the orthorhombic cell
        tools.periodic.check_orthorhombic_cell(supercell)

        # the reciprocal lattice of the super cell
        g0_divider = 2.0 * np.pi / np.diagonal(supercell)

        if convert:
            g0_divider /=  units.ANGSTROM_TO_AU

        _adjusted_dKs = g0_divider[None,:] * np.ceil(requested_dKs/g0_divider[None,:])

        # only the unique dKs (http://stackoverflow.com/a/16971324)
        adjusted_dKs = _adjusted_dKs[np.lexsort(_adjusted_dKs.T)]
        adjusted_dKs = adjusted_dKs[np.concatenate(([True], np.any(adjusted_dKs[1:] != adjusted_dKs[:-1],axis=1)))]

        if verbose:
            msg = '\tadjusting created dK vectors to match scattering conditions of the super cell'
            dKs_abs = np.sqrt(adjusted_dKs[:,0]**2 + adjusted_dKs[:,1]**2)
            msg += '\n\t--> resulted in {0:} vectors from min(dK) = {1:.3f} {3:} to max(dK) = {2:.3f} {3:}'.format(dKs_abs.shape[0],
                                                                                                                   np.min(dKs_abs),
                                                                                                                   np.max(dKs_abs),
                                                                                                                   unit)
            if dKs_abs.shape[0] < Nkpts:
                msg += '\n\tWARNING: Could you create as many vectors as requested due to super cell constraints'

            print(msg)


        return adjusted_dKs

    else:
        if verbose:
            msg = '\t--> using equally spaced points as no super cell is given'
            print(msg)
        return requested_dKs
