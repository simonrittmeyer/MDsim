# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import sys
import re
import string

from MDsim.tools import check_kwargs

import MDsim.observables
from MDsim.observables.implemented import implemented
from MDsim.observables.implemented import postprocessing
from MDsim.observables.implemented import require_iatom
from MDsim.observables.implemented import require_delta_K
from MDsim.observables.implemented import require_fft
from MDsim.observables.implemented import require_alternative_eta
from MDsim.observables.implemented import require_scattering_amplitudes
from MDsim.observables.implemented import require_normalmodes

def convert_name_to_class(name):
    # all in small letters
    name = re.sub(r'\s', '', string.capwords(name.replace('_',' ').lower()))
    return name


def create_observable(system_inst, observable, **kwargs):
    param = {'iatom' : None,
             'verbose' : True,
             'min_width' : 20,
             'min_spacing' : 2,
             'fmt' : '{}.9E',
             'delta_K' : None,
             'alternative_eta' : None,
             'normalmodes' : None,
             'convert' : True}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    # formatters sub directory
    formatters = ['min_width', 'min_spacing', 'fmt']
    kwargs = {k : param[k] for k in formatters}

    # sanity check already at this stage
    obs = observable.lower()
    iatom = param['iatom']
    delta_K = param['delta_K']
    alternative_eta = param['alternative_eta']
    normalmodes = param['normalmodes']

    if obs in implemented:
        if obs in require_iatom and iatom is None:
            raise ValueError('Observable "{}" requires "iatom" != <None>'.format(obs))
        if obs in require_delta_K and delta_K is None:
            raise ValueError('Observable "{}" requires "delta_K" input'.format(obs))
        if obs in require_alternative_eta and alternative_eta is None:
            raise ValueError('Observable "{}" requires "alternative_eta" input'.format(obs))
        if obs in require_normalmodes and normalmodes is None:
            raise ValueError('Observable "{}" requires "normalmodes" input'.format(obs))
        if obs in postprocessing:
            raise ValueError('Observable "{}" is a PostProcessingObservable'.format(obs))
    else:
        raise NotImplementedError('Observable "{}" not implemented!'.format(obs))

    msg = 'Creating Observable instance for observable "{}"'.format(obs)

    Obs = getattr(MDsim.observables.implemented, convert_name_to_class(obs))

    if obs in require_iatom:
        msg += ' (iatom = {})'.format(param['iatom'])
        kwargs['iatom'] = iatom
    if obs in require_delta_K:
        kwargs['delta_K'] = delta_K
        kwargs['convert'] = param['convert']
    if obs in require_alternative_eta:
        kwargs['alternative_eta'] = alternative_eta
    if obs in require_normalmodes:
        kwargs['normalmodes'] = normalmodes

    obs_inst = Obs(system=system_inst, **kwargs)

    msg += '\n\t(class "{}")'.format(convert_name_to_class(obs))

    if param['verbose']:
        print msg
    return obs_inst


def create_postprocessingobservable(system_inst, observable, **kwargs):
    param = {'verbose' : True,
             'scattering_amplitudes_file' : None,
             'scattering_amplitudes' : None,
             'times' : None,
             'autocorrelation' : 'linear',
             'optimize_length' : True,
             'delta_K' : None,
             'normalize' : True,
             'convert' : True}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    # sanity check already at this stage
    obs = observable.lower()

    delta_K = param['delta_K']

    if obs in implemented:
        if obs not in postprocessing:
            raise ValueError('Observable "{}" is not a PostProcessingObservable'.format(obs))
        if obs in require_delta_K and delta_K is None:
            raise ValueError('Observable "{}" requires "delta K vector" input'.format(obs))
    else:
        raise NotImplementedError('Observable "{}" not implemented!'.format(obs))

    msg = 'Creating Observable instance for observable "{}"'.format(obs)
    Obs = getattr(MDsim.observables.implemented, convert_name_to_class(obs))

    kwargs = {'normalize' : param['normalize']}
    if obs in require_delta_K:
        kwargs['convert'] = param['convert']
        kwargs['delta_K'] = delta_K
    if obs in require_fft:
        kwargs['autocorrelation'] = param['autocorrelation']
        kwargs['optimize_length'] = param['optimize_length']
    if obs in require_scattering_amplitudes:
        kwargs['scattering_amplitudes_file'] = param['scattering_amplitudes_file']
        kwargs['scattering_amplitudes'] = param['scattering_amplitudes']
        kwargs['times'] = param['times']

    obs_inst = Obs(system=system_inst, **kwargs)

    msg += '\n\t(class "{}")'.format(convert_name_to_class(obs))

    if param['verbose']:
        print msg

    return obs_inst
