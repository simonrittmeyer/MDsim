# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

from MDsim import units
from MDsim.tools import check_kwargs
from MDsim.tools import randomize_seed
from MDsim.tools import get_all_combinations
from MDsim.tools.periodic import Distances

def create_dummy_system(Natoms=100, Ndim=3):
    from MDsim.systems.system import System
    p = np.random.randn(Natoms, Ndim)

    species = ['H']*Natoms

    system = System(positions=p,
                    velocities=None,
                    species=species,
                    temperature=354)

    return system


def create_surfaceperiodicsystem(Natoms,
                                 Ndim,
                                 species,
                                 coverage,
                                 surface_lattice_constant,
                                 **kwargs):

    param = {'saturation_coverage' : 1.,
             'surface_facet' : 'fcc111',
             'initial_positions' : 'random',
             'force_cutoff' : None,
             'p0' : None,
             'polarizability' : None,
             'pes' : None,
             'friction_matrix' : None,
             'verbose' : True,
             'temperature' : 0.,
             'random_seed' : None,
             'list_cutoff' : None,
             'masses' : None}

    from MDsim.potentials.pairwise.kohnlaupotential import topping_model
    from MDsim.systems.pairwise.kohnlausystem import KohnLauSystem
    from MDsim.systems.periodicsystem import PeriodicSystem
    from MDsim.coordinates.diffusion import distribute_initial_positions

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    if param['p0'] is None:
        p0 = -1
    else:
        p0 = param['p0']

    # user warnings
    if p0 > 0.:
        if param['force_cutoff'] is None:
            msg = 'Kohn-Lau system requires "force_cutoff"'
            raise ValueError(msg)
        if param['polarizability'] is None:
            msg = 'Kohn-Lau system requires "polarizability"'
            raise ValueError(msg)
        if param['initial_positions'] == 'origin':
            msg = 'Initial positions at origin will cause singularities in pairwise interaction potential'
            raise ValueError(msg)

    if not coverage > 0.:
        msg = 'Warning : Initializing system with zero coverage. Are you sure?'
        print(msg)

    # sanity checks
    if Ndim != 2:
        msg = 'Kohn-Lau/Periodic system is designed for surface diffusion within a 2D model'
        raise ValueError(msg)

    if verbose:
        if p0 > 0.:
            print('Creating Kohn-Lau system')
        else:
            print('Creating Periodic system')

    if param['surface_facet'].lower() in ['fcc111', 'fcc100']:
        from MDsim.coordinates.diffusion import create_real_space_lattice
        from MDsim.coordinates.diffusion import create_surface_supercell

        # adatom density at full coverage
        primitive_cell = create_real_space_lattice(surface_facet=param['surface_facet'].lower(),
                                                   a=surface_lattice_constant)

        # the primitive cell contains 1 surface atom
        n0 = param['saturation_coverage'] / np.abs(np.cross(*primitive_cell))

        if verbose:
            print('\tConstructing supercell with Natoms = {}'.format(Natoms))

        super_cell_dict = create_surface_supercell(surface_facet=param['surface_facet'].lower(),
                                                   a=surface_lattice_constant,
                                                   coverage=coverage,
                                                   saturation_coverage=param['saturation_coverage'],
                                                   squarify=True,
                                                   fix_Natoms=Natoms,
                                                   _verbose=False)
        super_cell = super_cell_dict['supercell']

        if verbose:
            print('\t\tCreated surface coverage of {:.4f} ML (target {:.4f} ML)'.format(super_cell_dict['true_coverage'],
                                                                                        super_cell_dict['target_coverage']))

            print('\t\tThis required {} cells in x- and {} cells in y-direction'.format(*super_cell_dict['cell_factors']))
    else:
        msg = 'Surface facet "{}" not implemented'.format(param['surface_facet'])
        raise NotImplementedError(msg)


    # initialize the random number generator
    if param['random_seed'] is None:
        random_seed = randomize_seed()
    else:
        random_seed = param['random_seed']

    # randomly distribute position and scale with the super cell size
    positions = distribute_initial_positions(super_cell_dict=super_cell_dict,
                                             initial_positions=param['initial_positions'].lower(),
                                             verbose=verbose,
                                             _random_seed=random_seed)


    args = {'unit_cell' : super_cell,
            'positions' : positions,
            'velocities' : None,
            'species' : species,
            'masses' : param['masses'],
            'potential' : param['pes'],
            'friction' : param['friction_matrix'],
            'temperature' : param['temperature'],
            'convert' : True,
            '_random_seed' : random_seed,
            '_coverage' : super_cell_dict['true_coverage'],
            '_cell_repeats' : super_cell_dict['cell_factors'],
            '_verbose' : False
            }

    if not p0 > 0:
        system = PeriodicSystem(**args)

    else:
        if verbose:
            print('\tEvaluating coverage dependent dipole moment following the Topping model')

        force_cutoff = float(param['force_cutoff'])
        polarizability = float(param['polarizability'])

        dipole_moment = topping_model(p0=p0,
                                      coverage=coverage,
                                      alpha_p=polarizability,
                                      n0=n0,
                                      convert=True)*units.AU_TO_DEBYE

        if verbose:
                print('\t\tObtained p = {:.6f} Debye (p0 = {:.6f} Debye)'.format(dipole_moment, p0))

        if param['list_cutoff'] is None:
            list_cutoff = 1.8 * force_cutoff
        else:
            list_cutoff = param['list_cutoff']

        args.update({'dipoles' : dipole_moment,
                     'force_cutoff' : force_cutoff,
                     'list_cutoff' : list_cutoff})

        system = KohnLauSystem(**args)

    if verbose:
        print('System initialized')
        print('\t* Initial instantaneous temperature is {:.2f} K'.format(system.get_instantaneous_temperature()))

        Epot_av = system._calc_pes_potential_energy(system.get_positions(_au=True))*units.AU_TO_EV / Natoms
        print('\t* Initial potential energy per atom due to PES is {:.3f} eV'.format(Epot_av))

        if p0 > 0:
            Epot_av = system._calc_pairwise_potential_energy(system.get_positions(_au=True))*units.AU_TO_EV / Natoms
            print('\t* Initial potential energy per atom due to pairwise interactions is {:.3f} eV'.format(Epot_av))

        dist = Distances().calc_distances_mic(system.get_positions(), system.get_cell())[np.tril_indices(Natoms, -1)]
        print('\t* Minimum initial distance between atoms is {:.1f} A'.format(np.min(dist)))
        print('\t* Maximum initial distance between atoms is {:.1f} A'.format(np.max(dist)))

    return system

