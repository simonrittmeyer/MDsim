# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim.tools import check_kwargs
from MDsim.observables.implemented import require_iatom
from MDsim.tools.createobjects.observables import create_observable
from MDsim.prettyprint.stdouthandler import StdoutHandler

def create_stdouthandler(system_inst, integrator_inst, observable, **kwargs):
    param = {'iatom' : None,
             'args' : None,
             'verbose' : True,
             'min_width' : 20,
             'min_spacing' : 2,
             'fmt' : '{}.9E'
             }


    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # pop stuff from param such that we can use param to directly expand in the
    # create_observable() function
    args = param.pop('args')

    # sanity checks are done in the create_observable() function
    observable = observable.lower()

    # it is enough if the file handler talks to us
    param['verbose'] = False
    observable_inst = create_observable(system_inst, observable, **param)

    msg = 'Creating StdoutHandler instance for observable "{}"'.format(observable)

    if observable in require_iatom:
       msg += ' (iatom = {})'.format(param['iatom'])

    if verbose:
        print msg

    stdoutHandler = StdoutHandler(system = system_inst,
                                 integrator = integrator_inst,
                                 observable = observable_inst,
                                 args = args)

    return stdoutHandler
