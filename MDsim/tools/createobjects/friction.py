# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import os
import glob

import numpy as np

from MDsim.tools import check_kwargs

def create_bivariatefrictioncoefficient(datafile, **kwargs):

    param = {'periodic' : False,
             'coordinates' : 'atomic',
             'bounds_check' : True,
             'usecols'  : (0,1,2),
             'convert' : True,
             'masses' : None,
             'positions_eq' : None,
             'verbose' : True}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    coords = param['coordinates'].lower()
    # sanity check
    if not coords in ['atomic', 'diatomic-perp', 'generic-dof']:
        raise ValueError('Unknown coords "{}"'.format(coords))

    # read in the potential values
    data = np.loadtxt(datafile, usecols = param['usecols'], unpack = True)

    data_origin = datafile
    data_origin +='\n\t(using columns {}, {} and {})'.format(*param['usecols'])

    periodic = param['periodic']

    if coords == 'atomic':
        if periodic:
            if verbose:
                print('Creating PeriodicAtomicBivariateFrictionCoefficient instance')

            from MDsim.friction.electronicfriction.ldfa.coefficient.atomic.periodic.periodicbivariatefriction import PeriodicAtomicBivariateFrictionCoefficient
            friction =  PeriodicAtomicBivariateFrictionCoefficient(points = data[0:2].T,
                                                        etas = data[2],
                                                        data_origin = data_origin,
                                                        convert = param['convert'],
                                                        function = 'RectBivariateSpline',
                                                        kind = 'cubic'
                                                        )
        else:
            if verbose:
                print('Creating AtomicBivariateFrictionCoefficient instance')
            from MDsim.friction.electronicfriction.ldfa.coefficient.atomic.atomicbivariatefriction import AtomicBivariateFrictionCoefficient
            friction =  AtomicBivariateFrictionCoefficient(points = data[0:2].T,
                                                etas = data[2],
                                                data_origin = data_origin,
                                                convert = param['convert'],
                                                function = 'RectBivariateSpline',
                                                kind = 'cubic',
                                                _bounds_check = param['bounds_check'])
    elif coords == 'diatomic-perp':
        if periodic:
            msg  = 'Error : Asked for Periodic/DiatomicPerp Coordinates BivariateFriction combination.\n'
            msg += '        This is not (yet) implemented.'
            if verbose:
                print(msg)
            raise NotImplementedError(msg)

        else:
            if param['masses'] is None:
                msg = 'DiatomicPerpFrictionCoefficient instance requires masses to do the coordinate transformations'
                raise ValueError(msg)

            if verbose:
                print('Creating DiatomicPerpFrictionCoefficient instance')

            from MDsim.friction.electronicfriction.ldfa.coefficient.internal.vibrations.diatomic.perp import DiatomicPerpFrictionCoefficient
            friction =  DiatomicPerpFrictionCoefficient(points = data[0:2].T,
                                                        etas = data[2],
                                                        data_origin = data_origin,
                                                        convert = param['convert'],
                                                        function = 'RectBivariateSpline',
                                                        masses=param['masses'],
                                                        positions_eq=param['positions_eq'],
                                                        kind = 'cubic',
                                                        _bounds_check = param['bounds_check'])
    elif coords =='generic-dof':
        if periodic:
            msg  = 'Error : Asked for Periodic/GenericDOFBivariateFrictionCoefficient combination.\n'
            msg += '        This is not (yet) implemented.'
            if verbose:
                print(msg)
            raise NotImplementedError(msg)

        else:
            if verbose:
                print('Creating GenericDOFBivariateFrictionCoefficient instance')
            from MDsim.friction.electronicfriction.ldfa.coefficient.internal.generic import GenericDOFBivariateFrictionCoefficient
            potential =  GenericDOFBivariateFrictionCoefficient(points=data[0:2].T,
                                                                etas=data[2],
                                                                data_origin=data_origin,
                                                                convert=param['convert'],
                                                                function='RectBivariateSpline',
                                                                kind='cubic',
                                                                 _bounds_check=param['bounds_check'])
    else:
        raise NotImplementedError('Coordinates "{}" not implemented'.format(coords))

    if verbose:
        print('\tusing file {}'.format(datafile))

    return friction


def create_minimumbasisfourierfrictioncoefficient(a,**kwargs):
    param = {'etas_dict' : None,
             'datafile' : None,
             'datapath' : 'data',
             'surface' : 'fcc111',
             'verbose' : True,
             'convert' : True,
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # read from file if not passed as dictionary
    if param['etas_dict'] is None:
        if param['datafile'] == None:
            raise ValueError('Pass either "etas_dict" or "datafile"')
        else:
            datafile = param['datafile']

        data = np.loadtxt(datafile, dtype = str)

        # create the energies dict
        etas_dict = {}
        for d in data:
            etas_dict[d[0].lower()] = float(d[1])
    else:
        etas_dict = param['etas_dict']

    if param['surface'].lower() == 'fcc111':
        from MDsim.friction.electronicfriction.ldfa.coefficient.atomic.periodic.diffusion.fcc111 import MinimumBasisFourierFrictionFCC111
        if verbose:
            print('Creating MinimumBasisFourierFrictionFCC111 instance')

        friction = MinimumBasisFourierFrictionFCC111(a = a,
                                                     etas_dict = etas_dict,
                                                     convert = param['convert'])


    else:
        msg = 'Surface facet "{}" not implemented'.format(param['surface'])
        raise NotImplementedError(msg)

    if verbose and param['etas_dict'] is None:
        print('\tusing file {}'.format(datafile))

    return friction


def create_fourierfrictioncoefficient(a, real_order, imag_order, datafile, **kwargs):
    param = {'usecols' : (0, 1, 2),
             'constant' : True,
             'surface' : 'fcc111',
             'verbose' : True,
             'convert' : True,
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # read in the potential values
    data = np.loadtxt(datafile, usecols = param['usecols'], unpack = True)

    if param['surface'].lower() == 'fcc111':
        from MDsim.friction.electronicfriction.ldfa.coefficient.atomic.periodic.diffusion.fcc111 import FourierFrictionFCC111

        if verbose:
            print('Creating FourierFrictionFCC111 instance')
            print('\treal_order = {}; imag_order = {}; constant = <{}>'.format(real_order,
                                                                               imag_order,
                                                                               param['constant']))
        potential = FourierFrictionFCC111(a=a,
                                          points=data[0:2,:].T,
                                          etas=data[2],
                                          real_order=real_order,
                                          imag_order=imag_order,
                                          constant=param['constant'],
                                          convert = param['convert'])


    else:
        msg = 'Surface facet "{}" not implemented'.format(param['surface'])
        raise NotImplementedError(msg)

    if verbose:
        print('\tusing file {}'.format(datafile))

    return potential


def create_tensorialfriction(masses, **kwargs):
    param = {'friction_matrix_mw' : None,
             'datafile' : None,
             'verbose' : True,
             'convert' : True,
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # read in the potential values
    if param['friction_matrix_mw'] is None and param['datafile'] is None:
        raise ValueError('Pass either "friction_matrix_mw" or "datafile"')

    if verbose:
        print('Creating FrictionTensor instance')
    if param['friction_matrix_mw'] is None:
        param['friction_matrix_mw'] = np.loadtxt(datafile)
        if verbose:
            print('\tusing file {}'.format(datafile))

    from MDsim.friction.frictiontensor import FrictionTensor
    eta = FrictionTensor(friction_matrix_mw=param['friction_matrix_mw'],
                         masses=masses,
                         convert=param['convert'])

    return eta
