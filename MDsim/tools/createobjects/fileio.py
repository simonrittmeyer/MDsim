# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to automize the creation of file and memory handlers
"""
from __future__ import print_function

from MDsim.tools import check_kwargs
from MDsim.observables.implemented import require_iatom
from MDsim.tools.createobjects.observables import create_observable
from MDsim.fileio.asciihandler import ASCIIHandler
from MDsim.fileio.memoryhandler import MemoryHandler

def create_filehandler(system_inst, integrator_inst, observable, **kwargs):
    param = {'iatom' : None,
             'fileformat' : 'ascii',
             'seed' : 'MDsim',
             'outfolder' : 'output',
             'args' : None,
             'compress' : True,
             'datatype' : 'f4',
             'verbose' : True,
             'min_width' : 20,
             'min_spacing' : 2,
             'convert' : True,
             'fmt' : '{}.9E',
             'delta_K' : None,
             'alternative_eta' : None,
             'normalmodes' : None,
             'Nsteps' : None
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # pop stuff from param such that we can use param to directly expand in the
    # create_observable() function
    seed = param.pop('seed')
    args = param.pop('args')
    compress = param.pop('compress')
    datatype = param.pop('datatype')
    outfolder = param.pop('outfolder')
    Nsteps = param.pop('Nsteps')
    fileformat = param.pop('fileformat').lower()

    if isinstance(observable, str):
        # sanity checks are done in the create_observable() function
        observable = observable.lower()

        # it is enough if the file handler talks to us
        param['verbose'] = False

        if fileformat not in ['ascii', 'netcdf', 'netcdf4', 'netcdf4_inmemory']:
            msg = 'Unknown file format "{}"'.format(fileformat)
            raise ValueError(msg)

        observable_inst = create_observable(system_inst, observable, **param)
    else:
        # assume we have an observable instance passed, if not, next line will
        # crash with AttributeError, which is just fine
        observable_inst = observable

    msg = 'Creating FileHandler instance for observable "{}"'.format(observable_inst.get_idstring())

    if observable in require_iatom:
       msg += ' (iatom = {})'.format(param['iatom'])

    if verbose:
        print(msg)

    # test if we have the netcdf bindings available
    if fileformat == 'netcdf4_inmemory':
        if Nsteps is None:
            warn = 'Caution: Asked for netcdf4_inmemory file format but no "Nsteps" given.'
            warn += '\n          Changing file format to regular netCDF4.'
            print(warn)
            fileformat = 'netcdf4'

    if fileformat.startswith('netcdf'):
        try:
            from MDsim.fileio.netcdfhandler import NetCDFHandler
        except ImportError:
            warn = 'Caution: Asked for netCDF4 file format but import failed.'
            warn += '\n          Changing file format to gzipped plain ASCII.'

            print(warn)

            fileformat = 'ascii'
            compress=True

    if fileformat == 'ascii':
        fileHandler = ASCIIHandler(system=system_inst,
                                   integrator=integrator_inst,
                                   observable=observable_inst,
                                   seed=seed,
                                   outfolder=outfolder,
                                   args=args,
                                   gzip_posthoc=compress)
    elif fileformat in ['netcdf', 'netcdf4']:
        fileHandler = NetCDFHandler(system=system_inst,
                                    integrator=integrator_inst,
                                    observable=observable_inst,
                                    seed=seed,
                                    outfolder=outfolder,
                                    compression=compress,
                                    datatype=datatype,
                                    args=args)

    elif fileformat == 'netcdf4_inmemory':
        from MDsim.fileio.netcdfhandler import InMemoryNetCDFHandler
        fileHandler = InMemoryNetCDFHandler(Nsteps=Nsteps,
                                            system=system_inst,
                                            integrator=integrator_inst,
                                            observable=observable_inst,
                                            seed=seed,
                                            outfolder=outfolder,
                                            compression=compress,
                                            datatype=datatype,
                                            args=args)
    return fileHandler


def create_memoryhandler(system_inst, integrator_inst, observable, Nsteps, **kwargs):
    param = {'iatom' : None,
             'args' : None,
             'datatype' : 'f4',
             'verbose' : True,
             'min_width' : 20,
             'min_spacing' : 2,
             'fmt' : '{}.9E',
             'delta_K' : None,
             'convert' : True,
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # pop stuff from param such that we can use param to directly expand in the
    # create_observable() function
    args = param.pop('args')
    datatype = param.pop('datatype')

    if isinstance(observable, str):
        # sanity checks are done in the create_observable() function
        observable = observable.lower()

        # it is enough if the file handler talks to us
        param['verbose'] = False

        observable_inst = create_observable(system_inst, observable, **param)
    else:
        observable_inst = observable

    msg = 'Creating MemoryHandler instance for observable "{}"'.format(observable_inst.get_idstring())

    if observable in require_iatom:
       msg += ' (iatom = {})'.format(param['iatom'])

    if verbose:
        print(msg)

    memoryHandler = MemoryHandler(Nsteps=Nsteps,
                                  system=system_inst,
                                  integrator=integrator_inst,
                                  observable=observable_inst,
                                  args=args,
                                  datatype=datatype)
    return memoryHandler
