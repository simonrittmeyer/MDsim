# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import os
import glob

import numpy as np

from MDsim.tools import check_kwargs

def create_univariatepotential(datafile, **kwargs):
    param = {'periodic' : False,
             'forces_method' : 'analytical',
             'forces_stepwidth' : 0.001,
             'bounds_check' : True,
             'usecols'  : (0,1),
             'normalize' : True,
             'convert' : True,
             'verbose' : True}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # read in the potential values
    data = np.loadtxt(datafile, usecols = param['usecols'], unpack = True)

    periodic = param['periodic']

    data_origin = datafile
    data_origin +='\n\t(using columns {} and {})'.format(*param['usecols'])

    if periodic:
        if verbose:
            print('Creating PeriodicUnivariatePotential instance')

        from MDsim.potentials.atomic.periodic.periodicunivariatepotential import PeriodicUnivariatePotential
        potential =  PeriodicUnivariatePotential(points=data[0],
                                                 energies=data[1],
                                                 data_origin=data_origin,
                                                 forces_method=param['forces_method'],
                                                 forces_stepwidth=param['forces_stepwidth'],
                                                 convert=param['convert'],
                                                 function='InterpolatedUnivariateSpline',
                                                 kind='cubic',
                                                 normalize=param['normalize'],
                                                 smoothing=None,
                                                 )
    else:
        if verbose:
            print('Creating AtomicUnivariatePotential instance')
        from MDsim.potentials.atomic.univariatepotential import UnivariatePotential
        potential =  UnivariatePotential(points=data[0],
                                         energies=data[1],
                                         data_origin=data_origin,
                                         forces_method=param['forces_method'],
                                         forces_stepwidth=param['forces_stepwidth'],
                                         convert=param['convert'],
                                         function='InterpolatedUnivariateSpline',
                                         kind='cubic',
                                         normalize=param['normalize'],
                                         _bounds_check=param['bounds_check'])
    if verbose:
        print('\tusing file {}'.format(datafile))

    return potential


def create_bivariatepotential(datafile, **kwargs):

    param = {'periodic' : False,
             'coordinates' : 'atomic',
             'forces_method' : 'analytical',
             'forces_stepwidth' : 0.001,
             'bounds_check' : True,
             'usecols'  : (0,1,2),
             'masses' : None,
             'positions_eq' : None,
             'normalize' : True,
             'convert' : True,
             'verbose' : True}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    coords = param['coordinates'].lower()
    # sanity check
    if not coords in ['atomic', 'diatomic-perp', 'generic-dof']:
        raise ValueError('Unknown coords "{}"'.format(coords))

    # read in the potential values
    data = np.loadtxt(datafile, usecols = param['usecols'], unpack = True)

    periodic = param['periodic']

    data_origin = datafile
    data_origin +='\n\t(using columns {}, {} and {})'.format(*param['usecols'])

    if coords == 'atomic':
        if periodic:
            if verbose:
                print('Creating PeriodicAtomicBivariatePotential instance')

            from MDsim.potentials.atomic.periodic.periodicbivariatepotential import PeriodicAtomicBivariatePotential
            potential =  PeriodicAtomicBivariatePotential(points = data[0:2].T,
                                                          energies = data[2],
                                                          data_origin = data_origin,
                                                          forces_method = param['forces_method'],
                                                          forces_stepwidth = param['forces_stepwidth'],
                                                          convert = param['convert'],
                                                          function = 'RectBivariateSpline',
                                                          kind = 'cubic',
                                                          normalize = param['normalize'],
                                                          )
        else:
            if verbose:
                print('Creating AtomicBivariatePotential instance')
            from MDsim.potentials.atomic.bivariatepotential import AtomicBivariatePotential
            potential =  AtomicBivariatePotential(points = data[0:2].T,
                                                  energies = data[2],
                                                  data_origin = data_origin,
                                                  forces_method = param['forces_method'],
                                                  forces_stepwidth = param['forces_stepwidth'],
                                                  convert = param['convert'],
                                                  function = 'RectBivariateSpline',
                                                  kind = 'cubic',
                                                  normalize = param['normalize'],
                                                  _bounds_check = param['bounds_check'])
    elif coords == 'diatomic-perp':
        if periodic:
            msg  = 'Error : Asked for Periodic/DiatomicPerp Coordinates BivariatePotential combination.\n'
            msg += '        This is not (yet) implemented.'
            if verbose:
                print(msg)
            raise NotImplementedError(msg)

        else:
            if param['masses'] is None:
                msg = 'DiatomicPerpPotential instance requires masses to do the coordinate transformations'
                raise ValueError(msg)
            if verbose:
                print('Creating DiatomicPerpPotential instance')
            from MDsim.potentials.internal.vibrations.diatomic.perp import DiatomicPerpPotential
            potential =  DiatomicPerpPotential(points=data[0:2].T,
                                               energies=data[2],
                                               masses=param['masses'],
                                               data_origin=data_origin,
                                               forces_method=param['forces_method'],
                                               forces_stepwidth=param['forces_stepwidth'],
                                               convert=param['convert'],
                                               function='RectBivariateSpline',
                                               positions_eq=param['positions_eq'],
                                               kind='cubic',
                                               normalize=param['normalize'],
                                                _bounds_check=param['bounds_check'])

    elif coords =='generic-dof':
        if periodic:
            msg  = 'Error : Asked for Periodic/GenericDOFBivariatePotential combination.\n'
            msg += '        This is not (yet) implemented.'
            if verbose:
                print(msg)
            raise NotImplementedError(msg)

        else:
            if verbose:
                print('Creating GenericDOFBivariatePotential instance')
            from MDsim.potentials.internal.generic import GenericDOFBivariatePotential
            potential =  GenericDOFBivariatePotential(points=data[0:2].T,
                                                      energies=data[2],
                                                      data_origin=data_origin,
                                                      forces_method=param['forces_method'],
                                                      forces_stepwidth=param['forces_stepwidth'],
                                                      convert=param['convert'],
                                                      function='RectBivariateSpline',
                                                      kind='cubic',
                                                      normalize=param['normalize'],
                                                       _bounds_check=param['bounds_check'])

    else:
        raise NotImplementedError('Coordinates "{}" not implemented'.format(coords))

    if verbose:
        print('\tusing file {}'.format(datafile))

    return potential


def _create_reducedbasisfourierpotential(a, basis, **kwargs):
    # generic routine for all reducedbasis potentials
    param = {'energies_dict' : None,
             'datafile' : None,
             'forces_method' : 'analytical',
             'forces_stepwidth' : 0.001,
             'surface' : 'fcc111',
             'verbose' : True,
             'convert' : True,
             'normalize' : True,
             'constant' : True
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    basis = basis.lower()
    if not basis in ['minimal', 'top-hollow']:
        msg = 'Unknown reduced basis "{}"'.format(basis)
        raise NotImplementedError(msg)

    # read from file if not passed as dictionary
    if param['energies_dict'] is None:
        if param['datafile'] == None:
            raise ValueError('Pass either "energies_dict" or "datafile"')
        else:
            datafile = param['datafile']

        data = np.loadtxt(datafile, dtype = str)

        # create the energies dict
        energies_dict = {}
        for d in data:
            energies_dict[d[0].lower()] = float(d[1])
    else:
        energies_dict = param['energies_dict']

    if param['surface'].lower() == 'fcc111':

        if basis == 'minimal':
            from MDsim.potentials.atomic.periodic.diffusion.fcc111 import MinimumBasisFourierPotentialFCC111 as Potential
            if verbose:
                print('Creating MinimumBasisFourierPotentialFCC111 instance')

            potential = Potential(a = a,
                                  energies_dict = energies_dict,
                                  forces_method = param['forces_method'],
                                  forces_stepwidth = param['forces_stepwidth'],
                                  normalize = param['normalize'],
                                  convert = param['convert'])

        elif basis == 'top-hollow':
            from MDsim.potentials.atomic.periodic.diffusion.fcc111 import TopHollowFourierPotentialFCC111 as Potential
            if verbose:
                print('Creating TopHollowFourierPotentialFCC111 instance')

            potential = Potential(a = a,
                                  energies_dict = energies_dict,
                                  forces_method = param['forces_method'],
                                  forces_stepwidth = param['forces_stepwidth'],
                                  normalize = param['normalize'],
                                  constant = param['constant'],
                                  convert = param['convert'])

    elif param['surface'].lower() == 'fcc100':
        if basis == 'minimal':
            from MDsim.potentials.atomic.periodic.diffusion.fcc100 import MinimumBasisFourierPotentialFCC100 as Potential
            if verbose:
                print('Creating MinimumBasisFourierPotentialFCC100 instance')
            potential = Potential(a = a,
                                  energies_dict = energies_dict,
                                  forces_method = param['forces_method'],
                                  forces_stepwidth = param['forces_stepwidth'],
                                  normalize = param['normalize'],
                                  convert = param['convert'])

        else:
            msg = 'Reduced basis "{}" not implemented for fcc100 surfaces'.format(basis)
            raise NotImplementedError(msg)
    else:
        msg = 'Surface facet "{}" not implemented'.format(param['surface'])
        raise NotImplementedError(msg)

    if verbose and param['energies_dict'] is None:
        print('\tusing file {}'.format(datafile))

    return potential

def create_minimumbasisfourierpotential(a, **kwargs):
    return _create_reducedbasisfourierpotential(a, basis='minimal', **kwargs)

def create_tophollowfourierpotential(a, **kwargs):
    return _create_reducedbasisfourierpotential(a, basis='top-hollow', **kwargs)


def create_fourierpotential(a, real_order, imag_order, datafile, **kwargs):
    param = {'usecols' : (0, 1, 2),
             'constant' : True,
             'forces_method' : 'analytical',
             'forces_stepwidth' : 0.001,
             'surface' : 'fcc111',
             'verbose' : True,
             'convert' : True,
             'normalize' : True
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # read in the potential values
    data = np.loadtxt(datafile, usecols = param['usecols'], unpack = True)

    if param['surface'].lower() == 'fcc111':
        from MDsim.potentials.atomic.periodic.diffusion.fcc111 import FourierPotentialFCC111

        if verbose:
            print('Creating FourierPotentialFCC111 instance')
            print('\treal_order = {}; imag_order = {}; constant = <{}>'.format(real_order,
                                                                               imag_order,
                                                                               param['constant']))
        potential = FourierPotentialFCC111(a=a,
                                           points=data[0:2,:].T,
                                           energies=data[2],
                                           real_order=real_order,
                                           imag_order=imag_order,
                                           constant=param['constant'],
                                           forces_method = param['forces_method'],
                                           forces_stepwidth = param['forces_stepwidth'],
                                           normalize = param['normalize'],
                                           convert = param['convert'])

    elif param['surface'].lower() == 'fcc100':
        from MDsim.potentials.atomic.periodic.diffusion.fcc100 import FourierPotentialFCC100

        potential = FourierPotentialFCC100(a=a,
                                           points=data[0:2,:].T,
                                           energies=data[2],
                                           real_order=real_order,
                                           imag_order=imag_order,
                                           constant=param['constant'],
                                           forces_method = param['forces_method'],
                                           forces_stepwidth = param['forces_stepwidth'],
                                           normalize = param['normalize'],
                                           convert = param['convert'])
    else:
        msg = 'Surface facet "{}" not implemented'.format(param['surface'])
        raise NotImplementedError(msg)

    if verbose:
        print('\tusing file {}'.format(datafile))

    return potential

def create_cambridgepotential(a, **kwargs):
    param = {'energies_dict' : None,
             'datafile' : None,
             'forces_method' : 'analytical',
             'forces_stepwidth' : 0.001,
             'surface' : 'fcc100',
             'verbose' : True,
             'convert' : True,
             }

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    verbose = param['verbose']

    # read from file if not passed as dictionary
    if param['energies_dict'] is None:
        if param['datafile'] == None:
            raise ValueError('Pass either "energies_dict" or "datafile"')
        else:
            datafile = param['datafile']

        data = np.loadtxt(datafile, dtype = str)

        # create the energies dict
        energies_dict = {}
        for d in data:
            energies_dict[d[0].lower()] = float(d[1])
    else:
        energies_dict = param['energies_dict']

    if param['surface'].lower() == 'fcc100':
        from MDsim.potentials.atomic.periodic.diffusion.fcc100 import CambridgePotentialFCC100 as Potential

        if verbose:
            print('Creating CambridgePotentialFCC100 instance')

        potential = Potential(a = a,
                              forces_method = param['forces_method'],
                              forces_stepwidth = param['forces_stepwidth'],
                              convert = param['convert'],
                              energies_dict=energies_dict)
    else:
        msg = 'Surface facet "{}" not implemented'.format(param['surface'])
        raise NotImplementedError(msg)

    if verbose:
        print('\tusing file {}'.format(datafile))

    return potential
