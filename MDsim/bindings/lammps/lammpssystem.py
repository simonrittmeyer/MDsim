# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import os
from MDsim.bindings.ase.asetomdsim import ASESystem


class LammpsSystem(ASESystem):
    # This far, this is only a wrapper function...
    def __init__(self, atoms,
                       friction=None,
                       temperature=None,
                       calc_folder='output',
                       seed='MDsim__LAMMPS',
                       _init_etas=True,
                       _init_forces=True,
                       _verbose=True):
        # check if we have the correct calculator attached:
        if atoms.calc.__class__.__name__ not in ['LAMMPS', 'ExtendedLAMMPS']:
            msg = 'This class requires an attached "LAMMPS" or "ExtendedLAMMPS" calculator instance '
            msg += '(provided : "{}").'.format(atoms.calc.__class__.__name__)
            raise ValueError(msg)

        self._calc_folder = calc_folder
        self._seed = seed

        atoms.calc.tmpdir = os.path.abspath(calc_folder)
        atoms.calc.label = seed

        ASESystem.__init__(self, atoms=atoms,
                                 friction=friction,
                                 temperature=temperature,
                                 _init_etas=_init_etas,
                                 _init_forces=_init_forces,
                                 _verbose=_verbose)


