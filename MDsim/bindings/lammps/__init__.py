# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim import tools
from MDsim.bindings.lammps.lammpssystem import LammpsSystem
from MDsim.bindings.lammps.extendedlammpsrun import ExtendedLAMMPS



def installed():
    """
    This is a tweaked version from ase.tests.castep.__init__.py
    """
    import os
    import subprocess
    from ase.test import NotAvailable
    commands = [('LAMMPS_COMMAND', 'lammps', 'LAMMPS')]

    for (var, default, name) in commands:
        if var not in os.environ:
            print("WARNING: Environment variable '{}' is not set".format(var))
            print("Will set {} = {} for the sake of this test".format(var, default))
            print("Please change it if this does not run lammps in your environment")
            os.environ[var] = default

        if not tools.which(os.environ[var]):
            msg =  "Could not find {}. If you have it installed make sure,".format(name)
            msg += "you set the {} environment variable correctly""".format(var)
            raise NotAvailable(msg)

    return True
