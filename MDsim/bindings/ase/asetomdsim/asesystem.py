# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import copy

from MDsim import units
from MDsim import tools

from MDsim.systems.system import System

# taylored towards usage with castep as QM code!
# this is to be used in cases where we want to use an ase calculator to obtain
# energies!

from ase.constraints import FixAtoms
from ase.constraints import FixCartesian

class ASESystem(System):
    """
    System that synchronizes an atoms object, rather slow but highly useful for
    AIMD and friends.

    Atoms object MUST have calculator attached already at this stage!

    Initialization
    --------------
    atoms : ASE atoms object
        The atoms object that defines the system. Positions, masses, species
        and velocities will be deduced from it, so prepare it well. Please note
        that ASE does *not* use atomic units internally. Also, the atoms object
        must have a proper calculator attached that supports atoms.get_forces()
        and atoms.get_potential_energy(). There is not explicit check for it,
        yet at the latest the first propagation step will fail.

        Note that since v.3.4.4 also constraints will be deduced from the atoms
        object (FixAtom and FixCartesian only!)

        .. versionadded: 3.4.4

    friction : MDsim FrictionMatrix instance, optional (default=None)
        The friction matrix. If a float or integer is passed, it will be
        interpreted as constant (diagonal) friction. In this case, units are
        atomic mass units per femto second if convert=<True>, and a.u. else.

        Note that the term friction is a bit ambigious in the sense of
        Langevin-MD. We call friction "eta" in this context:

            m * d^2/dt^2 pos = -d/dpos V -eta * d/dt pos + R(t)

        Note that quite often one calls gamma = eta / m a friction coefficient.
        Thus be careful. For more details see the "MDsim.friction" submodule.

    temperature : float, optional (default=0.)
        The temperature of the system.

        Units are Kelvin if convert=<True>, a.u. else. Note that the
        temperature in a.u. is equivalent to thermal energy (=kT) in a.u.!

    _init_etas : bool, optional (default=False)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the friction matrix will be made in
        the constructor of this object. This may be useful if the friction
        matrix is only created afterwards.

    _init_forces : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the potential will be made in the
        constructor of this object. This may be useful if the potential is only
        created afterwards.

    _random_seed : int, optional (default=True)
        The seed for the RNG used to draw from a Maxwell-Boltzman distribution.
        Set this one in order to obtain reproducible results. If <None>, the
        seed will be randomized.

    _verbose : bool, optional (default=True)
        Print some (more) status information to stdout.
    """

    _supported_constraints = [FixAtoms, FixCartesian]

    def __init__(self,
                 atoms,
                 friction=None,
                 temperature=0.,
                 _init_etas=False,
                 _init_forces=True,
                 _verbose=True,
                 _random_seed=None):

        # catch constraints from the atoms object
        fix_coordinates_mask = np.zeros_like(atoms.get_positions(), dtype=bool)

        for constraint in atoms.constraints:
            if not (isinstance(constraint, tuple(self._supported_constraints))):
                raise RuntimeError('Atoms object contains unsupported constraints')
            elif isinstance(constraint, FixAtoms):
                fix_coordinates_mask[constraint.index,:] = True
            elif isinstance(constraint, FixCartesian):
                # here, a <True> in constrain.mask means "free"
                fix_coordinates_mask[constraint.a, np.logical_not(constraint.mask)] = True

        # create positions etc from the atoms object (do convert in any case!)
        kwargs = {'positions' : atoms.get_positions(),
                  'masses' : atoms.get_masses(),
                  # the ase momenta are weired unit-wise; time is not in fs here
                  'velocities' : atoms.get_momenta() / atoms.get_masses()[:,np.newaxis] * units.ASE_TO_ANGSTROM_PER_FS,
                  'species' : atoms.get_chemical_symbols(),
                  'potential' : None,
                  'friction' : friction,
                  'temperature' : temperature,
                  'fix_coordinates' : fix_coordinates_mask,
                  'convert' : True,
                  '_init_forces' : False,
                  '_init_etas' : _init_etas,
                  '_random_seed': _random_seed,
                  '_verbose' : _verbose}

        System.__init__(self, **kwargs)

        # we store a true copy here
        try:
            self.atoms = copy.deepcopy(atoms)
        except TypeError:
            # here the Lammps interface crashes...
            self.atoms = atoms

        # update the time once manually (afterwards via set_time())
        self.atoms.info.update({'time' : self.get_time()})

        # delta that determines if positions have changed
        self._eps = 1e-10

        # here we go, initializing the forces manually
        if _init_forces:
            self._init_forces()

        self._name = 'Generic ASE System'

    # overload this system routines to evaluate it directly from the atoms
    # object
    def calc_potential_energy(self, positions):
        _pos_change = not np.all(np.abs(positions - self.positions) < self._eps)
        if _pos_change:
            self.atoms.set_positions(positions * units.AU_TO_ANGSTROM)
        Epot = self.atoms.get_potential_energy() * units.EV_TO_AU
        # change positions back to avoid that atoms object goes out of sync
        if _pos_change:
            self.atoms.set_positions(self.get_positions())
        return Epot


    def calc_forces(self, positions):
        _pos_change = not np.all(np.abs(positions - self.positions) < self._eps)
        if _pos_change:
            self.atoms.set_positions(positions * units.AU_TO_ANGSTROM)
        # ok, screw all those nice ideas about not copying arrays...
        # it is not about speed on this level in AIMD simulations
        forces =  self.atoms.get_forces() * units.EV_PER_ANGSTROM_TO_AU
        if _pos_change:
            self.atoms.set_positions(self.get_positions())
        return forces


    # automatically keep the atoms object updated
    # without touching too much from the underlying routines
    def set_positions(self, positions, **kwargs):
        super(ASESystem, self).set_positions(positions, **kwargs)
        # get routine converts automatically
        self.atoms.set_positions(self.get_positions())


    def set_velocities(self, velocities, **kwargs):
        super(ASESystem, self).set_velocities(velocities, **kwargs)
        # get routine converts automatically
        # note that in ASE we do not have FS=1
        self.atoms.set_momenta(self.get_momenta() * units.ANGSTROM_AMU_PER_FS_TO_ASE)


    def set_time(self, time, **kwargs):
        super(ASESystem, self).set_time(time, **kwargs)
        # additionally also update the atoms information (doing this, we can
        # read the time directly from the atoms objects of a trajectory)
        # Yet, in the spirit of ASE, we use fs as units and not au.
        self.atoms.info.update({'time' : self.get_time()})
