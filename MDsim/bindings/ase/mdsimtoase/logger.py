# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import ase.units as units
from ase.md.logger import MDLogger

class MDsimLogger(MDLogger):
    def __init__(self, *args, **kwargs):
        # do not write header in the init of the parent
        header_bak = kwargs.pop('header', False)

        kwargs['header']= False
        MDLogger.__init__(self, *args, **kwargs)

        # we want the time column with a bit more precision
        if self.dyn is not None:
            self.fmt = "%-12.6f " + self.fmt[7::]
            self.hdr = "%-12s " %("# Time [ps]")+ self.hdr[12::]
        if header_bak or kwargs.pop('mode', 'a') == 'w':
            self.logfile.write(self.hdr+"\n")

    def __call__(self):
        epot = self.atoms.get_potential_energy()
        ekin = self.atoms.get_kinetic_energy()
        temp = ekin / (1.5 * units.kB * self.natoms)
        if self.peratom:
            epot /= self.natoms
            ekin /= self.natoms
        if self.dyn is not None:
            # we have to modify it a bit here, as our system returns times
            # in fs already.
            t = self.dyn.get_time()/1000.
            dat = (t,)
        else:
            dat = ()
        dat += (epot+ekin, epot, ekin, temp)
        if self.stress:
            dat += tuple(self.atoms.get_stress() / units.GPa)
        self.logfile.write(self.fmt % dat)
        self.logfile.flush()

