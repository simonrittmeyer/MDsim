# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Very basic interface that should allow to create ase trajectories
"""
import numpy as np
from ase.calculators.calculator import Calculator

def _system_to_calc(system):
    """
    Function that creates a results dictionary based on the current system
    state. You may want to implement your own function, if you want to add
    ghost atoms or something similar. Anyhow, the behavior, ie. input and
    output must be the same. In particular, the results dictionary must at
    least contain the variables 'forces' and 'energy', where 'energy' is the
    potential energy! ASE logic...

    Parameters
    ----------
    system : MDsim system instance
        System to be interfaced.

    Returns
    -------
    results : dictionary
        Dictionary which will be used to update the internal calculator
        values. Note that this defines the properties that the calculator
        can provide.
    """
    forces = system.get_forces()
    energy = system.get_potential_energy()
    etas = system.get_etas()

    # well, ase requires 3D arrays...so, let's add zeros
    if system.Ndim != 3:
        forces = np.c_[forces,np.zeros((system.Natoms, 3-system.Ndim))]
        etas = np.c_[etas,np.zeros((system.Natoms, 3-system.Ndim))]

    results = {'energy' : energy,
               'forces' : forces,
               'etas' : etas,
              }

    return results


class MDsimCalculator(Calculator):
    """
    A fake calculator that pretends to be an actual ase calculator but just
    returns whatever the system tells us

    Initialization
    --------------
    system : MDsim system instance
        The system we want to interface to ase.

    system_to_calc : function, optional (default = None)
        Function that creates a dictionary which is ultimately used to update
        the calculator dictionary. If you want to change anything, please stick
        to the behavior of the static method "_system_to_calc()". Anyhow, the
        resulting dictionary must at least contain "energy" and "forces" as
        keys. A ValueError is raised if not.
    """

    implemented_properties = ['energy',
                              'forces',
                              'etas']

    name = 'MDsim/ASE interface'

    def __init__(self, system, system_to_calc=None, _init_forces = True):
        Calculator.__init__(self)

        self._system = system

        if system_to_calc is None:
            self.system_to_calc = _system_to_calc
        else:
            self.system_to_calc = system_to_calc

        self.results = {'stress' : np.zeros((self._system.Ndim, self._system.Ndim))}

        if _init_forces:
            self._init_forces()

    def _init_forces(self):
        self.calculate()

        # nifty way to extend the "implemented" properties
        MDsimCalculator.implemented_properties = self.results.keys()

        # check for the necessary stuff
        for prop in ['energy', 'forces']:
            if not prop in MDsimCalculator.implemented_properties:
                msg = 'Your system_to_calc() function does not provide a result for "{}".'.format(prop)
                raise ValueError(msg)



    def calculate(self, *args, **kwargs):
        """
        A fake new calculation. Well, it is not actually fake, but the system
        routines to it for us anyway. They also check whether a calculation is
        required or not.

        Using "update()" on self.results we can store stress etc once and for
        all.

        Arguments are just dummies with no effect.
        """

        self.results.update(self.system_to_calc(self._system))

