# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Very basic interface. The class carries an atoms object to which we attach a
calculator. Calling update_atoms() updates the atoms object.
"""
import numpy as np
import ase

from MDsim.bindings.ase.mdsimtoase.calculator import MDsimCalculator


def _system_to_atoms(system, atoms=None):
    """
    Routine that creates or updates an atoms object which matches the
    system properties. If system is less than three dimensional, remaining
    dimensions are filled with zeros.

    Parameters
    ----------
    system : MDsim system instance
        The system that is to be interfaced.

    atoms : ASE Atoms instance, optional (default = None)
        If passed, the Atoms instance that shall be updated with the
        current system state.

    Returns
    -------
    atoms : ASE Atoms instance
        Atoms instance that represents the current system state.
    """
    positions = system.get_positions()
    momenta = system.get_momenta()

    # well, ase requires 3D arrays...so, let's add zeros
    if system.Ndim != 3:
        positions = np.c_[positions,np.zeros((system.Natoms, 3-system.Ndim))]
        momenta = np.c_[momenta,np.zeros((system.Natoms, 3-system.Ndim))]

    if atoms is None:
        try:
            unit_cell = system.get_unit_cell()
        except AttributeError:
            unit_cell = None


        atoms = ase.Atoms(symbols = system.get_species(),
                          positions = positions,
                          masses = system.get_masses(),
                          momenta = momenta,
                          cell = unit_cell)
    else:
        atoms.set_positions(positions)
        atoms.set_momenta(momenta)

    return atoms


class MDsimToASE(object):
    """
    Class that converts an MDsim system to something that actually ASE can deal
    with. It carries an atoms and calculator instance.

    Calling "update_atoms()" updates the atoms object with the new positions
    and momenta. The forces and energy upate is then called via the calculator.
    This is ase logic.

    Initialization
    --------------
    system : MDsim system instance
        System we want to interface to ASE.

    system_to_atoms : function, optional (default = None)
        Function that converts the system to an ase atoms object. If None is
        passed, all properties are taken directly from the system (and extended
        to 3D if necessary). If you plan to implement a very own function, then please
        write this routine in conformity with the staticmethod
        "_system_to_ase()" from MDsim.bindings.ase.convert. This means
        two arguments are required. If an atoms object is passed, then it shall
        be updated only. Note however, that you also need to tweak the
        calculator in this case.

    system_to_calc : function, optional (default = None)
        Function that creates a dictionary which is ultimately used to update
        the calculator dictionary. If you want to change anything, please stick
        to the behavior of the static method "_system_to_calc()" from
        MDsim.bindings.ase.calculator. Anyhow, the resulting dictionary
        must at least contain "energy" and "forces" as keys. A ValueError is
        raised if not.
    """
    def __init__(self, system, system_to_atoms=None, system_to_calc=None):
        self.system = system

        if system_to_atoms is None:
            self.system_to_atoms = _system_to_atoms
        else:
            self.system_to_atoms = system_to_atoms

        # just some sanity checks
        if system_to_atoms is None:
            if system_to_calc is not None:
                print('Info: You specified "system_to_calc" but not "system_to_atoms".')
                print('      Sure about that?')
        else:
            if system_to_calc is None:
                print('Info : You specified "system_to_atoms" but not "system_to_calc".')
                print('       Sure about that?')

        # initializie atoms
        self.atoms = self.system_to_atoms(self.system, atoms=None)
        self.calc = MDsimCalculator(self.system, system_to_calc=system_to_calc)
        self.atoms.set_calculator(self.calc)

    def update_atoms(self):
        """
        Update the atoms object with the current system state.
        """
        self.atoms = self.system_to_atoms(self.system, atoms=self.atoms)
