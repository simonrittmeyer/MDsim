# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import os
import ase

from MDsim.bindings.ase.mdsimtoase.convert import MDsimToASE
from MDsim.bindings.ase.mdsimtoase.logger import MDsimLogger

class TrajectoryHandler(object):
    def __init__(self,
                 system,
                 system_to_atoms=None,
                 system_to_calc=None,
                 seed='MDsim',
                 outfolder='.',
                 write_logfile=True,
                 properties=['energy', 'positions', 'momenta', 'forces'],
                 _traj_class='Trajectory'
                 ):

        self.system = system

        # this will be changed to 'a' if file exists
        self.mode = 'w'

        # if we have an ase system we use directly this object
        if hasattr(system, 'atoms'):
            self._system_has_atoms = True
        else:
            self.converter = MDsimToASE(system, system_to_atoms, system_to_calc)
            self._system_has_atoms = False

        self.properties = properties
        self.seed = seed
        self.outfolder = os.path.abspath(outfolder)
        self._traj_class = _traj_class.lower()

        self._write_logfile = write_logfile

        self.open()


    def open(self):
        if self._system_has_atoms:
            atoms = self.system.atoms
        else:
            atoms = self.converter.atoms

        # create the folder if necessary
        if not os.path.exists(self.outfolder):
            os.makedirs(self.outfolder)

        if self._traj_class == 'trajectory':
            from ase.io import Trajectory
            self.trajfile = os.path.join(self.outfolder, self.seed + '__ase.traj')
            if os.path.isfile(self.trajfile):
                self.mode='a'
            self.traj_inst = Trajectory(filename=self.trajfile,
                                        atoms=atoms,
                                        mode=self.mode,
                                        properties=self.properties)
        elif self._traj_class =='bundletrajectory':
            from ase.io import BundleTrajectory
            self.trajfile = os.path.join(self.outfolder, self.seed + '__ase.bundle')
            if os.path.isfile(self.trajfile):
                self.mode='a'
            self.traj_inst = BundleTrajectory(filename=self.trajfile,
                                              mode=self.mode,
                                              atoms=atoms)
        else:
            msg = 'ASE trajectory class "{}" not implemented'.format(self._traj_class)
            raise NotImplementedError(msg)

        if self._write_logfile:
            self.logfile = os.path.join(self.outfolder, self.seed + '__ase.mdlog')
            self.mdlog_inst = MDsimLogger(dyn=self.system,
                                          atoms=atoms,
                                          logfile=self.logfile,
                                          stress=False,
                                          peratom=False,
                                          mode=self.mode)

        # DEPRECATED!
        # if we are in append mode, we interprete this a continuation!
        # we thus only do the initial push for new trajectories
        # if self.mode == 'w':
            # self.push()

        self._closed=False

    def push(self):
        if self._system_has_atoms:
            atoms = self.system.atoms
        else:
            self.converter.update_atoms()
            atoms = self.converter.atoms
        self.traj_inst.write(atoms)

        if self._write_logfile:
            self.mdlog_inst()

    def close(self):
        self.traj_inst.close()
        if self._write_logfile:
            self.mdlog_inst.close()
        self._closed = True


