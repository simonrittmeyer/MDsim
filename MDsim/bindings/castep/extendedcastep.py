# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil
import os
import time
import re
import copy

from ase.calculators.castep import Castep
from ase.calculators.castep import shell_stdouterr
from ase.io.castep import write_param
from ase.io.castep import read_seed

from MDsim import tools
from MDsim.tools.cube.interpolatedcube import InterpolatedCube

from MDsim.prettyprint import format_timing

class ExtendedCastep(Castep):
    # extend the class variables from Castep
    internal_keys  = Castep.internal_keys  + ['_castep2cube_command',
                                              '_castep_hirshfeld_command',
                                              '_castep_hirshfeld_version',
                                              '_clean_leftovers',
                                              '_check_suffix',
                                              '_scf_cubefile',
                                              #'_scf_param',
                                              '_hirshfeld_w_cubefiles',
                                              '_embedding_densities_cubefiles',
                                              '_castep2cube_calculation_required',
                                              '_castep_hirshfeld_calculation_required',
                                              '_interpolated_scf_density',
                                              '_interpolated_hirshfeld_w',
                                              '_interpolated_embedding_densities',
                                              '_continuation_calls',
                                              '_verbose']


    def __init__(self, castep2cube_command = None,
                       castep_hirshfeld_command = None,
                       castep_hirshfeld_version = 2,
                       clean_leftovers = True,
                       verbose = True,
                       **kwargs):

        # initialize the parent class
        Castep.__init__(self, **kwargs)

        self._verbose = verbose

        self._check_suffix = '.check'
        self._continuation_calls = 0

        # get the required commands
        self._castep2cube_command = get_castep2cube_command(castep2cube_command)
        self._castep_hirshfeld_command = get_castep_hirshfeld_command(castep_hirshfeld_command)

        # both versions need to be handled differently
        self._castep_hirshfeld_version = int(castep_hirshfeld_version)
        if not self._castep_hirshfeld_version in [1,2]:
            raise NotImplementedError('There is only support for "castep_hirshfeld_version" = 1 or 2')

        # assing clean leftover flag
        self._clean_leftovers = clean_leftovers

        # setting all continuation-related flags and storages
        self._reset_continuation_status()

    # =====================================================
    # Highlevel get_X() routines, called with atoms object!
    # =====================================================

    def get_scf_density(self, atoms):
        """
        High-level routine to obtain the SCF electronic density in e/A**3
        """
        self.update_scf_density(atoms)
        return self._interpolated_scf_density

    def get_aim_densities(self, atoms):
        """
        High-level routine to obtain the SCF electronic density in e/A**3
        """

        self.update_aim_densities(atoms)
        return self._aim_densities


    # ==============================
    # the high-level update routines
    # ==============================

    def update_scf_density(self, atoms):
        """
        Ensures that self._interpolated_scf_density corresponds to the state
        defined by atoms. Uses many routines that only make sense when called
        in specific order, so please, do not use them without exactly knowing
        what you are doing.
        """

        # do we need a new SCF calculation? If so, all flags are reset anyway...
        self.update(atoms)

        # if a new SCF calculation was run, this is None. If there was never a
        # cube calculation, it is None as well... so everything is cool
        if not self._scf_cubefile:
            if not self._checkfile_exists(atoms):
                self.calculate(atoms)
            # now we definitely have a matching check file
            self._calculate_scf_density(atoms)

        # now we do have a matching cube file and self._scf_cubefile is the
        # correct cube file. If there was a new cube calculation, this is None,
        # so everything is cool.
        if not self._interpolated_scf_density:
            self._interpolate_scf_density(atoms)

    def update_aim_densities(self, atoms):
        """
        Ensures that self._interpolated_scf_density and
        self._interpolated_hirshfeld_weights corresponds to the state defined
        by atoms. Uses many routines that only make sense when called in
        specific order, so please, do not use them without exactly knowing what
        you are doing.
        """
        self.update(atoms)

        # here comes the switch due to the new castep_hirshfeld f90 routine
        if self._castep_hirshfeld_version == 1:
            if not self._hirshfeld_weights_cubefiles:
                if not self._checkfile_exists(atoms):
                    self.calculate(atoms)
                # calculation will also give us new SCF cube file.
                self._calculate_hirshfeld_weights(atoms)

            if not self._interpolated_hirshfeld_weights:
                self._interpolate_hirshfeld_weights(atoms)

            # this one will as well be updated if we ran a new hirshfeld calculation
            if not self._interpolated_scf_density:
                self._interpolate_scf_density(atoms)

            # calling the interpolation is for free... let's get it here
            self._aim_densities = np.empty(len(self.atoms), dtype=np.float64)
            for i, atom in enumerate(self.atoms):
                self._aim_densities[i] = ((1.-self._interpolated_hirshfeld_weights[i](atom.position))
                                           *self._interpolated_scf_density(atom.position))

        elif self._castep_hirshfeld_version == 2:
            if not self._embedding_densities_cubefiles:
                if not self._checkfile_exists(atoms):
                    self.calculate(atoms)
                self._calculate_embedding_densities(atoms)

            if not self._interpolated_embedding_densities:
                self._interpolate_embedding_densities(atoms)

            # this one will as well be updated if we ran a new hirshfeld
            # calculation, why not using it for the sake of consistency
            if not self._interpolated_scf_density:
                self._interpolate_scf_density(atoms)

            # calling the interpolation is for free... let's get it here
            self._aim_densities = np.empty(len(self.atoms), dtype=np.float64)
            for i, atom in enumerate(self.atoms):
                self._aim_densities[i] = self._interpolated_embedding_densities[i](atom.position)

    # =====================================================================
    # the mid-level update routine helper -- DO NOT USE OUTSIDE HIGH-LEVEL
    # UPDATE ROUTINES UNLESS YOU KNOW VERY EXACTLY WHAT YOU ARE DOING!
    # =====================================================================

    def _calculate_scf_density(self, atoms):
        """
        Calculate the SCF density using castep2cube. This routines requires
        that self.calculation_required(atoms) and self._checkfile_exists(atoms)
        (in this order) evaluate to True. Otherwise a ValueError is raised.

        We try to catch all possible mal-usages of this routine. Yet, only use
        it from the outside if you are really sure that you know what you are
        doing.
        """
        if self.calculation_required(atoms):
            msg  = 'Input "atoms" does not correspond to self.atoms. '
            msg += 'Run an SCF calculation first!'
            raise ValueError(msg)

        if not self._checkfile_exists(atoms):
            msg = 'There is no checkfile corresponding to the state of <atoms>. '
            msg += 'Run an SCF calculation first!'
            raise ValueError(msg)

        self._prepare_continuation_input()

        # this one sets self._scf_cubefile != None
        self._run_castep2cube()

        self._restore_original_files()

        # now that we have a new cube file, in any case we need a new
        # interpolation as well
        self._interpolated_scf_density = None
        self._aim_densities = None

        if self._clean_leftovers:
            self._clean_castep2cube()



    def _interpolate_scf_density(self, atoms):
        """
        Routine that interpolates a previously calculated cube file and stores
        it under self._interpolated_scf_cubefile. Raises a ValueError if there
        was no previous castep2cube or castep_hirshfeld calculation.

        We try to catch all possible mal-usages of this routine. Yet, only use
        it from the outside if you are really sure that you know what you are
        doing.
        """

        if self.calculation_required(atoms):
            msg  = 'Input "atoms" does not correspond to self.atoms. '
            msg += 'Run an SCF calculation first, followed by a '
            msg += 'castep2cube/castep_hirshfeld calculation!'
            raise ValueError(msg)

        # If atoms passes the previous test, then it must correspond to self.atoms.
        # This in turn means that either  we had to run a castep SCF
        # calculation which resets self._scf_cubefile, or we never ever ran a
        # cube calculation. In both cases self._scf_cubefile would be None.
        # This means, we will never interpolate a density that does not
        # correspond to the state of atoms if we pass this sanity test.
        if self._scf_cubefile is None:
            msg = 'There is no cubefile that could be interpolated. '
            msg += 'Run a castep2cube/castep_hirshfeld calculation first!'
            raise ValueError(msg)

        if self._new_scf_interpolation_required(self._scf_cubefile):
            self._interpolated_scf_density = InterpolatedCube(self._scf_cubefile,
                                                              verbose=self._verbose,
                                                              convert_cube_content=True,
                                                              convert_to_rs=False)
            # new scf means new aim_densities
            self._aim_densities = None

    def _new_scf_interpolation_required(self, scf_cubefile):
        """
        Routine that checks whether the the currently interpolated cube file is
        actually the input cube file
        """
        if self._interpolated_scf_density is None:
            return True
        return not scf_cubefile == self._interpolated_scf_density.cubefile


    def _calculate_hirshfeld_weights(self, atoms):
        """
        Calculate the Hirshfeld weights using castep_hirshfeld. This routines
        requires that self.calculation_required(atoms) and
        self._checkfile_exists(atoms) (in this order) evaluate to True.
        Otherwise a ValueError is raised.

        We try to catch all possible mal-usages of this routine. Yet, only use
        it from the outside if you are really sure that you know what you are
        doing.
        """
        if self.calculation_required(atoms):
            msg  = 'Input "atoms" does not correspond to self.atoms. '
            msg += 'Run an SCF calculation first!'
            raise ValueError(msg)

        if not self._checkfile_exists(atoms):
            msg = 'There is no checkfile corresponding to the state of <atoms>. '
            msg += 'Run an SCF calculation first!'
            raise ValueError(msg)

        self._prepare_continuation_input()

        # this one sets self._scf_cubefile != None
        self._run_castep_hirshfeld()

        self._restore_original_files()

        # now that we have new hirshfeld weights, in any case we need a new
        # interpolation as well
        self._interpolated_hirshfeld_weights = []
        self._interpolated_scf_density = None
        self._aim_densities = None

        if self._clean_leftovers:
            self._clean_castep_hirshfeld()


    def _interpolate_hirshfeld_weights(self, atoms):
        """
        Routine that interpolates previously calculated cube files
        corresponding to Hirshfeld_weights and stores them under
        self._interpolated_hirshfeld_weights. Raises a ValueError if there was
        no previous castep2cube or castep_hirshfeld calculation.

        We try to catch all possible mal-usages of this routine. Yet, only use
        it from the outside if you are really sure that you know what you are
        doing.
        """

        if self.calculation_required(atoms):
            msg  = 'Input "atoms" does not correspond to self.atoms. '
            msg += 'Run an SCF calculation first, followed by a '
            msg += 'castep_hirshfeld calculation!'
            raise ValueError(msg)

        # If atoms passes the previous test, then it must correspond to self.atoms.
        # This in turn means that either  we had to run a castep SCF
        # calculation which resets self._hirshfeld_weights_cubefiles, or we
        # never ever ran a cube calculation. In both cases
        # self._hirshfeld_weights_cubefiles would be None. This means, we
        # will never interpolate a density that does not correspond to the
        # state of atoms if we pass this sanity test.
        if not self._hirshfeld_weights_cubefiles:
            msg = 'There are no cubefiles that could be interpolated. '
            msg += 'Run a castep_hirshfeld calculation first!'
            raise ValueError(msg)

        if self._new_hirshfeld_weights_interpolation_required(self._hirshfeld_weights_cubefiles):
            # update the Hirshfeld weight interpolation
            self._interpolated_hirshfeld_weights = []
            for cubefile in self._hirshfeld_weights_cubefiles:
                interpolated_cube = InterpolatedCube(cubefile,
                                                     verbose=self._verbose,
                                                     convert_cube_content=False,
                                                     convert_to_rs=False)
                self._interpolated_hirshfeld_weights.append(interpolated_cube)

            # if we need new interpolations, then we can screw the old values
            self._aim_densities = None

    def _new_hirshfeld_weights_interpolation_required(self, hirshfeld_weights_cubefiles):
        """
        Routine that checks whether the currently interpolated cube files
        actually are input cube files
        """
        if not self._interpolated_hirshfeld_weights:
            return True

        Nfiles = len(hirshfeld_weights_cubefiles)
        if not len(self._interpolated_hirshfeld_weights) == Nfiles:
            return True

        interpolated_files = [self._interpolated_hirshfeld_weights[i].cubefile]
        return np.any(hirshfeld_weights_cubefiles != interpolated_files)

    #
    # NEW DUE TO CHANGES IN castep_hirshfeld:
    #   The f90 routine now directly evaluates the embedding densities. No need
    #   to go via the Hirshfeld weights any longer.
    #
    def _calculate_embedding_densities(self, atoms):
        """
        Calculate the Hirshfeld embedding densities using castep_hirshfeld.
        This routines requires that self.calculation_required(atoms) and
        self._checkfile_exists(atoms) (in this order) evaluate to True.
        Otherwise a ValueError is raised.

        We try to catch all possible mal-usages of this routine. Yet, only use
        it from the outside if you are really sure that you know what you are
        doing.
        """
        self._calculate_hirshfeld_weights(atoms)
        self._interpolated_embedding_densities = []

    def _interpolate_embedding_densities(self, atoms):
        """
        Routine that interpolates previously calculated cube files
        corresponding to embedding densities as directly evaluated with later
        versions of castep_hirshfeld and stores them under
        self._interpolated_embedding_densities. Raises a ValueError if there
        was no previous castep2cube or castep_hirshfeld calculation.

        We try to catch all possible mal-usages of this routine. Yet, only use
        it from the outside if you are really sure that you know what you are
        doing.
        """

        if self.calculation_required(atoms):
            msg  = 'Input "atoms" does not correspond to self.atoms. '
            msg += 'Run an SCF calculation first, followed by a '
            msg += 'castep_hirshfeld calculation!'
            raise ValueError(msg)

        # If atoms passes the previous test, then it must correspond to self.atoms.
        # This in turn means that either  we had to run a castep SCF
        # calculation which resets self._hirshfeld_weights_cubefiles, or we
        # never ever ran a cube calculation. In both cases
        # self._hirshfeld_weights_cubefiles would be None. This means, we
        # will never interpolate a density that does not correspond to the
        # state of atoms if we pass this sanity test.
        if not self._embedding_densities_cubefiles:
            msg = 'There are no cubefiles that could be interpolated. '
            msg += 'Run a castep_hirshfeld calculation first!'
            raise ValueError(msg)

        if self._new_embedding_densities_interpolation_required(self._embedding_densities_cubefiles):
            # update the embedding density interpolation
            self._interpolated_embedding_densities = []
            for cubefile in self._embedding_densities_cubefiles:
                interpolated_cube = InterpolatedCube(cubefile,
                                                     verbose=self._verbose,
                                                     # conversion to e/Angstrom**3
                                                     convert_cube_content=True,
                                                     convert_to_rs=False)
                self._interpolated_embedding_densities.append(interpolated_cube)

            # if we need new interpolations, then we can screw the old values
            self._aim_densities = None


    def _new_embedding_densities_interpolation_required(self, embedding_densities_cubefiles):
        """
        Routine that checks whether the currently interpolated cube files
        actually are input cube files
        """
        if not self._interpolated_embedding_densities:
            return True

        Nfiles = len(embedding_densities_cubefiles)
        if not len(self._interpolated_embedding_densities) == Nfiles:
            return True

        interpolated_files = [self._interpolated_embedding_densities[i].cubefile]
        return np.any(embedding_densities_cubefiles != interpolated_files)


    def _checkfile_exists(self, atoms):
        """
        Asks whether a *.check file corresponding to the current _seed exists
        This may be the only weakpoint in this calculator. In principle we
        would need a check whether the check file really corresponds to the
        atoms + param. But how can we do it here? Rather we rely on some
        user-intelligence: If _seed and/or _directory have not changed, the
        check file is considered to be valid.

        In addition, we have a check for the atoms object before... This means,
        if atoms does not correspond to self.atoms, then we also do not trust a
        possibly existing check file.
        """
        if self.calculation_required(atoms):
            return False
        return os.path.exists(os.path.join(self._directory,
                                           self._seed + self._check_suffix))


    # =========================
    # Running castep -- tweaked
    # =========================

    def run(self):
        """
        Tweaked run() version, that calls CASTEP acc. to the parent class and
        cleans afterwards. Additionally it pushes the SCF state so that we can
        savely restore it.
        """
        if self._verbose:
            print('Running castep (seed = %s)'%self._seed)
            print('\tcalculation directory: %s'%self._directory)

        _t = time.time()
        Castep.run(self)

        if self._verbose:
            print('\truntime : {}'.format(format_timing(_t, time.time())))

        # save the SCF state
        self._push_scf_state()
        self._clean_castep()

        # a new SCF calculation definitely requires a new continuation
        # calculation
        self._reset_continuation_status()

    def _reset_continuation_status(self):
        """
        Routine that resets all continuation related flags and storages.
        """
        self._scf_cubefile = None
        self._interpolated_scf_density = None

        self._hirshfeld_weights_cubefiles = []
        self._embedding_densities_cubefiles = []
        self._interpolated_hirshfeld_weights = None
        self._interpolated_embedding_densities = None

        self._aim_densities = None

    # ==============================
    # Running the continuation tools
    # ==============================

    def _run_castep2cube(self):
        """
        Modified version of the original run() command. Runs castep2cube and
        additionally renames the output from {seed}.chargeden_cube to
        {seed}-chargeden.cube to make life easier for visualization tools. Runs
        without asking and crashes if you did not take care of having a correct
        check file or forgot to prepare input files.
        """
        # change to target directory
        cwd = os.getcwd()
        os.chdir(self._directory)

        # run castep2cube itself
        if self._verbose:
            print('Running castep2cube (seed = %s)'%self._seed)
            print('\tcalculation directory: %s'%self._directory)

        _t = time.time()
        stdout, stderr = shell_stdouterr('%s %s' % (self._castep2cube_command,
                                                    self._seed))

        if stdout and self._verbose:
            print('castep2cube call stdout:\n%s' % stdout)
        if stderr and self._verbose:
            print('castep2cube call stderr:\n%s' % stderr)

        # check for non-empty error files
        err_file = '%s.0001.err' % self._seed
        if os.path.exists(err_file):
            err_file = open(err_file)
            self._error = err_file.read()
            err_file.close()

        if self._error:
            raise RuntimeError(self._error)

        # again EAFP paradigm...
        try:
            os.rename(self._seed + '.chargeden_cube',
                      self._seed + '-chargeden.cube')
            self._scf_cubefile = os.path.join(self._directory, self._seed + '-chargeden.cube')
        except OSError:
            pass

        os.chdir(cwd)

        self._continuation_calls += 1

        #once everything is done, push the SCF state back
        self._restore_scf_state()

        # after this command the scf density is up-to-date and
        # self._scf_cubefile points to the correct file. But we need a new
        # interpolation.
        self._interpolated_scf_density = None
        self._aim_densities = None

        if self._verbose:
            print('\truntime : {}'.format(format_timing(_t, time.time())))

    def _run_castep_hirshfeld(self):
        """
        Modified version of the original run() command. Runs castep_hirshfeld,
        which also produces an SCF cube file. Runs without asking and crashes
        if you did not take care of having a correct check file or forgot to
        prepare input files.
        """
        # change to target directory
        cwd = os.getcwd()
        os.chdir(self._directory)

        # run castep_hirshfeld (does not work on several cores!)
        if self._verbose:
            print('Running castep_hirshfeld (seed = %s)'%self._seed)
            print('\tcalculation directory: %s'%self._directory)

        _t = time.time()
        stdout, stderr = shell_stdouterr('%s %s' % (self._castep_hirshfeld_command,
                                                    self._seed))

        if stdout and self._verbose:
            print('castep_hirshfeld call stdout:\n%s' % stdout)
        if stderr and self._verbose:
            print('castep_hirshfeld call stderr:\n%s' % stderr)

        # check for non-empty error files
        err_file = '%s.0001.err' % self._seed
        if os.path.exists(err_file):
            err_file = open(err_file)
            self._error = err_file.read()
            err_file.close()

        if self._error:
            raise RuntimeError(self._error)

        os.chdir(cwd)

        # SCF cube file
        self._scf_cubefile = os.path.join(self._directory, self._seed + '-chargeden.cube')


        index_map = ase_index_to_castep_indices(self.atoms)

        # here comes the new switch as we can now evaluate embedding densities
        # directly from within the f90 routine and thus can retain a bit more
        # precision
        if self._castep_hirshfeld_version == 1:
            # via Hirshfeld weights
            pattern='{0:s}-ns_{2:03d}-{1:s}-ni_{3:04d}-Hirshfeld_w.cube'
            self._hirshfeld_weights_cubefile = []

            for atom in self.atoms:
                f = pattern.format(self._seed, atom.symbol, *index_map[atom.index])
                self._hirshfeld_weights_cubefiles.append(os.path.join(self._directory, f))

        elif self._castep_hirshfeld_version == 2:
            # directly via embedding densities
            pattern='{0:s}-ns_{2:03d}-{1:s}-ni_{3:04d}-Hirshfeld_AIM.cube'
            self._embedding_densities_cubefiles = []

            for atom in self.atoms:
                f = pattern.format(self._seed, atom.symbol, *index_map[atom.index])
                self._embedding_densities_cubefiles.append(os.path.join(self._directory, f))


        self._continuation_calls += 1

        #once everything is done, push the SCF state back
        self._restore_scf_state()

        # after this command the scf density and the aim densites are
        # up-to-date and the respective self._*_files point to the correct
        # files. But we need new interpolations
        self._interpolated_scf_density = None
        self._interpolated_embedding_densities = []
        self._interpolated_hirshfeld_weights = []
        self._aim_densities = None

        if self._verbose:
            print('\truntime : {}'.format(format_timing(_t, time.time())))

    # ========================================================================
    # preparing input files for continuation runs and restoring back to normal
    # ========================================================================

    def _prepare_continuation_input(self):
        """
        Routine that prepares the input for a continuation run:

        a) Backup existing .param and .castep files. This is necessary because
        on the one hand we need to add ``continuation : default'' to the param
        file (and thus remove a possible ``rieuse'' flag). On the other hand,
        continuation runs tend to append their output to existing .castep
        files. However, as their io is usually not standard conform, this might
        break our parser in the end.

        b) Write a new param file with all previous settings but remove (if
        existing) the ``reuse'' flag and append ``continuation : default''.
        """

        # back up the original files
        paramfile = os.path.join(self._directory, self._seed + '.param')
        castepfile = os.path.join(self._directory, self._seed + '.castep')
        for f in [paramfile, castepfile]:
            os.rename(f, f + '.bak')

        # write the new param file. Remove the reuse flag (if it was set) and
        # append continuation
        self.param.reuse.value = None
        self.param.continuation = self._seed + '.check'

        if self._export_settings:
            interface_options = self._opt
        else:
            interface_options = None

        write_param(paramfile,
                    self.param,
                    check_checkfile = True,
                    interface_options = interface_options)


    def _restore_original_files(self):
        """
        This routine restores the previously backupped .castep and .param
        files. The ones created only for the continuation run are added a
        ``.continuation'' suffix.
        """

        paramfile = os.path.join(self._directory, self._seed + '.param')
        castepfile = os.path.join(self._directory, self._seed + '.castep')

        for f in [paramfile, castepfile]:
            # you can only restore if the backup files exists
            if os.path.exists(f+ '.bak'):
                os.rename(f, f + '.continuation')
                os.rename(f+'.bak', f)
            else:
                msg  = 'Backup file {} does not exist!'
                raise OSError(msg)
        return None


    # =============================
    # pushing and recovering states
    # =============================

    def push_oldstate(self):
        """
        Routine that pushes the old state as expected from parent. Additionally
        it pushes the param to a _scf_param.
        """
        Castep.push_oldstate(self)
        self._push_scf_state()

    def _push_scf_state(self):
        """
        This function pushes the current state of the (CASTEP) Atoms object
        onto the previous 'SCF' state. We do this to avoid confusion because of
        changed 'reuse' or 'continuation' keywords in the param instance after a
        continuation calculation.
        """
        # make a backup of the param of the scf
        # remaining part should not change under no circumstance
        self._scf_param = copy.deepcopy(self.param)

    def _restore_scf_state(self):
        """
        This function restores the scf state. Call it after a continuation
        calculation in order to to recalculate stuff. In other words, calling this
        routine after a continuation calculation allows to ask for SCF properties
        without an extra calculation.
        """
        self.param = copy.deepcopy(self._scf_param)

        # also restore the old atoms etc
        self.push_oldstate()


    # ==========================
    # cleaning unnecessary files
    # ==========================

    def _clean_generic(self, pattern):
        """
        Clean some unnecessary files that are determined via the file ending in
        clean_list.

        Parameters
        ----------
        pattern : (raw) string
            Regular expression that matches all files in self._directory that
            shall be deleted.
        """
        # change to target directory
        cwd = os.getcwd()
        os.chdir(self._directory)

        for f in os.listdir(os.getcwd()):
            if re.search(pattern, f):
                try:
                    os.remove(f)
                    if self._verbose:
                        print('\t%s'%(f))
                except OSError:
                    if self._verbose:
                        print('\tCannot delete %s'%(f))
        os.chdir(cwd)

        return None

    def _clean_castep(self):
        """
        Clean some unnecessary files that are written by castep by default and
        not needed in the MD.
        """
        # clean list
        clean_list = ['.castep_bin',
                      '.cst_esp',
                      '.bib'
                      ]

        pattern = '|'.join([s.replace('.','\.') for s in clean_list])

        if self._verbose:
            print('Deleting unnecessary output files from castep run:')
        self._clean_generic(pattern)

        return None


    def _clean_castep2cube(self):
        """
        Clean some unnecessary files that are written by castep2cube (if you do
        not want to fudge around with the respective f90 files...)
        """
        # clean list
        clean_list = ['.chargeden_xsf',
                      '.chdiff_cube',
                      '.chdiff_xsf',
                      '.cst_esp',
                      '.spinden_xsf',
                      '.spinden_cube',
                      '.spinden.cube']

        pattern = '|'.join([s.replace('.','\.') for s in clean_list])

        if self._verbose:
            print('Deleting unnecessary output files from castep2cube run:')
        self._clean_generic(pattern)

        return None


    def _clean_castep_hirshfeld(self):
        """
        Clean some unnecessary files that are written by castep_hirshfeld (if you do
        not want to fudge around with the respective f90 files...)
        """
        clean_list = ['Hirshfeld_rho_ba']

        pattern = '|'.join([s.replace('.','\.') for s in clean_list])

        if self._verbose:
            print('Deleting unnecessary output files from castep_hirshfeld run:')
        self._clean_generic(pattern)

        return None



    # =======================
    # Further helper routines
    # ========================

    def reuse_previous_calculation(self, directory):
        """
        Glob for a check file in the given folder.
        """
        pattern = r'.+' + '{}'.format(self._check_suffix)
        for f in os.listdir(directory):
            if re.search(pattern, f):
                checkfile = os.path.abspath(os.path.join(directory, f))
                break

        # remove the continuation flag if there is any
        self.param.continuation.value = None
        self.param.reuse = checkfile


    def merge_defaultcastep(self, calc):
        """
        Routine that merges a CastepCalculator instance to the present
        instance. As little information as possible will be lost by simply
        updating the __dict__ variable
        """
        if not calc.__class__.__name__ == 'Castep':
            raise ValueError('Only Castep calculators can be converted (provided {} instance).'.format(calc.__class__.__name__))

        if calc.atoms is not None:
            atoms = calc.atoms.copy()

        for k in vars(calc).keys():
            # if we pass the _opt dictionary, the new options from
            # ExtendedCastep will become unknown
            if k == '_opt':
                continue
            self.__setattr__(k, calc.__getattr__(k))

        self._continuation_calls = self._calls

        # make sure that the instance-assigned atoms object does not longer
        # reference to the old calculator
        self.set_atoms(atoms)


# ========================================
# Routines that do not require an instance
# ========================================

def ase_index_to_castep_indices(atoms=None):
    """
    Routine that maps from ase indices (just continuous numbers) to castep
    indices (separate number for species and within species). Acc. to the
    mailing list, the ordering within aspecies is not touched.
    """
    atomic_numbers = np.unique(atoms.get_atomic_numbers())
    ase_indices = range(len(atoms))

    index_map = {}

    ns = 1
    ni = 1
    for atomic_number in atomic_numbers:
        for i in ase_indices:
            if atoms[i].number == atomic_number:
                index_map[i] = (ns, ni)
                ni += 1
        ni = 1
        ns +=1

    return index_map


def get_castep2cube_command(castep2cube_command = ''):
    """Abstract the quest for a castep2cube_command string."""
    if castep2cube_command:
        return castep2cube_command
    else:
        try:
            return os.environ['CASTEP2CUBE_COMMAND']
        except KeyError:
            return 'castep2cube'



def get_castep_hirshfeld_command(castep_hirshfeld_command = ''):
    """Abstract the quest for a castep_hirshfeld_command string."""
    if castep_hirshfeld_command:
        return castep_hirshfeld_command
    else:
        try:
            return os.environ['CASTEP_HIRSHFELD_COMMAND']
        except KeyError:
            return 'castep_hirshfeld'


# =============================================
# Check if all continuation tools are installed
# =============================================

def installed():
    """
    This is a tweaked version from ase.tests.castep.__init__.py that also
    checks for castep2cube and castep_hirshfeld
    """
    import os
    import subprocess
    from ase.test import NotAvailable
    commands = [('CASTEP_COMMAND', 'castep', 'CASTEP'),
                ('CASTEP_HIRSHFELD_COMMAND', 'castep_hirshfeld', 'CASTEP Hirshfeld'),
                ('CASTEP2CUBE_COMMAND', 'castep2cube', 'CASTEP2Cube')]

    for (var, default, name) in commands:
        if var not in os.environ:
            print("WARNING: Environment variable '{}' is not set".format(var))
            print("Will set {} = {} for the sake of this test".format(var, default))
            print("Please change it if this does not run castep in your environment")
            os.environ[var] = default

        # the "split" is to avoid mpirun issues
        if not tools.which(os.environ[var]):
            msg =  "Could not find {}. If you have it installed make sure,".format(name)
            msg += "you set the {} environment variable correctly""".format(var)
            raise NotAvailable(msg)


    return True



