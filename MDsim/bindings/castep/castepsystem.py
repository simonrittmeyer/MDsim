# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import re
import numpy as np

from ase.io.castep import read_seed
from ase.io import read

from MDsim import units
from MDsim.bindings.ase.asetomdsim import ASESystem

from MDsim.bindings.castep import ExtendedCastep

class CastepSystem(ASESystem):
    """
    This is a system that relies on CASTEP to evaluate energies and forces, ie.
    which can be used to run AIMD simulations.

    It uses the ASE CASTEP calculator, or the modified version shipped with
    MDsim, respectively. To keep track of output files, we use the calc._calls
    variable, though this is not very pythonic.

    Initialization
    --------------
    atoms : ASE atoms object
        The system that is to be considered. This atoms object must necessarily
        have a Castep or ExtendedCastep instance attached as calculator.
        In case of a restart, positions and momenta will be updated according
        to the provided trajectory.

    friction : Any derivative of FrictionMatrix, optional (default = None)
        Friction to be applied. See the respective classes for details.

    temperature : float, optional (default = None)
        Temperature in Kelvin in a Langevin-like description. Defaults to 0K.

    calc_folder : string, optional (default = 'castep_runs')
        The folder in which all calculations are stored. Will be created if not
        yet existing.

    seed : string, optional (default = 'MDsim__castep_ef')
        Common seed of all calculations. Individual steps will be tagged <seed>
        + step number.

    restart_traj : ASE trajectory as written with the TrajectoryHandler, optional
        A trajectory file from which to restart the system. This trajectory
        MUST be written by the MDsim/ASE interface TrajectoryHandler, as it
        dumps crucial information for the restart.

        If you want to restart, then the <calc_folder> must exist and it has to
        contain the *.cell, *.param and *.castep file of the last run(s). *The
        seed must not be changed as compared to the previous run!* If you have
        also *.check files, those will be used for the next step.

        For a smooth restart, initialize exactly the same way as the original
        run. However, you have to set <atoms>!=None in order to properly
        restore all calculator settings.

    _init_forces : Boolean, optional (default = True)
        Initialize forces in the constructor.

    _init_etas : Boolean, optional (default = True)
        Initialize friction coefficients in the constructor.

    _keep_checkfiles : integer, optional (default = 2)
        The latest N checkfiles will be stored. Others deleted. "all" is also a
        valid option.

    _verbose : Boolean, optional (default = True)
        Print some status information to the screen.
    """
    def __init__(self, atoms,
                       friction=None,
                       temperature=0.,
                       calc_folder='castep_runs',
                       seed='MDsim__castep',
                       restart_traj=None,
                       _init_etas=False,
                       _init_forces=False,
                       _keep_checkfiles=2,
                       _verbose=False):


        self._calc_folder = calc_folder
        self._seed = seed

        # let's create the calculation folder here already. Otherwise we may
        # run into trouble because of the parallelization of the QM part of
        # QM/Me+EF
        if not os.path.exists(calc_folder):
            os.makedirs(calc_folder)

        # sanity check:
        if not (_keep_checkfiles == 'all' or isinstance(_keep_checkfiles, int)):
            msg = 'Variable _keep_checkfiles must either be "all" or an integer (provided = "{}")'.format(_keep_checkfiles)
            raise ValueError(msg)
        else:
            self._keep_checkfiles = _keep_checkfiles

        if restart_traj:
            # read the trajectory (index=':' gives entire trajectory)
            self._restart_traj = restart_traj
            traj_inst = read(self._restart_traj, index=':')

            # time is stored in fs in the atoms object
            self._restart_time = traj_inst[-1].info['time']*units.FS_TO_AU

            # first item is step 0
            self._restart_step = traj_inst[-1].info['atoms.calc._calls'] - 1

            # seed of last calculation incl. full path
            restart_seed = os.path.join(self._calc_folder,
                                        self._seed + '-%06d'%self._restart_step)

            # caution: read_seed restores a CastepCalculator not an
            # ExtendedCastep calculator...
            # !!!this is error prone, as read_seed restores a different
            # !!! order of the atoms than the trajectory does!
            # !!! we hence read a ghost atoms and copy all of the calculator
            # info for convenient re-setup
            #_atoms = read_seed(restart_seed)

            # if we restart, we will always use the extended castep calculator.
            # It does precisely the same as the default castep calculator if
            # you do not require any continuation calls
            #calc = ExtendedCastep(verbose=_verbose)
            #calc.merge_defaultcastep(_atoms.get_calculator())

            #atoms.set_calculator(calc)
            atoms.calc.push_oldstate()

            # update the time and momenta, ie. the dynamic information we
            # cannot obtain from reading plain castep information...
            atoms.info.update(traj_inst[-1].info)
            atoms.set_momenta(traj_inst[-1].get_momenta())

            # the positions from the trajectory file are supposed to be more accurate
            atoms.set_positions(traj_inst[-1].get_positions())

            # also set the internal calculator counter to properly handle restarting
            atoms.calc._calls = self._restart_step + 1

            # if we have an extended castep calculator also sync the
            # continuation calls counter
            try:
                atoms.calc._continuation_calls = self._restart_step
            except AttributeError:
                pass

            # do not forget to add the check file (has to be done manually, as
            # read_seed does not do it -- though it probably should)
            # UPDATE: new revision of castep calculator does this now... keep
            # it here anyway in case we do not have the latest version.
            atoms.calc._check_file = os.path.basename(restart_seed) + '.check'


            # initializeing forces is for free
            _init_forces = True
            if _verbose:
                print('Re-starting a {:s} instance from previous run'.format(self.__class__.__name__))
                print('\ttrajectory file : {}'.format(self._restart_traj))
                print('\tstep : {:d}'.format(self._restart_step))
                print('\ttime : {:.3f} au = {:.3f} fs'.format(self._restart_time, self._restart_time*units.AU_TO_FS))

        else:
            if atoms is None:
                msg = 'Cannot initialize a new {:s} without an atoms instance'.format(self.__class__.__name__)
                raise ValueError(msg)
            if _verbose:
                print('Initializing a new {:s} instance'.format(self.__class__.__name__))
            self._restart_traj = None
            self._restart_step = 0
            self._restart_time = 0.
            atoms.calc._calls = 0

        # check if we have the correct calculator attached:
        if atoms.calc.__class__.__name__ not in ['Castep', 'ExtendedCastep']:
            msg = 'This class requires an attached "Castep" or "ExtendedCastep" calculator instance '
            msg += '(provided : "{}").'.format(atoms.calc.__class__.__name__)
            raise ValueError(msg)

        atoms._export_settings = True

        ASESystem.__init__(self, atoms=atoms,
                                 friction=friction,
                                 temperature=temperature,
                                 _init_etas=_init_etas,
                                 _init_forces=False,
                                 _verbose=_verbose)

        # use the pre-calculated dissipated energy as well here (be careful, this is in eV!)
        try:
            self.Ediss = atoms.info['dissipated_energy']*units.EV_TO_AU
        except KeyError:
            self.Ediss = 0

        # set the time after init of parent (as this does not allow for restart)
        # do not do it via set_time() here, as the children may encounter problems
        # when overloading set_time().
        self.time = self._restart_time
        self.atoms.info.update({'time': self.get_time(),
                                'atoms.calc._calls': self.atoms.calc._calls,
                                'dissipated_energy' : self.get_dissipated_energy(),
                                'dissipation_rate' : self.get_dissipation_rate()})

        # use the automatic output tracking of the calculator
        self.atoms.calc._track_output = True
        self.atoms.calc._try_reuse = True
        self.atoms.calc._label = self._seed
        self.atoms.calc._directory = self._calc_folder

        if _init_forces:
            if self._verbose:
                print('\tInitializing forces')
            self._init_forces()

    def get_Ncalls(self):
        return self.atoms.calc._calls

    def get_calc_folder(self):
        return self._calc_folder

    def _clean_checkfiles(self):
        """
        Clean outdated checkfiles to save diskspace.
        """
        # we can also keep all check files
        if self._keep_checkfiles == 'all':
            return

        pattern = self._seed + r'\-([\d]+)\.check'
        _print_head = True
        # numbering starts with 0...
        clean_calls = self.atoms.calc._calls - self._keep_checkfiles

        if clean_calls < 0:
            pass
        else:
            for f in os.listdir(self._calc_folder):
                match = re.search(pattern, f)
                if match:
                    if int(match.groups()[0]) < clean_calls:
                        delete = os.path.join(self._calc_folder, match.group())
                        if self._verbose:
                            if _print_head:
                                print('Cleaning previous check files:')
                                _print_head = False
                            print('\t{}'.format(delete))
                        os.remove(delete)


    def set_time(self, time, **kwargs):
        """
        Clean checkfiles in addition...
        """
        super(CastepSystem, self).set_time(time, **kwargs)
        # restart info for QM/Me+EF
        self.atoms.info.update({'atoms.calc._calls' : self.atoms.calc._calls})
        try:
            self.atoms.info.update({'atoms.calc._continuation_calls' : self.atoms.calc._continuation_calls})
        except AttributeError:
            # in case we do not have an ExtendedCastep calculator attached
            pass

        self._clean_checkfiles()
