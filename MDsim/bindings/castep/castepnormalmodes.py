# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

from ase.io.castep import read_castep_phonon

from MDsim.coordinates.normalmodes import Normalmodes

class CastepNormalmodes(Normalmodes):
    """
    Class that handles coordinate transformations in the context of a
    normalmode analysis done with CASTEP. All necessary information is provided
    by the .phonon file. Note that this this not cover phonon analysis except
    from the Gamma point.

    For details on the actual transformations, please see the parent module.

    Initialization
    --------------
    phononfile : str
        Location of the phononfile on which the transformations are based.
    """
    def __init__(self, phononfile):
        self.phononfile = phononfile
        # we avoid the generic ase reading routine here
        with open(self.phononfile, 'r') as fd:
            [omegas, displacements], self.atoms = read_castep_phonon(fd,
                                                                     read_vib_data=True,
                                                                     gamma_only=True)

        # "displacements" is organized as follows:
        # (Natoms*3 x Natoms*3) [mode, atomic displacement]
        # hence we need the transpose (and the real part only!)
        super(CastepNormalmodes, self).__init__(displacements=displacements.real.T,
                                                omegas=omegas,
                                                masses=self.atoms.get_masses(),
                                                positions_eq=self.atoms.get_positions(),
                                                convert=True)
