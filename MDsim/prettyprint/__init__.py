# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim import __version__

logo = r"""
+------------------------------------------------------------------------------+
|                        __  __ ____      _                                    |
|                       |  \/  |  _ \ ___(_)_ __ ___                           |
|                       | |\/| | | | / __| | '_ ` _ \                          |
|                       | |  | | |_| \__ \ | | | | | |                         |
|                       |_|  |_|____/|___/_|_| |_| |_|                         |
|                                                                              |
|          Simon P. Rittmeyer (simon.rittmeyer@tum.de), 2013-17                |
+------------------------------------------------------------------------------+
| Version : {0:<67s}|
+------------------------------------------------------------------------------+
| Contributors                                                                 |
| ------------                                                                 |
| Patrick Guetlein, TUM                                                        |
+------------------------------------------------------------------------------+
"""[1::].format(__version__)

limiter = "-"*78
big_limiter = "="*78

_section_width = len(logo.split('\n')[0])-4
_section_line = '\n+-' + '-' *_section_width + '-+'

section_template = _section_line + '\n| {{:<{:2d}s}} |'.format(_section_width)+ _section_line

import sys

def format_time(runtime):
    """
    Return a formatted time difference
    """
    h = runtime / int(3600) # hours
    runtime = runtime % 3600 # remaining seconds
    m = runtime / 60 # minutes
    s = runtime % 60 # seconds
    return '%02dh %02dm %02ds'%(h,m,s)

def format_timing(start, stop):
    """
    Return a formatted time difference
    """
    runtime=int(round(stop-start))
    return format_time(runtime)

def update_progress(progress):
    barLength = 61 # Modify this to change the length of the progress bar
    status = ""
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\r[{0}] {1:>5.1f}% {2}".format( "#"*block + " "*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()

def get_date():
    # returns the current date and time well formatted
    from time import localtime, strftime
    return strftime("%Y-%m-%d %H:%M:%S", localtime())
