# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import re
from MDsim import units
from MDsim.prettyprint import logo

class IOInterface():
    """
    This is a parentclass to any pretty print IO routine.

    It handles different output observables, which are stored in the
    self.observables list.

    Besides from this list, there are no instance variables that are stored. If
    you want to have something stored in your IO object, then do derive a
    subclass from this interface.

    There is a check_observable() method which checks whether a requested
    observable is actually implemented. If not, the method will raise a
    NotImplementedError while suggesting correctios if there might have a typo.

    Information on the system in general can be obtained via the
    get_system_info() method, the same holds for the integrator via the
    get_integrtor_info() method.

    At the heart of this class, however, are the methods get_dataline() and
    get_headline(). These are called with the system as well as with the
    observable name and return pretty printed output lines.

    CONVENTION: any of the get_X() routines returns strings that do neither
    start nor end with a newline character!
    """

    # convention: info methods to return string w/o newline at the end and
    # without any comment signs. The latter will be added only by the file methods


    _section_width = len(logo.split('\n')[0])-4
    _section_line = '\n+-' + '-' *_section_width + '-+'
    _section_template = _section_line + '\n| {{:<{:2d}s}} |'.format(_section_width)+ _section_line


    @staticmethod
    def get_integrator_info(integrator):
        info = IOInterface._section_template.format('INTEGRATOR / TIME PROPAGATION')
        info += '\n{}'.format(integrator)

        return info

    @staticmethod
    def get_system_info(system):

        # get all the relevant information
        system_name = str(system)
        species = system.get_species()
        masses  = system.get_masses()
        Natoms  = system.get_Natoms()
        Ndim  = system.get_Ndim()

        kT = system.get_kT()
        potential = system.get_potential()

        info  = IOInterface._section_template.format('SYSTEM')[1::]
        info += '\n{}'.format(system)
        info += '\n'
        info += IOInterface._section_template.format('POTENTIAL ENERGY SURFACE')
        if system.has_pes:
            potential = system.get_potential()
            info += '\n'+str(potential)
        else:
            info += '\nNo PES available, ie. zero PES-forces'

        info += '\n'
        info += IOInterface._section_template.format('(ELECTRONIC) FRICTION MATRIX')
        if system.has_friction:
            friction_matrix = system.get_friction()
            info += '\n' + str(friction_matrix)
        else:
            info += '\nNo friction matrix available'

        return info

    @staticmethod
    def get_units_info():
        info = IOInterface._section_template.format('OUTPUT UNITS (based on CODATA {})'.format(units.__codata_version__))
        info += '\nunit of time                 : femtosecond'
        info += '\nunit of position             : Angstrom'
        info += '\nunit of velocity             : Angstrom per femtosecond'
        info += '\nunit of mass                 : atomic mass unit'
        info += '\nunit of energy               : electronvolt'
        info += '\nunit of force                : electronvolt per Angstrom'
        info += '\nunit of eta                  : atomic mass unit per femtosecond'
        info += '\nunit of normalmodes          : a.u.'
        info += '\nunit of dipole moments       : Debye'
        info += '\nunit of FOM integrand        : sqrt(electronvolt per femtosecond)'
        info += '\nunit of Rayleigh function    : electronvolt per femtosecond'

        return info

    @staticmethod
    def get_options(args):
        """
        Function that prints all internal settings
        """
        info = IOInterface._section_template.format('INTERNAL OPTIONS')
        max_len = max([len(i.split('.')[1]) for i in args.keys()])
        template = '\n\t{0:<%ds} : {1:s}'%max_len
        prev_sec = ''
        for arg, val in sorted(args.items()):
            sec, arg = arg.split('.')
            if prev_sec != sec:
                info += '\n[{}]'.format(sec)
            info += template.format(arg, str(val))
            prev_sec = sec
        return info

    @staticmethod
    def get_observable_info(observable):
        """
        Function that returns information important to understand the
        observable
        """
        obs_name = observable.get_idstring()
        info = IOInterface._section_template.format('OBSERVABLE SPECIFIC INFORMATION ({})'.format(obs_name))
        if observable.get_specific_info():
            info += '\n{}'.format(observable.get_specific_info())
        else:
            info += '\nNo specific information available.'
        return info

    @staticmethod
    def get_column_description(observable):
        info = IOInterface._section_template.format('COLUMNS CONTENT (pythonic enumeration)')
        col_names = observable.get_column_names()
        for i, n in enumerate(col_names):
            info += '\n{:<6d} : {:s}'.format(i,n)
        return info

