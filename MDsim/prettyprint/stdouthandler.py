# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys
import re
import gzip
import os
import shutil

from MDsim.prettyprint import logo
from MDsim.prettyprint.interface import IOInterface

class StdoutHandler(IOInterface):
    """
    This is a class that handles IO to stdout. Each object is uniquely
    connected to an observable as well as to an output file it writes the
    output to.

    Initialization
    --------------
    system : System instance of MDsim
        The actual system from which the information is to be read.

        NOTE: The corresponding object saves a reference to the system as an
        internal variable and can thus, in principle, also do harmful things to
        your system. So, make sure you know what your IO routines are actually
        doing.  The reason for this behavior is that it simplyfies the further
        IO processes - the object can be called by push() without any further
        arguments.

    integrator : Integrator instance of MDsim
        The integrator employed in the simulation. It is needed to write the
        corresponding information such as timestep and integration scheme to
        the file header.

    observable : Observable instance of MDsim, optional (default=None)
        Any of the observables that are implemented in
        MDsim.prettyprint.observables.implemented

    args : dictionary, optional (default = None)
        The dictionary holding all internal settings. There will only be an
        effect if it is passed, but then it simply lists all internal options.

    NOTE
    ----
    * The observable for the current system state is pushed to stdout via
      the push() method.

    """
    def __init__(self, system,
                       integrator,
                       observable = None,
                       args  = None):


        # raises error if not implemented
        self.obs_inst = observable
        self.system = system
        self.integrator = integrator # needed for the file header

        self.args = args

        self._headline_pushed = False

        # default it
        self._out = sys.stdout

    def push_system_info(self):
        print(self.get_system_info(self.system), file=self._out)
        print(self.get_integrator_info(self.integrator), file=self._out)

    def push_units_info(self):
        print(self.get_units_info(), file=self._out)

    def push_args_info(self):
        print(self.get_options(self.args), file=self._out)

    def push_logo(self):
        print(logo, file=self._out)

    def push_headline(self):
        print(self.obs_inst.get_headline(), file=self._out)

    def push(self):
        if not self._headline_pushed:
            self.push_headline()
            self._headline_pushed = True
        print(self.obs_inst.get_dataline(), file=self._out)
