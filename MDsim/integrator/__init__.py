# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

from MDsim import units

class Integrator(object):
    """
    Base class for any time-integrator
    """

    def __init__(self, dt, convert = True):
        self.dt = dt
        self._convert = convert
        if self._convert:
            self.dt *= units.FS_TO_AU
        self._name = 'DummyIntegrator'

    def change_dt(self, dt):
        self.dt = dt
        if self._convert:
            self.dt *= units.FS_TO_AU

    def get_stepsize(self):
        return self.dt * units.AU_TO_FS

    def propagate(self, system):
        raise NotImplementedError

    def _create_info(self):
        info = self._name
        info += '\n\nTime step size : dt = {}'.format(self.dt * units.AU_TO_FS)
        return info

    def __str__(self):
        return self._create_info()
