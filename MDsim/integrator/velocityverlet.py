# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim.integrator import Integrator

class VelocityVerlet(Integrator):
    """
    Implementation of the VelocityVerlet integration scheme according to
    Tuckerman's direct translation technique.

    For details see my MSc thesis
    """

    def __init__(self, *args, **kwargs):
        Integrator.__init__(self, *args, **kwargs)
        self._name = "Velocity Verlet"
        self._name += "\n(implemented acc. to Tuckerman's direct translation technique)"

    def propagate(self, system):
        """
        Propagation function that calls the hooks
        """
        system.pre_integration_hook()
        self._propagate(system)
        system.post_integration_hook()

    def _propagate(self, system):
        """
        Propagate the system by dt.

        We conveniently work on system arrays via
        the provided set function. Note that any system method starting with
        calc requires an explicit input of positions and returns the respective
        array. It does hence not change the system properties unless you pipe
        it into the corresponding "set_X" function
        """
        # first step: update velocities acc. to forces (effect of exp(i L_p dt/2))
        # masses are broadcasted only once by the system. Hence, access
        # system.masses_bc instead of system.masses. This allows to use ufuncs!
        # system.set_velocities(system.velocities + 0.5 * self.dt * system.forces / system.masses_bc)
        system.set_velocities(system.get_velocities(_au=True)
                              + 0.5 * self.dt * system.get_forces(_au=True)
                              / system.get_masses(_au=True)[:,None])

        # second step: update positions (effect of exp(i L_r dt))
        #system.set_positions(system.positions + system.velocities * self.dt)
        system.set_positions(system.get_positions(_au=True)
                             + system.get_velocities(_au=True) * self.dt)

        # intermediate steps: calculate new forces from potential
        # we use the get_function, as it automatically evaluates the forces due
        # to the new positions set above
        # use the a.u. switch to avoid unnecessary mutliplications
        #
        # NOTE: there is absolutely no need to set these forces, as the
        # get_function sets it automatically.
        #
        #system.set_forces(system.get_forces(_au=True))
        # actually, an update_forces() function would be nice...
        system.update_forces()

        # third step: update velocities again (effect of exp(i L_p dt/2))
        #system.set_velocities(system.velocities + 0.5 * self.dt * system.forces / system.masses_bc)
        system.set_velocities(system.get_velocities(_au=True)
                              + 0.5 * self.dt * system.get_forces(_au=True)
                              / system.get_masses(_au=True)[:,None])

        # update system time
        system.set_time(system.time + self.dt)
