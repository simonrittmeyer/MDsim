# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import math
import numpy as np
from numpy import exp
from numpy.random import randn

from MDsim.integrator.langevin.bussiparrinello import LangevinBP

class LangevinBPFrictionTensor(LangevinBP):
    """
    G. Bussi and M. Parrinello, Phys. Rev. E 75, (2007).
    http://www.dx.doi.org/10.1103/PhysRevE.75.056707

    This is a hybrid. We derive if from velocity verlet but want to appear like
    a langevin integrator. Hence some copy-pasteing.

    ** MODIFIED VERSION THAT ALLOWS TO WORK WITH FULL NON-DIAGONAL FRICTION
    MATRIX VIA COORDINATE TRANSFORMS **
    """
    def __init__(self, *args, **kwargs):
        super(LangevinBPFrictionTensor,self).__init__(*args, **kwargs)
        self._name += "\n*** EXTENDED VERSION TO WORK WITH FULL NON-DIAGONAL FRICTION TENSOR ***"

    def __str__(self):
        info = 'Integration scheme for a Langevin-like EOM acc. to\n'
        info += '    '+self._name
        info += '\n'
        info += '\nRandom seed for stochastic forces : {}'.format(self._random_seed)
        return info


    def _thermostat(self, system, _log_effective_energy=False):
        friction_tensor = system.get_friction()
        if friction_tensor.is_diagonal():
            return super(LangevinBPFrictionTensor,self)._damp_velocities(system)

        else:
            # the random numbers
            R = np.random.randn(system.Ndof)
            gamma_mw_diag = friction_tensor.get_diagonal_friction_tensor_mw(_au=True)

            # preparation step: get coefficients c1 and c2
            # note that in this case we directly use the diagonal gamma_mw, ie. the mass-weighted friction
            c1 = np.exp(-0.5 * self.dt * gamma_mw_diag)
            # note that we nedd c2 mass-weighted, so do not divide by the mass here
            c2 = np.sqrt((1.-c1**2) * system.get_kT(_au=True))

            # velocities in friction modes
            velocities_fm = friction_tensor.velocities_cartesian_to_frictionmodes(system.get_velocities(_au=True))

            # update the velocities
            velocities_fm = c1 * velocities_fm + c2*R

            exp(-0.5 * self.dt * friction_tensor.get_diagonal_friction_tensor_mw())

            # velocities in proper cartesian
            velocities = friction_tensor.velocities_frictionmodes_to_cartesian(velocities_fm)

            if _log_effective_energy:
                # save some multiplications by using au
                dEeff_conint = system.get_kinetic_energy(_au=True)
                system.set_velocities(velocities)
                dEeff_conint -= system.get_kinetic_energy(_au=True)

                return dEeff_conint

            else:
                system.set_velocities(velocities)
                return 0.

