# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from numpy import exp

from MDsim.integrator.liouville import Liouville

class LiouvilleFrictionTensor(Liouville):
    """
    Implementation of the VelocityVerlet integration scheme according to
    Tuckerman's direct translation technique.

    For details see my MSc thesis
    """

    def __init__(self, *args, **kwargs):
        super(LiouvilleFrictionTensor, self).__init__(*args, **kwargs)
        self._name += "\n*** EXTENDED VERSION TO WORK WITH FULL NON-DIAGONAL FRICTION TENSOR ***"

    # only need to overload the damping routine to work with the new eta
    def _damp_velocities(self, system):
        friction_tensor = system.get_friction()
        if friction_tensor.is_diagonal():
            return super(LiouvilleFrictionTensor,self)._damp_velocities(system)
        else:
            # velocities in friciton modes
            velocities_fm = friction_tensor.velocities_cartesian_to_frictionmodes(system.get_velocities(_au=True))

            # note that in this case we directly use the diagonal gamma_mw, ie. the mass-weighted friction
            velocities_fm *= exp(-0.5 * self.dt * friction_tensor.get_diagonal_friction_tensor_mw(_au=True))

            return friction_tensor.velocities_frictionmodes_to_cartesian(velocities_fm)
