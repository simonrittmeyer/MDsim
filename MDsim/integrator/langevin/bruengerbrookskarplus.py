# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy.random import randn

from MDsim.integrator.langevin import Langevin

class LangevinBBK(Langevin):
    """
    Implementation by Patrick Guetlein.

    Debugged and adjusted according to new MDsimv3 standards by Simon
    Rittmeyer.
    """

    def __init__(self, *args, **kwargs):
        Langevin.__init__(self, *args, **kwargs)
        self._name = "Brooks, Bruenger and Karplus (BBK)"

    def propagate(self, system):
        """
        BBK (Brooks-Bruenger-Karplus)

        see : http://localscf.com/localscf.com/LangevinDynamics.aspx.html
        """
        system.pre_integration_hook()
        sigma =  np.sqrt(2. * system.kT * system.etas / self.dt)

        R = np.random.randn(system.Natoms, system.Ndim)

        # step one: auxiliary velocity update of stepsize dt/2
        system.set_velocities((1. - system.etas / system.masses_bc * self.dt / 2.)*system.velocities
                              +self.dt / (2.*system.masses_bc) * (system.forces + sigma * R)
                             )

        # step two: position update
        system.set_positions(system.positions + self.dt * system.velocities)

        # step three: update random variable
        R = np.random.randn(system.Natoms, system.Ndim)

        # step five: force update
        system.set_forces(system.calc_forces(system.positions))
        system.set_etas(system.calc_etas(system.positions))

        # step four: complete velocity update
        system.set_velocities(1./(1. + system.etas / system.masses_bc * self.dt / 2.)
                              * (system.velocities + self.dt / (2.*system.masses_bc) * (system.forces + sigma * R))
                              )


        # step six: update time
        system.set_time(system.time + self.dt)
        system.post_integration_hook()

#    def propagate(self, system):
#        """
#        Following the implementation suggested by
#
#        C.J. Cotter and S.Reich,
#        2006,
#        "Time Stepping Algorithms for Classical Molecular Dynamics,
#        Handbook of Theoretical and Computational Nanotechnology",
#        Editors: Reith, Schommers,
#        Publisher: American Scientific Publishers,
#        ISBN: 9781588830425
#        """
#
#        # preparation step: evaluate variance and draw random number
#        sigma = np.sqrt(2 * system.kT * system.etas)
#        R = np.random.randn(system.Natoms, system.Ndim)

#        pass
