# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy.random import randn

from MDsim.integrator.langevin import Langevin

class LangevinGJF(Langevin):
    """
    Implementation according to:

    N. Gronbech-Jensen and O. Farago, Mol. Phys. 111, 983 (2013).
    """
    def __init__(self, *args, **kwargs):
        Langevin.__init__(self, *args, **kwargs)
        self._name = 'Gronbech-Jensen and Farago (GJF)'
        # repeating factors in langevin integrator
        # allows for more efficient propagation
        self._dt_square = self.dt**2


    def propagate(self, system):
        # may be optimized at some point

        system.pre_integration_hook()
        # first, get the parameter b
        b = 1. / (1. + system.etas * self.dt * 0.5 / system.masses_bc)

        # get the gaussian random numbers
        sigma =  np.sqrt(2. * system.kT * system.etas * self.dt)
        beta = sigma * np.random.randn(system.Natoms, system.Ndim)

        # first step : propagate positions
        _positions_old = system.positions.copy()
        system.set_positions(system.positions + b * self.dt * system.velocities
                                              + b * self._dt_square / (2.*system.masses_bc) * system.forces
                                              + b * self.dt / (2.*system.masses_bc) * beta)

        # second: update forces
        _forces_old = system.forces.copy()
        system.set_forces(system.calc_forces(system.positions))

        # third: update positions
        system.set_velocities(system.velocities + self.dt / (2. * system.masses_bc) * (_forces_old + system.forces)
                                                - system.etas / system.masses_bc * (system.positions - _positions_old)
                                                + 1./ system.masses_bc * beta)


        # fourth step: update the etas
        system.set_etas(system.calc_etas(system.positions))

        # forces are already updated

        # last: update time
        system.set_time(system.time + self.dt)
        system.post_integration_hook()
