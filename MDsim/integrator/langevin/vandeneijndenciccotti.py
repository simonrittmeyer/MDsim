# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import math
import numpy as np
from numpy.random import randn

from MDsim.integrator.langevin import Langevin

class LangevinVEC(Langevin):
    """
    Implementation by Patrick Guetlein.

    Scheme follows the derivation given by Tuckerman and originally developped
    by Vanden-Eijnden and Ciccotti:

    E. Vanden-Eijnden and G. Ciccotti, Chem. Phys. Lett. 429, 310 (2006).
    http://www.dx.doi.org/10.1016/j.cplett.2006.07.086

    Debugged and adjusted according to new MDsimv3 standards by Simon
    Rittmeyer.
    """

    def __init__(self, *args, **kwargs):
        Langevin.__init__(self, *args, **kwargs)

        self._name = 'Vanden-Eijnden and Cicotti (VEC)'

        # repeating factors in langevin integrator
        # allows for more efficient propagation
        self._prefactor_theta = 1./(2. * math.sqrt(3.))
        self._half_dt_power_two = 0.5 * self.dt**2
        self._dt_power_three_half = self.dt**(3./2.)
        self._dt_sqrt = math.sqrt(self.dt)



    def propagate(self, system):
        """
        To be described somewhat more in detail
        """
        system.pre_integration_hook()
        # get random force parameters
        chi, theta = np.random.randn(2, system.Natoms, system.Ndim)

        # use those to calculate the crucial propagation parameters
        # if frition was no function of positions, sigma was a constant
        sigma =  np.sqrt(2. * system.kT * system.etas) / system.masses_bc

        # calculate the "random part" A
        A = (self._half_dt_power_two * (system.forces/system.masses_bc - system.etas/system.masses_bc * system.velocities)
             + sigma * self._dt_power_three_half * (0.5 * chi + self._prefactor_theta * theta))

        # ----
        # first step: update system positions
        system.set_positions(system.positions + self.dt * system.velocities + A)

        # second step: update forces and etas
        _forces_old = system.forces.copy()

        system.set_forces(system.calc_forces(system.positions))

        # third step: update system velocities
        system.set_velocities(system.velocities + ( 0.5 * self.dt * (system.forces + _forces_old)/system.masses_bc)
                                                - self.dt * system.etas / system.masses_bc * system.velocities
                                                + sigma * self._dt_sqrt * chi
                                                - system.etas / system.masses_bc * A
                              )


        # fourth step: update the etas
        system.set_etas(system.calc_etas(system.positions))

        # forces are already updated
        # @ Patrick: Sure that we need the etas at the old positions in step 3?

        # last: update time
        system.set_time(system.time + self.dt)
        system.post_integration_hook()
