# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy.random

from MDsim.integrator import Integrator
from MDsim.tools import randomize_seed

class Langevin(Integrator):
    def __init__(self, dt, convert = True, _random_seed = None):

        Integrator.__init__(self, dt = dt, convert = convert)

        if _random_seed is None:
            _random_seed = randomize_seed()

        self._random_seed = _random_seed

        # initialize seed to obtain repeatable results
        numpy.random.seed(self._random_seed)

        self._name = 'DummyLangevinIntegrator'

    def __str__(self):
        info = 'Integration scheme for a Langevin-like EOM acc. to\n'
        info += Integrator.__str__(self)
        info += '\n'
        info += '\nRandom seed for stochastic forces : {}'.format(self._random_seed)
        return info



