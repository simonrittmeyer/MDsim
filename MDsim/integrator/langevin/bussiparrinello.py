# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import math
import numpy as np
from numpy.random import randn

from MDsim.integrator.velocityverlet import VelocityVerlet

class LangevinBP(VelocityVerlet):
    """
    G. Bussi and M. Parrinello, Phys. Rev. E 75, (2007).
    http://www.dx.doi.org/10.1103/PhysRevE.75.056707

    This is a hybrid. We derive if from velocity verlet but want to appear like
    a langevin integrator. Hence some copy-pasteing.
    """
    def __init__(self, dt, convert = True, _random_seed = None):
        VelocityVerlet.__init__(self, dt = dt, convert = convert)

        if _random_seed is None:
            _random_seed = randomize_seed()
        self._random_seed = _random_seed

        # initialize seed to obtain repeatable results
        np.random.seed(self._random_seed)

        self._name = 'Bussi and Parrinello (BP)'
        # allows for more efficient propagation
        self._half_dt_power_two = 0.5 * self.dt**2

    def __str__(self):
        info = 'Integration scheme for a Langevin-like EOM acc. to\n'
        info += '    '+self._name
        info += '\n'
        info += '\nRandom seed for stochastic forces : {}'.format(self._random_seed)
        return info


    # def _thermostat(self, system):
        # # the random numbers
        # R = np.random.randn(system.Natoms, system.Ndim)

        # # preparation step: get coefficients c1 and c2
        # c1 = np.exp(-0.5 * system.etas * self.dt / system.masses_bc)
        # c2 = np.sqrt((1.-c1**2) * system.kT / system.masses_bc)

        # system.set_velocities(c1*system.velocities + c2*R)

    # def propagate(self, system, _log_effective_energy=False):
        # """
        # Nomenclature directly follows the original paper of Bussi and
        # Parrinello, execpt that we replace c2 by c2/m as we propagate
        # velocities rather than momenta. Moreover, we use kT = 1/beta.
        # """
        # if system.has_friction:
            # self._thermostat(system)

            # if _log_effective_energy:
                # _dEeff_inc = -system.get_total_energy(_au=True)
                # VelocityVerlet.propagate(self, system)
                # _dEeff_inc += system.get_total_energy(_au=True)
                # system.set_effective_energy_increment(system.Eeff_inc + _dEeff_inc, _convert = False)
            # else:
                # VelocityVerlet.propagate(self, system)

            # self._thermostat(system)

        # # no friction = no thermostat
        # else:
            # VelocityVerlet.propagate(self, system)


    def _thermostat(self, system, _log_effective_energy=False):
        # the random numbers
        R = np.random.randn(system.Natoms, system.Ndim)

        # preparation step: get coefficients c1 and c2
        c1 = np.exp(-0.5 * system.get_etas(_au=True) * self.dt / system.get_masses(_au=True)[:,None])
        c2 = np.sqrt((1.-c1**2) * system.get_kT(_au=True) / system.get_masses(_au=True)[:,None])

        if _log_effective_energy:
            # save some multiplications by using au
            dEeff_conint = system.get_kinetic_energy(_au=True)
            system.set_velocities(c1*system.get_velocities(_au=True) + c2*R)
            dEeff_conint -= system.get_kinetic_energy(_au=True)

            return dEeff_conint

        else:
            system.set_velocities(c1*system.get_velocities(_au=True) + c2*R)
            return 0.


    def propagate(self, system, _log_effective_energy=False):
        """
        Nomenclature directly follows the original paper of Bussi and
        Parrinello, execpt that we replace c2 by c2/m as we propagate
        velocities rather than momenta. Moreover, we use kT = 1/beta.
        """
        if system.has_friction:
            system.pre_integration_hook()
            if _log_effective_energy:
                dEeff_conint = self._thermostat(system, _log_effective_energy=_log_effective_energy)
                VelocityVerlet._propagate(self, system)
                dEeff_conint += self._thermostat(system, _log_effective_energy=_log_effective_energy)
                # dEeff_conint is in au
                system.set_effective_energy_conint(system.get_effective_energy_conint(_au=True) + dEeff_conint)
            else:
                self._thermostat(system)
                VelocityVerlet._propagate(self, system)
                self._thermostat(system)
            system.post_integration_hook()

        # no friction = no thermostat
        else:
            VelocityVerlet.propagate(self, system)

