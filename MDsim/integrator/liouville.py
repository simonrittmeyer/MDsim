# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from numpy import exp
import warnings
from MDsim.integrator.velocityverlet import VelocityVerlet

class Liouville(VelocityVerlet):
    """
    Implementation of the VelocityVerlet integration scheme according to
    Tuckerman's direct translation technique.

    For details see my MSc thesis
    """

    def __init__(self, *args, **kwargs):
        VelocityVerlet.__init__(self, *args, **kwargs)
        self._name = "Propagation according to Tuckerman's direct translation technique"
        self._name += "\n(based on Liouville operator formalism, friction is included!)"


    def _damp_velocities(self, system):
        return system.get_velocities(_au=True) * exp(-0.5 * self.dt * system.get_etas(_au=True) / system.get_masses(_au=True)[:,None])

    def propagate(self, system):
        """
        Propagate the system by dt.

        We conveniently work on system arrays via
        the provided set function. Note that any system method starting with
        calc requires an explicit input of positions and returns the respective
        array. It does hence not change the system properties unless you pipe
        it into the corresponding "set_X" function
        """

        if system.has_friction:
            system.pre_integration_hook()

            # first step: damp the velocities (effect of exp(i L_f dt/2))
            # system.set_velocities(system.velocities * exp(-0.5 * self.dt * system.etas / system.masses_bc))
            system.set_velocities(self._damp_velocities(system))

            # steps two - four are the velocity verlet steps (without hooks)
            VelocityVerlet._propagate(self, system)

            # intermediate step: update the etas
            # system.set_etas(system.calc_etas(system.positions))
            system.update_etas()

            # fifth step: damp velocities again (effect of exp(i L_f dt/2))
            # system.set_velocities(system.velocities * exp(-0.5 * self.dt * system.etas / system.masses_bc))
            system.set_velocities(self._damp_velocities(system))

            # do not need to update forces since that has been done in the velocity Verlet
            # and do not need to update etas since positions have not changed since the last update
            # moreover, also time has already been updated
            system.post_integration_hook()

       # TODO: Check if this is still well-defined for eta = f(positions)
       #     # update dissipated energy (see Rayleigh's dissipation function)
       #     dEdt = system.get_dEdt(convert = False)
       #     system.Ediss += dEdt * self.dt

        else:
            warnings.warn('Using the "Liouville" integrator without friction -- fallback to "VelocityVerlet"')
            VelocityVerlet.propagate(self, system)

