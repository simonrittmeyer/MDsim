# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units
from MDsim.potentials import Potential

from MDsim.derivatives import gradient_central_differences

class HarmonicPotential(Potential):
    """
    Primitive harmonic potential along the cartesian coordinates. Note that if
    you want a harmonic potential that couples cartesian degrees of freedom,
    you need to go for a "NormalmodePotential", which implements these
    coordinate transformations.

    Initialization
    --------------
    omegas : (Natoms, Ndim) array
        The harmonic frequencies.

    masses : (Natoms,) array
        The masses of the respective oscillators.

    positions_eq : (Natoms, Ndim) array, optional (default = None)
        The equilibrium positions of the oscillators. If <None> is given, these
        will all be zero.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces use the exact analytical
        definition of harmonic forces, where numerical forces invoke finite
        (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Converts input from electronvolts (omega), amu (masses) and Angstrom
        (positions_eq) in a.u.. If <False> input in a.u..
    """
    def __init__(self, omegas, masses,
                       positions_eq = None,
                       forces_method = 'analytical',
                       forces_stepwidth = 0.001,
                       convert = True):
        self.omegas = np.asarray(omegas, dtype = np.float64, order = 'C')
        self.masses = np.asarray(masses, dtype = np.float64, order = 'C')

        self.Natoms, self.Ndim = self.omegas.shape

        if positions_eq is None:
            self.positions_eq = np.zeros_like(omegas, dtype = np.float64, order = 'C')
        else:
            if not np.all(positions_eq.shape == self.omegas.shape):
                print('Error : positions_eq and omegas have different dimensions!')
                raise ValueError

            self.positions_eq = np.asarray(positions_eq, dtype = np.float64, order = 'C')

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        self._convert = convert
        if self._convert:
            self.masses *= units.AMU_TO_AU
            self.omegas *= units.EV_TO_AU
            self.positions_eq *= units.ANGSTROM_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

        self.masses_bc = self.masses[:,np.newaxis]

        # only do the multiplication once (h==1 in atomic units)
        self._pref = self.omegas**2 * self.masses_bc

    def get_Epots(self, positions):
        Epots = np.sum(0.5 * self._pref * (positions - self.positions_eq)**2, axis = 1)
        return Epots

    def get_Epot(self, positions):
        return np.sum(self.get_Epots(positions))

    def get_forces(self, positions):
        """
        Routine to evaluate the forces originating from this potential.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Position at which to evaluate the forces.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        if self._forces_method == 'analytical':
            return -self._pref * (positions - self.positions_eq)
        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)

    def get_omegas(self):
        if self._convert:
            return self.omegas * units.AU_TO_EV
        else:
            return self.omegas

    def get_spring_constants(self):
        if self._convert:
            return self._pref*units.AU_TO_EV_PER_ANGSTROM_SQUARED
        else:
            return self._pref

    def _create_infostring(self):
        info  = 'Harmonic potential along cartesian axes'
        info += '\n'
        info += '\nFrequencies:'
        info += '\n{0:<15s}'.format('')

        for i in range(self.Ndim):
            info += '{0:>15s}'.format('omega_{}'.format(['x','y','z'][i]))
        info += '\n' + '-'*(3*15)

        omegas_meV = self.omegas * units.AU_TO_EV * 1000
        for i in range(self.Natoms):
            info += '\nAtom {0:<10d}'.format(i)
            for j in range(self.Ndim):
                info += '{:>15s}'.format('{:>10.3f} meV'.format(omegas_meV[i,j]))
        info += '\n'
        info += '\nForces method : {}'.format(self._forces_method)
        if self._forces_method == 'numerical':
            info += ' (forces stepwidth = {} Angstrom)'.format(self._forces_stepwidth*units.AU_TO_ANGSTROM)
        return info




    def __str__(self):
        return self._create_infostring()
