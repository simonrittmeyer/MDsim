# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units
from MDsim.potentials import Potential

from MDsim.derivatives import gradient_central_differences

class NormalmodePotential(Potential):
    """
    Harmonic approximation to a given potential. Requires a normalmode analysis
    prior to creation.

    In principle, things are easy in normalmodes. The potential can be
    decomposed into independent mode-contributions. If q is the positions
    vector in normal mode space, then the total potential energy is

        V = 0.5 * sum_i(omega_i^2 * q_i^2)                                      (1)

    where omega is the square roots of the mass-weighted Hessian eigenvalues.
    At the same time, the force Fnm in normalmode space is trivial:

        Fnm_i = -omega_i^2 * q_i                                                (2)

    which transforms to cartesian space as follows:

        Fcart = (Msqrt.Q).Fnm                                                   (3)

    if we adapt the notation from the normalmodes module and Q transforms from
    normalmode space to mass-weighted cartesian space. (Yes indeed, forces
    require transformation in the opposite way as they are gradients)

    But these transformations are done via the Normalmodes coordinates object
    anyway. What we hence do is we transform the cartesian input to normalmode
    space, evaluate potential energy and forces, and transform forces back to
    cartesian space.

    Initialization
    --------------
    normalmodes : Normalmodes Coordinates object from MDsim
        The coordinates object from a normalmode analysis. Bindings to external
        codes are work in progress.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical use the procedure described
        above, where numerical forces invoke finite (central) differences in
        cartesian space.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Converts input for the forces stepwidth from Angstrom to a.u.. If
        <False> input in a.u..
    """

    def __init__(self,
                 normalmodes,
                 forces_method='analytical',
                 forces_stepwidth=0.001,
                 convert=True):

        self.omegas_squared = normalmodes.get_omegas(_au=True)**2
        self.coords = normalmodes

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth
        self._convert = convert
        if self._convert:
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

    def get_Epots_modes(self, positions):
        """
        Return the potential energy contained in each mode.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions in cartesian space specifying the configuration.

        Returns
        -------
        Epots : (Nmodes=Natoms*Ndim,) array
            Potential energy contained within each mode.
        """
        return 0.5*self.omegas_squared*self.coords.positions_cartesian_to_normalmodes(positions)**2

    def get_Epot(self, positions):
        """
        Return the total potential energy.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions in cartesian space specifying the configuration.

        Returns
        -------
        Epot : float
            Potential energy contained in the system.
        """
        return np.sum(self.get_Epots_modes(positions))



    def get_forces(self, positions):
        """
        Routine to evaluate the forces originating from this potential.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions in cartesian space specifying the configuration.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces in cartesian space at the respective positions.
        """

        if self._forces_method == 'analytical':

            return self.coords.gradient_normalmodes_to_cartesian(
                    -self.omegas_squared*self.coords.positions_cartesian_to_normalmodes(
                        positions)
                    )

        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)


    def _create_infostring(self):
        info  = 'Harmonic potential along normalmode axes'
        info += '\n\nFrequencies:'
        info += '\n------------'
        for i, o in enumerate(self.coords.get_omegas(_au=True)):
            info += '\n\tomega_{}'.format(i)
            info += ' = {:9.3E} a.u.'.format(o)
            info += ' = {:9.3f} meV'.format(o*units.AU_TO_EV*1000)
            info += ' = {:9.3f} cm^-1'.format(o*units.AU_TO_KAYSER)
            info += ' = {:9.3f} THz'.format(o*units.AU_TO_THZ)
        info += '\n'
        info += '\nForces method : {}'.format(self._forces_method)
        if self._forces_method == 'numerical':
            info += ' (forces stepwidth = {} Angstrom)'.format(self._forces_stepwidth*units.AU_TO_ANGSTROM)
        return info

    def __str__(self):
        return self._create_infostring()
