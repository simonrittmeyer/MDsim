# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units
from MDsim.potentials import Potential

from MDsim.derivatives import gradient_central_differences

from MDsim.interpolation.univariatespline import UnivariateSpline


class DiagonalAnharmonicPotential(Potential):
    """
    Diagonal anharmonic approximation to a given potential. Requires a
    normalmode analysis prior to creation.

    What we do here is the following: We transform the a normalmode
    decomposition of the positions. Then, for each populated mode, we
    independently evaluate the potential energy, ie. we create an artificial
    mode vector that is only populated at one mode and evaluate the potential
    energy for this mode vector using the underlying potential.

    As it comes to forces, we do somethong similar: Again, we create an
    artificial mode vector, but then we also throw away any mode-coupling
    effects, ie. we zero all force components potentially populating other
    modes.

    All required transformations are done via the Normalmodes coordinates object
    anyway. What we hence do is we transform the cartesian input to normalmode
    space, evaluate potential energy and forces, and transform forces back to
    cartesian space.

    Initialization
    --------------
    normalmodes : Normalmodes Coordinates object from MDsim
        The coordinates object from a normalmode analysis. Bindings to external
        codes are work in progress.

    full_potential : Potential object from MDsim
        The "real" potential.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical use the procedure described
        above, where numerical forces invoke finite (central) differences in
        cartesian space.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Converts input for the forces stepwidth from Angstrom to a.u.. If
        <False> input in a.u..
    """

    def __init__(self,
                 normalmodes,
                 full_potential,
                 include_quanta=None,
                 forces_method='analytical',
                 forces_stepwidth=0.001,
                 convert=True):

        # we simply throw away all coupling terms
        self.coords = normalmodes

        self.Ndim = self.coords.get_Nmodes()

        self.full_potential = full_potential

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth
        self._convert = convert
        if self._convert:
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

    def get_Epots_modes(self, positions):
        """
        Return the potential energy contained in each mode.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions in cartesian space specifying the configuration.

        Returns
        -------
        Epots : (Nmodes=Natoms*Ndim,) array
            Potential energy contained within each mode.
        """
        # we simply throw away all cross terms... this is diagonal anharmonicity
        # ie. a pseudo 1D picture
        nm_pos = self.coords.positions_cartesian_to_normalmodes(positions)

        Epots = np.zeros(self.Ndim)
        for i in range(self.Ndim):
            # only one mode populated at a time
            _nm_pos = np.zeros(self.Ndim)
            _nm_pos[i] = nm_pos[i]
            Epots = self.full_potential.get_Epot(self.coords.positions_normalmodes_to_cartesian(_nm_pos))
        return Epots


    def get_Epot(self, positions):
        """
        Return the total potential energy.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions in cartesian space specifying the configuration.

        Returns
        -------
        Epot : float
            Potential energy contained in the system.
        """
        return np.sum(self.get_Epots_modes(positions))



    def get_forces(self, positions):
        """
        Routine to evaluate the forces originating from this potential.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions in cartesian space specifying the configuration.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces in cartesian space at the respective positions.
        """

        if self._forces_method == 'analytical':

            # we simply throw away all cross terms... this is diagonal anharmonicity
            # ie. a pseudo 1D picture
            nm_pos = self.coords.positions_cartesian_to_normalmodes(positions)

            nm_forces = np.zeros(self.Ndim)
            for i in range(self.Ndim):
                # only one mode populated at a time
                _nm_pos = np.zeros(self.Ndim)
                _nm_pos[i] = nm_pos[i]
                _nm_force = self.coords.gradient_cartesian_to_normalmodes(
                                self.full_potential.get_forces(
                                    self.coords.positions_normalmodes_to_cartesian(_nm_pos)
                                    )
                                )
                nm_forces[i] = _nm_force[i]

            return  self.coords.gradient_normalmodes_to_cartesian(nm_forces)

        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)


    def _create_infostring(self):
        info  = 'Diagonal anharmonic potential along normalmode axes'
        info += '\n\nFrequencies:'
        info += '\n------------'
        for i, o in enumerate(self.coords.get_omegas(_au=True)):
            info += '\n\tomega_{}'.format(i)
            info += ' = {:9.3E} a.u.'.format(o)
            info += ' = {:9.3f} meV'.format(o*units.AU_TO_EV*1000)
            info += ' = {:9.3f} cm^-1'.format(o*units.AU_TO_KAYSER)
            info += ' = {:9.3f} THz'.format(o*units.AU_TO_THZ)
        info += '\n'
        info += '\nForces method : {}'.format(self._forces_method)
        if self._forces_method == 'numerical':
            info += ' (forces stepwidth = {} Angstrom)'.format(self._forces_stepwidth*units.AU_TO_ANGSTROM)
        return info

    def __str__(self):
        return self._create_infostring()
