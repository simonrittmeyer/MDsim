# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

from MDsim import units
from MDsim.derivatives import gradient_central_differences
from MDsim.interpolation.univariatespline import UnivariateSpline

class UnivariatePotential(UnivariateSpline):
    def __init__(self, points, energies,
                 function='InterpolatedUnivariateSpline',
                 kind='cubic',
                 data_origin='None',
                 forces_method='numerical',
                 forces_stepwidth=0.001,
                 convert=True,
                 smoothing=None,
                 normalize=True,
                 _bounds_check=True):

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            # no in-place modification here
            points   = points * units.ANGSTROM_TO_AU
            energies = energies * units.EV_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

        # normalization
        if normalize:
            energies -= np.min(energies)

        UnivariateSpline.__init__(self, points=points,
                                        values=energies,
                                        function=function,
                                        kind=kind,
                                        smoothing=smoothing,
                                        data_origin=data_origin,
                                        _bounds_check=_bounds_check)

        self._name = "UnivariatePotential\n\t(interpolated potential)"

    def get_Epot(self, positions):
        """
        Evaluate the potential for the entire system specified via "positions".

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the potential. As there is no F90
            routine involved, have it 'C'-contigious.

        Returns
        -------
        energy : float
            The potential energy corresponding to <positions>.
        """
        return self.get_value(positions)[0]


    def get_forces(self, positions, method = None, d = 0.001):
        """
        Routine to evaluate the forces originating from this potential..

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the forces. As there is no F90
            routine involved, have it 'C'-contigious.

        Returns
        -------
        forces : (Natoms x Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        if self._forces_method == 'analytical':
            return -self.get_gradient(positions)

        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)

