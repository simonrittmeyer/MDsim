# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units
from MDsim.potentials import Potential

class DragPotential(Potential):
    """
    This is a drag force mimicking the force of a homogeneous field.

    Basically just implemented to test the Langevin integrators.
    """

    def __init__(self, drag_forces,
                       forces_method=None,
                       forces_stepwidth=None,
                       convert=True):
        # forces method and forces_stepwidth are dummies with no effect
        self.forces = drag_forces.copy()
        self._convert=convert
        if self._convert:
            self.forces *= units.EV_PER_ANGSTROM_TO_AU

    def get_forces(self, positions, method=None, d=None):
        # again, method and d are dummies
        return self.forces

    # it is just about the force
    def get_Epots(self, positions):
        return np.zeros_like(positions)

    def get_Epot(self, positions):
        return 0.
