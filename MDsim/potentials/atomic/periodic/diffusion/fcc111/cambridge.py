# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import copy
import numpy as np
from numpy import linalg

from MDsim import units

from MDsim.interpolation.fourier import FourierExpansion

from fourier import FourierPotentialFCC111

from MDsim.potentials.atomic.periodic.diffusion.cambridgepotential import CambridgePotential
from MDsim.coordinates.diffusion.fcc111 import create_sites
from MDsim.coordinates.diffusion.fcc111 import create_real_space_lattice
from MDsim.coordinates.diffusion.fcc111 import create_reciprocal_lattice


from MDsim.speedup import plainpython
try:
    from MDsim.speedup import f90support
    from MDsim.speedup.f90support import cambridge
    _f90 = True
except ImportError:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    from MDsim.speedup.numbasupport import cambridge
    _numba = True
except ImportError:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    from MDsim.speedup.cythonsupport import cambridge
    _cython = True
except ImportError:
    _cython = False


class DJWPotential(FourierPotentialFCC111):
    """
    This is a minimum basis Fourier potential to model diffusion on a hexagonal
    fcc(111) surface. It is basically just a two-component Fourier Potential
    as it is used most often by the Cambridge He-SE group. The hollow sites are
    necessarily degenerate for this potential and the bridge site is some
    interpolation value but no input. Hence, this potential is defined only via
    the top-hollow amplitude.

    The lattice vectors are defined as

        l1 = a * [ sin(2*pi/3), cos(2*pi/3) ]
        l2 = a * [0, 1]

    in cartesian coordinates, where 'a' is the surface lattice constant. For
    details, see the routine "create_real_space_lattice()" from
    MDsim.coordinates.diffusion.fcc111.

    By default, we  define the considered adsorption sites as

             'top'    = (0, 0)
             'bridge' = 0.5 * l1 or 0.5 * l2
             'hcp'    = 1./3. * l1 + 2./3. * l2
             'fcc'    = 2./3. * l1 + 1./3. * l2

    Note that for this potential, the hollow sites are degenerate.

    For details see the routine "create_sites()" from
    MDsim.coordinates.diffusion.fcc111.

    Yet, you may change it by passing a different "sites" dictionary upon
    initialization.


    Initialization
    --------------
    a : float
        The surface lattice constant, ie. a = d / sqrt(2) where 'd' is the
        fcc bulk lattice constant.

    p2 : float
        The scaling parameter. Still to be investigated.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/eV to a.u. or not. If set False, then
        input must be provided in a.u.
    """
    def __init__(self, a, p2,
                       forces_method = 'analytical',
                       forces_stepwidth = 0.001,
                       convert = True):

        # the real space surface lattice vectors
        self.a = a
        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            self.a *= units.ANGSTROM_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

        self.lattice = create_real_space_lattice(self.a)

        # we do not need H in this particular case
        G, H = create_reciprocal_lattice(self.a)

        # init the great grand-parent class
        # I am sorry python! This is ugly but quick'n'dirty...
        FourierExpansion.__init__(self,
                                  G=G,
                                  H=H,
                                  real_coeff=[1., p2],
                                  imag_coeff=[],
                                  const=None,
                                  data_origin=None,
                                  fit_type=None)

        # Name the thing
        self._name = 'DJWPotential for FCC(111) surface'
        self._name += '\n\tsurface lattice constant : {1:.3f} a.u. = {1:.3f} Angstrom'.format(self.a, self.a * units.AU_TO_ANGSTROM)

    def show_cuts(self):
        import matplotlib.pyplot as plt
        sites = create_sites(self.a)
        self._create_cut_plots(sites)
        plt.show()


class CambridgePotentialFCC111(CambridgePotential):
    """
    Simple fcc(100) potential that is most often used by the Cambridge group

    Functional form is

        V(x,y) = energy_site1 / 4. + energy_site2 /2
                + energy_site2/4 * [cos(2pi*x/a) + cos(2pi*y/a)]
                + (energy_site1 2*energy_site2)/4 * cos(2pi*x/a) * cos(2pi*y/a)

    Note that there is no interpretation of the site from the functional form.
    We have actually three sites:

    site1 : located at [0,0]
    site2 : located at [0.5,0] * a
    site3 : located at [0.5, 0.5] * a

    Site 3 is always == 0, the input energies of the other sites are with
    respect to the latter.


    Initialization
    --------------
    a : float
        The surface lattice constant, ie. a = d where 'd' is the fcc bulk
        lattice constant / sqrt(2).

    energies_dict : dictionary
        Dictionary that holds the energies of the sites specified above a
        values, and the site names {'site1', 'site2'} as keys.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/eV to a.u. or not. If set False, then
        input must be provided in a.u.

    Note
    ----
    Timings on my MacBook
    """
    _implemented_support = ['fortran',
                            'cython',
                            'numba',
                            'fallback']

    if _f90:
        _support = 'fortran'
    elif _cython:
        _support = 'cython'
    elif _numba:
        _support = 'numba'
    else:
        _support = 'fallback'

    _support='fallback'
    def __init__(self,
                 a,
                 height,
                 forces_method='analytical',
                 forces_stepwidth=0.001,
                 convert=True,
                 ):


        self.a = a
        self.height = height
        self._forces_method = forces_method.lower()


        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            self.a *= units.ANGSTROM_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU
            self.height *= units.EV_TO_AU

        self.lattice = create_real_space_lattice(self.a)
        self._forces_method = forces_method.lower()

        self._sites = create_sites(a)

        # coefficients to call the speedup routines
        self._coeff = {'a'  : self.a,
                       'height' : self.height}

    def __str__(self):
        # some verbosity
        name = 'Cambridge Fourier Potential for FCC(111) surface'
        name += '\n\tsurface lattice constant : {0:.3f} a.u. = {1:.3f} Angstrom'.format(self.a, self.a * units.AU_TO_ANGSTROM)
        name += '\n\tHeight of the potential  : {0:.6f} a.u. = {1:.3f} meV'.format(self.height, self.height * units.AU_TO_EV*1000)

        return name


    # wrappers for the speedup routines
    def _fortran_call(self, positions):
        """
        Routine to call the expansion on a Fortran level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions. This array is
            Fortran-contigioues, yet it does not matter for 1D arrays.
        """
        values = np.zeros(len(positions), dtype=np.float64, order='F')
        f90support.cambridge.evaluate_expansion_fcc111(positions.T, values, **self._coeff)
        return values


    def _cython_call(self, positions):
        """
        Routine to call the expansion on a Python level. Utilizing Cython
        extension.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.
        """
        return cythonsupport.cambridge.evaluate_expansion_fcc111(positions, **self._coeff)


    def _numba_call(self, positions):
        """
        Routine to call the expansion on a Python level. Plain python optimized
        with numba's JIT compilation capabilities.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.
        """
        return numbasupport.cambridge.evaluate_expansion_fcc111(positions, **self._coeff)


    def _python_call(self, positions):
        """
        Routine to call the expansion on a Python level. Plain python call.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.
        """
        return plainpython.cambridge.evaluate_expansion_fcc111(positions, **self._coeff)


    def _python_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        pure python level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        """
        return plainpython.cambridge.evaluate_gradient_fcc111(positions, **self._coeff)


    def _fortran_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        Fortran level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.
        """
        # gradients is supposed to be (Ndim x Natoms) upon input
        gradients = np.zeros(positions.shape[::-1], dtype=np.float64, order='F')
        f90support.cambridge.evaluate_gradient_fcc111(positions.T, gradients, **self._coeff)
        # transpose gradients upon output again
        return np.array(gradients.T, dtype=np.float64, order='C')


    def _cython_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        cython level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.
        """
        return cythonsupport.cambridge.evaluate_gradient_fcc111(positions, **self._coeff)


    def _numba_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        JIT compiled numba level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.
        """
        return numbasupport.cambridge.evaluate_gradient_fcc111(positions, **self._coeff)


    def show(self):
        """
        Visualize the potential.
        """
        import matplotlib.pyplot as plt
        plot_dict = self._create_lattice_plot()

        # call it a potential
        cbar = plot_dict['cbar']
        cbar.set_label(r'V(x,y) (a.u.)')
        plt.show()


    def _create_lattice_plot(self, **kwargs):
        """
        Simple function that creates a 2D surface plot of the expansion + the
        underlying lattice.

        This routine first calls "_create_contour_plot" and then adds some
        additional stuff.

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'lim' : (2 x 1) array
                    The plot limits in x and y direction.

                'repeats' : int
                    The number of repeating units of the unit cell

                'a' : float
                    The applied lattice constant

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot.

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot.
        """
        plot_dict = self._create_contour_plot(**kwargs)

        ax = plot_dict['ax']
        a = plot_dict['a']
        repeats = plot_dict['repeats']

        # the lattice
        for n in np.arange(-repeats, repeats+1):
            for m in np.arange(-repeats, repeats + 1):
                ax.plot(*((m*self.lattice[0]+n*self.lattice[1])),
                        marker = 'o',
                        markersize = 8,
                        color = 'lightgray',
                        markeredgecolor = 'black')

        # the unit cell
        for l in self.lattice:
            ax.arrow(0, 0, l[0], l[1],
                     head_width=0.1,
                     linewidth = 2,
                     length_includes_head = True,
                     head_length=0.1,
                     zorder = 5,
                     color='red')

        return plot_dict


    def _create_contour_plot(self, npts=300, surf_plot=True):
        """
        Simple function that creates a 2D surface plot of the expansion

        Parameters
        ----------
        npts : integer, optional (default=300)
            Number of points in x and y direction at which to evaluate the
            potential.

        surf_plot : bool, optional (default=True)
            Whether to plot an actual 2D surface plot (or just contour lines).

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'lim' : (2 x 1) array
                    The plot limits in x and y direction.

                'repeats' : int
                    The number of repeating units of the unit cell

                'a' : float
                    The applied lattice constant

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot (if surf_plot = <True>).

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot (if
                    surf_plot = <True>).
        """

        import matplotlib.pyplot as plt
        # get the range from the inverse of the reciprocal lattice vector

        repeats = 2
        lim = repeats * self.a * np.array([-1, 1])

        # creating the x/y points
        x = np.linspace(lim[0], lim[1], npts)
        y = np.linspace(lim[0], lim[1], npts)
        X, Y = np.meshgrid(x, y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        # evaluating using vectorized call ;)
        Z = self.get_Epots(points).reshape(npts, npts)

        # plotting
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        if surf_plot is True:
            cmap = plt.get_cmap("coolwarm")
            surf = ax.pcolormesh(X, Y, Z, cmap=cmap)
            # color bar
            cbar = plt.colorbar(surf, format="%.6f")
            cbar.set_label(r'f(x,y) (a.u.)')

        cont = ax.contour(X, Y, Z, colors='black')


        # axes limits and labels
        ax.set_xlim(*lim)
        ax.set_ylim(*lim)
        ax.set_xlabel('x (a.u.)')
        ax.set_ylabel('y (a.u.)')

        ax.set_aspect('equal')

        plot_dict = {'a' : self.a,
                     'lim' : lim,
                     'repeats' : repeats,
                     'ax' : ax,
                     'fig' : fig,
                     'cont' : cont
                    }

        if surf_plot:
            plot_dict.update({'surf' : surf,
                              'cbar' : cbar})

        return plot_dict

    def _create_cut_plots(self, sites, **kwargs):
        """
        Create some plots cutting along certain lines
        """
        import matplotlib.pyplot as plt

        # get some increments
        npts = 200
        alphas = np.linspace(0,1,npts)

        # distance from top to top over the bridge site
        _len = 2*self.a*np.cos(np.pi/6.)

        # positions along the cuts
        cuts = {# top -- fcc -- bridge -- hollow -- top
                'tfbht' : np.array([i*_len*np.array([1,0]) for i in alphas]),
                # top -- bridge -- top
                'tbt'   : np.array([i*self.lattice[0] for i in alphas]),
                # top -- fcc -- top
                'tft'   : np.array([i*sites['fcc'] for i in alphas[::2]]
                                 + [(1-i)*sites['fcc'] for i in alphas[::2]]),
                # top -- hcp -- top
                'tht'   : np.array([i*sites['hcp'] for i in alphas[::2]]
                                 + [(1-i)*sites['hcp'] for i in alphas[::2]])
                }

        labels = {'tfbht' : ['top', 'fcc', 'bridge', 'hollow', 'top'],
                  'tbt'   : ['top', 'bridge', 'top'],
                  'tft'   : ['top', 'fcc', 'top'],
                  'tht'   : ['top', 'hcp', 'top']}

        # positions of the labels
        labelpos = {'tfbht' : [0, sites['fcc'][0]/_len, .5, (sites['hcp'] + self.lattice[0])[0] / _len , 1],
                    'tbt'   : [0, .5, 1],
                    'tft'   : [0, .5, 1],
                    'tht'   : [0, .5, 1]}

        # evaluate the energy using the vectorized call ;)
        energy_cuts = dict()
        for key in cuts.keys():
            energy_cuts[key] = self.get_Epots(cuts[key])


        # plot the cuts
        maxE = max([max(e) for e in energy_cuts.values()])
        minE = min([min(e) for e in energy_cuts.values()])

        fig = plt.figure()

        for i,key in enumerate(cuts.keys()):
            ax_cut = fig.add_subplot(2, 2, i+1)
            energy = energy_cuts[key]
            ax_cut.plot(alphas, energy, color = 'blue')

            ax_cut.set_xticks(labelpos[key])
            ax_cut.set_xticklabels(labels[key], rotation=45)
            ax_cut.set_xlabel('position')
            ax_cut.set_ylabel('V(x,y) (a.u.)')

            ax_cut.set_ylim(minE, maxE)

            # make it a square plot
            x0,x1 =  ax_cut.get_xlim()
            y0,y1 =  ax_cut.get_ylim()

            ax_cut.set_aspect(abs(x1-x0)/abs(y1-y0))

    def show_cuts(self):
        import matplotlib.pyplot as plt
        self._create_cut_plots(self._sites)
        plt.show()


