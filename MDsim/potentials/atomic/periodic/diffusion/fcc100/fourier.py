# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Fourier expansion-based potential for fcc(100) surfaces
"""

from __future__ import print_function

import copy
import numpy as np

from MDsim import units

from MDsim.derivatives import gradient_central_differences
from MDsim.speedup import plainpython
from MDsim.interpolation.fourier import FourierExpansion
from MDsim.potentials.atomic.periodic.fourierpotential import FourierPotential

from MDsim.coordinates.diffusion.fcc100 import create_sites
from MDsim.coordinates.diffusion.fcc100 import create_real_space_lattice
from MDsim.coordinates.diffusion.fcc100 import create_reciprocal_lattice

try:
    from MDsim.speedup import f90support
    _f90 = True
except ImportError:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    _numba = True
except ImportError:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    _cython = True
except ImportError:
    _cython = False


class FourierPotentialFCC100(FourierPotential):
    """
    This is a Fourier potential to model diffusion on a rectangular fcc(100)
    surface.

    Initialization
    --------------
    a : float
        The surface lattice constant, ie. a = d / sqrt(2) where 'd' is the
        fcc bulk lattice constant.

    points : (Npoints x Ndim) array
        Numpy array containing all points that shall enter the interpolation or
        least square approximation of the potential.

    energies : (Npoints) array
        Energy vaules corresponding to the <points>.

    real_order : integer
        Order of the Fourier expansion in cosine contributions, ie. the real
        part.

    imag_order : integer
        Order of the Fourier expansions in sine contributions, ie. the
        imaginary part.

    constant : Boolean, optional (default = True)
        Flag wether a constant m=n=0 amplitude shall be fitted or not. Note
        that this functionality requires at least one more independent input
        point.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/eV to a.u. or not. If set False, then
        input must be provided in a.u.

    normalize : boolean, optional (default = True)
        Normalizes the energy values such that the lowest energy equal 0.0 meV.
    """
    def __init__(self, a, points, energies, real_order, imag_order,
                       constant = True, forces_method = 'analytical',
                       forces_stepwidth = 0.001, convert = True,
                       normalize = True):

        # the real space surface lattice vectors
        self.a = a

        if convert:
            self.a *= units.ANGSTROM_TO_AU

        self.lattice = create_real_space_lattice(self.a)

        G, H = create_reciprocal_lattice(self.a)

        # init the parent class
        FourierPotential.__init__(self, G = G,
                                        H = H,
                                        points = points,
                                        energies = energies,
                                        real_order = real_order,
                                        imag_order = imag_order,
                                        constant = constant,
                                        forces_method = forces_method,
                                        forces_stepwidth = forces_stepwidth,
                                        convert = convert,
                                        normalize = normalize)

        # Name the thing
        self._name = '2D Fourier Potential for FCC(100) surface'
        self._name += '\n\tsurface lattice constant : {0:.3f} a.u. = {1:.3f} Angstrom'.format(self.a, self.a * units.AU_TO_ANGSTROM)


    def _create_lattice_plot(self, **kwargs):
        """
        Simple function that creates a 2D surface plot of the expansion + the
        underlying lattice.

        This routine first calls "_create_contour_plot" and then adds some
        additional stuff.

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'lim' : (2 x 1) array
                    The plot limits in x and y direction.

                'repeats' : int
                    The number of repeating units of the unit cell

                'a' : float
                    The applied lattice constant

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot.

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot.
        """
        plot_dict = self._create_contour_plot(**kwargs)

        ax = plot_dict['ax']
        a = plot_dict['a']
        repeats = plot_dict['repeats']

        # the lattice
        for n in np.arange(-repeats, repeats+1):
            for m in np.arange(-repeats, repeats + 1):
                ax.plot(*((m*self.lattice[0]+n*self.lattice[1])),
                        marker = 'o',
                        markersize = 8,
                        color = 'lightgray',
                        markeredgecolor = 'black')

        # the unit cell
        for l in self.lattice:
            ax.arrow(0, 0, l[0], l[1],
                     head_width=0.1,
                     linewidth = 2,
                     length_includes_head = True,
                     head_length=0.1,
                     zorder = 5,
                     color='red')

        return plot_dict


    def _create_cut_plots(self, sites, **kwargs):
        """
        Create some plots cutting along certain lines
        """
        import matplotlib.pyplot as plt

        # get some increments
        npts = 200
        alphas = np.linspace(0,1,npts)

        # distance from top to top over the bridge site
        _len = 2*self.a*np.cos(np.pi/6.)

        # positions along the cuts
        cuts = {# top -- bridge -- top
                'tbt'   : np.array([i*self.lattice[0] for i in alphas]),
                # top -- fcc -- top
                'tht'   : np.array([i*sites['hollow'] for i in alphas[::2]]
                                 + [(1-i)*sites['hollow'] for i in alphas[::2]]),
                }

        labels = {'tbt'   : ['top', 'bridge', 'top'],
                  'tht'   : ['top', 'hollow', 'top']}

        # positions of the labels
        labelpos = {'tbt'   : [0, .5, 1],
                    'tht'   : [0, .5, 1]}

        # evaluate the energy using the vectorized call ;)
        energy_cuts = dict()
        for key in cuts.keys():
            energy_cuts[key] = self.get_Epots(cuts[key])


        # plot the cuts
        maxE = max([max(e) for e in energy_cuts.values()])
        minE = min([min(e) for e in energy_cuts.values()])

        fig = plt.figure()

        for i,key in enumerate(cuts.keys()):
            ax_cut = fig.add_subplot(2, 2, i+1)
            energy = energy_cuts[key]
            ax_cut.plot(alphas, energy, color = 'blue')

            ax_cut.set_xticks(labelpos[key])
            ax_cut.set_xticklabels(labels[key], rotation=45)
            ax_cut.set_xlabel('position')
            ax_cut.set_ylabel('V(x,y) (a.u.)')

            ax_cut.set_ylim(minE, maxE)

            # make it a square plot
            x0,x1 =  ax_cut.get_xlim()
            y0,y1 =  ax_cut.get_ylim()

            ax_cut.set_aspect(abs(x1-x0)/abs(y1-y0))


    def show(self):
        """
        Visualize the potential.
        """
        import matplotlib.pyplot as plt
        plot_dict = self._create_lattice_plot()

        # call it a potential
        cbar = plot_dict['cbar']
        cbar.set_label(r'V(x,y) (a.u.)')
        plt.show()


    def show_cuts(self, sites):
        import matplotlib.pyplot as plt
        self._check_site_names(sites)
        self._create_cut_plots(sites)
        plt.show()


class MinimumBasisFourierPotentialFCC100(FourierPotentialFCC100):
    """
    This is a minimum basis Fourier potential to model diffusion on a
    rectangular fcc(100) surface.  We basically use a single real coefficient
    and a constant.

    The lattice vectors are defined as

        l1 = a * [1, 0]
        l2 = a * [0, 1]

    in cartesian coordinates, where 'a' is the surface lattice constant. For
    details, see the routine "create_real_space_lattice()" from
    MDsim.coordinates.diffusion.fcc100.

    By default, we define the considered adsorption sites as
             'top'    = (0, 0)
             'bridge' = 0.5 * l1 or 0.5 * l2
             'hollow' = 0.5 * (l1 + l2)

    where li are real space lattice vectors.

    For details see the routine "create_sites()" from
    MDsim.coordinates.diffusion.fcc100.

    Yet, you may change it by passing a different "sites" dictionary upon
    initialization.


    Initialization
    --------------
    a : float
        The surface lattice constant, ie. a = d / sqrt(2) where 'd' is the
        fcc bulk lattice constant.

    energies_dict : dictionary
        Dictionary that holds the energies of the sites specified above a
        values, and the site names {'top', 'bridge', 'hollow'} as keys.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/eV to a.u. or not. If set False, then
        input must be provided in a.u.

    sites : dictionary, optional (default = None)
        Dictionary holding the positions of the respective adsorption sites as
        items in form of (2x1) arrays, and the site names {'top', 'bridge',
        'hollow'} as keys. If None is passed, the default definition from
        above applies.

    normalize : boolean, optional (default = True)
        Normalizes the energy values such that the lowest energy equal 0.0 meV.
    """

    _site_names = ['top', 'bridge', 'hollow']

    def __init__(self, a, energies_dict, forces_method = 'analytical',
                          forces_stepwidth = 0.001, convert = True,
                          sites = None, normalize = True):
        if sites is None:
            self._sites = create_sites(a)
        else:
            self._check_site_names(sites.keys())
            self._sites = sites

        self._check_site_names(energies_dict.keys())
        self._input_energies_dict = copy.deepcopy(energies_dict)

        if normalize:
            _min = np.min(self._input_energies_dict.values())
            for i in self._input_energies_dict.keys():
                self._input_energies_dict[i] -= _min

        # create the data for the fit, unit conversion is done in the parent class
        points = np.array([self._sites[n] for n in self._site_names], dtype = np.float64, order = 'C')
        energies = np.array([self._input_energies_dict[n] for n in self._site_names], dtype = np.float64, order = 'C')

        # fit the coefficients
        FourierPotentialFCC100.__init__(self, a = a,
                                              points = points,
                                              energies = energies,
                                              real_order = 1,
                                              imag_order = 0,
                                              constant = True,
                                              forces_method = forces_method,
                                              forces_stepwidth = forces_stepwidth,
                                              # has already been normalized
                                              normalize=False,
                                              convert = convert)

        # unit conversion
        if convert:
            for n in self._site_names:
                self._input_energies_dict[n] *= units.EV_TO_AU
                self._sites[n] *= units.ANGSTROM_TO_AU

        self._name = 'Minimum Basis 2D Fourier Potential for FCC(100) surface'
        self._name += '\n\tsurface lattice constant : {0:.3f} a.u. = {1:.3f} Angstrom'.format(self.a, self.a * units.AU_TO_ANGSTROM)
        self._name += '\n'
        self._name += '\nInput adsorption sites:'
        self._name += '\n-----------------------'
        for key, val in self._input_energies_dict.items():
            self._name += '\n\t{0:6s} : {1:+.6f} a.u. = {2:+.3f} meV'.format(key, val, val * units.AU_TO_EV*1000)


    def _check_site_names(self,names):
        """
        Check sites names that are used as keys in energies and sites dictionary

        Parameters
        ----------
        names : list of strings
            The keys of the respective dictionary.

        Returns
        -------
        <True> if everything is ok.

        Raises
        ------
        NotImplementedError : if some keys are invalid.
        """
        for n in names:
            if not n in self._site_names:
                print('Error : site name "{}" not recognized!'.format(n))
                print('        Must be one of "top", "bridge", "hollow".')
                raise NotImplementedError
        return True

    def show_cuts(self):
        import matplotlib.pyplot as plt
        self._create_cut_plots(self._sites)
        plt.show()



