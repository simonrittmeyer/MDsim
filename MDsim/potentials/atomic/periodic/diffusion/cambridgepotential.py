# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Potential that is most often used in Cambridge simulations
"""

from __future__ import print_function

import numpy as np
from MDsim import units

from MDsim.derivatives import gradient_central_differences
from MDsim.speedup import plainpython

try:
    from MDsim.speedup import f90support
    from MDsim.speedup.f90support import cambridge
    _f90 = True
except ImportError:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    from MDsim.speedup.numbasupport import cambridge
    _numba = True
except ImportError:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    from MDsim.speedup.cythonsupport import cambridge
    _cython = True
except ImportError:
    _cython = False


class CambridgePotential():
    """
    Framework for simple Cambridge potentials.
    """

    _implemented_support = ['fortran',
                            'cython',
                            'numba',
                            'fallback']

    if _f90:
        _support = 'fortran'
    elif _cython:
        _support = 'cython'
    elif _numba:
        _support = 'numba'
    else:
        _support = 'fallback'


    def __init__(self, *args, **kwargs):
        # needs to implement self._coeff
        raise NotImplementedError


    def get_Epot(self, positions):
        """
        Evaluate the potential for the entire system specified via "positions".

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential

        Returns
        -------
        energy : float
            The potential energy corresponding to <positions>.

        """
        return np.sum(self.get_Epots(positions))


    def get_Epots(self, positions):
        """
        Evaluate the potential at given positions and return each indiviudal
        atomic contribution to the potential energy.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        energies : (Natoms,) array
            The values at respective positions.

        """
        if self._support == 'fortran':
            return self._fortran_call(positions)
        elif self._support == 'cython':
            return self._cython_call(positions)
        elif self._support == 'numba':
            return self._numba_call(positions)
        elif self._support == 'fallback':
            return self._python_call(positions)

    # wrappers for the speedup routines
    def _fortran_call(self, positions):
        raise NotImplementedError

    def _cython_call(self, positions):
        raise NotImplementedError

    def _numba_call(self, positions):
        raise NotImplementedError

    def _python_call(self, positions):
        raise NotImplementedError

    def get_forces(self, positions):
        """
        Routine to evaluate the forces originating from this potential..

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the forces.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        if self._forces_method == 'analytical':
            return -self.get_gradient(positions)
        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)


    def get_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        """
        if self._support == 'fortran':
            return self._fortran_gradient(positions)
        elif self._support == 'cython':
            return self._cython_gradient(positions)
        elif self._support == 'numba':
            return self._numba_gradient(positions)
        elif self._support == 'fallback':
            return self._python_gradient(positions)


    def _python_gradient(self, positions):
        raise NotImplementedError

    def _fortran_gradient(self, positions):
        raise NotImplementedError

    def _cython_gradient(self, positions):
        raise NotImplementedError

    def _numba_gradient(self, positions):
        raise NotImplementedError

    def _change_support(self, support):
        """
        Change the applied speedup support.

        Parameters
        ----------
        support : string
            One of 'fortran', 'cython', 'numba', 'fallback'
        """
        if support not in self._implemented_support:
            raise NotImplementedError('Support "{}" not implemented'.format(support))

        self._support = support
