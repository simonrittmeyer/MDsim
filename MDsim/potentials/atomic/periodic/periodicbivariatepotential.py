# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

from MDsim import units
from MDsim.derivatives import gradient_central_differences
from MDsim.interpolation.bivariatespline.periodic.periodicatomicbivariatespline import PeriodicAtomicBivariateSpline

class PeriodicAtomicBivariatePotential(PeriodicAtomicBivariateSpline):
    """
    2D potential that is based on a bivariate spline interpolation. The actual
    degree of the splines can be chosen. Moreover, there are different spline
    routine available through SciPy, however you most-likely want to use the
    highly performant "RectBivariateSpline" routine. There are mainly three
    important member functions:

    get_Epots() : Vectorized call expecting a (Natoms x Ndim) positions array
                  and returning a (Natoms x 1) array of potential enery
                  values.

    get_Epot() : Vectorized call expecting a (Natoms x Ndim) positions array
                 and returning a single number being the potential energy of
                 the entire system.

    get_forces() : Vectorized call expecting a (Natoms x Ndim) positions array
                   and returning a (Natoms x Ndim) array of forces on each
                   particle.

    The actual evaluation is done on a F90 level, where SciPy does the
    interfacing though. So we do not need to care about contiousness of the
    arrays.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    energies : (Npoints) array
        energies corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    normalize : boolean, optional (default = True)
        Normalize energies such that lowest energy provided is zero.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    """

    def __init__(self, points, energies,
                 function = 'RectBivariateSpline',
                 kind = 'cubic',
                 data_origin = 'None',
                 forces_method = 'analytical',
                 forces_stepwidth = 0.001,
                 normalize = True,
                 convert = True
                 ):

        # normalization
        if normalize:
            energies -= np.min(energies)

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            points   *= units.ANGSTROM_TO_AU
            energies *= units.EV_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

        PeriodicAtomicBivariateSpline.__init__(self, points = points,
                                               values = energies,
                                               function = function,
                                               kind = kind,
                                               data_origin = data_origin)

        self._name  = "PeriodicAtomicBivariatePotential"
        self._name += "\n\t(interpolated potential with Born-von Karman PBC wrap"
        self._name += "\n\t of positions at interpolation boundaries)"


    def get_Epot(self, positions):
        """
        Evaluate the potential for the entire system specified via "positions".

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the potential. As there is no F90
            routine involved, have it 'C'-contigious.

        Returns
        -------
        energy : float
            The potential energy corresponding to <positions>.
        """
        return np.sum(self.get_Epots(positions))


    def get_Epots(self, positions):
        """
        Evaluate the potential at given positions and return each indiviudal
        atomic contribution to the potential energy.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the potential. As there is no F90
            routine involved, have it 'C'-contigious.

        Returns
        -------
        energies : (Natoms x 1) array
            The values at respective positions. This array is
            Fortran-contigious, yet it does not matter for 1D arrays. Please
            note that the entries in "energies" are a single number per atom!
            Splitting into contributions along coordinate axes is just not
            possible in general.
        """
        return self.get_value(positions)


    def get_forces(self, positions, method = None, d = 0.001):
        """
        Routine to evaluate the forces originating from this potential..

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the forces. As there is no F90
            routine involved, have it 'C'-contigious.

        method : string, optional ({*'analytical'*, 'numerical'})
            How to evaluate the forces. Analytical forces deduce the gradient
            from the analytical spline derrivatives, where numerical forces
            invoke finite (central) differences.

        d : float, optional (default = 0.001)
            Stepwidth in case of finite differences evaluation.
            When going for analytical forces, this flag is of no importance.

        Returns
        -------
        forces : (Natoms x Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        if self._forces_method == 'analytical':
            return -self.get_gradient(positions)
        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)

