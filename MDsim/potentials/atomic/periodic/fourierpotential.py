# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy import linalg

from MDsim import units
from MDsim.interpolation.fourier import FourierExpansion
from MDsim.derivatives import gradient_central_differences

class FourierPotential(FourierExpansion):
    """
    Class for a Fourier-expanded potential. This is basically just a wrapper
    around the respective FourierExpansion class, so many details can be found
    there. There are mainly three important member functions:

    get_Epots() : Vectorized call expecting a (Natoms x Ndim) positions array
                  and returning a (Natoms x 1) array of potential enery
                  values.

    get_Epot() : Vectorized call expecting a (Natoms x Ndim) positions array
                 and returning a single number being the potential energy of
                 the entire system.

    get_forces() : Vectorized call expecting a (Natoms x Ndim) positions array
                   and returning a (Natoms x Ndim) array of forces on each
                   particle.

    The actual evaluation of the Fourier expansion and its derrivative is done
    on a F90 level supported with BLAS routines to gain an optimal performance.

    The potential is expanded according to the one mentioned by Matt Probert in
    his (yet) unpublished manuscript. TODO: Add reference.

    Initialization
    --------------
    G : (Nlattice_G x Ndim) array
        Numpy array containing the reciprocal lattice vectors for the real part
        of the Fourier expansion, ie. the cosine terms. Please, do note the
        dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    H : (Nlattice_H x Ndim) array
        Numpy array containing the reciprocal lattice vectors for the imaginary
        part of the Fourier expansion, ie. the sine terms. Please, do note
        the dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    points : (Npoints x Ndim) array
        Numpy array containing all points that shall enter the interpolation or
        least square approximation of the potential.

    energies : (Npoints) array
        Energy vaules corresponding to the <points>.

    real_order : integer
        Order of the Fourier expansion in cosine contributions, ie. the real
        part.

    imag_order : integer
        Order of the Fourier expansions in sine contributions, ie. the
        imaginary part.

    constant : Boolean, optional (default = True)
        Flag wether a constant m=n=0 amplitude shall be fitted or not. Note
        that this functionality requires at least one more independent input
        point.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/eV to a.u. or not. If set False, then
        input must be provided in a.u.. Output will always be in a.u.

    normalize : boolean, optional (default = True)
        Normalizes the energy values such that the lowest energy equal 0.0 meV.

    Note
    ----
    If <real_order> + <imag_order> < Npoints, there will be a least square fit
    to the given datapoints. You will have an exact interpolation only if
    <real_order> + <imag_order> == Npoints. This is yet not recommended, as you
    usually will require very high frequencies that cause highly unphysical
    oscillations in the potential. Definietly check the quality of your fit
    before you apply it in any simulation.
    """
    def __init__(self, G, H, points, energies, real_order, imag_order,
                       constant = True, forces_method = 'analytical',
                       forces_stepwidth = 0.001, convert = True, normalize = True):

        # init the parent
        FourierExpansion.__init__(self, G, H)

        # fit coefficients and assign
        self._input_points = np.asarray(points, dtype = np.float64, order = 'C')
        self._input_energies = np.asarray(energies, dtype = np.float64, order = 'C')

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            self._input_points *= units.ANGSTROM_TO_AU
            self._input_energies *= units.EV_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

        if normalize:
            self._input_energies -= np.min(self._input_energies)

        self.fit(points = self._input_points,
                 values = self._input_energies,
                 real_order = real_order,
                 imag_order = imag_order,
                 constant = constant)

        self._name = 'Periodic potential based on a Fourier Expansion'


    def get_Epot(self, positions):
        """
        Evaluate the potential for the entire system specified via "positions".

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        energy : float
            The potential energy corresponding to <positions>.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        return np.sum(self.get_Epots(positions))


    def get_Epots(self, positions):
        """
        Evaluate the potential at given positions and return each indiviudal
        atomic contribution to the potential energy.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        energies : (Natoms x 1) array
            The values at respective positions. This array is
            Fortran-contigious, yet it does not matter for 1D arrays. Please
            note that the entries in "energies" are a single number per atom!
            Splitting into contributions along coordinate axes is just not
            possible in general.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        return self.get_value(positions)

    def get_forces(self, positions, method = 'analytical', d = 0.001):
        """
        Routine to evaluate the forces originating from this potential..

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the forces. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        forces : (Natoms x Ndim) array
            The forces at the respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """

        if self._forces_method == 'analytical':
            return -self.get_gradient(positions)
        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)
