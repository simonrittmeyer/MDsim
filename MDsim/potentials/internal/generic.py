# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

from MDsim import units
from MDsim.derivatives import gradient_central_differences
from MDsim.interpolation.bivariatespline.internal.generic import GenericDOFBivariateSpline


class GenericDOFBivariatePotential(GenericDOFBivariateSpline):
    """
    Potential that handles generic coordinates other than the usual cartesian
    (Natoms, Ndim) scheme, ie., generic DOF where each atom is only one
    dimension and a separate mass.

    Initialization
    --------------
    points : (Npoints, 2) array
        Numpy array containing all points that shall enter the interpolation.
        These are in internal coordinates!

    energies : (Npoints,) array
        energies corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    normalize : boolean, optional (default = True)
        Normalizes the energy values such that the lowest energy equal 0.0 meV.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """

    def __init__(self, points, energies, **kwargs):

        # potential-related
        if 'convert' not in kwargs.keys():
            kwargs['convert'] = True

        # leave it in the kwargs as we need it again for the coordinate
        # transfer
        convert = kwargs.pop('convert')

        normalize = kwargs.pop('normalize', True)
        forces_method = kwargs.pop('forces_method', 'analytical')
        forces_stepwidth =kwargs.pop('forces_stepwidth', 0.001)

        # normalization
        if normalize:
            # no inplace modification
            energies = energies - np.min(energies.copy())

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            # explicit copies here!
            points   = points * units.ANGSTROM_TO_AU
            energies = energies * units.EV_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU

        GenericDOFBivariateSpline.__init__(self,
                                           points=points,
                                           values=energies,
                                           **kwargs)

        self._name = "GenericDOFBivariatePotential\n\t(interpolated biariate spline-potential)"


    def get_Epot(self, positions):
        """
        Evaluate the potential for the system specified via "positions".

        Parameters
        ----------
        positions : (2,1) array
            Position at which to evaluate the potential

        Returns
        -------
        energy : float
            The potential energy corresponding to <positions>.
        """
        return self.get_value(positions)


    def get_forces(self, positions):
        """
        Routine to evaluate the forces originating from this potential..

        Parameters
        ----------
        positions : (2, 1) array
            Position at which to evaluate the forces.

        Returns
        -------
        forces : (2, 1) array
            The forces at the respective positions. This array is C-contigious.
        """

        if self._forces_method == 'analytical':
            return -self.get_gradient(positions)
        elif self._forces_method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, self._forces_stepwidth)
