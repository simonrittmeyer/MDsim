# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim import units

# pair potentials should be called a bit differently than PES potentials

class PairPotential(object):
    """
    Base class for a pairwise potential.

    This class is just a dummy that defines the interface!

    The intention is, that you create a PairwiseSystem class for each
    interaction potential. And that it is the system that initializes the
    PairPotential instance.

    Initialization
    --------------
    force_cutoff : float
        Cutoff distance for the force evaluation. If None, there will not be
        any cutoff in the evaluation (which can become problematic). Strictly
        speaking, the cutoff will be set to 1e20, so basically infinity ;)

    convert : booelan, optional (default = True)
        Whether to convert the cutoff distance from Angstrom to a.u. or not. If
        <False> input is expected in a.u.
    """
    def __init__(self, force_cutoff = None, convert = True):

        if force_cutoff is None:
            force_cutoff = 1e20

        self.force_cutoff = np.float64(force_cutoff)

        if convert:
            self.force_cutoff *= units.ANGSTROM_TO_AU

        self._initialized = False

    def calc_Epot_bruteforce(self, positions):
        raise NotImplementedError

    def calc_forces_bruteforce(self, positions):
        raise NotImplementedError

    def calc_Epot_neighborlist(self, positions, neighbor_lists, num_neighbors):
        raise NotImplementedError

    def calc_forces_neighborlist(self, positions, neighbor_lists, num_neighbors):
        raise NotImplementedError

    def calc_tail_correction(self):
        raise NotImplementedError
