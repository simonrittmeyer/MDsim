# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Implementation of the Kohn-Lau dipole-dipole interaction model on metal
surfaces.
"""

from __future__ import print_function
import numpy as np

from MDsim import units
from MDsim import tools
from MDsim.potentials.pairwise import PairPotential

from MDsim.speedup import plainpython

try:
    from MDsim.speedup import f90support
    _f90 = True
except ImportError:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    _numba = True
except ImportError:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    _cython = True
except ImportError:
    _cython = False


def topping_model(coverage, p0, alpha_p, n0, convert=True):
    """
    Function to evaluate the dipole moment as a function of the surface
    coverage according to Topping model[1].

    [1] J. Topping. Proc. R. Soc. A 114, 766 (1927).
        DOI: 10.1098/rspa.1927.0025.

    Contribution by Patrick Guetlein.

    Parameters
    ----------
    coverage : float
        Relative coverage with respect to one monolayer (ML).

    p0 : float
         Zero-coverage dipole moment. Unit is Debye if "convert" = <True>, else
         a.u..

    alpha_p : float
        Polarizability of the dipoles. Unit is Angstrom**3 if "convert" =
        <True>, else a.u.

    n0 : float
        Adatom density at 1 ML saturation coverage. Unit is 1/Angstrom**2 if
        "convert" = <True>, else a.u.

    convert : boolean, optional (default True)
        Convert the input from the specified units to a.u.

    Returns
    -------
    p : float
        Dipole moment (in a.u.) at the given coverage.
    """

    if convert is True:
        p0 *= units.DEBYE_TO_AU
        alpha_p *= units.ANGSTROM_TO_AU**3
        n0 *= units.ANGSTROM_TO_AU**-2

    p = p0 / (1. + 9. * alpha_p * (coverage * n0)**1.5)

    return p


class KohnLauPotential(PairPotential):
    """
    Class that implements a Kohn-Lau dipole potential [1] to model the interaction
    between charged adsorbates on a metal surface. Note that trunkating forces
    is only possible for Ndim < 3. For 3D systems we need to go for Ewald
    summation. But this is not yet implemented.

    The Kohn-Lau interaction dipol potential is given by:

        V_ij(r) = 2 * mu_i * mu_j / |r|**3

    where mu_i/j are the respective dipol moments and r is the distance vector
    between the dipoles. The factor 2 accounts for image-charge interactions.
    Nice explanations can be found in many papers by John Ellis.

    The respective forces are then given by:

        F_ij(r) = 6 * mu_i * mu_j * r / |r|**5

    Note that these are repulsive for dipole moments of the same sign.

    All forces and also the potential energy are evaluated on a F90-level to be
    able to cope with somewhat larger systems still within a reasonable time
    consumption. Yet, this requires to take a bit care of the 'F' vs. 'C'
    contigious array handling. However, it should all the hidden to the user
    and any routines from the outside.

    There is also a fallback C-implementation created with cython and a
    numba-based JIT compiled version. The implementation will try the following
    order (can be overwritten with the class _support variable). Automatic
    detection in the order F90 --> Cython --> Numba --> Fallback (numpy and
    python)


    NOTE: In order to use this potential, you will have to broadcast some
    system information. But in principle, this should be done within your
    system instance and is nothing you have to worry about in the context of MD
    simulations. If you want to use the potential without a system, however,
    you have to broadcast this information yourself via
    "broadcast_system_info()".

    References
    ----------
    [1] W. Kohn and K.-H. Lau, Solid State Commun. 18, 553 (1976)
        http://dx.doi.org/10.1016/0038-1098(76)91479-4

    """

    _implemented_support = ['fortran',
                            'cython',
                            'numba',
                            'fallback']


    if _f90:
        _support = 'fortran'
    elif _cython:
        _support = 'cython'
    elif _numba:
        _support = 'numba'
    else:
        _support = 'fallback'

    def __init__(self, force_cutoff, Natoms, Ndim, dipoles, unit_cell, convert=True):
        """
        Initialization
        --------------
        force_cutoff : float
            Cutoff distance for the force evaluation. If None, there will not
            be any cutoff in the evaluation (which can become problematic).
            Strictly speaking, the cutoff will be set to 1e20, so basically
            infinity ;)

        Natoms : integer
            Number of atoms in the system, ie. the first dimension of the
            obtained forces array.

        Ndim : integer
            Number of cartesian degrees of freedom per atom, ie. the second
            dimension of the obtained forces array. Can be either 1 or 2. 3D
            systems require Ewald summation which is not yet implemented.

        dipoles : (Natoms,) array
            Array specifiying the dipol moments of all atoms. If you want to
            change the dipol moments during the simulation, use
            "set_dipoles()". Input in a.u.

        unit_cell : (Ndim, Ndim) array
            The unit cell. This far, there is only support for orthorhombic
            cells.

        convert : boolean, optional (default = True)
            Whether to convert Angstrom and Debye to a.u. or not. If <False>,
            then input in a.u.
        """
        # first intialize the parent and the force_cutoff
        PairPotential.__init__(self,
                               force_cutoff=force_cutoff,
                               convert=convert)

        # further infos
        self.Natoms = Natoms
        self.Ndim = Ndim

        if self.Ndim not in [1, 2]:
            error = 'Kohn-Lau potential for Ndim = {} not implemented!'.format(self.Ndim)
            error += '\nNote that 3D dipolar interaction requires Ewald '
            error += '\nsummation which is not yet implemented.'
            raise NotImplementedError(error)

        self.set_dipoles(dipoles, _convert=convert)

        self.unit_cell = np.asarray(unit_cell, dtype=np.float64, order='C')

        tools.periodic.check_orthorhombic_cell(self.unit_cell)

        if convert:
            self.unit_cell *= units.ANGSTROM_TO_AU

        self._unit_cell_diag = np.diag(self.unit_cell)

        # our forces dump storage
        self._forces_F90 = np.zeros((Ndim, Natoms), dtype=np.float64, order='F')

        # the particle density
        self.rho = self.Natoms / np.prod(self._unit_cell_diag)


        self._initialized = True


    def _change_support(self, support):
        """
        Change the applied speedup support.

        Parameters
        ----------
        support : string
            One of 'fortran', 'cython', 'numba', 'fallback'
        """
        if support not in self._implemented_support:
            raise NotImplementedError('Support "{}" not implemented'.format(support))

        self._support = support

    def __str__(self):
        info = 'Kohn-Lau dipole interaction potential'
        return info


    def set_dipoles(self, dipoles, _convert=True):
        """
        Routine to set/change the dipole moments of the system.
        This is a routine that may be used from the outside as well, eg. if you
        want to change the dipol moments during your simulation.

        Parameters
        ----------
        dipoles : (Natoms,) array
            Array specifiying the dipol moments of all atoms. Input in a.u.

        convert : boolean, optional (default = True)
            Whether to convert from Debye to a.u. or  not. If <False>, input is
            expected in a.u.
        """

        self.dipoles = dipoles
        if _convert:
            self.dipoles *= units.DEBYE_TO_AU



    def calc_forces_bruteforce(self, positions):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces.

        This routine is brute force, ie. it accounts for all pairs in the unit
        cell and may thus be slow. Use the neighbor list version for speed-up.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        if self._support == 'fortran':
            return self._calc_forces_bruteforce_fortran(positions)
        elif self._support == 'cython':
            return self._calc_forces_bruteforce_cython(positions)
        elif self._support == 'numba':
            return self._calc_forces_bruteforce_numba(positions)
        elif self._support == 'fallback':
            return self._calc_forces_bruteforce_python(positions)
        else:
            raise ValueError('Unknown support "{}"'.format(self._support))


    def _calc_forces_bruteforce_python(self, positions):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses a plain
        python implementation.

        This routine is brute force, ie. it accounts for all pairs in the unit
        cell and may thus be slow. Use the neighbor list version for speed-up.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        return plainpython.kohnlau.calc_forces_bruteforce(positions=positions,
                                                          dipoles=self.dipoles,
                                                          unit_cell_diag=self._unit_cell_diag,
                                                          cutoff=self.force_cutoff)


    def _calc_forces_bruteforce_numba(self, positions):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses numba JIT
        compilation.

        This routine is brute force, ie. it accounts for all pairs in the unit
        cell and may thus be slow. Use the neighbor list version for speed-up.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        return numbasupport.kohnlau.calc_forces_bruteforce(positions=positions,
                                                           dipoles=self.dipoles,
                                                           unit_cell_diag=self._unit_cell_diag,
                                                           cutoff=self.force_cutoff)

    def _calc_forces_bruteforce_cython(self, positions):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Uses cython
        extensions.

        This routine is brute force, ie. it accounts for all pairs in the unit
        cell and may thus be slow. Use the neighbor list version for speed-up.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        return cythonsupport.kohnlau.calc_forces_bruteforce(positions=positions,
                                                            dipoles=self.dipoles,
                                                            unit_cell_diag=self._unit_cell_diag,
                                                            cutoff=self.force_cutoff)


    def _calc_forces_bruteforce_fortran(self, positions):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces. Makes use of an
        F90 implementation.

        This routine is brute force, ie. it accounts for all pairs in the unit
        cell and may thus be slow. Use the neighbor list version for speed-up.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """

        f90support.kohnlau.kohnlau.calc_forces_bruteforce(positions=positions.T,
                                                          dipoles=self.dipoles,
                                                          unit_cell_diag=self._unit_cell_diag,
                                                          cutoff=self.force_cutoff,
                                                          forces=self._forces_F90)

        return np.asarray(self._forces_F90.T, dtype=np.float64, order='C')

    def calc_forces_neighborlist(self,
                                 positions,
                                 neighbor_lists,
                                 num_neighbors):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces.

        This routine uses neighbor lists as created with the Neighbors class
        and yields a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        if self._support == 'fortran':
            return self._calc_forces_neighborlist_fortran(positions=positions,
                                                          neighbor_lists=neighbor_lists,
                                                          num_neighbors=num_neighbors)
        elif self._support == 'cython':
            return self._calc_forces_neighborlist_fortran(positions=positions,
                                                          neighbor_lists=neighbor_lists,
                                                          num_neighbors=num_neighbors)
        elif self._support == 'numba':
            return self._calc_forces_neighborlist_fortran(positions=positions,
                                                          neighbor_lists=neighbor_lists,
                                                          num_neighbors=num_neighbors)
        elif self._support == 'fallback':
            return self._calc_forces_neighborlist_python(positions=positions,
                                                         neighbor_lists=neighbor_lists,
                                                         num_neighbors=num_neighbors)
        else:
            raise ValueError('Unknown support "{}"'.format(self._support))


    def _calc_forces_neighborlist_fortran(self,
                                          positions,
                                          neighbor_lists,
                                          num_neighbors):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces using a F90
        implementation.

        This routine uses neighbor lists as created with the Neighbors class
        and yields a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        f90support.kohnlau.kohnlau.calc_forces_neighbor(positions=positions.T,
                                                        dipoles=self.dipoles,
                                                        unit_cell_diag=self._unit_cell_diag,
                                                        cutoff=self.force_cutoff,
                                                        neighbor_lists=neighbor_lists.T,
                                                        num_neighbors=num_neighbors,
                                                        forces=self._forces_F90)
        # copies are cheap... well, more or less but there is no other way
        return np.asarray(self._forces_F90.T, dtype=np.float64, order='C')


    def _calc_forces_neighborlist_python(self,
                                         positions,
                                         neighbor_lists,
                                         num_neighbors):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces using a plain
        python implementation.

        This routine uses neighbor lists as created with the Neighbors class
        and yields a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        return plainpython.kohnlau.calc_forces_neighbor(positions=positions,
                                                        dipoles=self.dipoles,
                                                        unit_cell_diag=self._unit_cell_diag,
                                                        cutoff=self.force_cutoff,
                                                        neighbor_lists=neighbor_lists,
                                                        num_neighbors=num_neighbors)


    def _calc_forces_neighborlist_numba(self,
                                        positions,
                                        neighbor_lists,
                                        num_neighbors):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces using numba's JIT
        compilation.

        This routine uses neighbor lists as created with the Neighbors class
        and yields a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        return numbasupport.kohnlau.calc_forces_neighbor(positions=positions,
                                                         dipoles=self.dipoles,
                                                         unit_cell_diag=self._unit_cell_diag,
                                                         cutoff=self.force_cutoff,
                                                         neighbor_lists=neighbor_lists,
                                                         num_neighbors=num_neighbors)


    def _calc_forces_neighborlist_cython(self,
                                         positions,
                                         neighbor_lists,
                                         num_neighbors):
        """
        Routine to evaluate pairwise Kohn-Lau dipole forces using a cython
        implementation.

        This routine uses neighbor lists as created with the Neighbors class
        and yields a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.
        """
        return cythonsupport.kohnlau.calc_forces_neighbor(positions=positions,
                                                          dipoles=self.dipoles,
                                                          unit_cell_diag=self._unit_cell_diag,
                                                          cutoff=self.force_cutoff,
                                                          neighbor_lists=neighbor_lists,
                                                          num_neighbors=num_neighbors)


    def calc_Epot_bruteforce(self, positions):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions.

        This is a brute force approach and thus rather slow.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """
        if self._support == 'fortran':
            return self._calc_Epot_bruteforce_fortran(positions)
        elif self._support == 'cython':
            return self._calc_Epot_bruteforce_cython(positions)
        elif self._support == 'numba':
            return self._calc_Epot_bruteforce_numba(positions)
        elif self._support == 'fallback':
            return self._calc_Epot_bruteforce_python(positions)
        else:
            raise ValueError('Unknown support "{}"'.format(self._support))


    def _calc_Epot_bruteforce_fortran(self, positions):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. F2PY wrapped Fortran extension.

        This is a brute force approach and thus rather slow.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """

        Epot = f90support.kohnlau.kohnlau.calc_energy_bruteforce(positions=positions.T,
                                                                 dipoles=self.dipoles,
                                                                 unit_cell_diag=self._unit_cell_diag,
                                                                 cutoff=self.force_cutoff)
        return Epot

    def _calc_Epot_bruteforce_python(self, positions):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Plain python implementation.

        This is a brute force approach and thus rather slow.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """

        return plainpython.kohnlau.calc_energy_bruteforce(positions=positions,
                                                          dipoles=self.dipoles,
                                                          unit_cell_diag=self._unit_cell_diag,
                                                          cutoff=self.force_cutoff)

    def _calc_Epot_bruteforce_numba(self, positions):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Implementation based on numba's JIT compilation.

        This is a brute force approach and thus rather slow.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """

        return numbasupport.kohnlau.calc_energy_bruteforce(positions=positions,
                                                           dipoles=self.dipoles,
                                                           unit_cell_diag=self._unit_cell_diag,
                                                           cutoff=self.force_cutoff)


    def _calc_Epot_bruteforce_cython(self, positions):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Uses cython extensions.

        This is a brute force approach and thus rather slow.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """

        return cythonsupport.kohnlau.calc_energy_bruteforce(positions=positions,
                                                            dipoles=self.dipoles,
                                                            unit_cell_diag=self._unit_cell_diag,
                                                            cutoff=self.force_cutoff)


    def calc_Epot_neighborlist(self, positions, neighbor_lists, num_neighbors):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions.

        This routine uses neighbor lists as created with the Neigbors class and
        thus can yield a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """
        if self._support == 'fortran':
            return self._calc_Epot_neighborlist_fortran(positions=positions,
                                                        neighbor_lists=neighbor_lists,
                                                        num_neighbors=num_neighbors)
        elif self._support == 'cython':
            return self._calc_Epot_neighborlist_fortran(positions=positions,
                                                        neighbor_lists=neighbor_lists,
                                                        num_neighbors=num_neighbors)
        elif self._support == 'numba':
            return self._calc_Epot_neighborlist_fortran(positions=positions,
                                                        neighbor_lists=neighbor_lists,
                                                        num_neighbors=num_neighbors)
        elif self._support == 'fallback':
            return self._calc_Epot_neighborlist_python(positions=positions,
                                                       neighbor_lists=neighbor_lists,
                                                       num_neighbors=num_neighbors)
        else:
            raise ValueError('Unknown support "{}"'.format(self._support))


    def _calc_Epot_neighborlist_fortran(self, positions, neighbor_lists, num_neighbors):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Uses an efficient F90 implementation of the core routine.

        This routine uses neighbor lists as created with the Neigbors class and
        thus can yield a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """

        Epot = f90support.kohnlau.kohnlau.calc_energy_neighbor(positions=positions.T,
                                                               dipoles=self.dipoles,
                                                               unit_cell_diag=self._unit_cell_diag,
                                                               cutoff=self.force_cutoff,
                                                               neighbor_lists=neighbor_lists.T,
                                                               num_neighbors=num_neighbors)
        return Epot


    def _calc_Epot_neighborlist_python(self, positions, neighbor_lists, num_neighbors):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Uses a very slow plain python implementation.

        This routine uses neighbor lists as created with the Neigbors class and
        thus can yield a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.
        """

        return plainpython.kohnlau.calc_energy_neighbor(positions=positions,
                                                        dipoles=self.dipoles,
                                                        unit_cell_diag=self._unit_cell_diag,
                                                        cutoff=self.force_cutoff,
                                                        neighbor_lists=neighbor_lists,
                                                        num_neighbors=num_neighbors)


    def _calc_Epot_neighborlist_numba(self, positions, neighbor_lists, num_neighbors):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Uses numba JIT compilation.

        This routine uses neighbor lists as created with the Neigbors class and
        thus can yield a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.

        Raises
        ------
        ValueError : If system information have not yet been broadcasted.
        """

        return numbasupport.kohnlau.calc_energy_neighbor(positions=positions,
                                                         dipoles=self.dipoles,
                                                         unit_cell_diag=self._unit_cell_diag,
                                                         cutoff=self.force_cutoff,
                                                         neighbor_lists=neighbor_lists,
                                                         num_neighbors=num_neighbors)


    def _calc_Epot_neighborlist_cython(self, positions, neighbor_lists, num_neighbors):
        """
        Routine to evaluate the potential energy due to pairwise Kohn-Lau pair
        interactions. Uses a cython implementation.

        This routine uses neighbor lists as created with the Neigbors class and
        thus can yield a quite decent speedup.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        neighbor_lists : (Natoms, Natoms) array
            Neighbor lists as created with the Neighbors instance. Note that
            although these are (Natom x Natom) arrays, only some entries are
            actually meaningful. This is controlled by "num_neighbors", ie. the
            next positional parameter.

        num_neighbors : (Natoms,) array
            Array specifying the number on neighbors of all atoms, ie.,
            num_neighbors[i] specifies the number of neighbors of atom <i>.
            This also implies that only the entries
            neighbor_lists[i,0:num_neighbors(i)] are meaningful entries in the
            neighbor lists.

        Returns
        -------
        Epot : float
            The potential energy. No tail correction involved.

        Raises
        ------
        ValueError : If system information have not yet been broadcasted.
        """

        return cythonsupport.kohnlau.calc_energy_neighbor(positions=positions,
                                                          dipoles=self.dipoles,
                                                          unit_cell_diag=self._unit_cell_diag,
                                                          cutoff=self.force_cutoff,
                                                          neighbor_lists=neighbor_lists,
                                                          num_neighbors=num_neighbors)

    @staticmethod
    def call_potential(mu_i, mu_j, dist):
        """
        Ealuate the potential for a given distance for a given pair of dipoles.
        No optimization required.

        Parameters
        ----------
        mu_i : float
            Dipole moment of atom i.

        mu_j : float
            Dipole moment of atom j.

        dist : float
            Distance between atom i and j.

        Returns
        -------
        energy : float
            The Kohn-Lau dipol-dipol interaction energy.
        """
        return 2.0 * mu_i * mu_j /  dist**3


    @staticmethod
    def call_force(mu_i, mu_j, distance):
        """
        Ealuate the potential for a given distance for a given pair of dipoles.
        No optimization required.

        Parameters
        ----------
        mu_i : float
            Dipole moment of atom i.

        mu_j : float
            Dipole moment of atom j.

        distance : float
            Distance *vector* between atom i and j.

        Returns
        -------
        energy : float
            The Kohn-Lau dipol-dipol interaction energy.
        """
        dist = np.linalg.norm(distance)
        return 6.0 * mu_i * mu_j * distance / dist**5


    def calc_tail_correction(self):
        """
        Evaluate the tail correction for this pair potential in 1D

            V_tail = Natoms * rho / 2 * int_rc^infty dr V_ij(r) * 2
                   = Natoms * rho * mu**2 / rc**2

        and 2D :

            V_tail = Natoms * rho / 2 * int_rc^infty dr V_ij(r) 2 * pi * r
                   = 2 * pi * Natoms * rho * mu**2 / rc

        Obviously, this only works out if all dipoles are the same. Note that
        in 3D trunkation of dipole interactions does not converge and is thus
        not possible.

        Moreover, we assume that the radial distribution function g(r) = 1 for
        r > rc. See Frenkel/Smit, chap. 3.2 for more details.
        """

        if not np.all([self.dipoles[0] == d for d in self.dipoles]):
            raise NotImplementedError('Tail correction only implemented for homogeneous system of dipoles')

        if self.Ndim == 1:
            V_tail = 2 * self.Natoms * self.rho * self.dipoles[0]**2 / self.force_cutoff**2

        elif self.Ndim == 2:
            V_tail = 2 * np.pi * self.Natoms * self.rho * self.dipoles[0]**2 / self.force_cutoff

        else:
            raise ValueError('Tail correction does not converge for Ndim = {}'.format(self.Ndim))

        return V_tail

