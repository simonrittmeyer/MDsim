# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.derivatives import gradient_central_differences

class Potential(object):
    """
    This class defines the interface for any potential. It basically has to
    provide two functions: get_Epot and get_forces. That's all...
    """

    def get_Epot(self, positions):
        return 0.

    def get_forces(self, positions, method ='analytical', d = 0.001):
        if method == 'analytical':
            raise NotImplementedError
        elif method == 'numerical':
            return -gradient_central_differences(self.get_Epot, positions, d)


