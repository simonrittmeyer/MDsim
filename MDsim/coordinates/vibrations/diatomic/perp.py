# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
This is the coordinate transformations necessary for diatomic adsorbates that
are oriented along the z coordinate, assuming that the substrate is within the
xy plane.

The internal coordinates used in this case are the centre of mass of the
diatomic as well as the bonding distance. Note that within Cartesian space,
only the z-coordinate of each atom is used. The distance will be defined such
that it is positive in the equilibrium configuration.
"""

from __future__ import print_function

import numpy as np
from MDsim import units

# plain python at first, if necessary, we will go for F90 extensions

class InternalCoordsDiatomicPerp(object):
    def __init__(self, masses, positions_eq=None, convert=True):

        if positions_eq is None:
            self.positions_eq = np.zeros((2, 1), order='C', dtype=np.float64)
        else:
            self.positions_eq = np.array(positions_eq, order='C', dtype=np.float64)

        self.masses = np.array(masses, order='C', dtype=np.float64)
        self._convert = convert

        if self._convert:
            self.masses *= units.AMU_TO_AU
            self.positions_eq *= units.ANGSTROM_TO_AU

        # sanity checks
        self.Natoms, self.Ndim = self.positions_eq.shape

        if self.Natoms != 2:
            msg = 'These coordinate transformations are for diatomics only'
            raise ValueError(msg)

        if self.Ndim != 1:
            msg = 'These coordinate transformations only accept one DOF per atom'
            raise ValueError(msg)

        if self.masses.shape[0] != 2:
            msg = 'These coordinate transformations are for diatomics only'
            raise ValueError(msg)

        # we define the distance at equilibrium to be positive
        if self.positions_eq[0] > self.positions_eq[1]:
            sign = 1
        else:
            sign = -1

        # the transformation matrix from cartesian --> internal
        Q = np.zeros((2,2), dtype = np.float64, order='C')
        Q[0,0] = self.masses[0] / np.sum(self.masses)
        Q[0,1] = self.masses[1] / np.sum(self.masses)
        Q[1,0] = 1. * sign
        Q[1,1] = -1. * sign

        self.Q = Q

        # this is the back-transformation internal --> cartesian
        Qinv = np.zeros((2,2), dtype = np.float64, order='C')
        Qinv[0,0] = 1.
        Qinv[0,1] = self.masses[1] / np.sum(self.masses) * sign
        Qinv[1,0] = 1.
        Qinv[1,1] = -self.masses[0] / np.sum(self.masses) * sign

        self.Qinv = Qinv

        # the Jacobian we need to transform forces from internal --> cartesian
        self.jacobian = self.Q.T

    def positions_cartesian_to_internals(self, positions):
        """
        Transformation from Carthesian coordinates to the internal coordinates.
        For this reason we define the centre of mass of the diatomic as

            Rcom = sum_i(masses[i], positions[i,0]) / sum_i(masses[i])

        and the bonding distance

            Rdist = sign*(positions[0,0] - positions[1,0])

        where "sign" is chosen such that the equilibrium bonding distance is
        positive. Note that we assume that each atom does only have one
        Cartesian degree of freedom, ie. the z-valu perpendicular to the
        surface.

        Parameters
        ----------
        positions : (2,1) array
            The Cartesian positions.


        Returns
        -------
        internals : (2,) array
            The internal positions, where internals[0]=Rcom and
            internals[1]=dist.

        """
        return np.dot(self.Q, (positions - self.positions_eq).ravel())

    def positions_internals_to_cartesian(self, internals):
        return np.dot(self.Qinv, internals).reshape(2,1) + self.positions_eq

    def gradient_internal_to_cartesian(self, forces):
        return np.dot(self.jacobian, forces).reshape(2,1)
