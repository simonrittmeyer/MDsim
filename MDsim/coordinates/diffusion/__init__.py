# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to create lattices and coordinates for diffusion.
"""
from __future__ import print_function

import os
import numpy as np

from MDsim import tools
from MDsim import units

# # just a nice list to check the surfaces we have implemented
# implemented_surfaces = []
# for f in os.listdir(os.path.abspath(os.path.dirname(__file__))):
    # if f.endswith('.py') and not f == '__init__.py':
        # implemented_surfaces.append(f.replace('.py', ''))

# no, register it manually
implemented_surfaces = ['fcc111',
                        'fcc100']

def _create_surface_supercell(unit_cell,
                              Natoms_surface,
                              coverage,
                              surface_lattice_constant=None,
                              saturation_coverage=1.,
                              sites=None,
                              surface_facet=None,
                              fix_Natoms=None,
                              fix_Ncells=None,
                              squarify=True,
                              _verbose=True):
    """
    Function to create a rectangular 2-dimensional supercell, based on an
    input unit cell and coverage. The size of supercell is chosen by
    a fixed number of atoms or repetitions of the input unit cell. Note that
    this routine only accepts orthorhombic unit cells as input.

    We will try to create a cell that is as close to a square as possible. Note
    that there is not unit conversion whatsoever.

    Contribution by Patrick Guetlein (revised by Simon P. Rittmeyer).

    Parameters
    ----------
    unit_cell : (2,2) array
        The *orthorhombic* unit cell. If the cell is not orthorhombic, an error
        will be raised.

    Natoms_surface : int
        Number of surface atoms per unit cell as given by the size of the
        cell vector.

    coverage : float
        Relative coverage of the supercell with respect to full (saturation)
        coverage, ie.  one monolayer (ML). Must be in [0,1].

    surface_lattice_constant : float, optional (default=None)
        The surface lattice constant. Is just passed for convenience and will
        be stored in output dictionary.

    surface_facet : string, optional (default=None)
        The surface facet actually handled.

    saturation_coverage : float, optional (default= '1.')
        Constant multiplication factor for the coverage to account for
        different definition of coverages wrt. surface atoms. Note that
        "saturation_coverage" = <1> results in a "true" saturation coverage,
        where we have on adsorbate per surface atom. This may yet not be
        physical.

    sites : dictionary, optional (default=None)
        Dictionary holding possible high-symmetry sites.

    fix_Natoms : int, optional (default=None)
        Number of atoms, that shall be contained in the final supercell.

    fix_Ncells : int, optional (default=None)
        Number of repetitions of the input unit cell for the total
        supercell

    squarify : bool, optional (default=True)
        Try to return a super cell that is a close to a square as possible. If
        <False>, the form of the unit_cell is retained.

    _verbose : bool, optional (default=True)
        Print some information to stdout.

    Returns
    -------
    output : dictionary
        Dictionary containing the following keys
            "surface_lattice_constant" : float
                The surface lattice constant (if passed upon initialization)
            "sites"
                Dictionary with coordinates of high symmetry sites (if passed
                upon initialization).
            "surface_facet" : string
                The surface facet (if passed upon initialization).
            "supercell" : (2, 2) array
                The actual super cell vectors.
            "Natoms" : int
                The number of atoms in the supercell required for the coverage.
            "Ncells" : int
                The number of unit_cells included in the super cell.
            "cell_factors" : (2,) array
                The number of repetitions of the unit cell along the cell
                vectors.
            "target_coverage" : float
                The input coverage.
            "true_coverage" : float
                The coverage that is actually produced with the output.
            "unit_cell" : (2, 2) array
                The unit cell used.
            "saturation_coverage" : float
                The saturation coverage applied.
    """

    # sanity checks
    _true_count = np.count_nonzero([i is None for i in [fix_Natoms, fix_Ncells]])
    if _true_count != 1:
        msg = 'Chose *one and only one* of "fix_Natoms" and "fix_Ncells"!'
        raise ValueError(msg)

    # now check the unit cell
    unit_cell = np.asarray(unit_cell, dtype=float, order='C')
    tools.periodic.check_orthorhombic_cell(unit_cell)

    _unit_cell_diag = np.diagonal(unit_cell)

    if _verbose:
        print('Creating surface supercell ', end='')
    # initialization of system by number of cells or atoms
    if fix_Natoms is not None:
        if _verbose:
            print('(fixed number of atoms : {})'.format(fix_Natoms))
        Natoms_adsorbate = fix_Natoms
        Ncells = int(round(Natoms_adsorbate / (coverage * Natoms_surface *
                                               saturation_coverage)))

    elif fix_Ncells is not None:
        if _verbose:
            print('(fixed number of unit cells per supercell : {})'.format(fix_Ncells))
        Ncells = fix_Ncells
        Natoms_adsorbate = int(np.round(coverage * Ncells *
                                        Natoms_surface * saturation_coverage))

    # get the rough ratio of cell lengths (the number of cells will be
    # distributed accordingly) -- no required to obtain a strict square but
    # close.
    if squarify:
        _cell_ratio = np.round(_unit_cell_diag[0] / _unit_cell_diag[1],
                               decimals=1)
    else:
        _cell_ratio = 1.

    # get elongation of unit cell to supercell factorization
    _cell_factors = np.array([int(np.round(np.sqrt(Ncells / _cell_ratio))),
                              int(np.round(np.sqrt(Ncells * _cell_ratio)))])

    Ncells_true = np.prod(_cell_factors)

    # setup up final supercell with positions of atoms
    supercell = _cell_factors[:, None] * unit_cell

    # "True" coverage of final system
    true_coverage = Natoms_adsorbate / (Ncells_true * float(Natoms_surface) * saturation_coverage)

    if _verbose:
        print('\tCreated surface coverage : {:.4E}'.format(true_coverage))
        print('\tTarget surface coverage  : {:.4E}'.format(coverage))
        print('\t------------------------')
        print('\tDeviation                : {:.2f}%'.format(100*abs(1- true_coverage/coverage)))

    output = {'supercell' : supercell,
              'Natoms' : Natoms_adsorbate,
              'sites' : sites,
              'surface_lattice_constant' : surface_lattice_constant,
              'surface_facet' : surface_facet,
              'Ncells' : Ncells_true,
              'cell_factors' : _cell_factors,
              'target_coverage' : coverage,
              'true_coverage' : true_coverage,
              'saturation_coverage' : saturation_coverage,
              'unit_cell' : unit_cell}

    return output


# wrapper routine that maps to surfaces
def create_real_space_lattice(surface_facet, *args, **kwargs):
    """
    Wrapper that maps to the respective surface-specififc functions.
    """
    if surface_facet == 'fcc100':
        from fcc100 import create_real_space_lattice as _create_real_space_lattice
    elif surface_facet == 'fcc111':
        from fcc111 import create_real_space_lattice as _create_real_space_lattice
    else:
        raise NotImplementedError('Unknown surface facet "{}"'.format(surface_facet))

    return _create_real_space_lattice(*args, **kwargs)


def create_rectangular_real_space_lattice(surface_facet, *args, **kwargs):
    """
    Wrapper that maps to the respective surface-specififc functions.
    """
    if surface_facet == 'fcc100':
        from fcc100 import create_rectangular_real_space_lattice as _create_rectangular_real_space_lattice
    elif surface_facet == 'fcc111':
        from fcc111 import create_rectangular_real_space_lattice as _create_rectangular_real_space_lattice
    else:
        raise NotImplementedError('Unknown surface facet "{}"'.format(surface_facet))

    return _create_rectangular_real_space_lattice(*args, **kwargs)


def create_surface_supercell(surface_facet, *args, **kwargs):
    """
    Wrapper that maps to the respective surface-specififc functions.
    """
    if surface_facet == 'fcc100':
        from fcc100 import create_surface_supercell as _create_surface_supercell
    elif surface_facet == 'fcc111':
        from fcc111 import create_surface_supercell as _create_surface_supercell
    else:
        raise NotImplementedError('Unknown surface facet "{}"'.format(surface_facet))

    return _create_surface_supercell(*args, **kwargs)


def create_surface_direction(surface_facet, *args, **kwargs):
    """
    Wrapper that maps to the respective surface-specific functions.
    """
    if surface_facet == 'fcc100':
        from fcc100 import create_surface_direction as _create_surface_direction
    elif surface_facet == 'fcc111':
        from fcc111 import create_surface_direction as _create_surface_direction
    else:
        raise NotImplementedError('Unknown surface facet "{}"'.format(surface_facet))
    return _create_surface_direction(*args, **kwargs)


def create_reciprocal_lattice(surface_facet, *args, **kwargs):
    """
    Wrapper that maps to the respective surface-specific functions.
    """
    if surface_facet == 'fcc100':
        from fcc100 import create_reciprocal_lattice as _create_reciprocal_lattice
    elif surface_facet == 'fcc111':
        from fcc111 import create_reciprocal_lattice as _create_reciprocal_lattice
    else:
        raise NotImplementedError('Unknown surface facet "{}"'.format(surface_facet))

    return _create_reciprocal_lattice(*args, **kwargs)


def create_sites(surface_facet, *args, **kwargs):
    """
    Wrapper that maps to the respective surface-specific functions.
    """
    if surface_facet == 'fcc100':
        from fcc100 import create_sites as _create_sites
    elif surface_facet == 'fcc111':
        from fcc111 import create_sites as _create_sites
    else:
        raise NotImplementedError('Unknown surface facet "{}"'.format(surface_facet))

    return _create_sites(*args, **kwargs)


def distribute_initial_positions(super_cell_dict, initial_positions, verbose=True, _random_seed=None):
    """
    Distribute initial positions in a supercell. Either completely random or
    randomly distributed over high-symmetry sites.

    Parameters
    ----------
    super_cell_dict : dictionary
        Dictionary as returned from the respective "create_super_cell()"
        routine. Contains all necessary coordinates informations.

    initial_positions : str
        Either "random" or any of the high-symmetry sites available for the
        surface facet.

    verbose : boolean, optional (default=True)
        Print information to stdout.

    _random_seed : integer, optional (default=None)
        Seed for numpy's random number generator.


    Returns
    -------
    positions : (Natoms, 2) array
        The initial positions, distributed accordingly.
    """
    initial_positions = initial_positions.lower()

    Natoms = super_cell_dict['Natoms']
    Ndim = 2

    # initialize the random number generator
    if _random_seed is None:
        from MDsim.tools import randomize_seed
        _random_seed = randomize_seed()

    np.random.seed(_random_seed)

    if not initial_positions in super_cell_dict['sites'].keys() + ['random', 'origin']:
        msg = 'Unsupported site "{}" for surface facet "{}"'.format(initial_positions,
                                                                    super_cell_dict['surface_facet'])
        raise ValueError(msg)

    elif initial_positions == 'random':
        from MDsim.tools.periodic import Distances
        if verbose:
            print('\tAssigning random initial positions')

        while True:
            # need the copy here due to deprecated warnings
            positions = np.random.rand(Natoms, Ndim) * np.diag(super_cell_dict['supercell']).copy()[None,:]

            # check if any atoms may be too close
            # compare only lower triangle without the diagonal
            if np.all(Distances().calc_distances_mic(positions, super_cell_dict['supercell'])[np.tril_indices(Natoms, -1)]
                      >= super_cell_dict['surface_lattice_constant']):
                break

    elif initial_positions == 'origin':
        if verbose:
            print('\tAll initial positions will be at the origin')
            positions = np.zeros((Natoms, Ndim))

    else:
        offset = super_cell_dict['sites'][initial_positions]

        # all possible combinations of lattice vectors
        positions_pool = np.empty((super_cell_dict['Ncells'], Ndim))
        rectangular_lattice = super_cell_dict['unit_cell']

        _n = 0
        for i in np.arange(super_cell_dict['cell_factors'][0]):
            for j in np.arange(super_cell_dict['cell_factors'][1]):
                positions_pool[_n,:] = i*rectangular_lattice[0] + j*rectangular_lattice[1] + offset
                _n += 1

        if verbose:
            print('\tDistributing initial positions over {} sites'.format(initial_positions))

        # now draw Natoms samples... "ohne Zuruecklegen"
        idx = np.random.choice(super_cell_dict['Ncells'], Natoms, replace=False)
        positions = positions_pool[idx]

    return positions
