# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to allow for a consistent definition of surface coordinates in the
context of diffusion. Note that these units apply for fcc(100) surfaces.
"""
from __future__ import print_function

import numpy as np

from MDsim.coordinates.diffusion import _create_surface_supercell


def create_real_space_lattice(a):
    """
    Create the real space lattice vectors for a fcc(100) surface.

    The lattice vectors are defined as

        l1 = a * [1, 0]
        l2 = a * [0, 1]

    in cartesian coordinates, where 'a' is the surface lattice constant.


    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(100) surface, ie. just the bulk
        lattice constant / sqrt(2).

    Returns
    -------
    lattice : (2, 2) array
        The real-space lattice vectors. First index gives vector, second gives
        cartesian components.
    """
    lattice = a * np.array([[1,0],
                            [0,1]], dtype = np.float64, order = 'C')

    return lattice


def create_rectangular_real_space_lattice(a):
    """
    Create two rectangular real-space lattice vectors for the fcc(100) surface
    unit cell. For this surface, this is just a wrapper but anyway we keep it
    for convenience.

    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(100) surface, ie bulk
        lattice constant / sqrt(2).

    Returns
    -------
    lattice : (2, 2) array
        The real-space lattice vectors. First index gives vector, second gives
        cartesian components.
    """
    return create_real_space_lattice(a=a)


def create_reciprocal_lattice(a):
    """
    Create the reciprocal space lattice vectors.

    These are defined as

        g1 = gamma * [ 1, 0 ]
        g2 = gamma * [ 0, 1 ]

    where

        gamma = 2*pi / a

    and 'a' is the real-space surface lattice constant. The vectors gi are
    collrected in matrix G and are used for the real-part of the Fourier
    expansion, whereas the hi are collected in H and form the basis for the
    imaginary part.


    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(111) surface, ie bulk lattice
        constant / sqrt(2).

    Returns
    -------
    G : (3 x 2) array
        The reciprocal lattice vectors for the real part of the Fourier
        expansion.

    H : (3 x 2) array
        The reciprocal lattice vectors for the imaginary part of the Fourier
        expansion.
    """

    #construct the reciprocal lattice vectors according to MJP
    gamma = (2.*np.pi) / a

    G = gamma * np.array([[1., 0.],
                          [0., 1.]], dtype = np.float64, order = 'C')
    H = G.copy()
    return G, H


def create_sites(a):
    """
    Function that creates a dictionary with the coordinates of the top, bridge,
    hcp and fcc sites.

    By default, we define the considered adsorption sites as
             'top'    = (0, 0)
             'bridge' = 0.5 * l1 or 0.5 * l2
             'hollow' = 0.5 * (l1 + l2)

    where li are real space lattice vectors as obtained from
    "create_real_space_lattice()".

    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(100) surface, ie the
        bulk lattice constant / sqrt(2).

    Returns
    -------
    sites : dictionary
        Dictionary holding the positions of the respective adsorption sites as
        items in form of (2x1) arrays, and the site names {'top', 'bridge',
        'hollow'} as keys.
    """

    lattice = create_real_space_lattice(a)
    sites = {'top'    : 0.0 * lattice[0],
             'bridge' : 0.5 * lattice[0],
             'hollow' : 0.5 * (lattice[0] + lattice[1]),
             }

    return sites


def create_surface_supercell(a, coverage, **kwargs):
    """
    Function that creates a surface super cell for an fcc(111) surface, based
    on an input coverage. The size of supercell is chosen by a fixed number of
    atoms or repetitions of the input unit cell. Note that this routine only
    accepts orthorhombic unit cells as input.

    We will try to create a cell that is as close to a square as possible. Note
    that there is not unit conversion whatsoever.

    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(100) surface, ie just the bulk
        lattice constant / sqrt(2).

    coverage : float
        Relative coverage of the supercell with respect to full (saturation)
        coverage, ie.  one monolayer (ML). Must be in [0,1].

    saturation_coverage : float, optional (default= '1.')
        Constant multiplication factor for the coverage to account for
        different definition of coverages wrt. surface atoms. Note that
        "saturation_coverage" = <1> results in a "true" saturation coverage,
        where we have on adsorbate per surface atom. This may yet not be
        physical.

    fix_Natoms : int, optional (default=None)
        Number of atoms, that shall be contained in the final supercell.

    fix_Ncells : int, optional (default=None)
        Number of repetitions of the input unit cell for the total
        supercell

    squarify : bool, optional (default=True)
        Try to return a super cell that is a close to a square as possible. If
        <False>, the form of the unit_cell is retained.

    _verbose : bool, optional (default=True)
        Print some information to stdout.

    Returns
    -------
    output : dictionary
        Dictionary containing the following keys
            "surface_lattice_constant" : float
                The surface lattice constant.
            "sites"
                Dictionary with coordinates of high symmetry sites.
            "surface_facet" : string
                The surface facet.
            "supercell" : (2, 2) array
                The actual super cell vectors.
            "Natoms" : int
                The number of atoms in the supercell required for the coverage.
            "Ncells" : int
                The number of unit_cells included in the super cell.
            "cell_factors" : (2,) array
                The number of repetitions of the unit cell along the cell
                vectors.
            "target_coverage" : float
                The input coverage.
            "true_coverage" : float
                The coverage that is actually produced with the output.
            "unit_cell" : (2, 2) array
                The unit cell used.
            "saturation_coverage" : float
                The saturation coverage applied.
    """

    unit_cell = create_rectangular_real_space_lattice(a)
    return _create_surface_supercell(unit_cell=unit_cell,
                                     Natoms_surface=1,
                                     coverage=coverage,
                                     surface_lattice_constant=a,
                                     surface_facet='fcc100',
                                     sites=create_sites(a=a),
                                     **kwargs)


implemented_directions = ['[110]','[100]']

def create_surface_direction(direction, switch_hollow=False):
    """
    Function that returns a vector of the specified direction along the
    fcc(100) surface. The coordinate system as defined in
    "create_real_space_lattice" is adapted.

    Parameters
    ----------
    direction : string
        The requested direction along the fcc(100) surface. Implemented this
        far:
            * '[110]' (along the x-axis)
            * '[100]' (along the diagonal)
    Returns:
    --------
    surface_direction : (2,) array
        The normalized vector describing the direction along the fcc(100)
        surface.

    Note
    ----
    For definition of surface directions see

        A. P. Graham et al., Phys. Rev. B 56, 10567 (1997)
        http://www.dx.doi.org/10.1103/PhysRevB.56.10567
    """

    if direction not in implemented_directions:
        raise NotImplementedError('direction "{}" not (yet) implemented!'.format(direction))

    # it is about direction, not length
    vec1, vec2 = create_real_space_lattice(a=1.)

    if direction == '[110]':
        surface_dir = vec1
    elif direction == '[100]':
        surface_dir = vec1 + vec2

    return surface_dir/np.linalg.norm(surface_dir)


implemented_sites = create_sites(a=1).keys()
