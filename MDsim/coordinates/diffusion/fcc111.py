# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to allow for a consistent definition of surface coordinates in the
context of diffusion. Note that these units apply for fcc(111) surfaces.
"""
from __future__ import print_function

import numpy as np

from MDsim.coordinates.diffusion import _create_surface_supercell

def create_real_space_lattice(a):
    """
    Create the real space lattice vectors for a fcc(111) surface.
    Here, we create the ones that correspond to the reciprocal lattice vectors
    of MJP's implementation, ie. with an angle of 120 degree.

    The lattice vectors are defined as

        l1 = a * [ sin(2*pi/3), cos(2*pi/3) ]
        l2 = a * [0, 1]

    in cartesian coordinates, where 'a' is the surface lattice constant.


    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(111) surface, ie bulk
        lattice constant / sqrt(2).

    Returns
    -------
    lattice : (2, 2) array
        The real-space lattice vectors. First index gives vector, second gives
        cartesian components.
    """
    lattice = a * np.array([[np.sin(2*np.pi/3.), np.cos(2*np.pi/3.)],
                            [0,1]], dtype = np.float64, order = 'C')

    return lattice


def create_rectangular_real_space_lattice(a):
    """
    Create two rectangular real-space lattice vectors for the fcc(111) surface
    unit cell. We adapt the definition from "create_real_space_lattice"

        l1 = a * [ sin(2*pi/3), cos(2*pi/3) ]
        l2 = a * [0, 1]

    and define

        lr1 = 2*l1 + l2
        lr2 = l2

    in cartesian coordinates, where 'a' is the surface lattice constant. Note
    that this is not the minimal surface unit cell.

    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(111) surface, ie bulk
        lattice constant / sqrt(2).

    Returns
    -------
    lattice : (2, 2) array
        The real-space lattice vectors. First index gives vector, second gives
        cartesian components.
    """
    lattice = create_real_space_lattice(a)
    lattice[0, :] += lattice[0, :] + lattice[1, :]

    # ok, make them really zero
    lattice[0, 1] = lattice[1, 0] = 0.
    return lattice


def create_reciprocal_lattice(a):
    """
    Create the reciprocal space lattice vectors according to MJP's
    implementation. This will be three lattice vectors each, but different ones
    for the real and imaginary part of the Fourier series.

    These are defined as

        g1 = gamma * [ 1, 0 ]
        g2 = gamma * [ cos(pi/3), sin(pi/3) ]
        g3 = gamma * [ -cos(pi/3), sin(pi/3) ]

    and

        h1 = gamma * [ 1, 0 ]
        h2 = gamma * [ -cos(pi/3), -sin(pi/3) ]
        h3 = gamma * [ -cos(pi/3), sin(pi/3) ]

    where

        gamma = 4*pi / (sqrt(3)*a)

    and 'a' is the real-space surface lattice constant. The vectors gi are
    collrected in matrix G and are used for the real-part of the Fourier
    expansion, whereas the hi are collected in H and form the basis for the
    imaginary part.


    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(111) surface, ie bulk lattice
        constant / sqrt(2).

    Returns
    -------
    G : (3 x 2) array
        The reciprocal lattice vectors for the real part of the Fourier
        expansion.

    H : (3 x 2) array
        The reciprocal lattice vectors for the imaginary part of the Fourier
        expansion.
    """

    #construct the reciprocal lattice vectors according to MJP
    gamma = (4.*np.pi) / (np.sqrt(3) * a)

    G = gamma * np.array([[1., 0.],
                          [np.cos(np.pi/3.), np.sin(np.pi/3.)],
                          [-np.cos(np.pi/3.), np.sin(np.pi/3.)]
                          ], dtype = np.float64, order = 'C')
    H = G.copy()
    H[1] *= -1

    return G, H

def create_sites(a, switch_hollow = False):
    """
    Function that creates a dictionary with the coordinates of the top, bridge,
    hcp and fcc sites.

    By default, we define the considered adsorption sites as
             'top'    = (0, 0)
             'bridge' = 0.5 * l1 or 0.5 * l2
             'hcp'    = 1./3. * l1 + 2./3. * l2
             'fcc'    = 2./3. * l1 + 1./3. * l2

    where li are real space lattice vectors as obtained from
    "create_real_space_lattice()". The positions of the hollow sites can be
    switched via <switch_hollow> = True. This is arbitrary anyway, as the 2D
    periodicity per se does not resolve different hollow sites. This is rather
    provoked by the z-coordinate which we do not consider. Yet, by using a
    imaginary lattice vector, we can distinguish between both sites.

    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(111) surface, ie
        bulk lattice constant / sqrt(2).

    switch_hollow : Boolean, optional (default = False)
        Switches the positions of the hollow sites.

    Returns
    -------
    sites : dictionary
        Dictionary holding the positions of the respective adsorption sites as
        items in form of (2x1) arrays, and the site names {'top', 'bridge',
        'hcp', 'fcc'} as keys.
    """

    lattice = create_real_space_lattice(a)
    sites = {'top'    : 0.0 * lattice[0],
             'bridge' : 0.5 * lattice[0],
             'hcp'    : 1./3. * lattice[0] + 2./3. * lattice[1],
             'fcc'    : 2./3. * lattice[0] + 1./3. * lattice[1]
             }

    if switch_hollow:
        sites['hcp'] = 2./3. * lattice[0] + 1./3. * lattice[1]
        sites['fcc'] = 1./3. * lattice[0] + 2./3. * lattice[1]

    return sites


def create_surface_supercell(a, coverage, **kwargs):
    """
    Function that creates a surface super cell for an fcc(111) surface, based
    on an input coverage. The size of supercell is chosen by a fixed number of
    atoms or repetitions of the input unit cell. Note that this routine only
    accepts orthorhombic unit cells as input.

    We will try to create a cell that is as close to a square as possible. Note
    that there is not unit conversion whatsoever.

    Contribution by Patrick Guetlein (revised by Simon P. Rittmeyer).

    Parameters
    ----------
    a : float
        Surface lattice constant of the fcc(111) surface, ie bulk
        lattice constant / sqrt(2).

    coverage : float
        Relative coverage of the supercell with respect to full (saturation)
        coverage, ie.  one monolayer (ML). Must be in [0,1].

    saturation_coverage : float, optional (default= '1.')
        Constant multiplication factor for the coverage to account for
        different definition of coverages wrt. surface atoms. Note that
        "saturation_coverage" = <1> results in a "true" saturation coverage,
        where we have on adsorbate per surface atom. This may yet not be
        physical.

    fix_Natoms : int, optional (default=None)
        Number of atoms, that shall be contained in the final supercell.

    fix_Ncells : int, optional (default=None)
        Number of repetitions of the input unit cell for the total
        supercell

    squarify : bool, optional (default=True)
        Try to return a super cell that is a close to a square as possible. If
        <False>, the form of the unit_cell is retained.

    _verbose : bool, optional (default=True)
        Print some information to stdout.

    Returns
    -------
    output : dictionary
        Dictionary containing the following keys
            "surface_lattice_constant" : float
                The surface lattice constant.
            "sites"
                Dictionary with coordinates of high symmetry sites.
            "surface_facet" : string
                The surface facet.
            "supercell" : (2, 2) array
                The actual super cell vectors.
            "Natoms" : int
                The number of atoms in the supercell required for the coverage.
            "Ncells" : int
                The number of unit_cells included in the super cell.
            "cell_factors" : (2,) array
                The number of repetitions of the unit cell along the cell
                vectors.
            "target_coverage" : float
                The input coverage.
            "true_coverage" : float
                The coverage that is actually produced with the output.
            "unit_cell" : (2, 2) array
                The unit cell used.
            "saturation_coverage" : float
                The saturation coverage applied.
    """

    unit_cell = create_rectangular_real_space_lattice(a)
    return _create_surface_supercell(unit_cell=unit_cell,
                                     Natoms_surface=2,
                                     surface_lattice_constant=a,
                                     sites=create_sites(a=a),
                                     surface_facet='fcc111',
                                     coverage=coverage,
                                     **kwargs)


# implemented_directions = ['[10-1]',
#                           '[1-10]',
#                           '[-101]',
#                           '[01-1]' ,
#                           '[1-21]',
#                           '[-1-12]']

# adapting Cambrdige notation
implemented_directions = ['[110]', '[11-2]']

def create_surface_direction(direction, switch_hollow=False):
    """
    Function that returns a vector of the specified direction along the
    fcc(111) surface. The coordinate system as defined in
    "create_real_space_lattice" is adapted.

    Contribution by Patrick Guetlein.

    Parameters
    ----------
    direction : string
        The requested direction along the fcc(111) surface. Implemented this
        far:
            * '[110]' (short direction: top-bridge-top)
            * '[11-2'] (long direction: top-hcp/fcc-bridge-fcc/hcp-top)

        ***OBSOLETE/***
            * '[10-1]' (top-bridge-top, fcc on left-hand side)
            * '[1-10]' (top-bridge-top, fcc on right-hand side)
            * '[-101]' (top-bridge-top, fcc on right-hand side)
            * '[01-1]' (top-bridge-top, fcc on right-hand side)
            * '[1-21]' (top-fcc-bridge-hcp-top)
            * '[-1-12]' (top-hcp-bridge-fcc-top)
        ***/OBSOLETE***

        Note that some of these energetically degenerate.

    switch_hollow : boolen, optional (default = False)
        Switch the position of hcp and fcc site in the unit cell. This is
        effectively a rotation of the surface direction by 30 degree.

    Returns:
    --------
    surface_direction : (2,) array
        The normalized vector describing the direction along the fcc(111)
        surface.

    Note
    ----
    For definition of surface directions see

        http://surface-science.uni-graz.at/techniques/pix/cleanRh_model.jpg
    """

    if direction not in implemented_directions:
        raise NotImplementedError('direction "{}" not (yet) implemented!'.format(direction))

    # it is about direction, not length
    vec1, vec2 = create_real_space_lattice(a=1.)

# we have changed to Cambridge notation
# with version 3.4.4
    if direction == '[110]':
        surface_dir = vec2
    elif direction == '[11-2]':
        surface_dir = 2*vec1 + vec2
#     if direction == '[10-1]':
#         surface_dir = vec2
#     elif direction == '[1-10]':
#         surface_dir = vec1 + vec2
#     elif direction == '[-101]':
#         surface_dir = -vec2
#     elif direction == '[01-1]':
#         surface_dir = - vec1
#     elif direction == '[1-21]':
#         surface_dir = 2*vec1 + vec2
#     elif direction == '[-1-12]':
#         surface_dir = vec1 - vec2

    if switch_hollow:
        # rotate by some 30 degree
        alpha = np.pi/6.
        rotation_matrix = np.array([[np.cos(alpha), -np.sin(alpha)],
                                    [np.sin(alpha), np.cos(alpha)]])
        surface_dir = rotation_matrix.dot(surface_dir)

    return surface_dir/np.linalg.norm(surface_dir)



implemented_sites = create_sites(a=1).keys()
