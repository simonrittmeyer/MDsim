# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
This is a module handling the coordinate transformations in the context of
propagating a full non-diagonal friction tensor. It is very similar to the
normalmodes module.

Nomenclature
============
In this case, things are relatively simple. Consider a friction matrix Eta in
*mass-weighted* cartesian coordinates. Then we can find a transformation matrix L
such that

    Q^T.Eta.Q = Gamma                                                   (1)

such that Gamma is a diagonal matrix of frictional eigenvalues. The
transformation matrix Q thus consists of the eigenvectors of Eta, i.e. the
so-called frictionmodes. We here assume that Q is made up of the eigenvectors
as column vectors, see here:

    https://en.wikipedia.org/wiki/Symmetric_matrix#Decomposition.

The friction tensor is real-symmetric by construction, hence Q is an ORTHOGONAL
MATRIX such that

    Q^T.L = Q.Q^T = 1 and hence Q^-1 = Q^T.                             (2)

We are interested in transforming cartesian positions/velocities into the basis
of friction modes. This is straightforward if we are famiiliar with the
normalmode formalism.

The Carthesian velocity vector v can be transformed into the basis of
mass-weighted Carthesian coordinates using the transformation matrix M, i.e.

    Msqrt.v = y.                                                        (3)

Where, y is the respective vector in mass-weighted Carthesian coordinates. The
corresponding transformation matrix Msqrt is of diagonal structure with

    Msqrt_ij = sqrt(m_i) * delta_ij,                                    (4)

where m_i is the mass associated with coordinate i, and delta_ij is the
Kronecker delta.

The transformation matrix Q transforms the vector from mass-weighted Carthesian
coordinates into the basis of (mass-weighted) frictionmodes:

    Q^T.y = q.                                                          (5)

The direct transformation from carthesian to (mass-weighted) frictionmode
basis corresponds to a successive application of M and Q

    Q^T.(M.v) = Q^T.y = q.                                              (7)

Hence, defining the matrix Uinv := Q^T.M one can write

    Uinv.v = q.                                                         (8)

Note that Uinv is in general not orthogonal.
"""

# linear algebra
import numpy as np

# unit conversion
from MDsim import units
from MDsim import tools

class FrictionModes(object):
    """
    This is a module handling the coordinate transformations in the context of
    propagating a full non-diagonal friction tensor.

    Initialization
    --------------
    friction_tensor_mw : (Natoms*Ndim, Natoms*Ndim) array
        The full non-diagonal friction tensor. Please note that we have
        flattened the (Natoms,Ndim) dimension for convenience.
        Unit is fs^-1, if convert = <True>, else it is a.u.

    masses : (Natoms,) array
        The masses associated with each atom. Required to yield a proper
        convertion from mass-weighted space to cartesian space.
        Unit is AMU, if convert = <True>, else it is a.u.

    convert : boolean, optional (default=True)
        Do unit conversion.
    """

    def __init__(self, friction_tensor_mw, masses, convert=True):
        # make a real copy!
        if not tools.is_square_matrix(friction_tensor_mw):
            raise ValueError('Input tensor is not a square matrix')

        self.friction_tensor_mw = np.array(friction_tensor_mw, order='C', dtype=np.float64)
        self.masses = np.array(masses, order='C', dtype=np.float64)

        self.Nmodes = self.friction_tensor_mw.shape[0]
        self.Natoms = self.masses.shape[0]
        self.Ndim = self.Nmodes / self.Natoms

        self._convert = convert

        if self._convert:
            # no in-place modifications here
            self.friction_tensor_mw = self.friction_tensor_mw / units.FS_TO_AU
            self.masses = self.masses * units.AMU_TO_AU

        # the the eigenvectors of the friction matrix
        self.eigenvals, self.Q = np.linalg.eigh(self.friction_tensor_mw)

        self.QT = self.Q.copy().T
        self.gamma_mw = np.dot(self.QT, self.friction_tensor_mw.dot(self.Q))

        # cartesian --> mass weighted carthesian
        self.Msqrt = np.diag(np.repeat(np.sqrt(self.masses), self.Ndim))

        # mass-weighted cartesian --> cartesian
        self.Msqrtinv = np.diag(1./ np.diag(self.Msqrt))

        # THIS MATRIX IS NOT ORTHOGONAL!
        # frictionmodes --> cartesian
        self.U  = np.dot(self.Msqrtinv, self.Q)

        # cartesian --> friction modes
        self.Uinv = np.dot(self.QT, self.Msqrt)

    # -----------------------
    # transformation matrices
    # -----------------------

    def get_U(self, _au=False):
        """
        Returns the transformation matrix U as build from the frictionmodes in
        Carthesian Coordinates. This matrix is not orthogonal!
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.U
        else:
            return self.U / np.sqrt(units.AU_TO_AMU)


    def get_Uinv(self, _au=False):
        """
        Returns the inverse of the transformation matrix U as build from the
        frictionmodes in Carthesian Coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Uinv
        else:
            return self.Uinv * np.sqrt(units.AU_TO_AMU)

    def get_Q(self, _au=False):
        """
        Returns the transformation matrix Q as build from the frictionmodes
        in mass weighted coordinates. This matrix IS orthogonal.
        If au = True, output in atomic units.
        """
        # matrix is dimensionless
        return self.Q


    def get_QT(self, _au=False):
        """
        Returns the inverse of the transformation matrix Q (a.k.a. the
        transpose, since Q is orthogonal) as build from the frictionmodes
        in mass weighted coordinates.
        If au = True, output in atomic units.
        """
        return self.QT

    def get_Msqrt(self, _au=False):
        """
        Returns the transformation matrix from Carthesian coordinates into mass
        weighted coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Msqrt
        else:
            return self.Msqrt * np.sqrt(units.AU_TO_AMU)

    def get_Msqrtinv(self, _au=False):
        """
        Returns the inverse of the transformation matrix from Carthesian
        coordinates into mass weighted coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Msqrtinv
        else:
            return self.Msqrtinv / np.sqrt(units.AU_TO_AMU)

    # ------------
    # pretty print
    # ------------
    def _print_matrix(self, matrix):
        info =' i \ j '
        for i in range(self.Nmodes):
            info += '{:>20d}'.format(i)
        lim = '-'*len(info)
        info += '\n{}'.format(lim)
        for i in range(self.Nmodes):
            line = '\n{:>3d} |  '.format(i)
            for j in range(self.Nmodes):
                line += '{:>20.9f}'.format(matrix[i,j])
            info += line
        return info

    def print_U(self, _au=False):
        return self._print_matrix(self.get_U(_au=_au))

    def print_Uinv(self, _au=False):
        return self._print_matrix(self.get_Uinv(_au=_au))

    def print_Q(self, _au=False):
        return self._print_matrix(self.get_Q(_au=_au))

    def print_QT(self, _au=False):
        return self._print_matrix(self.get_QT(_au=_au))

    def print_Msqrt(self, _au=False):
        return self._print_matrix(self.get_Msqrt(_au=_au))

    def print_Msqrtinv(self, _au=False):
        return self._print_matrix(self.get_Msqrtinv(_au=_au))

    # --------------------------
    # coordinate transformations
    # --------------------------

    def __cartesian_to_frictionmodes(self, cartesian):
        return np.dot(self.Uinv, cartesian.ravel())

    def __frictionmodes_to_cartesian(self, frictionmodes):
        return np.dot(self.U, frictionmodes).reshape(self.Natoms, self.Ndim)

    def __massweighted_to_cartesian(self, massweighted):
        return np.dot(self.Msqrtinv, massweighted).reshape(self.Natoms, self.Ndim)

    def __cartesian_to_massweigthed(self, cartesian):
        return np.dot(self.Msqrt, cartesian.ravel())

    def __frictionmodes_to_massweighted(self, frictionmodes):
        return np.dot(self.Q, frictionmodes)

    def __massweighted_to_frictionmodes(self, massweighted):
        return np.dot(self.QT, massweighted)

    def velocities_cartesian_to_frictionmodes(self, velocities):
        return self.__cartesian_to_frictionmodes(velocities)

    def velocities_frictionmodes_to_cartesian(self, frictionmodes):
        return self.__frictionmodes_to_cartesian(frictionmodes)

    def frictiontensor_massweightedcartesian_to_frictionmodes(self, frictiontensor):
        return np.dot(self.QT, frictiontensor.dot(self.Q))
