# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
This is a module handling the results from a normalmodes analysis.

Nomenclature
============

Consider the displacement vector

    x = R - R_0,                                                        (1)

where R is the position vector of the displaced system in Carthesian
coordinates and R_0 corresponds to the equilibrium configuration of the
system, likewise in Carthesian coordinates.

This displacement vector can be transformed into the basis of mass-weighted
Carthesian coordinates using the transformation matrix M, i.e.

    Msqrt.x = y.                                                        (2)

Hence, y is the displacement vector in mass-weighted Carthesian
coordinates. The corresponding transformation matrix Msqrt is of diagonal
structure with

    Msqrt_ij = sqrt(m_i) * delta_ij,                                    (3)

where m_i is the mass associated with coordinate i, and delta_ij is the
Kronecker delta.

The transformation matrix Q transforms the displacement vector in
mass-weighted Carthesian coordinates into the basis of (mass-weighted)
normalmodes:

    Q^T.y = q.                                                          (4)

Q is the matrix that diagonalizes the force constant matrix F, i.e. the
Hessian of the potential energy in (mass-weighted) Carthesian coordinates at R_0.

    Q^T.F.Q = L                                                         (5)

where L is the diagonal matrix of eigenvalues omega^2. Thus, Q
consists of the eigenvectors of F, i.e. the so-called normalmodes. We here
assume that Q is made up of the eigenvectors as column vectors, see here:

    https://en.wikipedia.org/wiki/Symmetric_matrix#Decomposition.

Since F is real-symmetric by construction, Q is an ORTHOGONAL MATRIX such that

    Q^T.Q = Q.Q^T = 1 and hence Q^-1 = Q^T.                             (6)

The matrix Q can be directly taken from the *.phonon file resulting from
the CASTEP normalmode analysis.

The direct transformation from carthesian to (mass-weighted) normalmode
basis corresponds to a successive application of M and Q

    Q^T.(Msqrt.x) = Q^T.y = q.                                          (7)

Hence, defining the matrix Uinv := Q^T.Msqrt one can write

    Uinv.x = q.                                                         (8)

Note that Uinv is in general not orthogonal.


Forces, or more generally gradients transform differently. Here we have, since
we have linear transformations:

    fx = Msqrt.fy = Msqrt.Q.fq                                          (9)

Hence, the direct transformation is done via the matrix

    fx = W.fq = (Msqrt.Q).fq                                           (10)

and

    fn = Winv.fx = (Q^T.Winv).fx                                       (11)

"""

# linear algebra
import numpy as np

# unit conversion
from MDsim import units


class Normalmodes(object):
    """
    Class that handles coordinate transformations in the context of a
    normalmode analysis.

    There is so far only reliable support of constraint normalmodes if the
    constraints restrict cartesian axes and are all the same for all atoms.
    This means that Nmodes=Natoms*Ndim. More complex schemes are possible (see
    legacy code) but not required here.

    Initialization
    --------------
    displacements : (Nmodes=Natoms*Ndim, Nmodes) array
        The actual displacement patterns of the normalmodes in mass-weighted
        cartesian space. Note that we stick to the convention that the *first
        index names the mass-weighted cartesian components, second index names
        the mode*. This means, "normalmodes" is just the matrix Q that
        diagonalizes the mass-weighted Hessian according to Q^T.H.Q (see module
        documentation).

        Example:

            normalmodes[:,i] is the i-th mode vector.

    omegas : (Nmodes,) array
        The positive square roots of the eigenvalues of the mass-weighted
        Hessian H, a.k.a. the frequencies of the normalmodes (or the curvature
        of the PES in mass-weighted coordinates).

        Example:
            H.normalmodes[:,i] = omegas[i]**2 * normalmodes[:,i]

    masses : (Natoms,) array
        The masses associated with each atom. Required to yield a proper
        convertion from mass-weighted space to cartesian space. Note that you
        should enter the same masses here as you used for the normalmode
        analysis to avoid subtile problems.

    positions_eq : (Natoms, Ndim) array, optional (default=None)
        The positions at which the normalmodes where evaluated. Necessary to
        map from cartesian displacements to absolute positions. Defaults to the
        respective zero vector.

    convert : boolean, optional (default=True)
        Do unit conversion. This is in this case a non-trivial thing. If the
        mass-weighted Hessian was diagonalized in eV/Angstrom/atomic mass
        units, then the omegas are in sqrt(eV/Angstrom**2*atomic mass unit),
        which has the dimension of an inverse time, ie. frequency. The energy
        of the omega here comes into play by talking about energy equivalents
        hbar*omega. In atomic units, hbar==1 and hence energy and time are
        equivalent, anyhow. But in any other unit system they differ...

        Note that the normalmodes themselves are unitless, they are just
        orthonormalized combinations of the basis vectors the Hessian is
        spanned in. All unit changes are supposed to be reflected in omega, not
        in the normalmodes.

        To cut a long story short, convert = <True> converts masses from amu to
        au and omegas from eV to au.
    """
    def __init__(self, displacements, omegas, masses, positions_eq=None, convert=True):

        # make a real copy!
        self.displacements = np.array(displacements, order='C', dtype=np.float64)
        self.omegas = np.array(omegas, order='C', dtype=np.float64)
        self.masses = np.array(masses, order='C', dtype=np.float64)

        self.Nmodes = self.displacements.shape[0]
        self.Natoms = self.masses.shape[0]
        self.Ndim = self.Nmodes / self.Natoms

        if positions_eq is None:
            self.positions_eq = np.zeros((self.Natoms, self.Ndim), order='C', dtype=np.float64)
        else:
            self.positions_eq = np.array(positions_eq, order='C', dtype=np.float64)

        self._convert = convert

        if self._convert:
            # no in-place modifications here
            self.masses = self.masses * units.AMU_TO_AU
            self.positions_eq = self.positions_eq * units.ANGSTROM_TO_AU
            self.omegas = self.omegas * units.EV_TO_AU

        # now create the transformation matrices

        # Build the ORTHOGONAL transformation matrix Qinv which brings us
        # mass weighted carthesian --> normal modes
        # make really sure that Q and Qinv are independent here
        #
        # Ok, this is a bit tricky wrt. to notation. We assume that the
        # mass-weighted Hessian H is diagonalized via Q^T.H.Q. Then Q consists
        # of the eigenvectors of H arranged in columns. Hence, a vector in
        # normalmodespace (n) is transfered to mass-weighted cartesian space
        # (y) via Q.n = y. The other way round, a mass-weighted cartesian space
        # vector (y) is transformed to normalmode space via Q^T.y = n
        #

        # normalmodes --> mass-weighted cartesian
        self.Q = self.displacements.copy()

        # mass-weighted cartesian --> normalmodes
        self.QT = self.displacements.copy().T

        # sanity check if we really have an orthogonal Q
        if not np.allclose(np.dot(self.Q, self.QT), np.identity(self.Nmodes)):
            msg = '"displacements" do not create a unitary transformation matrix'
            msg += '\nnp.dot(Q, Q.T) yields\n{}'.format(np.dot(self.Q, self.Q.T))
            raise ValueError(msg)

        # cartesian --> mass weighted carthesian
        self.Msqrt = np.diag(np.repeat(np.sqrt(self.masses), self.Ndim))

        # mass-weighted cartesian --> cartesian
        self.Msqrtinv = np.diag(1./ np.diag(self.Msqrt))

        # THIS MATRIX IS NOT ORTHOGONAL!
        # normalmodes --> cartesian
        self.U  = np.dot(self.Msqrtinv, self.Q)

        # cartesian --> normal modes
        self.Uinv = np.dot(self.QT, self.Msqrt)

        # for force transformations
        self.W = np.dot(self.Msqrt, self.Q)
        self.Winv = np.dot(self.QT, self.Msqrtinv)

    # -----------------------
    # transformation matrices
    # -----------------------

    def get_U(self, _au=False):
        """
        Returns the transformation matrix U as build from the normalmodes in
        Carthesian Coordinates. This matrix is not orthogonal!
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.U
        else:
            return self.U / np.sqrt(units.AU_TO_AMU)


    def get_Uinv(self, _au=False):
        """
        Returns the inverse of the transformation matrix U as build from the
        normalmodes in Carthesian Coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Uinv
        else:
            return self.Uinv * np.sqrt(units.AU_TO_AMU)

    def get_W(self, _au=False):
        """
        Returns the transformation matrix U as build from the normalmodes in
        Carthesian Coordinates. This matrix is not orthogonal!
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.W
        else:
            return self.W * np.sqrt(units.AU_TO_AMU)


    def get_Winv(self, _au=False):
        """
        Returns the inverse of the transformation matrix U as build from the
        normalmodes in Carthesian Coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Winv
        else:
            return self.Winv / np.sqrt(units.AU_TO_AMU)


    def get_Q(self, _au=False):
        """
        Returns the transformation matrix Q as build from the normalmodes
        in mass weighted coordinates. This matrix IS orthogonal.
        If au = True, output in atomic units.
        """
        # matrix is dimensionless
        return self.Q


    def get_QT(self, _au=False):
        """
        Returns the inverse of the transformation matrix Q (a.k.a. the
        transpose, since Q is orthogonal) as build from the normalmodes
        in mass weighted coordinates.
        If au = True, output in atomic units.
        """
        return self.QT

    def get_Msqrt(self, _au=False):
        """
        Returns the transformation matrix from Carthesian coordinates into mass
        weighted coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Msqrt
        else:
            return self.Msqrt * np.sqrt(units.AU_TO_AMU)

    def get_Msqrtinv(self, _au=False):
        """
        Returns the inverse of the transformation matrix from Carthesian
        coordinates into mass weighted coordinates.
        If au = True, output in atomic units.
        """
        if _au or not self._convert:
            return self.Msqrtinv
        else:
            return self.Msqrtinv / np.sqrt(units.AU_TO_AMU)

    # ------------
    # pretty print
    # ------------
    def _print_matrix(self, matrix):
        info =' i \ j '
        for i in range(self.Nmodes):
            info += '{:>20d}'.format(i)
        lim = '-'*len(info)
        info += '\n{}'.format(lim)
        for i in range(self.Nmodes):
            line = '\n{:>3d} |  '.format(i)
            for j in range(self.Nmodes):
                line += '{:>20.9f}'.format(matrix[i,j])
            info += line
        return info

    def print_U(self, _au=False):
        return self._print_matrix(self.get_U(_au=_au))

    def print_Uinv(self, _au=False):
        return self._print_matrix(self.get_Uinv(_au=_au))

    def print_W(self, _au=False):
        return self._print_matrix(self.get_W(_au=_au))

    def print_Winv(self, _au=False):
        return self._print_matrix(self.get_Winv(_au=_au))

    def print_Q(self, _au=False):
        return self._print_matrix(self.get_Q(_au=_au))

    def print_QT(self, _au=False):
        return self._print_matrix(self.get_QT(_au=_au))

    def print_Msqrt(self, _au=False):
        return self._print_matrix(self.get_Msqrt(_au=_au))

    def print_Msqrtinv(self, _au=False):
        return self._print_matrix(self.get_Msqrtinv(_au=_au))

    # --------------------------
    # coordinate transformations
    # --------------------------

    def __cartesian_to_normalmodes(self, cartesian):
        return np.dot(self.Uinv, cartesian.ravel())

    def __normalmodes_to_cartesian(self, normalmodes):
        return np.dot(self.U, normalmodes).reshape(self.Natoms, self.Ndim)

    def __massweighted_to_cartesian(self, massweighted):
        return np.dot(self.Msqrtinv, massweighted).reshape(self.Natoms, self.Ndim)

    def __cartesian_to_massweigthed(self, cartesian):
        return np.dot(self.Msqrt, cartesian.ravel())

    def __normalmodes_to_massweighted(self, normalmodes):
        return np.dot(self.Q, normalmodes)

    def __massweighted_to_normalmodes(self, massweighted):
        return np.dot(self.QT, massweighted)

    def positions_cartesian_to_normalmodes(self, positions):
        return self.__cartesian_to_normalmodes(positions-self.positions_eq)

    def positions_normalmodes_to_cartesian(self, normalmodes):
        return self.__normalmodes_to_cartesian(normalmodes) + self.positions_eq

    def velocities_cartesian_to_normalmodes(self, velocities):
        return self.__cartesian_to_normalmodes(velocities)

    def velocities_normalmodes_to_cartesian(self, normalmodes):
        return self.__normalmodes_to_cartesian(normalmodes)

    def gradient_cartesian_to_normalmodes(self, gradient):
        return np.dot(self.Winv, gradient.ravel())

    def gradient_normalmodes_to_cartesian(self, gradient):
        return np.dot(self.W, gradient.ravel()).reshape(self.Natoms, self.Ndim)

    # ----------------------
    # vibrational properties
    # ----------------------

    def get_frequencies(self, _au=False):
        if _au:
            return self.omegas
        else:
            return self.omegas * units.AU_TO_KAYSER

    def get_omegas(self, _au=False):
        if _au:
            return self.omegas
        else:
            return self.omegas * units.AU_TO_EV

    def get_Nmodes(self):
        return self.Nmodes

    def pump_modes_kinetic_energy(self,
                                 mode_idx,
                                 quanta,
                                 zpe=False,
                                 sign='+',
                                 verbose=True,
                                 _au=False):
        """
        Pump a specific mode with kinetic energy. In normalmode space, the
        kinetic energy is just

            T = 0.5 * sum_i(qdot_i**2)                                      (1)

        The kinetic energy is just a superposition of kinetic energies within
        modes, there is no coupling. Hence we can define a "kinetic energy
        vector" t such that

            t_i = 0.5 * qdot_i**2                                           (2)

        and

            T = sum_i(t_i)                                                  (3)

        We only want kinetic energy in selected modes specified by the
        "mode_idx". Moreover, we want to specify each contribution. Hence we
        know the elements of the t-vector to be

            t[mode_idx][i] = omega[mode_idx][i]*quanta[i]

        for the specified indices and = 0 for all unexcited modes. If you chose
        the zero point energy options, then we add 0.5*omega[i] to *all* modes.
        Finally we can equate Eq. (2) for qdot to yield

            qdot[i] = +/- sqrt(2*t[i])                                      (4)

        The sign actually is arbitrary as the kinetic energy is insensitive
        towards it. You can chose either "+", "-" or "random" via the "sign"
        switch.

        Once we have the respective qdot vector, all we do is to transform it
        to cartesian space. Note that velocities transform the same way
        positions do as the potential is a static quantity by definition
        (curvature does not depend on time -- all not explicitly included DOF
        are assumed to be frozen/time-constant).


        Parameters
        ----------
        mode_idx : (Nexc,) int-array
            The indices of the excited modes.

        quanta : (Nexc,) array
            The excitation energy in vibrational quanta.

        zpe : boolean, optional (default=False)
            Zero point energy is considered. Effectively, each mode will carry
            an additional 0.5*omega in energy. Also known as quasi-classical in
            some fields (gas-surface scattering)

        sign : str, optional ({*'+'*, '-', 'random'})
            Convention for the velocity direction in normalmode space.

        verbose : boolean, optional (default=True)
            Print information to stdout.

        _au : boolean, optional (default=False)
            Return the velocities in atomic units (_au = <True>) or in Angstrom
            per femtosecond (_au=<False>).


        Returns
        -------
        velocities : (Natoms, Ndim) array
            Velocities in cartesian coordinates correponding to the respective
            excitation kinetic energy. Note that you have to start from an
            equilibrium positions, otherwise you will also have non-predictable
            contributions from the PES.

        info : str
            Infostring.
        """

        # make sure we have the correct arrays
        mode_idx = np.array(mode_idx, dtype=np.int32)

        init_quanta = np.zeros(self.Nmodes)
        init_quanta[mode_idx] = quanta

        if zpe:
            init_quanta += 0.5

        # the kinetic energy vector
        t = init_quanta * self.omegas

        # the velocities
        qdot = np.sqrt(2*t)

        # the sign
        sign = sign.lower()
        if sign == '+':
            pass
        elif sign == '-':
            qdot *= -1
        elif sign == 'random':
            qdot *= -1**np.random.randint(low=0, high=2, size=self.Nmodes)

        velocities = self.__normalmodes_to_cartesian(qdot)

        if not _au:
            velocities *= units.AU_TO_ANGSTROM_PER_FS

        # some nice verbosity
        info = 'Pumping eigenmodes with kinetic energy'

        if zpe:
            zpe_str = ''
        else:
            zpe_str = '*not* '

        info += '\n\tZero point vibrational energy is {}considered'.format(zpe_str)
        info += '\n\tSign convention for velocities in normal modes space is "{}"'.format(sign)
        info += '\n\n'
        head = '{0:>10s}{1:>20s}{2:>20s}{3:>20s}'.format('Mode #', 'hbar*omega (meV)', 'quanta', 'Ekin (meV)')
        info += head
        info += '\n' + '-'*len(head)
        for i in range(self.Nmodes):
            info += '\n{0:>10d}{1:>20.3f}{2:>20.2f}{3:20.3f}'.format(i, self.omegas[i] * units.AU_TO_EV * 1000,
                                                                     init_quanta[i], t[i] * units.AU_TO_EV * 1000)
        info += '\n{0:55s}'.format('') + '-'*15
        info += '\n{0:55s}sum = '.format('') + '{0:9.3f}'.format(np.sum(t)*units.AU_TO_EV * 1000)

        if verbose:
            print(info)

        return velocities, info

