# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

# vibrational handling
from ase.vibrations import Vibrations

# unit conversion
from MDsim import units

# castep phonon support
from MDsim.tools.castep import read_phonon

# parent class
from MDsim.normalmodes import Normalmodes


class CastepNormalmodes(Normalmodes):
    """
    Class that handles the normalmodes of a system as read from a CASTEP .phonon
    file.

    Initialization
    --------------
    atoms_eq   : AtomsEq instance
                 Specifying the equilibrium geometry and constraints masks.
    phononfile : string
                 Location of the CASTEP .phonon file
    use_modes  : list of integers (default = [])
                 List of normalmodes from the phonon analysis to be used.
                 Caution, python enumeration is used, i.e. starting with 0. If
                 nothing is passed, then all non-zero-energy modes will be
                 used.
    mode_names : list/array/tuple (default = None)
                 Names associated with the active modes.

    """

    def __init__(self, atoms_eq,
                       phononfile,
                       use_modes = None,
                       mode_names = None):

        self.mode_names = mode_names

        # equilibrium properties
        self.atoms_eq = atoms_eq

        # read the phononfile
        self.phononfile = phononfile
        self.vibdata, atoms = read_phonon(self.phononfile,
                                      read_vib_data = True)

        # just for convenience, save a vib_obj
        self.vib_obj = Vibrations(atoms, vibdata = self.vibdata)

        # dimensions:
        # self.energies     : (Natoms x 1)
        # self.displacments : (Natoms*3 x Natoms*3) (mode, atomic displacement)
        self.energies, self.displacements = self.vibdata


        # process the use_modes flag
        if use_modes is None:
            self.modes_mask = self._get_modes_mask(self.energies)
        else:
            self.modes_mask = np.zeros((len(self.energies)), dtype = bool)
            for idx in use_modes:
                self.modes_mask[idx] = True

        # data contains: Q, Qinv, U, Uinv, M, Minv, modes_mask,
        # constraints_mask, and filtered versions of energies, displacements
        data = self._create_trafo_matrices(self.atoms_eq, self.displacements,
                                           self.energies, self.modes_mask)

        # assign the content from data to instance variables
        self.__dict__.update(data)

        # save original mode indices from the phonon file
        self.modes_idx = np.arange(len(self.modes_mask))[self.modes_mask]

    def __str__(self):
        name  = 'CastepNormalmodes instance based on phononfile:'
        name += '\n\t{}'.format(self.phononfile)
        name += '\nFree parameters:'
        name += '\n----------------'

        free_atoms = self.atoms_eq.free_atoms
        free_dof   = self.atoms_eq.free_dof

        name += '\n\tfree atoms :{}'.format((' {}'*len(free_atoms)).format(*free_atoms))
        name += '\n\tfree DOF   :{}'.format((' {}'*len(free_dof)).format(*free_dof))
        name += '\n\tfree modes :{}'.format((' {}'*len(self.modes_idx)).format(*self.modes_idx))
        name += '\n\t(--> indices correspond to phonon file, starting from 0)'

        energies = self.energies.copy()
        omegas   = self.energies.copy() * units.EV_TO_KAYSER

        name += '\nAssociated energies and frequencies:'
        name += '\n------------------------------------'
        for i,n,e,o in zip(self.modes_idx, self.get_mode_names(), energies, omegas):
            name += '\n\tmode #{0:2d} ({1:<15s}) : {2:>10.3f} cm^-1 = {3:>10.6f} eV'.format(i, n, o, e)
        return name

    def get_phononfile(self):
        return self.phononfile

    def get_modes_idx(self):
        return self.modes_idx

