# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to transform cartesian coordinates to some fractional coordinates.
The purpose is that you can then easily map a triangluar grid in cartesian
space if you go for instance for an fcc(111)-PES
"""

from __future__ import print_function

import numpy as np

from MDsim import units

class FractionalCoordinates(object):
    """
    Transforming between fractional and cartesian coordinates at ease.
    Numpy's einsum is used to gain very neat performances without resorting to
    any external speedup routines.

    Initialization
    --------------
    cell : (Ndim, Ndim) array
        The cell for which fractional coordinates should be evaluated.
        First index names the vectors, second the cartesian component.

    convert : boolean, optional (default=True)
        Convert input cell from Angstrom to a.u.. If <False> then input is
        considered to be in a.u.
    """

    def __init__(self, cell, convert=True):
        self.cell = cell
        if convert:
            self.cell = self.cell * units.ANGSTROM_TO_AU

        # we define U.y = x where x is cartesian and y is fractional space basis
        self.Q = self.cell.T.copy()
        self.Qinv = np.linalg.inv(self.Q)

        self.Q_ANGSTROM = self.Q * units.AU_TO_ANGSTROM
        self.Qinv_ANGSTROM = self.Qinv / units.AU_TO_ANGSTROM

        # Jacobian to transform from internal fractional space to cartesian space
        self.jacobian = self.Qinv.T.copy()
        self.jacobian_ANGSTROM = self.Qinv_ANGSTROM.T.copy()

    def positions_cartesian_to_fractional(self, positions, periodic=False, _au=True):
        """
        Convert from cartesian to fractional coordinates.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions to be converted. See the _au switch for units.

        periodic : boolean, optional (default=False)
            Consider positions periodic w.r.t. the cell.

        _au : boolean, optional (default=True)
            Input positions are in atomic units, ie. Bohr if <True>. If <False>
            units are expected in Angstrom.

        Returns
        -------
        fractionals : (Natoms, Ndim) array
            Fractional coordinates. Note that this is still an (Natoms, Ndim)
            vector!
        """

        # get fractional positions, note that this is still (Natoms, Ndim)
        # einsum outperforms tensordot + transpose here by factor of ~6
        # return np.tensordot(self.Qinv, positions, axes=(1,1)).T
        if _au:
            if periodic:
                return np.einsum('ij,kj->ki', self.Qinv, positions)%1
            else:
                return np.einsum('ij,kj->ki', self.Qinv, positions)
        else:
            if periodic:
                return np.einsum('ij,kj->ki', self.Qinv_ANGSTROM, positions)%1
            else:
                return np.einsum('ij,kj->ki', self.Qinv_ANGSTROM, positions)


    def positions_fractional_to_cartesian(self, fractionals, periodic=False,_au=True):
        """
        Convert from fractional to cartesian coordinates.

        Parameters
        ----------
        fractionals : (Natoms, Ndim) array
            Fractional coordinates. Note that this is still an (Natoms, Ndim)
            vector.

        periodic : boolean, optional (default=False)
            Consider positions periodic w.r.t. the cell.

        _au : boolean, optional (default=True)
            Output positions are in atomic units, ie. Bohr if <True>. If <False>
            units are expected in Angstrom.

        Returns
        -------
        positions : (Natoms, Ndim) array
            Positions in cartesian space. See the _au switch for units.
        """
        # einsum outperforms tensordot + transpose here by factor of ~6
        # return np.tensordot(self.Q, fractionals, axes=(1,1)).T
        if periodic:
            fractionals = fractionals%1
        if _au:
            return np.einsum('ij,kj->ki', self.Q, fractionals)
        if _au:
            return np.einsum('ij,kj->ki', self.Q_ANGSTROM, fractionals)


    def gradient_fractional_to_cartesian(self, gradient, _au=True):
        """
        Transform a gradient from fractional coordinates to cartesian
        coordinates.

        Parameters
        ----------
        gradient : (Natoms, Ndim) array
            Gradient in fractional coordinates. Note that this is still an
            (Natoms, Ndim) vector.

        _au : boolean, optional (default=True)
            Output gradient is in are in atomic units, ie. <unit of
            interpolations function>/Bohr if <True>. Else it is <units of
            interpolation function>/Angstrom.

        Returns
        -------
        gradient : (Natoms, Ndim) array
            Gradient in cartesian space. See the _au switch for units.
        """
        if _au:
            return np.einsum('ij,kj->ki', self.jacobian, gradient)
        else:
            return np.einsum('ij,kj->ki', self.jacobian_ANGSTROM, gradient)
