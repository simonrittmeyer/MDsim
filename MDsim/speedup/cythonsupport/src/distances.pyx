# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import cython
cimport cython

import numpy as np
cimport numpy as np

from libc.math cimport round
from libc.math cimport sqrt

DTYPE_float = np.float64
DTYPE_int = np.int32
ctypedef np.float64_t DTYPE_float_t
ctypedef np.int32_t DTYPE_int_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def calc_distances_mic(np.ndarray[DTYPE_float_t, ndim=2] positions,
                       np.ndarray[DTYPE_float_t, ndim=1] unit_cell_diag):

    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    
    cdef np.ndarray[DTYPE_float_t, ndim=2] distances = np.zeros((Natoms, Natoms))
    
    cdef DTYPE_float_t tmp = 0.
    cdef DTYPE_float_t dist = 0.

    cdef DTYPE_int_t i,j,k
    
    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp
            dist = sqrt(dist)
            distances[i, j] = dist
            distances[j, i] = dist
    return distances


