# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import cython
cimport cython

import numpy as np
cimport numpy as np

from libc.math cimport cos
from libc.math cimport sin

import math

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def evaluate_expansion_fcc100(np.ndarray[DTYPE_t, ndim=2] positions, 
                              DTYPE_t a, 
                              DTYPE_t site1, 
                              DTYPE_t site2):
    
    cdef unsigned int Natoms = positions.shape[0]
    cdef unsigned int Ndim = positions.shape[1]

    cdef np.ndarray[DTYPE_t, ndim=1] values = np.zeros(Natoms)

    # evaluate the prefactors only once and not in the loop
    cdef double constant = site1 / 4. + site2 / 2.

    cdef double args_pref = 2 * math.pi / a
    cdef double sum_pref = site1 / 4.
    cdef double prod_pref = (site1 - 2*site2) / 4.


    cdef unsigned int i
    cdef double x, y

    for i in range(Natoms):
        x = args_pref * positions[i,0]
        y = args_pref * positions[i,1]
        values[i] = (constant
                     + sum_pref * (cos(x) + cos(y))
                     + prod_pref * cos(x)*cos(y))


    return values


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def evaluate_gradient_fcc100(np.ndarray[DTYPE_t, ndim=2] positions, 
                              DTYPE_t a, 
                              DTYPE_t site1, 
                              DTYPE_t site2):
    cdef unsigned int Natoms = positions.shape[0]
    cdef unsigned int Ndim = positions.shape[1]

    cdef np.ndarray[DTYPE_t, ndim=2] gradient = np.zeros((Natoms, Ndim))

    # evaluate the prefactors only once and not in the loop
    cdef double args_pref = 2 * math.pi / a
    cdef double sum_pref = site1 / 4.
    cdef double prod_pref = (site1 - 2*site2) / 4.
    cdef double sum_args_pref = sum_pref * args_pref
    cdef double prod_args_pref = prod_pref * args_pref


    cdef unsigned int i
    cdef double x, y


    for i in range(Natoms):
        x = args_pref * positions[i,0]
        y = args_pref * positions[i,1]

        gradient[i,0] = -(sum_args_pref * sin(x)
                        + prod_args_pref * sin(x)*cos(y))

        gradient[i,1] = -(sum_args_pref * sin(y)
                        + prod_args_pref * sin(y)*cos(x))

    return gradient
