# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import cython
cimport cython

import numpy as np
cimport numpy as np

from libc.math cimport round
from libc.math cimport pow 
from libc.math cimport sqrt 

DTYPE_float = np.float64
DTYPE_int = np.int32
ctypedef np.float64_t DTYPE_float_t
ctypedef np.int32_t DTYPE_int_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def calc_energy_bruteforce(np.ndarray[DTYPE_float_t, ndim=2] positions,
                           np.ndarray[DTYPE_float_t, ndim=1] dipoles,
                           np.ndarray[DTYPE_float_t, ndim=1] unit_cell_diag,
                           DTYPE_float_t cutoff):

    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    cdef DTYPE_float_t cutoff_square = pow(cutoff, 2)
    cdef DTYPE_float_t energy = 0.
    cdef DTYPE_float_t energy_correction = 0.
    cdef DTYPE_float_t tmp = 0.
    cdef DTYPE_float_t dist = 0.

    cdef DTYPE_int_t i,j,k
    
    energy_correction = 2.0 * dipoles[0] * dipoles[0] / pow(cutoff, 3)

    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                energy += 2.0 * dipoles[i] * dipoles[j] / pow(dist, 3) - energy_correction
    return energy


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def calc_forces_bruteforce(np.ndarray[DTYPE_float_t, ndim=2] positions,
                           np.ndarray[DTYPE_float_t, ndim=1] dipoles,
                           np.ndarray[DTYPE_float_t, ndim=1] unit_cell_diag,
                           DTYPE_float_t cutoff):

    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    cdef DTYPE_float_t cutoff_square = pow(cutoff, 2)
    cdef DTYPE_float_t energy = 0.
    cdef DTYPE_float_t tmp = 0.
    cdef DTYPE_float_t dist = 0.

    cdef DTYPE_int_t i,j,k

    cdef np.ndarray[DTYPE_float_t, ndim=1] distance = np.zeros(Ndim)
    cdef np.ndarray[DTYPE_float_t, ndim=2] forces = np.zeros((Natoms, Ndim))
    


    cutoff_square = cutoff**2
    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp
                distance[k] = tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                tmp = 6.0 * dipoles[i] * dipoles[j] / pow(dist, 5)
                for k in range(Ndim):
                    # Newton's actio=reactio
                    forces[i,k] += tmp * distance[k]
                    forces[j,k] -= tmp * distance[k]

    return forces


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def calc_energy_neighbor(np.ndarray[DTYPE_float_t, ndim=2] positions,
                           np.ndarray[DTYPE_float_t, ndim=1] dipoles,
                           np.ndarray[DTYPE_float_t, ndim=1] unit_cell_diag,
                           np.ndarray[DTYPE_int_t, ndim=2] neighbor_lists,
                           np.ndarray[DTYPE_int_t, ndim=1] num_neighbors,
                           DTYPE_float_t cutoff):

    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    cdef DTYPE_float_t cutoff_square = pow(cutoff, 2)
    cdef DTYPE_float_t energy = 0.
    cdef DTYPE_float_t energy_correction = 0.
    cdef DTYPE_float_t tmp = 0.
    cdef DTYPE_float_t dist = 0.

    cdef DTYPE_int_t i,j,k,n
    
    energy_correction = 2.0 * dipoles[0] * dipoles[0] / pow(cutoff, 3)
    
    for i in range(Natoms-1):
        for n in range(num_neighbors[i]):
            j = neighbor_lists[i,n]
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                energy += 2.0 * dipoles[i] * dipoles[j] / pow(dist, 3) - energy_correction
    return energy


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def calc_forces_neighbor(np.ndarray[DTYPE_float_t, ndim=2] positions,
                         np.ndarray[DTYPE_float_t, ndim=1] dipoles,
                         np.ndarray[DTYPE_float_t, ndim=1] unit_cell_diag,
                         np.ndarray[DTYPE_int_t, ndim=2] neighbor_lists,
                         np.ndarray[DTYPE_int_t, ndim=1] num_neighbors,
                         DTYPE_float_t cutoff):

    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    cdef DTYPE_float_t cutoff_square = pow(cutoff, 2)
    cdef DTYPE_float_t energy = 0.
    cdef DTYPE_float_t tmp = 0.
    cdef DTYPE_float_t dist = 0.

    cdef DTYPE_int_t i,j,k,n

    cdef np.ndarray[DTYPE_float_t, ndim=1] distance = np.zeros(Ndim)
    cdef np.ndarray[DTYPE_float_t, ndim=2] forces = np.zeros((Natoms, Ndim))
    

    cutoff_square = cutoff**2
    for i in range(Natoms-1):
        for n in range(num_neighbors[i]):
            j = neighbor_lists[i,n]
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp
                distance[k] = tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                tmp = 6.0 * dipoles[i] * dipoles[j] / pow(dist, 5)
                for k in range(Ndim):
                    # Newton's actio=reactio
                    forces[i,k] += tmp * distance[k]
                    forces[j,k] -= tmp * distance[k]

    return forces
