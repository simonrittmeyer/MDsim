# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import cython
cimport cython

import numpy as np
cimport numpy as np

from libc.math cimport round

DTYPE_float = np.float64
DTYPE_int = np.int32
ctypedef np.float64_t DTYPE_float_t
ctypedef np.int32_t DTYPE_int_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def construct_neighbor_lists(np.ndarray[DTYPE_float_t, ndim=2] positions, 
                             DTYPE_float_t list_cutoff):
    """
    Implementation of a primitive neighbor search that scales as O(N^2). All
    non-redundant distances are computed and the respective neighbor lists are
    created. There is no awareness of periodic boundary conditions.

    Parameters
    ----------
    positions : (Natoms, Ndim) array
        The positions array.

    list_cutoff : float
        The cutoff distance that separates neighbors from non-neigbors.

    Returns
    -------
    neighbor_lists : (Natoms, Natoms) array
        The actual neighbor lists. In principle, neighbor_list[i] is an array
        that contains the indices of neighbors of atom i. However, the need to
        be interpreted with care. Firstly, not all entries are valid as not
        every atom has Natom neighbors. Invalid entries are marked with "-1".
        The number of valid entries for atom i is stored in "num_neighbors(i)"
        to allow for convenient looping (see below). Secondly, in order to
        avoid double counting, only neighbor indices j > i are stored in the
        neighbor list of atom i. Hence, simple loops over these lests
        automatically avoid double counting and save computation time.

    num_neighbors : (Natoms,) array
        The number of neighbors for all atoms. Allows for convenient loops.
        Note that only neighbor_lists[i, :num_neighbors[i][ are actually valid
        neighbor indices of atom[i].
    """
    # the primitive neighbor search that scales as N^2
    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    cdef DTYPE_int_t i,j,k,m,n
    cdef DTYPE_float_t dist = 0.0
    cdef DTYPE_float_t tmp = 0.0

    # this is the number of neighbors each atom has
    cdef np.ndarray[DTYPE_int_t, ndim=1] num_neighbors = np.zeros(Natoms, dtype = np.int32)
    # the actual neighbor lists -- init with -1
    cdef np.ndarray[DTYPE_int_t, ndim=2] neighbor_lists = -np.ones((Natoms, Natoms), dtype = np.int32)

    list_cutoff = list_cutoff**2

    for i in range(Natoms):
        for j in range(i+1, Natoms):
            dist = 0
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                dist += tmp * tmp

            if dist < list_cutoff:
                # note: atom i only holds neighbors with index j > i
                # this avoids expensive douple checking in the force evaluation.
                num_neighbors[i] += 1
                neighbor_lists[i, num_neighbors[i]-1] = j

    return neighbor_lists, num_neighbors

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def construct_neighbor_lists_mic(np.ndarray[DTYPE_float_t, ndim=2] positions, 
                                 np.ndarray[DTYPE_float_t, ndim=1] unit_cell_diag,
                                 DTYPE_float_t list_cutoff):
    """
    Implementation of a primitive neighbor search that scales as O(N^2). All
    non-redundant distances are computed and the respective neighbor lists are
    created. Periodic boundary conditions are considered in terms of applying
    the minimum image convention (MIC) to evaluate distances. This is currently
    only available for orthorhombic cells.

    Parameters
    ----------
    positions : (Natoms, Ndim) array
        The positions array.

    unit_cell_diag : (Ndim,) array
        The boxlength of the orthorhombic unit cell along each dimension (has
        to be aligned with the cartesian coordinate axis).

    list_cutoff : float
        The cutoff distance that separates neighbors from non-neigbors.

    Returns
    -------
    neighbor_lists : (Natoms, Natoms) array
        The actual neighbor lists. In principle, neighbor_list[i] is an array
        that contains the indices of neighbors of atom i. However, the need to
        be interpreted with care. Firstly, not all entries are valid as not
        every atom has Natom neighbors. Invalid entries are marked with "-1".
        The number of valid entries for atom i is stored in "num_neighbors(i)"
        to allow for convenient looping (see below). Secondly, in order to
        avoid double counting, only neighbor indices j > i are stored in the
        neighbor list of atom i. Hence, simple loops over these lests
        automatically avoid double counting and save computation time.

    num_neighbors : (Natoms,) array
        The number of neighbors for all atoms. Allows for convenient loops.
        Note that only neighbor_lists[i, :num_neighbors[i][ are actually valid
        neighbor indices of atom[i].
    """
    # the primitive neighbor search that scales as N^2
    cdef DTYPE_int_t Natoms = positions.shape[0]
    cdef DTYPE_int_t Ndim = positions.shape[1]
    cdef DTYPE_int_t i,j,k,m,n
    cdef DTYPE_float_t dist = 0.0
    cdef DTYPE_float_t tmp = 0.0

    # this is the number of neighbors each atom has
    cdef np.ndarray[DTYPE_int_t, ndim=1] num_neighbors = np.zeros(Natoms, dtype = np.int32)
    # the actual neighbor lists -- init with -1
    cdef np.ndarray[DTYPE_int_t, ndim=2] neighbor_lists = -np.ones((Natoms, Natoms), dtype = np.int32)

    list_cutoff = list_cutoff**2

    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            dist = 0
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here comes the minimum image convention
                tmp = tmp - unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp * tmp

            if dist < list_cutoff:
                num_neighbors[i] += 1
                neighbor_lists[i, num_neighbors[i]-1] = j

    return neighbor_lists, num_neighbors

