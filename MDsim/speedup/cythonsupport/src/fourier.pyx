# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import cython
cimport cython

import numpy as np
cimport numpy as np

from libc.math cimport cos
from libc.math cimport sin

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def evaluate_fourier_expansion(np.ndarray[DTYPE_t, ndim=2] positions, 
                               np.ndarray[DTYPE_t, ndim=2] G,
                               np.ndarray[DTYPE_t, ndim=2] H, 
                               np.ndarray[DTYPE_t, ndim=1] real_coeff, 
                               np.ndarray[DTYPE_t, ndim=1] imag_coeff, 
                               DTYPE_t constant):

    cdef unsigned int Natoms = positions.shape[0]
    cdef unsigned int Ndim = positions.shape[1]
    cdef unsigned int Nlattice = G.shape[0]
    cdef unsigned int Nreal = real_coeff.shape[0]
    cdef unsigned int Nimag = imag_coeff.shape[0]

    cdef np.ndarray[DTYPE_t, ndim=1] values = np.zeros(Natoms, dtype=DTYPE)
    cdef unsigned int i,j,k,m,n
    
    cdef DTYPE_t G_dot_pos
    cdef DTYPE_t H_dot_pos

    for i in range(Natoms):
        values[i] = constant
        for j in range(Nlattice):
            G_dot_pos = 0.0
            H_dot_pos = 0.0
            for k in range(Ndim):
                G_dot_pos += positions[i,k] * G[j,k]
                H_dot_pos += positions[i,k] * H[j,k]
            for n in range(Nreal):
                values[i] += real_coeff[n] * cos((n+1) * G_dot_pos)
            for m in range(Nimag):
                values[i] += imag_coeff[m] * sin((m+1) * H_dot_pos)
    
    return values

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def evaluate_fourier_gradient(np.ndarray[DTYPE_t, ndim=2] positions, 
                               np.ndarray[DTYPE_t, ndim=2] G,
                               np.ndarray[DTYPE_t, ndim=2] H, 
                               np.ndarray[DTYPE_t, ndim=1] real_coeff, 
                               np.ndarray[DTYPE_t, ndim=1] imag_coeff, 
                              ):

    cdef unsigned int Natoms = positions.shape[0]
    cdef unsigned int Ndim = positions.shape[1]
    cdef unsigned int Nlattice = G.shape[0]
    cdef unsigned int Nreal = real_coeff.shape[0]
    cdef unsigned int Nimag = imag_coeff.shape[0]

    cdef np.ndarray[DTYPE_t, ndim=2] gradients = np.zeros((Natoms, Ndim), dtype=DTYPE)
    cdef unsigned int i,j,k,m,n
    
    cdef DTYPE_t G_dot_pos
    cdef DTYPE_t H_dot_pos

    for i in range(Natoms):
        for j in range(Nlattice):
            G_dot_pos = 0.0
            H_dot_pos = 0.0
            for k in range(Ndim):
                G_dot_pos += positions[i,k] * G[j,k]
                H_dot_pos += positions[i,k] * H[j,k]
            for n in range(Nreal):
                # no numpy broadcasting in cython!
                for k in range(Ndim):
                    # watch out, there is a minus sign due to the cos derivative
                    gradients[i,k] -= (n+1) * real_coeff[n] * sin((n+1) * G_dot_pos) * G[j,k]
            for m in range(Nimag):
                for k in range(Ndim):
                    gradients[i,k] += (m+1) * imag_coeff[m] * cos((m+1) * H_dot_pos) * H[j,k]
    
    return gradients
