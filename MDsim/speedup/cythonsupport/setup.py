# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np

extensions = [
    Extension('fourier', ['src/fourier.pyx'],
        include_dirs = [],
        libraries = [],
        library_dirs = []),
    Extension('neighbors', ['src/neighbors.pyx'],
        include_dirs = [],
        libraries = [],
        library_dirs = []),
    Extension('kohnlau', ['src/kohnlau.pyx'],
        include_dirs = [],
        libraries = [],
        library_dirs = []),
    Extension('cambridge', ['src/cambridge.pyx'],
        include_dirs = [],
        libraries = [],
        library_dirs = []),
    Extension('distances', ['src/distances.pyx'],
        include_dirs = [],
        libraries = [],
        library_dirs = [])
    ]

setup(name='Cython extensions to MDsim',
      ext_modules = cythonize(extensions),
      include_dirs=[np.get_include()]
      )
