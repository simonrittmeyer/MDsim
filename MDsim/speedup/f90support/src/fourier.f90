! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

subroutine evaluate_fourier_expansion(positions, &
&                                     values,&
&                                     G,&
&                                     H,&
&                                     real_coeff,&
&                                     imag_coeff,&
&                                     constant)
    ! -------------------------------------------------------------------------
    ! Routine to evaluate a Fourier expansion of type
    !
    !       V(pos_i) = const. + sum_n^Nreal A_n sum_j^Nlattice cos(n * G_j.dot.pos_i)  
    !                         + sum_m^Nimag B_m sum_j^Nlattice sin(m * H_j.dot.pos_i)  
    !
    ! Parameters
    ! ----------
    ! positions : (Ndim, Natoms) array (in)
    !     The positions at which to evaluate the expansion. Note that this is
    !     the transpose of the pythonic array.
    !
    ! values : (Natoms) array (inout)
    !     The array in which we store the values.
    !
    ! G : (Ndim, Nlattice) array (in)
    !     The reciprocal lattice vectors for the real part of the expansion. 
    !
    ! H : (Ndim, Nlattice) array (in)
    !     The reciprocal lattice vectors for the imaginary part of the 
    !     expansion.
    !
    ! real_coeff: (Nreal) array (in)
    !     The expansion coefficients for the real part.
    !
    ! imag_coeff: (Nimag) array (in)
    !     The expansion coefficients for the imaginary part.
    !
    ! constant : float (in)
    !     The constant from above.
    ! -------------------------------------------------------------------------
    implicit none
    
    ! positions, expected (Ndim, Natoms)
    double precision, dimension(:,:), intent(in) :: positions
    
    ! The actual values
    double precision, dimension(:), intent(inout) :: values
    
    ! The real (G) and imaginary (H) reciprocal lattice vectors
    ! expected (Ndim, Nlattice)
    double precision, dimension(:,:), intent(in) :: G
    double precision, dimension(:,:), intent(in) :: H
    
    ! the coefficients of the Fourier expansion
    ! distinguish real and imaginary coefficients in different arrays
    ! this makes it easier to handle the evaluation
    double precision, dimension(:), intent(in) :: real_coeff
    double precision, dimension(:), intent(in) :: imag_coeff
 
    ! The constant offset
    double precision, intent(in) :: constant 
    
    ! the array to store the dot products (do it only once)
    ! dimensionality is (Nlattice x Natoms) in the end
    double precision :: G_dot_pos
    double precision :: H_dot_pos


    ! the dimensions....
    integer :: Ndim, Nlattice, Natoms, Nreal, Nimag

    ! helper 
    integer :: i, j, m, n
    
    ! Note that G and H enter as (Ndim, Nlattice) arrays, ie. the transpose of
    ! the sourrounding python framework
    Ndim = size(G, 1)
    Nlattice = size(G,2)

    Natoms = size(values)
    Nreal = size(real_coeff)
    Nimag = size(imag_coeff)
    
    values = constant
    
    ! the real-part loop
    do i = 1, Natoms
        do j = 1, Nlattice

            ! The dot product by hand
!            G_dot_pos = 0.d0
!            H_dot_pos = 0.d0
!            do k = 1, Ndim
!                G_dot_pos = G_dot_pos + positions(k,i) * G(k,j)
!                H_dot_pos = H_dot_pos + positions(k,i) * H(k,j)
!            end do
            
            ! let's just go with the dot products
            G_dot_pos = dot_product(positions(:,i), G(:,j))
            H_dot_pos = dot_product(positions(:,i), H(:,j))

            do n = 1, Nreal
                values(i) = values(i) + real_coeff(n) * cos(n * G_dot_pos)
            end do
            do m = 1, Nimag
                values(i) = values(i) + imag_coeff(m) * sin(m * H_dot_pos)
            end do
        end do
    end do

end subroutine evaluate_fourier_expansion

subroutine evaluate_fourier_gradient(positions, &
&                                    gradients, &
&                                    G, &
&                                    H, &
&                                    real_coeff, &
&                                    imag_coeff)
    ! -------------------------------------------------------------------------
    ! Routine to evaluate the gradient of a Fourier expansion of type
    !
    !       V(pos_i) = const. + sum_n^Nreal A_n sum_j^Nlattice cos(n * G_j.dot.pos_i)  
    !                         + sum_m^Nimag B_m sum_j^Nlattice sin(m * H_j.dot.pos_i)  
    !
    ! ie.,
    !
    !       grad(pos_i, k) = sum_n^Nreal (-n*A_n) sum_j^Nlattice G(j,k) * sin(G_j.dot.pos_i)
    !                        + sum_m^Nimag (m*B_m) sum_j^Nlattice H(j,k) * cos(H_j.dot.pos_i)
    ! Parameters
    ! ----------
    ! positions : (Ndim, Natoms) array (in)
    !     The positions at which to evaluate the expansion. Note that this is
    !     the transpose of the pythonic array.
    !
    ! gradients : (Ndim, Natoms) array (inout)
    !     The array in which we store the gradient values.
    !
    ! G : (Ndim, Nlattice) array (in)
    !     The reciprocal lattice vectors for the real part of the expansion. 
    !
    ! H : (Ndim, Nlattice) array (in)
    !     The reciprocal lattice vectors for the imaginary part of the 
    !     expansion.
    !
    ! real_coeff: (Nreal) array (in)
    !     The expansion coefficients for the real part.
    !
    ! imag_coeff: (Nimag) array (in)
    !     The expansion coefficients for the imaginary part.
    ! -------------------------------------------------------------------------
    implicit none
    
    ! positions, expected (Ndim, Natoms)
    double precision, dimension(:,:), intent(in) :: positions
    
    ! The actual gradients
    ! Assume (Ndim, Natoms) for maximum speed, make sure to transpose again
    ! afterwards!
    double precision, dimension(:,:), intent(inout) :: gradients
    
    ! The real (G) and imaginary (H) reciprocal lattice vectors
    ! expected (Ndim, Nlattice)
    double precision, dimension(:,:), intent(in) :: G
    double precision, dimension(:,:), intent(in) :: H
    
    ! the coefficients of the Fourier expansion
    ! distinguish real and imaginary coefficients in different arrays
    ! this makes it easier to handle the evaluation
    double precision, dimension(:), intent(in) :: real_coeff
    double precision, dimension(:), intent(in) :: imag_coeff
 
    
    ! the array to store the dot products (do it only once)
    ! dimensionality is (Nlattice x Natoms) in the end
    double precision :: G_dot_pos
    double precision :: H_dot_pos


    ! the dimensions....
    integer :: Ndim, Nlattice, Natoms, Nreal, Nimag

    ! helper 
    integer :: i, j, m, n
    
    ! Note that G and H enter as (Ndim, Nlattice) arrays, ie. the transpose of
    ! the sourrounding python framework
    Ndim = size(G, 1)
    Nlattice = size(G,2)

    Natoms = size(positions,2)
    Nreal = size(real_coeff)
    Nimag = size(imag_coeff)


    ! zero the gradients, just in case
    gradients = 0.d0
    
    ! the real-part loop
    do i = 1, Natoms
        do j = 1, Nlattice

            ! The dot product by hand
!            G_dot_pos = 0.d0
!            H_dot_pos = 0.d0
!            do k = 1, Ndim
!                G_dot_pos = G_dot_pos + positions(k,i) * G(k,j)
!                H_dot_pos = H_dot_pos + positions(k,i) * H(k,j)
!            end do
            
            ! let's just go with the dot products
            G_dot_pos = dot_product(positions(:,i), G(:,j))
            H_dot_pos = dot_product(positions(:,i), H(:,j))

            do n = 1, Nreal
                gradients(:,i) = gradients(:,i) - n * real_coeff(n) * G(:,j) * sin(n * G_dot_pos)
            end do
            do m = 1, Nimag
                gradients(:,i) = gradients(:,i) + m * imag_coeff(m) * H(:,j) * cos(m * H_dot_pos)
            end do
        end do
    end do

end subroutine evaluate_fourier_gradient

!subroutine evaluate_fourier_expansion(positions, &
!&                                     values,&
!&                                     G,&
!&                                     H,&
!&                                     real_coeff,&
!&                                     imag_coeff,&
!&                                     constant)
!    ! vectorized call... probably some overhead for single position call
!    ! note that positions must be fortran contigiously stored, otherwise
!    ! blas will kill you

!    implicit none

!    ! positions in real space: please, call with F-contigious array
!    ! otherwise blas will kill you ;)
!    ! indexing is (Natoms x Ndim) , BLAS takes care of properly transposing
!    ! things
!    double precision, dimension(:,:), intent(in) :: positions
!    
!    ! The actual values
!    double precision, dimension(:), intent(inout) :: values
!    
!    ! The real (G) and imaginary (H) reciprocal lattice vectors
!    ! Note that G is (nlattice_g x ndim) and H is (nlattice_h x ndim)
!    double precision, dimension(:,:), intent(in) :: G
!    double precision, dimension(:,:), intent(in) :: H
!    
!    ! the coefficients of the Fourier expansion
!    ! distinguish real and imaginary coefficients in different arrays
!    ! this makes it easier to handle the evaluation
!    double precision, dimension(:), intent(in) :: real_coeff
!    double precision, dimension(:), intent(in) :: imag_coeff

!    
!    ! the array to store the dot products (do it only once)
!    ! dimensionality is (Nlattice x Natoms) in the end
!    double precision, dimension(:,:), allocatable :: G_dot_products
!    double precision, dimension(:,:), allocatable :: H_dot_products
!    
!    ! The constant offset
!    double precision, intent(in) :: constant 


!    ! the dimensions....
!    integer :: Ndim, Nlattice_G, Nlattice_H, Natoms, Nreal, Nimag

!    ! helper 
!    integer :: n, m, k, stat

!    Nlattice_G = size(G,1)
!    Nlattice_H = size(H,1)
!    Ndim = size(G, 2)
!    Natoms = size(values)
!    Nreal = size(real_coeff)
!    Nimag = size(imag_coeff)

!    allocate(G_dot_products(Nlattice_G, Natoms), stat = stat)
!    if (stat .ne. 0) write(*,*) "Error allocating G_dot_products" 
!    
!    allocate(H_dot_products(Nlattice_H, Natoms), stat = stat)
!    if (stat .ne. 0) write(*,*) "Error allocating H_dot_products" 

!    
!    ! calculate the dot products via matrix multiplication
!    ! need some transpose for the loop afterwards
!    ! We want G/H_dot_products to be (Nlattice x Natoms)
!    ! Such that we can loop over the fast index.
!    ! Hence it must result from (Nlattice x Ndim) * (Ndim x Natoms)
!    ! i.e. G/H .dot. positions^T
!    call DGEMM('n', 't', Nlattice_G, Natoms, Ndim, 1.d0, G, Nlattice_G, positions, Natoms, 0.d0, G_dot_products, Nlattice_G)
!    call DGEMM('n', 't', Nlattice_H, Natoms, Ndim, 1.d0, H, Nlattice_H, positions, Natoms, 0.d0, H_dot_products, Nlattice_H)
!    values = constant
!    
!    ! the real-part loop
!    do n = 1, Natoms
!        do m = 1, Nreal
!            do k = 1, Nlattice_G
!                values(n) = values(n) + real_coeff(m) * cos(m*G_dot_products(k,n))
!            end do
!        end do
!! no need to break this loop here
!!    end do
!!    
!!    ! the imaginary-part loop
!!    do n = 1, Natoms
!        do m = 1, Nimag
!            do k = 1, Nlattice_H
!                values(n) = values(n) + imag_coeff(m) * sin(m*H_dot_products(k,n))
!            end do
!        end do
!    end do
!    
!    deallocate(G_dot_products, stat = stat)
!    deallocate(H_dot_products, stat = stat)

!end subroutine evaluate_fourier_expansion


!subroutine evaluate_fourier_gradient(positions, gradients, G, H, real_coeff, imag_coeff)
!    ! vectorized call... probably some overhead for single position call
!    ! note that positions must be fortran contigiously stored, otherwise
!    ! blas will kill you

!    implicit none

!    ! positions in real space: please, call with F-contagious array
!    ! otherwise blas will kill you ;)
!    ! Assumed indexing is (Natoms x Ndim), blas takes care of transposing things
!    double precision, dimension(:,:), intent(in) :: positions
!    
!    ! The actual gradients
!    ! Assume (Ndim x Natoms) for maximum speed, make sure to transpose again
!    ! afterwards!
!    double precision, dimension(:,:), intent(inout) :: gradients
!    
!    ! The real (G) and imaginary (H) reciprocal lattice vectors
!    ! Note that G is (nlattice_g x ndim) and H is (nlattice_h x ndim)
!    
!    double precision, dimension(:,:), intent(in) :: G
!    double precision, dimension(:,:), intent(in) :: H
!    
!    ! the coefficients of the Fourier expansion
!    ! distinguish real and imaginary coefficients in different arrays
!    ! this makes it easier to handle the evaluation
!    double precision, dimension(:), intent(in) :: real_coeff
!    double precision, dimension(:), intent(in) :: imag_coeff

!    
!    ! the array to store the dot products (do it only once)
!    ! dimensionality is (Nlattice x Natoms) in the end
!    double precision, dimension(:,:), allocatable :: G_dot_products
!    double precision, dimension(:,:), allocatable :: H_dot_products
!    

!    ! the dimensions....
!    integer :: Ndim, Nlattice_G, Nlattice_H, Natoms, Nreal, Nimag

!    ! helper 
!    integer :: n, m, k, j, stat

!    Nlattice_G = size(G,1)
!    Nlattice_H = size(H,1)
!    Ndim = size(G, 2)
!    Natoms = size(positions,1)
!    Nreal = size(real_coeff)
!    Nimag = size(imag_coeff)

!    allocate(G_dot_products(Nlattice_G, Natoms), stat = stat)
!    if (stat .ne. 0) write(*,*) "Error allocating G_dot_products" 
!    
!    allocate(H_dot_products(Nlattice_H, Natoms), stat = stat)
!    if (stat .ne. 0) write(*,*) "Error allocating H_dot_products" 

!    
!    ! calculate the dot products via matrix multiplication
!    ! need some transpose for the loop afterwards
!    ! We want G/H_dot_products to be (Nlattice x Natoms)
!    ! Such that we can loop over the fast index.
!    ! Hence it must result from (Nlattice x Ndim) * (Ndim x Natoms)
!    ! i.e. G/H .dot. positions^T
!    
!    call DGEMM('n', 't', Nlattice_G, Natoms, Ndim, 1.d0, G, Nlattice_G, positions, Natoms, 0.d0, G_dot_products, Nlattice_G)
!    call DGEMM('n', 't', Nlattice_H, Natoms, Ndim, 1.d0, H, Nlattice_H, positions, Natoms, 0.d0, H_dot_products, Nlattice_H)

!    ! zero the gradients, just in case
!    gradients = 0.d0
!    
!    ! the real-part loop
!    do n = 1, Natoms
!        do m = 1, Nreal
!            do k = 1, Nlattice_G
!                gradients(:,n) = gradients(:,n) - m * real_coeff(m) * G(k,:) *sin(m* G_dot_products(k, n))
!            end do
!        end do
!    end do
!    
!    ! the imaginary-part loop
!    do n = 1, Natoms
!        do m = 1, Nimag
!            do k = 1, Nlattice_H
!                gradients(:,n) = gradients(:,n) + m * imag_coeff(m) * H(k,:) *cos(m* H_dot_products(k, n))
!            end do
!        end do
!    end do


!    deallocate(G_dot_products, stat = stat)
!    deallocate(H_dot_products, stat = stat)
!end subroutine evaluate_fourier_gradient

