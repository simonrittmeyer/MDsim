! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

subroutine construct_neighbor_lists(positions, &
&                                   list_cutoff, &
&                                   neighbor_lists, &
&                                   num_neighbors)

    ! -------------------------------------------------------------------------
    ! Routine to evaluate neighbor lists. Adapted from Frenkel/Smit
    ! 
    ! * NO PERIODIC BOUNDARY CONDITIONS *
    !
    ! Parameters
    ! ----------
    ! positions : (Ndim x Natoms) array (in)
    !     The actual positions. *Note that this is the transpose of the pythonic
    !     array*.
    !
    ! list_cutoff : float (in)
    !     The list cutoff. Any distance pair beyond this cutoff is not
    !     considered a pair of neighbors
    !     
    ! neighbor_lists : (Natoms x Natoms) array (inout)
    !     The actual neighbor listst. Make sure to pass an F-contigious array,
    !     otherwise this routine and its python wrapping respectively will fail.
    !     
    !     A little bit of care must be taken with these neighbor lists. Not all
    !     elements are meaningful. This is simply due to the fact that not every
    !     atom has Natom neighbors. Each entry neighbor_lists(i) is an array of
    !     Natoms elements (the neighbors for atom i), yet only some first 
    !     elements are meaningful. All other elements should be "-1" if
    !     everything is ok.
    !
    !     You find the meaningful elements from the helper variable
    !     "num_neighbors". Usually as an average user you do not need to worry 
    !     about these details. The routines requiring the neighbor lists should 
    !     just be aware of this.
    !
    !     NOTE: The indices in these lists correspond to pythonic notation, ie.,
    !     they start with 0. Moreover, follwing python logics, you will have to
    !     transpose this array afterwards.
    ! 
    ! num_neighbors : (Natoms) array (inout) 
    !     An array that gives us the number of neighbors of atom i. Crucial in
    !     order not to run into troubles with the neighbor lists.
    ! -------------------------------------------------------------------------
    implicit none
    
    
    ! we do not consider assumed shape to make life easier with f2py
    double precision, dimension(:,:), intent(in) :: positions
    double precision, intent(in) :: list_cutoff
    ! watch out, we denote neighbor_lists(:,i) the neighbor list of atom i,
    ! hence transpose the thing!
    integer, dimension(:,:), intent(inout) :: neighbor_lists
    integer, dimension(:), intent(inout) :: num_neighbors

    integer :: i,j,k 
    integer :: Natoms, Ndim 

    double precision :: dist=0.d0, tmp=0.d0, list_cutoff_square
    
    Ndim = size(positions,1)
    Natoms = size(positions,2)

    ! zero the neighbor_lists and number of neighbors
    ! important: for each entry neighbor_lists(i), only elements from 
    !            1 to num_neighbors(i) are meaningfull
    neighbor_lists = -1
    num_neighbors = 0

    ! only do it once
    list_cutoff_square = list_cutoff**2

    do i=1, Natoms-1
        do j= i+1, Natoms
            dist=0.d0
            ! it is absolutely sufficient to go for the squares
            do k=1, Ndim
                tmp = positions(k,i) - positions(k,j)
                dist = dist + tmp * tmp
            end do 
            if (dist .lt. list_cutoff_square) then
                ! both are neighbors to each other
                num_neighbors(i) = num_neighbors(i) + 1
                ! (note: python indexing, starting with 0!)
                neighbor_lists(num_neighbors(i), i) = j-1
            end if
        end do
    end do

end subroutine construct_neighbor_lists


subroutine construct_neighbor_lists_mic(positions, &
&                                       unit_cell_diag, &
&                                       list_cutoff, &
&                                       neighbor_lists, &
&                                       num_neighbors)

    ! -------------------------------------------------------------------------
    ! Routine to evaluate neighbor lists. Adapted from Frenkel/Smit
    ! 
    ! * MINIMUM IMAGE CONVENTION IS ACKNOWLEDGED *
    ! 
    ! Parameters
    ! ----------
    ! positions : (Ndim x Natoms) array (in)
    !     The actual positions. *Note that this is the transpose of the pythonic
    !     array*.
    !
    ! unit_cell_diag : (Ndim) array (in)
    !     The diagonal elements of the cell vector matrix. Note that this
    !     assumes a orthorhombic cell in the coordinates we are working in.
    !
    ! list_cutoff : float (in)
    !     The list cutoff. Any distance pair beyond this cutoff is not
    !     considered a pair of neighbors
    !     
    ! neighbor_lists : (Natoms x Natoms) array (inout)
    !     The actual neighbor listst. Make sure to pass an F-contigious array,
    !     otherwise this routine and its python wrapping respectively will fail.
    !     
    !     A little bit of care must be taken with these neighbor lists. Not all
    !     elements are meaningful. This is simply due to the fact that not every
    !     atom has Natom neighbors. Each entry neighbor_lists(:,i) is an array of
    !     Natoms elements (the neighbors for atom i), yet only some first 
    !     elements are meaningful. All other elements should be "-1" if
    !     everything is ok.
    !
    !     You find the meaningful elements from the helper variable
    !     "num_neighbors". Usually as an average user you do not need to worry 
    !     about these details. The routines requiring the neighbor lists should 
    !     just be aware of this.
    !
    !     NOTE: The indices in these lists correspond to pythonic notation, ie.,
    !     they start with 0. Moreover, follwing python logics, you will have to
    !     transpose this array afterwards.
    ! 
    ! num_neighbors : (Natoms) array (inout) 
    !     An array that gives us the number of neighbors of atom i. Crucial in
    !     order not to run into troubles with the neighbor lists.
    ! -------------------------------------------------------------------------
    implicit none
    
    
    ! we do not consider assumed shape to make life easier with f2py
    double precision, dimension(:,:), intent(in) :: positions
    double precision, dimension(:), intent(in) :: unit_cell_diag 
    double precision, intent(in) :: list_cutoff

    ! watch out, we denote neighbor_lists(:,i) the neighbor list of atom i,
    ! hence transpose the thing!
    integer, dimension(:,:), intent(inout) :: neighbor_lists
    integer, dimension(:), intent(inout) :: num_neighbors

    integer :: i,j,k 
    integer :: Natoms, Ndim 

    double precision :: dist=0.d0, tmp=0.d0, list_cutoff_square
    
    Ndim = size(positions,1)
    Natoms = size(positions,2)

    ! zero the neighbor_lists and number of neighbors
    ! important: for each entry neighbor_lists(:,i), only elements from 
    !            1 to num_neighbors(i) are meaningfull
    neighbor_lists = -1
    num_neighbors = 0

    ! only do it once
    list_cutoff_square = list_cutoff**2

    do i=1, Natoms-1
        do j= i+1, Natoms
            dist=0.d0
            ! it is absolutely sufficient to go for the squares
            do k=1, Ndim
                tmp = positions(k,i) - positions(k,j)
                ! here comes the MIC
                tmp = tmp - unit_cell_diag(k) * ANINT(tmp / unit_cell_diag(k))
                dist = dist + tmp * tmp
            end do 
            if (dist .lt. list_cutoff_square) then
                num_neighbors(i) = num_neighbors(i) + 1
                ! (note: python indexing, starting with 0!)
                neighbor_lists(num_neighbors(i), i) = j-1
            end if
        end do
    end do

end subroutine construct_neighbor_lists_mic
