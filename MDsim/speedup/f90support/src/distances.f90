! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

subroutine calc_distances_mic(positions, &
&                             unit_cell_diag, &
&                             distances)
  ! Orthorhombic cells only
  ! Evaluate Kohn-Lau interaction energy by directly calculating all distances    
  ! compute distances considering the minimum image convention
  double precision, dimension(:,:), intent(in) :: positions
  double precision, dimension(:), intent(in) :: unit_cell_diag
  double precision, dimension(:,:), intent(inout) :: distances

  double precision :: dist, tmp 

  integer :: Natoms, Ndim 
  integer :: i,j,k 

  ! assume that positions is (Ndim x Natoms), ie the transpose of the
  ! Python array
  Ndim = size(positions, 1)
  Natoms = size(positions,2)

  ! zero energy
  distances = 0.d0
	
  do i=1, Natoms-1
    do j=i+1, Natoms
      dist=0.d0
      ! minimum image convention
      do k=1, Ndim
        tmp = positions(k,i) - positions(k,j)
        ! here comes the MIC
        tmp = tmp - unit_cell_diag(k) * ANINT(tmp / unit_cell_diag(k))
        dist = dist + tmp * tmp
      end do 

      ! do not forget that we need distances here, not the squares!
      dist=sqrt(dist)
      distances(i, j) = dist
      distances(j, i) = dist
    end do
  end do
end subroutine calc_distances_mic

