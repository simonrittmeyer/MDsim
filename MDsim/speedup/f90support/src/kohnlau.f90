! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

module kohnlau
    implicit none
    
    contains
        ! ---------------------
        ! BASIC ROUTINES 
        ! ---------------------

!        subroutine kohnlau_potential(mu_i, mu_j, dist, energy)
!            ! Note that dist is not a vector here!
!            implicit none
!            double precision, intent(in) :: mu_i, mu_j, dist
!            double precision, intent(out) :: energy
!            energy = 2.d0 * mu_i * mu_j / dist**3 
!        end subroutine kohnlau_potential
        
        double precision function kohnlau_potential(mu_i, mu_j, dist)
            ! Note that dist is not a vector here!
            double precision :: mu_i, mu_j, dist
            kohnlau_potential = 2.d0 * mu_i * mu_j / dist**3 
        end function kohnlau_potential
        
        subroutine kohnlau_force(mu_i, mu_j, dist, distance, force)
            ! Note that dist is not a vector here but distance is!
            double precision, intent(in) :: mu_i, mu_j, dist
            double precision, dimension(:), intent(in) :: distance
            double precision, dimension(:), intent(inout) :: force
            force = 6.d0 * mu_i * mu_j * distance / dist**5 
        end subroutine kohnlau_force
   

        ! ---------------------
        ! BRUTE FORCE 
        ! ---------------------

        subroutine calc_forces_bruteforce(positions,& 
&                                         dipoles, &
&                                         unit_cell_diag, &
&                                         cutoff, &
&                                         forces)
            ! Orthorhombic cells only
            ! Evaluate Kohn-Lau forces by directly calculating all distances    
            ! compute distances considering the minimum image convention
            double precision, dimension(:,:), intent(in) :: positions
            double precision, dimension(:), intent(in) :: dipoles 
            double precision, dimension(:), intent(in) :: unit_cell_diag
            double precision, intent(in) :: cutoff
            ! This little guy is supposed to be (Ndim x Natoms) -- remember to
            ! transpose!
            double precision, dimension(:,:), intent(inout) :: forces
            

            double precision :: dist=0.d0, cutoff_square=0.d0 
            double precision, dimension(:), allocatable :: distance 
            double precision, dimension(:), allocatable :: force 
            
            integer :: Natoms, Ndim 
            integer :: stat
            integer :: i,j
            
            ! assume we get the transpose of the python array
            Ndim = size(positions,1)
            Natoms = size(positions,2)
            
            allocate(distance(Ndim), stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error allocating distance vector" 
            end if

            allocate(force(Ndim), stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error allocating force" 
            end if
            
            ! zero all forces (assume forces is (Ndim x Natoms))
            forces = 0.d0
            
            cutoff_square = cutoff**2
            do i=1, Natoms-1
                do j=i+1, Natoms
                    distance = positions(:,i) - positions(:,j)
                    ! here come the MIC
                    distance = distance - unit_cell_diag * ANINT(distance / unit_cell_diag)
                    dist = dot_product(distance, distance)
                    ! minimum image convention
                    if (dist .le. cutoff_square) then
                        ! only evaluate sqrt if necessary
                        dist = sqrt(dist)
                        !force = 6.d0 * dipoles(i) * dipoles(j) * distance * dist**-5 
                        ! Fij = -Fji due to Newton
                        call kohnlau_force(dipoles(i), dipoles(j), dist, distance, force)
                        forces(:,i) = forces(:,i) + force
                        forces(:,j) = forces(:,j) - force
                    end if
                end do
            end do
            deallocate(distance, stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error deallocating distance vector" 
            end if
            deallocate(force, stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error deallocating force vector" 
            end if
        end subroutine calc_forces_bruteforce


        subroutine calc_energy_bruteforce(positions, &
&                                         dipoles, &
&                                         unit_cell_diag, &
&                                         cutoff, &
&                                         energy)
            ! Orthorhombic cells only
            ! Evaluate Kohn-Lau interaction energy by directly calculating all distances    
            ! compute distances considering the minimum image convention
            double precision, dimension(:,:), intent(in) :: positions
            double precision, dimension(:), intent(in) :: dipoles 
            double precision, dimension(:), intent(in) :: unit_cell_diag
            double precision, intent(in) :: cutoff
            double precision, intent(out) :: energy
            
            double precision :: dist, tmp, cutoff_square, energy_correction

            integer :: Natoms, Ndim 
            integer :: i,j,k 

            ! assume that positions is (Ndim x Natoms), ie the transpose of the
            ! Python array
            Ndim = size(positions, 1)
            Natoms = size(positions,2)
            
            ! zero energy
            energy = 0.d0

            ! to avoid discontinuities // just made some homopolar systems
            energy_correction = kohnlau_potential(dipoles(1), dipoles(1), cutoff)
            
            cutoff_square = cutoff**2
            do i=1, Natoms-1
                do j=i+1, Natoms
                    dist=0.d0
                    ! minimum image convention
                    do k=1, Ndim
                        tmp = positions(k,i) - positions(k,j)
                        ! here comes the MIC
                        tmp = tmp - unit_cell_diag(k) * ANINT(tmp / unit_cell_diag(k))
                        dist = dist + tmp * tmp
                    end do 
                    if (dist .le. cutoff_square) then
                        ! evaluate the square root only if necessary...
                        dist = sqrt(dist)
                        energy = energy + kohnlau_potential(dipoles(i), dipoles(j), dist) - energy_correction
                        !energy = energy +  2.d0 * dipoles(i) * dipoles(j) * dist**-3 
                    end if
                end do
            end do
        end subroutine calc_energy_bruteforce

        ! ---------------------
        ! USING NEIGHBOUR LISTS
        ! ---------------------

        subroutine calc_forces_neighbor(positions, &
&                                       dipoles, &
&                                       unit_cell_diag, &
&                                       neighbor_lists,&
&                                       num_neighbors, &
&                                       cutoff, &
&                                       forces)
            
            ! Evaluate Kohn-Lau forces using neighbor lists    
            double precision, dimension(:,:), intent(in) :: positions
            double precision, dimension(:), intent(in) :: dipoles 
            double precision, dimension(:), intent(in) :: unit_cell_diag
            double precision, intent(in) :: cutoff
            
            ! remember to transpose the python list
            integer, dimension(:,:), intent(in) :: neighbor_lists
            
            integer, dimension(:), intent(in) :: num_neighbors

            ! assume that this little guy is (Ndim x Natoms) -- remember to transpose
            double precision, dimension(:,:), intent(inout) :: forces
            
            double precision, dimension(:), allocatable :: distance, force
            
            double precision :: dist, cutoff_square
            
            integer :: Natoms, Ndim 
            integer :: stat
            integer :: i,j,n 
            
            ! assume that positions is (Ndim, Natoms)
            Ndim = size(positions, 1)
            Natoms = size(positions,2)
            
            ! allocate distance vector
            allocate(distance(Ndim), stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error allocating distance vector" 
            end if
            
            ! allocate force vector
            allocate(force(Ndim), stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error allocating force vector" 
            end if
            
            ! zero the forces
            forces = 0.d0
            
            cutoff_square = cutoff**2
            do i=1, Natoms-1
                ! n is the index within the neighbor_list, ie. not! the neighbor atom
                ! index in the positions list
                do n=1, num_neighbors(i)
                    ! j is the partner atom index (but in pythonic notation,
                    ! hence add a +1)!
                    j = neighbor_lists(n, i)+1
                    ! only calculate for those which have not yet been accounted for as
                    ! neighbor of a particle of lower index -- this is
                    ! automatically taken care of as neighbor lists only contain
                    ! neighbors of higher indices!
                    distance = positions(:,i) - positions(:,j)
                    ! here comes the MIC
                    distance = distance - unit_cell_diag * ANINT(distance / unit_cell_diag)
                    dist = dot_product(distance, distance)
                    if (dist .le. cutoff_square) then
                        ! only evaluate sqrt if necessary
                        dist = sqrt(dist)
                        call kohnlau_force(dipoles(i), dipoles(j), dist, distance, force)
                        ! Fij = -Fji due to Newton
                        forces(:,i) = forces(:,i) + force
                        forces(:,j) = forces(:,j) - force
                    end if
                end do
            end do
            deallocate(distance, stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error deallocating distance vector" 
            end if
            deallocate(force, stat = stat)
            if (stat .ne. 0) then
               write(*,*) "Error deallocating force vector" 
            end if
        end subroutine calc_forces_neighbor


        subroutine calc_energy_neighbor(positions, &
&                                       dipoles, &
&                                       unit_cell_diag, &
&                                       neighbor_lists, &
&                                       num_neighbors, &
&                                       cutoff, &
&                                       energy)
            ! Evaluate Kohn-Lau forces using neighbor lists    
            ! Note that no tail correction is made here!
            double precision, dimension(:,:), intent(in) :: positions
            double precision, dimension(:), intent(in) :: dipoles 
            double precision, dimension(:), intent(in) :: unit_cell_diag
            double precision, intent(in) :: cutoff
            integer, dimension(:,:), intent(in) :: neighbor_lists
            integer, dimension(:), intent(in) :: num_neighbors
            double precision, intent(out) :: energy
            
            double precision :: dist, tmp, cutoff_square, energy_correction
            
            integer :: Natoms, Ndim 
            integer :: i,j,k,n 
            
            ! assume that positions is (Ndim x Natoms)
            Ndim = size(positions, 1)
            Natoms = size(positions,2)
            
            ! zero the energy
            energy = 0.d0
            
            ! to avoid discontinuities // just made some homopolar systems
            energy_correction = kohnlau_potential(dipoles(1), dipoles(1), cutoff)
            
            cutoff_square = cutoff**2
            do i=1, Natoms-1
                ! n is the index within the neighbor_list, ie. not! the neighbor atom
                ! index in the positions list
                
                do n=1, num_neighbors(i)
                    ! j is the partner atom index (but in pythonic notation,
                    ! hence add a +1)!
                    j = neighbor_lists(n, i)+1
                    ! only calculate for those which have not yet been accounted for as
                    ! neighbor of a particle of lower index -- this is
                    ! automatically taken care of as neighbor lists only contain
                    ! neighbors of higher indices!
                    dist = 0.d0
                    ! minimum image convention
                    do k=1, Ndim
                        tmp = positions(k,i) - positions(k,j)
                        ! here comes the MIC
                        tmp = tmp - unit_cell_diag(k) * ANINT(tmp / unit_cell_diag(k))
                        dist = dist + tmp * tmp
                    end do 
                    if (dist .lt. cutoff_square) then
                        dist = sqrt(dist)
                        energy = energy + kohnlau_potential(dipoles(i), dipoles(j), dist) - energy_correction
                    end if
                end do
            end do
        end subroutine calc_energy_neighbor

end module
