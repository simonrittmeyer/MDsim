! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

subroutine get_distances(positions, distances)
    ! Orthorhombic cells only
    ! compute distances considering the minimum image convention
    implicit none
    double precision, dimension(:,:), intent(in) :: positions
    double precision, dimension(:,:), intent(inout) :: distances
    
    double precision :: dist 
    double precision, dimension(:), allocatable :: vdist 
    
    double precision, external :: DNRM2

    integer :: Natoms, Ndim 
    integer :: stat
    integer :: i,j 

    ! assume that positions is (Natoms, Ndim)
    ! The array is stored in memory C-contageous!
    Ndim = size(positions, 2)
    Natoms = size(positions,1)
    
    allocate(vdist(Ndim), stat = stat)
    if (stat .ne. 0) then
       write(*,*) "Error allocating vdist" 
    endif

    ! zero all distances
    distances = 0.d0

    do i=1, Natoms
        do j=i+1, Natoms
            vdist = positions(i,:) - positions(j,:)
            dist = DNRM2(Ndim, vdist, 1)
            ! same distance ij <--> ji
            distances(i,j) = dist
            distances(j,i) = dist
        end do
    end do

    deallocate(vdist, stat = stat)
    if (stat .ne. 0) then
       write(*,*) "Error deallocating vdist" 
    endif
end subroutine get_distances

subroutine get_distances_mic(positions, unit_cell_diag, distances)
    ! Diagonal cells only
    ! compute distances considering the minimum image convention
    implicit none
    double precision, dimension(:,:), intent(in) :: positions
    double precision, dimension(:), intent(in) :: unit_cell_diag 
    double precision, dimension(:,:), intent(inout) :: distances
    
    double precision :: dist 
    double precision, dimension(:), allocatable :: vdist 
    
    double precision, external :: DNRM2

    integer :: Natoms, Ndim 
    integer :: stat
    integer :: i,j,k 

    ! assume that positions is (Natoms, Ndim)
    ! The array is stored in memory C-contageous!
    Ndim = size(positions, 2)
    Natoms = size(positions,1)
    
    allocate(vdist(Ndim), stat = stat)
    if (stat .ne. 0) then
       write(*,*) "Error allocating vdist" 
    endif
    
    ! zero all distances
    distances = 0.d0

    do i=1, Natoms
        do j=i+1, Natoms
            vdist = positions(i,:) - positions(j,:)
            ! minimum image convention
            do k=1, Ndim
                vdist(k) = vdist(k) - unit_cell_diag(k) * ANINT(vdist(k) / unit_cell_diag(k))
            end do
            dist = DNRM2(Ndim, vdist, 1)
            ! same distance ij <--> ji
            distances(i,j) = dist
            distances(j,i) = dist
        end do
    end do

    deallocate(vdist, stat = stat)
    if (stat .ne. 0) then
       write(*,*) "Error deallocating vdist" 
    endif
end subroutine get_distances_mic
