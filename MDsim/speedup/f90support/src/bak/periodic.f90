! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

subroutine get_positions_frac_orthorhombic(positions, unit_cell_diag, positions_frac)
    ! Calculate positions in an orthorhombic cell
    ! Actually, this is a bit of too much overhead, numpy is just as good or
    ! even better...
    ! Note that this cell has to be "diagonal" in Carthesian coordinates.
    ! In case it is not, transform it correspondingly... But this can be done
    ! once upon initialization
    implicit none
    double precision, dimension(:,:), intent(in) :: positions
    double precision, dimension(:), intent(in) :: unit_cell_diag
    double precision, dimension(:,:), intent(inout) :: positions_frac
    
    integer :: Natoms, Ndim 
    integer :: i,j 

    ! assume that positions is (Natoms, Ndim)
    ! The array is stored in memory C-contageous!
    Ndim = size(positions, 2)
    Natoms = size(positions,1)
    
    positions_frac = 0.d0
    
    ! positions is 'C' contageous while positions_frac is 'F' contagious
    ! that's not optimal, I know...
    do i=1, Natoms
        do j=1, Ndim
            positions_frac(i,j) = positions(i,j) / unit_cell_diag(j)
        end do
    end do
end subroutine get_positions_frac_orthorhombic
