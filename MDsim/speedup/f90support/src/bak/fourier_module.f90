! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! May be problematic if we require more than once 'instance' as global variables
! may interfere in these cases
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! all input as F-contigious arrays, please!!!!
module fourierexpansion
    implicit none
    
    ! nomen clature follows Matt Probert's Preprint...
    type reciprocal_lattice
        ! The real (G) and imaginary (H) reciprocal lattice vectors
        ! The will be stored as (Ndim x Nbasis)
        double precision, dimension(:,:), allocatable :: G
        double precision, dimension(:,:), allocatable :: H
        ! dimensionality of real space vectors
        integer :: ndim 
        ! number of reciprocal lattice vectors in real/imag space
        integer :: nlattice
    end type
   
    type fourier_coefficients
        ! expansion coefficients
        double precision, dimension(:), allocatable :: real_coeff
        double precision, dimension(:), allocatable :: imag_coeff
        double precision :: constant
        integer :: nreal
        integer :: nimag
        logical :: nequal
    end type 

    ! the module types...
    ! mark as private as otherwise f2py tries to interface them.... and fails!
    type(reciprocal_lattice), private :: lattice
    type(fourier_coefficients), private :: coefficients
    logical, private :: initialized = .false.

    contains

    subroutine init(G, H, real_coeff, imag_coeff, constant)
        implicit none

        ! The real (G) and imaginary (H) reciprocal lattice vectors
        ! Note that lattice%G is (ndim x nlattice) as the first index is the fast
        ! But the input G is from Python and thus better (nlattice x ndim)
        ! But we will just transpose once in this routine
        double precision, dimension(:,:), intent(in) :: G
        double precision, dimension(:,:), intent(in) :: H
        
        ! the coefficients of the Fourier expansion
        ! distinguish real and imaginary coefficients in different arrays
        ! this makes it easier to handle the evaluation
        double precision, dimension(:), intent(in) :: real_coeff
        double precision, dimension(:), intent(in) :: imag_coeff
        
        ! The constant offset
        double precision, intent(in) :: constant 

        integer :: stat

        ! First determine the dimensions and then allocate stuff
        coefficients%constant = constant
        coefficients%nreal = size(real_coeff)
        coefficients%nimag = size(imag_coeff)
        coefficients%nequal = coefficients%nreal .eq. coefficients%nimag

        ! Take care upon input or just transpose once...
        lattice%nlattice = size(G,1) 
        lattice%ndim = size(G,2)
     
        ! now we can allocate
        allocate(coefficients%real_coeff(coefficients%nreal), stat = stat)
        if (stat .ne. 0) write(*,*) "Error allocating real coefficients" 
        
        allocate(coefficients%imag_coeff(coefficients%nimag), stat = stat)
        if (stat .ne. 0) write(*,*) "Error allocating imaginary coefficients" 
        
        allocate(lattice%G(lattice%nlattice,lattice%ndim), stat = stat)
        if (stat .ne. 0) write(*,*) "Error allocating real reciprocal lattice vectors" 

        allocate(lattice%H(lattice%nlattice,lattice%ndim), stat = stat)
        if (stat .ne. 0) write(*,*) "Error allocating imag reciprocal lattice vectors" 

        ! assign
        coefficients%real_coeff = real_coeff
        coefficients%imag_coeff = imag_coeff

        ! assign, the transpose as this is better for the blas matmul later on
        lattice%G = G
        lattice%H = H

        initialized = .true.

    end subroutine init

    subroutine del()
        ! deallocate our stuff...
        implicit none
        integer :: stat
        deallocate(coefficients%real_coeff, stat = stat)
        deallocate(coefficients%imag_coeff, stat = stat)
        deallocate(lattice%G, stat = stat)
        deallocate(lattice%H, stat = stat)
    end subroutine del

    subroutine get_value(positions, values)
        ! vectorized call... probably some overhead for single position call
        ! note that positions must be fortran contigiously stored, otherwise
        ! blas will kill you

        implicit none
        ! positions in real space: please, call with F-contagious array!!!
        ! that will help lapack a lot!!!
        ! indexing is (Natoms x Ndim) where Ndim is the fast one
        double precision, dimension(:,:), intent(in) :: positions
        
        ! The actual value
        double precision, dimension(:), intent(inout) :: values
        
        ! the array to store the dot products (do it only once)
        ! dimensionality is (Nlattice x Natoms) in the end
        double precision, dimension(:,:), allocatable :: G_dot_products
        double precision, dimension(:,:), allocatable :: H_dot_products

        ! the dimensions....
        integer :: n, m, k, Natoms, stat

        Natoms = size(values)

        allocate(G_dot_products(lattice%nlattice, Natoms), stat = stat)
        if (stat .ne. 0) write(*,*) "Error allocating G_dot_products" 
        
        allocate(H_dot_products(lattice%nlattice, Natoms), stat = stat)
        if (stat .ne. 0) write(*,*) "Error allocating H_dot_products" 
    
        ! calculate the dot products via matrix multiplication
        ! need some transpose for the loop afterwards
        call DGEMM('t', 't', lattice%nlattice, Natoms, lattice%ndim, 1.d0, positions, 1, lattice%G, 1, 0.d0, G_dot_products, 1)
        call DGEMM('t', 't', lattice%nlattice, Natoms, lattice%ndim, 1.d0, positions, 1, lattice%H, 1, 0.d0, H_dot_products, 1)
        
        values = coefficients%constant
        
        
        if (coefficients%nequal .eqv. .true.) then 
            ! we can combine the loops if the number of coefficients is the same
            do n = 1, Natoms
                do m = 1, coefficients%nreal
                    do k = 1, lattice%nlattice
                        values(n) = values(n) + coefficients%real_coeff(m) &
                            &                 * cos(m*G_dot_products(n,k)) &
                            &                 + coefficients%imag_coeff(m) &
                            &                 * sin(m*H_dot_products(n,k))
                    end do
                end do
            end do

        ! Otherwise we run through separately (should make that much of a
        ! difference...)
        else
            ! the real-part loop
            do n = 1, Natoms
                do m = 1, coefficients%nreal
                    do k = 1, lattice%nlattice
                        values(n) = values(n) + coefficients%real_coeff(m) * cos(m*G_dot_products(n,k))
                    end do
                end do
            end do
            
            ! the imaginary-part loop
            do n = 1, Natoms
                do m = 1, coefficients%nimag
                    do k = 1, lattice%nlattice
                        values(n) = values(n) + coefficients%imag_coeff(m) * sin(m*H_dot_products(n,k))
                    end do
                end do
            end do
        end if
        
        deallocate(G_dot_products, stat = stat)
        deallocate(H_dot_products, stat = stat)

    end subroutine get_value

!    subroutine get_fourierexpansion_gradient(r, G, H, real_coeff, imag_coeff, gradient)
!        implicit none
!        
!        ! The real (G) and imaginary (H) reciprocal lattice vectors
!        double precision, dimension(:,:), intent(in) :: G
!        double precision, dimension(:,:), intent(in) :: H
!        
!        ! the coefficients of the Fourier expansion
!        ! distinguish real and imaginary coefficients in different arrays
!        ! this makes it easier to handle the evaluation
!        double precision, dimension(:), intent(in) :: real_coeff
!        double precision, dimension(:), intent(in) :: imag_coeff
!        
!        ! The constant offset
!        double precision, intent(in) :: constant 
!        
!        ! r is the position in real space
!        double precision, dimension(:), intent(in) :: r
!        
!        ! The actual value
!        double precision, dimension(:), intent(out) :: gradient
!        
!        ! the dimensions....
!        integer :: nimag, nreal, ndim, nbasis
!        integer :: n, m, j
!        

!        nreal = size(real_coeff)
!        nimag = size(imag_coeff)
!        
!        ! Note that G is (ndim x nbasis) as the first index is the fast
!        ! index
!        ndim = size(G,1)
!        nbasis = size(G,2)
!        
!        ! initialize gradient
!        gradient = 0.0
!         
!        ! the real-part loop
!        do j = 1, ndim
!            do n = 1, nreal
!                do m = 1, nbasis
!                gradient(j) = gradient(j) - real_coeff(n) * n * G(j,m) * sin(n * dot_product(G(:,m), r))
!                end do
!            end do
!        end do 
!        
!        ! the imaginary-part loop
!        do j = 1, ndim
!            do n = 1, nimag
!                do m = 1, nbasis
!                gradient(j) = gradient(j) + imag_coeff(n) * m * H(j,m) * cos(m * dot_product(H(:,m), r))
!                end do
!            end do
!        end do

!    end subroutine get_fourierexpansion_gradient

end module fourierexpansion
