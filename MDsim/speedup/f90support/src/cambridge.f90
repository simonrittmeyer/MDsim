! This file is part of MDsim.
! 
! MDsim is a simulation package for full-tensorial Langevin MD.
! Copyright (C) 2014-2017  Simon P. Rittmeyer
!
! MDsim is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3.0 of the License, or
! (at your option) any later version.
! 
! MDsim is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public License
! along with MDsim. If not, see <http://www.gnu.org/licenses/>.

subroutine evaluate_expansion_fcc100(positions, &
&                                    values, &
&                                    a, &
&                                    site1, &
&                                    site2)
    implicit none

    ! positions, expected (Ndim, Natoms)
    double precision, dimension(:,:), intent(in) :: positions
    
    ! The actual values
    double precision, dimension(:), intent(inout) :: values
    
    ! lattice constant of the surface 
    double precision, intent(in) :: a 
    
    ! expansion values at the two sites (third site is implicit)
    double precision, intent(in) :: site1, site2
    

    ! the dimensions....
    integer :: Ndim, Natoms

    ! helper products
    double precision :: x, y 

    ! helper 
    integer :: i
    
    ! pi
    double precision, parameter :: pi = 3.141592653589793d0

    ! evaluate the prefactors only once and not in the loop
    double precision :: constant 
    double precision :: args_pref
    double precision :: sum_pref 
    double precision :: prod_pref
    
    constant = site1 / 4. + site2 / 2.
    args_pref = 2 * pi / a
    sum_pref = site1 / 4.
    prod_pref = (site1 - 2*site2) / 4.
    
    ! Note that positions enters as (Ndim, Natoms) arrays, ie. the transpose of
    ! the sourrounding python framework
    Ndim = size(positions, 1)
    Natoms = size(positions,2)

    
    ! the loop over all atoms
    do i = 1, Natoms
        x = args_pref * positions(1,i)
        y = args_pref * positions(2,i)
        values(i) = constant + sum_pref * (cos(x) + cos(y)) + prod_pref * cos(x) * cos(y) 
    end do

end subroutine evaluate_expansion_fcc100



subroutine evaluate_gradient_fcc100(positions, &
&                                   gradients, &
&                                   a, &
&                                   site1, &
&                                   site2)
    implicit none

    ! positions, expected (Ndim, Natoms)
    double precision, dimension(:,:), intent(in) :: positions
    
    ! The actual values
    double precision, dimension(:,:), intent(inout) :: gradients
    
    ! lattice constant of the surface 
    double precision, intent(in) :: a 
    
    ! expansion values at the two sites (third site is implicit)
    double precision, intent(in) :: site1, site2
    

    ! the dimensions....
    integer :: Ndim, Natoms

    ! helper products
    double precision :: x, y 

    ! helper 
    integer :: i
    
    ! pi
    double precision, parameter :: pi = 3.141592653589793d0

    ! evaluate the prefactors only once and not in the loop
    double precision :: args_pref
    double precision :: sum_pref 
    double precision :: prod_pref 
    double precision :: sum_args_pref
    double precision :: prod_args_pref
        
    args_pref = 2 * pi / a
    sum_pref = site1 / 4.
    prod_pref = (site1 - 2*site2) / 4.
    sum_args_pref = sum_pref * args_pref
    prod_args_pref = prod_pref * args_pref

    ! Note that positions enters as (Ndim, Natoms) arrays, ie. the transpose of
    ! the sourrounding python framework
    Ndim = size(positions, 1)
    Natoms = size(positions,2)
    
    ! the loop over all atoms
    do i = 1, Natoms
        x = args_pref * positions(1,i)
        y = args_pref * positions(2,i)
        
        gradients(1,i) = -(sum_args_pref * sin(x) + prod_args_pref * sin(x)*cos(y)) 
        gradients(2,i) = -(sum_args_pref * sin(y) + prod_args_pref * sin(y)*cos(x)) 
    
    end do

end subroutine evaluate_gradient_fcc100
