#!/usr/bin/env bash

if [ $OSTYPE == 'darwin15.3.0' ]; then
    f2py="/opt/local/bin/f2py-2.7"
    fcompiler='gnu95'
else
    f2py="f2py"
    fcompiler='intelem'
fi

#$f2py -lblas -m distances -c src/distances.f90 --opt="-O3" --f90flags=$options
#$f2py -m neighbors -c src/neighbors.f90 --opt="-O3" --f90flags=$options --fcompiler=$fcompiler #-DF2PY_REPORT_ON_ARRAY_COPY=1
$f2py -m fourier -c src/fourier.f90 --opt="-O3" --fcompiler=$fcompiler #-DF2PY_REPORT_ON_ARRAY_COPY=1
$f2py -m neighbors -c src/neighbors.f90 --opt="-O3" --fcompiler=$fcompiler #-DF2PY_REPORT_ON_ARRAY_COPY=1
$f2py -m kohnlau -c src/kohnlau.f90 --opt="-O3" --fcompiler=$fcompiler #-DF2PY_REPORT_ON_ARRAY_COPY=1
$f2py -m cambridge -c src/cambridge.f90 --opt="-O3" --fcompiler=$fcompiler #-DF2PY_REPORT_ON_ARRAY_COPY=1
$f2py -m distances -c src/distances.f90 --opt="-O3" --fcompiler=$fcompiler #-DF2PY_REPORT_ON_ARRAY_COPY=1


