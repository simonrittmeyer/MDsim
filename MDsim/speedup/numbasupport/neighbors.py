# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numba

from MDsim.speedup.plainpython.neighbors import construct_neighbor_lists as construct_neighbor_lists_python
from MDsim.speedup.plainpython.neighbors import construct_neighbor_lists_mic as construct_neighbor_lists_mic_python

# yeah really, that's all ;)
construct_neighbor_lists = numba.decorators.autojit(construct_neighbor_lists_python)
construct_neighbor_lists_mic = numba.decorators.autojit(construct_neighbor_lists_mic_python)
