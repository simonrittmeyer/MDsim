# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import math

def evaluate_expansion_fcc100(positions, a, site1, site2):
    Natoms = positions.shape[0]
    Ndim = positions.shape[1]

    values = np.zeros(Natoms)

    # evaluate the prefactors only once and not in the loop
    constant = site1 / 4. + site2 / 2.

    args_pref = 2 * math.pi / a
    sum_pref = site1 / 4.
    prod_pref = (site1 - 2*site2) / 4.

    for i in range(Natoms):
        x = args_pref * positions[i,0]
        y = args_pref * positions[i,1]
        values[i] = (constant
                     + sum_pref * (math.cos(x) + math.cos(y))
                     + prod_pref * math.cos(x)*math.cos(y))


    return values


def evaluate_gradient_fcc100(positions, a, site1, site2):
    Natoms = positions.shape[0]
    Ndim = positions.shape[1]

    gradient = np.zeros((Natoms, Ndim))

    # evaluate the prefactors only once and not in the loop
    args_pref = 2 * math.pi / a
    sum_pref = site1 / 4.
    prod_pref = (site1 - 2*site2) / 4.

    sum_args_pref = sum_pref * args_pref
    prod_args_pref = prod_pref * args_pref

    for i in range(Natoms):
        x = args_pref * positions[i,0]
        y = args_pref * positions[i,1]

        gradient[i,0] = -(sum_args_pref * math.sin(x)
                        + prod_args_pref * math.sin(x)*math.cos(y))

        gradient[i,1] = -(sum_args_pref * math.sin(y)
                        + prod_args_pref * math.sin(y)*math.cos(x))

    return gradient


def evaluate_expansion_fcc111(positions, a, height):
    # DJW potential
    # so this is the lenght in reciprocal space
    gamma = 4. * math.pi / (math.sqrt(3) * a)

    # and these are our
    g = np.empty((6,2))

    # vector 1
    g[0,0] = g[3,0] = 1
    g[0,1] = g[3,1] = 0

    # vector 2
    g[1,0] = g[4,0] = math.cos(math.pi / 3.)
    g[1,1] = g[4,1] = math.sin(math.pi / 3.)

    # vector 3
    g[2,0] = g[5,0] = -math.cos(math.pi / 3.)
    g[2,1] = g[5,1] = math.sin(math.pi / 3.)

    g = g * gamma
    # the amplitudes
    Vg = np.ones(6)
    Vg[3::] = Vg[3::] * height

    Natoms = positions.shape[0]
    values = np.zeros(Natoms)

    # loop over Natoms
    for i in range(Natoms):
        # loop over reciprocal vectors
        # well, we could actually save something by using the dot product...
        # but, anyway, this is plain python...
        for j in range(6):
            values[i] = values[i] + Vg[j] * math.cos(g[j,0] * positions[i,0] + g[j,1] * positions[i,1])

    return values


def evaluate_gradient_fcc111(positions, a, height):
    # so this is the lenght in reciprocal space
    gamma = 4. * math.pi / (math.sqrt(3) * a)

    # and these are our
    g = np.empty((6,2))

    # vector 1
    g[0,0] = g[3,0] = 1
    g[0,1] = g[3,1] = 0

    # vector 2
    g[1,0] = g[4,0] = math.cos(math.pi / 3.)
    g[1,1] = g[4,1] = math.sin(math.pi / 3.)

    # vector 3
    g[2,0] = g[5,0] = -math.cos(math.pi / 3.)
    g[2,1] = g[5,1] = math.sin(math.pi / 3.)

    # the amplitudes
    Vg = np.ones(6)
    Vg[3::] = Vg[3::] * height

    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    gradient = np.zeros((Natoms, Ndim))

    for i in range(Natoms):
        for j in range(6):
            # no broadcasting here in order to use numpy
            tmp = -Vg[j] * math.sin(g[j,0] * positions[i,0] + g[j,1] * positions[i,1])
            gradients[i,0] = gradients[i,0] + g[j,0] * tmp
            gradients[i,1] = gradients[i,1] + g[j,1] * tmp

    return gradients
