# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from math import sqrt

"""
Plain python (in the sense of not using any numpy routines that rely on
broadcasting) reference implementations.
"""


def calc_energy_bruteforce(positions,
                           dipoles,
                           unit_cell_diag,
                           cutoff):

    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    cutoff_square = cutoff**2
    energy = 0.

    energy_correction = 2.0 * dipoles[0] * dipoles[0] / cutoff**3

    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                energy += 2.0 * dipoles[i] * dipoles[j] / dist**3 - energy_correction
    return energy


def calc_forces_bruteforce(positions,
                           dipoles,
                           unit_cell_diag,
                           cutoff):

    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    distance = np.zeros(Ndim)

    forces = np.zeros((Natoms, Ndim))

    cutoff_square = cutoff**2
    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp
                distance[k] = tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                tmp = 6.0 * dipoles[i] * dipoles[j] / dist**5
                for k in range(Ndim):
                    # Newton's actio=reactio
                    forces[i,k] += tmp * distance[k]
                    forces[j,k] -= tmp * distance[k]

    return forces


def calc_energy_neighbor(positions,
                         dipoles,
                         unit_cell_diag,
                         neighbor_lists,
                         num_neighbors,
                         cutoff):

    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    distance = np.zeros(Ndim)

    energy_correction = 2.0 * dipoles[0] * dipoles[0] / cutoff**3

    cutoff_square = cutoff**2
    energy = 0.

    for i in range(Natoms-1):
        for n in range(num_neighbors[i]):
            j = neighbor_lists[i,n]
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                energy += 2.0 * dipoles[i] * dipoles[j] / dist**3 - energy_correction
    return energy


def calc_forces_neighbor(positions,
                         dipoles,
                         unit_cell_diag,
                         neighbor_lists,
                         num_neighbors,
                         cutoff):

    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    distance = np.zeros(Ndim)

    forces = np.zeros((Natoms, Ndim))
    cutoff_square = cutoff**2

    for i in range(Natoms-1):
        for n in range(num_neighbors[i]):
            j = neighbor_lists[i,n]
            # avoid any numpy broadcasting in here
            tmp = 0.
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp
                distance[k] = tmp

            if dist < cutoff_square:
                dist = sqrt(dist)
                tmp = 6.0 * dipoles[i] * dipoles[j] / dist**5
                for k in range(Ndim):
                    # Newton's actio=reactio
                    forces[i,k] += tmp * distance[k]
                    forces[j,k] -= tmp * distance[k]
    return forces
