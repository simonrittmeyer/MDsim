# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import math

def evaluate_fourier_expansion(positions, G, H, real_coeff, imag_coeff, constant):
    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    Nlattice = G.shape[0]

    Nreal = real_coeff.shape[0]
    Nimag = imag_coeff.shape[0]

    values = np.zeros(Natoms)

    for i in range(Natoms):
        values[i] = constant
        for j in range(Nlattice):
            G_dot_pos = 0.0
            H_dot_pos = 0.0
            for k in range(Ndim):
                G_dot_pos += positions[i,k] * G[j,k]
                H_dot_pos += positions[i,k] * H[j,k]
            for n in range(Nreal):
                # note that this loop starts from 0 but we need 1 in the cos/sin
                values[i] += real_coeff[n] * math.cos((n+1) * G_dot_pos)
            for m in range(Nimag):
                values[i] += imag_coeff[m] * math.sin((m+1) * H_dot_pos)
    return values

def evaluate_fourier_gradient(positions, G, H, real_coeff, imag_coeff):
    # do not need a constant here
    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    Nlattice = G.shape[0]

    Nreal = real_coeff.shape[0]
    Nimag = imag_coeff.shape[0]

    gradients = np.zeros((Natoms, Ndim))

    for i in range(Natoms):
        for j in range(Nlattice):
            G_dot_pos = 0.0
            H_dot_pos = 0.0
            for k in range(Ndim):
                G_dot_pos += positions[i,k] * G[j,k]
                H_dot_pos += positions[i,k] * H[j,k]
            for n in range(Nreal):
                # no numpy broadcasting here as this slows down numba
                for k in range(Ndim):
                    # watch out, there is a minus sign due to the cos derivative
                    gradients[i,k] -= (n+1) * real_coeff[n] * math.sin((n+1) * G_dot_pos) * G[j,k]
            for m in range(Nimag):
                for k in range(Ndim):
                    gradients[i,k] += (m+1) * imag_coeff[m] * math.cos((m+1) * H_dot_pos) * H[j,k]
    return gradients
