# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Evaluate distances using the minimum image convention
"""
import numpy as np
from math import sqrt

def calc_distances_mic(positions,
                       unit_cell_diag):

    Natoms = positions.shape[0]
    Ndim = positions.shape[1]
    energy = 0.

    distances = np.zeros((Natoms, Natoms))

    for i in range(Natoms-1):
        for j in range(i+1, Natoms):
            # avoid any numpy broadcasting in here
            dist = 0.
            for k in range(Ndim):
                tmp = positions[i,k] - positions[j,k]
                # here come the mic
                tmp -= unit_cell_diag[k] * round(tmp / unit_cell_diag[k])
                dist += tmp*tmp
            dist = sqrt(dist)
            distances[i, j] = dist
            distances[j, i] = dist

    return distances

