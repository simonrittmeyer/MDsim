# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
System to allow for periodic boundary conditions
"""

from __future__ import print_function

import numpy as np

from MDsim import units
from MDsim import tools

from MDsim.systems.system import System

class PeriodicSystem(System):
    """
    This is a basic system that allows to incorporate perdiodic boundary
    conditions (PBC). Up to now, only orthorhombic cells are supported. If your
    cell is not orthorhombic, you may consider a coordinate transformation in
    advance.

    New arguments as compared to the generic system are "unit_cell" and
    "_always_wrap_positions".

    Units
    -----
    If convert=<True>, then input is expected in Angstrom, femtoseconds, atomic
    mass units and electronvolt (or combinations thereof).  Internally,
    everything is handled in atomic units as defined here

        https://en.wikipedia.org/wiki/Atomic_units.

    If you want to directly pass atomic units (a.u.), use convert=False. Also
    the units of the get_X() methods will depend on this flag: It will be the
    same unit system as the input, ie. atomic units if convert=<False> and the
    "typical" units eV, A, ... if convert=<True>


    Notes
    -----
    In general, "_calc_X()" and "calc_X()" methods require a configuration as
    input, whereas "get_X()" and "_get_X()" methods always will use the current
    system configuration. The outside world should only use "get_X()", as this
    will also take care of proper unit conversion.

    I am very well aware of the principle of @properties in python. Yet this is
    grown code and I want to have the possibility of unit conversions. Hence we
    do not use this concept.


    Initialization
    --------------
    unit_cell : (Ndim, Ndim) array
        The *orthorhombic* unit cell. If the cell is not orthorhombic, an error
        will be raised.

        Unit if convert=<True> are Angstrom, else a.u.

    positions : (Natoms, Ndim) array
        The positions of atoms (or particles in a more general sense).

        Unit if convert=<True> are Angstrom, else a.u.

    velocities : (Natoms, Ndim) array, optional (default=None)
        The velocities associated with the atoms. If <None>, these will be
        drawn from a Maxwell-Boltzman distribution (see the temperature
        argument).

        Units if convert=<True> are Angstrom per femto second, else a.u.

    masses : (Natoms,) array, optional (default=None)
        The masses of the atoms/particles. If <None>, then masses will be
        deduced from "species" by interpreting them as atoms and using atomic
        masses.

        Units if convert=<True> are atomic mass units (amu), else a.u.

    species : (Natoms,) array, optional (default=None)
        The string identifyers of the atoms. Will be 'n.a.' for all elements if
        <None> is passed. Note that if "masses"=<None>, then this array must
        contain atomic symbols from which we can deduce masses.

    potential : MDsim Potential instance, optional (default=None)
        The interaction model in the sense of a potential energy surface. This
        instance has to provide a get_Epot() and get_forces() method, both if
        which are called with the system's positions (in a.u.). For more
        details see the MDsim.potentials submodule.

    friction : MDsim FrictionMatrix instance, optional (default=None)
        The friction matrix. If a float or integer is passed, it will be
        interpreted as constant (diagonal) friction. In this case, units are
        atomic mass units per femto second if convert=<True>, and a.u. else.

        Note that the term friction is a bit ambigious in the sense of
        Langevin-MD. We call friction "eta" in this context:

            m * d^2/dt^2 pos = -d/dpos V -eta * d/dt pos + R(t)

        Note that quite often one calls gamma = eta / m a friction coefficient.
        Thus be careful. For more details see the "MDsim.friction" submodule.

    temperature : float, optional (default=0.)
        The temperature of the system.

        Units are Kelvin if convert=<True>, a.u. else. Note that the
        temperature in a.u. is equivalent to thermal energy (=kT) in a.u.!

    fix_atoms : list of integers, optional (default=None)
        List of atoms that shall be fixed during the simulations. What this
        means effectively is that all the force components on these atoms will
        be zeros. Note that you can either specify "fix_atoms" or
        "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    fix_coordinates : (Natoms, Ndim) array (dtype=bool), optional (default=None)
        Array specifying fixed coordinates. Here, <True> fixes a coordinate,
        whereas <False> retains it free. Note that you can either specify
        "fix_atoms" or "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    convert : bool, optional (default=True)
        Convert input units to a.u. or not. Also determines if output units
        will be converted back.

    _always_wrap_positions : bool, optional (default=False)
        Wrap back positions to the unit cell with every call to
        "set_positions()". Note that this may destroy correlation.

    _init_forces : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the potential will be made in the
        constructor of this object. This may be useful if the potential is only
        created afterwards.

    _init_etas : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the friction matrix will be made in
        the constructor of this object. This may be useful if the friction
        matrix is only created afterwards.

    _random_seed : int, optional (default=True)
        The seed for the RNG used to draw from a Maxwell-Boltzman distribution.
        Set this one in order to obtain reproducible results. If <None>, the
        seed will be randomized.

    _coverage : float, optional (default=None)
        The coverage with this setup. Will only be used on the information
        string.

    _cell_repeats : (Ndim,) array (default=None)
        The number of repetitions of the primitive cell to yield the super cell
        applied. Is only used for info string.

    _verbose : bool, optional (default=True)
        Print some (more) status information to stdout.
    """
    def __init__(self,
                 unit_cell,
                 positions,
                 _always_wrap_positions=False,
                 **kwargs):

        # for io purposes...
        self._cell_repeats = kwargs.pop('_cell_repeats', None)
        self._coverage = kwargs.pop('_coverage', None)
        if not self._cell_repeats is None:
            self._cell_repeats = np.asarray(self._cell_repeats)

        System.__init__(self, positions, **kwargs)

        self._name = 'Periodic MD system'

        # -----------------------------
        # The periodic part starts here
        # -----------------------------

        # now check the unit cell
        self.unit_cell = np.array(unit_cell, dtype=float, order='C')

        tools.periodic.check_orthorhombic_cell(self.unit_cell)

        if self._convert:
            self.unit_cell *= units.ANGSTROM_TO_AU

        # the diagonal of the unit cell (needed to wrap back to unit cell)
        self._unit_cell_diag = np.diagonal(self.unit_cell)

        # start from thing within a unit cell...
        self.wrap_positions()

        # switch whether we want to wrap positions
        self._always_wrap_positions = _always_wrap_positions

        # create the additional info (but only once)
        self._additional_info = self._create_periodic_info() + '\n' + self._additional_info

    def wrap_positions(self):
        """
        Fold the current positions array back into the *orthorhombic* unit cell
        """
        return tools.periodic.wrap_positions_orthorhombic(positions=self.positions,
                                                          cell=self.unit_cell)

    def set_positions(self, positions, _convert=False):
        """
        Set the system variable positions. Setting positions will require
        updates of Epot, etas and forces. In contrast to the parent routine,
        this one allows to wrap the positions back into the unit cell via the
        member switch "_always_wrap_positions"

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions. Units depend on the _convert flag. If _convert
            = <True> then Angstrom, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        # convert already here, as this is crucial for the positions wrap
        if _convert:
            positions *= units.ANGSTROM_TO_AU

        if self._always_wrap_positions:
            # crucial, wrap the positions...
            positions = tools.periodic.wrap_positions_orthorhombic(positions=positions,
                                                                   cell=self.unit_cell)
        # unit conversion is already done
        System.set_positions(self, positions, _convert=False)


    def get_cell(self, _au=False):
        """
        Return the unit cell.

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to Angstrom, regardless of
            <self._convert>

        Returns
        -------
        unit_cell:
            The unit cell of the system. Call it super cell if you want to
            distinguish from the substrate unit cell.

            If _convert=<True> and _au=<False> : Angstrom, else a.u.
        """
        if _au or not self._convert:
            return self.unit_cell
        else:
            return self.unit_cell * units.AU_TO_ANGSTROM


    def _create_periodic_info(self):
        """
        Return some additional information based on the PBC.
        """
        info = '\n{:15s}'.format('Unit cell vectors')
        info += '\n'
        for i in self._dim_range:
            info += '{:>15s}'.format(['x', 'y', 'z'][i])
        info += '\n' + '-' * (self.Ndim * 15)
        for i in self._dim_range:
            info += '\n'
            for j in self._dim_range:
                info += '{:>15s}'.format('{:>12.6f} A'.format(self.unit_cell[i, j]
                                                              * units.AU_TO_ANGSTROM))
        if self._cell_repeats is not None:
            info += '\n\n'
            info += '--> This supercell is a ({}) array of primitive unit cells'.format('x'.join([str(i) for i in self._cell_repeats]))

        if self._coverage is not None:
            info += '\n\n'
            info += '\n--> Setup corresponds to a coverage of : {} monolayer'.format(self._coverage)
        return info

