# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
The most fundamental basis class of a system
"""
from __future__ import print_function

import numpy as np
import warnings

from MDsim import units
#from MDsim.tools import extend_to_Ndim
from MDsim.tools import species_to_masses
from MDsim.tools import randomize_seed
from MDsim.tools.maxwellboltzmann import draw_velocities


from MDsim.friction.constantfriction import ConstantFriction


class System(object):
    """
    This is the most fundamental class of all MD systems. It is mainly
    constructed to collect different things such as positions and velocities,
    but also to directly interact with potentials and friction matrices.
    Basically, the system is everythong that the outside (IO interface,
    integrator,...) has to interact with.


    Units
    -----
    If convert=<True>, then input is expected in Angstrom, femtoseconds, atomic
    mass units and electronvolt (or combinations thereof).  Internally,
    everything is handled in atomic units as defined here

        https://en.wikipedia.org/wiki/Atomic_units.

    If you want to directly pass atomic units (a.u.), use convert=False. Also
    the units of the get_X() methods will depend on this flag: It will be the
    same unit system as the input, ie. atomic units if convert=<False> and the
    "typical" units eV, A, ... if convert=<True>


    Notes
    -----
    In general, "_calc_X()" and "calc_X()" methods require a configuration as
    input, whereas "get_X()" and "_get_X()" methods always will use the current
    system configuration. The outside world should only use "get_X()", as this
    will also take care of proper unit conversion.

    I am very well aware of the principle of @properties in python. Yet this is
    grown code and I want to have the possibility of unit conversions. Hence we
    do not use this concept.


    Initialization
    --------------
    positions : (Natoms, Ndim) array
        The positions of atoms (or particles in a more general sense).

        Unit if convert=<True> are Angstrom, else a.u.

    velocities : (Natoms, Ndim) array, optional (default=None)
        The velocities associated with the atoms. If <None>, these will be
        drawn from a Maxwell-Boltzman distribution (see the temperature
        argument).

        Units if convert=<True> are Angstrom per femto second, else a.u.

    masses : (Natoms,) array, optional (default=None)
        The masses of the atoms/particles. If <None>, then masses will be
        deduced from "species" by interpreting them as atoms and using atomic
        masses.

        Units if convert=<True> are atomic mass units (amu), else a.u.

    species : (Natoms,) array, optional (default=None)
        The string identifyers of the atoms. Will be 'n.a.' for all elements if
        <None> is passed. Note that if "masses"=<None>, then this array must
        contain atomic symbols from which we can deduce masses.

    potential : MDsim Potential instance, optional (default=None)
        The interaction model in the sense of a potential energy surface. This
        instance has to provide a get_Epot() and get_forces() method, both if
        which are called with the system's positions (in a.u.). For more
        details see the MDsim.potentials submodule.

    friction : MDsim FrictionMatrix instance, optional (default=None)
        The friction matrix. If a float or integer is passed, it will be
        interpreted as constant (diagonal) friction. In this case, units are
        atomic mass units per femto second if convert=<True>, and a.u. else.

        Note that the term friction is a bit ambigious in the sense of
        Langevin-MD. We call friction "eta" in this context:

            m * d^2/dt^2 pos = -d/dpos V -eta * d/dt pos + R(t)

        Note that quite often one calls gamma = eta / m a friction coefficient.
        Thus be careful. For more details see the "MDsim.friction" submodule.

    temperature : float, optional (default=0.)
        The temperature of the system.

        Units are Kelvin if convert=<True>, a.u. else. Note that the
        temperature in a.u. is equivalent to thermal energy (=kT) in a.u.!

    fix_atoms : list of integers, optional (default=None)
        List of atoms that shall be fixed during the simulations. What this
        means effectively is that all the force components on these atoms will
        be zeros. Note that you can either specify "fix_atoms" or
        "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    fix_coordinates : (Natoms, Ndim) array (dtype=bool), optional (default=None)
        Array specifying fixed coordinates. Here, <True> fixes a coordinate,
        whereas <False> retains it free. Note that you can either specify
        "fix_atoms" or "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    convert : bool, optional (default=True)
        Convert input units to a.u. or not. Also determines if output units
        will be converted back.

    _init_forces : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the potential will be made in the
        constructor of this object. This may be useful if the potential is only
        created afterwards.

    _init_etas : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the friction matrix will be made in
        the constructor of this object. This may be useful if the friction
        matrix is only created afterwards.

    _random_seed : int, optional (default=True)
        The seed for the RNG used to draw from a Maxwell-Boltzman distribution.
        Set this one in order to obtain reproducible results. If <None>, the
        seed will be randomized.

    _verbose : bool, optional (default=True)
        Print some (more) status information to stdout.
    """

    def __init__(self,
                 positions,
                 velocities=None,
                 masses=None,
                 species=None,
                 potential=None,
                 friction=None,
                 temperature=0.,
                 fix_atoms=None,
                 fix_coordinates=None,
                 convert=True,
                 _init_forces=True,
                 _init_etas=True,
                 _random_seed=None,
                 _verbose=True
                ):

        self._verbose = _verbose

        # all floats in double precision and arrays C contigious
        self.positions = np.array(positions, dtype=np.float64, order='C')
        self.forces = np.zeros_like(self.positions, dtype=np.float64, order='C')

        # only consider diagonal friction matrices
        # note that this results in a (Natoms x Ndim) array though, because our
        # velocties array is likewise 2D!
        self.etas = np.zeros_like(self.positions, dtype=np.float64, order='C')

        # sanity checks for the dimensions of positions and velocities
        self.Natoms, self.Ndim = self.positions.shape

        # a zeros dummy if we need dummy forces, etas
        self._zeros = np.zeros((self.Natoms, self.Ndim), dtype=np.float64, order='C')

        if not self.Ndim in [1, 2, 3]:
            raise ValueError('Dimensionality "{}" not supported'.format(self.Ndim))


        # total number of degrees of freedom
        self.Ndof = self.Natoms * self.Ndim

        # do not want to call range function over and over again
        self._atoms_range = np.arange(0, self.Natoms)
        self._dim_range = np.arange(0, self.Ndim)
        self._dof_range = np.arange(0, self.Ndof)

        # either species or masses have to be passed
        if masses is None and species is None:
            raise ValueError('At least one of masses or species has to be passed')

        # if only species are given, interprete as atoms and lookup masses from ase
        elif masses is None and species is not None:
            if _verbose:
                print('Info : No masses given.')
                print('       Inteprete species as atoms and lookup masses from ase')

            if isinstance(species, str):
                species = [species] * self.Natoms
            self.species = np.array(species, order='C')

            # sanity check for number of atoms
            if not len(species) == self.Natoms:
                raise ValueError('Species have wrong dimensions')

            masses = species_to_masses(self.species)
            self.masses = np.array(masses, dtype=np.float64, order='C')
        else:
            self.masses = np.array(masses, dtype=np.float64, order='C')
            if not self.masses.shape[0] == self.Natoms:
                raise ValueError('Masses have wrong dimensions')

            if species is None:
                self.species = np.array(['n.a.'] * self.Natoms, order='C')
            else:
                self.species = np.array(species, order='C')
                # sanity check for number of atoms
                if not self.species.shape[0] == self.Natoms:
                    raise ValueError('Species have wrong dimensions')

        # the temperature
        if temperature is None:
            temperature = 0.

        if temperature >= 0.:
            self.kT = temperature
        else:
            raise ValueError('Negative temperature?!')

        self._convert = convert

        if self._convert:
            # as usual, at the end we want atomic units in here
            self.positions *= units.ANGSTROM_TO_AU
            self.masses *= units.AMU_TO_AU
            self.kT *= units.KELVIN_TO_AU

        # here we go with the velocities (a bit late but we need to wait for
        # the temperature)
        if _random_seed is None:
            _random_seed = randomize_seed()
        self._random_seed = _random_seed


        if velocities is not None:
            self._random_velocities = False
            self.velocities = np.array(velocities, dtype=np.float64, order='C')
            if self._convert:
                self.velocities *= units.ANGSTROM_PER_FS_TO_AU
        else:
            if self._verbose:
                print('Info : No velocities given.')
                print('       Draw random velocities from Maxwell-Boltzman distribution')

            self._random_velocities = True
            # all the input is already in a.u.
            velocities = draw_velocities(self.masses, self.Ndim, self.kT,
                                         convert=False,
                                         seed=self._random_seed)
            self.velocities = np.array(velocities, dtype=np.float64, order='C')


        # last sanity check
        if not np.all(positions.shape == velocities.shape):
            raise ValueError('Positions and velocities have different shapes')
        elif not self.positions.shape[-1] == self.Ndim:
            raise ValueError('Positions/Velocities have wrong dimensionality')


        # the mass broadcasting can be done once and for all -- legacy!
        self.masses_bc = self.masses[:, None]
        #self.masses_bc = extend_to_Ndim(self.masses, self.Ndim)

        # we start with zero time
        self.time = np.float64(0)

        # assign the potential
        # create a dummy if none is given
        self.set_potential(potential)
        self.set_friction(friction)

        # -----------------------
        # ...new in v.3.4.4
        # FIXED ATOMS CONSTRAINTS
        # -----------------------
        self._init_constraints(fix_atoms, fix_coordinates)

        # initialization via flag
        if _init_forces:
            self._init_forces()

        if _init_etas:
            self._init_etas()

        self._name = 'Generic MD system'

        # flags to avoid some energy calculations
        # basically get_Ekin and get_Epot ask for it
        self._update_Epot = True
        self._update_Ekin = True
        self._update_forces = True
        self._update_etas = True

        # containers to store the energy
        self.Epot = 0.
        self.Ekin = 0.

        # the total dissipated energy (dummy if not for QMMe+EF)
        self.Ediss = 0.

        # monitor Bussi's effective energy, anyway it is just the increments
        # that matter, which is why we start with 0
        self.Eeff_conint = 0.

        # save the initial values to obtain time-correlations
        self._positions_init = self.positions.copy()
        self._velocities_init = self.velocities.copy()

        if self._verbose:
            print('       Starting with initial temperature of {:.2f} K'.format(self.get_instantaneous_temperature()))

        # a container to store additional information
        self._additional_info = ''


    def _init_constraints(self, fix_atoms=None, fix_coordinates=None):
        """
        Initialize the internal constraints masks. So far only support for
        fixed atoms, and fixed internal coordinates.

        .. versionadded:: 3.4.4

        Parameters
        ----------
        fix_atoms : list of integers, optional (default=None)
            List of atoms that shall be fixed during the simulations. What this
            means effectively is that all the force components on these atoms
            will be zeros.
            Note that you can either specify "fix_atoms" or "fix_coordinates",
            not both.

        fix_coordinates : (Natoms, Ndim) array (dtype=bool), optional (default=None)
            Array specifying fixed coordinates. Here, <True> fixes a
            coordinate, whereas <False> retains it free.
            Note that you can either specify "fix_atoms" or "fix_coordinates",
            not both.
        """
        if (fix_atoms is not None and fix_coordinates is not None):
            raise ValueError('You can either specify "fix_atoms" or "fix_coordinates", not both!')

        if (fix_atoms is None and fix_coordinates is None):
            _fixed_coordinates_mask = np.zeros((self.Natoms, self.Ndim), dtype=bool)

        elif fix_atoms is not None:
            fix_atoms = np.asarray(fix_atoms, dtype=int)

            # sanity check on the indicies
            # check for double indices
            if len(fix_atoms) != len(np.unique(fix_atoms)):
                raise IndexError('Multiple occurences of same index in "fix_atoms"')
            try:
                self.positions[fix_atoms]
            except IndexError:
                raise IndexError('Invalid index in "fix_atoms"')

            _fixed_atoms_idx = np.unique(fix_atoms)

            # this is for the more generic handling at some point
            _fixed_coordinates_mask = np.zeros((self.Natoms, self.Ndim), dtype=bool)
            _fixed_coordinates_mask[_fixed_atoms_idx, :] = True

        elif fix_coordinates is not None:
            _fixed_coordinates_mask = fix_coordinates


        self.set_fixed_coordinates(_fixed_coordinates_mask)


    def _init_forces(self):
        """
        Initialize forces. Basically just calls and sets forces according to
        the initial positions such that get_forces() does not require a further
        calculation.
        """
        self.set_forces(self.calc_forces(self.positions), _convert=False)


    def _init_etas(self):
        """
        Initialize the friction coefficients. Basically just calls and sets the
        friction matrix corresponding to the initial positions such that
        get_etas() does not require a further calculation.
        """
        self.set_etas(self.calc_etas(self.positions), _convert=False)
        self._update_etas = False


    def set_potential(self, potential):
        """
        Wrapper routine to set the potential as a callable member method. May
        also be used to changed the PES from the outside.

        Parameters
        ----------
        potential : Potential instance or subclass thereof.
            PES potential to be employed. For specfications see the respective
            class documentation(s).
        """

        self.potential = potential

        if self.potential is None:
            self.has_pes = False
        else:
            self.has_pes = True

        self._update_Epot = True
        self._update_forces = True

    def set_friction(self, friction):
        """
        Wrapper routine to set the friction matrix as a callable member method.

        Parameters
        ----------
        friction : FrictionMatrix instance or subclass thereof.
            Friction Matrix to be employed.
        """

        if isinstance(friction, (int, float)):
            self.friction = ConstantFriction(friction * np.ones_like(self.positions),
                                             convert=self._convert)
        else:
            self.friction = friction

        if self.friction is None:
            self.has_friction = False
        else:
            self.has_friction = True

    # ------------------------------------------------------------
    # calculating attributes, does not alter any system attribute.
    # ------------------------------------------------------------

    def _calc_pes_forces(self, positions):
        """
        Calculate the forces due to the potential (the PES) for a given set of
        positions.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions at which to evaluate the forces (in a.u.).

        Returns
        -------
        forces : (Natoms, Ndim) array
            The corresponding forces (in a.u.). Will be a zero-valued array if
            no potential was set.
        """

        if self.has_pes:
            return self.potential.get_forces(positions)
        else:
            #warnings.warn('\nNo potential assigned but requesting force calculation\nWill return zero forces.', RuntimeWarning)
            return self._zeros


    def _calc_pes_potential_energy(self, positions):
        """
        Calculate the energy due to the potential (the PES) for a given set of
        positions.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions at which to evaluate the potential (in a.u.).

        Returns
        -------
        energy : float
            The corresponding energy (in a.u.) for this configuration. Will be
            a zero if no potential was set.
        """
        if self.potential is None:
            warnings.warn('\nNo potential assigned but requesting Epot calculation.\nWill return zero potential energy.', RuntimeWarning)
            return 0.
        else:
            return self.potential.get_Epot(positions)


    def calc_potential_energy(self, positions):
        """
        Evaluate the *total* potential energy for a given configuration. This
        may be trivially the PES-potential energy. But for systems with
        pairwise interactions as well, there are further contributions. Hence,
        this is just a high-level routine which needs to be overloaded if
        contributions other than the PES are important.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        Returns
        -------
        energy : float
            Corresponding potential energy (in a.u.)
        """
        return self._calc_pes_potential_energy(positions)


    def calc_forces(self, positions):
        """
        Evaluate the *total* forces for a given configuration. This may be
        trivially the PES-forces. But for systems with pairwise interactions as
        well, there are further contributions. Hence, this is just a high-level
        routine which needs to be overloaded if contributions other than the
        PES are important.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        Returns
        -------
        forces : (Natoms, Ndim) array
            Corresponding forces (in a.u.).
        """
        # here this is just an alias, but for pairwise systems a second evaluation follows
        return self._calc_pes_forces(positions)


    def calc_etas(self, positions):
        """
        Evaluate the friction coefficient(s) for a given configuration. Note
        that only diagonal friction tensors are implemented.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        Returns
        -------
        etas : (Natoms, Ndim) array
            Corresponding (isotropic) friction coefficients (in a.u.).
        """
        if self.has_friction:
            # note that this yields the non-mass-weighted friction in its eigenbasis
            # not necessasrily the cartesian basis for tensorial friction
            return self.friction(positions)
        else:
            #warnings.warn('\nNo eta matrix assigned but requesting friction matrix calculation\nWill return zero friction', RuntimeWarning)
            return self._zeros


    def calc_kinetic_energy(self, velocities):
        """
        Evaluate the kinetic energy for a given configuration.

        Parameters
        ----------
        velocities : (Natoms, Ndim) array
            The input velocities (in a.u.)

        Returns
        -------
        energy : float
            The corresponding kinetic energy in (a.u.)
        """
        if self.Ndim == 1 and self.Natoms == 1:
            # this is where einsum fails
            return 0.5*np.sum(self.masses * velocities**2)
        else:
            # here we go for einsum... magnificent speedup
            #                      # the sum             # the scalar products
            #                                            # the scalar product is faster via v**2!
            return 0.5 * np.einsum('i,i->', self.masses, np.einsum('ij,ij->i', velocities, velocities))


    def calc_velocity_autocorrelation(self, velocities, velocities_init=None):
        """
        Calculate the velocity autocorrelation (VAC), which is just
            VAC = 1 / Natoms * sum_i velocities[i,:] dot velocities_init[i,:]

        Yet, we use np.einsum to gain a considerable speedup thorugh compiled C
        code. Evaluation the VAC as a function of time yields the discrete
        velocity autocorrelation function (VACF).

        Parameters
        ----------
        velocities : (Natoms, Ndim) array
            The velocities for which to evaluate the VAC in a.u..

        velocities_init : (Natoms, Ndim) array, optional (default=None)
            The "t=0" velocities in a.u.. If <None>, the velocities from the
            initialization will be used.

        Returns
        -------
        vac : float
            The velocity autocorrelation corresponding to the two input
            velocity arrays.
        """
        if velocities_init is None:
            velocities_init = self._velocities_init
        # average over all atoms
        # einsum is just great ;)
        vac = np.einsum('ij,ij->', velocities_init, velocities) / self.Natoms
        #vac = np.sum(np.diag(np.dot(self._velocities_init, self.velocities.T))) / self.Natoms
        return vac


    def calc_dissipation_rate(self, etas, velocities):
        """
        Calculate the dissipated energy due to friction

        Parameters
        ----------
        etas : (Natoms, Ndim) array
            The input friction coefficients (in a.u.)

        velocities : (Natoms, Ndim) array
            The input velocities (in a.u.)

        Returns
        -------
        dissipation_rate : float
            The corresponding energy dissipation rate in (a.u.)
        """
        # efficiency is not a bottleneck here, otherwise we should go for einsum
        if self.Ndim == 1 and self.Natoms == 1:
            return -np.sum(etas * velocities**2)
        else:
            # use einsum magic
            return -np.einsum('ij,ij->', etas, velocities**2)

    # ----------------------------------------------------------
    # get_X() routines for the io interface
    # These will always correspond to the current system state.
    # If you want something different, use the calc_X() routines
    # -----------------------------------------------------------

    def update_forces(self):
        """
        Sync the forces to the current positions. This is basically just a
        wrapper for get_forces(), but this is more explicit as it indicates
        that we update the internal forces array.
        """
        self.get_forces(_au=True)

    def update_etas(self):
        """
        Sync the etas to the current positions. This is basically just a
        wrapper for get_etas(), but this is more explicit as it indicates
        that we update the internal etas array.
        """
        self.get_etas(_au=True)

    # this returns the direct system arrays... never ever change anything there

    def get_forces(self, _au=False):
        """
        Return the forces corresponding to the current configuration of the
        system.

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV / Angstrom, regardless of
            <self._convert>.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces. Units depend on the member variable "_convert", and the
            argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : eV / Angstrom, else a.u.
        """

        if self._update_forces:
            self.set_forces(self.calc_forces(self.positions), _convert=False)

        if _au or not self._convert:
            return self.forces
        else:
            return self.forces * units.AU_TO_EV_PER_ANGSTROM


    def get_positions(self, _au=False):
        """
        Return the positions corresponding to the current configuration of the
        system.

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to Angstrom, regardless of
            <self._convert>.

        Returns
        -------
        positions : (Natoms, Ndim) array
            The positions. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : Angstrom, else a.u.
        """
        if _au or not self._convert:
            return self.positions
        else:
            return self.positions * units.AU_TO_ANGSTROM


    def get_velocities(self, _au=False):
        """
        Return the velocities corresponding to the current configuration of the
        system.

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to Angstrom / femto second, regardless
            of <self._convert>.

        Returns
        -------
        velocities : (Natoms, Ndim) array
            The velocities. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : Angstrom / femto second, else
            a.u.
        """
        if _au or not self._convert:
            return self.velocities
        else:
            return self.velocities * units.AU_TO_ANGSTROM_PER_FS


    def get_masses(self, _au=False):
        """
        Return the masses corresponding to the current configuration of the
        system.

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to atomic mass units, regardless
            of <self._convert>.

        Returns
        -------
        masses : (Natoms,) array
            The masses. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : atomic mass units, else a.u.
        """
        if _au or not self._convert:
            return self.masses
        else:
            return self.masses * units.AU_TO_AMU


    def get_momenta(self, _au=False):
        """
        Return the momenta corresponding to the current configuration of the
        system. Note that this is not a propageted quantity (rather the
        velocities). Hence every call requires an element-wise multiplication.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to Angstrom * atomic mass units /
            femtoseconds, regardless of <self._convert>.

        Returns
        -------
        momenta : (Natoms, Ndim) array
            The momenta. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : Angstrom * atomic mass units /
            femtoseconds, else a.u.
        """
        # do not write to an array here
        if _au or not self._convert:
            return self.velocities * self.masses[:, None]
        else:
            return self.velocities * self.masses[:, None] * units.AU_TO_ANGSTROM_AMU_PER_FS


    def get_etas(self, _au=False):
        """
        Return the friction coefficients corresponding to the current
        configuration of the system.

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to atomic mass units / femtoseconds,
            regardless of <self._convert>.

        Returns
        -------
        etas : (Natoms, Ndim) array
            The friction coefficients. Units depend on the member variable
            "_convert", and the argument "_au", where the latter precedes the
            member variable.

            If _convert=<True> and _au=<False> : atomic mass units /
            femtoseconds, else a.u.
        """
        if self._update_etas:
            self.set_etas(self.calc_etas(self.positions), _convert=False)

        if _au or not self._convert:
            return self.etas
        else:
            return self.etas * units.AU_TO_AMU_PER_FS


    def get_kinetic_energy(self, _au=False):
        """
        Return the kinetic energy corresponding to the current configuration.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>.

        Returns
        -------
        Ekin : float
            The kinetic energy. Units depend on the member variable "_convert",
            and the argument "_au", where the latter precedes the member
            variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        if self._update_Ekin:
            self.set_kinetic_energy(self.calc_kinetic_energy(self.velocities),
                                    _convert=False)
        if _au or not self._convert:
            return self.Ekin
        else:
            return self.Ekin * units.AU_TO_EV


    def get_potential_energy(self, _au=False):
        """
        Return the potential energy corresponding to the current configuration.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>.

        Returns
        -------
        Epot : float
            The potential energy. Units depend on the member variable
            "_convert", and the argument "_au", where the latter precedes the
            member variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        if self._update_Epot:
            self.set_potential_energy(self.calc_potential_energy(self.positions),
                                      _convert=False)
        if _au or not self._convert:
            return self.Epot
        else:
            return self.Epot * units.AU_TO_EV


    def get_total_energy(self, _au=False):
        """
        Return the total energy corresponding to the current configuration.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>.

        Returns
        -------
        Etot : float
            The total energy. Units depend on the member variable "_convert",
            and the argument "_au", where the latter precedes the member
            variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        # this one is already converted
        Etot = self.get_kinetic_energy(_au=_au) + self.get_potential_energy(_au=_au)
        return Etot


    def get_time(self, _au=False):
        """
        Return the system time.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to femtoseconds, regardless of
            <self._convert>.

        Returns
        -------
        time : float
            The system time. Units depend on the member variable "_convert",
            and the argument "_au", where the latter precedes the member
            variable.

            If _convert=<True> and _au=<False> : femtoseconds, else a.u.
        """
        if _au or not self._convert:
            return self.time
        else:
            return self.time * units.AU_TO_FS


    def get_instantaneous_temperature(self, _au=False):
        """
        Evaluate the instantaneous temperature as defined in Frenkel/Smit:

            T_inst = 2 * Ekin / (Natoms * Ndim)

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to Kelvin, regardless of
            <self._convert>.

        Returns
        -------
        T_inst : float
            The instantaneous temperature. Units depend on the member variable
            "_convert", and the argument "_au", where the latter precedes the
            member variable.

            If _convert=<True> and _au=<False> : Kelvin, else a.u.
        """
        # factor two since 2*Ekin = mv**2...
        T = 2*self.get_kinetic_energy(_au=True) / self.Ndof

        if _au or not self._convert:
            return T
        else:
            return  T * units.AU_TO_KELVIN


    def get_temperature(self, _au=False):
        """
        Return the temperature from the initialization, ie. the target
        temperature.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to Kelvin, regardless of
            <self._convert>.

        Returns
        -------
        T : float
            The temperature. Units depend on the member variable "_convert",
            and the argument "_au", where the latter precedes the member
            variable.

            If _convert=<True> and _au=<False> : Kelvin, else a.u.
        """
        # note, we do not directly store T but rather kT
        if _au or not self._convert:
            return self.kT
        else:
            return self.kT * units.AU_TO_KELVIN


    def get_kT(self, _au=False):
        """
        Return the thermal energy from the initialization.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>

        Returns
        -------
        kT : float
            The thermal energy. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        if _au or not self._convert:
            return self.kT
        else:
            return self.kT * units.AU_TO_EV


    def get_effective_energy_conint(self, _au=False):
        """
        Bussi's effective energy is defined as

            Htilde = H - conint

        where H is the total energy of the system and conint is a quantity
        accumulated along the trajectory as minus the sum of all the increments
        of kinetic energy due to the thermostat. This routine returns conint.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>

        Returns
        -------
        effective_energy_conint : float
            The continues contribution. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        if _au or not self._convert:
            return self.Eeff_conint
        else:
            return self.Eeff_conint * units.AU_TO_EV

    def get_effective_energy(self, _au=False):
        """
        Bussi's effective energy is defined as

            Htilde = H - conint

        where H is the total energy of the system and conint is a quantity
        accumulated along the trajectory as minus the sum of all the increments
        of kinetic energy due to the thermostat. This routine returns Htilde.

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>

        Returns
        -------
        effective_energy : float
            The effective energy. Units depend on the member variable "_convert", and
            the argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        Etot = self.get_total_energy(_au=_au)

        if _au or not self._convert:
            return self.Eeff_conint + Etot
        else:
            return self.Eeff_conint * units.AU_TO_EV + Etot


    def get_dissipation_rate(self, _au=False):
        """
        Return the dissipation rate due to friction

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV/fs, regardless of <self._convert>

        Returns
        -------
        dissipation_rate : float
            The corresponding energy dissipation rate in (a.u.)
        """
        if self.has_friction:
            Edissdot = self.calc_dissipation_rate(etas=self.etas,
                                                  velocities=self.velocities)
            if _au or not self._convert:
                return Edissdot
            else:
                return Edissdot * units.AU_TO_EV_PER_FS
        else:
            return 0

    def get_dissipated_energy(self, _au=False):
        """
        Return the accumulated dissipated energy due to friction

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>

        Returns
        -------
        dissipation_rate : float
            The corresponding accumulated dissipated energy (a.u.)
        """
        if _au or not self._convert:
            return self.Ediss
        else:
            return self.Ediss * units.AU_TO_EV

    def get_Ndim(self):
        """
        Return the number of (Cartesian) dimensions, ie. the number of degrees
        of freedom per atom.

        Returns
        -------
        Ndim : int
            The number of degrees of freedom per atom.
        """
        return self.Ndim


    def get_Natoms(self):
        """
        Return the number of atoms.

        Returns
        -------
        Natoms : int
            The number of atoms.
        """
        return self.Natoms


    def get_Ndof(self):
        """
        Return the number of total degrees of freedom, ie.

            Ndof = Natoms * Ndim

        Returns
        -------
        Ndof : int
            The total number of degrees of freedom.
        """
        return self.Ndof


    def get_dimensions(self):
        """
        Return the dimensionality of the system, ie (Natoms, Ndim).

        Returns
        -------
        Natoms : int
            The number atoms.

        Ndim : int
            The number of degrees of freedom per atom.
        """

        return self.Natoms, self.Ndim


    def get_species(self):
        """
        Return the species identifyer strings.

        Returns
        -------
        species : (Natoms,) array
            The atom/particle identifyers.
        """
        return self.species


    def get_dEdt(self):
        """
        Return the dissipated energy according to Rayleigh's dissipation
        function
        """
        raise NotImplementedError


    def get_potential(self):
        """
        Return the attached Potential instance.

        Returns
        -------
        potential : MDsim potential instance
            The potential invoked.
        """
        return self.potential


    def get_friction(self):
        """
        Return the attached FrictionMatrix instance.

        Returns
        -------
        friction : MDsim FrictionMatrix instance.
            The friction matrix involved.
        """
        return self.friction


    def get_velocity_autocorrelation(self, velocities_init=None, _au=False):
        """
        Calculate the velocity autocorrelation (VAC), which is just
            VAC = 1 / Natoms * sum_i velocities[i,:] dot velocities_init[i,:]

        Parameters
        ----------
        velocities_init : (Natoms, Ndim) array, optional (default=None)
            The "t=0" velocities in a.u.. If <None>, the velocities from the
            initialization will be used.

        _au : bool, optional (default=False)
            Do *not* convert from a.u. to (Angstrom per femtoseconds)**2, regardless
            of <self._convert>

        Returns
        -------
        vac : float
            The velocity autocorrelation. Units depend on the member variable
            "_convert", and the argument "_au", where the latter precedes the
            member variable.

            If _convert=<True> and _au=<False> : (Angstrom per
            femtoseconds)**2, else a.u.
        """
        vac = self.calc_velocity_autocorrelation(self.velocities, velocities_init)
        if _au or not self._convert:
            return vac
        else:
            return vac * units.AU_TO_ANGSTROM_PER_FS**2


    def get_fixed_coordinates(self):
        """
        Return the mask with fixed coordinates marked as <True>

        .. versionadded: 3.4.4

        Returns
        -------
        fixed_coordinates_mask : (Natoms, Ndim) array (dtype=bool)
            Mask where <True> corresponds to a fixed coordinate (forces will be
            zeroed).
        """
        return self._fixed_coordinates_mask.copy()

    def get_free_coordinates(self):
        """
        Return the mask with free coordinates marked as <True>

        ..versionadded: 3.4.4

        Returns
        -------
        free_coordinates_mask : (Natoms, Ndim) array (dtype=bool)
            Mask where <True> corresponds to a free coordinate (forces will be
            zeroed).
        """
        return self._free_coordinates_mask.copy()


    # ---------------------------------
    # setting attributes (integrator)!
    # ---------------------------------

    def set_fixed_coordinates(self, fixed_coordinates_mask):
        """
        Set the constraints on the system.

        ..versionadded: 3.4.4

        Parameters
        ----------
        fixed_coordinates_mask : (Natoms, Ndim) array (dtype=bool)
            The mask controlling fixed (<True>) and free (<False>) coordinates.

        Returns
        -------
        <None>
        """
        if fixed_coordinates_mask.shape == self.positions.shape:
            # this is for the more generic handling at some point
            self._fixed_coordinates_mask = np.asarray(fixed_coordinates_mask, dtype=bool)
            self._free_coordinates_mask = np.logical_not(self._fixed_coordinates_mask)

            # flag if we have constraints at all
            self._constrained = np.any(self._fixed_coordinates_mask)
        else:
            raise IndexError('"fixed_coordinates_mask" is of wrong shape, not (Natoms, Ndim)')

    def set_kinetic_energy(self, Ekin, _convert=False):
        """
        Set the system variable Ekin. At the same time, this kinetic energy
        will be returned as long as the system configuration does not change.

        Parameters
        ----------
        Ekin : float
            The kinetic energy. Units depend on the _convert flag. If _convert
            = <True> then eV, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            Ekin *= units.EV_TO_AU

        self.Ekin = Ekin
        self._update_Ekin = False


    def set_potential_energy(self, Epot, _convert=False):
        """
        Set the system variable Epot. At the same time, this potential energy
        will be returned as long as the system configuration does not change.

        Parameters
        ----------
        Epot : float
            The potential energy. Units depend on the _convert flag. If _convert
            = <True> then eV, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            Epot *= units.EV_TO_AU

        self.Epot = Epot
        self._update_Epot = False


    def set_positions(self, positions, _convert=False):
        """
        Set the system variable positions. Setting positions will require
        updates of Epot, etas and forces.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions. Units depend on the _convert flag. If _convert
            = <True> then Angstrom, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            positions *= units.ANGSTROM_TO_AU

        self.positions = positions
        self._update_Epot = True
        self._update_forces = True
        self._update_etas = True


    def set_velocities(self, velocities, _convert=False):
        """
        Set the system variable velocities. This will require an update of
        Ekin.

        Parameters
        ----------
        velocities : (Natoms, Ndim) array
            The velocities. Units depend on the _convert flag. If _convert
            = <True> then Angstrom per femtoseconds, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            velocities *= units.ANGSTROM_PER_FS_TO_AU

        self.velocities = velocities
        self._update_Ekin = True


    def set_forces(self, forces, _convert=False):
        """
        Set the system variable forces. These forces will be reused as long as
        the system configuration (positions!) does not change.

        Parameters
        ----------
        velocities : (Natoms, Ndim) array
            The velocities. Units depend on the _convert flag. If _convert
            = <True> then eV  per Angstrom, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            forces *= units.EV_PER_ANGSTROM_TO_AU

        self.forces = forces

        if self._constrained:
            self.forces[self._fixed_coordinates_mask] = 0.

        self._update_forces = False


    def set_time(self, time, _convert=False):
        """
        Set the system variable time.

        Parameters
        ----------
        time : float
            The system time. Units depend on the _convert flag. If _convert
            = <True> then femtoseconds, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            time *= units.FS_TO_AU
        self.time = time


    def set_etas(self, etas, _convert=False):
        """
        Set the system variable etas. These friction coefficients will be
        reused as long as the system configuration (positions!) does not
        change.

        Parameters
        ----------
        etas : (Natoms, Ndim) array
            The friction coefficients. Units depend on the _convert flag. If
            _convert = <True> then atomic mass units per femtoseconds, else
            (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            etas *= units.AMU_PER_FS_TO_AU
        self.etas = etas
        self._update_etas = False


    def set_kT(self, kT, _convert=False):
        """
        Set the system variable kT. This may be a hook when invoking a
        two-temperature model.

        Parameters
        ----------
        kT : float
            The thermal energy. Units depend on the _convert flag. If
            _convert = <True> then eV, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            kT *= units.EV_TO_AU
        self.kT = kT


    def set_effective_energy_conint(self, Eeff_conint, _convert=False):
        """
        Set Bussi's effective energy.

        Parameters
        ----------
        Eeff_conint : float
            The effective energy. Units depend on the _convert flag. If
            _convert = <True> then eV, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            Eeff_conint *= units.EV_TO_AU

        self.Eeff_conint = Eeff_conint


    # some eyecandy
    def _get_atoms_info(self):
        """
        Return atom specific information
        """
        if np.all(self.species == self.species[0]) and np.all(self.masses == self.masses[0]) and not self._constrained:
            info = 'Homogeneous system'
            info += '\n------------------'
            info += '\nspecies : {}'.format(self.species[0])
            info += '\nmass    : {} amu'.format(self.masses[0]*units.AU_TO_AMU)
        else:
            info = '{0:<10s}{1:>20s}{2:>20s}{3:>30s}'.format('Atom ID', 'species', 'mass (amu)', 'constraints')
            info += '\n' + '-'*80
            for i, (s, m, c) in enumerate(zip(self.species, self.masses, self._fixed_coordinates_mask)):
                if np.all(c == False):
                    constrain_str = 'free'
                elif np.all(c == True):
                    constrain_str = 'fixed'
                else:
                    constrain_str = '('
                    for ic in c:
                        if ic:
                            constrain_str += 'fixed, '
                        else:
                            constrain_str += 'free, '
                    constrain_str = constrain_str[:-2] + ')'
                info += '\n{0:<10d}{1:>20s}{2:>20.8f}{3:>30s}'.format(i, s, m * units.AU_TO_AMU, constrain_str)
        return info

    def _create_general_info(self):
        """
        Return some general system information, excluding atom specific
        parameters.
        """
        info = '\n'
        info += '\nThermal energy : {0:.6f} eV = {1:.3f} K'.format(self.kT * units.AU_TO_EV,
                                                                   self.kT * units.AU_TO_KELVIN)
        info += '\n'
        info += '\nNumber of atoms : {}'.format(self.Natoms)
        info += '\nNumber of degrees of freedom per atom: {}'.format(self.Ndim)
        info += '\nSystem is constrained : {}'.format(self._constrained)

        info += '\n\n'
        info += self._get_atoms_info()
        if self._random_velocities:
            info += '\n'
            info += '\n--> Velocities are randomized from Maxwell-Boltzmann'
            info += '\n    distribution with random seed : {}'.format(self._random_seed)
        return info

    def __str__(self):
        info = self._name
        info += self._create_general_info()
        if self._additional_info:
            info += '\n' + self._additional_info
        return info

    # both required by vibrations module
    def get_additional_info(self):
        return self._additional_info

    def add_additional_info(self, info):
        self._additional_info += '\n' + info

    def pre_integration_hook(self):
        return None

    def post_integration_hook(self):
        return None


