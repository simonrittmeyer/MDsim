# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Class to work with periodic systems with pairwise additive interaction
potentials like e.g. Lennard-Jones Potentials.
"""
from __future__ import print_function

import numpy as np
import math

from MDsim import units

from MDsim.systems.periodicsystem import PeriodicSystem
from MDsim.neighbors import Neighbors


class PairwiseSystem(PeriodicSystem):
    """
    This system is intended to work with pairwise interaction potentials in a
    periodic system. It interfaces to the Neighbors instance to speed up the
    evaluation of the pairwise forces using neighbor lists. The minimum image
    convention (MIC) is applied throughout, ie. there is no interaction with
    multiple images.

    Note that this class is *not* functional, but pair potential-specific
    subclasses need to be derived.

    New arguments as compared to the generic periodic system are
    "force_cutoff", "list_cutoff" and "pair_potential".


    Units
    -----
    If convert=<True>, then input is expected in Angstrom, femtoseconds, atomic
    mass units and electronvolt (or combinations thereof).  Internally,
    everything is handled in atomic units as defined here

        https://en.wikipedia.org/wiki/Atomic_units.

    If you want to directly pass atomic units (a.u.), use convert=False. Also
    the units of the get_X() methods will depend on this flag: It will be the
    same unit system as the input, ie. atomic units if convert=<False> and the
    "typical" units eV, A, ... if convert=<True>


    Notes
    -----
    In general, "_calc_X()" and "calc_X()" methods require a configuration as
    input, whereas "get_X()" and "_get_X()" methods always will use the current
    system configuration. The outside world should only use "get_X()", as this
    will also take care of proper unit conversion.

    I am very well aware of the principle of @properties in python. Yet this is
    grown code and I want to have the possibility of unit conversions. Hence we
    do not use this concept.


    Initialization
    --------------
    unit_cell : (Ndim, Ndim) array
        The *orthorhombic* unit cell. If the cell is not orthorhombic, an error
        will be raised.

        Unit if convert=<True> are Angstrom, else a.u.

    positions : (Natoms, Ndim) array
        The positions of atoms (or particles in a more general sense).

        Unit if convert=<True> are Angstrom, else a.u.

    force_cutoff : float
        The cutoff distance for the forces. Must be less than half of the
        shortest unit cell axis in order to make sure the applied MIC is
        justified.

    list_cutoff : float, optional (default=None)
        The cutoff distance for the neighbor lists. Has to be >= force_cutoff.
        If a list_cutoff is provided upon initialization, neighbor lists will
        be made use of to evaluate the pairwise interactions. For details on
        this matter see the MDsim.neighbors submodule.

    velocities : (Natoms, Ndim) array, optional (default=None)
        The velocities associated with the atoms. If <None>, these will be
        drawn from a Maxwell-Boltzman distribution (see the temperature
        argument).

        Units if convert=<True> are Angstrom per femto second, else a.u.

    masses : (Natoms,) array, optional (default=None)
        The masses of the atoms/particles. If <None>, then masses will be
        deduced from "species" by interpreting them as atoms and using atomic
        masses.

        Units if convert=<True> are atomic mass units (amu), else a.u.

    species : (Natoms,) array, optional (default=None)
        The string identifyers of the atoms. Will be 'n.a.' for all elements if
        <None> is passed. Note that if "masses"=<None>, then this array must
        contain atomic symbols from which we can deduce masses.

    potential : MDsim Potential instance, optional (default=None)
        The interaction model in the sense of a potential energy surface. This
        instance has to provide a get_Epot() and get_forces() method, both if
        which are called with the system's positions (in a.u.). For more
        details see the MDsim.potentials submodule.

    pair_potential : MDsim PairwisePotential instance, optional (default=None)
        The pairwise interaction model. Resulting forces and energies will be
        added to the PES-related properties. This instance has to provide a
        get_Epot() and get_forces() method, both if which are called with the
        system's positions (in a.u.). For more details see the MDsim.potentials
        submodule.

    friction : MDsim FrictionMatrix instance, optional (default=None)
        The friction matrix. If a float or integer is passed, it will be
        interpreted as constant (diagonal) friction. In this case, units are
        atomic mass units per femto second if convert=<True>, and a.u. else.

        Note that the term friction is a bit ambigious in the sense of
        Langevin-MD. We call friction "eta" in this context:

            m * d^2/dt^2 pos = -d/dpos V -eta * d/dt pos + R(t)

        Note that quite often one calls gamma = eta / m a friction coefficient.
        Thus be careful. For more details see the "MDsim.friction" submodule.

    temperature : float, optional (default=0.)
        The temperature of the system.

        Units are Kelvin if convert=<True>, a.u. else. Note that the
        temperature in a.u. is equivalent to thermal energy (=kT) in a.u.!

    fix_atoms : list of integers, optional (default=None)
        List of atoms that shall be fixed during the simulations. What this
        means effectively is that all the force components on these atoms will
        be zeros. Note that you can either specify "fix_atoms" or
        "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    fix_coordinates : (Natoms, Ndim) array (dtype=bool), optional (default=None)
        Array specifying fixed coordinates. Here, <True> fixes a coordinate,
        whereas <False> retains it free. Note that you can either specify
        "fix_atoms" or "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    convert : bool, optional (default=True)
        Convert input units to a.u. or not. Also determines if output units
        will be converted back.

    _always_wrap_positions : bool, optional (default=False)
        Wrap back positions to the unit cell with every call to
        "set_positions()". Note that this may destroy correlation.

    _init_forces : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the potential will be made in the
        constructor of this object. This may be useful if the potential is only
        created afterwards.

    _init_etas : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the friction matrix will be made in
        the constructor of this object. This may be useful if the friction
        matrix is only created afterwards.

    _random_seed : int, optional (default=True)
        The seed for the RNG used to draw from a Maxwell-Boltzman distribution.
        Set this one in order to obtain reproducible results. If <None>, the
        seed will be randomized.

    _cell_repeats : (Ndim,) array (default=None)
        The number of repetitions of the primitive cell to yield the super cell
        applied. Is only used for info string.

    _verbose : bool, optional (default=True)
        Print some (more) status information to stdout.
    """
    def __init__(self,
                 unit_cell,
                 positions,
                 force_cutoff,
                 list_cutoff=None,
                 pair_potential=None,
                 **kwargs
                ):


        # ok, we initialize only after the parent
        _init_forces = kwargs.pop('_init_forces', True)
        kwargs['_init_forces'] = False

        #initialize the parent
        PeriodicSystem.__init__(self,
                                unit_cell,
                                positions,
                                **kwargs)

        self.force_cutoff = np.float64(force_cutoff)

        if self._convert:
            self.force_cutoff *= units.ANGSTROM_TO_AU

        # check if we can apply minimum image convention
        if not self.force_cutoff < 0.5 * min(self._unit_cell_diag):
            error = 'force_cutoff >= 0.5 * shortest unit cell length\n'
            error += 'This prohibits applying the minimum image convention'
            raise ValueError(error)

        # Neighbor list handling
        self._neighbors_inst = Neighbors(unit_cell=self.unit_cell)
        self._num_neighbors = np.zeros(self.Natoms, dtype=np.int32, order='C')
        self._neighbor_lists = np.zeros((self.Natoms, self.Natoms), dtype=np.int32, order='C')

        # some remaining attributes
        self._use_neighbor_lists = False
        self._neighbor_lists_updates = 0
        self._positions_last_update = self.positions.copy()

        # this one also activates neighbor lists
        if list_cutoff is not None:
            self.set_list_cutoff(list_cutoff, _convert=self._convert)

        # set the pair potential
        self.set_pair_potential(pair_potential)

        if _init_forces:
            self._init_forces()

        self._name = 'Generic PeriodicPairwise MD system'

        # extend the additional information (but only once)
        self._additional_info = self._create_pairwise_info() + '\n' + self._additional_info

    def set_list_cutoff(self, list_cutoff, _convert=False):
        """
        Set the list_cutoff. Note that this will also switch on the usage of
        neighbor lists. If list_cutoff < force_cutoff a ValueError is raised.

        Parameters
        ----------
        list_cutoff : float
            The cutoff for the neighbor lists. Units depend on the _convert
            flag. If _convert = <True> then Angstrom, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        if _convert:
            list_cutoff *= units.ANGSTROM_TO_AU

        self.list_cutoff = np.float64(list_cutoff)

        if self.list_cutoff < self.force_cutoff:
            msg = 'List cutoff < force cutoff'
            raise ValueError(msg)

        self._skin = self.list_cutoff - self.force_cutoff
        self._skin_delta = (0.5*self._skin)**2

        self._use_neighbor_lists = True
        self.update_neighbor_lists()


    def set_positions(self, positions, _convert=False):
        """
        Set the system variable positions. Setting positions will require
        updates of Epot, etas and forces. In addition to the parent routine,
        neighbor lists will be updated if required.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The positions. Units depend on the _convert flag. If _convert
            = <True> then Angstrom, else (default) a.u.

        _convert : bool, optional (default=False)
            Flag controlling the unit conversion (see above). The flag is set
            <False> by default to maintain the previous framework.
        """
        PeriodicSystem.set_positions(self, positions, _convert=_convert)
        if self._use_neighbor_lists:
            self.update_neighbor_lists()


    def set_pair_potential(self, pair_potential):
        """
        Wrapper routine to set the pair potential from the outside

        Parameters
        ----------
        pair_potential : PairwisePotential instance or subclass thereof.
            Pair potential to be employed. For specfications see the respective
            class documentation(s).
        """
        self.pair_potential = pair_potential


    def get_pair_potential(self):
        """
        Return the attached PairwisePotential instance.

        Returns
        -------
        pair_potential : MDsim PairwisePotential instance
            The pair-potential invoked.
        """
        return self.pair_potential



    def get_potential_energy(self, _au=False):
        """
        Return the potential energy corresponding to the current configuration.
        This is a tweaked version from the parent as we have to set the
        _check_neighbor_lists flag to <False>

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV, regardless of <self._convert>.

        Returns
        -------
        Epot : float
            The potential energy. Units depend on the member variable
            "_convert", and the argument "_au", where the latter precedes the
            member variable.

            If _convert=<True> and _au=<False> : eV, else a.u.
        """
        if self._update_Epot:
            # we do not need the check as we can be sure to have the correct positions array
            self.set_potential_energy(self.calc_potential_energy(self.positions,
                                                                 _check_neighbor_lists=False),
                                      _convert=False)
        if self._convert:
            return self.Epot * units.AU_TO_EV
        else:
            return self.Epot


    def calc_potential_energy(self, positions, **kwargs):
        """
        Evaluate the *total* potential energy for a given configuration. This
        the sum of PES and pairwise energy.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        _check_neighbor_lists (kwargs) : bool, optional (default=True)
            Check whether we can use the neighborlists for self.positions. If
            not, brute force is actually cheaper than rebuilding the list and
            evaluating the target with the new lists.

            Only uncheck this option, if you really know what you are doing and
            if you can be sure that the neighborlists are ok with the positions
            fed in.

        Returns
        -------
        energy : float
            Corresponding potential energy (in a.u.).
        """
        return (self._calc_pes_potential_energy(positions)
                + self._calc_pairwise_potential_energy(positions,
                                                       **kwargs))



    # Those two routines need to be overloaded
    def get_forces(self, _au=False):
        """
        Return the forces corresponding to the current configuration of the
        system. This is a tweaked version from the parent as we have to set the
        _check_neighbor_lists flag to <False>

        *Please note that these may as well be just references, so do not
        directly modify the respective arrays in place.*

        Parameters
        ----------
        _au : bool, optional (default=False)
            Do *not* convert from a.u. to eV / Angstrom, regardless of
            <self._convert>.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces. Units depend on the member variable "_convert", and the
            argument "_au", where the latter precedes the member variable.

            If _convert=<True> and _au=<False> : eV / Angstrom, else a.u.
        """

        if self._update_forces:
            self.set_forces(self.calc_forces(self.positions,
                                             _check_neighbor_lists=False),
                            _convert=False)
        if _au or not self._convert:
            return self.forces
        else:
            return self.forces * units.AU_TO_EV_PER_ANGSTROM


    def calc_forces(self, positions, **kwargs):
        """
        Evaluate the *total* forces for a given configuration. This the
        additive sum of PES and pairwise forces.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        _check_neighbor_lists (kwargs) : bool, optional (default=True)
            Check whether we can use the neighborlists for self.positions. If
            not, brute force is actually cheaper than rebuilding the list and
            evaluating the target with the new lists.

            Only uncheck this option, if you really know what you are doing and
            if you can be sure that the neighborlists are ok with the positions
            fed in.

        Returns
        -------
        forces : (Natoms, Ndim) array
            Corresponding forces (in a.u.).
        """
        # do not save in separate array
        return (self._calc_pes_forces(positions)
                + self._calc_pairwise_forces(positions, **kwargs))


        # has to implemented by subclasses
    def _calc_pairwise_forces(self, positions, _check_neighbor_lists=True):
        """
        Evaluate the pairwise forces. Has to be implemented by subclass.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        _check_neighbor_lists : bool, optional (default=True)
            Check whether we can use the neighborlists for self.positions. If
            not, brute force is actually cheaper than rebuilding the list and
            evaluating the target with the new lists.

            Only uncheck this option, if you really know what you are doing and
            if you can be sure that the neighborlists are ok with the positions
            fed in.

        Returns
        -------
        forces : (Natoms, Ndim) array
            Corresponding forces (in a.u.).
        """
        raise NotImplementedError

    # has to implemented by subclasses
    def _calc_pairwise_potential_energy(self, positions, _check_neighbor_lists=True):
        """
        Evaluate the pairwise potential energy for a given configuration.
        Has to be implemented by subclass.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Configuration (units a.u.) for which to evaluate the potential
            energy.

        _check_neighbor_lists : bool, optional (default=True)
            Check whether we can use the neighborlists for self.positions. If
            not, brute force is actually cheaper than rebuilding the list and
            evaluating the target with the new lists.

            Only uncheck this option, if you really know what you are doing and
            if you can be sure that the neighborlists are ok with the positions
            fed in.

        Returns
        -------
        energy : float
            Corresponding potential energy (in a.u.).
        """
        raise NotImplementedError


    def _construct_neighbor_lists(self):
        """
        Routine that actually construct neighbor lists and attaches them to the
        system.
        """
        self._neighbor_lists_updates += 1
#        if self._verbose:
#            print('Updating neighbor list (total updates : {})'.format(self._neighbor_lists_updates))
        self._neighbor_lists, self._num_neighbors = self._neighbors_inst.construct_neighbor_lists(positions=self.positions,
                                                                                                  list_cutoff=self.list_cutoff,
                                                                                                  mic=True)
        self._positions_last_update = self.positions


    def _neighbor_lists_update_required(self, positions):
        """
        Check if we need to update neighbor lists because any position has
        changed by more than half the skin distance.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions to check against self._positions_last_update.

        Returns
        -------
        <True> if update is required, <False> if not.
        """

        _diff = self._positions_last_update - positions
        # searching the maximum is faster than comparison on all elements
        # return np.max(np.sum((_diff)**2, 1)) > self._skin_sq
        # in fact, einsum is much faster
        return np.max(np.einsum('ij,ij->i', _diff, _diff)) > self._skin_delta


    def update_neighbor_lists(self):
        """
        Update the neighbor lists. This call actually re-evaluates the neighbor
        lists only in two cases
            1) This is the first call to the routine, ie. there are no neighbor
               lists yet.
            2) Any particle has moved by more than half the skin distance since
               the last update.
        """
        if np.isnan(self.list_cutoff):
            raise ValueError('No list cutoff')

        if self._neighbor_lists_updates == 0:
            self._construct_neighbor_lists()
        elif self._neighbor_lists_update_required(self.positions):
            self._construct_neighbor_lists()
        else:
            pass


    def estimate_list_cutoff(self, dt, update_frequency):
        """
        Estimate a list cutoff based on the maximum velocity in the system.

            list_cutoff = (2 * dt * update_frequency * max_velocity) + force_cutoff

        The entity in parenthesis is the skin distance.

        Parameters
        ----------
        dt : float
            The time step size in a.u.

        update_frequency : int
            How many steps the neighbor lists should be valid (approximately).
            Of course, the longer you want to live with it, the larger it gets.

        Returns
        -------
        list_cutoff : float
            An approximation for a suitable cutoff based on the maximum
            velocity in the system.
        """
        max_velocity = math.sqrt(np.max(np.einsum('ij,ij->i', self.velocities, self.velocities)))
        return (2 * dt * update_frequency * max_velocity)  + self.force_cutoff


    def get_neighbor_list_updates(self):
        """
        Return the number of neighbor lists updates.

        Returns
        -------
        Nupdates : int
            Number of neighbor lists updates
        """
        return self._neighbor_lists_updates


    def get_neighbor_lists(self):
        """
        Obtain the current neighbor lists

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array
            The current neighbor lists. In principle, neighbor_list[i] is an
            array that contains the indices of neighbors of atom i. However,
            the need to be interpreted with care. Firstly, not all entries are
            valid as not every atom has Natom neighbors. Invalid entries are
            marked with "-1".  The number of valid entries for atom i is stored
            in "num_neighbors(i)" to allow for convenient looping (see below).
            Secondly, in order to avoid double counting, only neighbor indices
            j > i are stored in the neighbor list of atom i. Hence, simple
            loops over these lests automatically avoid double counting and save
            computation time.

        num_neighbors : (Natoms,) array
            The number of neighbors for all atoms. Allows for convenient loops.
            Note that only neighbor_lists[i, :num_neighbors[i][ are actually
            valid neighbor indices of atom[i].
        """
        return self._neighbor_lists, self._num_neighbors


    def get_neighbor_masks(self):
        """
        Obtain a True/False matrix indicating neighboring atoms.

        Returns
        -------
        neighbor_masks : (Natoms, Natoms) array
            Boolean array indicating neighboring atoms with <True>.
        """
        return self._neighbors_inst.convert_neighbor_lists_to_python_masks(self._neighbor_lists,
                                                                           self._num_neighbors)

    def get_neighbor_lists(self):
        """
        Obtain the current neighbor lists

        Returns
        -------
        neighbor_lists : (Natoms, Natoms) array.
            The neighbor lists. See the respective Neighbors module to
            understand their meaning.
        """
        return self._neighbor_lists

    def _create_pairwise_info(self):
        """
        Return additional information on the pairwise interaction model.
        """
        info = '\nPairwise interaction parameters:'
        info += '\n--------------------------------'
        info += '\n* Minimum image convention (MIC) is applied to evaluate'
        info += '\n  distances under periodic boundary conditions.'
        info += '\n'
        info += '\n* Cutoff distance for pairwise force evaluation : {} Angstrom'.format(self.force_cutoff * units.AU_TO_ANGSTROM)
        info += '\n'
        if self._use_neighbor_lists:
            info += '\n* Neighbour lists are used to speed up pairwise force evaluation.'
            info += '\n  Initial list cutoff distance (may change during run) : {} Angstrom'.format(self.list_cutoff * units.AU_TO_ANGSTROM)
        else:
            info += '\n* No neighbor lists are used, pairwise forces are evaluated brute-force instead.'
            info += '\n  Be aware that this scales horrible with system size!'

        return info


    def show_neighbor_list(self, iatom=None):
        """
        Visualization of the neighbor list creation. Currently only available
        for 2D systems. Directly piped to the attached Neighbor instance.

        Parameters
        ----------
        iatom : integer, optional (default = None)
            Atom for which the neighbor list is visualized. If none is given, a
            random choice will be made.
        """
        import matplotlib.pyplot as plt

        details = self._neighbors_inst._create_neighbor_list_plot(self.positions,
                                                                  self.list_cutoff,
                                                                  iatom=iatom)
        iatom = details['iatom']

        # the force cutoff
        fcirc = plt.Circle(self.positions[iatom],
                           radius=self.force_cutoff,
                           fill=True,
                           color='red',
                           alpha=0.3)
        ax = details['ax']
        ax.add_artist(fcirc)
        plt.show()

    def activate_neighbor_lists(self):
        """
        Activate the usage of neighbor lists
        """
        self._use_neighbor_lists = True
        self.update_neighbor_lists()

    def deactivate_neighbor_lists(self):
        """
        Deactivate the usage of neighbor lists
        """
        self._use_neighbor_lists = False

