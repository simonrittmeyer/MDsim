# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
System to deal with perdiodic pairwise dipole-dipole interactions modelled
using the Kohn-Lau potential.
"""
from __future__ import print_function

import numpy as np
import warnings

from MDsim import units
from MDsim.systems.pairwise import PairwiseSystem
from MDsim.potentials.pairwise.kohnlaupotential import KohnLauPotential


class KohnLauSystem(PairwiseSystem):
    """
    Class of periodic system where the particles interact via dipolar
    Kohn-Lau forces as mentioned in [1] and implemented via the
    MDsim.potentials.pairwise.kohnlau submodule. For details on the potential
    please refer to the docs there.

    [1] W. Kohn and K.-H. Lau, Solid State Commun. 18, 553 (1976).

    Additional keywords as compared to the PairwiseSystem is "dipoles" and
    "_coverage", while "pair_potential" is missing as it defaults to KohnLau
    anyway..

    Units
    -----
    If convert=<True>, then input is expected in Angstrom, femtoseconds, atomic
    mass units and electronvolt (or combinations thereof such as Debye).
    Internally, everything is handled in atomic units as defined here

        https://en.wikipedia.org/wiki/Atomic_units.

    If you want to directly pass atomic units (a.u.), use convert=False. Also
    the units of the get_X() methods will depend on this flag: It will be the
    same unit system as the input, ie. atomic units if convert=<False> and the
    "typical" units eV, A, ... if convert=<True>


    Notes
    -----
    In general, "_calc_X()" and "calc_X()" methods require a configuration as
    input, whereas "get_X()" and "_get_X()" methods always will use the current
    system configuration. The outside world should only use "get_X()", as this
    will also take care of proper unit conversion.

    I am very well aware of the principle of @properties in python. Yet this is
    grown code and I want to have the possibility of unit conversions. Hence we
    do not use this concept.


    Initialization
    --------------
    unit_cell : (Ndim, Ndim) array
        The *orthorhombic* unit cell. If the cell is not orthorhombic, an error
        will be raised.

        Unit if convert=<True> are Angstrom, else a.u.

    positions : (Natoms, Ndim) array
        The positions of atoms (or particles in a more general sense).

        Unit if convert=<True> are Angstrom, else a.u.

    force_cutoff : float
        The cutoff distance for the forces. Must be less than half of the
        shortest unit cell axis in order to make sure the applied MIC is
        justified.

    dipoles : (Natoms,) array or float
        The dipole moments in Debye if convert=<True>. If only a number is
        passed, a homogeneous system is assumed.

    list_cutoff : float, optional (default=None)
        The cutoff distance for the neighbor lists. Has to be >= force_cutoff.
        If a list_cutoff is provided upon initialization, neighbor lists will
        be made use of to evaluate the pairwise interactions. For details on
        this matter see the MDsim.neighbors submodule.

    velocities : (Natoms, Ndim) array, optional (default=None)
        The velocities associated with the atoms. If <None>, these will be
        drawn from a Maxwell-Boltzman distribution (see the temperature
        argument).

        Units if convert=<True> are Angstrom per femto second, else a.u.

    masses : (Natoms,) array, optional (default=None)
        The masses of the atoms/particles. If <None>, then masses will be
        deduced from "species" by interpreting them as atoms and using atomic
        masses.

        Units if convert=<True> are atomic mass units (amu), else a.u.

    species : (Natoms,) array, optional (default=None)
        The string identifyers of the atoms. Will be 'n.a.' for all elements if
        <None> is passed. Note that if "masses"=<None>, then this array must
        contain atomic symbols from which we can deduce masses.

    potential : MDsim Potential instance, optional (default=None)
        The interaction model in the sense of a potential energy surface. This
        instance has to provide a get_Epot() and get_forces() method, both if
        which are called with the system's positions (in a.u.). For more
        details see the MDsim.potentials submodule.

    friction : MDsim FrictionMatrix instance, optional (default=None)
        The friction matrix. If a float or integer is passed, it will be
        interpreted as constant (diagonal) friction. In this case, units are
        atomic mass units per femto second if convert=<True>, and a.u. else.

        Note that the term friction is a bit ambigious in the sense of
        Langevin-MD. We call friction "eta" in this context:

            m * d^2/dt^2 pos = -d/dpos V -eta * d/dt pos + R(t)

        Note that quite often one calls gamma = eta / m a friction coefficient.
        Thus be careful. For more details see the "MDsim.friction" submodule.

    temperature : float, optional (default=0.)
        The temperature of the system.

        Units are Kelvin if convert=<True>, a.u. else. Note that the
        temperature in a.u. is equivalent to thermal energy (=kT) in a.u.!

    fix_atoms : list of integers, optional (default=None)
        List of atoms that shall be fixed during the simulations. What this
        means effectively is that all the force components on these atoms will
        be zeros. Note that you can either specify "fix_atoms" or
        "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    fix_coordinates : (Natoms, Ndim) array (dtype=bool), optional (default=None)
        Array specifying fixed coordinates. Here, <True> fixes a coordinate,
        whereas <False> retains it free. Note that you can either specify
        "fix_atoms" or "fix_coordinates", not both.

        .. versionadded:: 3.4.4

    convert : bool, optional (default=True)
        Convert input units to a.u. or not. Also determines if output units
        will be converted back.

    _always_wrap_positions : bool, optional (default=False)
        Wrap back positions to the unit cell with every call to
        "set_positions()". Note that this may destroy correlation.

    _init_forces : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the potential will be made in the
        constructor of this object. This may be useful if the potential is only
        created afterwards.

    _init_etas : bool, optional (default=True)
        Option that should just be called in the __init__ of derived
        subclasses. If <False>, no call to the friction matrix will be made in
        the constructor of this object. This may be useful if the friction
        matrix is only created afterwards.

    _random_seed : int, optional (default=True)
        The seed for the RNG used to draw from a Maxwell-Boltzman distribution.
        Set this one in order to obtain reproducible results. If <None>, the
        seed will be randomized.

    _coverage : float, optional (default=None)
        The coverage with this setup. Will only be used on the information
        string.

    _cell_repeats : (Ndim,) array (default=None)
        The number of repetitions of the primitive cell to yield the super cell
        applied. Is only used for info string.

    _verbose : bool, optional (default=True)
        Print some (more) status information to stdout.
    """
    def __init__(self,
                 unit_cell,
                 positions,
                 force_cutoff,
                 dipoles,
                 **kwargs
                ):

        # the pair potential is set when we have the dipoles
        kwargs['pair_potential'] = None

        # ok, we initialize only after the parent
        _init_forces = kwargs.pop('_init_forces', True)
        kwargs['_init_forces'] = False

        # init the parent first
        PairwiseSystem.__init__(self,
                                unit_cell,
                                positions,
                                force_cutoff,
                                **kwargs)

        # set up the dipole moments (need dimensions for sanity check)
        if isinstance(dipoles, (int, float)):
            self.dipoles = np.ones(self.Natoms, dtype=np.float64, order='C')*dipoles
        else:
            self.dipoles = np.array(dipoles, dtype=np.float64, order='C')
            # sanity check for dimensions
            if not np.all(self.dipoles.shape == self.masses.shape):
                raise ValueError('Dipoles is of wrong dimensionality')

        if self._convert:
            self.dipoles *= units.DEBYE_TO_AU

        # create the pair potential instance
        # unit conversion is already done by parent class
        kohnlau_pot = KohnLauPotential(force_cutoff=self.force_cutoff,
                                       Natoms=self.Natoms,
                                       Ndim=self.Ndim,
                                       dipoles=self.dipoles,
                                       unit_cell=self.unit_cell,
                                       convert=False)

        self.set_pair_potential(kohnlau_pot)

        # the tail correction has to be evaluated only once
        try:
            self._pairwise_tail_correction = self.pair_potential.calc_tail_correction()
        except NotImplementedError:
            warnings.warn('No tail correction available.', RuntimeWarning)
            self._pairwise_tail_correction = 0

        # do not forget to initialize forces...
        if _init_forces:
            self._init_forces()

        self._name = 'Kohn-Lau Dipol System'


    def _get_atoms_info(self):
        """
        Get some atom specific information.
        """
        if (np.all(self.species == self.species[0])
                and np.all(self.masses == self.masses[0])
                and np.all(self.dipoles == self.dipoles[0])):
            info = 'Homogeneous system'
            info += '\n------------------'
            info += '\nspecies       : {}'.format(self.species[0])
            info += '\nmass          : {} amu'.format(self.masses[0]*units.AU_TO_AMU)
            info += '\ndipole moment : {} Debye'.format(self.dipoles[0]*units.AU_TO_DEBYE)
            if self._coverage is not None:
                info += '\ncoverage      : {} monolayer'.format(self._coverage)
        else:
            info = '{0:<10s}{1:>20s}{2:>20s}{3:>30s}'.format('Atom ID',
                                                             'species',
                                                             'mass (amu)',
                                                             'dipole moment (Debye)')
            info += '\n' + '-' * 80
            for i, (s, m, d) in enumerate(zip(self.species, self.masses, self.dipoles)):
                info += '\n{0:<10d}{1:>20s}{2:>20.8f}{3:30.8f}'.format(i, s, m * units.AU_TO_AMU, d * units.AU_TO_DEBYE)

            if self._coverage is not None:
                info += '\n'
                info += '\n--> coverage : {:.6f} monolayer'.format(self._coverage)

        return info


    def _calc_pairwise_forces(self, positions, _check_neighbor_lists=True):
        """
        Routine to calculate forces on the atoms due the pairwise interaction
        potential.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the forces.

        _check_neighbor_lists : bool, optional (default=True)
            Check whether we can use the neighborlists for self.positions. If
            not, brute force is actually cheaper than rebuilding the list and
            evaluating the target with the new lists.

            Only uncheck this option, if you really know what you are doing and
            if you can be sure that the neighborlists are ok with the positions
            fed in.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces at the respective positions. This array is C-contigious.

        """
        _use_neighbor_lists = self._use_neighbor_lists

        if _use_neighbor_lists:
            if _check_neighbor_lists:
                # if positions have not changed we can use our neighbor lists
                # if they have changed, bruteforce is faster than creating
                # the list + using it
                _use_neighbor_lists = not self._neighbor_lists_update_required(positions)

        if _use_neighbor_lists:
            return self.pair_potential.calc_forces_neighborlist(positions=positions,
                                                                neighbor_lists=self._neighbor_lists,
                                                                num_neighbors=self._num_neighbors
                                                               )
        else:
            return self.pair_potential.get_forces_bruteforce(positions=positions)


    def _calc_pairwise_potential_energy(self, positions, _check_neighbor_lists=True):
        """
        Return the potential energy due to the pairwise interactions including
        the tail correction accounting for finite force cutoff.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the energy.

        _check_neighbor_lists : bool, optional (default=True)
            Check whether we can use the neighborlists for self.positions. If
            not, brute force is actually cheaper than rebuilding the list and
            evaluating the target with the new lists.

            Only uncheck this option, if you really know what you are doing and
            if you can be sure that the neighborlists are ok with the positions
            fed in.

        Returns
        -------
        Epot_pairwise : float
            Tail corrected interaction energy.

        """
        _use_neighbor_lists = self._use_neighbor_lists

        if _use_neighbor_lists:
            if _check_neighbor_lists:
                # if positions have not changed we can use our neighbor lists
                # if they have changed, bruteforce is faster than creating
                # the list + using it
                _use_neighbor_lists = not self._neighbor_lists_update_required(positions)
        if _use_neighbor_lists:
            Epot_pairwise = self.pair_potential.calc_Epot_neighborlist(positions=positions,
                                                                       neighbor_lists=self._neighbor_lists,
                                                                       num_neighbors=self._num_neighbors)

        else:
            Epot_pairwise = self.pair_potential.calc_Epot_bruteforce(positions=positions)

        return Epot_pairwise + self._pairwise_tail_correction


    def _change_support(self, support):
        """
        Change the applied speedup support.

        Parameters
        ----------
        support : string
            One of 'fortran', 'cython', 'numba', 'fallback'
        """
        self.pair_potential._change_support(support)
        self._neighbors_inst._change_support(support)

    def get_coverage(self):
        """
        Return the _coverage flag. Just used for IO reasons.
        """
        return self._coverage
