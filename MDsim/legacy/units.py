# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# load the constants recommended by CODATA, 2010
from scipy.constants import physical_constants as const
import scipy.constants as c

# mass
AMU_TO_AU =  const['atomic mass constant'][0] / const['atomic unit of mass'][0]
AU_TO_AMU = 1. / AMU_TO_AU

# time
FS_TO_AU = c.femto / const['atomic unit of time'][0]
AU_TO_FS = 1. / FS_TO_AU

# velocity
ANGSTROM_PER_FS_TO_AU = c.angstrom / (const['atomic unit of velocity'][0] * c.femto)
AU_TO_ANGSTROM_PER_FS = 1. / ANGSTROM_PER_FS_TO_AU

# length
ANGSTROM_TO_AU = c.angstrom / const['atomic unit of length'][0]
AU_TO_ANGSTROM = 1. / ANGSTROM_TO_AU

# energy
EV_TO_AU = 1. / const['Hartree energy in eV'][0]
AU_TO_EV = 1. / EV_TO_AU

# energy dissipation
EV_PER_FS_TO_AU = EV_TO_AU / FS_TO_AU
AU_TO_EV_PER_FS = 1./ EV_PER_FS_TO_AU

# force
EV_PER_ANGSTROM_TO_AU = EV_TO_AU / ANGSTROM_TO_AU
AU_TO_EV_PER_ANGSTROM = 1. / EV_PER_ANGSTROM_TO_AU

# force constant
EV_PER_ANGSTROM_SQUARED_TO_AU = EV_TO_AU / (ANGSTROM_TO_AU **2)
AU_TO_EV_PER_ANGSTROM_SQUARED = 1./EV_PER_ANGSTROM_SQUARED_TO_AU

# friction coefficients
AMU_PER_FS_TO_AU = AMU_TO_AU / FS_TO_AU
AU_TO_AMU_PER_FS = 1./AMU_PER_FS_TO_AU

# frequencies
KAYSER_TO_EV = c.h * c.c / c.centi / const['electron volt-joule relationship'][0]
EV_TO_KAYSER = 1. / KAYSER_TO_EV
KAYSER_TO_AU =  KAYSER_TO_EV * EV_TO_AU
AU_TO_KAYSER = 1./KAYSER_TO_AU
KAYSER_TO_THZ = 1. / c.centi /const['hertz-inverse meter relationship'][0] / c.tera
AU_TO_THZ=AU_TO_KAYSER*KAYSER_TO_THZ
THZ_TO_AU=1./AU_TO_THZ

# dipoles
# conversion factor from wikipedia
DEBYE_TO_AU = 3.33564e-30 / const['atomic unit of electric dipole mom.'][0]
AU_TO_DEBYE = 1./DEBYE_TO_AU

# temperature goes directly to thermal energy
AU_TO_KELVIN = const['hartree-kelvin relationship'][0]
KELVIN_TO_AU = 1./AU_TO_KELVIN

KELVIN_TO_EV = KELVIN_TO_AU * AU_TO_EV
EV_TO_KELVIN = 1./KELVIN_TO_EV

# momenta
ANGSTROM_AMU_PER_FS_TO_AU = ANGSTROM_PER_FS_TO_AU * AMU_TO_AU
AU_TO_ANGSTROM_AMU_PER_FS = 1./ANGSTROM_AMU_PER_FS_TO_AU

# Diffusion constant
EV_FS_PER_AMU_TO_AU = EV_TO_AU * FS_TO_AU / AMU_TO_AU
AU_TO_EV_FS_PER_AMU = 1. / EV_FS_PER_AMU_TO_AU
