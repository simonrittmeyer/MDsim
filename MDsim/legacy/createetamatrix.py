# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import sys
import os
import StringIO
import glob
import warnings

from MDsimv3.tools                            import check_for_array
from MDsimv3.tools                            import check_kwargs 
from MDsimv3.electronicfriction               import EtaMatrix
from MDsimv3.electronicfriction               import ConstantEtaMatrixNormalmode
from MDsimv3.electronicfriction               import CombinedEtaMatrices
from MDsimv3.electronicfriction               import InterpolatedCube 
from MDsimv3.customization.custom2d.eta       import InterpolatedEta2D
from MDsimv3.customization.custom2d.diatomic  import InterpolatedEtaDiatomic


# -----------------------------------------------------------------------------
# FRICTION MATRIX
# -----------------------------------------------------------------------------

def _create_eta_instances2D(atomsEq, etafile, **kwargs):
    param = {'usecols' : range(4),
             'flavor'  : 'perp',
             'scaling'  : None}
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    flavor = param['flavor'].lower()

    # read the friction file
    data = np.loadtxt(etafile, 
                      usecols = param['usecols'],
                      unpack  = True)
    
    # create the individual eta objects (2D interpolations)
    etas = []
    
    if param['scaling'] == None:
        param['scaling'] = [1.0, 1.0]
    
    for i, eta in enumerate(data[2::]):
        print '\tcreating InterpolatedEtaDiatomic instance for atom #{}'.format(i)
        print '\t\tusing transformation flavor "{}"'.format(flavor)
        # scale 
        scale = float(param['scaling'][i])
        if scale != 1.0:
            print '\t\t--> scaled friction coefficient for atom #{} with factor {}'.format(i, scale)
            eta = eta * scale
        
        if flavor == 'perp':
            eta_obj = InterpolatedEtaDiatomic(points      = data[0:2], 
                                              etas        = eta, 
                                              atoms_eq    = atomsEq,
                                              flavor      = flavor,
                                              convert     = True, 
                                              kind        = 'cubic',
                                              data_origin = etafile)
        
        elif flavor == 'para':
            eta_obj = InterpolatedEta2D(points      = data[0:2], 
                                        etas        = eta, 
                                        convert     = True, 
                                        kind        = 'cubic',
                                        data_origin = etafile)
        else:
            raise NotImplementedError
        
        etas.append(eta_obj)
    
    return etas


def create_eta_instances(dimensions, **kwargs):
    dimensions == int(dimensions)

    if dimensions == 2:
        return _create_eta_instances2D(**kwargs)


def create_EtaMatrix(atomsEq, **kwargs):
    param = {'datapath'  : 'data',
             'dimensions': 2,
             'usecols'   : None,
             'flavor'    : 'perp',
             'etafile'   : None,
             'scaling'   : None}
    
    dimensions = int(param['dimensions'])
    
    if param['usecols'] is None:
        param['usecols'] = range(2*dimensions)

    print 'creating EtaMatrix instance ({}-dim)'.format(dimensions)
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    if param['etafile'] == None:
        etafile = glob.glob(os.path.join(param['datapath'],'*.eta'))[0]
    else:
        etafile = param['etafile']

    print '\tusing file {}'.format(etafile)
    print '\tusing columns : {}'.format(param['usecols'])

    # get the etas instances
    etas = create_eta_instances(dimensions = dimensions,
                                atomsEq = atomsEq, 
                                etafile = etafile, 
                                flavor  = param['flavor'],
                                usecols = param['usecols'],
                                scaling = param['scaling'])
    
    etaMatrix = EtaMatrix(atomsEq = atomsEq,
                             eta_instances = etas)

    return etaMatrix


# not required anymore.... we interpolate eta anyways, so there is no
# difference in that sense
# both iaa and aim can be called with the full position vector, the
# interpolation function takes care of this

#def _create_etaMatrixIAA(atomsEq, usecols, **kwargs):
#    param = {'datapath' : 'data',
#             'dimensions' : 2,
#             'etafile'  : None,
#             'scaling'  : None}
#    
#    dimensions = int(param['dimensions'])
#    
#    print 'creating EtaMatrixIAA instance ({}-dim)'.format(dimensions)
#    
#    if check_kwargs(kwargs, param):
#        param.update(kwargs)
#    
#    if param['etafile'] == None:
#        etafile = glob.glob(os.path.join(param['datapath'],'*.eta'))[0]
#    else:
#        etafile = param['etafile']

#    print '\tusing file {}'.format(etafile)
#    print '\tusing columns : %s'%usecols

#    # get the etas instances
#    etas = create_eta_instances(dimensions = dimensions,
#                                atomsEq = atomsEq, 
#                                etafile = etafile, 
#                                usecols = usecols,
#                                scaling = param['scaling'])

#    etaMatrixIAA = EtaMatrixIAA(eta_instances = etas)

#    return etaMatrixIAA


#def _create_EtaMatrixAIM(atomsEq, usecols, **kwargs):
#    param = {'datapath' : 'data',
#             'dimensions' : 2,
#             'etafile'  : None,
#             'scaling'  : None}
#    
#    dimensions = int(param['dimensions'])
#    
#    print 'creating EtaMatrixAIM instance ({}-dim)'.format(dimensions)
#    
#    if check_kwargs(kwargs, param):
#        param.update(kwargs)
#    
#    if param['etafile'] == None:
#        etafile = glob.glob(os.path.join(param['datapath'],'*.eta'))[0]
#    else:
#        etafile = param['etafile']

#    print '\tusing file {}'.format(etafile)
#    print '\tusing columns : %s'%usecols

#    # get the etas instances
#    etas = create_eta_instances(dimensions = dimensions,
#                                atomsEq = atomsEq, 
#                                etafile = etafile, 
#                                usecols = usecols,
#                                scaling = param['scaling'])
#    
#    etaMatrixAIM = EtaMatrixAIM(eta_instances = etas)

#    return etaMatrixAIM


def create_constantEtaMatrix(eta_matrix, normalmodes_obj):
  
    eta_matrix = check_for_array(eta_matrix)

    print 'creating ConstantEtaMatrixNormalmodes instance'

    constantEtaMatrixNormalmodes = ConstantEtaMatrixNormalmode(eta_matrix = eta_matrix,
                                                               normalmodes = normalmodes_obj)
    return constantEtaMatrixNormalmodes


def create_combinedEtaMatrices(*eta_matrices):
    
    print 'merging *EtaMatrix* instances'
    combinedEtaMatrices = CombinedEtaMatrices(*eta_matrices)

    return combinedEtaMatrices


#def create_etaMatrixIAA_from_cube(**kwargs):
#    # DO NOT USE, THIS IS INCREDIBLY SLOW!
#    # RATHER MAP THE FRICTION COEFFICIENT BEFOREHAND
#    param = {'datapath'      : 'data',
#             'cubefile'      : None,
#             'species'       : ['C','O'],
#             # settings for Eta instances
#             'sp'            : False,     
#             'extrapolation' : False,
#             # settings for InterpolatedCube (some settings are hard coded!)
#             'constrain_x'   : 0.,
#             'constrain_y'   : 0.,
#             'constrain_z'   : None
#             }
#    
#    print 'creating EtaMatrixIAA instance'
#    
#    if check_kwargs(kwargs, param):
#        param.update(kwargs)

#    # just to make sure that we have an absolute path
#    param['datapath'] = os.path.abspath(param['datapath'])
#    
#    # we need information about the species
#    species = param['species']

#    # create the individual eta instances
#    etas = []
#    for s in species:
#        etas.append(Eta(species       = s,
#                        sp            = param['extrapolation'],
#                        extrapolation = param['extrapolation']
#                        )
#                    )           
#       
#    # create the density instance from the cube file
#    if param['cubefile']:
#        cubefile = glob.glob(os.path.join(param['datapath'],'*.cube'))[0]
#    else:
#        cubefile = param['cubefile']

#    print '\tusing file {}'.format(cubefile)
#   
#    den = InterpolatedCube(cubefile    = cubefile,
#                           convert     = True,
#                           PBC         = True,
#                           shift       = 'center',
#                           order       = 3,
#                           constrain_x = param['constrain_x'],
#                           constrain_y = param['constrain_y'],
#                           constrain_z = param['constrain_z'])

#    # create the density instances (twice the same, since IAA)
#    densities = []
#    for s in species:
#        densities.append(den)

#    # create the EtaMatrixIAA instance
#    # individual Eta instances are deepcopied internally!
#    etaMatrixIAA = EtaMatrixIAA(eta_instances = etas,
#                                density_instances = densities)        

#    return etaMatrixIAA

