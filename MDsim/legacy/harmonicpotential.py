# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import numpy as np

from MDsim import units
from MDsim.potentials import Potential


class HarmonicPotential(Potential):
    """
    Harmonic Potential corresponding to the harmonic frequencies from the
    normalmodes analysis.
    
    Initialization
    --------------
    Normalmodes      : Normalmodes instance
                       Instance providing all needed transformation matrices.
                       Furthermore, the initial positions are taken from the
                       equilibrium positions of the underlying *.phonon file.
                       Make sure that your Normalmode instance provides the
                       corresponding trafo matrices as well as positions
                       A/A_per_fs/M units and *NOT* atomic units (will be
                       converted internally).
    
    Call
    ----
    positions        : (Natoms*Ndof x 1) array
                        Positions vector of the entire system in a.u.
    
    Returns
    -------
    Epot             : float
                       Potential energy at given point.
                       
                       NOTE: You might also want to consider the
                       get_Etots_normalmodes() method.
    """
    def __init__(self, normalmodes):
        self.normalmodes_obj = normalmodes
        
        # get mode names
        self.mode_names = self.normalmodes_obj.get_mode_names()
        
        # normalmode frequencies (in eV --> convert)
        # avoid converting KAYSER to a.u. (which kind of is crap...) hence we
        # go via the energies (in a.u. h==1)!
        self.hnus  = self.normalmodes_obj.get_energies()*units.EV_TO_AU
        self.omegas = self.hnus.copy()

        # only do the multiplication once (h==1 in atomic units)
        self.halfomegas2 = 0.5*self.omegas**2

        self.Ndim = len(self.omegas)
    
    def __str__(self):
        name  = 'Harmonic potential from normalmode analysis with frequencies:'
        for n, f in zip(self.mode_names, self.omegas):
            f_eV     = f * units.AU_TO_EV
            f_kayser = f_eV * units.EV_TO_KAYSER 
            f_THz    = f_kayser * units.KAYSER_TO_THZ
            name += '\n\t{0:<15s} : {1:>10.3f} cm^-1 = {2:>10.3f} THz = {3:>10.6f} eV'.format(n, f_kayser, f_THz, f_eV)
        return name
   
    def get_Epot(self, positions):
        """
        Return the potential energy of the system at <position> (in carthesian
        coordinates).
        """
        Epots = self.get_Epots_normalmodes(positions)
        return sum(Epots)
     
    def get_Epots_normalmodes(self, positions):
        """
        Return the potential energy at <position> decomposed in basis of normal
        modes, ie. the potential energy within each mode.
        """
        nm = self.normalmodes_obj.pos_to_nm(positions, au = True)
        Epots = self.halfomegas2 * nm**2
        return Epots
        
    # FIXME: Implement properly 
    #copied from interpolated potential
    def get_force(self, positions, method = '', d = 0.01):
        # d is the finite difference step size
        force = np.empty_like(positions)
        
        # evaluate the gradient using central differences
        for i, p in enumerate(positions):
            disp  = positions.copy()
            disp_ = positions.copy()
            disp[i]  += d
            disp_[i] -= d
            force[i] = -(self.get_Epot(disp) - self.get_Epot(disp_)) / (2*d)  
        return force

