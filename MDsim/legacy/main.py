# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import time
import pickle

from MDsim.options import args
from MDsim.tools.parser import parse_inputfile
from MDsim.tools.sanity import check_completeness
from MDsim.tools.sanity import check_consistency
from MDsim.tools.sanity import find_files
from MDsim.tools.minimize import minimize_atoms
from MDsim.io import logo
from MDsim.io import limiter
from MDsim.io import big_limiter
from MDsim.io import format_timing
from MDsim.io import update_progress
from MDsim.io import IOinterface
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_atomsEq
from MDsim.tools.createobjects import create_oneBodyAtomsEq
from MDsim.tools.createobjects import create_paraAtomsEq
from MDsim.tools.createobjects import create_interpolatedPotential
from MDsim.tools.createobjects import create_aseNormalmodes
from MDsim.tools.createobjects import create_castepNormalmodes
from MDsim.tools.createobjects import create_harmonicPotential
from MDsim.tools.createobjects import create_EtaMatrix
from MDsim.tools.createobjects import create_surfaceOscillatorForces
from MDsim.tools.createobjects import create_surfaceOscillatorSystem
from MDsim.tools.createobjects import create_numericalForces
from MDsim.tools.createobjects import create_analyticalForces
from MDsim.tools.createobjects import create_normalmodeSystem
from MDsim.tools.createobjects import create_GLOSystem
from MDsim.tools.createobjects import create_fileHandlers
from MDsim.tools.createobjects import create_movieHandler


def main(args = args, cmd_settings = dict()):
    """
    Main routine that parses the input file, builds all objects and runs the
    simulation.

    For a detailed description on all arguments see the options module.

    Parameters
    ----------
    args : dictionary, optional (default = `args` from options.py)
        Default options. I cannot imagine any case where you would want to
        change the default value.

    cmd_settings: dictionary, optional (default = {})
        Options passed from the command line parser. Any option in here will
        overwrite whatever is in the input file and the default arguments.
    """

    start = time.time()

    print logo[:-1]

    # -----------------------------------------------------------------------------
    # Infile parsing
    # -----------------------------------------------------------------------------

    try:
        infile = cmd_settings.pop('infile')
        infile_settings = parse_inputfile(infile)
        args.update(infile_settings)
    except KeyError:
        print 'No input file given'

    # in conflict, cmd_settings overwrites infile_settings
    args.update(cmd_settings)

    print limiter

    # check for completeness and consistency
    check_completeness(args)
    check_consistency(args)
    args = find_files(args)

    print limiter

    # -----------------------------------------------------------------------------
    # Creating the MD system
    # -----------------------------------------------------------------------------

    print '--> Initializing MD system'
    # dictionary that stores all objects created
    obj = dict()

    # paths
    args['datapath'] = os.path.abspath(os.path.expanduser(args['datapath']))

    # create the Integrator instance
    integrator = create_integrator(method = args['integrator'],
                                   dt     = args['timestep'])
    obj['integrator'] = integrator


    if args['system'] in ['normalmodesystem',
                          'glosystem',
                          'sosystem',
                          'onebodysystem']:

        # first create the _atomsEq with the coordinates to which the potential
        # and etas refer to (no optimization on the interpolated potential!)

        # the fake atomsEq
        if args['system'] == 'onebodysystem':
            _atomsEq = create_oneBodyAtomsEq(masses = args['masses'])

        elif (args['ndim'] == 2 and args['transformation_flavor'] == 'para'):
            _atomsEq = create_paraAtomsEq(datapath = args['datapath'],
                                          cellfile = args['cellfile'])
        # the real atomsEq
        else:
            _atomsEq = create_atomsEq(datapath = args['datapath'],
                                      cellfile = args['cellfile'])

        obj['_atomsEq'] = _atomsEq


        # create the atomsEq and normalmode instance
        # not castepnormalmodes support for the onebody system
        if args['normalmodes'] == 'ase' or args['onebodysystem']:
            # if we choose ``ase'' normalmodes then the geometry of the input atoms
            # object will first be optimized on the interpolated potential (which is of
            # course necessary to be provided).

            # to calculate the normalmodes, we need an interpolated_potential in any
            # case...


            potential = create_interpolatedPotential(atomsEq    = _atomsEq,
                                                     dimensions = args['ndim'],
                                                     datapath   = args['datapath'],
                                                     potfile    = args['potfile'],
                                                     flavor     = args['transformation_flavor'],
                                                     usecols    = args['potfile_usecols'])

            obj['_potential'] = potential

            # use forces consistent with the MD forces
            atomsEq = minimize_atoms(_atomsEq, potential,
                                     fmax             = args['geo_opt_fmax'],
                                     forces           = args['forces'],
                                     forces_stepwidth = args['forces_stepwidth'])


            normalmodes = create_aseNormalmodes(atomsEq       = atomsEq,
                                                potential_obj = potential,
                                                delta         = args['normalmode_delta'],
                                                mode_names    = args['normalmode_names'])

        elif args['normalmodes'] == 'castep':
            # no changes in the _atomsEq due to minimization, hence use the same
            atomsEq = _atomsEq

            normalmodes = create_castepNormalmodes(atomsEq    = atomsEq,
                                                   datapath   = args['datapath'],
                                                   mode_names = args['normalmode_names'])


        # create the potential instance... If we use normalmodes from ASE we already
        # have the corresponding potential
        if args['potential'] == 'interpolated' and not args['normalmodes'] == 'ase':
            potential = create_interpolatedPotential(atomsEq    = _atomsEq,
                                                     dimensions = args['ndim'],
                                                     flavor     = args['transformation_flavor'],
                                                     datapath   = args['datapath'],
                                                     potfile    = args['potfile'],
                                                     usecols    = args['potfile_usecols'])

        elif args['potential'] == 'harmonic':
            potential = create_harmonicPotential(normalmodes_obj = normalmodes)


        # create the friction matrices
        # no friction support (yet) for one-body system
        if args['system'] == 'onebodysystem':
            etaMatrix = None
        else:
            if args['friction'] in ['aim', 'iaa']:
                etaMatrix = create_EtaMatrix(atomsEq    = _atomsEq,
                                             dimensions = args['ndim'],
                                             datapath   = args['datapath'],
                                             flavor     = args['transformation_flavor'],
                                             etafile    = args['etafile'],
                                             usecols    = args['etafile_usecols'],
                                             scaling    = args['etascale'])

            elif args['friction'] == 'none':
                etaMatrix = None


        # create system and forces instances
        if args['system'] == 'sosystem':
            forces = create_surfaceOscillatorForces(potential_obj   = potential,
                                                    surface_element = args['so_element'],
                                                    surface_freqs   = args['so_frequencies'],
                                                    stepwidth       = args['so_stepwidth'])

            system = create_surfaceOscillatorSystem(atomsEq                     = atomsEq,
                                                    normalmodes_obj             = normalmodes,
                                                    surfaceOscillatorForces_obj = forces,
                                                    etaMatrix_obj               = etaMatrix,
                                                    init_quanta                 = args['initial_quanta'],
                                                    surface_temp                = args['surface_temperature']
                                                    )

        elif args['system'] in ['normalmodesystem', 'onebodysystem']:
            # the onebody systems is as well a normalmode system but with a fake atomsEq
            if args['forces'] == 'numerical':
                forces = create_numericalForces(potential_obj = potential,
                                                stepwidth = args['forces_stepwidth'])
            elif args['forces'] == 'analytical':
                forces = create_analyticalForces(potential_obj = potential)

            system = create_normalmodeSystem(atomsEq         = atomsEq,
                                             normalmodes_obj = normalmodes,
                                             forces_obj      = forces,
                                             etaMatrix_obj   = etaMatrix,
                                             init_quanta     = args['initial_quanta'])

            if args['system'] == 'onebodysystem':
                # now fake the name ;)
                system.set_name('OneBody System (a.k.a. Normalmode System)')

            elif args['ndim'] == 2 and args['transformation_flavor'] == 'para':
                if args['species'] is None:
                    species = atomsEq.get_species()
                else:
                    species = args['species']
                system.set_species(species)

        elif args['system'] == 'glosystem':
            #print etaMatrix
            system = create_GLOSystem(atomsEq           = atomsEq,
                                      normalmodes_obj   = normalmodes,
                                      potential_obj     = potential,
                                      etaMatrix_obj     = etaMatrix,
                                      init_quanta       = args['initial_quanta'],
                                      glo_temperature   = args['glo_temperature'],
                                      so_element        = args['glo_so_element'],
                                      so_freqs          = args['glo_so_frequencies'],
                                      ghost_freqs       = args['glo_ghost_frequencies'],
                                      so_ghost_coupling = args['glo_so_ghost_coupling'],
                                      ghost_gamma       = args['glo_ghost_gamma'],
                                      time_step         = args['timestep'],
                                      stepwidth         = args['forces_stepwidth']
                                      )




        # collect objects in obj dict
        obj['potential'] = potential
        obj['forces'] = forces
        obj['etaMatrix'] = etaMatrix
        obj['atomsEq'] = atomsEq
        obj['system'] = system
        obj['args'] = args

        try:
            obj['normalmodes'] = normalmodes
        except:
            pass

    else:
        # here comes your extension for new systems
        raise NotImplementedError('System `{}` not (yet) implemented'.format(args['system']))

    print limiter
    print '--> MD system initialized:'

    system_info = IOinterface.get_system_info(system)
    integrator_info = IOinterface.get_integrator_info(integrator)


    info  = system_info.replace('\n','\n\t')
    info += '\n'

    info += integrator_info.replace('\n','\n\t')

    if args['debug']:
        debug_info = IOinterface.get_options(args)
        info += '\n'
        info += debug_info.replace('\n','\n\t')

    print info
    print limiter

    # -----------------------------------------------------------------------------
    # Creating the IO handlers
    # -----------------------------------------------------------------------------

    # output path
    args['outpath'] = os.path.abspath(os.path.expanduser(args['outpath']))

    # fallback if no seed is passed
    if args['seed'] is None:
        args['seed'] = os.path.basename(infile).split('.')[0]

    # extend the seed
    args['seed'] += '__{}-pot'.format(args['potential'])
    args['seed'] += '_{}-friction'.format(args['friction'])


    if args['friction'] != 'none' and args['etascale'] is not None:
        args['seed'] += '_etascale'
        for i in args['etascale']:
            args['seed'] += '-{}'.format(i)

    for i, q in enumerate(args['initial_quanta']):
        if args['normalmode_names'] is not None:
            args['seed'] += '_{}'.format(args['normalmode_names'][i])
        else:
            args['seed'] += '_mode{}'.format(i)

        args['seed'] += '-{0:04.1f}-hnu'.format(q)

    if args['system'] == 'sosystem':
        args['seed'] += '_SO-temp-{}K'.format(args['so_temperature'])
        args['seed'] += '_freqs'
        for f in args['so_frequencies']:
            args['seed'] += '-{0:.1f}meV'.format(f)


    if args['system'] == 'glosystem':
        args['seed'] += '_GLO-temp-{}K'.format(args['glo_temperature'])

    if not args['dryrun']:
        # file IO handlers
        if args['debug']:
            debug = args
        else:
            debug = None
        handlers = create_fileHandlers(system_obj     = system,
                                       integrator_obj = integrator,
                                       observables    = args['observables'],
                                       seed           = args['seed'],
                                       outfolder      = args['outpath'],
                                       args           = debug,
                                       gzip           = args['gzip'])

        # movie
        if args['movie'] is not None:
            movieHandler = create_movieHandler(system_obj = system,
                                               fileformat = args['movie'],
                                               seed       = args['seed'],
                                               outfolder  = args['outpath'])
            handlers.append(movieHandler)

        print 'Output folder:'
        print '\t{}'.format(args['outpath'])
        print 'Common output seed:'
        print '\t{}'.format(args['seed'])


    # -----------------------------------------------------------------------------
    # The actual simulation
    # -----------------------------------------------------------------------------

    # Only returning objects
    if args['dryrun']:
        print 'This is a dryrun.'
        print limiter
        return obj


    print big_limiter
    print '--> STARTING SIMULATION'

    # simulation loop
    step = 0

    # get the variables directly --> perfomance
    io_step = args['iostep']
    dt      = args['timestep']
    tcancel = args['simulation_time']

    update_step = io_step * 100

    while True:
        t = step * dt

        # IO
        if step%io_step == 0:
            for h in handlers:
                h.push()
#        if step%update_step == 0:
#            update_progress(t/tcancel)

        if t >= tcancel:
                break

        # propagate
        integrator.propagate(system)
        step += 1

    print '--> SIMULATION FINISHED'
    print '\tClosing handlers...'
    for h in handlers:
        h.close()

    stop = time.time()

    print '--> runtime: {}'.format(format_timing(start, stop))
    print big_limiter
    return None
