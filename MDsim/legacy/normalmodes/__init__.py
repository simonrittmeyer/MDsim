# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

# unit conversion
from MDsim import units


class Normalmodes(object):
    r""" This is a class handling the results from a
    normalmodes analysis. It only provides a framework but cannot be used directly.
    You will have to derive a subclass and implement a proper __init__ routine that
    actually provides the following arrays:

    Transformation matrices:
    ------------------------
    * Q, Qinv, U, Uinv, M, Minv U_au, Uinv_au, M_au, Minv_au

    System properties (sliced according to free DOF!):
    --------------------------------------------------
    * R0, masses_sqrt, R0_au, masses_sqrt_au, energies, displacements

    Further matrices (needed for friction analysis):
    ------------------------------------------------
    * mu, muinv, mu_au, muinv_au, _Qmu, _Qmu_au, _muinvQinv, _muinvQinv_au


    Nomenclature
    ============

    Consider the displacement vector

        x = R - R_0,                                                        (1)

    where R is the position vector of the displaced system in Carthesian
    coordinates and R_0 corresponds to the equilibrium configuration of the
    system, likewise in Carthesian coordinates.

    This displacement vector can be transformed into the basis of mass-weighted
    Carthesian coordinates using the transformation matrix M, i.e.

        Mx = y.                                                             (2)

    Hence, y is the displacement vector in mass-weighted Carthesian
    coordinates. The corresponding transformation matrix M is of diagonal
    structure with

        M_ij = sqrt(m_i) * delta_ij,                                        (3)

    where m_i is the mass associated with coordinate i, and delta_ij is the
    Kronecker delta.

    The transformation matrix Q transforms the displacement vector in
    mass-weighted Carthesian coordinates into the basis of (mass-weighted)
    normalmodes:

        Qy = q.                                                             (4)

    Q is the matrix that diagonalizes the force constant matrix F, i.e. the
    Hessian of the potential energy in Carthesian coordinates at R_0. Thus, Q
    consists of the eigenvalues of F, i.e. the so-called normalmodes. Since F
    is symmetric by construction, Q is an ORTHOGONAL MATRIX such that

        Q^T Q = Q Q^T = 1 and hence Q^-1 = Q^T.                             (5)

    The matrix Q can be directly taken from the *.phonon file resulting from
    the CASTEP normalmode analysis.

    The direct transformation from carthesian to (mass-weighted) normalmode
    basis corresponds to a successive application of M and Q

        Q(Mx) = Qy = q.                                                     (6)

    Hence, defining the matrix U := QM one can write

        Ux = q.                                                             (7)

    Note that U is in general not orthogonal.


    When propagating a system under friction, the corresponding friction matrix
    in normalmode basis, which we denote gamma, is given by

        gamma = Q mu eta Q^-1,                                              (8)

    where mu_ij = (1/m_i)*delta_ij and m_i is the mass associated with
    coordinate i.


    NOTE
    ====

    This class is very similar to the one in castep2aqd. However, the latter is
    more geared towards stand-alone usage.
    """

    def __init__(self):
        # in the end, this routine must provide:
        # Q, Qinv, U, Uinv, M, Minv, R0, mu
        # atoms_eq, atoms_mask, constraints_mask, modes_mask,
        # mode_names,
        # all transformation matrices in atomic units, i.e.
        # Q_au, Qinv_au, U_au, Uinv_au, M_au, Minv_au, R0_au, mu_au

        # use _create_trafo_matrices !
        print('Error : Constructor of Normalmode class needs to be implemented')
        raise NotImplementedError

    # -----------------------
    # transformation matrices
    # -----------------------

    def get_U(self, au = True):
        """
        Returns the transformation matrix U as build from the normalmodes in
        Carthesian Coordinates. This matrix is not orthogonal!
        If au = True, output in atomic units.
        """
        if au:
            return self.U_au.copy()
        else:
            return self.U.copy()

    def get_Uinv(self, au = True):
        """
        Returns the inverse of the transformation matrix U as build from the
        normalmodes in Carthesian Coordinates.
        If au = True, output in atomic units.
        """
        if au:
            return self.Uinv_au.copy()
        else:
            return self.Uinv.copy()

    def get_Q(self, au = True):
        """
        Returns the transformation matrix Q as build from the CASTEP normalmodes
        in mass weighted coordinates. This matrix IS orthogonal.
        If au = True, output in atomic units.
        """
        if au:
            return self.Q_au.copy()
        else:
            return self.Q.copy()

    def get_Qinv(self, au = True):
        """
        Returns the inverse of the transformation matrix Q (a.k.a. the
        transpose, since Q is orthogonal) as build from the CASTEP normalmodes
        in mass weighted coordinates.
        If au = True, output in atomic units.
        """
        if au:
            return self.Qinv_au.copy()
        else:
            return self.Qinv.copy()

    def get_M(self, au = True):
        """
        Returns the transformation matrix from Carthesian coordinates into mass
        weighted coordinates.
        If au = True, output in atomic units.
        """
        if au:
            return self.M_au.copy()
        else:
            return self.M.copy()

    def get_Minv(self, au = True):
        """
        Returns the inverse of the transformation matrix from Carthesian
        coordinates into mass weighted coordinates.
        If au = True, output in atomic units.
        """
        if au:
            return self.Minv_au.copy()
        else:
            return self.Minv.copy()

    def get_mu(self, au = True):
        """
        Returns the mu-matrix, consisting of the diagonal inverse masses:

            mu_ij = (1/m_i)*delta_ij

        If au = True, then output in atomic units.
        """
        if au:
            return self.mu_au.copy()
        else:
            return self.mu.copy()


    # --------------------------
    # coordinate transformations
    # --------------------------

    def pos_to_nm(self, R, au = True):
        """
        Convert a coordinate array in Carthesian basis into normalmode basis.
        If au = True, input and output in atomic units.
        """
        if au:
            x = R.flatten() - self.R0_au
            return self.U_au.dot(x)
        else:
            x = R.flatten() - self.R0
            return self.U.dot(x)

    def nm_to_pos(self, nm, au = True):
        """
        Convert a positions array from normalmode basis to a coordinate array
        in Carthesian basis.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.Uinv_au.dot(nm) + self.R0_au
        else:
            return self.Uinv.dot(nm) + self.R0

    def car_to_mw(self, x, au = True):
        """
        Function that converts input from carthesian coordinates to
        mass-weighted coordinates.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.M_au.dot(x)
        else:
            return self.M.dot(x)

    def mw_to_car(self, y, au = True):
        """
        Function that converts input from mass-weighted Carthesian coordinates
        to Carthesian coordinates.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.Minv_au.dot(y)
        else:
            return self.Minv.dot(y)

    def car_to_nm(self, x, au = True):
        """
        Function that converts input from carthesian coordinates to
        (mass-weighted) normalmodes.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.U_au.dot(x)
        else:
            return self.U.dot(x)

    def nm_to_car(self, q, au = True):
        """
        Function that converts input from (mass-weighted) normalmodes to
        carthesian displacements.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.Uinv_au.dot(q)
        else:
            return self.Uinv.dot(q)

    def mw_to_nm(self, y, au = True):
        """
        Function that converts input from mass-weighted carthesian
        displacements to (mass-weighted) normalmodes.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.Q_au.dot(y)
        else:
            return self.Q.dot(y)

    def nm_to_mw(self, q, au = True):
        """
        Function that converts input from normalmodes to (mass-weighted)
        carthesian displacements.
        If au = True, input and output in atomic units.
        """
        if au:
            return self.Qinv_au.dot(q)
        else:
            return self.Qinv.dot(q)


    # --------------------------
    # conversion to atoms object
    # --------------------------

    def nm_to_atoms(self, q, au = True):
        """
        Function that converts input from normalmodes to a full atoms object.
        If au = True, input in atomic units.
        """
        x = self.nm_to_car(q, au = au)

        return self.car_to_atoms(x, au = au)

    def mw_to_atoms(self, y, au = True):
        """
        Function that converts input from mass-weighted carthesian
        displacements to a full atoms object.
        If au = True, input in atomic units.
        """

        x = self.mw_to_car(y, au = True)

        return self.car_to_atoms(x, au = True)

    def car_to_atoms(self, x, au = True):
        """
        Function that converts input from carthesian coordiantes to a full
        atoms object.
        If au = True, input in atomic units.
        """

        # this is now in Angstrom
        if au:
            R  = x + self.R0_au
            R *= units.AU_TO_ANGSTROM
        else:
            R = x + self.R0

        return self.pos_to_atoms(R, au = False)

    def pos_to_atoms(self, R, au = True):
        """
        Function that converts from a given positions vector R in carthesian
        coordinates to a full atoms object.
        If au = True, input in atomic units.

        """
        # get equilibrium positions of entire system
        atoms = self.atoms_eq.get_full_atoms_obj()
        constraints_mask = self.atoms_eq.get_constraints_mask().flatten()

        pos = atoms.get_positions().flatten()

        # assign new positions
        new_pos = R.copy()
        if au:
            new_pos *= units.AU_TO_ANGSTROM
        pos[constraints_mask] = new_pos

        # reshape again
        pos.shape = (-1, 3)

        atoms.set_constraint(None)
        atoms.set_positions(pos)

        return atoms

    # ----------------------
    # vibrational properties
    # ----------------------

    def get_frequencies(self):
        return self.energies.copy() * units.EV_TO_KAYSER

    def get_energies(self):
        # this is in eV anyway
        return self.energies.copy()

    def get_mode_names(self):
        """
        Returns the mode names in the correct order.
        """
        if self.mode_names is None:
            return ['n.a.' for i in self.Q]
        else:
            return self.mode_names

    # --------------------------------
    # Friction-related transformations
    # --------------------------------

    def get_gamma_matrix(self, etas, au = True):
        """
        Returns a given eta matrix (in cartesian basis) in normalmode basis.
        This allows to asses friction-induced couplings

        The friction matrix in normalmode basis, which we denote gamma, is given by

        gamma = Q mu eta Q^-1,

        where mu_ij = (1/m_i)*delta_ij and m_i is the mass associated with
        coordinate i.

        If the eta matrix passed is of diagonal shape only, it will be expanded
        accordingly.

        If au = True, then input/ouput in atomic units.
        """

        # check for eta dimensions
        if len(etas.shape) == 1:
            etas = np.diag(etas)

        if au:
            return self._Qmu_au.dot(etas.dot(self.Qinv_au))
        else:
            return self._Qmu.dot(etas.dot(self.Qinv))

    def get_eta_matrix(self, gamma, au = True):
        """
        Return the friction matrix in cartesian basis as constructed from a
        gamma matrix in normalmode basis.

        If au = True, then input/ouput in atomic units.
        """
        if au:
            return self._muinvQinv_au.dot(gamma.dot(self.Q_au))
        else:
            return self._muinvQinv.dot(gamma.dot(self.Q))

    # --------------------------------
    # internal routines for subclasses
    # --------------------------------

    def _get_modes_mask(self, energies, Emin = 0.):
        """
        Routine to create a modes mask based on energies.
        All modes with energies < Emin will be excluded

        Parameters
        ----------
        energies : (Natoms * Ndim x 1) array
            Vibrational energies.

        Emin : float, optional (default = 0.0)
            Minimum energy a mode has to carry in order to be considered a true
            vibrational mode.

        Returns
        -------
        modes_mask : (Natoms * Ndim x 1) array
            Mask that is <True> for all considered modes and <False> for the
            others.
        """
        modes_mask = np.asarray(energies > Emin, dtype = 'bool')

        return modes_mask


    def _create_trafo_matrices(self, atoms_eq, displacements, energies):
        """
        This routine does two things:

        a) it filters the displacements and energies arrays
        b) it creates the transformation arrays needed

        The return value is a dictionary that should be used to update the
        __dict__ of the instance!

        Please note that we will drop the explicit (Natoms x Ndim) shape of the
        positions arrays here, as this makes thing smuch more complicated.
        Instead we rather work on flat arrays here.

        Parameters
        ----------
        atoms_eq : Modified ase.Atoms instance
            The atoms object that specifies the equilibrium configuration and
            the constraints.

        displacements : (Ndim * Natoms x Ndim * Natoms) array
            The normalmodes in mass-weighted carthesian coordinates.
            Note that the first index names the mode, the second index names
            its components.

        energies : (Ndim * Natoms x 1) array
           The vibrational energies.


        Returns
        -------
        data : dictionary
            Dictionary holding all necessary transformation matrices. For
            details see below.
        """

        # -------------------
        # filter the raw data
        # -------------------

        # get the masks we need
        modes_mask = self._get_modes_mask(energies)

        # get the constraints_mask to filter the raw data
        # This must be a flat array to match the dimensions of the displacement
        # vectors!
        constraints_mask = atoms_eq.get_constraints_mask().ravel()

        # filter the energies
        energies = energies[modes_mask]

        # DISPLACEMENTS
        # slice non-zero frequency modes
        displacements = displacements[modes_mask,:]

        # slice atomic constraints
        displacements = displacements[:,constraints_mask]

        # convert to real numbers
        displacements = displacements.real

        # MASSES

        # get the (original) masses array
        masses_sqrt = np.sqrt(atoms_eq._get_masses())

        # extend dimensionality from (Natoms x 1) to (3*Natoms x 1)
        masses_sqrt = np.repeat(masses_sqrt, 3)

        # filter according to constraints
        masses_sqrt = masses_sqrt[constraints_mask]

        # -----------------------------------
        # create the transformation matrices
        # -----------------------------------

        # Build the ORTHOGONAL transformation matrix Q which brings us
        # mass weighted carthesian --> normal modes
        #
        # According to Wilson, the matrix which brings us from
        # mass-weighted <-- normal coordinates is just the amplitudes
        # assosciated with the normal modes in mass-weighted basis...
        # Hence, Qinv is just the displacements matrix and Q is its transpose
        #
        # NOTE: the first index must name the mode and the second index must
        # name the coordinate. This means that the eigenvectors must be
        # aligned in rows, i.e. first index in python.

        # make really sure that Q and Qinv are independent here
        Qinv = displacements.copy()
        Q    = Qinv.copy().transpose()

        # carthesian --> mass weighted carthesian
        M     = np.diag(masses_sqrt)
        Minv  = np.diag(1. / masses_sqrt)

        # carthesian --> normal modes
        # THIS MATRIX IS NOT ORTHOGONAL!
        U     = np.dot(Q, M)

        # calculate the inverse
        Uinv  = np.dot(Minv, Qinv)

        # equilibrium positions
        R0    = self.atoms_eq.get_positions_eq().flatten()

        # handle to transform the friction matrix
        mu    = np.diag(1. / (masses_sqrt**2))
        muinv = np.diag(masses_sqrt**2)

        # transform to atomic units
        U_au      = U.copy()    * np.sqrt(units.AMU_TO_AU)
        Uinv_au   = Uinv.copy() * 1./np.sqrt(units.AMU_TO_AU)
        M_au      = M.copy()    * np.sqrt(units.AMU_TO_AU)
        Minv_au   = Minv.copy() * 1./np.sqrt(units.AMU_TO_AU)
        Q_au      = Q.copy()    # for convenience
        Qinv_au   = Qinv.copy() # for convenience
        R0_au     = R0.copy()   * units.ANGSTROM_TO_AU
        mu_au     = mu.copy()   * 1./units.AMU_TO_AU
        muinv_au  = mu.copy()   * units.AMU_TO_AU
        masses_sqrt_au = masses_sqrt.copy() * np.sqrt(units.AMU_TO_AU)

        # just some helper matrices to avoid unnessary multiplications
        _Qmu          = np.dot(Q, mu)
        _Qmu_au       = np.dot(Q_au, mu_au)
        _muinvQinv    = np.dot(muinv, Qinv)
        _muinvQinv_au = np.dot(muinv_au, Qinv_au)

        # return dictionary with all created matrices and masks
        data = {# in A/amu units
                'Q'                : Q,
                'Qinv'             : Qinv,
                'U'                : U,
                'Uinv'             : Uinv,
                'M'                : M,
                'Minv'             : Minv,
                'R0'               : R0,
                'mu'               : mu,
                'muinv'            : muinv,
                '_Qmu'             : _Qmu,
                '_muinvQinv'       : _muinvQinv,
                'masses_sqrt'      : masses_sqrt,
                # in atomic units
                'Q_au'             : Q_au,
                'Qinv_au'          : Qinv_au,
                'U_au'             : U_au,
                'Uinv_au'          : Uinv_au,
                'M_au'             : M_au,
                'Minv_au'          : Minv_au,
                'R0_au'            : R0_au,
                'mu_au'            : mu_au,
                'muinv_au'         : muinv_au,
                '_Qmu_au'          : _Qmu_au,
                '_muinvQinv_au'    : _muinvQinv_au,
                'masses_sqrt_au'   : masses_sqrt_au,
                # miscellaneous stuff
                'modes_mask'       : modes_mask,
                'constraints_mask' : constraints_mask,
                'energies'         : energies,
                'displacements'    : displacements
                }

        return data

