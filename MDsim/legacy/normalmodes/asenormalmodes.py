# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

# vibrational handling
from ase.vibrations import Vibrations

# unit conversion
from MDsim import units

# parent class
from MDsim.coordinates.normalmodes import Normalmodes


class ASENormalmodes(Normalmodes):
    def __init__(self, atoms_eq, vib_obj, use_modes = [],  mode_names = None):
        """
        TODO
        """

        self.mode_names = mode_names

        # equilibrium properties
        self.atoms_eq = atoms_eq
        self.R0       = self.atoms_eq.get_positions_eq().flatten()

        # get all information necessary from the vibrations object
        self.vib_obj = vib_obj
        self.energies = self.vib_obj.get_energies().real

        # these are in mass weighted Carthesian coordinates
        self.displacements = self.vib_obj.modes.copy()

        # process the use_modes flag
        if use_modes == []:
            self.modes_mask = self._get_modes_mask(self.energies)
        else:
            self.modes_mask = np.zeros((len(self.energies)), dtype = bool)
            for idx in use_modes:
                self.modes_mask[idx] = True
        # data contains: Q, Qinv, U, Uinv, M, Minv, modes_mask,
        # constraints_mask, and filtered versions of energies, displacements
        data = self._create_trafo_matrices(self.atoms_eq, self.displacements,
                                           self.energies, self.modes_mask)

        # assign the content from data to instance variables
        self.__dict__.update(data)


    def __str__(self):
        name  = 'ASENormalmodes instance'
        name += '\nAssociated energies and frequencies:'
        name += '\n------------------------------------'

        energies = self.energies.copy()
        omegas   = self.energies.copy() * units.EV_TO_KAYSER

        for e,n,o in zip(energies, self.get_mode_names(), omegas):
            name += '\n\t{0:<15s} : {1:>10.3f} cm^-1 = {2:>10.6f} eV'.format(n, o, e)
        return name

