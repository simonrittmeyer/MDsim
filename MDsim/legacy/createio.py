# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsimv3.tools                            import check_kwargs
from MDsimv3.io                               import FileHandler
from MDsimv3.io                               import MovieHandler

# -----------------------------------------------------------------------------
# I/O
# -----------------------------------------------------------------------------

def create_fileHandlers(system_obj, integrator_obj, **kwargs):
    # this is specified for normal mode systems

    param = {'observables' : ['positions',
                              'velocities',
                              'energy',
                              ],
             'seed'        : 'MDsimv3',
             'outfolder'   : 'output',
             'args'        : None,
             'gzip'        : True}


    if check_kwargs(kwargs, param):
        param.update(kwargs)

    print 'creating FileHandler instances'

    fileHandlers = []

    for observable in param['observables']:
        print '\tcreating handler for "{}"'.format(observable)
        fileHandler = FileHandler(system     = system_obj,
                                  integrator = integrator_obj,
                                  observable = observable,
                                  seed       = param['seed'],
                                  outfolder  = param['outfolder'],
                                  args       = param['args'],
                                  gzip_posthoc = param['gzip'])
        fileHandlers.append(fileHandler)

    return fileHandlers


def create_movieHandler(system_obj, **kwargs):

    param = {'fileformat'  : 'xyz',
             'seed'        : 'MDsimv3',
             'outfolder'   : 'output',
             'gzip'        : True}


    if check_kwargs(kwargs, param):
        param.update(kwargs)

    print 'creating MovieHandler instance'
    print '\tusing file format {}'.format(param['fileformat'])

    movieHandler = MovieHandler(system     = system_obj,
                                seed       = param['seed'],
                                outfolder  = param['outfolder'],
                                fileformat = param['fileformat'],
                                gzip       = param['gzip'])

    return movieHandler



