# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsimv3.tools                            import check_kwargs 
from MDsimv3.forces                           import NumericalForces
from MDsimv3.forces                           import NormalmodeForces
from MDsimv3.forces                           import AnalyticalForces
from MDsimv3.so                               import SOForces

# -----------------------------------------------------------------------------
# FORCES 
# -----------------------------------------------------------------------------

def create_analyticalForces(potential_obj):
    
    print 'creating AnalyticalForces instance'
    
    analyticalForces = AnalyticalForces(potential_obj) 

    return analyticalForces


def create_numericalForces(potential_obj, **kwargs):
    param = {'stepwidth' : 0.01}
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    print 'creating NumericalForces instance'
    
    numericalForces = NumericalForces(potential_obj, 
                                      stepwidth = param['stepwidth'])

    return numericalForces


def create_surfaceOscillatorForces(potential_obj, 
                                   surface_element, 
                                   surface_freqs, 
                                   **kwargs):
    
    param = {'stepwidth'      : 0.01,
             'convert'        : True}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    print 'creating SOForces instance'
    print '\telement: {}'.format(surface_element)
    print '\tfrequencies: %s'%surface_freqs
    surfaceOscillatorForces = SOForces(potential = potential_obj,
                                                      surface_element = surface_element,
                                                      surface_freqs = surface_freqs,
                                                      stepwidth = param['stepwidth'],
                                                      convert = param['convert'])

    return surfaceOscillatorForces

#def create_normalmodeForces(normalmodes_obj):
#    
#    print 'creating NormalmodeForces instance'
#    
#    normalmodeForces = NormalmodeForces(normalmodes_obj)
#    
#    return normalmodeForces
    
