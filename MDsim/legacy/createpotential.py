# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import sys
import os
import StringIO
import glob
import warnings

from MDsimv3.tools                            import check_for_array
from MDsimv3.tools                            import check_kwargs
from MDsimv3.potentials                       import HarmonicPotential
from MDsimv3.customization.custom1d           import InterpolatedPotential1D
from MDsimv3.customization.custom2d.potential import InterpolatedPotential2D
from MDsimv3.customization.custom2d.diatomic  import InterpolatedPotentialDiatomic

# -----------------------------------------------------------------------------
# POTENTIAL
# -----------------------------------------------------------------------------

def _create_interpolatedPotential1D(atomsEq, **kwargs):
    # atomsEq is just a dummy argument, in the 1D case we do not need it
    # anyways...

    param = {'datapath' : 'data',
             'potfile'  : None,
             'usecols'  : (0,1)}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    print 'creating Interpolated1DPotential instance'

    if param['potfile'] == None:
        potfile = glob.glob(os.path.join(param['datapath'],'*.pot'))[0]
    else:
        potfile = param['potfile']

    print '\tusing file {}'.format(potfile)

    # read in the potential values
    datapoints, E = np.loadtxt(potfile,
                               usecols = param['usecols'],
                               unpack = True)

    #create the potential instance
    interpolated1DPotential = InterpolatedPotential1D(points = datapoints,
                                                      energies = E,
                                                      convert = True,
                                                      kind = 'cubic',
                                                      data_origin = potfile)

    return interpolatedPotential


def _create_interpolatedPotential2D(atomsEq, **kwargs):
    param = {'datapath' : 'data',
             'flavor'   : 'perp',
             'usecols'  : range(3),
             'potfile'  : None}

    if check_kwargs(kwargs, param):
        param.update(kwargs)

    flavor = param['flavor'].lower()

    print 'creating Interpolated2DPotential instance'
    print '\tusing transformation flavor "{}"'.format(flavor)

    if param['potfile'] == None:
        potfile = glob.glob(os.path.join(param['datapath'],'*.pot'))[0]
    else:
        potfile = param['potfile']

    print '\tusing file {}'.format(potfile)
    print '\tusing columns : %s'%param['usecols']
    # read in the potential values
    data = np.loadtxt(potfile,
                      usecols = param['usecols'],
                      unpack = True)

    if flavor == 'generic':
        raise NotImplementedError

    elif flavor == 'perp':
        #create the potential instance
        interpolatedPotential = InterpolatedPotentialDiatomic(
                                                points = data[0:2],
                                                energies = data[-1],
                                                atoms_eq = atomsEq,
                                                convert = True,
                                                kind = 'cubic',
                                                data_origin = potfile)


    elif flavor == 'para':
        # just an synomym for the generic potential
        interpolatedPotential = InterpolatedPotential2D(points = data[0:2],
                                                        energies = data[-1],
                                                        convert = True,
                                                        kind = 'cubic',
                                                        data_origin = potfile)

    return interpolatedPotential


# wrapper routine...
def create_interpolatedPotential(dimensions, **kwargs):

    dimensions = int(dimensions)

    if dimensions == 1:
        # the 1D potential cannot handle this keyword
        kwargs.pop('flavor', None)
        return _create_interpolatedPotential1D(**kwargs)

    elif dimensions == 2:
        return _create_interpolatedPotential2D(**kwargs)


def create_harmonicPotential(normalmodes_obj):

    print 'creating HarmonicPotential instance'

    harmonicPotential = HarmonicPotential(normalmodes_obj)

    return harmonicPotential
