# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsimv3 import units

class AnalyticalForces(object):
    def __init__(self, potential):
        self.potential = potential # function with get_force method!
        
        if hasattr(self.potential, 'get_force_analytical'):
            self.force_func = self.potential.get_force
        else:
            print 'Sorry, your potential does not support get_force_analytical()!'
            print 'Consider using a NumericalForce instance instead'
            exit(1)        
            
        # pipe the Epots_normalmodes from the potential if available
        if hasattr(self.potential, 'get_Epots_normalmodes'):
            self.get_Epots_normalmodes = self.potential.get_Epots_normalmodes
            
    def get_Epot(self, positions):
        return self.potential(positions)
    
    def __str__(self):
        name  = 'Analytical forces using spline derivatives from potential:'
        name += '\n\t{}'.format(str(self.potential).replace('\n','\n\t'))
        return name
        
    def __call__(self, positions):
        return self.force_func(positions)
