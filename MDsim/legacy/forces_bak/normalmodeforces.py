# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsimv3 import units

# THIS IS NOT YET FUNCTIONAL!        
class NormalmodeForces(object):
    def __init__(self, normalmodes):
        self.normalmodes_obj = normalmodes
        
        # get mode names
        self.mode_names = self.normalmodes_obj.get_mode_names()
        
        # normalmode frequencies (we want the omegas -> 2pi)
        self.omegas  = self.normalmodes_obj.get_frequencies()*2*np.pi
        
        # convert the frequencies to atomic units
        self.omegas *= units.KAYSER_TO_EV
        
    def __call__(self, positions):
        # convert position to normalmode space
        nm        =   self.normalmodes_obj.pos_to_nm(positions, au = True)
        forces_nm = - self.omegas**2 * nm
        forces    =   self.normalmodes_obj.nm_to_car(positions, au = True)
        return forces
    
    def __str__(self):
        name  = 'Harmonic forces derived from normalmode analysis with frequencies:'
        for n, f in zip(self.mode_names, self.omegas):
            f_kayser = f * units.AU_TO_KAYSER / (2*np.pi)
            f_eV     = f_kayser * units.KAYSER_TO_EV
            f_THz    = f_kayser * units.KAYSER_TO_THZ
            name += '\n\t{0:<15s} : {1:>10.3f} cm^-1 = {2:>10.3f} THz = {3:>10.6f} eV'.format(n, f_kayser, f_THz, f_eV)
        name += '\n\t*.phonon file: {}'.format(self.normalmodes_obj.get_phononfile())
        return name
    
    def get_Epots_normalmodes(self, positions):
        nm = self.normalmodes_obj.pos_to_nm(positions, au = True)
        Epots = 0.5*(self.omegas**2 * nm**2)
        return Epots
        
    def get_Epot(self, positions):
        Epots = self.get_Epots_normalmodes(positions)
        return sum(Epots)
