# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy.random import normal
from scipy.constants import physical_constants

from ase.data import atomic_masses, atomic_numbers

import MDsimv3.units as units
import MDsimv3.tools as tools

class GLOForces(object):
    """
    Modified potential energy surface to implement the generalized Langevin oscillator (GLO)
    model. What we do in essence, is to shift the adsorbae PES according to a
    moving surface oscillator (SO).
   
    Moreover, to mimic phononic dissipation in the bulk, we couple the SO to a
    ghost oscillator, which in turn is acted on by dissipative friction forces
    as well as fluctuation white noise forces.     


    For more details see for instance
        H. F. Busnengo, M. A. Di Cesare, W. Dong and A. Salin, Phys. Rev. B 72, 125411, (2005). 
        DOI: 10.1103/PhysRevB.72.125411 

    This object is to be used like a force instance during the simulations.


    Intialization
    -------------
    ``potential'' : Any callable object/function corresponding to a potential.
        This instance must take the ``real'' coordinate system used in the MD
        simulation as input (in a.u.), i.e. without the ghost coordinate, and
        return a potential value (in a.u.).

    ``so_element'' String
        Element the surface consists of. Will be used to lookup the respective
        mass from the ase.data dictionaries.

    ``so_freqs'' : Array
        Frequencies of the SO in x, y, and z direction in a.u.. At the
        same time, the number of frequencies given determines the
        dimensionality of the SO.

    ``so_ghost_coupling'' : Array
        Couplings between surface oscillator and ghost coordinate in a.u.!

    ``ghost_freqs'' : array
        Frequencies of the ghost oscillator in a.u.. Must be of same
        dimensionality as so_freqs.

    ``ghost_gamma'' : float 
        Constant friction coefficient on the ghost atom. We assume isotropic
        friction, hence it is a number not a matrix. Caution: gamma = m*eta!

    ``temperature'' : float, optional (default = 0)
        Surface temperature in Kelvin.

    ``time_step'' : float, optional (default = 0.1)
        Integration time step (determines the flcutuating forces)

    ``stepwidth'' : float (default = 0.01)
        Stepwidth while evaluating the gradient:
                    
            F_i = -(potential(pos_i+d) - potential(pos_i-d)) / (2*d)
                  
        There is yet no possibility to apply adaptive stepwidths and/or
        different stepwidths for different coordinates. But feel free to
        implement whatever you need.
    
    ``convert'' : Boolean (default = True)
        Converts frequency input from meV --> a.u. If set False, you will have
        to provide a.u. input.
    
    Call
    ----
    ``positions'' : (Natoms*Ndof + Ndim_surface x 1) array
        Position and ghost coordinate at which to evaluate the force.
        Internally, the positions vector will be split into ``real'' and ghost
        coordinates to evaluate the respective forces on them.

    
    Returns
    -------
    ``force'' : (Natoms*Ndof + Ndim_so + Ndim_ghost x 1) array
        Forces at the specified position (in a.u., if potential is constructed
        as specified above)
    
    NOTE: There is a method get_Epot() which pipes the potential evaluation,
          and if available, there will also be get_Epots_normalmodes().
    """

    def __init__(self, 
                 potential, 
                 so_element, 
                 so_freqs,
                 so_ghost_coupling,
                 ghost_freqs,
                 ghost_gamma,
                 temperature = 0,
                 time_step = 0.1,
                 stepwidth = 0.001,
                 convert = True):
        
        # adsorbate
        self.potential = potential
        
        # surface oscillator   
        self.so_element = so_element
        self.so_mass = atomic_masses[atomic_numbers[so_element]] * units.AMU_TO_AU
        self.so_freqs = tools.check_for_array(so_freqs)
        
        # so-ghost coupling
        self.so_ghost_coupling = tools.check_for_array(so_ghost_coupling)
        
        # ghost oscillator
        self.ghost_freqs = tools.check_for_array(ghost_freqs)
        self.ghost_gamma = tools.check_for_array(ghost_gamma)
        self.ghost_mass = self.so_mass
        self.ghost_eta = self.ghost_gamma * self.ghost_mass

        # number of surface degrees of freedom
        self.so_Ndim = len(self.so_freqs)
        self.ghost_Ndim = len(self.ghost_freqs)
        self.system_Ndim = self.potential.get_Ndim()
        
        # information for the white noise source
        self.temperature = temperature
        self.kT = temperature * physical_constants['Boltzmann constant in eV/K'][0] 
        self.time_step = time_step

        if convert:
#            self.so_freqs *= units.EV_TO_AU
#            self.ghost_freqs *= units.EV_TO_AU
#            self.so_ghost_coupling *= units.EV_TO_AU
            self.kT *= units.EV_TO_AU
            self.time_step *= units.FS_TO_AU
        
        # we only need to calculate the square once...
        self.minus_so_freqs_squared_times_mass = -1 * self.so_freqs**2 * self.so_mass
        self.minus_ghost_freqs_squared_times_mass = -1 * self.ghost_freqs**2 * self.ghost_mass
        self.so_ghost_coupling_times_mass = -1 * self.so_ghost_coupling * self.so_mass

        self.variance = np.sqrt(2.*self.kT * self.ghost_eta / (self.so_mass * self.time_step))
        self.stddev = np.sqrt(self.variance)

        self.stepwidth = stepwidth
        
        # pipe the normalmode decomposition of potential energy if possible
        try:
            self.get_Epots_normalmodes = self.potential.get_Epots_normalmodes
        except AttributeError:
            pass

    def hand_variables(self):
        var = {'ghost_freqs' : self.ghost_freqs,
                'ghost_mass' : self.ghost_mass,
                'ghost_Ndim' : self.ghost_Ndim,
                'ghost_eta' : self.ghost_eta,
                'ghost_gamma' : self.ghost_gamma,
                'so_element' : self.so_element,
                'so_mass' : self.so_mass,
                'so_freqs' : self.so_freqs,
                'so_Ndim' : self.so_Ndim,
                'so_ghost_coupling' : self.so_ghost_coupling,
                'temperature' : self.temperature,
                'time_step' : self.time_step,
                'system_Ndim': self.system_Ndim}
        return var
    
    def __str__(self):
        name  = 'GLO forces using central differences (stepwidth = {})'.format(self.stepwidth)
        name += '\nSurface temperature: {}K'.format(self.temperature)
        name += '\n\tUnderlying potential:'
        name += '\n\t\t{}'.format(str(self.potential).replace('\n','\n\t\t'))
        name += '\n\tSO element: {} (m = {} amu)'.format(self.so_element, 
                                                              self.so_mass * units.AU_TO_AMU)
        name += '\n\tSO frequencies:'
        for i,f in enumerate(self.so_freqs):
            name += '\n\t\tomega_{0:d}  = {1:10.5E} a.u. = {2:10.5E} eV = {3:10.5E} cm^-1'.format(i,
                                                f,
                                                f*units.AU_TO_EV, 
                                                f*units.AU_TO_KAYSER)
        name += '\n\tSO-ghost coupling:' 
        for i,c in enumerate(self.so_ghost_coupling):
            name += '\n\t\tlambda_{1:d} = {0:10.5E} a.u. = {2:10.5E} eV/A**2'.format(c,i,
                                      c * units.AU_TO_EV / (units.AU_TO_ANGSTROM**2)) 
        
        name += '\n\tGhost frequencies:' 
        for i,f in enumerate(self.ghost_freqs):
            name += '\n\t\tomega_{0:d}  = {1:10.5E} a.u. = {2:10.5E} eV = {3:10.5E} cm^-1'.format(i,
                                                f,
                                                f*units.AU_TO_EV, 
                                                f*units.AU_TO_KAYSER)
            name += '\n\tGhost friction coefficient: {0:.2f}'.format(self.ghost_eta[0]) 
        return name

    def _divide_positions(self, positions):
        """
        Divide into ``real'' and artificial DOF.
        We assume, that the artificial DOF, i.e. those of the surface
        oscillator are the very last ones.
        """
        ads_pos   = positions[:-(self.so_Ndim + self.ghost_Ndim)]
        so_pos    = positions[-(self.so_Ndim + self.ghost_Ndim):-self.ghost_Ndim]
        ghost_pos = positions[-self.ghost_Ndim:] 

        return ads_pos, so_pos, ghost_pos


    def _ads_force(self, ads_pos, so_pos):
        ads_force = np.empty_like(ads_pos)
        for i, p in enumerate(ads_pos):
            disp  = ads_pos.copy()
            disp_ = ads_pos.copy()
            disp[i]  += self.stepwidth
            disp_[i] -= self.stepwidth
            ads_force[i] = -(self._shifted_pes(disp, so_pos) 
                             - self._shifted_pes(disp_ ,so_pos)) / (2*self.stepwidth)  
            if ads_force[i] == np.nan:
                raise RuntimeWarning('Seems you are running out of your fitted region')
                force[i] = 0.

        return ads_force 

    def _so_force(self, ads_pos, so_pos, ghost_pos):
        # harmonic contribution
        so_force = self.minus_so_freqs_squared_times_mass * so_pos

        # coupling to the ghost coordinate
        so_force += self.so_ghost_coupling_times_mass * ghost_pos 

        # contribution from the shifted interpolated PES
        for j, s in enumerate(so_pos):
            disp  = so_pos.copy()
            disp_ = so_pos.copy()
            disp[j]  += self.stepwidth
            disp_[j] -= self.stepwidth
            so_force[j] += -(self._shifted_pes(ads_pos, disp) 
                             - self._shifted_pes(ads_pos, disp_)) / (2*self.stepwidth)  
        
        return so_force

    def _ghost_force(self, so_pos, ghost_pos):
        ghost_force  = self.minus_ghost_freqs_squared_times_mass * ghost_pos
        # add coupling
        ghost_force += self.so_ghost_coupling_times_mass * so_pos
        # add random force
        if self.stddev > 0:
            ghost_force += normal(loc =0, scale = self.stddev, size = self.ghost_Ndim) 
        
        return ghost_force

    
        

    def _shifted_pes(self, ads_pos, so_pos):
        shifted_pos = (ads_pos.reshape(-1, self.so_Ndim) + so_pos).flatten()
        return self.potential(shifted_pos)


    def get_Epot(self, positions):
        return 0.


    def __call__(self, positions):
        ads_pos, so_pos, ghost_pos = self._divide_positions(positions)
        ads_force = self._ads_force(ads_pos, so_pos)
        so_force = self._so_force(ads_pos, so_pos, ghost_pos)
        ghost_force = self._ghost_force(so_pos, ghost_pos)
        forces = np.concatenate((ads_force, so_force, ghost_force))
        return forces



    def get_so_Ndim(self):
        return self.so_Ndim
    
    def get_ghost_Ndim(self):
        return self.ghost_Ndim

    def get_system_Ndim(self):
        return self.system_Ndim

    def get_so_frequencies(self):
        return self.so_freqs.copy()

    def get_so_mass(self):
        return self.so_mass
    
    def get_ghost_frequencies(self):
        return self.ghost_freqs.copy()

    def get_ghost_mass(self):
        return self.ghost_mass

    def get_so_element(self):
        return self.so_element
