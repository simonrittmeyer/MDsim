# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from scipy.constants import physical_constants

import MDsimv3.units as units
from MDsimv3.systems import NormalmodeSystem
from MDsimv3.electronicfriction import Eta

from MDsimv3.forces import NumericalForces
from MDsimv3.glo import GLOForces


class GLOSystem(NormalmodeSystem):
    def __init__(self, 
                 atoms_eq, 
                 normalmodes, 
                 init_quanta, 
                 potential, 
                 eta_function, 
                 so_element,
                 so_freqs,
                 so_ghost_coupling,
                 ghost_freqs,
                 ghost_gamma,
                 temperature = 0,
                 time_step = 0.1,
                 stepwidth = 0.001,
                 convert = True):
    
        # this is just to properly initialize the parent class
        _forces = NumericalForces(potential, stepwidth = stepwidth)
        
        NormalmodeSystem.__init__(self,
                                  atoms_eq = atoms_eq,
                                  normalmodes = normalmodes,
                                  init_quanta = init_quanta,
                                  force_function = _forces,
                                  eta_function = eta_function,
                                  init_etas_and_forces = False)
        
        # at least friction on the ghost atom
        self.friction = True

        self.force_function = GLOForces(potential = potential,
                                        so_element = so_element,
                                        so_freqs = so_freqs,
                                        so_ghost_coupling = so_ghost_coupling,
                                        ghost_freqs = ghost_freqs,
                                        ghost_gamma = ghost_gamma,
                                        temperature = temperature,
                                        time_step = time_step,
                                        stepwidth = stepwidth,
                                        convert = convert)
        self.calc_forces = self.force_function

        # pretty silly io convenience... 
        self.interaction = self.force_function

        # hand some variables from the force function to the instance variables
        self.__dict__.update(self.force_function.hand_variables())

        # position and velocity vector of the SO and ghost particle
        so_pos = np.zeros((self.so_Ndim))
        so_vel = np.zeros((self.so_Ndim))
        
        ghost_pos = np.zeros((self.ghost_Ndim))
        ghost_vel = np.zeros((self.ghost_Ndim))
    
        # extend positions and velocity vector of the entire system
        self.positions = np.concatenate((self.positions, so_pos, ghost_pos))
        self.velocities = np.concatenate((self.velocities, so_vel, ghost_vel))
        
        # for reset purposes
        self.positions_init  = self.positions.copy()
        self.velocities_init = self.velocities.copy()

        # further extendnd masses and species
        self.masses = np.concatenate((self.masses, 
                                      [self.so_mass]*self.so_Ndim, 
                                      [self.ghost_mass] * self.ghost_Ndim))
        self.masses_red = np.concatenate((self.masses_red, [self.so_mass], [self.ghost_mass]))
        self.species = np.concatenate((self.species, 
                                       ['{} (SO)'.format(self.so_element)],
                                       ['ghost particle']))
        
        # properly initialize forces and etas, now with correct dimensions
        self._init_etas_and_forces()

        # lastly, extend the mask 
        self._mask = np.concatenate((self._mask, np.zeros((self.so_Ndim+self.ghost_Ndim), dtype = bool))) 
        
        # extend the Natoms counter
        self.Natoms += 2
        
        self.inititalized = True
    

    def calc_etas(self, positions):
        # this function has to be adjusted in order not to mess with all eta instances...
        ads_pos, so_pos, ghost_pos = self.force_function._divide_positions(positions)
        etas = self.eta_function(ads_pos)
        etas = np.concatenate((etas, np.zeros((self.so_Ndim)), self.ghost_eta*self.ghost_Ndim)) 
        return etas
    
    def __str__(self):
        name = 'GLOSystem (T = {}K)'.format(self.temperature)
        return name
    
    def get_so_Ndim(self):
        return self.so_Ndim
    
    def get_ghost_Ndim(self):
        return self.ghost_Ndim

    def get_system_Ndim(self):
        return self.system_Ndim

    def get_so_frequencies(self):
        return self.so_freqs.copy() * units.AU_TO_EV

    def get_so_mass(self):
        return self.so_mass
    
    def get_ghost_frequencies(self):
        return self.ghost_freqs.copy() * units.AU_TO_EV

    def get_ghost_mass(self):
        return self.ghost_mass

    def get_so_element(self):
        return self.so_element
