# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import numpy as np
import ase

class AtomsEq(object):
    """ 
    This is basically an atoms object, enriched with masks to filter fixed
    atoms. It is used to conventiently communicate equilibrium properties of
    a system, geared towards usage within the context of adsorbate
    vibrations.
    
    For this reason, three masks are created (in any case, True = available)
    
    * constraints_mask (Natoms x Ndim)
    * atoms_mask (Natoms x 1)
    * dim_mask (Ndim x 1)
    
    All original atoms properties will be filtered by these masks!
    Unfiltered properties can be accessed via the private class methods.

    NOTE:
    -----
    There is no support yet to restrict different degress of freedom for
    different atoms, i.e. restricted DIM are global! However, apart from that,
    you can still entirely fix atoms.

    """
   
    
    def __init__(self, atoms, read_constraints = True):
        self.atoms  = atoms
        self.Natoms = len(self.atoms)
        self.Ndim   = 3
      
        # number of free atoms, dof
        self.Nfreeatoms = self.Natoms
        self.Nfreedim   = self.Ndim

        # offset, if geometry has been optimized on numerical potential
        # pos(optimized) + offset = pos(non-optimized) 
        self.offset = None

        # set up constraints mask : 1 = free, 0 = fixed
        self.constraints_mask = np.ones((self.Natoms, self.Ndim), dtype = bool)
        
        self.atoms_mask = np.ones(self.Natoms, dtype = bool)
        self.dim_mask   = np.ones(self.Ndim, dtype = bool)
            
        # note, these are indices not masks
        self.free_atoms = range(self.Natoms)
        self.free_dim   = range(self.Ndim)
       
        # read constraints from the atoms object
        if read_constraints:
            self.read_atom_constraints()


    def __str__(self):
        name = 'AtomsEq instance wrapping around:\n\t{}'.format(self.atoms)
        return name

    # # # # # # # # # # # # # # # #
    # BUILD MASKS                 #
    # # # # # # # # # # # # # # # #     
    
    def update(self):
        """
        Update internal lists and numbers after having read constraints.
        """
        self.Nfreedim   = np.count_nonzero(self.dim_mask)
        self.Nfreeatoms = np.count_nonzero(self.atoms_mask)
        self.free_atoms = np.arange(self.Natoms)[self.atoms_mask]
        self.free_dim   = np.arange(self.Ndim)[self.dim_mask]
    
    
    def _set_atoms(self, atoms):
        """
        Replace the atoms object without losing information of the constraints
        """
        self.atoms = atoms

    
    def read_atom_constraints(self):
        """
        Read the constraints from the atoms object attached to this class.
        """
        if self.atoms.constraints == []:
            print 'no atom constraints found'
        else:
            for constraint in self.atoms.constraints:
                if isinstance(constraint, ase.constraints.FixAtoms):
                    idx = constraint.index
                    self.constraints_mask[idx,:] = 0
                    self.atoms_mask[idx] = 0
                elif isinstance(constraint, ase.constraints.FixedLine):
                    idx = constraint.a
                    self.constraints_mask[idx,:] = constraint.dir
                    self.dim_mask = constraint.dir.astype(bool)
                elif isinstance(constraint, ase.constraints.FixCartesian):
                    idx = constraint.a
                    self.constraints_mask[idx,:] = constraint.mask
                    self.dim_mask = constraint.mask.astype(bool)

            self.update()


    def set_free_atoms(self, idx):
        """
        Unfix atoms specified by idx. Note that this will overwrite any
        specific constraints on the dimensions whereever clashes occur.
        
        Note the all atoms not included by idx will be set fixed!
        
        Parameters
        ----------
        idx : integer or list of integers
            The index of the atom to the non-fixed.

        Returns
        -------
        <None>
        """
        
        # make sure we can use this thing as index mask
        idx = np.asarray(idx, dtype = int)

        # This is just the selector mask for the atoms
        atoms_mask = np.zeros(self.Natoms, dtype = bool)
        atoms_mask[idx] = True
        
        # remember: True = free in all masks
        self.constraints_mask[atoms_mask,:] = True
        # the other atoms are fixed
        self.constraints_mask[~atoms_mask,:] = False

        self.atoms_mask = atoms_mask                

        self.update()
    
    def set_fixed_atoms(self, idx):
        """
        Fix atoms specified by idx. Note that this will overwrite any
        specific constraints on the dimensions whereever clashes occur.

        Note the all atoms not included by idx will be set free!
        
        Parameters
        ----------
        idx : integer or list of integers
            The index of the atom to the fixed.

        Returns
        -------
        <None>
        """
        # make sure we can use this thing as index mask
        idx = np.asarray(idx, dtype = int)

        # This is just the selector mask for the atoms
        atoms_mask = np.zeros(self.Natoms, dtype = bool)
        atoms_mask[idx] = True
        
        # fix the respective atoms by setting the values to False 
        self.constraints_mask[atoms_mask,:] = False
        # the other atoms are free
        self.constraints_mask[~atoms_mask,:] = True

        # this is just all elements not captured by idx
        self.atoms_mask = ~atoms_mask
        
        self.update()


    def set_free_dim(self, idx):
        """
        Unfix dimensions specified by idx. Note that this will only affect
        those atoms, that are not fixed. 
        
        Parameters
        ----------
        idx : integer or list of integers
            The index of the dimension to be non-fixed.

        Returns
        -------
        <None>
        """
        
        # make sure we can use this thing as index mask
        idx = np.asarray(idx, dtype = int)

        # This is just the selector mask for the atoms
        dim_mask = np.zeros(self.Ndim, dtype = bool)
        dim_mask[idx] = True
        
        # remember: True = free in all masks
        self.constraints_mask[self.atoms_mask, dim_mask] = True
        # the other dim will be set fixed
        self.constraints_mask[self.atoms_mask,~dim_mask) = False
            
        self.dim_mask = dim_mask 
        
        self.update()

    def set_fixed_dim(self, idx):
        """
        Fix dimensions specified by idx. Note that this will only affect
        those atoms, that are not fixed. 
        
        Parameters
        ----------
        idx : integer or list of integers
            The index of the dimension to be fixed.

        Returns
        -------
        <None>
        """
        
        # make sure we can use this thing as index mask
        idx = np.asarray(idx, dtype = int)

        # This is just the selector mask for the atoms
        dim_mask = np.zeros(self.Ndim, dtype = bool)
        dim_mask[idx] = True
        
        # remember: False = fixed in all masks
        self.constraints_mask[self.atoms_mask, dim_mask] = False
        # the other dim will be set free
        self.constraints_mask[self.atoms_mask,~dim_mask) = True
            
        self.dim_mask = ~dim_mask 
        
        self.update()

    def clear_masks(self):
        self.constraints_mask[:,:] = True
        self.atoms_mask[:]         = True
        self.dim_mask[:]           = True
        
        self.free_atoms = range(self.Natoms)
        self.free_dim   = range(self.Ndim)

        self.update()

    # # # # # # # # # # # # # # # #
    # GET MASKS                   #
    # # # # # # # # # # # # # # # #   

    def get_constraints_mask(self):
        return self.constraints_mask
    
    def get_atoms_mask(self):
        return self.atoms_mask
        
    def get_dim_mask(self):
        return self.dim_mask
    
    def get_free_atoms(self):
        return self.free_atoms

    def get_free_dim(self):
        return self.free_dim
    
    # # # # # # # # # # # # # # # #
    # MASKED/FILTERED ATTRIBUTES  #
    # # # # # # # # # # # # # # # #
    
    def get_masses(self):
        """ 
        Return only the masses of the free atoms
        """
        return self.atoms.get_masses()[self.atoms_mask]
    

    def get_positions_eq(self):
        """
        Return only the unconstrained equilibrium positions
        """
        pos = self.atoms.get_positions()[self.constraints_mask]
        
        return pos.reshape(self.Nfreeatoms, self.Nfreedim)
    
    def get_species(self):
        """
        Return only the unconstrained equilibrium positions
        """
        return np.array(self.atoms.get_chemical_symbols())[self.atoms_mask]

    # # # # # # # # # # # # # # # #
    # UNFILTERED ATTRIBUTES       #
    # # # # # # # # # # # # # # # #
    
    def _get_masses(self):
        return self.atoms.get_masses()
        
    def _get_positions_eq(self):
        return self.atoms.get_positions()
    
    def _get_species(self):
        return self.atoms.get_chemical_symbols()
    
    
    # # # # # # # # # # # # # # # #
    # GET ATOMS OBJECTS           #
    # # # # # # # # # # # # # # # # 
    
    def get_atoms_obj(self):
        atoms = self.atoms.copy()
        # delete any constraints
        atoms.constraints = None
        return atoms[self.atoms_mask]
        
    def get_full_atoms_obj(self):
        return self.atoms.copy()
      
      
if __name__ == '__main__':
    a = ase.Atoms('C10')
    aa = AtomsEq(a)
    aa.set_fixed_atoms(1)
    print aa.get_free_atoms()
    aa.set_free_atoms(1)
    print aa.get_free_atoms()
