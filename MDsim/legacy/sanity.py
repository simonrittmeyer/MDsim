# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import glob
import os

def check_completeness(args):
    print 'Checking for input completeness'
    complete = True
    
    required = ['system', 
                'simulation_time',
                'potential',
                'ndim'
                ]
    inc_msg = 'Incomplete input!'
    for r in required:
        if args[r] is None:
            inc_msg +="\n\t`{}` is not specified".format(r)
            complete = False
    
    if not complete:
        print inc_msg
        raise SystemExit(1)
    else:
        print '\tInput complete.'


def check_consistency(args):
    print 'Checking for input consistency'
    
    consistent = True
    
    err_messages = []

    # we need certain properties for different systems
    if args['system'] in ['genericsystem']:
        required = ['positions', 'species', 'masses', 'velocities']
        err_msg = "\nInconsistent input!\n\t`system` = `{}` but:".format(args['system'])
        for r in required:
            if args[r] is None:
                err_msg += "\n\t`{}` = None".format(r)
                consistent = False
        if not consistent:
            err_messages.append(err_msg)
        consistent = True
    

    if args['system'] in ['normalmodesystem', 'glosystem', 'sosystem']:
        required = ['initial_quanta','potfile_usecols']
        err_msg = "\nInconsistent input!\n\t`system` = `{}` but:".format(args['system'])
        for r in required:
            if args[r] is None:
                err_msg += "\n\t`{}` = None".format(r)
                consistent = False
        if not consistent:
            err_messages.append(err_msg)
        consistent = True
    
    elif args['system'] == 'sosystem':
        err_msg = "\nInconsistent input!\n\t`system` = `{}` but:".format(args['system'])
        if args['surface_element'] is None:
            err_msg += "\n\tno `surface_element` provided"
            consistent = False
        if args['surface_frequencies'] is None:
            err_msg += "\n\tno `surface_frequencies` provided"
            consistent = False
        if not consistent:
            err_messages.append(err_msg)
        consistent = True
    
    elif args['system'] == 'glosystem':
        err_msg = "\nInconsistent input!\n\t`system` = `{}` but:".format(args['system'])
        if args['glo_element'] is None:
            err_msg += "\n\tno `glo_element` provided"
            consistent = False
        if args['glo_so_freqs'] is None:
            err_msg += "\n\tno `glo_so_freqs` provided"
            consistent = False
        if not consistent:
            err_messages.append(err_msg)
        consistent = True
    
    if args['friction'] != 'none':
        err_msg = "\nInconsistent input!\n\t`friction` = `{}` but:".format(args['friction'])
        if args['etafile_usecols'] is None:
            err_msg += "\n\tno `etafile_usecols` = None"
    
    elif args['system'] == 'onebodysystem':
        err_msg = "\nInconsistent input!\n\t`system` = `{}` but:".format(args['system'])
        required = ['masses', 'initial_quanta', 'potfile_usecols']
        for r in required:
            if args[r] is None:
                err_msg += "\n\t`{}` = None".format(r)
                consistent = False

        if args['ndim'] != 1:
            err_msg += "\n\t`ndim` != 1".format(r)
            consistent = False

        if args['friction'] != 'none':
            err_msg += "\n\t`friction` = `{}`".format(args['friction'])
            err_msg += "\n\t(Support for electronic friction not (yet) implemented for this system type.)"
            consistent = False
        if args['normalmodes'] != 'ase':
            err_msg+= "\n\t`normalmodes` = `{}`".format(args['normalmodes'])
            err_msg += "\n\t(Support for this flavor of normalmode analysis not (yet) implemented for this system type.)"
        if not consistent:
            err_messages.append(err_msg)
        consistent = True
    
    if not err_messages:
        print '\tInput consistent.'
    else:
        # remove leading '\n' character
        print ''.join(err_messages).lstrip()
        raise SystemExit(1)


def _find_file(key, args, found_all):
    if args[key] is None:
        try:
            f = glob.glob(os.path.join(args['datapath'],
                                       '*.{}'.format(key.replace('file',''))))[0]
            args[key] = f
            print '\tFound {} : {}'.format(key, f)
        except IndexError:
            print '\tNo {} found!'.format(key)
            found_all = False
    else:
        if os.path.exists(args[key]):
            print '\tValid {} location specified : {} '.format(key, args[key])
        else:
            print '\tInvalid {} location specified : {}'.format(key, args[key])
            found_all = False
    
    return args, found_all


def find_files(args):
    print 'Detecting files and/or validating given locations'
    found_all = True
    
    if not args['system'] in ['onebodysystem']:
        args, found_all = _find_file('cellfile', args, found_all)

    if args['normalmodes'] == 'ase' or args['potential'] == 'interpolated':
        args, found_all = _find_file('potfile', args, found_all)

    if args['friction'] == 'iaa' or args['friction'] == 'aim':
        args, found_all = _find_file('etafile', args, found_all)
    
    if args['normalmodes'] == 'castep':
        args, found_all = _find_file('phononfile', args, found_all)

    if not found_all:
        raise SystemExit(1)
    else:
        return args
