# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsimv3.tools                            import check_kwargs 
from MDsimv3.customization.calculator         import CustomCalculator

# -----------------------------------------------------------------------------
# CALCULATOR
# -----------------------------------------------------------------------------


def create_customCalculator(atomsEq, potential_obj, **kwargs):
    param = {'forces'     : 'numerical',
             'forces_stepwidth' : 0.01}
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    print '\tcreating CustomCalculator'
    
    calculator = CustomCalculator(atomsEq,
                                  potential_obj,
                                  forces = param['forces'],
                                  forces_stepwidth = param['forces_stepwidth'])
    return calculator


