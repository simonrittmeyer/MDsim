# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units
from MDsim.interpolation.bivariatespline.internalbivariatespline import InternalBivariateSpline

class InternalBivariateFrictionCoefficient(InternalBivariateSpline):
    """
    Class that handles the bivariate spline interpolation for each ATOMIC
    friction coefficient.

    Note that within the LDFA formalism, the atomic
    friction coefficient is a function of an input density only. Yet, for
    efficiency reasons, we rather map this coeffient on a real-space grid to
    avoid unnecessary calls pos -> density -> eta. This is a very convenient
    way to do for small systems.

    However, as a result, also the IAA coefficients should be mapped over the
    full grid, ie. there is *no formal difference between AIM and IAA flavor*.

    Moreover, do not be confused by the fact that we use
    "InternalBivariatSpline" as a basis class here to evaluate atomic friction
    coefficients. The way we call this routine is independent of the fact that
    LDFA always gives us atomic friction coefficients that create a diagonal
    matrix in cartesian space. Transforming the latter into any other space
    really brings you into trouble in defining a new integrating scheme.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    etas : (Npoints) array
        friction coefficients corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """

    def __init__(self, points, etas,
                 function = 'RectBivariateSpline',
                 kind = 'cubic',
                 data_origin = 'None',
                 convert = True,
                 _bounds_check = True):

        if convert:
            points   *= units.ANGSTROM_TO_AU
            etas *= units.AMU_PER_FS_TO_AU

        InternalBivariateSpline.__init__(self, points = points,
                                         values = etas,
                                         function = function,
                                         kind = kind,
                                         data_origin = data_origin,
                                         _bounds_check = _bounds_check)

        self._name = "InternalBivariateFrictionCoefficient"
        self._name +="\n\t(interpolated atomic friction coefficient)"

        # this is a bivariate interpolation, hence the internal coordinates need to
        # be a flat 2D array
        self._internals = np.empty(2)

    def transform_coordinates(self, positions):
        """
        This is the transformation from the (Natoms x Ndim) positions array to
        a flat internal coordinates array. Needs to be implemented by subclasses.
        """
        #raise NotImplementedError('Coordinate transformation has to be implemented by subclass')
        # most simple way: ravel the thing
        return positions.ravel()

    def __call__(self, positions):
        return self.get_value(self.transform_coordinates(positions))
