# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import os
import copy
import re

# Arguments that can be used
# --------------------------------------------
# You can simply add new `Option` instances to the `options` dictionary.
# Make sure to understand the meaning of the respective keys in this template!
# Once an Option is added, anything else will be taken care of, including the
# creation of a command line parser.

class Option(object):
    """
    Template for any option processed by the code. Basically this is nothing
    but a small wrapper around a dictionary. It supports the __getitem__() and
    __setitem__() methods and thus acts like a dictionary to the outside.

    The respective keys, passed as keyword arguments upon initialization are:

    name : string, optional (default = '')
        The name of the option, ie. the key via which it will be identified. It
        must consist of lower case letters only! No numbers, no special
        characters, whitespaces,... only exception is the underscore(_). This
        means, the <key> has to match the regex pattern r'[a-z_]+' and is
        checked for it. If it matches '[a-zA-Z_]+' it will be converted,
        otherwise an error is raised. Note that also the default key raises an
        error (for good reason).

    category : string, optional (default = 'generic')
        Category of this option. This helps to have some form of ordering at
        least. "generic" options will always be considered.

    val : any type, optional (default = None)
        The default value of this option. If you require user input or if this
        variable is not essential, set it to `None`.

    fmt : string, optional (default = `str`)
        Internal format of the keyword. One of {`str`, `float`, `int`,
        `bool`} or a `list_*` with the respective format appended. Required to
        properly parse user input.

    choices : list, optional (default = [])
        If your option has a limited amount of choices for val, add the
        respective list here. If the list evaluates to False (ie. empty list or
        False), any `val` is possible. Note that the choices will be converted
        to strings in order to allow comparison already at the file i/o level.

    multvals : bool, optional (default = False)
        If your can have more than one `val`, set True. Of course, `val` must
        then be a list of something.

    case_sensitive : bool, optional (default = False)
        In case `val` is a string. Case sensitivity is important e.g. for
        filenames.

    info : string, optional (default = 'no info available')
        Short user info. Will be used in the creation of the argparser and may
        probably help at some time to create an internal help routine.

    active : bool, optional (default = True)
        Whether option is actively supported within the code. Set False if
        disabled.

    alias : list of strings, optional (default = [])
        Alternative names for the option. `name` will be added in any case.
    """
    defaults = {'name'           : '',
                'category'       : 'generic',
                'val'            : None,
                'fmt'            : 'str',
                'choices'        : [],
                'multvals'       : [],
                'case_sensitive' : False,
                'info'           : 'no info available',
                'active'         : True,
                'alias'          : []}

    def __init__(self,**kwargs):
        # check the name first
        key = self._check_name(kwargs['name'])
        kwargs.update({'name' : key})

        key = self._check_name(kwargs['category'])
        kwargs.update({'category' : key})

        # same with the alias
        _alias = []
        if 'alias' in kwargs:
            for a in kwargs['alias']:
                _alias.append(self._check_name(a))

        # add the name to the alias...
        if not key in _alias:
            _alias.append(key)
        kwargs.update({'alias' : _alias})

        self.option = copy.deepcopy(self.defaults)
        self.option.update(kwargs)

        # internally generated help string
        help_str = self.option['name'] + '\n\n'
        help_str += self.option['info'] + '\n'
        help_str += '\nDetails'
        help_str += '\n-------'
        help_str += '\n* category        : {}'.format(self.option['category'])
        help_str += '\n* format          : {}'.format(self.option['fmt'])
        help_str += '\n* case sensitive  : {}'.format(self.option['case_sensitive'])
        help_str += '\n* multiple values : {}'.format(self.option['multvals'])
        choices = self.option['choices']

        if choices:
            # convert to strings for comparison
            self.option['choices'] = [str(c) for c in choices]

            help_str += '\nValid choices'.format(self.option['multvals'])
            help_str += '\n-------------'
            for c in choices:
                help_str += '\n\t{}'.format(c)

        self.option['help_str'] = help_str

    def _check_name(self, key):
        if re.match(r'[a-z_]+', key):
            return key
        elif re.match(r'[a-zA-Z_]+', key):
            return key.lower()
        else:
            raise NameError('Invalid option name `{}` does not match r"[a-z_]"'.format(key))

    def __getitem__(self, key):
        return self.option[key]

    def __setitem__(self, key, value):
        self.option[key] = value

    def __str__(self):
        return 'Option object'

    def help(self):
        print self.option['help_str']


#
# Create the options dictionary which holds all of the Option instances
#

# store everything in a helper list first
_options = [# crucial keywords
            Option(name = 'system',
                   category = 'generic',
                   val = None,
                   fmt = 'str',
                   choices = ['genericsystem', 'normalmodesystem', 'sosystem', 'glosystem', 'onebodysystem'],
                   alias = ['sys'],
                   info = 'System type on which the simulation is based.'),

            Option(name = 'cellfile',
                   category = 'vibrations',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   info = 'CASTEP cellfile specifying the equilibrium geometry.'),

            Option(name = 'initial_quanta',
                   category = 'vibrations',
                   val = None,
                   fmt = 'list_float',
                   alias = ['init_quanta', 'q_init', 'qinit', 'init_q', 'initial_q'],
                   info = 'Initially excited quanta within each normalmode.'),

            Option(name = 'simulation_time',
                   category = 'generic',
                   val = None,
                   fmt = 'float',
                   alias = ['simulationtime', 'tend', 't_end', 't'],
                   info = 'Duration of the simulation in fs.'),

            Option(name = 'ndim',
                   category = 'vibrations',
                   val = None,
                   fmt = 'int',
                   # the minus one will be for full dimensionality at some point
                   choices = [1,2,-1],
                   alias = ['dimensions'],
                   info = 'Dimensionality of the dynamical (!) system.'),

            Option(name = 'transformation_flavor',
                   category = 'vibrations',
                   val = 'perp',
                   fmt = 'str',
                   choices = ['perp', 'para'],
                   alias = ['transform', 'trafos'],
                   info = 'Flavor of coordinate transformations applied.'),

            Option(name = 'potential',
                   val = None,
                   fmt = 'str',
                   choices = ['interpolated', 'harmonic'],
                   alias = ['pot'],
                   info = 'Potential type that is used to run the MD.'),

            Option(name = 'dryrun',
                   val = False,
                   fmt = 'bool',
                   alias = ['dry_run'],
                   info = 'Dryrun. Everything besides from file handlers is set up an initialized, but there is no actual simulation.'),

            # integrator related keywords
            Option(name = 'integrator',
                   val = 'liouville',
                   fmt = 'str',
                   choices = ['liouville', 'velocity_verlet'],
                   info = 'Integration scheme used.'),

            Option(name = 'timestep',
                   val = 0.1,
                   fmt = 'float',
                   alias = ['dt', 'time_step'],
                   info = 'Integration time step in fs'),


            # potential-related keywords (2D only!)
            Option(name = 'potfile',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['potentialfile', 'pot_file', 'potential_file'],
                   info = 'Path to the file storing information on the potential.'),

            Option(name = 'potfile_usecols',
                   val = None,
                   fmt = 'list_int',
                   alias = ['pot_usecols', 'potfile_usecol', 'pot_usecol', 'potential_usecol', 'potential_usecols'],
                   info = 'Which columns to be used from the potfile.'),


            # friction-related keywords
            Option(name = 'friction',
                   val = 'none',
                   fmt = 'str',
                   choices = ['none', 'iaa', 'aim'],
                   alias = ['friction_model', 'eta'],
                   info = 'Friction model employed'),

            Option(name = 'etafile',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['eta_file', 'etas_file', 'etasfile', 'friction_file', 'friction_files'],
                   info = 'Path to the file storing information on the friction coefficient(s) to be interpolated.'),

            Option(name = 'etafile_usecols',
                   val = None,
                   fmt = 'list_int',
                   alias = ['eta_usecols', 'etafile_usecol', 'eta_usecol', 'friction_usecol', 'friction_usecols'],
                   info = 'Which columns to be used from the etafile.'),

            Option(name = 'etascale',
                   val = None,
                   fmt = 'float',
                   active = False,
                   alias = ['eta_scale'],
                   info = 'Global scaling factor multiplied on the friction coefficients.'),

            Option(name = 'cubefile',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   active = False,
                   alias = ['cube_file'],
                   info = 'Path to the cubefile specifying the densities. In case this is specified, friction coefficients will be internally calculated from these densities.'),

            # forces-related keywords
            Option(name = 'forces',
                   val = 'numerical',
                   fmt = 'str',
                   choices = ['numerical', 'analytical'],
                   alias = ['force'],
                   info = 'How to evaluate forces. Either analytically or numerically from central differences'),

            Option(name = 'forces_stepwidth',
                   val = 0.01,
                   fmt = 'float',
                   alias = ['forces_delta', 'forces_stepsize'],
                   info = 'Displacement-stepwidth for the numerical evaluation of forces (in Angstrom).'),


            # normalmode-related keywords
            Option(name = 'normalmodes',
                   val = 'ase',
                   fmt = 'str',
                   choices = ['ase', 'castep'],
                   alias = ['normalmode'],
                   info = 'Which kind of normalmode analysis to use. `ase` runs an analysis internally on the interpolated potential, while `castep` requires input from a phononfile.'),


            Option(name = 'phononfile',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['phonon_file'],
                   info = 'Path to the CASTEP phononfile, if required.'),

            Option(name = 'normalmode_names',
                   val = None,
                   fmt = 'list_str',
                   case_sensitive = True,
                   alias = ['mode_names', 'normalmodes_names', 'modenames', 'nmnames', 'nm_names'],
                   info = 'Names of the normalmodes.'),

            Option(name = 'normalmode_delta',
                   val = 0.01,
                   fmt = 'float',
                   alias = ['nm_delta', 'normalmodes_delta', 'normalmodes_stepsize', 'normalmode_stepsize', 'normalmode_stepwidth', 'normalmodes_stepwidth'],
                   info = 'Stepwidth in the evaluation of the Hessian in case an `ase` normal mode analysis is requested. This option has to be handled with care and in particular in accordance with `forces_stepwidth` if `numerical` forces are used.'),


            Option(name = 'geo_opt_fmax',
                   val = 1e-6,
                   fmt = 'float',
                   alias = ['max_force', 'geo_fmax', 'fmax'],
                   info = 'Convergence tolerance for the internal BFGS optimizer that is employed to optimze the atoms object on the interpolated potential.'),


            # IO-related keywords
            Option(name = 'seed',
                   val = 'MDsim',
                   fmt = 'str',
                   case_sensitive = True,
                   info = 'Common seed for all output files.'),

            Option(name = 'datapath',
                   val = os.path.join(os.getcwd(), 'data'),
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['data_path', 'data_dir', 'datadir'],
                   info = 'Path to the directory in which all input files are expected to be found. By default <cwd>/data.'),

            Option(name = 'outpath',
                   val = os.path.join(os.getcwd(), 'output'),
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['out_path', 'resultpath', 'result_dir', 'out_dir', 'outdir'],
                   info = 'Path to the directory for all output files. By default <cwd>/output.'),

            Option(name = 'iostep',
                   val = 10,
                   fmt = 'int',
                   alias = ['io_step'],
                   info = 'Multiple of integration steps at which there is IO to the outfiles.'),

            Option(name = 'observables',
                   val = ['positions', 'energy', 'forces'],
                   fmt = 'list_str',
                   choices = [
                              # this keyword has been removed
                              #'onebody',
                              'casteptkv2',
                              'positions',
                              'velocities',
                              'forces',
                              'etas',
                              'energy',
                              'kinetic_energy',
                              'energies_normalmodes',
                              'positions_normalmodes',
                              'velocities_normalmodes',
                              # these keywords have been removed
                              #'energy_decomposition_normalmodes',
                              #'kinetic_energies_normalmodes',
                              #'kinetic_energies_so_model',
                              #'kinetic_energies_glo_model',
                              'gamma_matrix'
                              ],
                   multvals = True,
                   alias = ['observable', 'obs', 'output'],
                   info = 'Observables for which there will be an output file written'),

            Option(name = 'gzip',
                   val = True,
                   fmt = 'bool',
                   alias = ['zip'],
                   info = 'Gzip the output written by the file handlers.'),

            Option(name = 'movie',
                   val = None,
                   fmt = 'str',
                   choices = ['xyz', 'ase'],
                   alias = ['animation'],
                   info = 'Whether trajectory is written zu an .xyz file to visualize it. There is some rudimentary support for ase trajectories. Yet, these are subject to change in ASE trunk currently anyway, so better pass on this possibility.'),


            # Surface oscillator-related keywords
            Option(name = 'so_element',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['so_symbol'],
                   info = 'Chemical symbol of the SO Element. Used to calculate the mass.'),

            Option(name = 'so_frequencies',
                   val = None,
                   fmt = 'list_float',
                   alias = ['so_freqs', 'so_omegas', 'so_freq', 'so_frequency', 'so_omega'],
                   info = 'SO frequencies along each DOF. If only float is passed, frequencsy-isotropy will be assumed.'),

            Option(name = 'so_temperature',
                   val = 0.,
                   fmt = 'float',
                   alias = ['so_temp'],
                   info = 'Surface temperature in the SO model. Used to initialize the velocity of the SO'),


            # GLO-related keywords
            Option(name = 'glo_temperature',
                   val = 0.,
                   fmt = 'float',
                   alias = ['so_temp'],
                   info = 'Surface temperature in the GLO model. Controls the fluctuating forces and initial velocities.'),

            Option(name = 'glo_so_frequencies',
                   val = None,
                   fmt = 'list_float',
                   alias = ['glo_so_freqs', 'glo_so_omegas', 'glo_so_freq', 'glo_so_frequency'],
                   info = 'SO frequencies along each DOF within the GLO model. If only float is passed, frequency-isotropy will be assumed.'),

            Option(name = 'glo_so_element',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['glo_so_symbol'],
                   info = 'Chemical symbol of the SO Element in the GLO model. Used to calculate the mass.'),

            Option(name = 'glo_so_ghost_coupling',
                   val = None,
                   fmt = 'list_float',
                   info = 'Coupling between SO and ghost in the GLO model. Isotropic if only one number is given'),

            Option(name = 'glo_ghost_element',
                   val = None,
                   fmt = 'str',
                   case_sensitive = True,
                   alias = ['glo_ghost_symbol'],
                   info = 'Chemical symbol of the GLO_ghost Element. Used to calculate the mass.'),

            Option(name = 'glo_ghost_frequencies',
                   val = None,
                   fmt ='list_float',
                   alias = ['glo_ghost_freqs', 'glo_ghost_omegas', 'glo_ghost_freq', 'glo_ghost_omega', 'glo_ghost_frequency'],
                   info = 'Ghost frequencies along each DOF within the GLO model. Of only float is passed, frequency-isotropy will be assumed.'),

            Option(name = 'glo_ghost_gamma',
                   val = None,
                   fmt = 'float',
                   alias = ['glo_ghost_friction', 'glo_gamma', 'glo_friction'],
                   info = 'Friction on the ghost atom in the GLO model'),

            # options for the generic system
            Option(name = 'positions',
                   val = None,
                   fmt = 'list_float',
                   alias = ['pos', 'position', 'q'],
                   info = 'Positions in the generic system.'),

            Option(name = 'velocities',
                   val = None,
                   fmt = 'list_float',
                   alias = ['vel', 'velocity', 'qdot'],
                   info = 'Velocities in the generic system.'),

            Option(name = 'masses',
                   val = None,
                   fmt = 'list_float',
                   alias = ['mass'],
                   info = 'Masses in the generic system.'),

            Option(name = 'species',
                   val = None,
                   fmt = 'list_str',
                   case_sensitive = True,
                   alias = ['spec', 'element', 'atom', 'atoms', 'elements', 'symbol', 'symbols'],
                   info = 'Species in the generic system.'),

            Option(name = 'debug',
                   val = False,
                   fmt = 'bool',
                   case_sensitive = False,
                   info = 'Write more information to outfiles.'),

            # A fake, inactive infile Option for the command line parser
            # This allows us to use the same routines to check for completeness
            # etc. for input file and command line args.
            Option(name = 'infile',
                   val = 'MDsim.in',
                   fmt = 'str',
                   active = False,
                   case_sensitive = True,
                   info = 'Input file to control MDsim settings')
            ]

# create the options dictionary
options = dict()
for o in _options:
    options[o['name']] = o

# dictionary with default arguments (for convenience only)
args = dict()
for o in _options:
    args[o['name']] = o['val']

