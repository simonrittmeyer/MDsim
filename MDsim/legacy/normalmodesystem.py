# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/python

import numpy as np

# unit conversion
from MDsim import units

# internal harmonic potential
from MDsim.potentials import HarmonicPotential
from MDsim.systems import System

class NormalmodeSystem(System):
    """
    Subclass derived from System. This class is intended to be used when
    treating vibrating systems for which there is a normal modes analysis
    available.
    
    Initialization
    --------------
    atoms_eq         : AtomsEq instance
                       Instance providing all information on the equilibrium
                       geometry of the system, as well as species and masses.
    
    normalmodes      : Normalmodes instance
                       Instance providing all necessary coordinate
                       transformations and transformation matrices.

    init_quanta      : list/array/tuple
                       (Natoms * Ndof x 1) array of initial (vibrational) quanta
                       that should be assigned to individual normal modes in
                       terms of kinetic energy. 
                       
                       |DISABLED
                       ---------
                       |Non-zero elements will be added
                       |0.5 to account for 0K vibrational energy.

                       The corresponding vibrational energies are taken from the
                       Normalmodes instance.

    force_function   : Forces instance/callable object
                       Directly passed to parent class, see docstring there.

    eta_function     : EtaMD instance/callable object
                       Directly passed to parent class, see docstring there.

    init_etas_and_forces : Boolean (default = True)
                       Directly passed to parent class, see docstring there.
    Notes
    -----
    Everything is still handled in Carthesian coordinates, hence also the
    force/eta function are not supposed to expect any input in normal mode or
    mass weighted basis. Transformations into normal mode space are only done
    for I/O reasons.
    """
    
    name = 'NormalModeSystem'
     
    def __init__(self, atoms_eq,
                       normalmodes,
                       init_quanta,
                       force_function, 
                       eta_function = None,
                       init_etas_and_forces = True
                       ):
        
        self.atoms_eq        = atoms_eq
        self.normalmodes_obj = normalmodes
        
        # get mode names (for IO convenience)
        self.mode_names = self.normalmodes_obj.get_mode_names()
        
        # --------------------------------
        # start to initialize parent class
        # --------------------------------

        masses     = atoms_eq.get_masses()
        positions  = atoms_eq.get_positions_eq()
        species    = atoms_eq.get_species()
        

        # assign initial energy --> initial velocities
        self.hnus  = self.normalmodes_obj.get_energies() * units.EV_TO_AU
       
        init_quanta = np.array(init_quanta, dtype = float)
        self.init_quanta = init_quanta.copy()
        
        # initial kinetic energy per mode in a.u.
        E_inits     = init_quanta*self.hnus
        
        # velocities in normal mode basis (in a.u.)
        qdot = np.sqrt(2.* E_inits)
        
        # transform to carthesian basis and convert to A/fs
        # the latter conversion is needed for consistent
        # initialization of parent class
        velocities  = self.normalmodes_obj.nm_to_car(qdot, au = True)
        velocities *= units.AU_TO_ANGSTROM_PER_FS
        

        # correct shape of velocity
        velocities.resize(positions.shape)

        # initialize parent class
        System.__init__(self,
                        positions            = positions, 
                        velocities           = velocities, 
                        masses               = masses, 
                        species              = species,  
                        force_function       = force_function,
                        eta_function         = eta_function,
                        convert              = True,
                        init_etas_and_forces = init_etas_and_forces)

        # to gain a projection of the potential energy also in the case of
        # non-harmonic potentials we use an internal harmonic potential
        if not hasattr(self.interaction, 'get_Epots_normalmodes'):
            self._harmonic_potential = HarmonicPotential(normalmodes)

        # we create a mask to disinguish ``real'' DOF from articial ones like
        # i.e. the surface oscillator coordinate
        # This mask is useless if we do not have ghost coordinates, but we will
        # implement it anyways, such that further subclasses can make use of it.
        self._mask = np.ones(len(positions), dtype = bool)
        
        
    
    # ------------------------------------------------------------------------
    # Observables in normalmode basis
    # NOTE: the transformations all use methods from the normalmodes instance!
    # ------------------------------------------------------------------------

    def get_Epots_normalmodes(self):
        # if the interaction potential offers a decomposition, use it!
        if hasattr(self.interaction, 'get_Epots_normalmodes'):
            Epots = self.interaction.get_Epots_normalmodes(self.positions[self._mask]) 
            return Epots * units.AU_TO_EV
        else:
            Epots = self._harmonic_potential.get_Epots_normalmodes(self.positions[self._mask]) 
            return Epots * units.AU_TO_EV
    
    def get_Ekins_normalmodes(self):
        # transform velocities to normalmodes space
        qdot = self.normalmodes_obj.car_to_nm(self.velocities[self._mask], au = True)
        Ekins = 0.5 * qdot**2
        return Ekins * units.AU_TO_EV

    def get_Etots_normalmodes(self):
        # no need to convert
        return self.get_Ekins_normalmodes() + self.get_Epots_normalmodes()

    def get_positions_normalmodes(self):
        q = self.normalmodes_obj.pos_to_nm(self.positions[self._mask], au = True)
        return q * units.AU_TO_ANGSTROM * np.sqrt(units.AU_TO_AMU)
    
    def get_velocities_normalmodes(self):
        qdot = self.normalmodes_obj.car_to_nm(self.velocities[self._mask], au = True)
        return qdot * units.AU_TO_ANGSTROM_PER_FS * np.sqrt(units.AU_TO_AMU)
    
    # ----------------------
    # vibrational properties
    # ----------------------

    def get_init_quanta(self):
        return self.init_quanta.copy()
    
    def get_hnus(self):
        return np.array(self.hnus) * units.AU_TO_EV
        
    def get_frequencies(self):
        return self.get_hnus() * units.EV_TO_KAYSER
        
    def get_mode_names(self):
        return self.normalmodes_obj.get_mode_names()

    def get_gamma_matrix(self):
        return self.normalmodes_obj.get_gamma_matrix(self.etas)
    
    # --------------
    # IO convenience
    # --------------

    def get_all_atoms(self):
        return self.normalmodes_obj.pos_to_atoms(self.positions[self._mask], au = True)

