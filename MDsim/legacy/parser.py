# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import difflib
import re
import argparse
import numpy as np
from distutils.util import strtobool

from MDsim.options import options

def _normalize_key(key, pattern = r'[\s\-\.]+'):
    """
    Function that normalizes a key by removing all whitespaces, dots (.) and
    hyphens (-), and subsequently converting everything to lower case. This
    should allow for reliable comparison.

    Parameters
    ----------
    key : string
        Key to be normalized.

    pattern : raw-string, optional (default : r'[\s\-\.]+')
        Regex pattern that specifies every character that is to be deleted
        from the original string.

    Returns
    -------
    Normalized key.
    """
    return re.sub(pattern, '', key).lower()


def _read_file(inputfile,
               list_split_pattern = r'[\s,;]+'):
    """
    Function that reads an input file.

    Parameters
    ----------
    inputfile : string
        Path specifying the inputfile location.


    list_split_pattern: (raw) string, optional (default = r'[\s,;]+')
        Regular expression specifies the split pattern that separates multiple
        values.

    Returns
    -------
    Dictionary with key-value pairs from the file that ae separated with a ":"
    or "="
    """
    pattern = r'(.+)[:=](.+)'
    comment = '#'

    file_content = dict()

    with open(inputfile, 'r') as f:
        for line in f:
            # ignore commented lines
            if line.startswith(comment):
                continue
            # ignore inline comments

            line = line.split(comment)[0]

            match_obj = re.match(pattern, line)

            if match_obj:
                key, value = map(lambda x: x.strip().strip("'").strip('"'), match_obj.groups())

                file_content[key] = value

    return file_content


def parse_inputfile(inputfile,
                    # The options as defined in options.py
                   options = options,
                   list_split_pattern = r'[\s,;]+'):
    """
    Function that parses an input file.

    Parameters
    ----------
    inputfile : string
        Path specifying the inputfile location.

    options : dictionary of Option objects, optional (default = options from
              options.py)
        Dictionary defining all options. Basically there is no reason to change
        the default value.

    list_split_pattern: (raw) string, optional (default = r'[\s,;]+')
        Regular expression specifies the split pattern that separates multiple
        values.

    Returns
    -------
    Filtered and checked (for format and choices only) dictionary with settings
    extracted from the inputfile. For sanity checks use the tools/sanity module.
    """


    print 'Parsing input file:'
    print '\t{}'.format(inputfile)

    raw_settings = _read_file(filename = fielname,
                              list_split_pattern = list_split_pattern)


    # check for formal correctness
    user_settings = check_settings(raw_settings, options, list_split_pattern)

    print 'Recognized the following options from the input file:'

    template = '\t{0:<%ds} : {1:s}'%max([len(i) for i in user_settings.keys()])
    for k in sorted(user_settings.keys()):
        print template.format(k, str(user_settings[k]))

    return user_settings


def create_argparser(options = options):
    """
    Helper function that creates a command line parser based on the options
    defined in the `options` dictionary.

    There is no type conversion nor a choices check from the command line
    parser. All of this is done in den custom tools/parser.py module.
    """
    parser = argparse.ArgumentParser(description='MDsim : MD simulations including electronic friction.')
    for name, option in options.iteritems():
        # inactive options are not supported on command line
        if not option['active']:
            continue
        else:
            parser.add_argument('--{}'.format(name), type = str,
                                default = argparse.SUPPRESS,
                                help = option['info'])

    # infile is not a real internal option
    parser.add_argument('-i', '--infile', type = str,
                        default = argparse.SUPPRESS,
                        help = 'Infile specifying further options (same keywords as from commandline)')
    return parser


def parse_cmdline(options = options, list_split_pattern = r'[\s,;]+'):
    """
    Function that parses command line arguments.

    Parameters
    ----------
    options : dictionary of Option objects, optional (default = options from
              options.py)
        Dictionary defining all options. Basically there is no reason to change
        the default value.

    list_split_pattern: (raw) string, optional (default = r'[\s,;]+')
        Regular expression specifies the split pattern that separates multiple
        values.

    Returns
    -------
    Filtered and checked (for format and choices only) dictionary with settings
    extracted from the command line. For sanity checks use the tools/sanity module.
    """

    parser = create_argparser()
    raw_cmd_settings = vars(parser.parse_args())

    # check for formal correctness
    cmd_settings = check_settings(raw_cmd_settings, options, list_split_pattern)

    return cmd_settings


