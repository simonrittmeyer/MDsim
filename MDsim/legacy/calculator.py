# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# calculator base class
from ase.calculators.calculator import Calculator

# unit conversion
from MDsim import units


class CustomCalculator(Calculator):
    """
    ASE Calculator that interfaces a 2D numerical potential 
    """
    
    # calculator can handle energies
    implemented_properties = ['energy', 'forces']
    
    default_parameters = {'forces' : 'numerical',
                          'forces_stepwidth' : 1e-2}
    nolabel = True

    def __init__(self, atomsEq, potential_obj, **kwargs):
        Calculator.__init__(self, **kwargs)
        
        self.potential_obj = potential_obj
        self.atomsEq       = atomsEq
        self.energy        = None
        self.forces        = None
        
        self.free_atoms = atomsEq.get_free_atoms()
        self.free_dof   = atomsEq.get_free_dof()

        # index map for reduced dimensionality
        self.index_map = [(i,j) for i in self.free_atoms 
                                for j in self.free_dof
                                ]


    def __str__(self):
        name = 'ASE calculator for Interpolated2DPotential instances'
        return name

    def _filter_coords(self, pos):
        """
        Filters the positions array of the entire atoms object to those
        variables that are actually accounted for within the potential.
        
        THIS IS POTENTIAL-SPECIFIC!
        """
        pos = pos[self.free_atoms][:,self.free_dof]
        
        return pos.flatten()
    
    def _refilter_forces(self, forces):
        """ 
        Extends the forces obtained from the potential to a full forces vector.
        Any elements not given from the potential will be set zeros.
        """
        complete_forces = np.zeros_like(self.atoms.get_positions())
                
        for i,idx in enumerate(self.index_map):
            complete_forces[idx] = forces[i]
        
        return complete_forces
    
    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=['positions']):
        Calculator.calculate(self, atoms, properties, system_changes)
        
        pos        = self._filter_coords(self.atoms.get_positions())
        
        # convert to a.u. (the potential instance needs it like that!)
        pos   *= units.ANGSTROM_TO_AU
        
        energy = self.potential_obj.get_Epot(pos)
        
        # use the generic get_force routine from the potential...
        forces = self.potential_obj.get_force(pos, method = self.parameters['forces'], 
                                                   d = self.parameters['forces_stepwidth'])
        
        # convert back to ASE units
        self.energy = energy * units.AU_TO_EV
        self.forces = self._refilter_forces(forces) * units.AU_TO_EV_PER_ANGSTROM

    def update(self, atoms):
        if (self.energy is None) or (self.atoms != atoms):
            self.calculate(atoms)
            self.atoms = atoms.copy()
            
    def get_potential_energy(self, atoms):
        self.update(atoms)
        return self.energy
    
    def get_forces(self, atoms):
        self.update(atoms)
        return self.forces

