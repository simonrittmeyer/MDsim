# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
from MDsim import units

def central_differences(potential, positions, d):
    """
    Routine to evaluate forces using finite central differences. Becomes very
    unfeasable for large system though it may be somewhat useful for small and
    low-dimensional systems.

    Basically just a wrapper for _central_differences to maintain backward
    compatibility.

    Parameters
    ----------
    potential : MDsim potential instance
        Potential instance which returns a scalar upon calling a
        potential.get_Epot() method with the positions matrix.


    positions : (Natoms x Ndim) array
        Positions at which to evaluate the forces. Should in principle be
        rather C than Fortran contigious.

    d : float
        The stepwidth for the central differences. Note that the distance
        between the two points will be 2*d.

    Returns
    -------
    forces : (Natoms x Ndim) array
        The forces at the respective positions. This array is C-contigious.
    """
    calc_Epot = potential.get_Epot
    return _central_differences(calc_Epot, positions, d)


def _central_differences(calc_Epot, positions, d):
    """
    Routine to evaluate forces using finite central differences. Becomes very
    unfeasable for large system though it may be somewhat useful for small and
    low-dimensional systems.

    Parameters
    ----------
    calc_Epot : Callable
        Function that actually returns forces upon being called with a
        positions array. This is very generic, you can for instance also pass
        system.get_potential_energy here.

        Required interface is

            ``calc_Epot(positions)``

        and <positions> is expected in a.u..


    positions : (Natoms x Ndim) array
        Positions at which to evaluate the forces. Should in principle be
        rather C than Fortran contigious.

    d : float
        The stepwidth for the central differences. Note that the distance
        between the two points will be 2*d.

    Returns
    -------
    forces : (Natoms x Ndim) array
        The forces at the respective positions. This array is C-contigious.
        According to MDsim conventions, units are a.u.
    """
    Natoms, Ndim = positions.shape

    forces = np.empty((Natoms, Ndim), dtype = np.float64, order = 'C')

    tmp = positions.copy()

    for i in range(Natoms):
        for j in range(Ndim):
            # the positive displacement
            tmp[i,j] += d

            # no need to store several numbers
            dE = calc_Epot(tmp)

            # the negative displacement
            tmp[i,j] -= 2*d
            dE -= calc_Epot(tmp)

            # the negative gradient
            forces[i,j] = -(dE / (2.*d))

            # reset tmp
            tmp[i,j] += d
    return forces


