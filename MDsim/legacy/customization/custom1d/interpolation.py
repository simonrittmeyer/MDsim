# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings

# unit conversion
from MDsim import units



class InterpolatedObject(object):
    """
    Basis class for 1D interpolation.

    Initiliazation
    --------------
    points     : (Npoints x 1) array
                 Array containg all points (called x later on).
    
    observable : (Npoins x 1) array
                 Array containing the values for the points specified before.
    
    function   : string ("InterpolatedUnivariateSpline")
                 SciPy routine that will be employed to interpolate the grid.
                 This far there is only one routine available (but it should do
                 the job)
    
    kind       : string ({'linear', 'cubic', 'quintic'})
                 Degree of the bivariate splines unerlying the interpolation.

    data_origin : string (default= '(not available)')
                 String specifying the origin of the data (if wanted). Can be
                 useful for IO reasons.
    """
    def __init__(self, 
                 points, 
                 observable, 
                 function = 'InterpolatedUnivariateSpline', 
                 kind = 'cubic', 
                 data_origin = '(not available)'):
        
        # hard coded system dimensionality
        self.Ndim = 1
        
        self.points      = points.copy()
        self.observable  = observable.copy()
        self.kind        = kind
        self.function    = function
        
        # the interpolation range
        self.interp_range = np.array([np.min(points), np.max(points)])
        
        # check the grid
        self.check_grid(self.points, self.observable)

        # Initialiaze the interpolation function
        self.interp_func = self._init_interp(function   = self.function,
                                             points     = self.points, 
                                             observable = self.observable,
                                             kind       = self.kind) 
                           
        # remember where the data came from... IO reasons
        self.data_origin = data_origin
    
    def check_grid(self, points, observable):
        # make sure the points are sorted correctly
        x = points.copy()
        
        # sort x first, then y
        idx = np.lexsort(keys=(observable, x))

        # write to member variables
        self.points = points[idx]
        self.observable = observable[idx]
    

    def _init_interp(self, function, points, observable, kind):
        
        x = points.copy()
        
        if kind == 'linear':
            k = 1
        elif kind == 'cubic':
            k = 3
        elif kind == 'quintic':
            k = 5

        if function.lower() == 'interpolatedunivariatespline':
            # interpolation function: Based on the routine SURFIT from FITPACK
            from scipy.interpolate import InterpolatedUnivariateSpline as spline
            
            #ext = 2: raise ValueError if outside of interpolation range
            interp_func = spline(x = x, y = observable, k = k, ext = 2)
        
        return interp_func
    
    def reinterpolate(self, function, points, observable, kind):
        self.interp_func = self._init_interp(function   = function,
                                             points     = points, 
                                             observable = observable,
                                             kind       = kind)
    
    
    def __call__(self, positions):
        # sanity check whether we are in the interpolation range 
        if (float(positions) < self.interp_range[0] or float(positions) > self.interp_range[1]):
            warnings.warn('positions out of interpolation range', RuntimeWarning)
            return np.nan

        return float(self.interp_func(*positions))   
    
    def get_interpolation_range(self, au = False):
        if au:
            return self.interp_range.copy()
        else:
            return self.interp_range.copy() * units.AU_TO_ANGSTROM 

    def show(self):
        # display everything in internal coordinates (just as the potential 
        #                                            is fitted)
        import matplotlib.pyplot as plt
        
        # 'continuous' x,y values to plot
        x = np.linspace(np.min(self.points),np.max(self.points),200)
        
        plt.plot(x, self.interp_func(x)) 
        plt.plot(self.points, self.observable, makers  ='o', ls ='')

        plt.show()

    def get_Ndim(self):
        return self.Ndim
