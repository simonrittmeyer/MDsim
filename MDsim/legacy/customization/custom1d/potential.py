# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# unit conversion
from MDsim import units

# parent class
from MDsim.customization.custom1d.interpolation import InterpolatedObject

            
class InterpolatedPotential1D(InterpolatedObject):
    """
    Basically this is a wrapper class around a SciPy interpolation routine. 
    This routine expects input in atomic units!

    For detailed information about the paramenters see the parent class.
    """
    def __init__(self, points, 
                       energies, 
                       convert = True, 
                       function = 'InterpolatedUnivariateSpline', 
                       kind = 'cubic',
                       data_origin = '(not available)'):
        # points in format [(dRcom, ddist),...]
        self.points      = points
        
        self.energies    = energies
        self.energies   -= min(self.energies)
        
        if convert:
            self.points   *= units.ANGSTROM_TO_AU 
            self.energies *= units.EV_TO_AU
        
        InterpolatedObject.__init__(self, points      = self.points,
                                          observable  = self.energies,
                                          function    = function,
                                          kind        = kind,
                                          data_origin = data_origin)
      
        # in the 1D case, derivatives are for free ;) 
        self.derivative = self.interp_func.derivative()
                
    def __str__(self):
        name = 'Interpolated 1D potential (using {0:s} with {1:s} splines)'.format(self.function, self.kind)
        name += '\nData origin: {}'.format(self.data_origin)
        return name 
       
    
    def get_force(self, positions, method = 'analytical', d = 0.01):
        """
        Generic force interface.
        If method == 'analytical', then 'd' is useless.
        """
        if method == 'numerical':
            return self.get_force_numerical(positions, d = d)
        elif method == 'analytical':
            return self.get_force_analytical(positions)
        else:
            raise NotImplementedError('Requested force method {} not implemented'.format(method))
   

    def get_force_numerical(self, positions, d = 0.01):
        # d is the finite difference step size
        force = np.empty_like(positions)
        
        # evaluate the gradient using central differences
        for i, p in enumerate(positions):
            disp  = positions.copy()
            disp_ = positions.copy()
            disp[i]  += d
            disp_[i] -= d
            force[i] = -(self.get_Epot(disp) - self.get_Epot(disp_)) / (2*d)  
        return force
    
    def get_force_analytical(self, positions):
        # take care of the shape of the forces...
        return  -np.array([self.derivative(*positions)])

    def get_Epot(self, positions):
        return self.interp_func(*positions) 
        
