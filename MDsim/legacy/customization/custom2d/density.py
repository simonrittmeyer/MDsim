# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# unit conversion
from MDsim import units

# parent class
from MDsim.customization.custom2d.interpolation import InterpolatedObject

        
        
class InterpolatedDensity(InterpolatedObject):
    def __init__(self, points, 
                       densities, 
                       atoms_eq, 
                       convert = True, 
                       function = 'RectBivariateSpline', 
                       kind = 'cubic',
                       data_origin = '(not available)'):
        
        if convert:
            points *= units.ANGSTROM_TO_AU
    
        InterpolatedObject.__init__(self, points  = points,
                                      observable  = densities,
                                      atoms_eq    = atoms_eq,
                                      function    = function,
                                      kind        = kind,
                                      data_origin = data_origin)
    
        self.name  = 'Generic interpolated 2D electronic density'
        self.name += '\n\t(using {0:s} with {1:s} splines)'.format(self.function, self.kind)
        self.name += '\nData origin: {}'.format(self.data_origin)


