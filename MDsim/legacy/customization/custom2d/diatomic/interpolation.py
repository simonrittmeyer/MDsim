# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings

# unit conversion
from MDsim import units

# base class and transformations
from MDsim.customization.custom2d.interpolation import InterpolatedObject
from MDsim.customization.custom2d.diatomic.transformations import get_transformations_perp
#from MDsim.customization.custom2d.diatomic.transformations import get_transformations_para

class InterpolatedObjectDiatomic(InterpolatedObject):
    """
    Basis class for several things mapped on a dRcom-ddist grid for Diatomics
    instead of actual positions.
    

    Initiliazation
    --------------
    points     : (Npoints x 2) array
                 Array containg all x and y points.
    
    observable : (Npoins x 1) array
                 Array containing the values for the points specified before.
    
    atoms_eq   : AtomsEq instance
                 Specifying the equilibrium geometry to which the displacements
                 (a.k.a. the points) are relative to.
   
    flavor     : string, optional {"para", *"perp"*}
                 Determines the flavor of the coordinate transformations used.

    function   : string ({'interp2d', 'SmoothBivariateSpline', *'RectBivariateSpline'*})
                 SciPy routine that will be employed to interpolate the grid. Note that
                 'RectBivariateSpline' is very fast and reliable but only works
                 on regular, rectangular grids.
    
    kind       : string ({'linear', 'cubic', 'quintic'})
                 Degree of the bivariate splines unerlying the interpolation.

    data_origin : string (default= '(not available)')
                 String specifying the origin of the data (if wanted). Can be
                 useful for IO reasons.
    """
    def __init__(self, 
                 points, 
                 observable, 
                 atoms_eq,
                 flavor = 'perp',
                 function = 'RectBivariateSpline', 
                 kind = 'cubic', 
                 data_origin = '(not available)'):
        
        # initialize the parent 
        InterpolatedObject.__init__(self, points = points, 
                                          observable = observable, 
                                          function = function, 
                                          kind = kind, 
                                          data_origin = data_origin)
        
        # store equilibrium information (coordinate transformations)
        self.atoms_eq = atoms_eq

        self.flavor = flavor.lower()
        
        if self.flavor == 'perp':
            get_transformations = get_transformations_perp
        # we handle it differently... using fake atomsEq makes life much easier
        # than to reduce from 4D to 2D!
#        elif self.flavor == 'para':
#            get_transformations = get_transformations_para
        else:
            raise NotImplementedError('Flavor "{}" unknown.'.format(flavor))
        
        # this yields Q, Qinv and the Jacobian 
        self.__dict__.update(get_transformations(self.atoms_eq))
        
        # needed for conversion of positions
        self.positions_eq = self.atoms_eq.get_positions_eq().flatten() * units.ANGSTROM_TO_AU
        self.internal_eq = self.Q.dot(self.positions_eq)

    def _positions_to_internal(self, positions):
        return self.Q.dot(positions) - self.internal_eq

    
    def _internal_to_positions(self, internal):
        # these are detla displacements
        return self.Qinv.dot(internal) + self.positions_eq

    
    def __call__(self, positions):
        internal = self._positions_to_internal(positions)
        
        # raises value error if outside of range
        # but is extremely costly (multiplies cost by a factor of two)
        #self._check_bounds(internal)
        return float(self.interp_func(*internal))
   

    def direct_call(self, positions):
        """
        Call the interpolation function directly in the basis of the splines
        """
        return float(self.interp_func(*positions))
    

    def get_Ndim(self):
        return self.Ndim
