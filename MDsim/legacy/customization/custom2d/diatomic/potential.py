# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# unit conversion
from MDsim import units

# parent class
from MDsim.customization.custom2d.diatomic.interpolation import InterpolatedObjectDiatomic

class InterpolatedPotentialDiatomic(InterpolatedObjectDiatomic):
    """
    Basically this is a wrapper class around a SciPy interpolation routine. 

    We can obtain forces numerically via central differences or analytically
    via spline derivatives and successive transformation into positions space.
    
    Initiliazation
    --------------
    points     : (Npoints x 2) array
                 Array containg all x and y points.
    
    observable : (Npoins x 1) array
                 Array containing the values for the points specified before.
    
    atoms_eq   : AtomsEq instance
                 Specifying the equilibrium geometry to which the displacements
                 (a.k.a. the points) are relative to.
    
    flavor     : string, optional {"para", *"perp"*}
                 Determines the flavor of the coordinate transformations used.

    function   : string ({'interp2d', 'SmoothBivariateSpline', *'RectBivariateSpline'*})
                 SciPy routine that will be employed to interpolate the grid. Note that
                 'RectBivariateSpline' is very fast and reliable but only works
                 on regular, rectangular grids.
    
    kind       : string ({'linear', 'cubic', 'quintic'})
                 Degree of the bivariate splines unerlying the interpolation.

    data_origin : string (default= '(not available)')
                 String specifying the origin of the data (if wanted). Can be
                 useful for IO reasons.
    """
    def __init__(self, points, 
                       energies, 
                       atoms_eq, 
                       flavor = 'perp',
                       convert = True, 
                       function = 'RectBivariateSpline', 
                       kind = 'cubic',
                       data_origin = '(not available)'):
        
        if convert:
            points   *= units.ANGSTROM_TO_AU 
            energies *= units.EV_TO_AU
        
        energies -= np.min(energies)

        InterpolatedObjectDiatomic.__init__(self, 
                                        points      = points,
                                        observable  = energies,
                                        flavor      = flavor,
                                        atoms_eq    = atoms_eq,
                                        function    = function,
                                        kind        = kind,
                                        data_origin = data_origin)
      
        
        # The Jacobian for the backtransformation from internals to positions
        # is just the transformation matrix (linear transformations)
        # J_ij(internal --> positions) = d(internal_i)/d(positions_j)
        self.Jacobian = self.Qinv 
             
        # build the __str__ representation
        self.name = 'Interpolated 2D potential for diatomic adsorbates'
        self.name += '\n\tusing {0:s} with {1:s} splines'.format(self.function, self.kind)
        self.name += '\n\tusing "{}" transformation matrices'.format(self.flavor)
        self.name += '\n\tdata origin: {}'.format(self.data_origin)
       

    def get_force(self, positions, method = 'numerical', d = 0.01):
        """
        Generic force interface.
        If method == 'analytical', then 'd' is useless.
        """
        if method == 'numerical':
            return self.get_force_numerical(positions, d = d)
        elif method == 'analytical':
            return self.get_force_analytical(positions)
        else:
            raise NotImplementedError('Requested force method {} not implemented'.format(method))
    

    def get_force_analytical(self, positions):
        # input & output : positions basis
        internal = self._positions_to_internal(positions)
        force = self._get_force_splined(internal)
        # multiply with jacobian to back-transform the force
        return self.Jacobian.dot(force)
   

    def _get_force_splined(self, internal):
        # input & output: internal coordinates basis
        force = np.zeros(2)
        force[0] = -float(self.interp_func(*internal, dx = 1, dy = 0))
        force[1] = -float(self.interp_func(*internal, dx = 0, dy = 1))
        return force


    def get_force_numerical(self, positions, d = 0.01):
        # input & output : positions basis
        # d is the finite difference step size
        force = np.empty_like(positions)
        
        # evaluate the gradient using central differences
        for i, p in enumerate(positions):
            disp  = positions.copy()
            disp_ = positions.copy()
            disp[i]  += d
            disp_[i] -= d
            force[i] = -(self.get_Epot(disp) - self.get_Epot(disp_)) / (2*d)  
        return force
    
    def get_Epot(self, positions):
        # input and output : positions basis
        internal = self._positions_to_internal(positions)
        return float(self.interp_func(*internal)) 
