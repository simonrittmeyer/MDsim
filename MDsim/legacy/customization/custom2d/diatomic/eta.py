# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# unit conversion
from MDsim import units

# parent class
from MDsim.customization.custom2d.diatomic.interpolation import InterpolatedObjectDiatomic

class InterpolatedEtaDiatomic(InterpolatedObjectDiatomic):
    def __init__(self, points, 
                       etas, 
                       atoms_eq,
                       flavor,
                       convert = True, 
                       function = 'RectBivariateSpline', 
                       kind = 'cubic',
                       data_origin = '(not available)'):
        
        if convert:
            points *= units.ANGSTROM_TO_AU 
        
        InterpolatedObjectDiatomic.__init__(self, 
                                        points      = points,
                                        observable  = etas,
                                        atoms_eq    = atoms_eq,
                                        flavor      = flavor,
                                        function    = function,
                                        kind        = kind,
                                        data_origin = data_origin)
      
             
        # build the __str__ representation
        self.name = 'Interpolated 2D friction coefficient for diatomic adsorbates'
        self.name += '\n\tusing {0:s} with {1:s} splines'.format(self.function, self.kind)
        self.name += '\n\tusing "{}" transformation matrices'.format(self.flavor)
        self.name += '\n\tdata origin: {}'.format(self.data_origin)
       
   
