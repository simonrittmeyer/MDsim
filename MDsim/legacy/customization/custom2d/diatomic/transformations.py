# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# create the specific transformations based on the atoms object

# everything is based on displacement coordinates

# these functions must return a dictionary with:
# Q        : transformation matrix from positions to internal coordinates
# Qinv     : transformation matrix from internal coordinates to positions
# Jacobian : Jacobian for derivatices from internal to positions (== Qinv for linear transformations)
# atoms_eq : atoms_eq object on which these things are based (to maintain the correct mapping of masses and stuff)

import numpy as np
from MDsim import units


def get_transformations_perp(atoms_eq):
    # handle everything in a.u. here
    positions_eq = atoms_eq.get_positions_eq() * units.ANGSTROM_TO_AU
    masses = atoms_eq.get_masses() * units.AMU_TO_AU                
    M = sum(masses)
    
    # we define the distance at equilibrium to be positive
    if positions_eq[0] < positions_eq[1]:
        sign = -1
    else:
        sign = 1
    
    # the transformation matrix pos --> internal
    Q = np.zeros((2,2), dtype = float)
    Q[0,0] = masses[0] / M
    Q[0,1] = masses[1] / M
    Q[1,0] = 1. * sign
    Q[1,1] = -1. * sign

    # just in case we need it at some point
    # trafo matrix internal --> pos
    Qinv = np.zeros_like(Q)
    Qinv[0,0] = 1.
    Qinv[0,1] = masses[1] / M * sign
    Qinv[1,0] = 1.
    Qinv[1,1] = -masses[0] / M * sign
    
    return {'Q' : Q,
            'Qinv' : Qinv,
            'Jacobian' : Qinv}


# DEPRECATED since v2.4.1
# The transformation from 4D to 2D is easier done by reducing to effective
# two-body problem using a fake atomsEq instance with the resepctive reduced
# masses.

#def get_transformations_para(atoms_eq):
#    # pseudo 4d MD with 2D potential...
#    # handle everything in a.u. here
#    positions_eq = atoms_eq.get_positions_eq() * units.ANGSTROM_TO_AU
#    masses = atoms_eq.get_masses() * units.AMU_TO_AU                
#    M = sum(masses)

#    # we define the distance at equilibrium to be positive
#    # hard code the x coordinate here: pos = [x1, z1, x2, z2]
#    if positions_eq[0] < positions_eq[2]:
#        sign = -1
#    else:
#        sign = 1
#    
#    # the transformation matrix pos --> internal
#    # see 2015 notebook no. 1, page 80
#    
#    Q = np.zeros((2,4), dtype = zeros)
#    
#    # only assign the non-zero elements
#    # assume that the molecule is parallel to the surface
#    # the Zcom = Z1 = Z2 = 0.5(Z1 + Z2) if you wish...
#    Q[0,1] = 0.5
#    Q[0,3] = 0.5
#    Q[1,0] = 1. * sign
#    Q[1,2] = -1 * sign
#    
#    # back transformation is kind of tricky. We need to make assumptions to
#    # create 4 DOF out of 2
#    #
#    # First assumption 
#    # ----------------
#    # the molecule is oriented parallel to the surface (which will be
#    # correct as long as we start from such a
#    #
#    # Second assumption: 
#    # ------------------
#    # The centre of mass is fixed with respect to the x coordinate.
#    # configuration)
#    
#    # transformation matrix
#    Qinv = np.zeros((4,2))

#    # only assign the non-zero elements
#    Qinv[0,1] = 0.5 * sign
#    Qinv[1,0] = 1
#    Qinv[2,1] = -0.5 * sign
#    Qinv[3,0] = 1

#    return {'Q' : Q,
#            'Qinv' : Qinv,
#            'Jacobian' : Qinv}


