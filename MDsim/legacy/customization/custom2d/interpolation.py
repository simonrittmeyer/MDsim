# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings

# unit conversion
from MDsim import units

class InterpolatedObject(object):
    """
    Basis class for several things mapped on a 2D grid.
    
    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.
    
    Initiliazation
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.
    
    values : (Npoints) array
        Vaules corresponding to the <points>.
          
    observable : (1 x Npoints) array
                 Array containing the values for the points specified before.
    
    function   : string ({'interp2d', 'SmoothBivariateSpline', *'RectBivariateSpline'*})
                 SciPy routine that will be employed to interpolate the grid. Note that
                 'RectBivariateSpline' is very fast and reliable but only works
                 on regular, rectangular grids.
    
    kind       : string ({'linear', 'cubic', 'quintic'})
                 Degree of the bivariate splines unerlying the interpolation.

    data_origin : string (default= '(not available)')
                 String specifying the origin of the data (if wanted). Can be
                 useful for IO reasons.
    """
    def __init__(self, 
                 points, 
                 observable, 
                 function = 'RectBivariateSpline', 
                 kind = 'cubic', 
                 data_origin = '(not available)'):
        
        # hard coded system dimensionality
        self.Ndim = 2
                           
        # remember where the data came from... IO reasons
        self.data_origin = data_origin
        
        self.points      = points.copy()
        self.observable  = observable.copy()
        self.kind        = kind
        self.function    = function
        
        # the interpolation range
        self.interp_range = np.array([[np.min(points[0]), np.max(points[0])],
                                      [np.min(points[1]), np.max(points[1])]])
        # check the grid
        self._check_grid(self.points, self.observable)

        # Initialiaze the interpolation function
        self.interp_func = self._init_interp(function   = self.function,
                                             points     = self.points, 
                                             observable = self.observable,
                                             kind       = self.kind) 
                                              
        # the string representation
        self.name = "Generic 2D Interpolation object"

    def __str__(self):
        return self.name


    def __call__(self, positions):
        # no further conversion required
        # no bounds checks due to performance reasons
        # self._check_bounds(positions)
        return float(self.interp_func(*positions))   


    def _check_bounds(self, point):
        # sanity check whether we are in the interpolation range 
        if np.any(point < self.interp_range[:,0]) or np.any(point > self.interp_range[:,1]):
            raise ValueError('Out of interpolation range.')
   

    def _check_grid(self, points, observable):
        # make sure the points are sorted correctly
        x = points[0].copy()
        y = points[1].copy()
        
        # sort x first, then y
        idx = np.lexsort(keys=(observable, y, x))

        # write to member variables
        self.points = np.vstack((x[idx],y[idx]))
        self.observable = observable[idx]


    def _init_interp(self, function, points, observable, kind):
        
        x = points[0].copy()
        y = points[1].copy()
        
        if kind == 'linear':
            kx = 1; ky = 1
        elif kind == 'cubic':
            kx = 3; ky = 3
        elif kind == 'quintic':
            kx = 5; ky = 5

        if function.lower() == 'interp2d':
            # interpolation function: Based on the routine SURFIT from FITPACK
            from scipy.interpolate import interp2d
            
            interp_func = interp2d(x = x, y = y, z = observable, kind = kind, 
                                   fill_value = np.nan)
        
        elif function.lower() == 'smoothbivariatespline':
            # interpolation function: Based on the routine SURFIT from FITPACK
            from scipy.interpolate import SmoothBivariateSpline 
            
            interp_func = SmoothBivariateSpline(x = x, y = y, z = observable, 
                                   kx = kx, ky = ky,
                                   s = 0) #interpolation, no smoothing

        elif function.lower() == 'rectbivariatespline':
            # here we have to work on the given points...
            # this only works for regular, rectangular grids!
            # the x and y coordinate must be given in strictly ascending order!
            from scipy.interpolate import RectBivariateSpline
            
            # only the unique points 
            x = np.unique(x)
            y = np.unique(y)
            
            if not len(x)*len(y) == len(observable):
                raise ValueError('something is wrong with your data...')
                
            # reshape the observable points
            obs = observable.reshape((len(x), len(y)))

            interp_func = RectBivariateSpline(x = x, y = y, z = obs, 
                                              kx = kx, ky = ky,
                                              s = 0) # interpolation, no smoothing


        return interp_func
    
    def reinterpolate(self, function, points, observable, kind):
        self.interp_func = self._init_interp(function   = function,
                                             points     = points, 
                                             observable = observable,
                                             kind       = kind)
    
     
    
    
    
    def get_interpolation_range(self, au = False):
        if au:
            return self.interp_range.copy()
        else:
            return self.interp_range.copy() * units.AU_TO_ANGSTROM 

    
    def show(self, lim = None):
        # display everything in internal coordinates (just as the potential 
        #                                            is fitted)
        import matplotlib.pyplot as plt
        
        if lim is None:
            lim = (0,5)

        # 'continuous' x,y values to plot
        x = np.linspace(np.min(self.points[0]),np.max(self.points[0]),200)
        y = np.linspace(np.min(self.points[1]),np.max(self.points[1]),200)
        X,Y = np.meshgrid(x,y)
        
        #vectorize interpolation function
        inter = np.vectorize(self.interp_func)
        
        # get the interpolated values on a grid
        Z = inter(X,Y)
        
        # colormap
        my_cmap = plt.get_cmap('jet_r')
        
        # surface plot
        surf = plt.pcolormesh(X,Y,Z, cmap = my_cmap)
        
        # contour plot
        cont = plt.contour(X,Y,Z, colors = 'black', extend="both",
                           levels = np.linspace(*lim,num=11))
        
        # scatter plot
        scat = plt.scatter(x = self.points[0], y = self.points[1], 
                           c = self.observable, s = 20, cmap = my_cmap, 
                           label = 'sampled points')
        
        for i in [surf]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)
        
        plt.xlabel('X / a.u.')
        plt.ylabel('Y / a.u.')
        plt.clabel(cont, fmt ='%.2f a.u.')
        
        plt.show()
        

    def get_Ndim(self):
        return self.Ndim
    
    
    def set_name(self, name):
        self.name = name
