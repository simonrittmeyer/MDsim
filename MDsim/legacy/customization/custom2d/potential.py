# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# unit conversion
from MDsim import units

# parent class
from MDsim.customization.custom2d.interpolation import InterpolatedObject

            
class InterpolatedPotential2D(InterpolatedObject):
    """
    Basically this is a wrapper class around a SciPy interpolation routine. 
    There will be no coordinate transformations done.

    Initialization
    --------------
    points     : (Npoints x 2) array
                 Array containg all x and y points.
    
    energies   : (Npoints x 1) array
                 Array containing the energies for the points specified before.
    
    convert    : Boolean
                 Whether to convert input into a.u. (from A/eV) or not.

    function   : string ({'interp2d', 'SmoothBivariateSpline', *'RectBivariateSpline'*})
                 SciPy routine that will be employed to interpolate the grid. Note that
                 'RectBivariateSpline' is very fast and reliable but only works
                 on regular, rectangular grids.
    
    kind       : string ({'linear', 'cubic', 'quintic'})
                 Degree of the bivariate splines unerlying the interpolation.

    data_origin : string (default= '(not available)')
                 String specifying the origin of the data (if wanted). Can be
                 useful for IO reasons.
    """
    def __init__(self, points, 
                       energies, 
                       convert = True, 
                       function = 'RectBivariateSpline', 
                       kind = 'cubic',
                       data_origin = '(not available)'):
        
        if convert:
            points   *= units.ANGSTROM_TO_AU 
            energies *= units.EV_TO_AU
        
        energies -= np.min(energies)

        InterpolatedObject.__init__(self, 
                                    points      = points,
                                    observable  = energies,
                                    function    = function,
                                    kind        = kind,
                                    data_origin = data_origin)
      
         
        self.name = 'Generic interpolated 2D potential' 
        self.name += '\t(using {0:s} with {1:s} splines)'.format(self.function, self.kind)
        self.name += '\nData origin: {}'.format(self.data_origin)
       
    
    def get_force(self, positions, method = 'numerical', d = 0.01):
        """
        Generic force interface.
        If method == 'analytical', then 'd' is useless.
        """
        if method == 'numerical':
            return self.get_force_numerical(positions, d = d)
        elif method == 'analytical':
            return self.get_force_splined(positions)
        else:
            raise NotImplementedError('Requested force method {} not implemented'.format(method))
    
    
    def get_force_analytical(self, positions):
        force = np.zeros(2)
        force[0] = -float(self.interp_func(*positions, dx = 1, dy = 0))
        force[1] = -float(self.interp_func(*positions, dx = 0, dy = 1))
        return force


    def get_force_numerical(self, positions, d = 0.01):
        # d is the finite difference step size
        force = np.empty_like(positions)
        
        # evaluate the gradient using central differences
        for i, p in enumerate(positions):
            disp  = positions.copy()
            disp_ = positions.copy()
            disp[i]  += d
            disp_[i] -= d
            force[i] = -(self.get_Epot(disp) - self.get_Epot(disp_)) / (2*d)  
        return force


    def get_Epot(self, positions):
        return float(self.interp_func(*positions)) 
        
#    def show(self):
#        # display everythin in internal coordinates (just as the potential 
#        #                                            is fitted)
#        import matplotlib.pyplot as plt
#        
#        # 'continuous' x,y values to plot
#        x = np.linspace(np.min(self.points[:,0]),np.max(self.points[:,0]),200)
#        y = np.linspace(np.min(self.points[:,1]),np.max(self.points[:,1]),200)
#        X,Y = np.meshgrid(x,y)
#        
#        #vectorize interpolation function
#        inter = np.vectorize(self.interp_func)
#        
#        # get the interpolated values on a grid
#        Z = inter(X,Y)
#        
#        # colormap
#        my_cmap = plt.get_cmap('Blues_r')
#        
#        # maximal value to be shown
#        Elim = 3.5*units.EV_TO_AU
#        
#        # surface plot
#        surf = plt.pcolormesh(X,Y,Z, cmap = my_cmap)
#        
#        # contour plot
#        cont = plt.contour(X,Y,Z, colors = 'black', extend="both", 
#                           levels = np.linspace(0,Elim,11))
#        
#        # scatter plot
#        scat = plt.scatter(x = self.points[:,0], y = self.points[:,1], 
#                           c = self.energies, s = 20, cmap = my_cmap, 
#                           label = 'sampled points')
#        
#        # restrict the colormap to certain values
#        for i in [scat, surf]:
#           i.set_clim(-0.1 * units.EV_TO_AU, Elim)
#           i.cmap.set_under('white')
#           i.cmap.set_over('white')
#        
#        # colorbar
#        cb = plt.colorbar(surf)
#        cb.set_label(r'$E$ / a.u.')
#        # axes labels
#        plt.xlabel(r'$\Delta R_\mathrm{COM}$ / a.u.')
#        plt.ylabel(r'$\Delta d$ / a.u.')
#        plt.title(r'Fitted potential, arrows symbolize forces')
#        
#        # get the forces
#        xs = []
#        ys = []
#        _forces = []
#        forces = []
#        for x in self.points[:,0]:
#            for y in self.points[:,1]:
#                if self.direct_call([x,y]) < Elim:
#                    xs.append(x)
#                    ys.append(y)
#                    forces.append(self._get_force(x,y))
#                    _forces.append(self._get_force_splined((x,y)))
#        xs = np.array(xs)
#        ys = np.array(ys)
#        forces = np.array(forces)
#        _forces = np.array(_forces)
#        
#        plt.quiver(xs, ys, _forces[:,0], _forces[:,1], pivot = 'middle', scale = 2, color = 'lightgray')
#        plt.quiver(xs, ys, forces[:,0], forces[:,1], pivot = 'middle', scale = 2, color = 'darkgray')
#        
#        plt.show()
#    
#    def _get_force(self, dRcom, ddist, d = 0.01):
#        # internal routine to get the forces directly in internal coordinates
#        # for show() only 
#        positions = np.array([dRcom, ddist])
#        # d is the finite difference step size
#        force = np.empty_like(positions)
#        
#        # evaluate the gradient using central differences
#        for i, p in enumerate(positions):
#            disp  = positions.copy()
#            disp_ = positions.copy()
#            disp[i]  += d
#            disp_[i] -= d
#            force[i] = -(self.direct_call(disp) - self.direct_call(disp_)) / (2*d)  
#        return force
