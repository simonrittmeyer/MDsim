# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units
from MDsim.forces import central_differences
from MDsim.interpolation.bivariatespline.internalbivariatespline import InternalBivariateSpline

class InternalBivariatePotential(InternalBivariateSpline):
    """
    Basically the same as BivariatePotential with one very difficile diffrence:

    We account for the fact were we have to deal with non-cartesian coordinate
    systems (we call it internal coordinates [IC] here). Then we will have
    nominally Ndim == 1 and Natoms = Ncoords, such that we can assign different
    masses to each coordinate.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    energies : (Npoints) array
        energies corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    forces_method : string, optional ({*'analytical'*, 'numerical'})
        How to evaluate the forces. Analytical forces deduce the gradient
        from the analytical spline derrivatives, where numerical forces
        invoke finite (central) differences.

    forces_stepwidth : float, optional (default = 0.001)
        Stepwidth in case of finite differences evaluation.
        When going for analytical forces, this flag is of no importance.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    normalize : boolean, optional (default = True)
        Normalizes the energy values such that the lowest energy equal 0.0 meV.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, points, energies,
                 function = 'RectBivariateSpline',
                 kind = 'cubic',
                 data_origin = 'None',
                 forces_method = 'numerical',
                 forces_stepwidth = 0.001,
                 convert = True,
                 normalize = True,
                 _bounds_check = True):


        # normalization
        if normalize:
            energies -= np.min(energies)

        self._forces_method = forces_method.lower()

        if self._forces_method not in ['analytical', 'numerical']:
            print('Error : Forces method "{}" not implemented'.format(forces_method))
            raise NotImplementedError

        self._forces_stepwidth = forces_stepwidth

        if convert:
            points   *= units.ANGSTROM_TO_AU
            energies *= units.EV_TO_AU
            self._forces_stepwidth *= units.ANGSTROM_TO_AU


        InternalBivariateSpline.__init__(self, points = points,
                                       values = energies,
                                       function = function,
                                       kind = kind,
                                       data_origin = data_origin,
                                       _bounds_check = _bounds_check)

        self._name = "InternalBivariatePotential\n\t(interpolated potential)"

        # this is a bivariate potential, hence the internal coordinates need to
        # be a flat 2D array
        self._internals = np.empty(2)

    def transform_coordinates(self, positions):
        """
        This is the transformation from the (Natoms x Ndim) positions array to
        a flat internal coordinates array. Needs to be implemented by subclasses.
        """
        #raise NotImplementedError('Coordinate transformation has to be implemented by subclass')
        # most simple way: ravel the thing
        return positions.ravel()

    def backtransform_forces(self, forces):
        """
        Transform forces back to cartesian basis (Natoms x Ndim)
        """
        return forces.reshape(-1,1)

    def get_Epot(self, positions):
        """
        Evaluate the potential for the system specified via "positions".

        Parameters
        ----------
        positions : (Natoms x Ndof) array
            Position at which to evaluate the potential

        Returns
        -------
        energy : float
            The potential energy corresponding to <positions>.
        """
        return self.get_value(self.transform_coordinates(positions))


    def get_forces(self, positions, d = 0.001):
        """
        Routine to evaluate the forces originating from this potential..

        Parameters
        ----------
        positions : (Ndof x 1) array
            Position at which to evaluate the forces.

        Returns
        -------
        forces : (Ndof x 1 array
            The forces at the respective positions. This array is C-contigious.
        """

        if self._forces_method == 'analytical':
            return self.backtransform_forces(-self.get_gradient(self.transform_coordinates(positions)))
        elif self._forces_method == 'numerical':
            return central_differences(self, positions, self._forces_stepwidth)

