# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsimv3.tools                            import check_kwargs 
from MDsimv3.systems                          import NormalmodeSystem
from MDsimv3.so                               import SOSystem
from MDsimv3.glo                              import GLOSystem

# -----------------------------------------------------------------------------
# SYSTEMS 
# -----------------------------------------------------------------------------

def create_normalmodeSystem(atomsEq, normalmodes_obj,
                             forces_obj, **kwargs):

    param = {'etaMatrix_obj'  : None,
             'init_quanta'    : [0, 1]}
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    print 'creating NormalmodeSystem instance'

    print '\tusing initial quanta:'
    for i, q in enumerate(param['init_quanta']):
        print '\t\tmode_{0:d} : {1:3.1f}'.format(i,q)
    
    normalmodeSystem = NormalmodeSystem(atoms_eq = atomsEq,
                            normalmodes    = normalmodes_obj,
                            force_function = forces_obj,
                            eta_function   = param['etaMatrix_obj'],
                            init_quanta    = param['init_quanta'])

    return normalmodeSystem


def create_surfaceOscillatorSystem(atomsEq, normalmodes_obj,
                                   surfaceOscillatorForces_obj, **kwargs):

    param = {'etaMatrix_obj'  : None,
             'init_quanta'    : [0, 1],
             'surface_temp'   : 0}
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    print 'creating SOSystem instance'

    print '\tusing initial quanta %s'%param['init_quanta']
    print '\tusing surface temperature %sK'%param['surface_temp']
     
    surfaceOscillatorSystem = SOSystem(atoms_eq = atomsEq,
                                       normalmodes    = normalmodes_obj,
                                       force_function = surfaceOscillatorForces_obj,
                                       eta_function   = param['etaMatrix_obj'],
                                       init_quanta    = param['init_quanta'],
                                       surface_temp   = param['surface_temp'])

    return surfaceOscillatorSystem


def create_GLOSystem(atomsEq, 
                     normalmodes_obj,
                     potential_obj,
                     so_element,
                     so_freqs,
                     **kwargs):

    param = {'etaMatrix_obj'  : None,
             'init_quanta'    : [0, 1],
             'glo_temperature': 0,
             'ghost_gamma'    : 0,
             'time_step'      : 0.1,
             'stepwidth'      : 0.001,
             'ghost_freqs'    : None,
             'so_ghost_coupling' : None}
            
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
    
    print 'creating GLOSystem instance'

    print '\tusing initial quanta %s'%param['init_quanta']
    print '\tusing surface temperature %sK'%param['glo_temperature']
     
    if param['ghost_freqs'] == None:
        param['ghost_freqs'] = so_freqs
    if param['so_ghost_coupling'] == None:
        param['so_ghost_coupling'] = np.array(so_freqs)**2
   
    gloSystem = GLOSystem(atoms_eq = atomsEq,
                          normalmodes = normalmodes_obj,
                          init_quanta = param['init_quanta'],
                          potential = potential_obj,
                          eta_function = param['etaMatrix_obj'],
                          so_element =  so_element,
                          so_freqs = so_freqs,
                          so_ghost_coupling = param['so_ghost_coupling'],
                          ghost_freqs = param['ghost_freqs'],
                          ghost_gamma = param['ghost_gamma'],
                          temperature = param['glo_temperature'],
                          time_step = param['time_step'],
                          stepwidth = param['stepwidth'],
                          convert = True)
                            
    return gloSystem
