# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# Creating objects for MDsimv3

import numpy as np

import sys
import os
import StringIO
import glob

import ase

from ase.constraints import FixCartesian

from MDsimv3.tools                            import check_for_array
from MDsimv3.tools                            import check_kwargs 
from MDsimv3.tools.castep                     import read_cell
from MDsimv3.atomseq                          import AtomsEq

# -----------------------------------------------------------------------------
# ATOMS_EQ
# -----------------------------------------------------------------------------

def create_atomsEq(**kwargs):
    
    param = {'datapath' : 'data',
             'cellfile' : None}
    
    print 'creating AtomsEq instance'
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)
   
    if param['cellfile'] == None:
        cellfile = glob.glob(os.path.join(param['datapath'],'*.cell'))[0]
    else:
        cellfile = param['cellfile']

    print '\tusing file {}'.format(cellfile)

    # shut up, guys
    stdout = sys.stdout
    sys.stdout = StringIO.StringIO()
    atoms = read_cell(cellfile)
    sys.stdout = stdout

    atomsEq = AtomsEq(atoms)
    
    return atomsEq

def create_oneBodyAtomsEq(masses, **kwargs):
    # this is basically a fake atomseq object
    print 'creating AtomsEq instance for OneBody System'
    
    print '\tusing mass {}'.format(masses[0])
    
    # create a fake atoms object
    atoms = ase.Atoms('X', masses = masses)

    # x is the free variable...
    atoms.set_constraint(FixCartesian(0, mask = (0,1,1)))

    atomsEq = AtomsEq(atoms)

    return atomsEq


def create_paraAtomsEq(**kwargs):
    # We create a fake atom with the coordintes corresponding to the centre of
    # mass and the distance vector. Note that it is about displacements!
    #
    # This only works if we have two free atoms of the same element AND of the
    # same z-coordinate
    
    atomsEq = create_atomsEq(**kwargs)
    
    print 'transforming AtomsEq instance to AtomsEq for 2D "para" flavor'
    
    masses = atomsEq.get_masses()
    zpos   = atomsEq.get_positions_eq()[:,-1]
    
    if not((masses[0] == masses).all()):
        raise ValueError('Masses of free atoms must be the same for 2D "para" flavor!')
    if not((zpos[0] == zpos).all()):
        raise ValueError('Height (z-positions) of free atoms must be the same for 2D "para" flavor!')
    
    # get the reduced masses
    mRcom = sum(masses)
    mdist = mRcom / 4.
    
    print '\tusing Rcom reduced mass {}'.format(mRcom)
    print '\tusing dist reduced mass {}'.format(mdist)

    # create a fake atoms object
    atoms = ase.Atoms('XX', masses = [mRcom, mdist])

    # x is the free variable... for both fake atoms
    atoms.set_constraint([FixCartesian(i, mask = (0,1,1)) for i in range(2)])

    # shut up, guys
    atomsEq = AtomsEq(atoms)

    return atomsEq
    

        

# !!!!!
# This routine is deprecated since v 2.3.2!
# !!!!
#def create_aseAtomsEq(**kwargs):
#    """
#    Optimize structure on interpolated potential
#    """
#    raise DeprecationWarning("""Function `create_aseAtomsEq` deprecated since v2.3.2!
#Rather use `minimize_atoms` from MDsimv3.tools.minimize!""")
#    return None
