# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

# unit conversion and tools
from MDsim import units
from MDsim import tools

# copying
import copy


class System(object):
    """
    This class represents the system that is supposed to be propagated

    Initialization
    --------------
    positions        : array
                       (Natoms x Ndof) array containing the initial positions of
                       the system.

    velocities       : array
                       (Natoms x Ndof) array containing the initial velocities
                       of the system.

    masses           : array
                       (Natoms x 1) array containing the masses of the
                       respective species.

    species          : array/list/tuple
                       (Natoms x 1) array containing the names/identifier of
                       each species as strings

    force_function   : Forces instance/callable object
                       Instance of the Forces class or any subclass derived
                       therefrom. Upon calling with a flattened position array,
                       this function must be able to return an (Natoms * Ndof x
                       1) forces array in (a.u.). Additionally, it must provide
                       a method get_Epots() that returns the the potential
                       energy (in a.u.) within each coordinate upon calling it
                       with a flattened (Natoms * Ndof x 1) position array (in
                       a.u.).

    eta_function     : EtaMatrix* instance/callable object
                       Instance of one of the EtaMatrix subclassses. This
                       instance must return an (Natoms * Natoms x 1) array of
                       friction coefficients (in a.u.) upon calling it with an
                       (Natoms * Ndof x 1) positions array (in a.u.).

    convert          : Boolean (default = True)
                       If True, convert the input arrays as follows:
                       positions  : A      --> a.u
                       velocities : A / fs --> a.u.
                       masses     : M      --> a.u.
                       Internally, this program handles everything in atomic
                       units!

    init_etas_and_forces: Boolean (default = True)
                       Inititialize the eta and force matrices. Can be turned
                       off in case subclasses as for instance
                       SurfacOscillatorSystem extend the system dimensionality
                       and thus might crash the corresponding evaluation
                       routines at this point.
    """

    name = 'Generic MD System'

    def __init__(self, positions, velocities, masses, species,
                       force_function,
                       eta_function = None,
                       convert = True,
                       init_etas_and_forces = True):

        # type checks
        positions  = tools.check_for_array(positions)
        velocities = tools.check_for_array(velocities)
        masses     = tools.check_for_array(masses)
        species    = tools.check_for_array(species)
        convert    = tools.check_for_boolean(convert)


        # dimensionality checks
        assert positions.shape == velocities.shape, '{} {}'.format(positions.shape, velocities.shape)
        assert len(positions.shape) == 2
        assert positions.shape[0] == masses.shape[0], '{} {}'.format(positions.shape, masses.shape)
        assert positions.shape[0] == species.shape[0], '{} {}'.format(positions.shape, species.shape)


        # actual constructor
        self.initialized = False
        self.friction = False
        self.convert = convert

        self.species      = species
        self.positions    = positions
        self.masses       = masses
        self.velocities   = velocities

        if convert:
            self.positions  *= units.ANGSTROM_TO_AU
            self.velocities *= units.ANGSTROM_PER_FS_TO_AU
            self.masses     *= units.AMU_TO_AU

        # get the system dimensions
        self.Natoms, self.Ndof = self.positions.shape

        # flatten all arrays
        self.positions  = self.positions.flatten()
        self.velocities = self.velocities.flatten()

        # for convenience (--> reset): store initial conditions
        self.positions_init  = self.positions.copy()
        self.velocities_init = self.velocities.copy()

        # extend masses to diagonal matrix, but save a copy first
        self.masses_red = self.masses.copy()
        self.masses     = tools.extend2Ndof(self.masses, Ndof = self.Ndof)

        # set the force function
        self._set_force_function(force_function)

        # set the eta function
        self._set_eta_function(eta_function)

        # set system time
        self.time = 0.

        # counter for dissipated energy
        self.Ediss = 0

        if init_etas_and_forces:
            self._init_etas_and_forces()
            self.initialized = True

    def __str__(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def set_species(self, species):
        species      = tools.check_for_array(species)
        self.species = species

    def calc_etas(self, pos):
        return self.eta_function(pos)

    def reset(self):
        # reset positions, velocities and time, Ediss
        self.positions  = self.positions_init.copy()
        self.velocities = self.velocities_init.copy()
        self.Ediss      = 0
        self.time       = 0

        # re-initialize forces and etas
        self._init_etas_and_forces()

        print 'System reset'


    def _set_force_function(self, force_function):
        # store the force function, one time as callable object, one time as
        # interaction object to evaluate energies. This is only for convenience
        self.interaction = force_function
        self.calc_forces = force_function

    def _set_eta_function(self, eta_function):
        if eta_function == None:
            # if no eta function specified, create a dummy instance
            # this is just implemented for convenience, the dummy will always
            # return zero friction.

            from MDsim.electronicfriction import EtaMatrixDummy
            self.eta_function = EtaMatrixDummy()

            # actually the integrator reads this flag. If false, it will not
            # call the eta function...
            self.friction = False

        else:
            self.eta_function = eta_function
            # Make sure that dimensions match!
            # This is crucial while expanding the eta scalars...
            # has become unnecessart since eta function also carries atomsEq
            # istance as of version > 2.4
            #self.eta_function.set_Ndof(self.Ndof)
            self.friction = True


    def _init_etas_and_forces(self):
        # initialize eta matrix
        self.etas = self.calc_etas(self.positions)
        # initialize forces
        self.forces = self.calc_forces(self.positions)


    def change_force_function(self, force_func):
        self._set_force_function(force_function)

        print 'changed force function to:'
        print str(self.interaction)

        self.reset()

    def change_eta_function(self, eta_function):
        self._set_eta_function(self, eta_function)
        print 'changed eta function to:'
        print str(self.eta_function)

        self.reset()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # The public methods return the value (a copy) in proper units          #
    # main purpose is interfacing to IO routines                            #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def get_positions(self):
        return self.positions.copy() * units.AU_TO_ANGSTROM

    def get_velocities(self):
        return self.velocities.copy() * units.AU_TO_ANGSTROM_PER_FS

    def get_forces(self):
        return self.forces.copy() * units.AU_TO_EV_PER_ANGSTROM

    def get_dimensions(self):
        return self.Natoms, self.Ndof

    def get_time(self):
        return self.time * units.AU_TO_FS

    def get_masses(self):
        return self.masses_red * units.AU_TO_AMU

    def get_species(self):
        return self.species # strings with species names

    def get_etas(self):
        return self.etas.copy()

    def get_friction_forces(self):
        return - self.etas * self.velocities * units.AU_TO_EV_PER_ANGSTROM

    def get_Ekin(self):
        Ekin = np.sum(0.5*self.masses*self.velocities**2)
        return Ekin * units.AU_TO_EV

    def get_Epot(self):
        Epot = self.interaction.get_Epot(self.positions)
        return Epot * units.AU_TO_EV

    def get_Etot(self):
        return self.get_Epot() + self.get_Ekin()

    def get_Ekins(self):
        """
        Kinetic energy of each particle
        """
        Ekins = 0.5*self.masses*self.velocities**2
        return Ekins * units.AU_TO_EV

    def get_Ediss(self):
        """
        Return the amount of energy that has been dissipated so far.
        """
        return self.Ediss * units.AU_TO_EV

    def get_eta_function(self):
        try:
            return copy.deepcopy(self.eta_function)
        except AttributeError:
            return "friction-less system"

    def get_force_function(self):
        return copy.deepcopy(self.interaction)

    def get_dEdt(self, convert = True):
        """
        Calculate the energy dissipation rate at the current system
        configuration according to Rayleigh's dissipation function. See my
        master's thesis or the Landau/Lifschitz for details.
        """

        dEdt = -2 * np.sum(0.5 * self.etas * self.velocities**2)
        if convert:
           dEdt *= units.AU_TO_EV_PER_FS

        return dEdt

    def get_Ndof(self):
        return self.Ndof

    def get_Natoms(self):
        return self.Natoms

    def get_Ndim(self):
        return self.Ndof * self.Natoms
