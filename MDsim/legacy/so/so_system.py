# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from scipy.constants import physical_constants

import MDsimv3.units as units
from MDsimv3.systems import NormalmodeSystem
from MDsimv3.electronicfriction import Eta

class SOSystem(NormalmodeSystem):
    """
    Subclass derived from NormalmodeSystem. It extends the NormalmodeSystem by
    a surface oscillator (SO) mimiking the phononic contributions. In order to
    do so, additional ghost coordinates are introduced and appended to the
    ``real'' system coordinates.
    
    Initialization
    --------------
    atoms_eq         : AtomsEq instance
                       Directly passed to parent class, see docstring there.
    
    normalmodes      : Normalmodes instance
                       Directly passed to parent class, see docstring there.

    init_quanta      : list/array/tuple
                       Directly passed to parent class, see docstring there.

    force_function   : SurfaceOscillatorForces instance/callable object
                       This is not just a normal Forces instance but one that
                       can implements the surface oscillator model. You can NOT
                       simply calculate a gradient from a potential in this
                       model but need to distinguish between ``real''
                       coordinates and ghost coordinates. Use the
                       SurfaceOscillatorForces class.

    eta_function     : EtaMD instance/callable object
                       Directly passed to parent class, see docstring there.
                       However, the respective call ``calc_eta'' will be
                       modified, such that only the real coordinates enter the
                       actual eta_function. This ensures the highest
                       probability of portability. To add friction to the SO,
                       see the argument ``surface_eta'' 

    surface_temp     : float
                       Temperature of the surface in Kelvin. The corresponding
                       thermal energy will be assigned to the SO in terms of
                       kinetic energy.

    surface_eta      : float
                       Additional constant friction contribution to the SO.
    """
    def __init__(self, atoms_eq,
                       normalmodes,
                       init_quanta,
                       force_function,
                       eta_function = None,
                       surface_temp = 0, # Kelvin
                       surface_eta  = 0
                       ):


    
        NormalmodeSystem.__init__(self,
                                  atoms_eq,
                                  normalmodes,
                                  init_quanta,
                                  force_function,
                                  eta_function,
                                  init_etas_and_forces = False)

        #
        # So far, we have a standard NormalmodeSystem... Let's create the
        # surface oscillator
        #

        # Dimensionality of the SO
        self.surface_Ndim = force_function.get_surface_Ndim()
        
        # get the position-dividing routine
        self._divide_positions = force_function._divide_positions
        
        # position and velocity vector of the SO
        surface_pos = np.zeros((self.surface_Ndim))
        surface_vel = np.zeros((self.surface_Ndim))

        # frequencies and mass of the SO
        self.surface_freq = force_function.get_surface_frequencies()
        self.surface_mass = force_function.get_surface_mass()
        self.surface_element = force_function.get_surface_element()
        
        # friction on surface element
        self.surface_eta = surface_eta

        # thermal energy
        self.surface_temp = surface_temp
        kT = physical_constants['Boltzmann constant in eV/K'][0] * surface_temp
        kT *= units.EV_TO_AU
        
        # distribute thermal energy
        for i in range(self.surface_Ndim):
            v = np.sqrt(2. * kT / self.surface_mass)
            surface_vel[i] = v 
    
        # extend positions and velocity vector of the entire system
        self.positions = np.concatenate((self.positions, surface_pos))
        self.velocities = np.concatenate((self.velocities, surface_vel))
        
        # for reset purposes
        self.positions_init  = self.positions.copy()
        self.velocities_init = self.velocities.copy()

        # further extendnd masses and species
        self.masses = np.concatenate((self.masses, [self.surface_mass]*self.surface_Ndim))
        self.masses_red = np.concatenate((self.masses_red, [self.surface_mass]))
        self.species = np.concatenate((self.species, ['{} (SO model)'.format(self.surface_element)]))
       
        # properly initialize forces and etas, now with correct dimensions
        self._init_etas_and_forces()

        # lastly, extend the mask 
        self._mask = np.concatenate((self._mask, np.zeros(self.surface_Ndim, dtype = bool))) 
        
        # extend the Natoms counter
        self.Natoms += 1

        self.inititalized = True
    
    
    def calc_etas(self, positions):
        # this function has to be adjusted in order not to mess with all eta instances...
        positions, surface_pos = self._divide_positions(positions)
        etas = self.eta_function(positions)
        etas = np.concatenate((etas, [self.surface_eta]*self.surface_Ndim)) 
        return etas
    
    def get_surface_Ndim(self):
        return self.surface_Ndim

    def get_surface_frequencies(self):
        return self.force_function.get_surface_frequencies() / 1000. * units.AU_TO_EV

    def __str__(self):
        name = 'SurfaceOscillatorSystem (T = {}K)'.format(self.surface_temp)
        return name
