# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import warnings

from ase.data import atomic_masses, atomic_numbers

import MDsimv3.units as units
import MDsimv3.tools as tools

class SOForces(object):
    """
    Modified potential energy surface to implement the surface oscillator (SO)
    model.

        V({R_i},R_S) = V({R_i - R_S}) + m_S / 2 sum (omega_n**2 R_S_n**2)
    
    This means, we shift the PES according to the surface movement and treat
    the latter as a harmonic oscillator. For more information see e.g.

        H. F. Busnengo et al., Phys. Rev. Lett. 87, 127601 (2001). 
        http://www.dx.doi.org/10.1103/PhysRevLett.87.12760

    This object is to be used like a force instance during the simulations in
    connection with the SurfaceOscillatorSystem class. Forces are evaluted
    semi-analytical, semi-numerical using central differences.


    Intialization
    -------------
    potential  :  Any callable object/function corresponding to a potential.
                  This instance must take the ``real'' coordinate system used
                  in the MD simulation as input (in a.u.), i.e. without the
                  ghost coordinate, and return a potential value (in a.u.).

    surface_element : String
                  Element the surface consists of. Will be used to lookup the
                  respective mass from the ase.data dictionaries.

    surface_freqs : Array
                  Frequencies of the SO in x, y, and z direction in
                  MILLIelectronvolt. At the same time, the number of
                  frequencies given determines the dimensionality of the SO.

    stepwidth  :  float (default = 0.01)
                  Stepwidth while evaluating the gradient:
                    
                    F_i = -(potential(pos_i+d) - potential(pos_i-d)) / (2*d)
                  
                  There is yet no possibility to apply adaptive stepwidths
                  and/or different stepwidths for different coordinates. But
                  feel free to implement whatever you need.
    
    convert    : Boolean (default = True)
                 Converts frequency input from meV --> a.u. If set False, you
                 will have to provide a.u. input.
    
    Call
    ----
    positions  :  (Natoms*Ndof + Ndim_surface x 1) array
                  Position and ghost coordinate at which to evaluate the force.
                  Internally, the positions vector will be split into ``real''
                  and ghost coordinates to evaluate the respective forces on
                  them.

    d          :  float (default = None)
                  Stepwidth as above. If not specified, the member variable
                  will be used. But there might be case where one wants to
                  chose d for every step...
    
    Returns
    -------
    force      :  (Natoms*Ndof + Ndim_surface x 1) array
                  Forces at the specified position (in a.u., if potential is
                  constructed as specified above)
    
    NOTE: There is a method get_Epot() which pipes the potential evaluation,
          and if available, there will also be get_Epots_normalmodes().
    """

    def __init__(self, 
                 potential, 
                 surface_element, 
                 surface_freqs, # meV 
                 stepwidth = 0.001,
                 convert = True):
                 
        self.potential = potential
        
        self.surface_element = surface_element
        
        self.surface_mass = atomic_masses[atomic_numbers[surface_element]]
        self.surface_mass *= units.AMU_TO_AU

        self.surface_freqs = tools.check_for_array(surface_freqs)

        # oscillate around zero by default
        # but flexibility to change it. Wathc out for possible unit
        # conversations that might be necessary...
        self.surface_pos_eq = np.zeros_like(self.surface_freqs)

        if convert:
            self.surface_freqs *= units.EV_TO_AU / 1000. # input in meV.

        # number of surface degrees of freedom
        self.surface_Ndim = len(self.surface_freqs)
        self.system_Ndim = self.potential.get_Ndim()
        
        self.stepwidth = stepwidth
        
        # pipe the normalmode decomposition of potential energy if possible
        try:
            self.get_Epots_normalmodes = self.potential.get_Epots_normalmodes
        except AttributeError:
            pass


    def __str__(self):
        name  = 'Surface oscillator forces using central differences (stepwidth = {})'.format(self.stepwidth)
        name += '\n\tUnderlying potential:'
        name += '\n\t\t{}'.format(str(self.potential).replace('\n','\n\t\t'))
        name += '\n\tSurface element: {} (m = {} amu)'.format(self.surface_element, 
                                                              self.surface_mass * units.AU_TO_AMU)
        name += '\n\tSurface frequencies:'
        for i,f in enumerate(self.surface_freqs):
            name += '\n\t\tomega_{1:d} = {0:5.1f} meV = {2:5.1f} cm^-1'.format(f*1000*units.AU_TO_EV, 
                                                                               i, 
                                                                               f*units.AU_TO_KAYSER)
        return name

    def _divide_positions(self, positions):
        """
        Divide into ``real'' and artificial DOF.
        We assume, that the artificial DOF, i.e. those of the surface
        oscillator are the very last ones.
        """
        return positions[:-self.surface_Ndim], positions[-self.surface_Ndim:]
    #
    # individual potential energy contributions
    #

    def _harmonic_surface_oscillator(self, surface_pos):
        Epot_surf = np.sum((surface_pos-self.surface_pos_eq)**2 * self.surface_freqs**2) * 0.5 * self.surface_mass
        return Epot_surf

    def _shifted_pes(self, positions, surface_pos):
        shifted_pos = (positions.reshape(-1, self.surface_Ndim) + surface_pos).flatten()
        return self.potential(shifted_pos)

    def _get_potential_energy(self, positions, surface_pos):
        return self._shifted_pes(positions, surface_pos) + self._harmonic_surface_oscillator(surface_pos)        


    def get_Epot(self, positions):
        positions, surface_pos = self._divide_positions(positions)
        return self._get_potential_energy(positions, surface_pos)        


    #
    # calculating forces
    #

    def __call__(self, positions, d = None):
        # d is the finite difference step size
        if d == None:
            d = self.stepwidth
        
        
        positions, surface_pos = self._divide_positions(positions)
        
        force = np.empty_like(positions)
        
        #
        # Evaluate the forces on the atoms using central differences
        #
        for i, p in enumerate(positions):
            disp  = positions.copy()
            disp_ = positions.copy()
            disp[i]  += d
            disp_[i] -= d
            force[i] = -(self._get_potential_energy(disp, surface_pos) 
                         - self._get_potential_energy(disp_ ,surface_pos)) / (2*d)  
            if force[i] == np.nan:
                raise RuntimeWarning('Seems you are running out of your fitted region')
                force[i] = 0.

        #
        # evaluate the forces on the surface oscillator
        #
        surface_force = np.empty_like(surface_pos)
        for j, s in enumerate(surface_pos):
            disp  = surface_pos.copy()
            disp_ = surface_pos.copy()
            disp[j]  += d
            disp_[j] -= d
            surface_force[j] += -(self._get_potential_energy(positions, disp) 
                                  - self._get_potential_energy(positions, disp_)) / (2*d)  
        return np.concatenate((force, surface_force))
    
    def get_surface_Ndim(self):
        return self.surface_Ndim

    def get_system_Ndim(self):
        return self.system_Ndim

    def get_surface_frequencies(self):
        return self.surface_freqs.copy()

    def get_surface_mass(self):
        return self.surface_mass

    def get_surface_element(self):
        return self.surface_element
