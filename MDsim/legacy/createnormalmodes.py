# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import sys
import os
import StringIO
import glob

from ase.vibrations import Vibrations

from MDsimv3.tools                            import check_kwargs 
from MDsimv3.normalmodes                      import CastepNormalmodes
from MDsimv3.normalmodes                      import ASENormalmodes
from MDsimv3.tools.createobjects              import create_customCalculator
# -----------------------------------------------------------------------------
# NORMALMODES
# -----------------------------------------------------------------------------

def create_castepNormalmodes(atomsEq, **kwargs):

    param = {'datapath'   : 'data',
             'phononfile' : None,
             'mode_names' : None}

    print 'creating CastepNormalmodes instance'
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)

    if param['phononfile'] == None:
        phononfile = glob.glob(os.path.join(param['datapath'],'*.phonon'))[0]
    else:
        phononfile = param['phononfile']

    print '\tusing file {}'.format(phononfile)

    castepNormalmodes = CastepNormalmodes(
                            atoms_eq = atomsEq,
                            phononfile  = phononfile, 
                            mode_names = param['mode_names'])

    return castepNormalmodes


def create_aseNormalmodes(atomsEq, potential_obj, **kwargs):
    
    param = {'mode_names' : None,
             'delta'      : 0.01,
             'forces'     : 'numerical',
             'verbose'    : False,
             'forces_stepwidth' : 0.01}
    
    if check_kwargs(kwargs, param):
        param.update(kwargs)

    print 'creating ASENormalmodes instance'
    
    calculator = create_customCalculator(atomsEq = atomsEq,
                                         potential_obj = potential_obj,
                                         forces = param['forces'],
                                         forces_stepwidth = param['forces_stepwidth'])

    # get atoms and attach calculator
    atoms = atomsEq.get_full_atoms_obj()
    atoms.set_calculator(calculator)

    print '\trunning vibrational analysis on numerical potential'
    
    if not param['verbose']:
        # shut up, guys
        stdout = sys.stdout
        sys.stdout = StringIO.StringIO()
    
    vib_obj = Vibrations(atoms, delta = param['delta'])
    vib_obj.run()
    vib_obj.read()
    vib_obj.clean()
    
    if not param['verbose']:
        # ok, now you may talk again
        sys.stdout = stdout
    
    aseNormalmodes = ASENormalmodes(atomsEq, vib_obj,
                                    mode_names = param['mode_names'])
   
    energies = aseNormalmodes.get_energies()
    print '\t\tobtained the following energies'
    for i, e in enumerate(energies):
        print '\t\t\tmode_{0:d} : {1:.3f} meV'.format(i,e*1000)

    return aseNormalmodes
