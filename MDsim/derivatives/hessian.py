# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Numerically evaluate a Hessian. Rely on numdifftools if possible
"""
from __future__ import print_function

import numpy as np
from MDsim import units
from MDsim.derivatives.gradient import gradient_central_differences

try:
    import numdifftools as nd
    _nd = True
except ImportError:
    _nd = False

def hessian_central_differences(func, positions, d):
    """
    Routine to evaluate a Hessian using finite central differences. Becomes very
    unfeasable for large system though it may be somewhat useful for small and
    low-dimensional systems.

    Parameters
    ----------
    func : Callable
        Function for which the Hessian should be evaluated. This is very
        generic, you can for instance also pass system.get_potential_energy
        here.

        Required interface is

            ``func(positions)``

        and <positions> is expected in a.u., as well as the return value.


    positions : (Natoms, Ndim) array
        Positions at which to evaluate the forces. Should in principle be
        rather C than Fortran contigious.

    d : float
        The stepwidth for the central differences. Note that the distance
        between the two points will be 2*d.

    Returns
    -------
    hessian : (Natoms, Ndim, Natoms, Ndim) array
        The hessian at the respective positions. This array is C-contigious.
        According to MDsim conventions, units are a.u.
    """

    Natoms, Ndim = positions.shape


    if _nd:
        _func = lambda x: func(x.reshape(Natoms, Ndim))
        hessian = nd.Hessian(_func, step=d)(positions.ravel())
        hessian.reshape(Natoms, Ndim, Natoms, Ndim)
    else:
        # this is our working array, hence a copy
        _positions = positions.copy()

        # Hessian_ij = d^2 E / dR_idRj = dF_i / dR_j
        # symmetric by construction!
        Ndof = Ndim * Natoms
        hessian = np.empty((Ndof,Ndof))

        # dimensions counter
        c = 0

        for iatom in range(Natoms):
            for idim in range(Ndim):
                # positive displacement
                _positions[iatom, idim] = positions[iatom, idim] + d
                fplus = gradient_central_differences(func, _positions, d=d).flatten()

                # negative displacement
                _positions[iatom, idim] = positions[iatom, idim] - d
                fminus = gradient_central_differences(func, _positions, d).flatten()

                # restore 'equilibrium' positions
                _positions[iatom, idim] = positions[iatom, idim]

                # note that forces are negative gradients...
                hessian[c,:] = (fplus - fminus) / (2.*d)

                c+=1
    hessian.reshape(Natoms, Ndim, Natoms, Ndim)

    return hessian
