# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Numerically evaluate a gradient. Rely on numdifftools if possible
"""

from __future__ import print_function

import numpy as np

try:
    import numdifftools as nd
    _nd = True
except ImportError:
    _nd = False

def gradient_central_differences(func, positions, d):
    """
    Routine to evaluate a gradient using finite central differences. Becomes very
    unfeasable for large system though it may be somewhat useful for small and
    low-dimensional systems.

    Parameters
    ----------
    func : Callable
        Function for which the gradient should be evaluated. This is very
        generic, you can for instance also pass system.get_potential_energy
        here.

        Required interface is

            ``func(positions)``

        and <positions> is expected in a.u., as well as the return value.


    positions : (Natoms, Ndim) array
        Positions at which to evaluate the forces. Should in principle be
        rather C than Fortran contigious.

    d : float
        The stepwidth for the central differences. Note that the distance
        between the two points will be 2*d.

    Returns
    -------
    gradient : (Natoms, Ndim) array
        The gradient at the respective positions. This array is C-contigious.
        According to MDsim conventions, units are a.u.
    """

    Natoms, Ndim = positions.shape

    if _nd:
        # if we have the numdifftools, use them!
        _func = lambda x: func(x.reshape(Natoms, Ndim))
        gradient = nd.Gradient(_func, step=d)(positions.ravel())
        gradient = gradient.reshape(Natoms, Ndim)

    else:
        gradient = np.empty((Natoms, Ndim), dtype = np.float64, order = 'C')
        tmp = positions.copy()
        for i in range(Natoms):
            for j in range(Ndim):
                # the positive displacement
                tmp[i,j] += d

                # no need to store several numbers
                df = func(tmp)

                # the negative displacement
                tmp[i,j] -= 2*d
                df -= func(tmp)

                # the negative gradient
                gradient[i,j] = (df / (2.*d))

                # reset tmp
                tmp[i,j] += d

    return gradient

