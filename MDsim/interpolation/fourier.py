# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

from numpy import linalg

from MDsim.speedup import plainpython

try:
    from MDsim.speedup import f90support
    _f90 = True
except ImportError:
    _f90 = False

try:
    from MDsim.speedup import numbasupport
    _numba = True
except ImportError:
    _numba = False

try:
    from MDsim.speedup import cythonsupport
    _cython = True
except ImportError:
    _cython = False


def fit_fourier_expansion(G, H, points, values, real_order, imag_order, constant=True):
    """
    Routine to fit the a Fourier expansion of given real and imaginary order to
    a set of point-value pairs. Either we will have an exact interpolation, if
    <real_order> + <imag_order> = len(values) or a least-squares approximation,
    if <real_order> + <imag_order> < len(values).

    Parameters
    ----------
    G : (Nlattice_G, Ndim) array
        Numpy array containing the reciprocal lattice vectors for the real part
        of the Fourier expansion, ie. the cosine terms. Please, do note the
        dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    H : (Nlattice_H, Ndim) array
        Numpy array containing the reciprocal lattice vectors for the imaginary
        part of the Fourier expansion, ie. the sine terms. Please, do note
        the dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    points : (Npoints, Ndim) array
        Numpy array containing all points that shall enter the interpolation or
        least square approximation of the potential.

    values : (Npoints) array
        Vaules corresponding to the <points>.

    real_order : integer
        Order of the Fourier expansion in cosine contributions, ie. the real
        part.

    imag_order : integer
        Order of the Fourier expansions in sine contributions, ie. the
        imaginary part.

    constant : Boolean, optional (default = True)
        Flag wether a constant m=n=0 amplitude shall be fitted or not. Note
        that this functionality requires at least one more independent input
        point.


    Returns
    -------
    coeff : dictionary
        Required fitted Fourier coefficients. The dictionary contains the
        following keys:

        * 'const' : The amplitude of the constant contribution. Will be <None>
                    if <constant> = False as input parameter.

        * 'real_coeff' : (real_order, 1) array with the respective real-part
                          amplitudes.

        * 'imag_coeff' : (imag_order, 1) array with the respective imaginary-part
                          amplitudes.

        Note that this dictionary can directly be used as as an expanded kwargs
        dict to initialize the coefficients of the FourierExpansion class.

    fit_type : string
        Some eyecandy I/O regarding the type of fit.
    """

    # dimensionality check
    if not np.all([i == G.shape[1] for i in [points.shape[1],
                                             H.shape[1]]]):
        print('Error : Inconsistent input dimensionality')
        raise ValueError

    # number of required points (including the constant term)
    ncoeff = real_order + imag_order
    if constant:
        ncoeff += 1

    # sanity check
    npoints = len(points)
    if  npoints < ncoeff:
        print('Error: Not enough datapoints!')
        print('       You passed {} points, but we need at least {}.'.format(npoint, ncoeff))
        raise ValueError

    else:
        # the coefficients matrix
        A = np.zeros((npoints, ncoeff))

        # do it loop-wise here (not performance critical)
        for i, point in enumerate(points):
            for n in range(real_order):
                A[i,n] = np.sum(np.cos((n + 1) * np.dot(G, point)))
            for m in range(imag_order):
                A[i,m+real_order] = np.sum(np.sin((m + 1) * np.dot(H, point)))

        if constant:
            A[:,-1] = 1

        if npoints == ncoeff:
            x = linalg.solve(A, values)

            fit_type = 'Exact fit through {} points'.format(npoints)

        elif npoints > ncoeff:
            x, sumofsquares = linalg.lstsq(A, values)[0:2]

            RMSD = np.sqrt(sumofsquares / npoints)
            fit_type  = 'Least square fit'
            fit_type += '\n\tNumber of variables                    : {}'.format(ncoeff)
            fit_type += '\n\tNumber of points                       : {}'.format(npoints)
            fit_type += '\n\tRMSD (units of interpolation function) : {0:<12.6f}'.format(*RMSD)

        # chunk the solutions array again:
        if constant:
            const = np.float64(x[-1])
            x = x[:-1]
        else:
            const = None

        real_coeff = np.array(x[0:real_order], dtype=np.float64, order='C')
        imag_coeff = np.array(x[real_order:], dtype=np.float64, order='C')

        coeff = {'const': const,
                 'real_coeff' : real_coeff,
                 'imag_coeff' : imag_coeff
                }

        return coeff, fit_type


class FourierExpansion(object):
    """
    Class that evaluates a given Fourier expansion. The evaluation is done on a
    highly efficient F90/C level if available.

    Initialization
    --------------
    G : (Nlattice_G, Ndim) array
        Numpy array containing the reciprocal lattice vectors for the real part
        of the Fourier expansion, ie. the cosine terms. Please, do note the
        dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    H : (Nlattice_H, Ndim) array
        Numpy array containing the reciprocal lattice vectors for the imaginary
        part of the Fourier expansion, ie. the sine terms. Please, do note
        the dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    real_coeff : (real_order, 1) array, optional (default = None)
        The amplitudes of real coefficients. The respective length of this
        array gives the order of your expansion. If you do not have the
        coefficients yet, the object may be initialized but the internal flag
        <self._initialized> = False. Set an empty matrix if you do not want any
        real coefficients.

    imag_coeff : (real_order, 1) array, optional (default = None)
        The amplitudes of imaginary coefficients. The respective length of this
        array gives the order of your expansion. If you do not have the
        coefficients yet, the object may be initialized but the internal flag
        <self._initialized> = False. Set an empty matrix if you do not want any
        imaginary coefficients.

    const : float, optional (default = None)
        Amplitude of the constant contribution to the Fourier expansion. Set
        None if you do not want any.

    fit_type : string, optional (default = None)
        Just some eyecandy. Basically, this should contain some information
        about how the coefficients were obtained. If not passed, nothing will
        be printed.

    data_origin: string, optional (default = None)
        If you want information stored on the object from where your data
        underlying the fit actually come from. If not passed, nothing will be
        printed.

    Notable member methods/variables
    --------------------------------
    init_coefficients() : Function that initializes also the coefficients of
                          the respective Fourier expansion.

    get_value() : Returns the value of the respective expansion. Note that this
                  routine is vectorized. For details see docstring there.

    get_gradient() : Returns the gradient of the respective expansion. Note,
                     that also this routine is vectorized. For details see
                     docstring there.

    _name : Internal string representation of this object. You may want to
            change it for derived classes *after* initializing the parent class.
    """

    _implemented_support = ['fortran',
                            'cython',
                            'numba',
                            'fallback']

    if _f90:
        _support = 'fortran'
    elif _cython:
        _support = 'cython'
    elif _numba:
        _support = 'numba'
    else:
        _support = 'fallback'


    def __init__(self, G, H, real_coeff=None,
                 imag_coeff=None, const=None,
                 fit_type=None, data_origin=None):
        # note that g and h are (Nlattice x Ndim)
        self.G = np.array(G, dtype=np.float64, order='C')
        self.H = np.array(H, dtype=np.float64, order='C')

        # get the dimensionality
        self.Nlattice, self.Ndim = self.G.shape

        # dimensionality check
        if not np.all(self.G.shape == self.H.shape):
            print('Error : Inconsistent input dimensionality')
            raise ValueError

        # to conveniently call the f90 routines
        # they are called with slightly different arguments, hence we create two dictionaries
        # note that we require the transpose for fast memory access (column
        # major, see the f90 file)
        self._F90coeff_value = {'g' : np.array(G.T, dtype=np.float64, order='F'),
                                'h' : np.array(H.T, dtype=np.float64, order='F')}
        self._F90coeff_gradient = self._F90coeff_value.copy()

        # same for the plain python routine
        self._PYcoeff_value = {'G' : self.G,
                               'H' : self.H}
        self._PYcoeff_gradient = self._PYcoeff_value.copy()


        # some io eyecandy
        self._name = 'Generic Fourier Expansion'
        self._fit_type = fit_type
        self._data_origin = data_origin

        self.initialized  = False

        if all([i is not None for i in (real_coeff, imag_coeff)]):
            self._init_coefficients(real_coeff, imag_coeff, const)


    def _init_coefficients(self, real_coeff, imag_coeff, const=None):
        """
        Routine to initialize the coefficients of the Fourier expansion.
        If len(coeff) is odd, then the *last* coefficient will be interpreted
        as constant coefficient.

        Parameters
        ----------
        real_coeff : (real_order, 1) array, optional (default = None)
            The amplitudes of real coefficients. The respective length of this
            array gives the order of your expansion. If you do not have the
            coefficients yet, the object may be initialized but the internal
            flag <self._initialized> = False. Set an empty matrix if you do not
            want any real coefficients.

        imag_coeff : (real_order, 1) array, optional (default = None)
            The amplitudes of imaginary coefficients. The respective length of
            this array gives the order of your expansion. If you do not have
            the coefficients yet, the object may be initialized but the
            internal flag <self._initialized> = False. Set an empty matrix if
            you do not want any imaginary coefficients.

        const : float, optional (default = None)
            Amplitude of the constant contribution to the Fourier expansion.
            Set None if you do not want any. It will then be initialized with 0.


        Returns
        -------
        None
        """

        self.real_coeff = np.array(real_coeff, dtype=np.float64, order='C')
        self.imag_coeff = np.array(imag_coeff, dtype=np.float64, order='C')

        if const:
            self.const = const
        else:
            self.const = 0.

        self.real_order = len(self.real_coeff)
        self.imag_order = len(self.imag_coeff)

        self.real_ns = np.arange(1, self.real_order + 1, dtype=np.int32)
        self.imag_ms = np.arange(1, self.imag_order + 1, dtype=np.int32)

        self._F90coeff_value.update({'constant'   : self.const,
                                     'real_coeff' : np.array(self.real_coeff, dtype=np.float64, order='F'),
                                     'imag_coeff' : np.array(self.imag_coeff, dtype=np.float64, order='F')
                                    })

        # the coefficients for the gradient may not include 'constant'
        self._F90coeff_gradient = self._F90coeff_value.copy()
        self._F90coeff_gradient.pop('constant')

        self._PYcoeff_value.update({'constant'   : self.const,
                                    'real_coeff' : self.real_coeff,
                                    'imag_coeff' : self.imag_coeff,
                                    })

        self._PYcoeff_gradient = self._PYcoeff_value.copy()
        self._PYcoeff_gradient.pop('constant')

        self._initialized = True

        return None


    def _create_infostring(self):
        """
        Function that creates some eyecandy I/O.
        """
        info = '{}'.format(self._name)
        info += '\n'
        info += '\nReciprocal lattice vectors (a.u.):'
        info += '\n----------------------------------'
        info += '\nG (real part of Fourier series)'
        for i, Gi in enumerate(self.G):
            info += '\n\tG_{} : {}'.format(i, Gi)
        info += '\nH (imaginary part of Fourier series)'
        for i, Hi in enumerate(self.H):
            info += '\n\tH_{} : {}'.format(i, Hi)

        info += '\n'
        info += '\nFourier coefficients (a.u.):'
        info += '\n----------------------------'

        if self._initialized:
            _maxlen = int(np.log10(max([self.real_order, self.imag_order])))
            if self.const is not None:
                info += '\nConstant'
                info += '\n\tC {{0:<{0:d}s}} = {{1:+.3e}}'.format(_maxlen).format(' ', self.const)
            if self.real_order > 0:
                info += '\nReal coefficients'
            for n, An in enumerate(self.real_coeff):
                info +='\n\tA_{{0:<{0:d}d}} = {{1:+.3e}}'.format(_maxlen).format(n+1, An)
            if self.imag_order > 0:
                info += '\nImaginary coefficients'
            for m, Bm in enumerate(self.imag_coeff):
                info +='\n\tB_{{0:<{0:d}d}} = {{1:+.3e}}'.format(_maxlen).format(m+1, Bm)
            if self._fit_type:
                info += '\n'
                info += '\nType of coefficients fit'
                info += '\n------------------------'
                info += '\n'
                info += self._fit_type
            if self._data_origin:
                info += '\n'
                info += '\nOrigin of underlying data points'
                info += '\n---------------------------------'
                info += '\n\t'
                info += self._data_origin

        else:
            info += '\n!!!!!!!!!!!!!!!!!!!!!!'
            info += '\n! NOT YET INITIALIZED!'
            info += '\n!!!!!!!!!!!!!!!!!!!!!!'
        return info


    def __str__(self):
        return self._create_infostring()


    def get_value(self, positions):
        """
        Evaluate the expansion at given positions.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If available,
            high-performace f90 extensions will be used. Fallback is some
            numpy-broadcasting implementation, which on average is about 50%
            slower -- which is not too bad (see timings below).

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions. This array is
            Fortran-contigioues, yet it does not matter for 1D arrays.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.

        Note
        ----
        Timings on MacbookAir (1.4 GHz Intel Core i5, 8 GB 1600 MHz DDR3)
         -----------------------------------------------------------------
        |                      |                  Natoms                  |
        | function             |     10       100       1000     10000    |
         -----------------------------------------------------------------
        | _python_call()       |  185.40us    1.81ms   18.18ms  180.56ms  |
        | _numpy_call()        |   26.21us   51.64us  303.53us    3.81ms  |
        | _fortran_call()      |    4.85us   20.58us  186.67us    1.90ms  |
        | _numba_call()        |    4.61us   18.75us  164.45us    1.59ms  |
        | _cython_call()       |    6.92us   21.48us  168.12us    1.61ms  |
         -----------------------------------------------------------------
        """

        if self._initialized:
            if self._support == 'fortran':
                return self._fortran_call(positions)
            elif self._support == 'cython':
                return self._cython_call(positions)
            elif self._support == 'numba':
                return self._numba_call(positions)
            elif self._support == 'fallback':
                return self._numpy_call(positions)
        else:
            print('Error : You are trying to evaluate a non-initialized Fourier')
            print('        expansion')
            raise ValueError


    def get_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.

        Note
        ----
        Timings on MacbookAir (1.4 GHz Intel Core i5, 8 GB 1600 MHz DDR3)
         -----------------------------------------------------------------
        |                      |                  Natoms                  |
        | function             |     10       100       1000     10000    |
         -----------------------------------------------------------------
        | _python_gradient()   |  442.49us    4.41ms   43.28ms  457.17ms  |
        | _numpy_gradient()    |   60.00us  147.06us  921.41us    9.47ms  |
        | _fortran_gradient()  |    6.55us   26.20us  221.06us    2.22ms  |
        | _numba_gradient()    |    5.97us   35.53us  287.99us    2.81ms  |
        | _cython_gradient()   |    7.57us   29.69us  258.92us    2.52ms  |
         -----------------------------------------------------------------
        """
        if self._initialized:
            if self._support == 'fortran':
                return self._fortran_gradient(positions)
            elif self._support == 'cython':
                return self._cython_gradient(positions)
            elif self._support == 'numba':
                return self._numba_gradient(positions)
            elif self._support == 'fallback':
                return self._numpy_gradient(positions)
        else:
            print('Error : You are trying to evaluate a non-initialized Fourier')
            print('        expansion')
            raise ValueError


    def _python_call(self, positions):
        """
        Routine to call the expansion on a Python level. Plain python call.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        return plainpython.fourier.evaluate_fourier_expansion(positions, **self._PYcoeff_value)


    def _numba_call(self, positions):
        """
        Routine to call the expansion on a Python level. Plain python optimized
        with numba's JIT compilation capabilities.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        return numbasupport.fourier.evaluate_fourier_expansion(positions, **self._PYcoeff_value)


    def _numpy_call(self, positions):
        """
        Routine to call the expansion on a Python level. Most optimized with
        numpy but still slower than the F90 counter part.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        values = ( np.sum(np.cos(np.dot(positions, self.G.T)[:,:,None]*self.real_ns)*self.real_coeff[None, None, :], axis=(1,2))
                 + np.sum(np.sin(np.dot(positions, self.H.T)[:,:,None]*self.imag_ms)*self.imag_coeff[None, None, :], axis=(1,2))
                 + self.const)
        return values


    def _fortran_call(self, positions):
        """
        Routine to call the expansion on a Fortran level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions. This array is
            Fortran-contigioues, yet it does not matter for 1D arrays.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        values = np.zeros(len(positions), dtype=np.float64, order='F')
        f90support.fourier.evaluate_fourier_expansion(positions.T, values, **self._F90coeff_value)

        return values

    def _cython_call(self, positions):
        """
        Routine to call the expansion on a Python level. Utilizing Cython
        extension.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        values : (Natoms, ) array
            The values at respective positions.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        return cythonsupport.fourier.evaluate_fourier_expansion(positions, **self._PYcoeff_value)


    def _fortran_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        Fortran level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """

        # gradients is supposed to be (Ndim x Natoms) upon input
        gradients = np.zeros(positions.shape[::-1], dtype=np.float64, order='F')
        f90support.fourier.evaluate_fourier_gradient(positions.T, gradients, **self._F90coeff_gradient)

        # transpose gradients upon output again
        return np.array(gradients.T, dtype=np.float64, order='C')


    def _python_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        pure python level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """

        return plainpython.fourier.evaluate_fourier_gradient(positions, **self._PYcoeff_gradient)


    def _numba_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        JIT compiled numba level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """

        return numbasupport.fourier.evaluate_fourier_gradient(positions, **self._PYcoeff_gradient)


    def _cython_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        cython level.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """

        return cythonsupport.fourier.evaluate_fourier_gradient(positions, **self._PYcoeff_gradient)


    def _numpy_gradient(self, positions):
        """
        Routine to *analytically* evaluate the gradient of the expansion on a
        numpy level involving broadcasting routines and thus C-compiled loops.

        If you need any proof that numpy broadcasting is nice speedwise but
        destroys any readability, have a look at this routine.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Positions at which to evaluate the potential.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            The gradients at respective positions. This array is C-contigious.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """

        return (# the real part
                # this is the sum over the lattice vectors
                np.sum(# this is the sum over the expansion coefficients
                       np.sum(# this nice little thing creates the An*sin(n dotprod) for all n and j
                              -np.sin(np.dot(positions,self.G.T)[:,:,None]*self.real_ns)*self.real_ns[None,None,:]*self.real_coeff[None,None,:],
                                        # this part creates us a gradient out of it (multiply with the lattice vectors before doing the j summation)
                              axis = 2)[:,:,None] * self.G[None,:,:],
                       axis = 1)
                +
                # the imaginary part
                np.sum(# this is the sum over the expansion coefficients
                       np.sum(# this nice little thing creates the An*sin(n dotprod) for all n and j
                              np.cos(np.dot(positions,self.H.T)[:,:,None]*self.imag_ms)*self.imag_ms[None,None,:]*self.imag_coeff[None,None,:],
                                        # this part creates us a gradient out of it (multiply with the lattice vectors before doing the j summation)
                              axis = 2)[:,:,None] * self.H[None,:,:],
                       axis = 1)
                )



    def fit(self, points, values, real_order, imag_order, constant, data_origin=None):
        """
        Routine to fit the a Fourier expansion of given real and imaginary order to
        a set of point-value pairs. Either we will have an exact interpolation, if
        <real_order> + <imag_order> = len(values) or a least-squares approximation,
        if <real_order> + <imag_order> < len(values).

        This is a wrapper routine around the fit_fourier_expansion() method
        from MDsim.interpolation.fourier. It automatically initializes the
        instance with the coefficients fitted.

        Parameters
        ----------
        points : (Npoints x Ndim) array
            Numpy array containing all points that shall enter the interpolation or
            least square approximation of the potential.

        values : (Npoints) array
            Vaules corresponding to the <points>.

        real_order : integer
            Order of the Fourier expansion in cosine contributions, ie. the real
            part.

        imag_order : integer
            Order of the Fourier expansions in sine contributions, ie. the
            imaginary part.

        constant : Boolean, optional (default = True)
            Flag wether a constant m=n=0 amplitude shall be fitted or not. Note
            that this functionality requires at least one more independent input
            point.

        data_origin : string, optional (default = None)
            String specifying the origin of the data (if wanted). Can be useful for
            IO reasons.


        Returns
        -------
        <None>
        """

        coeff, fit_type = fit_fourier_expansion(self.G, self.H, points, values,
                                                real_order, imag_order, constant)
        self._init_coefficients(**coeff)
        self._fit_type = fit_type
        self._data_origin = data_origin
        return None


    def _create_contour_plot(self, npts=300, surf_plot=True):
        """
        Simple function that creates a 2D surface plot of the expansion

        Parameters
        ----------
        npts : integer, optional (default=300)
            Number of points in x and y direction at which to evaluate the
            potential.

        surf_plot : bool, optional (default=True)
            Whether to plot an actual 2D surface plot (or just contour lines).

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'lim' : (2 x 1) array
                    The plot limits in x and y direction.

                'repeats' : int
                    The number of repeating units of the unit cell

                'a' : float
                    The applied lattice constant

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot (if surf_plot = <True>).

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot (if
                    surf_plot = <True>).
        """

        import matplotlib.pyplot as plt
        if self.Ndim == 2:
            # get the range from the inverse of the reciprocal lattice vector
            a = 4 * np.pi / (np.sqrt(3) * linalg.norm(self.G[0]))

            repeats = 2
            lim = repeats * a * np.array([-1, 1])

            # creating the x/y points
            x = np.linspace(lim[0], lim[1], npts)
            y = np.linspace(lim[0], lim[1], npts)
            X, Y = np.meshgrid(x, y)
            points = np.vstack((X.flatten(), Y.flatten())).T

            # evaluating using vectorized call ;)
            Z = self.get_value(points).reshape(npts, npts)

            # plotting
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)

            if surf_plot is True:
                cmap = plt.get_cmap("coolwarm")
                surf = ax.pcolormesh(X, Y, Z, cmap=cmap)
                # color bar
                cbar = plt.colorbar(surf, format="%.6f")
                cbar.set_label(r'f(x,y) (a.u.)')

            cont = ax.contour(X, Y, Z, colors='black')


            # axes limits and labels
            ax.set_xlim(*lim)
            ax.set_ylim(*lim)
            ax.set_xlabel('x (a.u.)')
            ax.set_ylabel('y (a.u.)')

            ax.set_aspect('equal')

            plot_dict = {'a' : a,
                         'lim' : lim,
                         'repeats' : repeats,
                         'ax' : ax,
                         'fig' : fig,
                         'cont' : cont
                        }

            if surf_plot:
                plot_dict.update({'surf' : surf,
                                  'cbar' : cbar})

            return plot_dict

        else:
            print('Error : plotting tools only implemented for 2D expansion')
            raise NotImplementedError


    def show(self, **kwargs):
        import matplotlib.pyplot as plt
        self._create_contour_plot(**kwargs)
        plt.show()


    def _change_support(self, support):
        """
        Change the applied speedup support.

        Parameters
        ----------
        support : string
            One of 'fortran', 'cython', 'numba', 'fallback'
        """
        if support not in self._implemented_support:
            raise NotImplementedError('Support "{}" not implemented'.format(support))

        self._support = support
