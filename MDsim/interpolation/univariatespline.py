# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import scipy

import warnings
from distutils.version import LooseVersion


class UnivariateSpline(object):
    """
    Basis class for several things mapped on a 1D grid using UnivariateSplines
    as provided by scipy. There is no atomic vs. internal routine as so ofar we
    do not want to deal with reduced dimensionalities upon transforming
    coordinates. Hence, see this class as the "atomic" class.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    Note: Do not confuse this class with scipy.interpolate.UnivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    Initialization
    --------------
    points : (Npoints) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'UnivariateSpline', *'InterpolatedUnivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that "UnivariateSpline" is a more general form allowing also
        regression rather than interpolation. See also the "smoothing" argument.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    smoothing : float, optional (default = None)
        Smoothing factor in case of a regression rather than interpolation.
        This arguent is only effectie when going for "UnivariateSpline" as
        interpolation function. For more details, see the corresponding
        documentation.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea. Note that this comes with no extra cost, as the
        underlying SciPy routine does the check anyway.
    """
    def __init__(self,
                 points,
                 values,
                 function='InterpolatedUnivariateSpline',
                 kind='cubic',
                 data_origin='(not available)',
                 smoothing=None,
                 _bounds_check=True):

        # hard coded system dimensionality
        self.Ndim = 1

        if not len(points.shape) == self.Ndim:
            print('Error : Wrong dimensionality for UnivariateSpline interpolation')
            raise ValueError

        if not points.shape[0] == values.shape[0]:
            print('Error : Number of points is unequal to number of values')
            raise ValueError
        else:
            self.Npts = len(values)

        # remember where the data came from... IO reasons
        self._data_origin = data_origin
        self._bounds_check = _bounds_check
        self._smoothing = smoothing

        self.kind = kind
        self.function = function

        # the interpolation range
        self._interp_range = np.array([np.min(points), np.max(points)])

        # check the grid
        self.points, self. values = self._check_grid(points, values)

        # Initialiaze the interpolation function
        self.interp_func, self.derivative = self._init_interp(function=self.function,
                                                              points=self.points,
                                                              values=self.values,
                                                              kind=self.kind,
                                                              smoothing=self._smoothing,
                                                              bounds_check=self._bounds_check)

        # remember where the data came from... IO reasons
        self.data_origin = data_origin

        self._name = 'Generic Univariate Spline interpolation'

    @staticmethod
    def _check_grid(points, values):
        """
        Check whether points are sorted in ascending order

        Parameters
        ----------
        points : (Npoints) array
            Numpy array containing all points that shall enter the
            interpolation.

        values : (Npoints) array
            Values corresponding to the <points>.

        Returns
        -------
        points_ordered : (Npoints) array
            Correctly ordered points

        values_ordered : (Npoints) array
            Correctly ordered values
        """
        # sort x first, then y
        idx = np.lexsort(keys=(values, points))

        # write to member variables
        points_ordered = points.copy()[idx]
        values_ordered = values.copy()[idx]

        return points_ordered, values_ordered

    @staticmethod
    def _init_interp(function, points, values, kind, bounds_check, smoothing=None):
        """
        Initialize the interpolation function, ie. construct the spline
        interpolation.

        Parameters
        ----------
        points : (Npoints) array
            Numpy array containing all points that shall enter the
            interpolation.

        values : (Npoints) array
            Values corresponding to the <points>.

        function : string, optional ({'UnivariateSpline', 'InterpolatedUnivariateSpline'})
            SciPy routine that will be employed to interpolate the grid.
            Note that "UnivariateSpline" is a more general form allowing also
            regression rather than interpolation. See also the "smoothin" argument.

        kind : string, optional ({'linear', 'cubic', 'quintic'})
            Degree of the bivariate splines unerlying the interpolation.

        smoothing : float, optional (default = None)
            Smoothing factor in case of a regression rather than interpolation.
            This arguent is only effectie when going for "UnivariateSpline" as
            interpolation function. For more details, see the corresponding
            documentation.

        _bounds_check : boolean, optional (default = True)
            Whether to perform a bounds check upon calling the interpolation. If
            <True> raises a ValueError if out-of-bounds. Only turn false if you
            know what you are doing, as extrapolating with interpolation splines is
            an extremely bad idea. Note that this comes with no extra cost, as the
            underlying SciPy routine does the check anyway.

        Returns
        -------
        interp_func : SciPy UnivariateSpline object.
            Readily parametrized interpolation function.

        derivative : SciPy UnivariateSpline object.
            The corresponding spline derivative.
        """
        x = points.copy()
        if kind == 'linear':
            k = 1
        elif kind == 'cubic':
            k = 3
        elif kind == 'quintic':
            k = 5

        if bounds_check:
            if (LooseVersion(scipy.__version__) >= LooseVersion('0.16.0')):
                ext = 2
            else:
                msg = 'INFO: Chose bounds check, but feature is only available for SciPy >_ 0.16.0'
                warnings.warn(msg)
        else:
            ext = 0


        if function.lower() == 'interpolatedunivariatespline':
            # interpolation function: Based on the routine SURFIT from FITPACK
            from scipy.interpolate import InterpolatedUnivariateSpline as spline
            # ext = 2: raise ValueError if outside of interpolation range
            if (LooseVersion(scipy.__version__) < LooseVersion('0.16.0')):
                interp_func = spline(x=x, y=values, k=k)
            else:
                interp_func = spline(x=x, y=values, k=k, ext=ext)
        elif function.lower() == 'univariatespline':
            # interpolation function: Based on the routine SURFIT from FITPACK
            from scipy.interpolate import UnivariateSpline as spline
            if (LooseVersion(scipy.__version__) < LooseVersion('0.16.0')):
                interp_func = spline(x=x, y=values, k=k, s=smoothing)
            else:
                interp_func = spline(x=x, y=values, k=k, ext=ext, s=smoothing)

        else:
            print('Error : Interpolation method "{}" unknown'.format(function))
            raise NotImplementedError

        derivative = interp_func.derivative(n=1)

        return interp_func, derivative


    def get_interpolation_range(self):
        """
        Function that returns the interpolation range without any unit
        conversion.
        """
        return self._interp_range


    def _create_infostring(self):
        """
        Function that creates some eyecandy I/O.
        """
        info = '{}'.format(self._name)
        info += '\n'
        info += '\nUnderlying SciPy routine : "{}"'.format(self.function)
        if self.function.lower() == 'univariatespline':
            info += '\n\tsmoothing : '
            if self._smoothing is None:
                info += 'None (automatic)'
            else:
                info += '{}'.format(self._smoothing)

        info += '\nDegree of spline functions : "{}"'.format(self.kind)
        if self._data_origin:
            info += '\n\nOrigin of underlying data points :'
            info += '\n\t' + self._data_origin

        info += '\n'
        info += '\nInterpolation range:'

        info += '\n{0:>20}{1:>20}'.format('min', 'max')
        info += '\n' + '-'*40

        info += '\n'
        for i in range(2):
            info += '{:>20s}'.format('{:>12.6f} a.u.'.format(self._interp_range[i]))

        return info


    def __str__(self):
        return self._create_infostring()

    def get_value(self, positions):
        """
        Call the spline interpolation. Vectorized calls are supported.

        Parameters
        ----------
        positions : (Natoms, Ndim) array with Ndim == 1.
            The positions at which to call the spline.

        Returns
        -------
        values : (Natoms) array
            The respective values.
        """
        return self.interp_func(positions.ravel())

    def get_gradient(self, positions):
        """
        Call the derivative of the spline interpolation. Vectorized calls are
        supported.

        Parameters
        ----------
        positions : (Natoms, Ndim) array with Ndim == 1.
            The positions at which to call the spline.

        Returns
        -------
        gradients : (Natoms) array
            The respective values.
        """

        return self.derivative(positions.ravel())

    def show(self):
        """
        Display small plot in internal coordinates
        """
        import matplotlib.pyplot as plt

        # 'continuous' x,y values to plot
        x = np.linspace(np.min(self.points), np.max(self.points), 400)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        ax.plot(x, self.get_value(x), color='red', lw=2 ,label='interpolation')
        ax.plot(self.points, self.values, color='black', marker='o', ls='', label='support')
        ax.set_xlabel('x (a.u.)')
        ax.set_ylabel('f(x) (a.u.)')
        ax.legend()

        plt.show()


class PeriodicUnivariateSpline(UnivariateSpline):
    """
    Basis class for several periodic things mapped on a 1D grid using
    UnivariateSplines as provided by scipy. There is no atomic vs. internal
    routine as so ofar we do not want to deal with reduced dimensionalities
    upon transforming coordinates. Hence, see this class as the "atomic" class.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    Note: Do not confuse this class with scipy.interpolate.UnivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    Initialization
    --------------
    points : (Npoints) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'UnivariateSpline', *'InterpolatedUnivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that "UnivariateSpline" is a more general form allowing also
        regression rather than interpolation. See also the "smoothing" argument.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    smoothing : float, optional (default = None)
        Smoothing factor in case of a regression rather than interpolation.
        This arguent is only effectie when going for "UnivariateSpline" as
        interpolation function. For more details, see the corresponding
        documentation.
    """
    #derived subclass, only need to change the calling routines
    def __init__(self, *args, **kwargs):
        kwargs['_bounds_check'] = False
        UnivariateSpline.__init__(self, *args, **kwargs)
        self._unit_cell_shift = self._interp_range[0]
        self._unit_cell_length = self._interp_range[1] - self._interp_range[0]

        self._name = "Generic PeriodicUnivariateSpline interpolation"
        self._name += '\n--> Born-von Karman PBC are applied at edges of interpolation range'

    def _wrap_positions(self, positions):
        # backfolding the positions and evaluation of fractional positions, and wrap back into cell
        pos = ((positions - self._unit_cell_shift) / self._unit_cell_length)%1
        pos *= self._unit_cell_length
        pos += self._unit_cell_shift
        return pos


    def get_value(self, positions):
        return UnivariateSpline.get_value(self, self._wrap_positions(positions))

    def get_gradient(self, positions):
        return UnivariateSpline.get_gradient(self, self._wrap_positions(positions))

    def show(self, repeats = 1.5):
        """
        Display small plot in internal coordinates
        """
        import matplotlib.pyplot as plt

        # 'continuous' x,y values to plot
        x = np.linspace(np.min(self.points) - repeats * self._unit_cell_length,
                        np.max(self.points) + repeats * self._unit_cell_length,
                        400)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.plot(x, self.get_value(x), color='red', lw=2 ,label='interpolation')
        ax.plot(self.points, self.values, color='black', marker='o', ls='', label='support')
        for y in self._interp_range:
            ax.axvline(y, ls='--', color='blue')
        ax.set_xlabel('x (a.u.)')
        ax.set_ylabel('f(x) (a.u.)')
        ax.legend()

        plt.show()
