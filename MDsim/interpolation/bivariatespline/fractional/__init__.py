# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings

from MDsim.interpolation.bivariatespline.atomicbivariatespline import AtomicBivariateSpline
from MDsim.coordinates.fractional import FractionalCoordinates


class FractionalBivariateSpline(AtomicBivariateSpline):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy. This can be highly benefitial, if you want to map on
    a triangular grid. You transform to fractional (and orthogonal!)
    coordinates and map therein (rectangular grids are easy now)...

    The big difference to the BivariateSpline base class is that we pipe all
    positions through a transform_coordinates routine, and that the
    get_gradient() routine does a matrix multiplication with the Jacobian of
    the transformation. This is all due to the fact that we may span the
    interpolation in a space other than cartesian, but still propagate in the
    latter due to isotropic friction.

    Note: Do not confuse this class with scipy.interpolate.BivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.
        This is to be in fractional coordinates.

    values : (Npoints) array
        Values corresponding to the <points>.

    cell : (2, 2) array
        The cell that defines the fractional coordinates. For units, see the
        convert switch.

    convert : boolean, optional (default=True)
        If <True> then the cell is expected in Angstrom, if <False> in atomic
        units, ie., Bohr.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, cell, *args, **kwargs):
        convert = kwargs.pop('convert', True)
        super(FractionalBivariateSpline, self).__init__(*args, **kwargs)
        self._name = "Generic FractionalBivariateSpline interpolation"

        # lazy coder ;)
        self._periodic = False

        # the coordinate transformation object
        self.coords = FractionalCoordinates(cell=cell, convert=convert)

        # storage to efficiently dump the gradient without having to create a
        # copy
        self._gradient = np.empty(2)


    def get_value(self, positions):
        """
        Evaluate the spline interpolation at the respective position. Note that
        this is a vectorized call. We rather account for the fact were we
        have to deal with non-cartesian coordinate systems. This means, we do a
        coordinate transformation before actually calling the interpolation
        function.

        Bounds checks can be enabled via the member variable "_bounds_check"

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Cartesian position vector. Will first be transformed.

        Returns
        -------
        values : (Natoms,) array
            Value corresponding to the positions specified.
        """
        return super(FractionalBivariateSpline, self).get_value(
                     self.coords.positions_cartesian_to_fractional(positions,
                         periodic=self._periodic)
                     )


    def get_gradient(self, positions):
        """
        Evaluate the gradient of the spline interpolation at the respective
        cartesian position. Note that this yields an analytical gradient.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Cartesian coordinate vector at which to evaluate the gradient.

        Returns
        -------
        gradient : (Natoms, Ndim) array
            Gradient corresponding to the respective coordinates in cartesian
            basis.
        """

        return self.coords.gradient_fractional_to_cartesian(
                    super(AtomicBivariateSpline, self).get_gradient(
                        self.coords.positions_cartesian_to_internals(positions,
                                                                     periodic=self._periodic)
                        )
                    )

    def _create_infostring(self):
        """
        Function that creates some eyecandy I/O.
        """
        info = super(FractionalBivariateSpline, self)._create_infostring()
        info += '\n'
        info += '\nCell for fractional coordinates:'

        info += '\n{0:>20}{1:>20}{2:>20}'.format('', 'x', 'y')
        info += '\n' + '-'* 60

        for i in range(self.Ndim):
            info += '\n' + '{:<20s}'.format('axis {}'.format(i))
            for j in range(2):
                info +='{:>20s}'.format('{:>12.6f} a.u.'.format(self.coords.cell[i,j]))

        return info
