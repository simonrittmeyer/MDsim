# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings

from MDsim.interpolation.bivariatespline import BivariateSpline

class AtomicBivariateSpline(BivariateSpline):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    Note: Do not confuse this class with scipy.interpolate.BivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    This class provides a vectorized get_value() routine. Only use it if you
    propagate in original (atomic) coordinates.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, *args, **kwargs):
        BivariateSpline.__init__(self, *args, **kwargs)
        self._name = "Generic AtomicBivariateSpline interpolation"

    def get_value(self, positions):
        """
        Evaluate the spline interpolation at the respective positions.
        Note that this function can deal with vectorized calls. Bounds checks
        can be enabled via the member variable "_bounds_check"

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the interpolation.

        Returns
        -------
        values : (Natoms x 1) array
            Values corresponding to the respective positions.
        """
        if self._bounds_check:
            self._check_bounds(positions)
        return self.interp_func.ev(*positions.T)

    def get_gradient(self, positions):
        """
        Evaluate the gradient of the spline interpolation at the respective
        positions. Note that this yields an analytical gradient.

        Note that this function can deal with vectorized calls. Bounds checks
        can be enabled via the member variable "_bounds_check"

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the interpolation.

        Returns
        -------
        gradient : (Natoms x Ndim) array
            Gradients corresponding to the respective positions.
        """
        if self._bounds_check:
            self._check_bounds(positions)

        gradient = np.empty_like(positions, dtype = np.float64, order = 'C')
        gradient[:,0] = self.interp_func.ev(*positions.T, dx = 1, dy = 0)
        gradient[:,1] = self.interp_func.ev(*positions.T, dx = 0, dy = 1)

        return gradient


    def _check_bounds(self, positions):
        """
        Function to check whether the input position is out of interpolation
        range or not.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions to be tested.

        Returns
        -------
        None

        Raises
        ------
        ValueError : If any position in "positions" is out of bounds.
        """
        # sanity check whether we are in the interpolation range

        if (np.any(positions < self._interp_range[:,0])
            or np.any(positions > self._interp_range[:,1])):
            raise ValueError('Out of interpolation range.')


    def _create_contour_plot(self, lim = None, npts = 200, _repeats = 1):
        """
        Function to create a neat little 2D contour plot.

        Parameters
        ----------
        lim : 2-tuple, optional (default = None)
            The limits of the color map.

        npts : int, optional (default = 200)
            The number of points at which the interpolation is evaluated.

        _repeats : float, optional (default = 1.)
            Scales the plot axes range. Will be _repeats * interp_range. Note
            that this will fail for non-periodic interpolation with
            bounds_check if _repeats > 1.

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'xy_range' : (2 x 2) array
                    The plot limits in x and y direction.

                'lim' : (2 x 1) array
                    The cmap limits.

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot.

                'repeats' : float
                    The number of repeating units of the unit cell

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot.
        """


        import matplotlib.pyplot as plt

        # 'continuous' x,y values to plot
        xy_range = self._interp_range * _repeats

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)
        X,Y = np.meshgrid(x,y)

        points = np.vstack((X.flatten(), Y.flatten())).T

        # we use this direct way here to support also the fracitonal coordinates
        if self._periodic:
            Z = self.interp_func.ev(*points.T%1).reshape(npts, npts)
        else:
            Z = self.interp_func.ev(*points.T).reshape(npts, npts)

        # colormap
        cmap = plt.get_cmap('coolwarm')

        fig = plt.figure()

        ax = fig.add_subplot(1,1,1)

        if lim is None:
            lim = (np.min(Z.ravel()), np.max(Z.ravel()))

        # surface plot
        surf = ax.pcolormesh(X,Y,Z, cmap = cmap)

        # contour plot
        levels = np.linspace(*lim, num = 10)
        cont = ax.contour(X,Y,Z, colors = 'black', extend="both",
                          levels = levels)

        # scatter plot
        scat = ax.scatter(x = self.points[:,0], y = self.points[:,1],
                          c = self.values, s = 20, cmap = cmap,
                          label = 'sampled points')


        for i in [surf, cont, scat]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel('X / a.u.')
        ax.set_ylabel('Y / a.u.')

        ax.clabel(cont, fmt ='%.2f a.u.')

        # set a colorbar
        cbar = fig.colorbar(surf, orientation='vertical')
        cbar.set_label('f(X,Y) / a.u.')

        plot_dict= {'lim' : lim,
                    'repeats' : _repeats,
                    'ax' : ax,
                    'fig' : fig,
                    'cbar' : cbar,
                    'surf' : surf,
                    'scat' : scat,
                    'xy_range' : xy_range,
                    'cont' : cont
                    }

        return plot_dict


    def show(self, *args, **kwargs):
        """
        Show a nice little 2D plot

        Parameters
        ----------
        lim : 2-tuple, optional (default = None)
            The limits of the color map.

        npts : int, optional (default = 200)
            The number of points at which the interpolation is evaluated.
        """
        import matplotlib.pyplot as plt

        plot_dict = self._create_contour_plot(*args, **kwargs)
        plt.show()


