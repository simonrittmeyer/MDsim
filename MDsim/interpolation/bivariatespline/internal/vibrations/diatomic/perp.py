# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to handle interpolations that are spanned on a dRcom, ddist grid
"""

from __future__ import print_function
import numpy as np

from MDsim.interpolation.bivariatespline.internal import InternalBivariateSpline
from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp


class DiatomicPerpSpline(InternalBivariateSpline):
    """
    Interpolation that handles the diatomics that adsorb perpendicular to the
    surface plane.

    Initialization
    --------------
    masses : (2,) array
        The masses of the atoms.

    points : (Npoints, 2) array
        Numpy array containing all points that shall enter the interpolation.
        These are in internal coordinates!

    values : (Npoints,) array
        Values corresponding to the <points>.

    positions_eq : (2,1) array, optional (default=None)
        In case you want to use a constant offset converting from internals to
        cartesian evv. This is the offset in cartesian coordinates.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, masses, *args, **kwargs):
        self.coordinates = InternalCoordsDiatomicPerp(masses=masses,
                                                      positions_eq=kwargs.pop('positions_eq', None),
                                                      convert=kwargs.pop('convert', True)
                                                     )

        InternalBivariateSpline.__init__(self, *args, **kwargs)

    def coordinates_cartesian_to_internals(self, positions):
        """
        This is the transformation from the (Natoms x Ndim) positions array to
        a flat internal coordinates array. Needs to be implemented by subclasses.
        """
        #raise NotImplementedError('Coordinate transformation has to be implemented by subclass')
        # most simple way: ravel the thing
        return self.coordinates.positions_cartesian_to_internals(positions)

    def gradient_internals_to_cartesian(self, gradient):
        """
        Transform forces back to cartesian basis (Natoms x Ndim)
        """
        return self.coordinates.gradient_internal_to_cartesian(gradient)

