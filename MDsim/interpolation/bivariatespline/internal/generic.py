# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.interpolation.bivariatespline.internal import InternalBivariateSpline


class GenericDOFBivariateSpline(InternalBivariateSpline):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    This class is intended to be used in situations where each "Atom" is 1D,
    thus representing some abstract DOF with a mass associated.

    Note: Do not confuse this class with scipy.interpolate.BivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    Initialization
    --------------
    points : (Npoints, 2) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, *args, **kwargs):
        InternalBivariateSpline.__init__(self, *args, **kwargs)
        self._name = "GenericDOFBivariateSpline interpolation"

        # storage to efficiently dump the gradient without having to create a
        # copy
        self._gradient = np.empty(2)


    def coordinates_cartesian_to_internals(self, positions):
        """
        Transform the positions to internal by simply flattening the array.

        Arguments
        ---------
        positions : (2,1) array
            The positions in MDsim representation.

        Returns
        -------
        internals : (2,) array
            The flatten array.
        """
        return positions.ravel()


    def gradient_internals_to_cartesian(self, gradient):
        """
        Transform the gradient from internal by simply reshaping the array.

        Arguments
        ---------
        gradient : (2,) array
            The gradient in internal representation.

        Returns
        -------
        internals : (2,1) array
            The reshaped array.
        """
        return gradient.reshape(2,1)

