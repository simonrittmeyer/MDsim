# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings

from MDsim.interpolation.bivariatespline import BivariateSpline

class InternalBivariateSpline(BivariateSpline):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    The big difference to the BivariateSpline base class is that we pipe all
    positions through a transform_coordinates routine, and that the
    get_gradient() routine does a matrix multiplication with the Jacobian of
    the transformation. This is all due to the fact that we may span the
    interpolation in a space other than cartesian, but still propagate in the
    latter due to isotropic friction.

    Note: Do not confuse this class with scipy.interpolate.BivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, *args, **kwargs):
        BivariateSpline.__init__(self, *args, **kwargs)
        self._name = "Generic InternalBivariateSpline interpolation"

        # storage to efficiently dump the gradient without having to create a
        # copy
        self._gradient = np.empty(2)


    def coordinates_cartesian_to_internals(self, positions):
        """
        To be implemented by any derived class.

        Idea is to transform the cartesian (Natoms, Ndim) coordinates vector to
        an internal (flat) coordinates vector.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            The cartesian positions array.

        Returns
        -------
        internals : (2,) array
            The internal coordinates.
        """
        raise NotImplementedError


    def gradient_internals_to_cartesian(self, gradient):
        """
        To be implemented by any derived class.

        Idea is to backtransform a gradient array evaluated in internal
        coordinates to yield a force array in cartesian coordinates in the
        proper dimensionality.

        Parameters
        ----------
        gradient : (2,) array
            The gradient in internal coordinates basis.

        Returns
        -------
        forces : (Natoms, Ndim) array
            The forces in cartesian basis.
        """
        raise NotImplementedError


    def get_value(self, positions):
        """
        Evaluate the spline interpolation at the respective position. Note that
        this is not a vectorized call. We rather account for the fact were we
        have to deal with non-cartesian coordinate systems. This means, we do a
        coordinate transformation before actually calling the interpolation
        function.

        Bounds checks can be enabled via the member variable "_bounds_check"

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Cartesian position vector. Will first be transformed.

        Returns
        -------
        value : float
            Value corresponding to the positions specified.
        """

        return self.get_value_internals(self.coordinates_cartesian_to_internals(positions))


    def get_value_internals(self, internals):
        """
        Evaluate the spline interpolation directly feeding in internal coordinates.

        Bounds checks can be enabled via the member variable "_bounds_check"

        Parameters
        ----------
        internals : (2,) array
            Internal coordinates vector.

        Returns
        -------
        value : float
            Value corresponding to the positions specified.
        """
        if self._bounds_check:
            self._check_bounds(internals)

        return np.float64(self.interp_func.ev(*internals))


    def get_gradient(self, positions):
        """
        Evaluate the gradient of the spline interpolation at the respective
        cartesian position. Note that this yields an analytical gradient.

        Evaluate the spline interpolation at the respective position. Note that
        this is not a vectorized call. We rather account for the fact were we
        have to deal with non-cartesian coordinate systems and thus do a
        coordinate transform beforehand.

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Internal coordinate vector at which to evaluate the gradient.

        Returns
        -------
        gradient : (Natoms, Ndim) array
            Gradient corresponding to the respective coordinates in cartesian
            basis.
        """

        return self.gradient_internals_to_cartesian(
                self.get_gradient_internals(
                    self.coordinates_cartesian_to_internals(positions)))


    def get_gradient_internals(self, internals):
        """
        Evaluate the gradient directly at the internal coordinates. Note that
        also the gradient is then in internals basis.
        """
        if self._bounds_check:
            self._check_bounds(internals)

        self._gradient[0] = self.interp_func.ev(*internals, dx = 1, dy = 0)
        self._gradient[1] = self.interp_func.ev(*internals, dx = 0, dy = 1)

        return self._gradient


    def _check_bounds(self, internals):
        """
        Function to check whether the input position is out of interpolation
        range or not.

        Parameters
        ----------
        internals : (Ndof,) array (flat!)
            Internal coordinates to be tested.

        Returns
        -------
        None

        Raises
        ------
        ValueError : If any position in "internals" is out of bounds.
        """
        # sanity check whether we are in the interpolation range
        if (np.any(internals < self._interp_range[:,0])
                or np.any(internals > self._interp_range[:,1])):
            msg = 'Point [{0:.6f} {1:.6f}] is out of interpolation range'.format(*internals)
            msg += '\nInterpolation range is x in [{:.6f} {:.6f}] and y in [{:.6f}, {:.6f}]'.format(*self._interp_range.flatten())
            raise ValueError(msg)


    def _create_contour_plot(self, lim = None, npts = 200, _repeats = 1):
        """
        Function to create a neat little 2D contour plot.

        Parameters
        ----------
        lim : 2-tuple, optional (default = None)
            The limits of the color map.

        npts : int, optional (default = 200)
            The number of points at which the interpolation is evaluated.

        _repeats : float, optional (default = 1.)
            Scales the plot axes range. Will be _repeats * interp_range. Note
            that this will fail for non-periodic interpolation with
            bounds_check if _repeats > 1.

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'xy_range' : (2 x 2) array
                    The plot limits in x and y direction.

                'lim' : (2 x 1) array
                    The cmap limits.

                'repeats' : float
                    The number of repeating units of the unit cell

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot.

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot.
        """


        import matplotlib.pyplot as plt

        # 'continuous' x,y values to plot
        xy_range = self._interp_range * _repeats

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)
        X,Y = np.meshgrid(x,y)

        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.empty_like(points[:,0])
        for i in range(npts**2):
            Z[i] = self.get_value_internals(points[i].ravel())
        Z = Z.reshape(npts, npts)

        # colormap
        cmap = plt.get_cmap('coolwarm')

        fig = plt.figure()

        ax = fig.add_subplot(1,1,1)

        if lim is None:
            lim = (np.min(Z.ravel()), np.max(Z.ravel()))

        # surface plot
        surf = ax.pcolormesh(X,Y,Z, cmap = cmap)

        # contour plot
        levels = np.linspace(*lim, num = 10)
        cont = ax.contour(X,Y,Z, colors = 'black', extend="both",
                          levels = levels)

        # scatter plot
        scat = ax.scatter(x = self.points[:,0], y = self.points[:,1],
                          c = self.values, s = 20, cmap = cmap,
                          label = 'sampled points')


        for i in [surf, cont, scat]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel('X_internal / a.u.')
        ax.set_ylabel('Y_internal / a.u.')

        ax.clabel(cont, fmt ='%.2f a.u.')

        # set a colorbar
        cbar = fig.colorbar(surf, orientation='vertical')
        cbar.set_label('f(X,Y) / a.u.')

        plot_dict= {'lim' : lim,
                    'repeats' : _repeats,
                    'ax' : ax,
                    'fig' : fig,
                    'cbar' : cbar,
                    'surf' : surf,
                    'scat' : scat,
                    'xy_range' : xy_range,
                    'cont' : cont
                    }

        return plot_dict


    def show(self, *args, **kwargs):
        """
        Show a nice little 2D plot

        Parameters
        ----------
        lim : 2-tuple, optional (default = None)
            The limits of the color map.

        npts : int, optional (default = 200)
            The number of points at which the interpolation is evaluated.
        """
        import matplotlib.pyplot as plt


        plot_dict = self._create_contour_plot(*args, **kwargs)
        plt.show()
