# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.interpolation.bivariatespline.atomicbivariatespline import AtomicBivariateSpline

# atomic support only!

class PeriodicAtomicBivariateSpline(AtomicBivariateSpline):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy. This class adds periodic position wrapping on top of
    the BivariateSpline class.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'interp2d', 'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only works on
        regular, rectangular grids.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.
    """
    def __init__(self, *args, **kwargs):

        # no bounds check as we are perdiodic
        kwargs['_bounds_check'] = False
        AtomicBivariateSpline.__init__(self, *args, **kwargs)

        self._periodic = True
        self._unit_cell_shift = self._interp_range[:,0]
        self._unit_cell_diag = self._interp_range[:,1] - self._interp_range[:,0]
        self._unit_cell_diag_bc = self._unit_cell_diag[np.newaxis,:]

        self._name = "Generic PeriodicAtomicBivariateSpline interpolation"
        self._name += '\n--> Born-von Karman PBC are applied at edges of interpolation range'

    def _wrap_positions(self, positions):
        # backfolding the positions and evaluation of fractional positions, and wrap back into cell
        pos = ((positions - self._unit_cell_shift) / self._unit_cell_diag_bc)%1
        pos *= self._unit_cell_diag_bc
        pos += self._unit_cell_shift
        return pos


    def get_value(self, positions):
        return AtomicBivariateSpline.get_value(self, self._wrap_positions(positions))

    def get_gradient(self, positions):
        return AtomicBivariateSpline.get_gradient(self, self._wrap_positions(positions))


    def show(self, *args, **kwargs):
        if not '_repeats' in kwargs.keys():
            kwargs['_repeats'] = 2

        plot_dict = self._create_contour_plot(*args, **kwargs)

        import matplotlib.pyplot as plt

        # set the "unit cell"
        from matplotlib import patches
        cell = patches.Rectangle(self._interp_range[:,0],
                                 *self._unit_cell_diag,
                                 ec= 'black',
                                 fc = 'none',
                                 lw = 2)

        ax = plot_dict['ax']
        ax.add_patch(cell)

        plt.show()

