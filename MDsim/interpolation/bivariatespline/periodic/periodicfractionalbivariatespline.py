# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np
from MDsim.interpolation.bivariatespline.fractional import FractionalBivariateSpline

# atomic support only!

class PeriodicFractionalBivariateSpline(FractionalBivariateSpline):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy. This class adds periodic position wrapping on top of
    the BivariateSpline class.

    Note that perdiodicity is with respect to the fractional coordinates! You
    as a user need to take care that you properly cover this range with your
    input data points.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    cell : (2, 2) array
        The cell that defines the fractional coordinates. For units, see the
        convert switch.

    convert : boolean, optional (default=True)
        If <True> then the cell is expected in Angstrom, if <False> in atomic
        units, ie., Bohr.

    function : string, optional ({'interp2d', 'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only works on
        regular, rectangular grids.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.
    """
    def __init__(self, *args, **kwargs):

        # no bounds check as we are perdiodic
        kwargs['_bounds_check'] = False
        FractionalBivariateSpline.__init__(self, *args, **kwargs)
        self._periodic = True

        self._name = "Generic PeriodicFractionalBivariateSpline interpolation"
        self._name += '\n--> Born-von Karman PBC are applied at edges of interpolation range'


    def show(self, *args, **kwargs):
        if not '_repeats' in kwargs.keys():
            kwargs['_repeats'] = 2

        plot_dict = self._create_contour_plot(*args, **kwargs)

        import matplotlib.pyplot as plt

        ax = plot_dict['ax']

        # corners of the unit cell
        corners = np.array([np.zeros(2),
                            self.coords.cell[0],
                            self.coords.cell[1],
                            self.coords.cell[0] + self.coords.cell[1]])

        # draw the unit cell
        for connected in ([0, 1], [0, 2], [2, 3], [1, 3]):
            ax.plot(*corners[connected].T, color='red')

        plt.show()

