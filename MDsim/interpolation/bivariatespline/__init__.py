# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

# warnings if out of interpolation range
import warnings


class BivariateSpline(object):
    """
    Basis class for several things mapped on a 2D grid using Bivariate Splines
    as provided by scipy.

    Please note that there is no conversion of units whatsoever. What you feed
    in is what you get out. Make sure upon creation of this object or any
    derived objects thereof to take care of the proper unit conversions.

    Note: Do not confuse this class with scipy.interpolate.BivariateSpline! It
    does contain two derived subclasses thereof but additionally wraps some
    nice things.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    values : (Npoints) array
        Values corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, points, values,
                 function = 'RectBivariateSpline',
                 kind = 'cubic',
                 data_origin = 'None',
                 _bounds_check = True):

        # hard coded system dimensionality
        self.Ndim = 2

        if not points.shape[1] == self.Ndim:
            msg ='Wrong dimensionality for BivariateSpline interpolation'
            raise ValueError(msg)

        if not points.shape[0] == values.shape[0]:
            msg = 'Number of points is unequal to number of values'
            raise ValueError(msg)
        else:
            self.Npts = len(values)

        # remember where the data came from... IO reasons
        self._data_origin = data_origin
        self._bounds_check = _bounds_check

        self.kind = kind
        self.function = function

        # the interpolation range
        self._interp_range = np.array([[np.min(points[:,0]), np.max(points[:,0])],
                                      [np.min(points[:,1]), np.max(points[:,1])]
                                      ])

        # check the grid
        self.points, self.values = self._check_grid(points, values)

        # Initialiaze the interpolation function
        self.interp_func = self._init_interp(points = self.points,
                                             values = self.values,
                                             function = self.function,
                                             kind  = self.kind)

        self._name = "Generic BivariateSpline interpolation"

        # just a convenience flag
        self._periodic = False

    def get_value(self, *args, **kwargs):
        raise NotImplementedError('Implement "get_value()" in subclass!')

    def get_gradient(self, *args, **kwargs):
        raise NotImplementedError('Implement "get_gradient()" in subclass!')

    def _check_bound(self, *args, **kwargs):
        raise NotImplementedError('Implement "_check_bounds()" in subclass!')

    def _create_infostring(self):
        """
        Function that creates some eyecandy I/O.
        """
        info = '{}'.format(self._name)
        info += '\n'
        info += '\nUnderlying SciPy routine : "{}"'.format(self.function)
        info += '\nDegree of spline functions : "{}"'.format(self.kind)
        if self._data_origin:
            info += '\n\nOrigin of underlying data points :'
            info += '\n\t' + self._data_origin

        info += '\n'
        info += '\nInterpolation range:'

        info += '\n{0:>20}{1:>20}{2:>20}'.format('', 'min', 'max')
        info += '\n' + '-'* 60

        for i in range(self.Ndim):
            info += '\n' + '{:<20s}'.format('coordinate {}'.format(i))
            for j in range(2):
                info +='{:>20s}'.format('{:>12.6f} a.u.'.format(self._interp_range[i,j]))

        return info

    def __str__(self):
        return self._create_infostring()

    def _check_grid(self, points, values):
        """
        Function that checks the supplied points for ascending order.

        This is a requirement for "RectBivariateSpline" and does not hurt for
        "SmoothBivariateSpline".

        Parameters
        ----------
        points : (Natoms x Ndim) array
            The input points.

        values : (Natoms) array
            The input values.

        Returns
        -------
        points_ordered : (Npoints) array
            Correctly ordered points

        values_ordered : (Npoints) array
            Correctly ordered values
        """
        # sort x first, then y
        idx = np.lexsort(keys=(values, points[:,1], points[:,0]))

        # write to member variables
        points_ordered = points.copy()[idx]
        values_ordered = values.copy()[idx]

        return points_ordered, values_ordered

    def _init_interp(self, points, values, function, kind):
        """
        Initialize the interpolation function, ie. construct the spline
        interpolation.

        Parameters
        ----------
        points : (Npoints x 2) array
            Numpy array containing all points that shall enter the
            interpolation.

        values : (Npoints) array
            Values corresponding to the <points>.

        function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
            SciPy routine that will be employed to interpolate the grid.  Note
            that 'RectBivariateSpline' is very fast and reliable but only works
            on regular, rectangular grids. 'SmoothBivariateSpline' also works
            on irregular grids but may induce artifacts. Check carefully.

        kind : string, optional ({'linear', 'cubic', 'quintic'})
            Degree of the bivariate splines unerlying the interpolation.

        Returns
        -------
        interp_func : SciPy BivariateSpline object.
            Readily parametrized interpolation function.
        """
        x = points.T.copy()[0]
        y = points.T.copy()[1]

        if kind == 'linear':
            kx = 1; ky = 1
        elif kind == 'cubic':
            kx = 3; ky = 3
        elif kind == 'quintic':
            kx = 5; ky = 5

        if function.lower() == 'smoothbivariatespline':
            print('Info : You chose "SmoothBivariateSpline" as interpolation method.')
            print('       Note that if your data is on a regular grid you can yield')
            print('       much more performant results by choosing "RectBivariateSpline".')
            # interpolation function: Based on the routine SURFIT from FITPACK
            from scipy.interpolate import SmoothBivariateSpline

            interp_func = SmoothBivariateSpline(x = x, y = y, z = values,
                                                kx = kx, ky = ky,
                                                s = 0) #interpolation, no smoothing

        elif function.lower() == 'rectbivariatespline':
            # here we have to work on the given points...
            # this only works for regular, rectangular grids!
            # the x and y coordinate must be given in strictly ascending order!
            from scipy.interpolate import RectBivariateSpline

            # only the unique points
            x = np.unique(x)
            y = np.unique(y)

            if not len(x)*len(y) == len(values):
                raise ValueError('Something is wrong with your data...')

            # reshape the observable points
            vals = values.reshape((len(x), len(y)))

            interp_func = RectBivariateSpline(x = x, y = y, z = vals,
                                              kx = kx, ky = ky,
                                              s = 0) # interpolation, no smoothing

        else:
            msg = 'Interpolation method "{}" unknown'.format(function)
            raise NotImplementedError(msg)

        return interp_func


    def get_interpolation_range(self):
        return self._interp_range
