# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

from MDsim import units
from MDsim.friction import FrictionMatrix


class ScalarFriction(FrictionMatrix):
    """
    Eta matrix that mimics a scalar eta for all degrees of freedom

    Initialization
    --------------
    eta : float
        The scalar friction coefficient that will be the same for all elements
        of the friction matrix.

    convert : Boolean, optinal (default = True)
        Whether to convert from AMU/fs to a.u. or not. If <False> then input in
        a.u.
    """

    def __init__(self, eta, Natoms=None, Ndim=None, convert=True):
        self.eta = np.float64(eta)
        if convert:
            self.eta *= units.AMU_PER_FS_TO_AU

        self.Natoms = Natoms
        self.Ndim = Ndim

        # our container to avoid unneccessary copies
        self._friction_matrix_init = False
        self.friction_matrix = None

        if not np.any([Natoms is None, Ndim is None]):
            self.friction_matrix = self.eta * np.ones((Natoms, Ndim),
                                                 order='C',
                                                 dtype=np.float64)
            self._friction_matrix_init = True

        super(ScalarFriction, self).__init__()

    def __call__(self, positions):
        """
        Return the scalar eta times the unit matrix.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Just a dummy argument with no effect.

        Returns
        -------
        friction_matrix : (Natoms x Ndim) array
            The friction matrix, or better the diagonal elements thereof.
        """
        if not self._friction_matrix_init:
            self.friction_matrix = self.eta * np.ones_like(positions,
                                                      dtype=np.float64,
                                                      order='C')
            self._friction_matrix_init = True
        return self.friction_matrix

    def __str__(self):
        return "ScalarFriction instance with "\
                "eta = {0:.6e} a.u. = {1:.6e} amu/fs".format(self.eta,
                                                             self.eta * units.AU_TO_AMU_PER_FS)
