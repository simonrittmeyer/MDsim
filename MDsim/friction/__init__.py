# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

class FrictionMatrix(object):
    """
    This is the base class for any eta matrix employed within the MD
    simulations.

    It is supposed to return the diagonal elements of the (diagonal!) friction
    matrix as (Natoms x Ndim) vector. This far we do not support any non-diagonal
    friction matrices.

    Note that throughout this code a DIAGONAL FRICTION MATRIX IS ASSUMED. This
    may be confusing at first glance as the returned friction matrices are
    neither of square form nor diagonal. The reason is that we can drop matrix
    multiplications an replace the friction matrix by a vector that we can
    element-wise multiply, if we have diagonal friction!

    Most importantly, we follow the notation of the Langevin-EOM as follows:

    m*d^2/dt^2 pos(t) = -nabla PES(pos) - eta * d/dt pos(t) + F_random

    This means, we employ mass-weighted friction coefficients. In doing so, we
    follow the Notation of Pastor et al.[1], who call

        gamma = (eta / m)

    a "collision frequency" rather than a friction coefficent. Note that this
    very often interchanged and may confuse a lot. For instance Bussi and
    Parrinello [2] consider gamma to be their friction coefficient.

    This is confusing but we have to make a choice! And we define the friction
    force as

    F = - eta * v

    and use eta as a synonym of "friction".


    References
    ----------
    [1] R.W. Pastor, B.R. Brooks and A. Szabo, Mol. Phys. 65, 1409 (1988)
        DOI: 10.1080/00268978800101881

    [2] G. Bussi and M. Parrinello, Phys. Rev. E 75, 056707 (2007)
        DOI: 10.1103/PhysRevE.75.056707
    """

    def __init__(self):
        self._diagonal = True

    def is_diagonal(self):
        return self._diagonal

    def __call__(self, positions):
        """
        Calculate/return the diagonal elements of the friction tensor.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the friction tensor.

        Returns
        -------
        frictionmatrix : (Natoms x Ndim) array
            The diagonal elements of the friction tensor.
        """

        raise NotImplementedError

    def __str__(self):
        return "FrictionMatrix base class"

