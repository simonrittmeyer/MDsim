# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to implement a friction tensor that is not necessarily diagonal
"""

# linear algebra
import numpy as np
from MDsim import units

from MDsim.friction import FrictionMatrix
from MDsim.coordinates.frictiontensor import FrictionModes

class FrictionTensor(FrictionMatrix):
    """
    Constant eta matrix that can be used just like an friction_matrix which is
    position dependent.

    Initialization
    --------------
    friction_matrix_mw : (Natoms*Ndim, Natoms*Ndim) array
        The constant eta tensor in *mass-weighted coordinates*.

    masses : (Natoms,) array
        The masses associated with each atom. Required for the coordinate
        transformations.

    convert : Boolean, optinal (default = True)
        Whether to convert from fs^-1 and AMU to a.u. or not. If <False> then
        input in a.u.
    """

    def __init__(self, friction_matrix_mw, masses, convert=True):
        self.friction_matrix_mw = np.asarray(friction_matrix_mw, order='C', dtype=np.float64)
        self.masses = np.asarray(masses, order='C', dtype=np.float64)
        self._convert = convert
        if self._convert:
            # no in-place modifications
            self.friction_matrix_mw = self.friction_matrix_mw / units.FS_TO_AU
            self.masses = self.masses * units.AMU_TO_AU

        self.Natoms = self.masses.shape[0]
        self.Ndim = self.friction_matrix_mw.shape[0] / self.Natoms

        # conversion is already done in this init, do not double convert.
        self.coords = FrictionModes(friction_tensor_mw=self.friction_matrix_mw,
                                    masses=self.masses,
                                    convert=False)

        # the diagonal friction
        self.gamma_mw = self.coords.frictiontensor_massweightedcartesian_to_frictionmodes(self.friction_matrix_mw)

        # the non-mass weighted friction tensor (required for consistency)
        Msqrt = self.coords.get_Msqrt(_au=True)

        self.friction_matrix = Msqrt.dot(self.friction_matrix_mw.dot(Msqrt))
        w, V = np.linalg.eig(self.friction_matrix)
        Vinv = np.linalg.inv(V)

        self.gamma = Vinv.dot(self.friction_matrix.dot(V))

        # the diagonal versions in the eigenbasis for quick references...
        self.gamma_diag = np.diag(self.gamma)
        self.gamma_mw_diag = np.diag(self.gamma_mw)

        self._diagonal = False

    def velocities_cartesian_to_frictionmodes(self, velocities):
        return self.coords.velocities_cartesian_to_frictionmodes(velocities)

    def velocities_frictionmodes_to_cartesian(self, frictionmodes):
        return self.coords.velocities_frictionmodes_to_cartesian(frictionmodes)

    def __call__(self, positions):
        """
        Return the eigenvalues of the *non-mass-weighted* friction tensor. This
        is required for consistency!

        Parameters
        ----------
        positions : (Natoms, Ndim) array
            Just a dummy argument with no effect.

        Returns
        -------
        etas : (Natoms*Ndim, Natoms*Ndim) array
            The eta matrix, or the diagonal elements thereof.
        """
        # this strange thing here is for consistency
        return np.diag(self.gamma).reshape(self.Natoms, self.Ndim)

    def get_friction_tensor(self, _au=False):
        if _au or not self._convert:
            return self.friction_matrix
        else:
            return self.friction_matrix * units.AU_TO_AMU_PER_FS

    def get_friction_tensor_mw(self, _au=False):
        if _au or not self._convert:
            return self.friction_matrix_mw
        else:
            return self.friction_matrix_mw / units.AU_TO_FS

    def get_friction_tensor_eigenbasis(self, _au=False):
        if _au or not self._convert:
            return self.gamma
        else:
            return self.gamma * units.AU_TO_AMU_PER_FS

    def get_friction_tensor_eigenbasis_mw(self, _au=False):
        if _au or not self._convert:
            return self.gamma_mw
        else:
            return self.gamma_mw / units.AU_TO_FS

    def get_diagonal_friction_tensor(self, _au=False):
        if _au or not self._convert:
            return self.gamma_diag
        else:
            return self.gamma_diag * units.AU_TO_AMU_PER_FS

    def get_diagonal_friction_tensor_mw(self, _au=False):
        if _au or not self._convert:
            return self.gamma_mw_diag
        else:
            return self.gamma_mw_diag / units.AU_TO_FS

    def __str__(self):
        name  = "TensorialFriction instance with the following values"
        name += '\n--> first index numbers atom, second cartesian degree of freedom'
        name += '\n--> NOTE THAT THIS IS THE MASS-WEIGHTED FRICTION TENSOR'
        name += "\n\n"

        if np.all(self.friction_matrix_mw == self.friction_matrix_mw[0,0]):
            name += '--> homogeneous matrix with elements {{eta_ij}} = {:12.6e} a.u.'.format(self.friction_matrix_mw[0,0])
        else:
            for i in range(self.Natoms):
                for j in range(self.Ndim):
                    name += "{:>20s}".format("eta_{}_{} (a.u.)".format(i, ['x','y','z'][j]))
            name += '\n' + '-'*20*self.Ndim*self.Natoms

            for eta in self.friction_matrix_mw:
                name += '\n'
                for eta_i in eta:
                    name += '{:>+20.6e}'.format(eta_i)
        return name
