# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
This is supposed to be a matrix for homogeneous atoms systems where we can
basically efffectively evaluate one atomic friction coefficient in a vectorized
fashion.
"""

# linear algebra
import numpy as np

from MDsim import tools
from MDsim import units

from MDsim.friction.electronicfriction.ldfa.matrix import LDFAFrictionMatrix

class HomogeneousAtomsFrictionMatrix(LDFAFrictionMatrix):
    """
    LDFA matrix for a system of homogeneous atoms. Note that for such a system,
    assuming a quasi-IAA picture (ie. AIM only to account for surface-charge
    rearrangements) can hold. Then we can nicely evaluate the friction
    coeffient at several positions in a vecotrized fashion and save quite some
    computational time.

    Initialization
    --------------
    friction_coefficient : Friction Coefficient.
        FrictionCoefficient instances which map positions to friction
        coefficients. Note that the undelrying function must provide a
        vectorized calling routine.
    """
    def __init__(self, friction_coefficient):
        # no convert flag at this point since the eta instances have to take
        # care of it!
        self.friction_coefficient = friction_coefficient

        # the working array
        self._etas = None
        self._created_etas = False

        self._name = 'Homogeneous Atoms Friction Matrix'

    def __call__(self, positions):
        """
        Evaluate the LDFA-IAA matrix at the specified positions.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the friction tensor.

        Returns
        -------
        etas : (Natoms x Ndim) array
            The diagonal elements of the friction tensor.
        """
        if not self._created_etas:
            self.Natoms, self.Ndim = positions.shape
            self._etas = np.empty((self.Natoms, self.Ndim), order='C', dtype=np.float64)
            self._created_etas=True

        # alright, this is now our vectorized call... broadcasting, baby ;)
        self._etas[:,:] = self.friction_coefficient(positions)[:,np.newaxis]
        return self._etas

    def _create_info(self):
        info = self._name + " based on:"
        tmp = '\n' + str(self.friction_coefficient)
        tmp = tmp.replace('\n','\n\t')
        info += tmp
        return info
