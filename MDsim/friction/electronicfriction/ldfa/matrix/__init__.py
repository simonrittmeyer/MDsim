# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.friction import FrictionMatrix

class LDFAFrictionMatrix(FrictionMatrix):
    """
    Base class for any LDFA eta matrix. Note that this means that we have
    atomic friction coefficients. Note that you won't be able to use this class
    but only the IAA and AIM subclass derived thereof.

    Initialization
    --------------
    friction_coefficients : List of <Natoms> FrictionCoefficient instances.
        Friction coefficient instances which map positions to friction
        coefficients.
    """

    def __init__(self, friction_coefficients):
        # no convert flag at this point since the eta instances have to take
        # care of it!
        self.friction_coefficients = friction_coefficients
        self.Natoms = len(self.friction_coefficients)

        # do not want to call range function over and over again
        self._atoms_range = np.arange(0, self.Natoms)

        # the working array (not extended to Ndim)
        self._etas = np.empty(self.Natoms, dtype = np.float64, order = 'C')

        self._name = 'LDFA Friction Matrix'

        super(LDFAFrictionMatrix,self).__init__()

    def _create_info(self):
        info = self._name + " containing"
        for eta in self.friction_coefficients:
            tmp = '\n\n* ' + str(eta)
            tmp = tmp.replace('\n','\n\t')
            info += tmp
        return info

    def __str__(self):
        return self._create_info()
