# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

from MDsim import tools
from MDsim import units

from MDsim.friction.electronicfriction.ldfa.matrix import LDFAFrictionMatrix

    
class IAAFrictionMatrix(LDFAFrictionMatrix):
    """
    LDFA-Eta Matrix (atomic friction coefficients) within the independent-atom
    approximation (IAA). This means, each individual atomic eta instance is
    called via the position of the respective atom only. 
    
    Initialization
    --------------
    friction_coefficients : List of Natoms AtomicEtaIAA instances. 
        AtomicEta instances which map positions to friction coefficients. These
        may either be AIM or IAA instances, which means that each individual
        instance may either be called with the entire positions vector (AIM) or
        only with the position of the respective atom (IAA).
    """
    def __init__(self, *args, **kwargs):
        LDFAFrictionMatrix.__init__(self, *args, **kwargs)
        self._name = 'AIM Friction Matrix'

    def __call__(self, positions):
        """
        Evaluate the LDFA-IAA matrix at the specified positions.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the friction tensor.

        Returns
        -------
        etas : (Natoms x Ndim) array
            The diagonal elements of the friction tensor. 
        """
        for i in self._atoms_range:
            self._etas[i] = self.friction_coefficients[i](positions[i])
        # extend dimensionality
        return tools.extend_to_Ndim(self._etas, positions.shape[1])


