# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np

class LDFA(object):
    """
    Collected LDFA friction coefficients.

    Initialization
    --------------
    element : string
        Chemical element. Note that this is case sensitive.

    sp : boolean, optional (default = False)
        Use spin polarized friction coefficient if available.

    extrapolate : boolean, optional (default = False)
        Use the interpolation function to also extrapolate for rs values out of
        the given interpolation range. If <False> then the cutoff values at the
        edges will be used for any value outside the given rs range.

    """
    implemented = ['C', 'H', 'N', 'Li', 'K', 'Na', 'O', 'X']

    def __init__(self, element, sp = False, extrapolate = False):
        self.element = element
        self.sp = sp
        self.extrapolate = extrapolate
        if element == 'C':
            self.eta = lambda r_s : LDFA.eta_C(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'H':
            self.eta = lambda r_s : LDFA.eta_H(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'N':
            self.eta = lambda r_s : LDFA.eta_N(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'Li':
            self.eta = lambda r_s : LDFA.eta_Li(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'Na':
            self.eta = lambda r_s : LDFA.eta_Na(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'K':
            self.eta = lambda r_s : LDFA.eta_K(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'O':
            self.eta = lambda r_s : LDFA.eta_O(r_s, sp=self.sp, extrapolate=self.extrapolate)
        elif element == 'X':
            self.eta = lambda r_s : LDFA.eta_X(r_s, sp=self.sp, extrapolate=self.extrapolate)
        else:
            msg = 'Friction coefficient for {} not (yet) implemented'.format(element)
            raise NotImplementedError(msg)

    def __call__(self, rs):
        """
        Evaluate the respective friction coefficient.

        Parameters
        ----------
        rs : float or (N x 1) array
            Wigner-Seitz radii (rs) at which to evaluate the friction
            coefficient. Input in atomic units.

        Returns
        -------
        eta : float or (N x 1) array
            The respective friction coefficients.
        """
        return self.eta(rs)

    def __str__(self):
        if self.element == 'X':
            name = 'Dummy electronic friction coefficient (= 0 for any input)'
        else:
            name = 'Electronic friction coefficient for {0:s}'.format(self.element)
            name += '\n\t(sp = {0:s}, extrapolate = {1:s})'.format(str(self.sp),
                                                                   str(self.extrapolate))
        return name

    def show(self, rs_max = 5.):
        import matplotlib.pyplot as plt
        rs = np.linspace(1,rs_max,100)
        plt.plot(rs, self.eta(rs), lw = 2)
        plt.xlabel(r'$r_s$ (a.u.)')
        plt.ylabel(r'$\eta_\mathrm{%s}$ (a.u.)'%self.element)
        plt.show()

    # we implement the coefficients as static methods to avoid monkeypatching
    @staticmethod
    def eta_O(rs, sp=False, extrapolate=True):
        """
        Fitted function for oxygen, either spin polarized or not for the LDFA
        based friction coefficient of an oxygen ATOM in jellium Both input and
        output are in atomic Units. Both fits are valid within rs = 1 - 5.5.
        """

        rs = np.array(rs)

        if sp == False:
            p = [1.2214484526,
                 -1.99188262928,
                 0.00804974950106,
                 50.1289185681,
                 0.493063749991,
                 -2.6907911337]
        elif sp == True:
            p = [1.12042845e+00,
                -1.78635898e+00,
                -4.41875613e-02,
                 4.89656956e+01,
                 3.92198804e-01,
                 -2.62181417e+00]

        # respect the data range
        if not extrapolate:
            rs[rs > 5.5] = 5.5
            rs[rs < 1.] = 1.

        # return properly parametrized fit function
        return (p[0]*rs**(p[1])*np.exp(p[2]*rs)+p[3]*rs**(p[4])
                *np.exp(p[5]*rs))


    @staticmethod
    def eta_N(rs, sp=False, extrapolate=True):
        """
        Fitted function for nitrogen, non-spin polarized LDFA based friction
        coefficient of a nitrogen ATOM in jellium. both input and output are in
        atomic units

        functions fitted by Inaki Juaristi on 2014-09-17
        """
        rs = np.array(rs)

        # valid for values in between rs = 1-5!
        p = [39.2979,
            -0.127115,
            -0.837885,
            -34.62,
             0.332735,
            -0.999466]

        # respect the data range
        if not extrapolate:
            rs[rs > 5.] = 5.
            rs[rs < 1.] = 1.

        # return properly parametrized fit function
        return (p[0]*rs**(p[1])*np.exp(p[2]*rs)+p[3]*rs**(p[4])
                 *np.exp(p[5]*rs))

    @staticmethod
    def eta_Li(rs, sp=False, extrapolate=True):
        """
        Fitted function for lithium, non-spin polarized LDFA based friction
        coefficient of a nitrogen ATOM in jellium. both input and output are in
        atomic units

        functions fitted by Inaki Juaristi on 2016-03-16
        """
        rs = np.array(rs)

        # valid for values in between rs = 1-6!
        p = [12.8534,
             0.385615,
            -1.93097,
             4.21737e-2,
            -2.18419,
             0.560059,
             0.333958,
             7.34457,
            -3.42811]

        # respect the data range
        if not extrapolate:
            rs[rs > 6.] = 6.
            rs[rs < 1.] = 1.

        # return properly parametrized fit function
        return (p[0]*rs**(p[1])*np.exp(p[2]*rs)+
                p[3]*rs**(p[4])*np.exp(p[5]*rs)+
                p[6]*rs**(p[7])*np.exp(p[8]*rs))

    @staticmethod
    def eta_K(rs, sp=False, extrapolate=True):
        """
        Fitted function for potassium, non-spin polarized LDFA based friction
        coefficient of a nitrogen ATOM in jellium. both input and output are in
        atomic units

        functions fitted by Inaki Juaristi on 2016-03-16
        """
        rs = np.array(rs)

        # valid for values in between rs = 1.5-5.25!
        p = [23.5671,
             12.4528,
            -6.35591,
             2.36091e-8,
             16.0879,
            -2.63432,
             303.759,
             5.40341,
            -4.66122]

        # respect the data range
        if not extrapolate:
            rs[rs > 5.25] = 5.25
            rs[rs < 1.5] = 1.5

        # return properly parametrized fit function
        return (p[0]*rs**(p[1])*np.exp(p[2]*rs)+
                p[3]*rs**(p[4])*np.exp(p[5]*rs)+
                p[6]*rs**(p[7])*np.exp(p[8]*rs))

    @staticmethod
    def eta_H(rs, sp=False, extrapolate=True):
        """
        Fitted function for hydrogen, non-spin polarized LDFA based friction
        coefficient of a hydrogen ATOM in jellium; both input and output are in
        atomic units

        functions fitted by Inaki Juaristi on 2015-05-27
        """

        rs = np.array(rs)

        # valid for values in between rs = 1-10!
        p = [8.25494e-8,  10.0823,   -1.18939,
             0.651365,     0.375934, -0.605298,
             0.00062239, -14.0174,    2.47727]

        # respect the data range
        if not extrapolate:
            rs[rs > 10.] = 10.
            rs[rs < 1.] = 1.

        # return properly parametrized fit function
        return (p[0]*rs**(p[1])*np.exp(p[2]*rs)
               +p[3]*rs**(p[4])*np.exp(p[5]*rs)
               +p[6]*rs**(p[7])*np.exp(p[8]*rs)
               )

    @staticmethod
    def eta_C(rs, sp=False, extrapolate=True):
        """
        fitted functions for carbon, either spin polarized or not for the LDFA
        based friction coefficient of a carbon ATOM in jellium; both input and
        output are in atomic units

        functions fitted by Inaki Juaristi on 2014-01-31
        """

        rs = np.array(rs)

        if sp == False:
            # valid for values in between rs = 1-6!
            p = [22.654,
                 2.004,
                -3.134,
                 2.497,
                -2.061,
                 0.0793]

            # respect the data range
            if not extrapolate:
                rs[rs > 6.] = 6.
                rs[rs < 1.] = 1.

            # return properly parametrized fit function
            return (p[0]*rs**(p[1])*np.exp(p[2]*rs)+p[3]*rs**(p[4])
                     *np.exp(p[5]*rs))

        elif sp == True:
            # valid within rs = 1 - 7
            p = [138.975,
                -0.6825,
                -0.3444,
                +0.0006575,
                 22.0216,
                -6.495,
                -134.523,
                -0.6484,
                -0.350]

            # respect the data range
            if not extrapolate:
                rs[rs > 7.] = 7.
                rs[rs < 1.] = 1.

            # return properly parametrized fit function
            return (p[0]*rs**(p[1])*np.exp(p[2]*rs)+p[3]*rs**(p[4])
                     *np.exp(p[5]*rs)+p[6]*rs**(p[7])
                     *np.exp(p[8]*rs))


    @staticmethod
    def eta_Na(rs, sp, extrapolate = True):
        """
        Fitted function for sodium, non-spin polarized LDFA based friction
        coefficient of a sodium ATOM in jellium. Both input and output are in
        atomic units

        functions fitted by Inaki Juaristi on 2015-07-17
        """

        rs = np.array(rs)

        # valid for values in between rs = 1-10.75!
        p = [205.018,     0.520242, -3.87791,
               0.716797,  5.16723,  -3.08227,
               0.0003886, 4.676,    -0.707585]

        # respect the data range
        if not extrapolate:
            rs[rs > 10.75] = 10.75
            rs[rs < 1.] = 1.

        # return properly parametrized fit function
        return (p[0]*rs**(p[1])*np.exp(p[2]*rs)
               +p[3]*rs**(p[4])*np.exp(p[5]*rs)
               +p[6]*rs**(p[7])*np.exp(p[8]*rs)
               )

    @staticmethod
    def eta_X(rs, *args, **kwargs):
        """
        Dummy function, will always return 0.
        """
        return np.zeros_like(rs)

    @staticmethod
    def show_all(rs_max = 5.):
        import matplotlib.pyplot as plt
        rs = np.linspace(1,rs_max,100)

        plt.plot(rs, LDFA.eta_C(rs,True), lw = 2, label = 'C')
        plt.plot(rs, LDFA.eta_H(rs,True), lw = 2, label = 'H')
        plt.plot(rs, LDFA.eta_N(rs,True), lw = 2, label = 'N')
        plt.plot(rs, LDFA.eta_O(rs,True), lw = 2, label = 'O')
        plt.plot(rs, LDFA.eta_Na(rs,True), lw = 2, label = 'Na')
        plt.plot(rs, LDFA.eta_Li(rs,True), lw = 2, label = 'Li')
        plt.plot(rs, LDFA.eta_K(rs,True), lw = 2, label = 'K')

        plt.xlabel(r'$r_s$ (a.u.)')
        plt.ylabel(r'$\eta$ (a.u.)')

        plt.legend()
        plt.show()
