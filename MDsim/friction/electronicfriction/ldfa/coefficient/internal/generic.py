# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

from MDsim import units
from MDsim.interpolation.bivariatespline.internal.generic import GenericDOFBivariateSpline

class GenericDOFBivariateFrictionCoefficient(GenericDOFBivariateSpline):
    """
    Potential that handles generic coordinates other than the usual cartesian
    (Natoms, Ndim) scheme, ie., generic DOF where each atom is only one
    dimension and a separate mass.

    Initialization
    --------------
    points : (Npoints, 2) array
        Numpy array containing all points that shall enter the interpolation.
        These are in internal coordinates!

    etas : (Npoints,) array
        Friction coefficients corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    convert : boolean, optional (default = True)
        Convert from amu/fs and Angstrom to a.u. If <False>, then input in a.u.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """

    def __init__(self, points, etas, **kwargs):

        # potential-related
        if 'convert' not in kwargs.keys():
            kwargs['convert'] = True

        # leave it in the kwargs as we need it again for the coordinate
        # transfer
        convert = kwargs.pop('convert')

        if convert:
            # explicit copies here!
            points = points * units.ANGSTROM_TO_AU
            etas = etas * units.AMU_PER_FS_TO_AU

        GenericDOFBivariateSpline.__init__(self,
                                           points=points,
                                           values=etas,
                                           **kwargs)

        self._name = "GenericDOFBivariateFrictionCoefficient\n\t(interpolated biariate spline-friction coefficient)"


    def __call__(self, positions):
        return self.get_value(positions)
