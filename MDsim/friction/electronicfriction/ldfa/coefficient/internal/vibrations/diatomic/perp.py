# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to handle potentials that are spanned on a dRcom, ddist grid
"""

from __future__ import print_function
import numpy as np

from MDsim import units
from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp
from MDsim.interpolation.bivariatespline.internal.vibrations.diatomic.perp import DiatomicPerpSpline


class DiatomicPerpFrictionCoefficient(DiatomicPerpSpline):
    """
    Class that handles the bivariate spline interpolation for each ATOMIC
    friction coefficient.

    Note that within the LDFA formalism, the atomic
    friction coefficient is a function of an input density only. Yet, for
    efficiency reasons, we rather map this coeffient on a real-space grid to
    avoid unnecessary calls pos -> density -> eta. This is a very convenient
    way to do for small systems.

    However, as a result, also the IAA coefficients should be mapped over the
    full grid, ie. there is *no formal difference between AIM and IAA flavor*.

    Moreover, do not be confused by the fact that we use
    "InternalBivariatSpline" as a basis class here to evaluate atomic friction
    coefficients. The way we call this routine is independent of the fact that
    LDFA always gives us atomic friction coefficients that create a diagonal
    matrix in cartesian space. Transforming the latter into any other space
    really brings you into trouble in defining a new integrating scheme.

    Initialization
    --------------
    masses : (2,) array
        The masses of the atoms.

    points : (Npoints, 2) array
        Numpy array containing all points that shall enter the interpolation.
        These are in internal coordinates!

    etas : (Npoints,) array
        friction coefficients corresponding to the <points>.

    positions_eq : (2,1) array, optional (default=None)
        In case you want to use a constant offset converting from internals to
        cartesian evv. This is the offset in cartesian coordinates.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """
    def __init__(self, masses, points, etas, **kwargs):

        # potential-related
        if 'convert' not in kwargs.keys():
            kwargs['convert'] = True

        # leave it in the kwargs as we need it again for the coordinate
        # transfer
        convert = kwargs.get('convert')

        if convert:
            # explicit copies here!
            points = points * units.ANGSTROM_TO_AU
            etas = etas * units.AMU_PER_FS_TO_AU

        # masses are converted only later
        DiatomicPerpSpline.__init__(self,
                                    points=points,
                                    values=etas,
                                    masses=masses,
                                    **kwargs)

        self._name = "DiatomicPerpFrictionCoefficient\n\t(interpolated biariate splined friction coefficient)"


    def __call__(self, positions):
        return self.get_value(positions)

