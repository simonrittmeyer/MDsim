# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

from MDsim import units
from MDsim.interpolation.bivariatespline.atomicbivariatespline import AtomicBivariateSpline

class AtomicBivariateFrictionCoefficient(AtomicBivariateSpline):
    """
    Class that handles the bivariate spline interpolation for each ATOMIC
    friction coefficient.

    This function is geared towards the following scenario: You can determine
    the friction coefficient with the atomic position in cartesian space only.
    This may be the case within the independent atoms approximation, or as well
    within the atom-in-molecule approximation if one only considers eg. atomic
    diffusion. The important thing is that there will be no coordinate
    transformation whatsoever.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    etas : (Npoints) array
        Friction coefficients corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    _bounds_check : boolean, optional (default = True)
        Whether to perform a bounds check upon calling the interpolation. If
        <True> raises a ValueError if out-of-bounds. Only turn false if you
        know what you are doing, as extrapolating with interpolation splines is
        an extremely bad idea.
    """

    def __init__(self, points, etas,
                 function = 'RectBivariateSpline',
                 kind = 'cubic',
                 data_origin = 'None',
                 convert = True,
                 normalize = True,
                 _bounds_check = True):


        if convert:
            points *= units.ANGSTROM_TO_AU
            etas *= units.AMU_PER_FS_TO_AU

        AtomicBivariateSpline.__init__(self, points=points,
                                       values=etas,
                                       function=function,
                                       kind=kind,
                                       data_origin=data_origin,
                                       _bounds_check=_bounds_check)

        self._name = "AtomicBivariateFrictionCoeffient"
        self._name +="\n\t(interpolated atomic friction coefficient)"


    def __call__(self, positions):
        """
        Caution: You can use this thing very harmfully!
        If you just feed in the system positions vector, we will interprete
        each position in there as the equal atom type (friction wise) but only
        at different positions.
        """
        return self.get_value(positions)
