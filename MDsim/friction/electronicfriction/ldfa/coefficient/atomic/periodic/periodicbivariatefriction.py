# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra, numerics
import numpy as np

from MDsim import units
from MDsim.interpolation.bivariatespline.periodic.periodicatomicbivariatespline import PeriodicAtomicBivariateSpline

class PeriodicAtomicBivariateFrictionCoefficient(PeriodicAtomicBivariateSpline):
    """
    Class that handles the periodic bivariate spline interpolation for each
    ATOMIC friction coefficient.

    Note that within the LDFA formalism, the atomic friction coefficient is a
    function of an input density only. Yet, for efficiency reasons, we rather
    map this coeffient on a real-space grid to avoid unnecessary calls pos ->
    density -> eta. This is a very convenient way to do for small systems.

    However, as a result, also the IAA coefficients should be mapped over the
    full grid, ie. there is *no formal difference between AIM and IAA flavor*.

    Moreover, do not be confused by the fact that we use
    "InternalBivariatSpline" as a basis class here to evaluate atomic friction
    coefficients. The way we call this routine is independent of the fact that
    LDFA always gives us atomic friction coefficients that create a diagonal
    matrix in cartesian space. Transforming the latter into any other space
    really brings you into trouble in defining a new integrating scheme.

    Initialization
    --------------
    points : (Npoints x 2) array
        Numpy array containing all points that shall enter the interpolation.

    etas : (Npoints) array
        friction coefficients corresponding to the <points>.

    function : string, optional ({'SmoothBivariateSpline', *'RectBivariateSpline'*})
        SciPy routine that will be employed to interpolate the grid.
        Note that 'RectBivariateSpline' is very fast and reliable but only
        works on regular, rectangular grids. 'SmoothBivariateSpline' also works
        on irregular grids but may induce artifacts. Check carefully.

    kind : string, optional ({'linear', 'cubic', 'quintic'})
        Degree of the bivariate splines unerlying the interpolation.

    data_origin : string , optional (default= '(not available)')
        String specifying the origin of the data (if wanted). Can be useful for
        IO reasons.

    convert : boolean, optional (default = True)
        Convert from eV and Angstrom to a.u. If <False>, then input in a.u.

    """

    def __init__(self, points, etas,
                 function = 'RectBivariateSpline',
                 kind = 'cubic',
                 data_origin = 'None',
                 convert = True
                 ):

        if convert:
            points *= units.ANGSTROM_TO_AU
            etas *= units.AMU_PER_FS_TO_AU

        PeriodicAtomicBivariateSpline.__init__(self, points = points,
                                               values = etas,
                                               function = function,
                                               kind = kind,
                                               data_origin = data_origin)


        self._name = "PeriodicBivariateFrictionCoefficient"
        self._name += "\n\t(interpolated atomic friction coeffieicnt with Born-von Karman PBC wrap"
        self._name += "\n\t of positions at interpolation boundaries)"


    def __call__(self, positions):
        """
        Caution: You can use this thing very harmfully!
        If you just feed in the system positions vector, we will interprete
        each position in there as the equal atom type (friction wise) but only
        at different positions.
        """
        return self.get_value(positions)
