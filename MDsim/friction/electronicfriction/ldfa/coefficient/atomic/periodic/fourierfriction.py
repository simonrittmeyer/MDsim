# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy import linalg

from MDsim import units
from MDsim.interpolation.fourier import FourierExpansion
from MDsim.interpolation.fourier import fit_fourier_expansion

class FourierFriction(FourierExpansion):
    """
    Class for a Fourier-expanded friction coefficient. This is basically just a
    wrapper around the respective FourierExpansion class, so many details can
    be found there.

    The actual evaluation of the Fourier expansion and its derrivative is done
    on a F90 level supported with BLAS routines to gain an optimal performance.

    The potential is expanded according to the one mentioned by Matt Probert in
    his (yet) unpublished manuscript. TODO: Add reference.

    Initialization
    --------------
    G : (Nlattice_G x Ndim) array
        Numpy array containing the reciprocal lattice vectors for the real part
        of the Fourier expansion, ie. the cosine terms. Please, do note the
        dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    H : (Nlattice_H x Ndim) array
        Numpy array containing the reciprocal lattice vectors for the imaginary
        part of the Fourier expansion, ie. the sine terms. Please, do note
        the dimensionality of this matrix: First index numbers the respective
        reciprocal lattice vector, and second index gives the respective
        cartesian coordinates.

    points : (Npoints x Ndim) array
        Numpy array containing all points that shall enter the interpolation or
        least square approximation of the potential.

    etas : (Npoints) array
        Friction vaules corresponding to the <points>.

    real_order : integer
        Order of the Fourier expansion in cosine contributions, ie. the real
        part.

    imag_order : integer
        Order of the Fourier expansions in sine contributions, ie. the
        imaginary part.

    constant : Boolean, optional (default = True)
        Flag wether a constant m=n=0 amplitude shall be fitted or not. Note
        that this functionality requires at least one more independent input
        point.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/AMU per fs to a.u. or not. If set False, then
        input must be provided in a.u.. Output will always be in a.u.


    Note
    ----
    If <real_order> + <imag_order> < Npoints, there will be a least square fit
    to the given datapoints. You will have an exact interpolation only if
    <real_order> + <imag_order> == Npoints. This is yet not recommended, as you
    usually will require very high frequencies that cause highly unphysical
    oscillations in the potential. Definietly check the quality of your fit
    before you apply it in any simulation.
    """
    def __init__(self, G, H, points, etas, real_order, imag_order,
                       constant = True, convert = True):

        # init the parent
        FourierExpansion.__init__(self, G, H)

        # fit coefficients and assign
        self._input_points = np.asarray(points, dtype = np.float64, order = 'C')
        self._input_etas = np.asarray(etas, dtype = np.float64, order = 'C')

        if convert:
            self._input_points *= units.ANGSTROM_TO_AU
            self._input_etas *= units.AMU_PER_FS_TO_AU

        self.fit(points = self._input_points,
                 values = self._input_etas,
                 real_order = real_order,
                 imag_order = imag_order,
                 constant = constant)

        self._name = 'Periodic friction coefficient based on a Fourier Expansion'


    def get_etas(self, positions):
        """
        Evaluate the friction coefficitn at given positions and return each indiviudal
        atomic contribution to the potential energy.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Positions at which to evaluate the potential. If this array is not
            Fortran contigious, there a F-contigious copy thereof will be
            created. Thus, you can gain some speedup upon calling this with
            'F'-contigious input arrays.

        Returns
        -------
        etas : (Natoms x 1) array
            The values at respective positions. This array is
            Fortran-contigious, yet it does not matter for 1D arrays. Please
            note that the entries in "etas" are a single number per atom!
            Splitting into contributions along coordinate axes is just not
            possible in general.

        Raises
        ------
        ValueError : If underlying expansion has not been initialized.
        """
        return self.get_value(positions)

    def __call__(self, positions):
        """
        Just an alias provided for convenience
        """
        return self.get_etas(positions)
