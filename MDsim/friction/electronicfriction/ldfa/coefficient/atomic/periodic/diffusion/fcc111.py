# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy import linalg

from MDsim import units

from MDsim.friction.electronicfriction.ldfa.coefficient.atomic.periodic.fourierfriction import FourierFriction
from MDsim.coordinates.diffusion.fcc111 import create_sites
from MDsim.coordinates.diffusion.fcc111 import create_real_space_lattice
from MDsim.coordinates.diffusion.fcc111 import create_reciprocal_lattice

class FourierFrictionFCC111(FourierFriction):
    """
    This is a Fourier friction coefficient to model diffusion on a hexagonal
    fcc(111) surface. The potential is created according to Matt Probert's yet
    unpublished work but with an arbitrary number of coefficients.

    Initialization
    --------------
    a : float
        The surface lattice constant, ie. a = d / sqrt(2) where 'd' is the
        fcc bulk lattice constant.

    points : (Npoints x Ndim) array
        Numpy array containing all points that shall enter the interpolation or
        least square approximation of the potential.

    etas : (Npoints) array
        Friction coefficients corresponding to the <points>.

    real_order : integer
        Order of the Fourier expansion in cosine contributions, ie. the real
        part.

    imag_order : integer
        Order of the Fourier expansions in sine contributions, ie. the
        imaginary part.

    constant : Boolean, optional (default = True)
        Flag wether a constant m=n=0 amplitude shall be fitted or not. Note
        that this functionality requires at least one more independent input
        point.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/amu per fs to a.u. or not. If set False, then
        input must be provided in a.u.
    """
    def __init__(self, a, points, etas, real_order, imag_order,
                       constant = True, convert = True):

        # the real space surface lattice vectors
        self.a = a

        if convert:
            self.a *= units.ANGSTROM_TO_AU

        self.lattice = create_real_space_lattice(self.a)

        G, H = create_reciprocal_lattice(self.a)

        # init the parent class
        FourierFriction.__init__(self, G = G,
                                        H = H,
                                        points = points,
                                        etas = etas,
                                        real_order = real_order,
                                        imag_order = imag_order,
                                        constant = constant,
                                        convert = convert)

        # Name the thing
        self._name = '2D Fourier friction coefficient for FCC(111) surface'
        self._name += '\n\tsurface lattice constant : {0:.3f} a.u. = {1:.3f} Angstrom'.format(self.a, self.a * units.AU_TO_ANGSTROM)


    def _create_lattice_plot(self):
        """
        Simple function that creates a 2D surface plot of the expansion + the
        underlying lattice.

        This routine first calls "_create_contour_plot" and then adds some
        additional stuff.

        Returns
        -------
        plot_dict : Dictionary
            Dictionary with some features of the plot. In particular:

                'fig' : matplotlib figure instance
                    The figure to which <ax> is attached.

                'ax' : matplotlib axes instance
                    Axes instance with the contour plot on it.

                'lim' : (2 x 1) array
                    The plot limits in x and y direction.

                'repeats' : int
                    The number of repeating units of the unit cell

                'a' : float
                    The applied lattice constant

                'cbar' : matplotlib colorbar instance
                    The colorbar corresponding to the contour plot.

                'cont' : matplotlib contours instance
                    The contour plot instance.

                'surf' : matplotlib pcolormesh instance
                    The actual 2D plot.
        """
        plot_dict = self._create_contour_plot()

        ax = plot_dict['ax']
        a = plot_dict['a']
        repeats = plot_dict['repeats']

        # the lattice
        for n in np.arange(-repeats, repeats+1):
            for m in np.arange(-repeats, repeats + 1):
                ax.plot(*((m*self.lattice[0]+n*self.lattice[1])),
                        marker = 'o',
                        markersize = 8,
                        color = 'lightgray',
                        markeredgecolor = 'black')

        # the unit cell
        for l in self.lattice:
            ax.arrow(0, 0, l[0], l[1],
                     head_width=0.1,
                     linewidth = 2,
                     length_includes_head = True,
                     head_length=0.1,
                     zorder = 5,
                     color='black')

        return plot_dict


    def _create_cut_plots(self, sites):
        """
        Create some plots cutting along certain lines
        """
        import matplotlib.pyplot as plt

        # get some increments
        npts = 200
        alphas = np.linspace(0,1,npts)

        # distance from top to top over the bridge site
        _len = 2*self.a*np.cos(np.pi/6.)

        # positions along the cuts
        cuts = {# top -- fcc -- bridge -- hollow -- top
                'tfbht' : np.array([i*_len*np.array([1,0]) for i in alphas]),
                # top -- bridge -- top
                'tbt'   : np.array([i*self.lattice[0] for i in alphas]),
                # top -- fcc -- top
                'tft'   : np.array([i*self._sites['fcc'] for i in alphas[::2]]
                                 + [(1-i)*self._sites['fcc'] for i in alphas[::2]]),
                # top -- hcp -- top
                'tht'   : np.array([i*self._sites['hcp'] for i in alphas[::2]]
                                 + [(1-i)*self._sites['hcp'] for i in alphas[::2]])
                }

        labels = {'tfbht' : ['top', 'fcc', 'bridge', 'hollow', 'top'],
                  'tbt'   : ['top', 'bridge', 'top'],
                  'tft'   : ['top', 'fcc', 'top'],
                  'tht'   : ['top', 'hcp', 'top']}

        # positions of the labels
        labelpos = {'tfbht' : [0, self._sites['fcc'][0]/_len, .5, (self._sites['hcp'] + self.lattice[0])[0] / _len , 1],
                    'tbt'   : [0, .5, 1],
                    'tft'   : [0, .5, 1],
                    'tht'   : [0, .5, 1]}

        # evaluate the energy using the vectorized call ;)
        eta_cuts = dict()
        for key in cuts.keys():
            eta_cuts[key] = self.get_etas(cuts[key])


        # plot the cuts
        maxE = max([max(e) for e in eta_cuts.values()])
        minE = min([min(e) for e in eta_cuts.values()])

        fig = plt.figure()

        for i,key in enumerate(cuts.keys()):
            ax_cut = fig.add_subplot(2, 2, i+1)
            eta = eta_cuts[key]
            ax_cut.plot(alphas, eta, color = 'blue')

            ax_cut.set_xticks(labelpos[key])
            ax_cut.set_xticklabels(labels[key], rotation=45)
            ax_cut.set_xlabel('position')
            ax_cut.set_ylabel('eta(x,y) (a.u.)')

            ax_cut.set_ylim(minE, maxE)

            # make it a square plot
            x0,x1 =  ax_cut.get_xlim()
            y0,y1 =  ax_cut.get_ylim()

            ax_cut.set_aspect(abs(x1-x0)/abs(y1-y0))


    def show(self):
        """
        Visualize the potential.
        """
        import matplotlib.pyplot as plt
        plot_dict = self._create_lattice_plot()

        # call it a potential
        cbar = plot_dict['cbar']
        cbar.set_label(r'eta(x,y) (a.u.)')
        plt.show()


    def show_cuts(self, sites):
        import matplotlib.pyplot as plt
        self._check_site_names(sites)
        self._create_cut_plots(sites)
        plt.show()



class MinimumBasisFourierFrictionFCC111(FourierFrictionFCC111):
    """
    This is a minimum basis Fourier friction coefficient to model diffusion on
    a hexagonal fcc(111) surface. The potential is created according to Matt
    Probert's yet unpublished work. We basically use two real coefficients, and
    a single imaginary coefficient which allows to distinguish between fcc and
    hcp hollow sites.

    The lattice vectors are defined as

        l1 = a * [ sin(2*pi/3), cos(2*pi/3) ]
        l2 = a * [0, 1]

    in cartesian coordinates, where 'a' is the surface lattice constant. For
    details, see the routine "create_real_space_lattice()" from
    MDsim.coordinates.diffusion.fcc111.

    By default, we  define the considered adsorption sites as

             'top'    = (0, 0)
             'bridge' = 0.5 * l1 or 0.5 * l2
             'hcp'    = 1./3. * l1 + 2./3. * l2
             'fcc'    = 2./3. * l1 + 1./3. * l2

    For details see the routine "create_sites()" from
    MDsim.coordinates.diffusion.fcc111.

    Yet, you may change it by passing a different "sites" dictionary upon
    initialization.


    Initialization
    --------------
    a : float
        The surface lattice constant, ie. a = d / sqrt(2) where 'd' is the
        fcc bulk lattice constant.

    etas_dict : dictionary
        Dictionary that holds the etas of the sites specified above a
        values, and the site names {'top', 'bridge', 'hcp', 'fcc'} as keys.

    convert : Boolean, optional (default = True)
        Whether to convert from Angstrom/amu per fs eV to a.u. or not. If set
        False, then input must be provided in a.u.

    sites : dictionary, optional (default = None)
        Dictionary holding the positions of the respective adsorption sites as
        items in form of (2x1) arrays, and the site names {'top', 'bridge',
        'hcp', 'fcc'} as keys. If None is passed, the default definition from
        above applies.
    """

    _site_names = ['top', 'bridge', 'hcp', 'fcc']

    def __init__(self, a, etas_dict, convert = True, sites = None):
        if sites is None:
            self._sites = create_sites(a)
        else:
            self._check_site_names(sites.keys())
            self._sites = sites

        self._check_site_names(etas_dict.keys())
        self._input_etas_dict = etas_dict


        # create the data for the fit, unit conversion is done in the parent class
        points = np.array([self._sites[n] for n in self._site_names], dtype = np.float64, order = 'C')
        etas = np.array([self._input_etas_dict[n] for n in self._site_names], dtype = np.float64, order = 'C')

        # fit the coefficients
        FourierFrictionFCC111.__init__(self, a = a,
                                              points = points,
                                              etas = etas,
                                              real_order = 2,
                                              imag_order = 1,
                                              constant = True,
                                              convert = convert)

        # unit conversion
        if convert:
            for n in self._site_names:
                self._input_etas_dict[n] *= units.AMU_PER_FS_TO_AU
                self._sites[n] *= units.ANGSTROM_TO_AU

        self._name = 'Minimum Basis 2D Fourier Friction for FCC(111) surface'
        self._name += '\n\tsurface lattice constant : {0:.3f} a.u. = {1:.3f} Angstrom'.format(self.a, self.a * units.AU_TO_ANGSTROM)
        self._name += '\n'
        self._name += '\nInput adsorption sites:'
        self._name += '\n-----------------------'
        for key, val in self._input_etas_dict.items():
            self._name += '\n\t{0:6s} : {1:+.6f} a.u. = {2:+.6f} amu / fs'.format(key, val, val * units.AU_TO_AMU_PER_FS)


    def _check_site_names(self,names):
        """
        Check sites names that are used as keys in etas and sites dictionary

        Parameters
        ----------
        names : list of strings
            The keys of the respective dictionary.

        Returns
        -------
        <True> if everything is ok.

        Raises
        ------
        NotImplementedError : if some keys are invalid.
        """
        for n in names:
            if not n in self._site_names:
                print('Error : site name "{}" not recognized!'.format(n))
                print('        Must be one of "top", "bridge", "hcp", "fcc".')
                raise NotImplementedError
        return True

    def show_cuts(self):
        import matplotlib.pyplot as plt
        self._create_cut_plots(self._sites)
        plt.show()

