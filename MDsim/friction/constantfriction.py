# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# linear algebra
import numpy as np
from MDsim import units

from MDsim.friction import FrictionMatrix

class ConstantFriction(FrictionMatrix):
    """
    Constant eta matrix that can be used just like an friction_matrix which is
    position dependent.

    Initialization
    --------------
    friction_matrix : (Natoms x Ndim) array
        The constant eta matrix, or the diagonal elements respectively.

    convert : Boolean, optinal (default = True)
        Whether to convert from AMU/fs to a.u. or not. If <False> then input in
        a.u.
    """

    def __init__(self, friction_matrix, convert=True):
        self.friction_matrix = np.asarray(friction_matrix, order='C', dtype=np.float64)
        if convert:
            self.friction_matrix *= units.AMU_PER_FS_TO_AU

        self.Natoms, self.Ndim = self.friction_matrix.shape

        super(ConstantFriction, self).__init__()

    def __call__(self, positions):
        """
        Return the constant eta matrix.

        Parameters
        ----------
        positions : (Natoms x Ndim) array
            Just a dummy argument with no effect.

        Returns
        -------
        etas : (Natoms x Ndim) array
            The eta matrix, or the diagonal elements thereof.
        """
        return self.friction_matrix

    def __str__(self):
        name  = "ConstantFriction instance with the following values"
        name += "\n\n"

        if np.all(self.friction_matrix == self.friction_matrix[0,0]):
            name += '--> homogeneous matrix with elements {{eta_ij}} = {:12.6e} a.u.'.format(self.friction_matrix[0,0])
        else:
            for i in range(self.Ndim):
                name += "{:>20s}".format("eta_{} (a.u.)".format(['x','y','z'][i]))
            name += '\n' + '-'*20*self.Ndim

            for eta in self.friction_matrix:
                name += '\n'
                for eta_i in eta:
                    name += '{:>+20.6e}'.format(eta_i)
        return name
