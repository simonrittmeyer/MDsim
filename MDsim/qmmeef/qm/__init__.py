# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from MDsim.systems.system import System
from castepqm import CastepQM

class QMstubs(System):
    """
    A dummy class that provides all necessary routines in addition to those
    provided by the (ASE)System as stubs. These stubs raise errors.

    This class is just there to help you with implementing any other DFT code
    than CASTEP.
    """
    def __init__(self, *args, **kwargs):
        raise NotImplementedError
    def get_Nslab(self):
        raise NotImplementedError
    def get_slab_idx(self):
        raise NotImplementedError
    def get_adsorbate_idx(self):
        raise NotImplementedError
    def calc_forces_qm(self, positions, _convert=False):
        raise NotImplementedError
    def calc_potential_energy_slab(self, positions, _convert=False):
        raise NotImplementedError
    def pre_integration_hook(self):
        raise NotImplementedError
    def post_integration_hook(self):
        raise NotImplementedError
    def get_dissipated_energy(self):
        raise NotImplementedError
