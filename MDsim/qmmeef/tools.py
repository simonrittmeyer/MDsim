# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

from ase import Atoms

from MDsim import tools
from MDsim.qmmeef.coordinates import Coordinates

def cut_qm_from_me(me_atoms,
                   me_size,
                   qm_size,
                   _qm_lattice_scale=1.,
                   _decimals=6,
                   _return_idx=False,
                   _verbose=True):
    """
    Routine that calculates the indices of an Me system that belong to a QM
    system as well, ie. it cuts out a part of the Me-cell. The shape of the
    unit cell will remain unchanged as well as the absolute vacuum distance.

    Parameters
    ----------
    me_atoms : ASE atoms instance
        The atoms symbolizing the Me region.

    me_size : list/array of 3 integers
        The Me supercell size. It will be checked, whether the atoms object
        actually is commensurate with this cell size.

    qm_size : list/array of 3 integers
        The desired QM cell size to be cut out.

    _qm_lattice_scale : float, optional (default = 1.)
        In case of differing lattice constants "a_X" in the QM and Me part due
        to different potentials, this is the ratio "a_QM / a_Me". Will be used
        to properly scale positions and the unit cell.

    _decimals : integer, optional (default = 6)
        Tolerance of the position-comparison routines. This allows to avoid
        errors due to numerical roundoff-erros in the coordinate
        transformations of non-orthorhombic cells. Do not specify too low to
        avoid mis-assignments.

    _return_idx : boolean, optional (default = False)
        Return also the indices of the QM atoms as defined in the Me atoms
        object, ie. a filter.

    _verbose : boolean, optional (default = True)
        Print some information to stdout.

    Returns
    -------
    qm_atoms : ASE atoms instance
        The QM-atoms. The unit cell is properly adjusted, yet thet positions
        are NOT folded back.

    idx : Array of (prod(qm_size)) integers, optional
        The indices of the Me-atoms corresponding to the QM region. Will only
        be returned if <_return_idx> = True.

    """
    # make sure that we can work on these arrays as expected
    me_size = np.asarray(me_size, dtype = int)
    qm_size = np.asarray(qm_size, dtype = int)

    if np.any(qm_size > me_size):
        msg='QM cell is larger than Me cell.'
        raise ValueError(msg)

    if np.all(qm_size == me_size):
        msg="QM cell is the same as Me cell. What's the point in doing QM/Me then?"
        raise ValueError(msg)

    if _verbose:
        print('Cutting QM cell from Me cell')
    atoms = me_atoms.copy()

    # get the upper layer indices
    maxZ = np.max(atoms.get_positions()[:,2])

    # tolerance for upper layer (should be zero in principle, but numerics...)
    tol = 1**(-_decimals)
    upper_idx = [atom.index for atom in atoms if np.abs(atom.position[2] - maxZ) < tol]

    # Transform into orthogonal cell-vector basis. Required in case we do not
    # have orthorhombic cells.
    #
    # Nomenclature
    # -------------
    # If Q is the matrix consisting of the grid basis vectors in Cartesian
    # coordinates, then
    #
    #        x = Q y
    #
    # where x is the position vector in Cartesian space and y is the position
    # vector in ``grid space``. For this formular to work, we require Q to be
    # built of the grid basis vectors columnwise! Hence,
    #
    #        y = Qinv x
    #
    # is the transformation we ultimately need. Luckily this is only 3D, thus
    # the inverse is numerically basically for free.
    #
    Q = atoms.get_cell().transpose().copy()
    Qinv = np.linalg.inv(Q)

    # we require a rounding here, ie. some tolerance. Otherwise, numerical
    # uncertainties kill us...
    positions_cell = np.round(np.dot(Qinv, atoms.get_positions().T).T, decimals=_decimals)

    # sanity checks
    if _verbose:
        print('\tChecking periodicity of Me cell')

    for i in range(2):
        if not len(np.unique(positions_cell[upper_idx,i])) == me_size[i]:
            msg = 'Input Me cell does not fit atoms object in dimension {}.'.format(i)
            raise ValueError(msg)

    if _verbose:
        print('\tTransforming to fractional coordinates')


    # calculate from which atom we want to start (laterally) and where to end
    start = (me_size[0:2] - qm_size[0:2]) / 2
    end = start + qm_size[0:2]

    x_unique = np.unique(positions_cell[upper_idx,0])
    xmin = x_unique[start[0]]
    xmax = x_unique[end[0]]

    y_unique = np.unique(positions_cell[upper_idx,1])
    ymin = y_unique[start[1]]
    ymax = y_unique[end[1]]

    # z will just be from the top down
    z_unique = np.unique(positions_cell[:,2])[::-1]
    zmax = z_unique[0]

    # in case, we have the same size in QM and Me cell..
    zmin = z_unique[qm_size[-1]-1]

    if _verbose:
        print('\tAssigning atoms')

    idx = []
    for i in range(np.prod(me_size)):
        x, y, z = positions_cell[i,:]
        if (xmin <= x < xmax and
            ymin <= y < ymax and
            # yes indeed, "less-equal" appears twice
            zmin <= z <= zmax):
            idx.append(i)

    idx = np.asarray(idx)

    qm_atoms = atoms.copy()[idx]

    # This far, everything was done in Me coordinates. Now we rescale the QM part.
    coords = Coordinates(qm_lattice_scale=_qm_lattice_scale, Ndim=3)

    # scale the positions (also z)
    qm_atoms.set_positions(coords.points_me_to_qm(qm_atoms.get_positions()))

    # lastly, downscale the unit cell
    qm_cell = atoms.get_cell()
    qm_cell[0:2,:] *= np.true_divide(qm_size[0:2],me_size[0:2])[:,np.newaxis]

    # this one also rescales the z-coordinte (fixed afterwards)
    qm_cell = np.round(coords.points_me_to_qm(qm_cell), decimals=_decimals)

    # scaling the z-positions messes up the vacuum spacing... we want the
    # absolute vacuum distance to be equal.
    me_vacuum = atoms.get_cell()[-1,-1] - (np.max(atoms.get_positions()[:,-1]) - np.min(atoms.get_positions()[:,-1]))
    qm_vacuum = qm_cell[-1,-1] - (np.max(qm_atoms.get_positions()[:,-1]) - np.min(qm_atoms.get_positions()[:,-1]))

    # now downscale the qm_cell to appropriate size
    qm_cell[-1,-1] -= (qm_vacuum - me_vacuum)

    # rescaling would yield a mess
    qm_atoms.set_cell(qm_cell, scale_atoms=False)


    if _return_idx:
        return qm_atoms, idx
    else:
        return qm_atoms


def create_me_atoms(me_element, me_facette, me_size=None, me_a=None, vacuum=None):
    import ase.build
    me_facette = tools.normalize_str(me_facette, pattern= r'[\s\-_\./\(\)]*')
    try:
        func = getattr(ase.build, me_facette)
        if vacuum is not None:
            vacuum /= 2.
        me_atoms = func(symbol=me_element,
                        a=me_a,
                        size=me_size,
                        vacuum=vacuum)
    except AttributeError:
        msg  ='Surface facette "{}" is not implemented through ase. '.format(me_facette)
        msg += 'You may need to write a fallback routine (e.g. using ase.lattice.*).'
        raise NotImplementedError(msg)


    return me_atoms

def check_lammps_calc(calc):
    # sanity check
    if calc.__class__.__name__ not in ['LAMMPS', 'ExtendedLAMMPS']:
        msg = 'Me part requires a "LAMMPS" or "ExtendedLAMMPS" calculator instance '
        msg += '(provided : "{}").'.format(atoms.calc.__class__.__name__)
        raise ValueError(msg)

def check_castep_calc(calc):
    if calc.__class__.__name__ not in ['Castep', 'ExtendedCastep']:
        msg = 'QM part requires a "Castep" or "ExtendedCastep" calculator instance '
        msg += '(provided : "{}").'.format(atoms.calc.__class__.__name__)
        raise ValueError(msg)

def find_adsorbate_idx(atoms_with_adsorbate, atoms_without_adsorbate, tol=1e-6):
    adsorbate_idx = []
    for iatom in atoms_with_adsorbate:
        adsorbate=True
        for jatom in atoms_without_adsorbate:
            if np.linalg.norm(iatom.position - jatom.position) < tol**2:
                adsorbate=False
                break
        if adsorbate:
            adsorbate_idx.append(iatom.index)
    return adsorbate_idx
