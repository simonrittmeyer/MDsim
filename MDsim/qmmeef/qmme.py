# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

import ase

from MDsim import units

from MDsim.bindings.ase.asetomdsim.asesystem import ASESystem
from MDsim.bindings.ase.mdsimtoase import MDsimCalculator

from MDsim.qmmeef.coordinates import Coordinates

class QMMe(ASESystem):
    """
    Thing can combines the quantum and the classical world ;)

    This system is made to combine a classical description of a metal slab with
    a quantum mechanical description of some part in this slab. Problems may
    arise if the lattice constants in the QM world and the Me world are not the
    same. We will always use the Me lattice constant for within the system via
    scaling the QM contributions, yet the QM sub-part will be told the unscaled
    positions always.

    Initialization
    --------------
    qm_system : QM (or derived) instance
        The part that should be treated on a quantum mechanical level. Make
        sure that <Nslab> atoms are at identical positions (after scaling, see
        below) to counter-atoms in the Me part. If this does not match, no
        chance! Currently there is only a CASTEP implementation.

        If you ever want to interface some different QM code, make sure that
        you create create a compatible QM class. Use the QMstubs class as a
        guideline.

    me_system : System (or derived) instance
        The part that should be treated on a classical level. Basically, we
        only require basic System functionality. Hence, interfacing a different
        code than LAMMPS should be straight-forward from this side (I am not
        talking about having matching potentials...).

    _qm_lattice_scale : float, optional (default = 1.)
        In case of differing lattice constants "a_X" in the QM and Me part due
        to different potentials, this is the ratio "a_QM / a_Me". Will be used
        to properly scale positions and forces.

    _init_forces : boolean, optional (default = True):
        Initialize forces for the entire system in the constructor. Should be
        no problem if the sub-systems are initialized anyway.

    _init_etas : boolean, optional (default = True):
        Initialize friction coefficients in the constructor.
    """

    def __init__(self, qm_system,
                       me_system,
                       # this is the ratio QM/Me lattice constant
                       _qm_lattice_scale=1.,
                       _init_forces=True,
                       _init_etas=True,
                       _verbose=True):

        # dimensionality is hard coded!
        self.Ndim = 3

        # we need this to rescale the slab if lattice constants differ
        # this is done via the respective Coordinates object
        self._qm_lattice_scale = _qm_lattice_scale
        self._coords = Coordinates(qm_lattice_scale=self._qm_lattice_scale,
                                   Ndim=self.Ndim)

        self.qm_system = qm_system
        self.me_system = me_system

        # for convenience (Natoms will be set by the parent init)
        self.Natoms_qm = self.qm_system.get_Natoms() # total number of quantum atoms
        self.Natoms_overlap = self.qm_system.get_Nslab() # number of quantum slab atoms
        self.Natoms_me = self.me_system.get_Natoms() # number of classical atoms

        # no double counting...
        self.Natoms = self.Natoms_qm + self.Natoms_me - self.Natoms_overlap

        self._verbose = _verbose

        # this is how we identify unassigned values
        self._unassigned = -1

        # this creates the index maps
        self.idx, self._me_idx, self._qm_idx = self.merge_systems()

        # now fill the thing. This is how we do it:
        # First part is the full qm_system, second part is the Me system
        # without the overlapping part.
        positions = np.empty((self.Natoms, self.Ndim), order='C', dtype=np.float64)
        positions[self.idx['me_part'],:] = self.me_system.get_positions()[:,:]

        # these need to be scaled...
        positions[self.idx['qm_part'],:] = self._coords.points_qm_to_me(self.qm_system.get_positions())

        velocities = np.empty_like(positions)
        # as we do not work with these velocities, they do not need to be
        # scaled in principle. But let's do in anyhow. They transform the same
        # way positions do.
        velocities[self.idx['me_part'],:] = self.me_system.get_velocities()[:,:]
        velocities[self.idx['qm_part'],:] = self._coords.points_qm_to_me(self.qm_system.get_velocities())

        masses = np.empty(self.Natoms, dtype=np.float64, order='C')
        masses[self.idx['me_part']] = self.me_system.get_masses()[:]
        masses[self.idx['qm_part']] = self.qm_system.get_masses()[:]

        species = np.empty(self.Natoms, dtype=object)
        species[self.idx['me_part']] = self.me_system.get_species().tolist()
        species[self.idx['qm_part']] = self.qm_system.get_species().tolist()
        species = np.array(species, order='C')

        # here we go, assemble the atoms object
        atoms = ase.Atoms(symbols=species,
                          # positions are all scaled to the w.r.t. Me lattice constant
                          positions=positions,
                          # convert to proper ASE units... sigh
                          momenta=(velocities*masses[:,np.newaxis])*units.ANGSTROM_AMU_PER_FS_TO_ASE,
                          masses=masses,
                          # cell must be the one of the bath, obviously
                          cell=self.me_system.atoms.get_cell(),
                          pbc=self.me_system.atoms.get_pbc())

        ASESystem.__init__(self, atoms,
                                 friction=None,
                                 temperature=0.,
                                 _init_etas=False,
                                 _init_forces=False,
                                 _verbose=_verbose)

        # get this flag from the QM-region (Me won't have friction anyway)
        self.has_friction = qm_system.has_friction

        if _init_forces:
            self._init_forces()

        if _init_etas:
            self._init_etas()

        # our atoms object needs a calculator in order to work properly...
        calc = MDsimCalculator(system=self, _init_forces=_init_forces)
        self.atoms.set_calculator(calc)

        # attach some nice stuff to the atoms object
        self.atoms.info.update(self.idx)
        self.atoms.info.update({'qm_lattice_scale' : self._qm_lattice_scale})

        self.atoms.info.update({'dissipated_energy' : self.get_dissipated_energy(),
                                'dissipation_rate' : self.get_dissipation_rate()})

    def _unassigned_array(self, *args, **kwargs):
        """
        Return an unassigned index array (filled with the symbol for
        unassigned). *args and **kwargs are directly passed to np.ones().
        """
        return np.ones(*args, **kwargs) * self._unassigned


    def merge_systems(self):
        """
        Routine that merges the QM and Me part of the simulation and creates
        the corresponding index maps. For all system arrays, we define the
        convention:

        * Elements 0 to Natoms_me-1 is the Me part. Hence, there is no shuffling
          for the largest part. This part contains the overlap!

        * Elements Natoms_me to Natoms-1 are then the pure QM parts without
          overlap, ie. the adsorbate.

        The overlapping atoms are found via positional overlap!

        Returns
        -------
        idx : dict
            The main dictionary. All elements in these lists are indices that
            correspond to the QM/Me-system. The order of these elements is
            chosen such, that upon applying the index map to the QM/Me
            properties, the order of the Me or QM part, respectively, is
            recovered.

        """

        idx = {} # indices refering to QM/Me properties
        qm_idx = {} # indices referring to the QM part
        me_idx = {} # indices referring to the Me part

        if self._verbose:
            print('Merging QM and Me systems, ie. creating index maps')
            print('\tAssigning Me-part')

        idx['me_part'] = np.arange(0, self.Natoms_me, dtype=int)
        idx['all'] = np.arange(0, self.Natoms, dtype=int)

        # after having assigned things, there should be only positive indices
        idx['qm_part'] = self._unassigned_array(self.Natoms_qm, dtype=int)
        idx['overlap'] = self._unassigned_array(self.Natoms_overlap, dtype=int)

        qm_idx['overlap'] = self._unassigned_array(self.Natoms_overlap, dtype=int)
        me_idx['overlap'] = self._unassigned_array(self.Natoms_overlap, dtype=int)

        qm_positions = self._coords.points_qm_to_me(self.qm_system.get_positions())
        me_positions = self.me_system.get_positions()

        qm_species = self.qm_system.get_species()
        me_species = self.me_system.get_species()

        # map the slab atom indices from the QM system to those of the Me system
        # this is done in a position matching-basis, just like the castep
        # calculator from ase used to do it.
        tolerance = 1e-3

        if self._verbose:
            print('\tMatching QM and Me overlapping atoms (tolerance = {} a.u.)'.format(tolerance))

        i_overlap = 0

        # within this loop, i_me == i_sys by definition
        # overlap is only possible if we are in the range of Me atoms
        for i_qm in self.qm_system.get_slab_idx(): # QM index
            # only need to loop over the slab atoms
            # because the adsorbate always comes last
            for i_me in idx['me_part']:
                dist = np.linalg.norm(qm_positions[i_qm] - me_positions[i_me])
                if dist < tolerance :
                    if qm_species[i_qm] == me_species[i_me]:
                        idx['qm_part'][i_qm] = i_me
                        idx['overlap'][i_overlap] = i_me
                        qm_idx['overlap'][i_overlap] = i_qm
                        me_idx['overlap'][i_overlap] = i_me
                        i_overlap += 1
                        if self._verbose:
                            print('\t\tFound : QM idx = {} --> QM/Me idx = {}'.format(i_qm, i_me))
                        break
                    else:
                        # raise an error if species do not match but we have an overlap
                        msg = 'Overlapping atoms of different species!'
                        msg += '\n\tMe (idx = {}) : {}'.format(i_me, me_species[i_me])
                        msg += '\n\tQM (idx = {}) : {}'.format(i_qm, qm_species[i_qm])
                        raise ValueError(msg)

        # sanity checks
        if not i_overlap == self.Natoms_overlap:
            msg  = 'Did not find all overlapping atoms! The following QM-metal atoms were not assigned:'
            for i in np.arange(self.Natoms_qm)[self.qm_system.get_slab_idx()]:
                if not i in qm_idx['overlap']:
                    msg += '\n\tatom #{} (species : {})'.format(i, self.qm_system.species[i])
            raise ValueError(msg)

        if not np.all(self.qm_system.get_slab_idx() == qm_idx['overlap']):
            msg = 'Not all slab atoms of the QM system were recognized as overlapping atoms!'
            msg += ' Did you properly specify "adsorbate_idx" in the QM system?'
            raise ValueError(msg)

        if not np.all(self.qm_system.get_masses()[qm_idx['overlap']] == self.me_system.get_masses()[me_idx['overlap']]):
            msg = 'Masses of overlapping atoms differ in Me and QM part!'
            raise ValueError(msg)

        # at this point, we have all the overlapping atoms... but just make sure...
        if np.any(me_idx['overlap'] == self._unassigned):
            msg = 'Incomplete overlap list for Me system'
            raise ValueError(msg)

        if np.any(qm_idx['overlap'] == self._unassigned):
            msg = 'Incomplete overlap list for QM system'
            raise ValueError(msg)

        if self._verbose:
            print('\tAssigning adsorbate atoms (purely QM)')

        # here comes the remaining (quantum) part
        for i_sys in range(self.Natoms_me, self.Natoms):
            # only need to loop over the adsorbate
            for i_qm in self.qm_system.get_adsorbate_idx():
                if idx['qm_part'][i_qm] == self._unassigned:
                    idx['qm_part'][i_qm] = i_sys
                    if self._verbose:
                        print('\t\tFound : QM idx = {} --> QM/Me idx = {}'.format(i_qm, i_sys))
                    # crucial bugfix for admolecules (f***, took me an entire
                    # day *banging head against the desk*)
                    break


        if np.any(idx['qm_part'] == self._unassigned):
            msg = 'Incomplete QM-part index map'
            raise ValueError(msg)

        # purely qm part
        idx['adsorbate'] = np.array([i for i in idx['qm_part'] if not i in idx['overlap']], dtype=int)

        # entire substrate
        idx['substrate'] = np.array([i for i in range(self.Natoms) if not i in idx['adsorbate']], dtype=int)

        # Me part without overlap
        idx['bath'] = np.array([i for i in idx['me_part'] if not i in idx['overlap']], dtype=int)
        me_idx['bath'] = idx['bath']

        # entire substrate
        idx['qm_substrate'] = np.array([i for i in idx['qm_part'] if not i in idx['adsorbate']], dtype=int)


        # finally check if our indices actually reach each element
        _test = self._unassigned_array(self.Natoms)
        _test[idx['qm_part']] = 1
        _test[idx['me_part']] = 1
        if np.any(_test == -1):
            msg = 'Indices do not cover the full range of <self.Natoms>!'
            msg +=' Uncovered system indices are:'
            for _i in np.arange(self.Natoms)[_test == self._unassigned]:
                msg += '\n\t* {}'.format(_i)
            raise ValueError(msg)

        return idx, me_idx, qm_idx


    def get_idx(self):
        return self.idx

    def get_me_idx(self):
        return self._me_idx

    def get_qm_idx(self):
        return self._qm_idx

    def get_Natoms_qm(self):
        return self.Natoms_qm

    def get_Natoms_me(self):
        return self.Natoms_me

    def get_Natoms_overlap(self):
        return self.Natoms_overlap


    # routines to be overloaded
    def _init_forces(self):
        ASESystem._init_forces(self)
        try:
            self.atoms.calc._init_forces()
        except AttributeError:
            # in this case we have called the thing before the calculator was attached.
            pass

    def calc_forces(self, positions):
        # zero the forces (crucial as we add things...)
        forces = np.zeros((self.Natoms, self.Ndim), order='C', dtype=np.float64)
        # fill it
        forces[self.idx['me_part'],:] = self.me_system.calc_forces(positions[self.idx['me_part']])

        # ok, we first need to transform the positions to QM world, and
        # afterwards we need to backtransform forces to Me world.
        forces[self.idx['qm_part'],:] += self._coords.gradients_qm_to_me(
                                            self.qm_system.calc_forces_qm(
                                                self._coords.points_me_to_qm(positions[self.idx['qm_part']])
                                                )
                                            )
        return forces


    def set_positions(self, positions, **kwargs):
        """
        Setting positions of the system and the two subsystems.
        Note that the input positions vector has to be in Me-coordinates, as
        they will be transformed when assigning to the QM-subsystem!
        """
        super(QMMe, self).set_positions(positions, **kwargs)
        self.qm_system.set_positions(self._coords.points_me_to_qm(positions[self.idx['qm_part'],:]), **kwargs)
        self.me_system.set_positions(positions[self.idx['me_part'],:], **kwargs)


    def set_forces(self, forces, **kwargs):
        super(QMMe, self).set_forces(forces)
        # NOTE: we do not need to sync those here... the subsystem's forces are
        # set upon calling anyway, so leave the "clean" subsystem froces as
        # they are.
        # in other words: there is no convenient way to disentangle these
        # forces here into the subsystems forces. so we do not do it! any
        # integrator first sets positions and then sets forces.

        # self.qm_system.set_forces(self._coords.gradients_me_to_qm(forces[self.idx['qm_part'],:]), **kwargs)
        # self.me_system.set_forces(forces[self.idx['me_part'],:], **kwargs)


    def set_velocities(self, velocities, **kwargs):
        super(QMMe, self).set_velocities(velocities, **kwargs)
        self.qm_system.set_velocities(self._coords.points_me_to_qm(velocities[self.idx['qm_part'],:]), **kwargs)
        self.me_system.set_velocities(velocities[self.idx['me_part'],:], **kwargs)


    def set_time(self, time, **kwargs):
        super(QMMe, self).set_time(time, **kwargs)
        self.qm_system.set_time(time, **kwargs)
        self.me_system.set_time(time, **kwargs)


    def set_kT(self, kT, **kwargs):
        super(QMMe, self).set_kT(kT, **kwargs)
        self.qm_system.set_kT(kT, **kwargs)
        self.me_system.set_kT(kT, **kwargs)


    def set_etas(self, etas, **kwargs):
        super(QMMe, self).set_etas(etas, **kwargs)
        self.qm_system.set_etas(etas[self.idx['qm_part'],:], **kwargs)
        self.me_system.set_etas(etas[self.idx['me_part'],:], **kwargs)


    def calc_etas(self, positions):
        etas = np.zeros((self.Natoms, self.Ndim), order='C', dtype=np.float64)
        # those should not interference, ie. the qm friction should only be on the adsorbate atoms
        etas[self.idx['me_part'],:] = self.me_system.calc_etas(positions[self.idx['me_part']])
        etas[self.idx['qm_part'],:] += self.qm_system.calc_etas(self._coords.points_me_to_qm(positions[self.idx['qm_part']]))
        return etas


    def calc_potential_energy(self, positions):
        Epot_me = self.me_system.calc_potential_energy(positions[self.idx['me_part']])
        Epot_qm = self.qm_system.calc_potential_energy(self._coords.points_me_to_qm(positions[self.idx['qm_part']]))
        Epot_qm_slab = self.qm_system.calc_potential_energy_slab(self._coords.points_me_to_qm(positions[self.idx['qm_part']]))
        # see Eq.(1) in:
        #       Meyer and K. Reuter, Angew. Chem. Int. Ed. 53, 4721 (2014)
        #       http://www.dx.doi.org/10.1002/anie.201400066
        # for the definition of potential energy
        return Epot_me + (Epot_qm - Epot_qm_slab)


    def calc_dissipation_rate(self, etas, velocities):
        # no friction of the me part!
        return self.qm_system.calc_dissipation_rate(etas[self.idx['qm_part']],
                                                    velocities[self.idx['qm_part']])

    def pre_integration_hook(self):
        """
        We use this hook here to evaluate the dissipated energy.
        Call only qm_atoms, however :)
        """
        self.qm_system.pre_integration_hook()


    def post_integration_hook(self):
        """
        We use this hook here to evaluate the dissipated energy.
        Call only qm_atoms, however :)
        """
        self.qm_system.post_integration_hook()
        for key in ['dissipated_energy', 'dissipation_rate']:
            self.atoms.info[key] = self.qm_system.atoms.info[key]
        self.Ediss = self.qm_system.atoms.info['dissipated_energy']
