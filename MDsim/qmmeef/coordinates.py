# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

"""
Module to handle to conversion from QM <--> Me coordinates
"""

class Coordinates(object):
    """
    Class handling the necessary coordinate transformations for QM/Me. In case
    we have differing lattice constants, ie,

        qm_lattice_scale := a_QM/a_Me != 1

    we will scale the unit cell and all positions in there the respective
    matrix Q to convert from Me --> QM is for instance

        Q_ij = a_QM/a_Me * delta_ij

    Initialization
    --------------
    qm_lattice_scale : float, optional (default = 1.)
        In case of differing lattice constants "a_X" in the QM and Me part due
        to different potentials, this is the ratio "a_QM / a_Me". Will be used
        to properly scale positions and forces.

    Ndim : integer, optional ({1, 2, *3*})
        Number of cartesian dimensions per atom.
    """


    def __init__(self, qm_lattice_scale=1., Ndim=3):

        if Ndim in [1, 2, 3]:
            self.Ndim = int(Ndim)
        else:
            raise ValueError('Cartesian dimensionality must be either 1, 2 or 3')

        self._qm_lattice_scale = qm_lattice_scale

        # scaling locations in phase space (positions and velocities)
        self._point_me_to_qm_scale = np.identity(self.Ndim) * qm_lattice_scale
        self._point_qm_to_me_scale = np.identity(self.Ndim) / qm_lattice_scale

        # scaling gradients (forces)
        self._gradient_qm_to_me_scale = np.identity(self.Ndim) * qm_lattice_scale
        self._gradient_me_to_qm_scale = np.identity(self.Ndim) / qm_lattice_scale


    def points_qm_to_me(self, points):
        """
        Transform positions/velocities from QM-space to Me-space. Basically
        this is just a scaling. We use einsum here to conveniently avoid
        looping. Performance is not the issue here. Note that we stick with the
        more general matrix multiplication that in principle would also allow
        to do some sheereing.

        Parameters
        ----------
        points : (Natoms, Ndim) array
            Positions/velocities in QM-coordinates.

        Returns
        -------
        points : (Natoms, Ndim) array
            Positions/velocities in Me-coordinates.
        """
        return np.einsum('ij,kj->ki', self._point_qm_to_me_scale, points)


    def points_me_to_qm(self, points):
        """
        Transform positions/velocities from Me-space to QM-space. Basically
        this is just a scaling. We use einsum here to conveniently avoid
        looping. Performance is not the issue here. Note that we stick with the
        more general matrix multiplication that in principle would also allow
        to do some sheereing.

        Parameters
        ----------
        points : (Natoms, Ndim) array
            Positions/velocities in Me-coordinates.

        Returns
        -------
        points : (Natoms, Ndim) array
            Positions/velocities in QM-coordinates.
        """
        return np.einsum('ij,kj->ki', self._point_me_to_qm_scale, points)


    def gradients_qm_to_me(self, gradients):
        """
        Transform gradients (forces) from QM-space to Me-space. Basically this
        is just a scaling. We use einsum here to conveniently avoid looping.
        Performance is not the issue here. Note that we stick with the more
        general matrix multiplication that in principle would also allow to do
        some sheering.

        Parameters
        ----------
        gradients : (Natoms, Ndim) array
            Gradients (forces) in QM-coordinates.

        Returns
        -------
        gradients : (Natoms, Ndim) array
            Gradients (forces) in Me-coordinates.
        """
        return np.einsum('ij,kj->ki', self._gradient_qm_to_me_scale, gradients)


    def gradients_me_to_qm(self, gradients):
        """
        Transform gradients (forces) from Me-space to QM-space. Basically this
        is just a scaling. We use einsum here to conveniently avoid looping.
        Performance is not the issue here. Note that we stick with the more
        general matrix multiplication that in principle would also allow to do
        some sheering.

        Parameters
        ----------
        gradients : (Natoms, Ndim) array
            Gradients (forces) in Me-coordinates.

        Returns
        -------
        forces : (Natoms, Ndim) array
            Gradients (forces) in QM-coordinates.
        """
        return np.einsum('ij,kj->ki', self._gradient_me_to_qm_scale, gradients)
