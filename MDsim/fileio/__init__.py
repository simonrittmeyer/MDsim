# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import os
from MDsim.prettyprint.interface import IOInterface

class FileHandler(IOInterface):
    """
    Base class for any file handling issues. Not usable as is, as format
    specific stuff is missing.

    Initialization
    --------------
    system : System instance of MDsim
        The actual system from which the information is to be read.

        NOTE: The corresponding object saves a reference to the system as an
        internal variable and can thus, in principle, also do harmful things to
        your system. So, make sure you know what your IO routines are actually
        doing.  The reason for this behavior is that it simplyfies the further
        IO processes - the object can be called by push() without any further
        arguments.

    integrator : Integrator instance of MDsim
        The integrator employed in the simulation. It is needed to write the
        corresponding information such as timestep and integration scheme to
        the file header.

    observable : Observable instance of MDsim
        Any of the observables that are implemented in
        MDsim.prettyprint.observables.implemented

    seed : string, optinal (default = 'MDsim')
        Suffix for the output file. The file will be named:
            {seed}_{observable}.dat(.gz)

    outfolder : string, optional (default = '.')
        Folder where to put the outfile.

    args : dictionary, optional (default = None)
        The dictionary holding all internal settings. There will only be an
        effect if it is passed, but then it simply lists all internal options.

    _filesuffix : string, optional (default = None)
        Suffix of the respective file format (including the dot). If your
        derived class does not pass this, an error will be raised.

    NOTE
    ----
    * The observable for the current system state is written to the outfile via
      the push() method.

    * The output file will automatically be opened upon initialization and
      closed upon destruction of this object. To close the file use close().
    """
    def __init__(self,
                 system,
                 integrator,
                 observable,
                 seed='MDsim',
                 outfolder='.',
                 args=None,
                 _filesuffix=None,
                ):


        # raises error if not implemented
        self.obs_inst = observable
        self.system = system
        self.integrator = integrator # needed for the file header

        self._Ncols = self.obs_inst.get_Ncolumns()
        self.out = None

        if _filesuffix is None:
            raise ValueError('"_filesuffix" is None')

        self.outfolder = outfolder
        if not os.path.isdir(self.outfolder):
            os.makedirs(self.outfolder)

        self.outfile = os.path.join(outfolder,
                                    seed + '__'
                                    + observable.get_idstring().upper()
                                    + _filesuffix)
        self.args = args

        self.open()

        self._closed = False

    def open(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError

    def push(self):
        raise NotImplementedError

    def write(self):
        raise NotImplementedError

    def add(self, *args, **kwargs):
        """
        This one is to accumulate runs. Just adds something directly to the
        observable. The observable will raise an error, if this is not
        supported.
        """
        self.obs_inst.add(*args, **kwargs)

    def get_filename(self):
        return os.path.abspath(self.outfile)

