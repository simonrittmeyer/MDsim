# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to handle dumping to a numpy array of appropriate size instead of a
file.
"""
from __future__ import print_function

import os
import re
import numpy as np
from time import strftime

from MDsim.prettyprint import logo
from MDsim.prettyprint.interface import IOInterface

class MemoryHandler(IOInterface):
    """
    Class to store all information in a memory-internal numpy array. Be careful
    with memory restrictions.

    Initialization
    --------------
    Nsteps : int
        Number of steps that shall be logged.

    system : System instance of MDsim
        The actual system from which the information is to be read.

        NOTE: The corresponding object saves a reference to the system as an
        internal variable and can thus, in principle, also do harmful things to
        your system. So, make sure you know what your IO routines are actually
        doing.  The reason for this behavior is that it simplyfies the further
        IO processes - the object can be called by push() without any further
        arguments.

    integrator : Integrator instance of MDsim
        The integrator employed in the simulation. It is needed to write the
        corresponding information such as timestep and integration scheme to
        the file header.

    observable : Observable instance of MDsim
        Any of the observables that are implemented in the
        MDsim.prettyprint.observables.implemented module

    datatype : string, optional (default = 'f4')
        Datatype of the dumped numerical information. Available are
            'f4' : 32-bit floating point
            'f8' : 64-bit floating point
            'i4' : 32-bit signed integer
            'i2' : 16-bit signed integer
            'i8' : 64-bit signed integer
            'i1' : 8-bit signed integer
            'u1' : 8-bit unsigned integer
            'u2' : 16-bit unsigned integer
            'u4' : 32-bit unsigned integer
            'u8' : 64-bit unsigned integer

    args : dictionary, optional (default = None)
        The dictionary holding all internal settings. There will only be an
        effect if it is passed, but then it simply lists all internal options.

    """

    # available dtypes
    _dtypes = ['f4',
               'f8',
               'i4',
               'i2',
               'i8',
               'i1',
               'u1',
               'u2',
               'u4',
               'u8'
              ]
    _dtype_map = {'f4' : np.float32,
                  'f8' : np.float64,
                  'i4' : np.int32,
                  'i2' : np.int16,
                  'i8' : np.int64,
                  'i1' : np.int8,
                  'u1' : np.uint8,
                  'u2' : np.uint16,
                  'u4' : np.uint32,
                  'u8' : np.uint64}

    def __init__(self,
                 Nsteps,
                 system,
                 integrator,
                 observable,
                 args=None,
                 datatype='f4'
                ):


        # raises error if not implemented
        self.obs_inst = observable
        self.system = system
        self.integrator = integrator # needed for the file header
        self._Nsteps = Nsteps
        self._Ncols = self.obs_inst.get_Ncolumns()
        self.args = args

        self._dtype = datatype.lower()
        if not self._dtype in self._dtypes:
            msg = 'Unknown data type "{}"'.format(self._dtype)
            raise ValueError(msg)

        self._data = np.ones((self._Nsteps, self._Ncols),
                              dtype=self._dtype_map[self._dtype])*np.nan
        self._idx = 0


    def add(self, *args, **kwargs):
        """
        This one is to accumulate runs. Just adds something directly to the
        observable. The observable will raise an error, if this is not
        supported.
        """
        self.obs_inst.add(*args, **kwargs)

    def push(self):
        self._data[self._idx,0] = self.system.get_time()
        self._data[self._idx,1:] = self.obs_inst.get_observable_values()
        self._idx += 1

    def write(self):
        self._data[:,:] = self.obs_inst.get_all_data()

    def get_data(self):
        return self._data

    def dump_to_netcdf(self, seed='MDsim', outfolder='output'):
        import netCDF4

        if not os.path.isdir(outfolder):
            os.makedirs(outfolder)

        outfile = os.path.join(outfolder,
                               seed + '__'
                                    + self.obs_inst.get_idstring().upper()
                                    + '.nc')

        ncfile = netCDF4.Dataset(outfile, 'w', format='NETCDF4')
        # the x axis
        ncfile.createDimension('x', self._Nsteps)
        # the observable axis (per time step)
        ncfile.createDimension('cols', self._Ncols)

        # write information to the file
        ncfile.created = 'File created : {}'.format(strftime('%c'))
        ncfile.system = self.get_system_info(self.system)
        ncfile.integrator = self.get_integrator_info(self.integrator)
        ncfile.units = self.get_units_info()
        ncfile.observable = self.get_observable_info(self.obs_inst)
        ncfile.columns = self.get_column_description(self.obs_inst)
        if self.args is not None:
            ncfile.internalargs = self.get_options(self.args)

        # create the actual variable, single precission is sufficient
        _ncdata = ncfile.createVariable(varname='data',
                                        datatype=self._dtype,
                                        dimensions=('x', 'cols'))

        _ncdata[:,:] = self._data

        ncfile.closed = 'File closed : {}'.format(strftime('%c'))
        ncfile.close()


    def dump_to_ascii(self, seed='MDsim', outfolder='output'):
        if not os.path.isdir(outfolder):
            os.makedirs(outfolder)

        outfile = os.path.join(outfolder,
                               seed + '__'
                                    + self.obs_inst.get_idstring().upper()
                                    + '.dat')


        with open(outfile, 'w') as out:
            # information
            out.write(self._get_ascii_head(system=self.system,
                                           integrator=self.integrator,
                                           comment=outfile))
            if self.args is not None:
                out.write('\n#')
                out.write(self.get_options(self.args).replace('\n', '\n# '))

            # headline
            out.write('\n#\n#\n')
            out.write(re.sub(r'\n[\s\-]', r'\n#', self.obs_inst.get_headline()))

            # write data
            for data in self._data:
                out.write('\n' + self.obs_inst._data_template.format(*data))


    def _get_ascii_head(self, system, integrator, comment=None):
        head = logo
        if comment is not None:
            head += '\n{}\n'.format(comment)

        head += '\nFile written : {}'.format(strftime('%c'))
        head += '\n\n'
        head += self.get_system_info(system)
        head += '\n'
        head += self.get_integrator_info(integrator)
        head += '\n'

        if self.obs_inst.get_specific_info():
            head += self.get_observable_info(self.obs_inst)
            head += '\n'

        head += self.get_units_info()

        head = head.replace('\n', '\n# ')
        head = '# ' + head

        return head

    def close(self):
        # convenience function
        pass
