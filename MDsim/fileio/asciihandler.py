# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from time import strftime
import re
import gzip
import os

from MDsim.prettyprint import logo
from MDsim.fileio import FileHandler

class ASCIIHandler(FileHandler):
    """
    This is a class that handles IO to ASCII output files. Each object is
    uniquely connected to an observable as well as to an output file it writes
    the output to.

    Initialization
    --------------
    system : System instance of MDsim
        The actual system from which the information is to be read.

        NOTE: The corresponding object saves a reference to the system as an
        internal variable and can thus, in principle, also do harmful things to
        your system. So, make sure you know what your IO routines are actually
        doing.  The reason for this behavior is that it simplyfies the further
        IO processes - the object can be called by push() without any further
        arguments.

    integrator : Integrator instance of MDsim
        The integrator employed in the simulation. It is needed to write the
        corresponding information such as timestep and integration scheme to
        the file header.

    observable : Observable instance of MDsim
        Any of the observables that are implemented in
        MDsim.prettyprint.observables.implemented

    seed : string, optinal (default = 'MDsim')
        Suffix for the output file. The file will be named:
            {seed}_{observable}.dat(.gz)

    outfolder : string, optional (default = '.')
        Folder where to put the outfile.

    args : dictionary, optional (default = None)
        The dictionary holding all internal settings. There will only be an
        effect if it is passed, but then it simply lists all internal options.

    gzip_posthoc : Boolean, optional (default = True)
        Don't write to an open gzip file but compress after the simulation only.

    gzip : Boolean, optional (default = False)
        Decides whether output is written to a *.gz file or not. The gzip
        module will be used. This might be comparably slow, however it runs on
        all platforms. There might be a faster implementation at some point if
        this turns out to be a real issue.

    NOTE
    ----
    * The observable for the current system state is written to the outfile via
      the push() method.

    * The output file will automatically be opened upon initialization. To
      manually open/close the files use close() and open().
    """
    def __init__(self, *args, **kwargs):

        # pop the new kwargs
        self.gzip = kwargs.pop('gzip', False)
        self.gzip_posthoc = kwargs.pop('gzip_posthoc', True)

        # little sanity check
        if all([self.gzip, self.gzip_posthoc]):
            msg = 'Both "gzip" and "gzip_posthoc" are chosen. This combination does not make sense.'
            msg += '\nWill deactivate "gzip" for performance reasons'
            self.gzip = False

        # set the ending
        if self.gzip:
            _filesuffix = '.dat.gz'
        else:
            _filesuffix = '.dat'

        kwargs['_filesuffix'] = _filesuffix

        # init the parent
        FileHandler.__init__(self, *args, **kwargs)


    def open(self):
        # check if folder exists
        folder = os.path.dirname(self.outfile)
        if not os.path.exists(folder):
            os.makedirs(folder)

        # decide wheter gzip output or plain text
        if self.gzip:
            self.out = gzip.open(self.outfile, 'w')
        else:
            self.out = open(self.outfile, 'w')

        # write the header information
        self.write_head()

    def close(self):
        self.out.close()
        if self.gzip_posthoc:
            try:
                # bash way of life
                os.system('gzip -f {}'.format(self.outfile))
            except OSError:
                # python way of life
                # from: https://docs.python.org/2/library/gzip.html
                f_in = open(self.outfile, 'r')
                f_out = gzip.open(self.outfile + '.gz', 'w')
                f_out.writelines(f_in)
                f_out.close()
                f_in.close()
        self._closed = True


    def write_head(self):
        self.out.write(self._get_file_head(system=self.system,
                                           integrator=self.integrator,
                                           comment=self.outfile))
        if self.args is not None:
            self.out.write('\n#')
            self.out.write(self._get_file_options(self.args))

        self.out.write('\n#')
        self.out.write('\n#')
        self.out.write(self._get_headline())


    def push(self):
        self.out.write('\n')
        self.out.write(self.obs_inst.get_dataline())

    def write(self):
        """
        Function that is only available for post processing observables
        """
        for data in self.obs_inst.get_all_datalines():
            self.out.write('\n' + data)

    def get_filename(self):
        fname = FileHandler.get_filename(self)
        if self.gzip_posthoc:
            fname += '.gz'
        return fname


    def _get_file_head(self, system, integrator, comment=None):
        head = logo
        if comment is not None:
            head += '\n{}\n'.format(comment)

        head += '\nFile written : {}'.format(strftime('%c'))
        head += '\n\n'
        head += self.get_system_info(system)
        head += '\n'
        head += self.get_integrator_info(integrator)
        head += '\n'

        if self.obs_inst.get_specific_info():
            head += self.get_observable_info(self.obs_inst)
            head += '\n'

        head += self.get_units_info()

        head = head.replace('\n', '\n# ')
        head = '# ' + head

        return head

    def _get_file_options(self, args):
        opt = self.get_options(args).replace('\n', '\n# ')
        return opt


    def _get_headline(self):
        head = '\n'
        head += self.obs_inst.get_headline()
        # insert the hash tags...
        head = re.sub(r'\n[\s\-]', r'\n#', head)

        return head

