# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import netCDF4
import numpy as np
from time import strftime

from MDsim.prettyprint import logo
from MDsim.fileio import FileHandler


class NetCDFHandler(FileHandler):
    """
    Wrapper class for IO to a netCDF4 file

    Initialization
    --------------
    system : System instance of MDsim
        The actual system from which the information is to be read.

        NOTE: The corresponding object saves a reference to the system as an
        internal variable and can thus, in principle, also do harmful things to
        your system. So, make sure you know what your IO routines are actually
        doing.  The reason for this behavior is that it simplyfies the further
        IO processes - the object can be called by push() without any further
        arguments.

    integrator : Integrator instance of MDsim
        The integrator employed in the simulation. It is needed to write the
        corresponding information such as timestep and integration scheme to
        the file header.

    observable : Observable instance of MDsim
        Any of the observables that are implemented in
        MDsim.prettyprint.observables.implemented

    seed : string, optinal (default = 'MDsim')
        Suffix for the output file. The file will be named:
            {seed}_{observable}.dat(.gz)

    outfolder : string, optional (default = '.')
        Folder where to put the outfile.

    args : dictionary, optional (default = None)
        The dictionary holding all internal settings. There will only be an
        effect if it is passed, but then it simply lists all internal options.

    datatype : string, optional (default = 'f4')
        Datatype of the dumped numerical information. Available are
            'f4' : 32-bit floating point
            'f8' : 64-bit floating point
            'i4' : 32-bit signed integer
            'i2' : 16-bit signed integer
            'i8' : 64-bit signed integer
            'i1' : 8-bit signed integer
            'u1' : 8-bit unsigned integer
            'u2' : 16-bit unsigned integer
            'u4' : 32-bit unsigned integer
            'u8' : 64-bit unsigned integer

    compression : boolean, optional (default = False)
        Use zlib compression.

    NOTE
    ----
    * The observable for the current system state is written to the outfile via
      the push() method.

    * The output file will automatically be opened upon initialization and
      closed upon destruction of this object. To close the file use close().
    """
    # available dtypes
    _dtypes = ['f4',
               'f8',
               'i4',
               'i2',
               'i8',
               'i1',
               'u1',
               'u2',
               'u4',
               'u8'
              ]

    def __init__(self, *args, **kwargs):
        kwargs['_filesuffix'] = '.nc'

        self._dtype = kwargs.pop('datatype', 'f4').lower()
        if not self._dtype in self._dtypes:
            msg = 'Unknown data type "{}"'.format(self._dtype)
            raise ValueError(msg)

        self._compression = kwargs.pop('compression', False)

        # init the parent
        FileHandler.__init__(self, *args, **kwargs)

    def open(self):
        self.ncfile = netCDF4.Dataset(self.outfile, 'w', format='NETCDF4')
        # the time axis
        self.ncfile.createDimension('time', None)
        # the observable axis (per time step)
        self.ncfile.createDimension('cols', self._Ncols)

        # create the actual variable, single precission is sufficient
        self._data = self.ncfile.createVariable(varname='data',
                                                datatype=self._dtype,
                                                dimensions=('time', 'cols'),
                                                zlib = self._compression)

        self._write_ncfile_info()
        self._time_idx = 0

    def push(self):
        self._data[self._time_idx,0] = self.system.get_time()
        self._data[self._time_idx,1:] = self.obs_inst.get_observable_values()
        self._time_idx += 1

    def write(self):
        self._data[:,:] = self.obs_inst.get_all_data()


    def close(self):
        self.ncfile.closed = 'File closed : {}'.format(strftime('%c'))
        self.ncfile.close()

    def _write_ncfile_info(self):
        self.ncfile.created = 'File created : {}'.format(strftime('%c'))
        self.ncfile.system = self.get_system_info(self.system)
        self.ncfile.integrator = self.get_integrator_info(self.integrator)
        self.ncfile.units = self.get_units_info()
        self.ncfile.observable = self.get_observable_info(self.obs_inst)
        self.ncfile.columns = self.get_column_description(self.obs_inst)
        if self.args is not None:
            self.ncfile.internalargs = self.get_options(self.args)

        # write an additional info file that is human-readable
        with open(self.outfile.replace('.nc', '.info'), 'w') as f:
            f.write(self._get_file_head(system = self.system,
                                        integrator = self.integrator,
                                        comment = "Infofile accompanying {}".format(self.outfile)
                                        ))
            if self.args is not None:
                f.write('\n#')
                f.write(self._get_file_options(self.args))

    # copied from the ascii handler
    def _get_file_head(self, system, integrator, comment=None):
        head = logo
        if comment is not None:
            head += '\n{}\n'.format(comment)

        head += '\nFile written : {}'.format(strftime('%c'))
        head += '\n\n'
        head += self.get_system_info(system)
        head += '\n'
        head += self.get_integrator_info(integrator)
        head += '\n'

        if self.obs_inst.get_specific_info():
            head += self.get_observable_info(self.obs_inst)
            head += '\n'

        head += self.get_units_info()

        head = head.replace('\n', '\n# ')
        head = '# ' + head

        return head

    def _get_file_options(self, args):
        opt = self.get_options(args).replace('\n', '\n# ')
        return opt


class InMemoryNetCDFHandler(NetCDFHandler):
    """
    netCDF4 handler that stores everything in memory during the simulation and
    only afterwards writes to a netCDF4 file. Be careful, this may easily
    result in a MemoryError but may speed up your calculation as such
    tremendously.

    Initialization
    --------------
    Nsteps : int
        Number of steps that shall be logged.

    system : System instance of MDsim
        The actual system from which the information is to be read.

        NOTE: The corresponding object saves a reference to the system as an
        internal variable and can thus, in principle, also do harmful things to
        your system. So, make sure you know what your IO routines are actually
        doing.  The reason for this behavior is that it simplyfies the further
        IO processes - the object can be called by push() without any further
        arguments.

    integrator : Integrator instance of MDsim
        The integrator employed in the simulation. It is needed to write the
        corresponding information such as timestep and integration scheme to
        the file header.

    observable : Observable instance of MDsim
        Any of the observables that are implemented in
        MDsim.prettyprint.observables.implemented

    seed : string, optinal (default = 'MDsim')
        Suffix for the output file. The file will be named:
            {seed}_{observable}.dat(.gz)

    outfolder : string, optional (default = '.')
        Folder where to put the outfile.

    args : dictionary, optional (default = None)
        The dictionary holding all internal settings. There will only be an
        effect if it is passed, but then it simply lists all internal options.

    datatype : string, optional (default = 'f4')
        Datatype of the dumped numerical information. Available are
            'f4' : 32-bit floating point
            'f8' : 64-bit floating point
            'i4' : 32-bit signed integer
            'i2' : 16-bit signed integer
            'i8' : 64-bit signed integer
            'i1' : 8-bit signed integer
            'u1' : 8-bit unsigned integer
            'u2' : 16-bit unsigned integer
            'u4' : 32-bit unsigned integer
            'u8' : 64-bit unsigned integer

    compression : boolean, optional (default = False)
        Use zlib compression.

    NOTE
    ----
    * The observable for the current system state is written to the memory via
      the push() method.

    * Upon calling the close() method, the memory-contained data is written to
      the netCDF4 file.
    """
    _dtype_map = {'f4' : np.float32,
                  'f8' : np.float64,
                  'i4' : np.int32,
                  'i2' : np.int16,
                  'i8' : np.int64,
                  'i1' : np.int8,
                  'u1' : np.uint8,
                  'u2' : np.uint16,
                  'u4' : np.uint32,
                  'u8' : np.uint64}

    def __init__(self, Nsteps, *args, **kwargs):
        self._Nsteps = Nsteps
        NetCDFHandler.__init__(self, *args, **kwargs)

    def open(self):
        """
        Overload the parent routine. We create the entire netCDF file at the end
        """
        self._data = np.zeros((self._Nsteps, self._Ncols),
                              dtype=self._dtype_map[self._dtype])
        self._time_idx = 0

    def close(self):
        """
        We only write at the very end to the netCDF4 file.
        """
        self.ncfile = netCDF4.Dataset(self.outfile, 'w', format='NETCDF4')
        # the time axis
        self.ncfile.createDimension('time', self._Nsteps)
        # the observable axis (per time step)
        self.ncfile.createDimension('cols', self._Ncols)

        # create the actual variable, single precission is sufficient
        self._ncdata = self.ncfile.createVariable(varname='data',
                                                  datatype=self._dtype,
                                                  dimensions=('time', 'cols'),
                                                  zlib = self._compression)

        self._write_ncfile_info()
        self._ncdata[:,:] = self._data

        NetCDFHandler.close(self)

