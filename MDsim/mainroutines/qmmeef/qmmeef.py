# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import sys
import StringIO
import numpy as np
import time
import re
import warnings

from ase.io import read
from ase.io import Trajectory

from MDsim import units
from MDsim import __version__

from MDsim.prettyprint import section_template
from MDsim.prettyprint import format_timing
from MDsim.prettyprint import format_time
from MDsim.prettyprint import get_date


from MDsim.tools import convert_walltime

from MDsim.bindings.castep import installed as castep_installed
from MDsim.bindings.lammps import installed as lammps_installed

from MDsim.qmmeef import LammpsMe
from MDsim.qmmeef import CastepQM
from MDsim.qmmeef import QMMe
from MDsim.qmmeef.tools import cut_qm_from_me
from MDsim.qmmeef.tools import create_me_atoms
from MDsim.qmmeef.tools import check_lammps_calc
from MDsim.qmmeef.tools import check_castep_calc
from MDsim.qmmeef.tools import find_adsorbate_idx
from MDsim.qmmeef.coordinates import Coordinates


from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_trajectoryhandler


logo="""
+------------------------------------------------------------------------------+
|                ___  __  __    ____  __            _____ _____                |
|               / _ \|  \/  |  / /  \/  | ___   _  | ____|  ___|               |
|              | | | | |\/| | / /| |\/| |/ _ \_| |_|  _| | |_                  |
|              | |_| | |  | |/ / | |  | |  __/_   _| |___|  _|                 |
|               \__\_\_|  |_/_/  |_|  |_|\___| |_| |_____|_|                   |
|                                                                              |
|                             Simon P. Rittmeyer                               |
|                         simon.rittmeyer(at)tum.de                            |
+------------------------------------------------------------------------------+
| Part of the MDsim package {0:<51}|
+------------------------------------------------------------------------------+
"""[1:-1:].format('(version {})'.format(__version__))


def main(attach_me_calc,
         attach_qm_calc,
         me_element=None,
         me_facette=None,
         me_size=None,
         me_a=None,
         vacuum=None,
         qm_size=None,
         add_adsorbate=None,
         friction_model=None,
         simulation_time=10,
         timestep=1,
         integrator='liouville',
         qm_update_step=1,
         qmme_update_step=1,
         walltime=None,
         seed=None,
         outfolder='output',
         qm_a=None,
         optimize_me=True,
         optimizer='FIRE',
         optimizer_run_kwargs={},
         observed_quantities=['forces', 'energy'],
         preview=False,
         random_seed=None,
         verbose=False,
         restart_folder=None,
         check_installation=True,
         dryrun=False,
         keep_checkfiles=2):

    """
    This is the main routine that runs the QM/Me+EF implementation as
    included in MDsim. It is crucial in this context to be aware of the fact
    that actually a QM/Me+EF simulation consists of two distinct parts.

    First, there is the Me-part. It consists of all the metal substate atoms.
    The interaction between those is treated on a pair-potential level using
    the modified embedded atoms method (MEAM). The respective forces and
    energies are evaluated using a modified version of ase's LAMMPSrun
    calculator shipped with MDsim as well. The Me part is the trivial part of
    the simulation.

    Second, there is the QM part. It consists of a small subset of the Me
    system plus the adsorbate. In contrast to the Me part, all interactions in
    the QM part are treated on a DFT level using a modified version of ase's
    CASTEP calculator. The latter is also shipped with MDsim. The QM part is
    the more complex part of the simulation as it comes to the implementation.
    It is also the much more time-consuming part. What is done here is
    basically also simple:

    One calculation of the full-QM system is done. Then, a second calculation
    of only the slab is run. Afterwards, QM-Delta forces are evaluated as a
    difference of both. yes you are right, this means that the Delta forces are
    teh full forces on the adsorbate plus everything on the slab atoms that
    arises due to the presence of the adsorbate. We can view it as the chemical
    forces arising due to the interaction of the adsorbate with the substrate.

    The actual forces that are propagated are then the elastic Me-forces plus
    the Delta-QM forces, see e.g.

    J. Meyer and K. Reuter, Angew. Chem. Int. Ed. 53, 4721 (2014).
    http://www.dx.doi.org/10.1002/anie.201400066

    Electronic friction contributions are also evaluated on the QM level. There
    are two embedding schemes implemented, namely 'AIM' and 'IAA'. For a
    detailed description and comparison of both, please see

    S. P. Rittmeyer, J. Meyer, J. I. Juaristi and K. Reuter, Phys. Rev. Lett. 115, (2015).
    http://www.dx.doi.org/10.1103/PhysRevLett.115.046102

    Before using this code, please make sure that you a) read the papers cited
    and now what you are doing in theory, b) read and understood the entire doc
    string here and c) now how to use ASE with the CASTEP and LAMMPSrun
    calculator. In case of any questions, feel free to email the developper:

    simon.rittmeyer(at)tum.de


    The workflow for a *new* calculation is as follows
    --------------------------------------------------
    1. A new surface slab will be created using ase's routines from the
    ase.lattice.surface module. If the required surface facette is not
    available, one would have to code a respective routine. The size of this
    slab corresponds to the Me system.

    2. The modified LAMMPS calculator is attached using the routine
    "attach_me_calc" that is provided by the user. If demanded, this is
    followed by an optimization of the atoms positions.

    3. The slab atoms of the QM system will be cut from the Me part using a
    dedicated routine. Note that you can cut out any cell, as long as the angle
    of the unit cell vector stays the same and the QM cell is smaller than the
    Me cell.

    4. The adsorbate is added to the QM system using the routine
    "add_adsorbate" as provided by the user. The respective index of the
    adsorbate will be detected automatically.

    5. The extended CASTEP calculator is attached via the "attach_qm_calc"
    routine, also provided through the user input.

    6. Both, the QM and Me part are combined and forces as well as friction
    coefficients are intialized.

    7. IO handlers are created. These are a trajectory for the QM system, and
    another trajectory for the combined QM/Me system.

    8. Time propagation starts. The average time per step will be monitored. If
    a walltime is specified, the propagation loop will be stopped if there is
    not enough time for on emore propagation step.

    9. Restart files are created that allow to nicely restart a trajectory from
    the last point of the current run.


    The workflow for a *restart* run is as follows
    ----------------------------------------------
    1. Fetch whatever files are in the <restart_folder>. Required are at least
    the trajectory files. Benfetial are the check files that were linked from
    the previous run.

    2. Create the QM/Me, Me and QM atoms object from the trajectories.

    3. Check for positions and time consistency.

    4. The modified LAMMPS calculator is attached to the Me atoms using the
    routine "attach_me_calc" that is provided by the user.

    5. Forces and energy for the Me system are restored.

    6. The extended CASTEP calculator is attached via the "attach_qm_calc"
    routine, also provided through the user input.

    7. If found, check files will be linked to the calculator object in order
    to make use of density extrapolation.

    8. Forces, friction coefficients and energy for the QM system are restored.

    9. QM/Me system is re-build with the restored QM and Me system,
    respectively.

    10. QM/Me forces, energy and friction coefficients are restored from the
    information in the qmme_atoms.info dict. System time is set to the restart
    time.

    11. Steps 7-9 from a new simulation. Note that if <outfolder>/trajectories
    exists and contains the old trajectories, the new steps will be appended.
    Otherwise, a new trajectory file will be created.


    Parameters
    ----------
    attach_me_calc : function
        Function that attaches the Me calculator to an atoms object provided as
        argument. Must return the atoms object with the attached calculator. If
        the latter is not an extended LAMMPSrun calculator, an error will be
        raised. In short, this function must return an atoms object with
        attached calculator upon calling it via "attach_me_calc(atoms)" See the
        example below for more details.

    attach_qm_calc : function
        Function that attaches the QM calculator to the respective atoms
        object. Same behavior as "attach_me_calc". Yet, if no ExtendedCastep
        calculator is attached, error will be raised.

    me_element : string, required for new simulations (default=None)
        The chemical symbol of the surface element. *Will not be considered for
        restarted simulations.*

    me_facette : string, required for new simulations (default=None)
        The surface facette of the metall slab. Currently, this must be one of
        the facettes implemented by ase.lattice.surface. *Will not be
        considered for restarted simulations.*

    me_size : 3-tuple, required for new simulations (default=None)
        The size of the Me supercell in x, y and z direction. *Will not be
        considered for restarted simulations.*

    me_a : float, required for new simulations (default=None)
        The lattice constant as obtained on the MEAM level. *Will not be
        considered for restarted simulations*.

    vacuum : float, required for new simulations (default=None)
        Vacuum layer thickness separating the slabs. Note that this is in
        Me-units, in case the lattice parameters differs from the QM value.
        *Will not be considered for restarted simulations.*

    qm_size : 3-tuple, required for new simulations (default=None)
        The size of the QM cell in x,y and z-direction. Will be cut from the Me
        cell, as long as a) smaller than Me cell and b) angles of unit vectors
        remains the same. *Will not be considered for restarted simulations.*

    add_adsorbate : function, required for new simulations (default=None)
        Function that takes the slab-part of the QM cell as input and adds the
        adsorbate to the respective atoms object. See the example below for
        more details. *Will not be considered for restarted simulations.*

    friction_model : string, optional (default=None):
        Embedding model to be applied for the LDFA electronic friction. Either
        'IAA' or 'AIM'.

    simulation_time : float, optional (default=10)
        Simulation time in fs.

    timestep : float, optional (default=1)
        Time step size for the numerical integrator.

    integrator : string, optional (default='Liouville')
        Integration scheme applied. Any of the integrators from
        MDsim.integrators can be used. Yet, without any temperature,
        'Liouville' is a no-brainer. If you have temperature I recommened
        'Bussi Parrinello'.

    qm_update_step : int, optional (default=1):
        Everyt nth step, will be dumped to the QM trajectory.

    qmme_update_step : int, optional (default=1):
        Everyt nth step, will be dumped to the QM/Me trajectory.

    walltime : float, optional (default=None)
        Walltime is *seconds*. The routine will make sure that the simulation
        loop is abandoned and restart information are written before this limit
        is reached.

    seed : string, optional (default='QMMeEF-MDsim')
        Common seed for all output files.

    outfolder : string, optional (default='output')
        Folder that contains all output written. Will be created if not yet
        existing.

    qm_a : float, optional (default = None)
        The lattice constant of the metal as obtained with the applied DFT
        setup. If differing from the Me value, positions will be scaled
        accordingly *in all dimensions*! *Will not be considered for restarted
        simulations*.

    optimize_me : boolean, optional (default=True)
        Update the Me system using the specified <optimizer> after setting up a
        new simulation. *Will not be considerd for restarted simulations.*

    optimizer : string, optional (default='FIRE')
        Any of the optimizers provided through ase.optimize. *Will not be
        considered for restarted calculations.*

    optimizer_run_kwargs : dictionary, optional (default={})
        Arguments passed to the optimizer. *Will not be considered for
        restarted calculations.*

    observed_quantities : List of strings, optional (default = ['energy', 'forces'])
        Will be directly passed to the "properties" argument of the ase
        trajectory backend, see:
            https://wiki.fysik.dtu.dk/ase/ase/trajectory.html

    preview : boolean, optional (default=False):
        The gui from ase will be used to display the systems before any
        calculation starts. May lead to problems if you do not have an X server
        running.

    random_seed : int, optional (default=None)
        Seed for the random number generator (if applied).

    verbose : boolean, optional (default=False):
        Controls the verbosity of the sub-parts of the simulation. May lead to
        lots of output to stdout, but helpful when debugging.

    restart_folder : string, required for restarted calculations.
        Path to restart folder written after previous calculation. Must contain
        at least the traj files, it is benefitial if also the check files are
        in there to make use of density extrapolation within castep.

    keep_checkfiles : integer, optional (default=2)
        Number of castep check files to be kept. "all" is also a valid option.

    check_installation : boolean, optional (default=True)
        Check of required CASTEP, LAMMPS, etc are available.

    Example
    -------
    Below are examples of the definitions of the required functions. This is
    just a guideline for the user how these functions should react.

    >>> def attach_me_calc(me_atoms):
            import os
            from MDsim.bindings.lammps import ExtendedLAMMPS
            # setting up the lammps calculator
            pair_style = 'meam'
            # use an abspath here!
            Pd_eam_file = os.path.abspath('Pd-00PBE.meam')
            pair_coeff = ['* * ' + Pd_eam_file + ' Pd NULL Pd']
            parameters = {'pair_style' : pair_style,
                          'pair_coeff' : pair_coeff}
            files = [Pd_eam_file]
            calc = ExtendedLAMMPS(parameters=parameters,
                                  files=files)
            me_atoms.set_calculator(calc)
            return me_atoms

    >>> def add_adsorbate(qm_atoms):
            import numpy as np
            from MDsim import units
            # This routine should as well assign momenta!
            pos = qm_atoms.get_positions()[np.lexsort(qm_atoms.get_positions().T)][-1]
            height = 1.5
            pos[-1] += height
            # kinetic energy in eV
            Ekin = 5
            mass = 83.798
            mom = -np.sqrt(2 * Ekin * units.EV_TO_AU * mass
                              * units.AMU_TO_AU)*units.AU_TO_ANGSTROM_AMU_PER_FS
            adsorbate_element='Kr'
            qm_atoms.extend(ase.Atoms(adsorbate_element, positions=[pos],
                                      momenta=[[0,0,mom]]))
            return qm_atoms

    >>> def attach_qm_calc(qm_atoms):
            import numpy as np
            from MDsim.bindings.castep import ExtendedCastep
            calc = ExtendedCastep(verbose=False)
            calc._castep_command = 'mpiexec.openmpi -np 4 castep'
            calc._rename_existing_dir = False
            calc._copy_pspots = False
            calc._link_pspots = True
            calc.spin_polarized = False
            qm_atoms.set_calculator(calc)
            calc.set_pspot(pspot = 'OTF')
            calc.cell.fix_com = False
            calc.cell.kpoints_mp_grid = '4 4 1'
            return qm_atoms

    Note
    ----
    To facilite the usage, there are also wrapper functions called run() and
    restart() that initiliaze a new simulation and restart/continue a
    simulation.
    """
    # make sure that "walltime" is either float or <None>, right before we
    # waste any cpu time
    walltime = convert_walltime(walltime)

    # save this one
    if seed is None:
        seed = 'QMMe+EF--MDsim'
    common_seed = seed

    timings = {}
    timings['program'] = {'start' : time.time()}
    timings['init'] = {'start' : time.time()}
    timings['scf'] = {}

    print(logo)
    print(section_template.format('Program started : {}'.format(get_date())))

    # check if everything is installed correctly
    if check_installation:
        try:
            castep_installed()
        except Exception:
            print('Some of CASTEP/castep_hirshfeld/castep2cube are not installed.')
            return

        try:
            lammps_installed()
        except Exception:
            print('LAMMPS is not installed.')
            return


    # the restart files dict
    restart_info = dict()
    parts = ['Me', 'QM', 'QMMe']
    for part in parts:
        restart_info[part] = {'traj_pattern' : r'restart\..+\.{}\.traj'.format(part),
                              'traj_file' : None}

    restart_info['castep'] = {'atoms' : {'check' : None},
                              'slab_atoms' : {'check' : None}
                             }

                              # will be used to store the restart atoms
    qm_atoms = None
    me_atoms = None
    qmme_atoms = None


    if restart_folder:
        print(section_template.format('Restart'))
        print('Demanded restart from folder')
        print('\t{}'.format(restart_folder))

        # check if keywords have been specified but will not be considered
        if np.any([i is not None for i in [me_element,
                                           me_facette,
                                           me_size,
                                           me_a,
                                           qm_size,
                                           vacuum,
                                           add_adsorbate,
                                           optimize_me,
                                           qm_a]]):
            msg ='Info: Some keywords that were specified do not have any effect on restart runs. '
            msg+= 'Consult the documentation, please.'
            print(msg)

        print('\nChecking for required restart files')
        for f in os.listdir(restart_folder):

            # the trajectory files
            for part in parts:
                info = restart_info[part]
                if re.search(info['traj_pattern'], f):
                    info['trajfile'] = os.path.join(restart_folder, f)
                    print('\tFound {}'.format(f))
                    break

            if f.endswith('.check'):
                if not 'slab_only' in f:
                    restart_info['castep']['atoms']['check'] = os.path.join(restart_folder, f)
                elif'slab_only' in f:
                    restart_info['castep']['slab_atoms']['check'] = os.path.join(restart_folder, f)
                else:
                    continue

                print('\tFound {}'.format(f))

        if not np.all([restart_info[part]['trajfile'] is not None for part in parts]):
            msg='Not all required restart files found.'
            raise ValueError(msg)
        else:
            msg = '--> All necessary restart files to restore positions, forces and etas found'
            print(msg)

        if np.any([restart_info['castep'][i]['check'] is None for i in ['atoms', 'slab_atoms']]):
            msg='--> Not all CASTEP check files provided. This will increase the duration of your first timestep.'
            print(msg)
        else:
            msg='--> Both CASTEP .check files found. This will speedup your first time step thanks to density extrapolation.'
            print(msg)

        print('\nChecking restart consistency')

        qm_atoms = read(restart_info['QM']['trajfile'])
        me_atoms = read(restart_info['Me']['trajfile'])
        qmme_atoms = read(restart_info['QMMe']['trajfile'])

        # consistency checks
        eps = 5e-4

        idx = {}
        idx['qm_part'] = qmme_atoms.info['qm_part']
        idx['me_part'] = qmme_atoms.info['me_part']
        qm_lattice_scale = qmme_atoms.info['qm_lattice_scale']
        coords = Coordinates(qm_lattice_scale=qm_lattice_scale, Ndim=3)

        # checking positions
        if np.allclose(qmme_atoms.get_positions()[idx['me_part']], me_atoms.get_positions(), atol=eps):
            print('\tQM/Me and Me positions are consistent')
        else:
            msg='QM/Me and Me positions are inconsistent'
            raise ValueError(msg)

        if np.allclose(coords.points_me_to_qm(qmme_atoms.get_positions()[idx['qm_part']]), qm_atoms.get_positions(), atol=eps):
            print('\tQM/Me and QM positions are consistent')
        else:
            msg='QM/Me and QM positions are inconsistent'
            raise ValueError(msg)

        if not friction_model==qm_atoms.info['friction_model']:
            warnings.warn('\tFriction model has changed. Are you sure?')

        atoms = [qm_atoms, me_atoms, qmme_atoms]
        restart_time = qm_atoms.info['time']
        if np.all([restart_time == a.info['time'] for a in atoms]):
            print('\tSub-system times are consistent. Restarting with time = {} fs'.format(restart_time))


        # Restore Me
        print('\nRe-creating Me-system')

        me_forces = me_atoms.get_forces() * units.EV_PER_ANGSTROM_TO_AU
        me_energy = me_atoms.get_potential_energy() * units.EV_TO_AU

        print('\tAttaching LAMMPS calculator to restored atoms')
        me_atoms = attach_me_calc(me_atoms)

        # sanity check
        check_lammps_calc(me_atoms.calc)

        print('\tInitializing LammpsMe system')

        folder = os.path.join(outfolder, 'me')

        me_system = LammpsMe(atoms=me_atoms,
                             calc_folder=folder,
                             _verbose=verbose)

        # here we go, set the forces, but remember that we need AU here
        print('\tRestoring forces and energy')
        me_system.set_forces(me_forces)
        me_system.set_potential_energy(me_energy)

        # Restore QM
        print('\nRe-creating QM-system')

        # save here, otherwise new calculator deletes it
        qm_forces = qm_atoms.get_forces() * units.EV_PER_ANGSTROM_TO_AU
        qm_energy = qm_atoms.get_potential_energy() * units.EV_TO_AU
        qm_etas = qm_atoms.info['etas'] * units.AMU_PER_FS_TO_AU
        qm_forces_slab = qm_atoms.info['forces_slab'] * units.EV_PER_ANGSTROM_TO_AU
        qm_forces_qm = qm_atoms.info['forces_qm'] * units.EV_PER_ANGSTROM_TO_AU

        print('\tAttaching CASTEP calculator to restored atoms object')
        qm_atoms = attach_qm_calc(qm_atoms)

        # sanity check
        check_castep_calc(qm_atoms.calc)

        print('\tInitializing QM system using friction model "{}"'.format(friction_model))

        folder = os.path.join(outfolder, 'qm')
        qm_system = CastepQM(qm_atoms,
                             calc_folder=folder,
                             seed=common_seed + '__QM',
                             adsorbate_idx = qm_atoms.info['adsorbate_idx'],
                             friction=friction_model,
                             _verbose=False,
                             _keep_checkfiles=keep_checkfiles,
                             # will always be False for our purpose...
                             _eval_parallel = False)

        # possibly attaching restart information
        checkfile = restart_info['castep']['atoms']['check']
        if checkfile:
            print('\t\tRe-using checkfile for atoms: {}'.format(checkfile))
            qm_system.atoms.calc._try_reuse = True
            # put the absolute path here
            checkfile = os.path.abspath(checkfile)
            # we set this internal variable here, since also ncalls is > 0; see
            # castep calculator for details
            qm_system.atoms.calc._check_file = checkfile
            # also set this variable as it is preferred
            qm_system.atoms.calc._castep_bin_file = checkfile

        checkfile = restart_info['castep']['slab_atoms']['check']
        if checkfile:
            print('\t\tRe-using checkfile for slab atoms: {}'.format(checkfile))
            qm_system.atoms.calc._try_reuse = True
            checkfile = os.path.abspath(checkfile)
            qm_system.slab_atoms.calc._check_file = checkfile
            qm_system.slab_atoms.calc._castep_bin_file = checkfile

        # set the call flags of the calculators
        qm_system.atoms.calc._calls = qm_atoms.info.get('atoms.calc._calls', 0)
        qm_system.atoms.calc._continuation_calls = qm_atoms.info.get('atoms.calc._continuation_calls', 0)
        qm_system.slab_atoms.calc._calls = qm_atoms.info.get('slab_atoms.calc._calls', 0)
        qm_system.slab_atoms.calc._continuation_calls = qm_atoms.info.get('slab_atoms.calc._continuation_calls', 0)


        # here we go, set the forces, but remember that we need AU here
        # that's why we converted before
        print('\tRestoring forces, energy and friction coefficients')
        qm_system.set_forces(qm_forces)
        qm_system.set_potential_energy(qm_energy)
        qm_system.set_etas(qm_etas)

        # restoring stuff from the forces calculations
        qm_system._set_forces_castep_calc(forces_slab = qm_forces_slab,
                                          forces_qm = qm_forces_qm)

        # tricking the calculator to not re-evaluate this step (without having a castep file)
        qm_system.atoms.calc._forces = qm_forces
        qm_system.atoms.calc._energy_0K = qm_energy
        qm_system.atoms.calc._energy_total = qm_energy
        qm_system.atoms.calc.push_oldstate()

        print('\nRe-assembling QM/Me+EF system')

        # again, we need AU to assign the forces
        qmme_forces = qmme_atoms.get_forces() * units.EV_PER_ANGSTROM_TO_AU
        qmme_energy = qmme_atoms.get_potential_energy() * units.EV_TO_AU
        qmme_etas = qmme_atoms.info['etas'] * units.AMU_PER_FS_TO_AU

        qmme_system = QMMe(me_system=me_system,
                           qm_system=qm_system,
                            _qm_lattice_scale=qm_lattice_scale,
                           _init_forces=False,
                           _init_etas=False,
                           _verbose=verbose)

        print('\tRestoring forces and energy')
        qmme_system.set_forces(qmme_forces)
        qmme_system.set_potential_energy(qmme_energy)
        qmme_system.set_etas(qmme_etas)

        qmme_system.set_time(restart_time * units.FS_TO_AU)

        _restart = True

        if preview:
            from ase.visualize import view
            for s in [qm_system, me_system, qmme_system]:
                view(s.atoms)
            raw_input('\nDemanded Preview... Press any key to continue')


        if dryrun:
            print('\n=== THIS IS A DRYRUN... ABORTING ===')
            return

    else:
        if np.any([i is None for i in [me_element,
                                       me_facette,
                                       me_size,
                                       me_a,
                                       qm_size,
                                       vacuum,
                                       add_adsorbate]]):
            msg='Incomplete input. Cannot initialize simulation.'
            raise ValueError(msg)

        print(section_template.format('Initializing simulation'))

        # Here goes the Me part
        print('Creating Me-System based on LAMMPS')

        # normalize the facette name
        print('\tCreating atoms object')

        if optimize_me:
            import ase.optimize

            # create a 1x1 surface unit cell to optimize first
            _me_atoms = create_me_atoms(me_element=me_element,
                                        me_facette=me_facette,
                                        me_size=[1,1,me_size[-1]],
                                        me_a=me_a,
                                        vacuum=vacuum)

            _me_atoms.set_pbc(True)
            _me_atoms = attach_me_calc(_me_atoms)

            print('\tOptimizing (1x1x{}) Me slab'.format(me_size[-1]))
            try:
                opt = getattr(ase.optimize, optimizer)
                print('\t\tUsing optimizer "{}" through ase'.format(optimizer))

            except:
                msg='Optimizer "{}" is not implemented through ase'.format(optimizer)
                raise NotImplementedError(msg)

            # do not need the IO from the optimizer
            stdout = sys.stdout
            sys.stdout = StringIO.StringIO()

            opt_inst = opt(atoms=_me_atoms)
            opt_inst.run(**optimizer_run_kwargs)

            # enable IO again
            sys.stdout = stdout

            # evaluate the interlayer-distances
            Zs = np.unique(_me_atoms.get_positions()[:,-1])

            ds = np.abs(Zs[:-1]-Zs[1:])

            msg = '\t\tResulting interlayer distances'
            max_dist_info = 5

            if len(ds)> max_dist_info:
                msg += ' (first {} distances only)'.format(max_dist_info)
            print(msg+':')

            for i,d in enumerate(ds):
                if i >= max_dist_info:
                    print('\t\t\t...')
                    break

                print('\t\t\t{:10s} : {:.3f}'.format('d_{}-{}'.format(i+1, i+2), d))


            print('\tExtending (1x1x{}) Me slab to actual size of ({}x{}x{})'.format(me_size[-1], *me_size))
            me_atoms = _me_atoms.repeat((me_size[0], me_size[1], 1))

        else:
            # create the atoms object
            me_atoms = create_me_atoms(me_element=me_element,
                                       me_facette=me_facette,
                                       me_size=me_size,
                                       me_a=me_a,
                                       vacuum=vacuum)


        # get the lammps calculator
        print('\tAttaching LAMMPS calculator')

        me_atoms.set_pbc(True)
        me_atoms = attach_me_calc(me_atoms)

        # sanity check
        check_lammps_calc(me_atoms.calc)

        # shift top layer to zero
        maxZ = np.max(me_atoms.get_positions()[:,2])
        me_atoms.positions[:,2] -= maxZ

        print('\tInitializing LammpsMe system')

        folder = os.path.join(outfolder, 'me')

        me_system = LammpsMe(atoms=me_atoms,
                             calc_folder=folder,
                             _verbose=verbose)


        # creating the QM system
        print('\nCreating QM-system based on CASTEP')

        if qm_a is None:
            qm_lattice_scale = 1.
        else:
            qm_lattice_scale = qm_a / me_a

        print('\tCutting QM slab from Me slab')

        qm_atoms = cut_qm_from_me(me_atoms,
                                  me_size=me_size,
                                  qm_size=qm_size,
                                  _qm_lattice_scale=qm_lattice_scale,
                                  _verbose=verbose)
        print('\tAdding adsorbate')

        # save the old atoms to automatically detect the adsorbate idx
        _old_qm_atoms = qm_atoms.copy()

        qm_atoms = add_adsorbate(qm_atoms)

        print('\tDetecting adsorbate index')

        adsorbate_idx = find_adsorbate_idx(atoms_with_adsorbate=qm_atoms,
                                           atoms_without_adsorbate=_old_qm_atoms)

        # get the lammps calculator
        print('\tAttaching CASTEP calculator')

        qm_atoms = attach_qm_calc(qm_atoms)

        # sanity check
        check_castep_calc(qm_atoms.calc)

        print('\tInitializing QM system using friction model "{}"'.format(friction_model))

        folder = os.path.join(outfolder, 'qm')

        qm_system = CastepQM(qm_atoms,
                             calc_folder=folder,
                             adsorbate_idx = adsorbate_idx,
                             seed = common_seed + '__QM',
                             friction=friction_model,
                             _verbose=verbose,
                             _keep_checkfiles=keep_checkfiles,
                             # will always be False for our purpose...
                             _eval_parallel = False)

        print('\nAssembling QM/Me+EF system')

        qmme_system = QMMe(me_system=me_system,
                           qm_system=qm_system,
                           _qm_lattice_scale=qm_lattice_scale,
                           _init_forces=False,
                           _init_etas=False,
                           _verbose=verbose)

        print('\tKinetic energy of the system : {:.3f} eV'.format(qmme_system.get_kinetic_energy()))

        _restart=False

        if preview:
            from ase.visualize import view
            for s in [qm_system, me_system, qmme_system]:
                view(s.atoms)
            raw_input('\nDemanded Preview... Press any key to continue')

        if dryrun:
            print('\n=== THIS IS A DRYRUN... ABORTING ===')
            return

        print('\nInitializing forces and friction coefficients...', end='')

        timings['scf'][0] = {'start' : time.time()}

        qmme_system._init_forces()
        qmme_system._init_etas()
        print(' done')

        timings['scf'][0].update({'end' : time.time()})

    # ok, here we are done with initiializing the systen
    # empty print are for pretty io
    print()

    integrator = create_integrator(method=integrator,
                                   dt=timestep,
                                   seed=random_seed,
                                   verbose=True)


    folder = os.path.join(outfolder, 'trajectories')
    handlers = {}

    print()

    for seed, system in [('QMMe', qmme_system),
                         ('QM', qm_system),
                        ]:
        print('Created {} handler with calculator observables'.format(seed))
        for p in observed_quantities:
            print('\t{}'.format(p))

        handler = create_trajectoryhandler(system_inst=system,
                                           verbose=False,
                                           outfolder=folder,
                                           properties=observed_quantities,
                                           seed=common_seed + '__' + seed)
        if not _restart:
            if not handler.mode =='w':
                raise RuntimeError('No restart but handler.mode != "w"')
            handler.push()
        else:
            if not handler.mode =='a':
                raise RuntimeError('Restart but handler.mode != "a"')

        handlers[seed] = handler

    timings['init'].update({'end' : time.time()})
    timings['init']['timing'] = format_timing(timings['init']['start'], timings['init']['end'])

    timings['propagation'] = {'start' : time.time()}

    # here comes the time-propagation
    print(section_template.format('Time propagation'))
    print('--> Propagating with time step dt = {} fs'.format(timestep))

    nstep=0


    while True:
        try:
            nstep += 1

            print('\tStep {} (system time = {} fs)... '.format(nstep, qmme_system.get_time()), end='')

            timings['scf'][nstep] = {'start' : time.time()}

            integrator.propagate(qmme_system)

            for seed, update_step in [('QMMe', qmme_update_step),
                                      ('QM', qm_update_step),
                                     ]:
                if nstep%update_step == 0:
                    handlers[seed].push()

            timings['scf'][nstep].update({'end' : time.time()})

            print('done (timing : {})'.format(format_timing(timings['scf'][nstep]['start'],
                                                       timings['scf'][nstep]['end'])))

            if qmme_system.get_time() >= simulation_time:
                print('--> End of simulation reached.')
                break

            # check if we have time for one more cycle
            avg_time = (time.time() - timings['propagation']['start']) / (nstep + 1)
            runtime = time.time() - timings['program']['start']

            scale = 1.3
            buffertime = avg_time
            if walltime:
                if (walltime - runtime - buffertime) < (avg_time * scale):
                    print('--> Insufficient walltime left. Breaking propagation loop at t = {} fs and writing restart files.'.format(qmme_system.get_time()))
                    break
        except KeyboardInterrupt:
            print('Caught KeyboardInterrupt... Exiting')
            return

    timings['propagation'].update({'end' : time.time()})
    timings['propagation']['timing'] = format_timing(timings['propagation']['start'], timings['propagation']['end'])
    timings['propagation']['avg_timing'] = format_time((timings['propagation']['end']-timings['propagation']['start']) / nstep)

    print(section_template.format('Finalizing simulation'))
    print('Closing trajectory handlers')

    for seed, handler in handlers.items():
        print('\t{} handler'.format(seed))
        handler.close()


    # write a restart trajectory
    print('\nWriting restart information')

    # restart things
    folder = os.path.join('restart')

    # rename possibly existing restart folder
    if os.path.isdir(folder):
        # this is taken from the ase castep calculator
        ctime = time.localtime(os.lstat(folder).st_ctime)
        os.rename(folder, '{}.bak-{}'.format(folder, time.strftime('%Y%m%d-%H%M%S', ctime)))

    restart_prfx = os.path.join(folder, 'restart.{}'.format(seed))



    if not os.path.exists(folder):
        os.makedirs(folder)

    for name, system in [('QMMe', qmme_system),
                         ('Me', me_system),
                         ('QM', qm_system)]:
        fname = restart_prfx + '.' + name + '.traj'
        atoms =  system.atoms.copy()
        restart_traj = Trajectory(atoms=atoms,
                                  filename=fname,
                                  mode = 'w')

        print('\t{}'.format(fname))

        # makes it easier to restore things
        if name == 'QMMe':
            info = {'etas' : system.get_etas()}
        elif name == 'QM':
            info = {'forces_qm' : system.get_forces_qm(),
                    'forces_slab' : system.get_forces_slab()}
        else:
            info = {}

        atoms.info.update(info)

        # do not ask why we have to do this here... I do not know it myself
        restart_traj.write(atoms, forces=system.get_forces(),
                                  energy=system.get_potential_energy())
        restart_traj.close()

    # the checkfiles are only linked
    _linknames = []
    print('\nLinking last checkfiles')
    for name, atoms in [('full_system', qm_system.atoms), ('slab_only', qm_system.slab_atoms)]:
        linkname = restart_prfx + '.{}.check'.format(name)
        checkfile = os.path.relpath(os.path.abspath(os.path.join(atoms.calc._directory,
                                                                 atoms.calc._seed + '.check')), folder)

        print('\t{}'.format(linkname))
        os.symlink(checkfile, linkname)
        _linknames.append(linkname)

    print('\nNOTE:')
    print('-----')
    print('You may want to resolve the symlinks before using them to restart a simulation!')
    print('This can be done via (in the restart folder):')
    for link in _linknames:
        link = os.path.basename(link)
        print('\tcp --remove-destination $(readlink {0}) {0}'.format(link))

    timings['program'].update({'end' : time.time()})
    timings['program']['timing'] = format_timing(timings['program']['start'], timings['program']['end'])


    print(section_template.format('Runtime information'))
    print('System initialization          : {}'.format(timings['init']['timing']))
    print('Time propagation               : {}'.format(timings['propagation']['timing']))
    print('Average time per step          : {}'.format(timings['propagation']['avg_timing']))
    print('------------------------------   -----------')
    print('Total Runtime                  : {}'.format(timings['program']['timing']))

    print(section_template.format('Program ended : {} -- Have a nice day :)'.format(get_date())))


def run(attach_me_calc,
        attach_qm_calc,
        add_adsorbate,
        me_element,
        me_facette,
        me_size,
        me_a,
        vacuum,
        qm_size,
        friction_model=None,
        simulation_time=10,
        timestep=1,
        integrator='liouville',
        qm_update_step=1,
        qmme_update_step=1,
        walltime=None,
        seed='QMMeEF-MDsim',
        outfolder='output',
        qm_a=None,
        optimize_me=True,
        optimizer='FIRE',
        optimizer_run_kwargs={},
        observed_quantities=['forces', 'energy'],
        preview=False,
        random_seed=None,
        keep_checkfiles=2,
        check_installation=True,
        dryrun=False,
        verbose=False):
    """
    Wrapper to run a new simulation and to avoid mal-usage.

    Parameters
    ----------
    attach_me_calc : function
        Function that attaches the Me calculator to an atoms object provided as
        argument. Must return the atoms object with the attached calculator. If
        the latter is not an extended LAMMPSrun calculator, an error will be
        raised. In short, this function must return an atoms object with
        attached calculator upon calling it via "attach_me_calc(atoms)" See the
        example below for more details.

    attach_qm_calc : function
        Function that attaches the QM calculator to the respective atoms
        object. Same behavior as "attach_me_calc". Yet, if no ExtendedCastep
        calculator is attached, error will be raised.

    add_adsorbate : function
        Function that takes the slab-part of the QM cell as input and adds the
        adsorbate to the respective atoms object. See the example below for
        more details.

    me_element : string
        The chemical symbol of the surface element.

    me_facette : string
        The surface facette of the metall slab. Currently, this must be one of
        the facettes implemented by ase.lattice.surface.

    me_size : 3-tuple
        The size of the Me supercell in x, y and z direction.

    me_a : float
        The lattice constant as obtained on the MEAM level.

    vacuum : float
        Vacuum layer thickness separating the slabs. Note that this is in
        Me-units, in case the lattice parameters differs from the QM value.

    qm_size : 3-tuple
        The size of the QM cell in x,y and z-direction. Will be cut from the Me
        cell, as long as a) smaller than Me cell and b) angles of unit vectors
        remains the same.

    friction_model : string, optional (default=None):
        Embedding model to be applied for the LDFA electronic friction. Either
        'IAA' or 'AIM'.

    simulation_time : float, optional (default=10)
        Simulation time in fs.

    timestep : float, optional (default=1)
        Time step size for the numerical integrator.

    integrator : string, optional (default='Liouville')
        Integration scheme applied. Any of the integrators from
        MDsim.integrators can be used. Yet, without any temperature,
        'Liouville' is a no-brainer. If you have temperature I recommened
        'Bussi Parrinello'.

    qm_update_step : int, optional (default=1):
        Everyt nth step, will be dumped to the QM trajectory.

    qmme_update_step : int, optional (default=1):
        Everyt nth step, will be dumped to the QM/Me trajectory.

    walltime : float, optional (default=None)
        Walltime is *seconds*. The routine will make sure that the simulation
        loop is abandoned and restart information are written before this limit
        is reached.

    seed : string, optional (default='QMMeEF-MDsim')
        Common seed for all output files.

    outfolder : string, optional (default='output')
        Folder that contains all output written. Will be created if not yet
        existing.

    qm_a : float, optional (default = None)
        The lattice constant of the metal as obtained with the applied DFT
        setup. If differing from the Me value, positions will be scaled
        accordingly *in all dimensions*! *Will not be considered for restarted
        simulations*.

    optimize_me : boolean, optional (default=True)
        Update the Me system using the specified <optimizer> after setting up a
        new simulation. *Will not be considerd for restarted simulations.*

    optimizer : string, optional (default='FIRE')
        Any of the optimizers provided through ase.optimize. *Will not be
        considered for restarted calculations.*

    optimizer_run_kwargs : dictionary, optional (default={})
        Arguments passed to the optimizer. *Will not be considered for
        restarted calculations.*

    observed_quantities : List of strings, optional (default = ['energy', 'forces'])
        Will be directly passed to the "properties" argument of the ase
        trajectory backend, see:
            https://wiki.fysik.dtu.dk/ase/ase/trajectory.html

    preview : boolean, optional (default=False):
        The gui from ase will be used to display the systems before any
        calculation starts. May lead to problems if you do not have an X server
        running.

    random_seed : int, optional (default=None)
        Seed for the random number generator (if applied).

    verbose : boolean, optional (default=False):
        Controls the verbosity of the sub-parts of the simulation. May lead to
        lots of output to stdout, but helpful when debugging.

    check_installation : boolean, optional (default=True)
        Check of required CASTEP, LAMMPS, etc are available.

    Example
    -------
    Below are examples of the definitions of the required functions. This is
    just a guideline for the user how these functions should react.

    >>> def attach_me_calc(me_atoms):
            import os
            from MDsim.bindings.lammps import ExtendedLAMMPS
            # setting up the lammps calculator
            pair_style = 'meam'
            # use an abspath here!
            Pd_eam_file = os.path.abspath('Pd-00PBE.meam')
            pair_coeff = ['* * ' + Pd_eam_file + ' Pd NULL Pd']
            parameters = {'pair_style' : pair_style,
                          'pair_coeff' : pair_coeff}
            files = [Pd_eam_file]
            calc = ExtendedLAMMPS(parameters=parameters,
                                  files=files)
            me_atoms.set_calculator(calc)
            return me_atoms

    >>> def add_adsorbate(qm_atoms):
            import numpy as np
            from MDsim import units
            # This routine should as well assign momenta!
            pos = qm_atoms.get_positions()[np.lexsort(qm_atoms.get_positions().T)][-1]
            height = 1.5
            pos[-1] += height
            # kinetic energy in eV
            Ekin = 5
            mass = 83.798
            mom = -np.sqrt(2 * Ekin * units.EV_TO_AU * mass
                              * units.AMU_TO_AU)*units.AU_TO_ANGSTROM_AMU_PER_FS
            adsorbate_element='Kr'
            qm_atoms.extend(ase.Atoms(adsorbate_element, positions=[pos],
                                      momenta=[[0,0,mom]]))
            return qm_atoms

    >>> def attach_qm_calc(qm_atoms):
            import numpy as np
            from MDsim.bindings.castep import ExtendedCastep
            calc = ExtendedCastep(verbose=False)
            calc._castep_command = 'mpiexec.openmpi -np 4 castep'
            calc._rename_existing_dir = False
            calc._copy_pspots = False
            calc._link_pspots = True
            calc.spin_polarized = False
            qm_atoms.set_calculator(calc)
            calc.set_pspot(pspot = 'OTF')
            calc.cell.fix_com = False
            calc.cell.kpoints_mp_grid = '4 4 1'
            return qm_atoms
    """
    main(attach_me_calc=attach_me_calc,
         attach_qm_calc=attach_qm_calc,
         me_element=me_element,
         me_facette=me_facette,
         me_size=me_size,
         me_a=me_a,
         vacuum=vacuum,
         qm_size=qm_size,
         add_adsorbate=add_adsorbate,
         friction_model=friction_model,
         simulation_time=simulation_time,
         timestep=timestep,
         integrator=integrator,
         qm_update_step=qm_update_step,
         qmme_update_step=qmme_update_step,
         walltime=walltime,
         seed=seed,
         outfolder=outfolder,
         qm_a=qm_a,
         optimize_me=optimize_me,
         optimizer=optimizer,
         optimizer_run_kwargs=optimizer_run_kwargs,
         observed_quantities=observed_quantities,
         preview=preview,
         random_seed=random_seed,
         verbose=verbose,
         restart_folder=None,
         dryrun=dryrun,
         keep_checkfiles=keep_checkfiles,
         check_installation=check_installation
         )


def restart(attach_me_calc,
            attach_qm_calc,
            restart_folder,
            friction_model=None,
            simulation_time=10,
            timestep=1,
            integrator='liouville',
            qm_update_step=1,
            qmme_update_step=1,
            walltime=None,
            seed='QMMeEF-MDsim',
            outfolder='output',
            observed_quantities=['forces', 'energy'],
            preview=False,
            dryrun=False,
            random_seed=None,
            keep_checkfiles=2,
            check_installation=True,
            verbose=False):
    """
    Wrapper function around main() to facilitate the use and to avoid mal-usage.

    Parameters
    ----------
    attach_me_calc : function
        Function that attaches the Me calculator to an atoms object provided as
        argument. Must return the atoms object with the attached calculator. If
        the latter is not an extended LAMMPSrun calculator, an error will be
        raised. In short, this function must return an atoms object with
        attached calculator upon calling it via "attach_me_calc(atoms)" See the
        example below for more details.

    attach_qm_calc : function
        Function that attaches the QM calculator to the respective atoms
        object. Same behavior as "attach_me_calc". Yet, if no ExtendedCastep
        calculator is attached, error will be raised.

    restart_folder : string
        Path to restart folder written after previous calculation. Must contain
        at least the traj files, it is benefitial if also the check files are
        in there to make use of density extrapolation within castep.

    friction_model : string, optional (default=None):
        Embedding model to be applied for the LDFA electronic friction. Either
        'IAA' or 'AIM'.

    simulation_time : float, optional (default=10)
        Simulation time in fs.

    timestep : float, optional (default=1)
        Time step size for the numerical integrator.

    integrator : string, optional (default='Liouville')
        Integration scheme applied. Any of the integrators from
        MDsim.integrators can be used. Yet, without any temperature,
        'Liouville' is a no-brainer. If you have temperature I recommened
        'Bussi Parrinello'.

    qm_update_step : int, optional (default=1):
        Everyt nth step, will be dumped to the QM trajectory.

    qmme_update_step : int, optional (default=1):
        Everyt nth step, will be dumped to the QM/Me trajectory.

    walltime : float, optional (default=None)
        Walltime is *seconds*. The routine will make sure that the simulation
        loop is abandoned and restart information are written before this limit
        is reached.

    seed : string, optional (default='QMMeEF-MDsim')
        Common seed for all output files.

    outfolder : string, optional (default='output')
        Folder that contains all output written. Will be created if not yet
        existing.

    optimize_me : boolean, optional (default=True)
        Update the Me system using the specified <optimizer> after setting up a
        new simulation. *Will not be considerd for restarted simulations.*

    optimizer : string, optional (default='FIRE')
        Any of the optimizers provided through ase.optimize. *Will not be
        considered for restarted calculations.*

    observed_quantities : List of strings, optional (default = ['energy', 'forces'])
        Will be directly passed to the "properties" argument of the ase
        trajectory backend, see:
            https://wiki.fysik.dtu.dk/ase/ase/trajectory.html

    preview : boolean, optional (default=False):
        The gui from ase will be used to display the systems before any
        calculation starts. May lead to problems if you do not have an X server
        running.

    random_seed : int, optional (default=None)
        Seed for the random number generator (if applied).

    verbose : boolean, optional (default=False):
        Controls the verbosity of the sub-parts of the simulation. May lead to
        lots of output to stdout, but helpful when debugging.

    check_installation : boolean, optional (default=True)
        Check of required CASTEP, LAMMPS, etc are available.

    Example
    -------
    Below are examples of the definitions of the required functions. This is
    just a guideline for the user how these functions should react.

    >>> def attach_me_calc(me_atoms):
            import os
            from MDsim.bindings.lammps import ExtendedLAMMPS
            # setting up the lammps calculator
            pair_style = 'meam'
            # use an abspath here!
            Pd_eam_file = os.path.abspath('Pd-00PBE.meam')
            pair_coeff = ['* * ' + Pd_eam_file + ' Pd NULL Pd']
            parameters = {'pair_style' : pair_style,
                          'pair_coeff' : pair_coeff}
            files = [Pd_eam_file]
            calc = ExtendedLAMMPS(parameters=parameters,
                                  files=files)
            me_atoms.set_calculator(calc)
            return me_atoms

    >>> def attach_qm_calc(qm_atoms):
            import numpy as np
            from MDsim.bindings.castep import ExtendedCastep
            calc = ExtendedCastep(verbose=False)
            calc._castep_command = 'mpiexec.openmpi -np 4 castep'
            calc._rename_existing_dir = False
            calc._copy_pspots = False
            calc._link_pspots = True
            calc.spin_polarized = False
            qm_atoms.set_calculator(calc)
            calc.set_pspot(pspot = 'OTF')
            calc.cell.fix_com = False
            calc.cell.kpoints_mp_grid = '4 4 1'
            return qm_atoms
    """
    main(attach_me_calc=attach_me_calc,
         attach_qm_calc=attach_qm_calc,
         restart_folder=restart_folder,
         me_element=None,
         me_facette=None,
         me_size=None,
         me_a=None,
         vacuum=None,
         qm_size=None,
         add_adsorbate=None,
         friction_model=friction_model,
         simulation_time=simulation_time,
         timestep=timestep,
         integrator=integrator,
         qm_update_step=qm_update_step,
         qmme_update_step=qmme_update_step,
         walltime=walltime,
         seed=seed,
         outfolder=outfolder,
         qm_a=None,
         optimize_me=False,
         optimizer='FIRE',
         optimizer_run_kwargs={},
         observed_quantities=observed_quantities,
         preview=preview,
         random_seed=random_seed,
         dryrun=dryrun,
         keep_checkfiles=keep_checkfiles,
         check_installation=check_installation,
         verbose=verbose
         )


