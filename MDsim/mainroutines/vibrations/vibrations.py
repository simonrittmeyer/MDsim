# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import sys
import re

import numpy as np
import time
import copy

from MDsim import units
from MDsim import __version__

from MDsim.prettyprint import format_time
from MDsim.prettyprint import get_date
from MDsim.prettyprint import update_progress

from MDsim.tools.options import update_options_values
from MDsim.tools.options import create_args_dict
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_filehandler

from MDsim.prettyprint.stdouthandler import StdoutHandler

from MDsim.mainroutines.vibrations.options import options_set
from MDsim.mainroutines.vibrations.options import get_options_dict
from MDsim.mainroutines.vibrations.sanity import check_sanity

logo="""
+------------------------------------------------------------------------------+
|                __     ___ _     ____                                         |
|                \ \   / (_) |__ |  _ \  __ _ _ __ ___  _ __                   |
|                 \ \ / /| | '_ \| | | |/ _` | '_ ` _ \| '_ \                  |
|                  \ V / | | |_) | |_| | (_| | | | | | | |_) |                 |
|                   \_/  |_|_.__/|____/ \__,_|_| |_| |_| .__/                  |
|                                                      |_|                     |
|                                                                              |
|                             Simon P. Rittmeyer                               |
|                         simon.rittmeyer(at)tum.de                            |
+------------------------------------------------------------------------------+
| Part of the MDsim package {0:<51}|
+------------------------------------------------------------------------------+
"""[1:-1:].format('(version {})'.format(__version__))


def main(infile=None, py_args={}, cmd_args={}, verbose=True, progressbar=True):
    """
    Vibrational Damping main routine.

    We have the following order in case of conflicting arguments

        py_args > cmd_args > infile

    Parameters
    ----------
    infile : str, optional (default=None)
        Infile to be parsed.

    py_args : dictionary, optional (default = {})
        Options additionally passed on a python level.

    cmd_args : dictionary, optional (default = {})
        Options passed from the command line.

    verbose : boolean, optional (default = True)
        Print information to stdout. If False, then simply stdout is directed
        to /dev/null ;)

    progressbar : bool, optional (default=True)
        Show a progress bar that updates dynamically. Do *not* use this option
        if you pipe to a file.
    """

    # this is the dictionary in which we store the timings
    tmp = time.time()
    _start = tmp
    times = {'start': tmp}

    if not verbose:
        devnull = open(os.devnull, 'w')
        _stdout = sys.stdout
        sys.stdout = devnull

    section_template = StdoutHandler._section_template

    print(logo[:-1])
    print(section_template.format('Program started : {}'.format(get_date())))

    # sanity check for arguments

    print(section_template.format('INITIALIZATION Part I'))

    options_dict = get_options_dict()

    if infile:
        from MDsim.tools.parser import parse_inputfile
        options_dict = parse_inputfile(infile, options_dict)
    else:
        print('No input file given')

    if cmd_args:
        print('Updating options with additional command line arguments')
        options_dict = update_options_values(cmd_args, options_dict)
    else:
        print('No additional command line arguments passed')

    if py_args:
        print('Updating options with additional python-level input arguments')
        options_dict = update_options_values(py_args, options_dict)
    else:
        print('No additional python-level arguments passed')

    check_sanity(options_dict, verbose=verbose)

    # ok, now we can change things
    args = create_args_dict(options_dict)

    # ok let's go...
    if args['system.masses'] is None:
        from MDsim.tools import species_to_masses
        args['system.masses'] = species_to_masses(args['system.species'])


    # --------------------------------
    # SETTING UP THE POTENTIAL, Part I
    # --------------------------------

    # we need the normalmodes on our potential, so create it first
    from MDsim.tools.createobjects import create_bivariatepotential

    # as we do not allow for atomic coordinates so far, each DOF is considered an atom anyway.
    Natoms = len(args['system.masses'])
    Ndim = args['system.ndof']/Natoms

    positions_eq = np.asarray(args['system.positions_eq'], dtype=np.float64, order='C').reshape(Natoms, Ndim)

    potential = create_bivariatepotential(datafile=args['potential.potfile'],
                                          coordinates=args['potential.coordinates'],
                                          forces_method=args['potential.forces_method'],
                                          forces_stepwidth=args['potential.forces_stepwidth'],
                                          bounds_check=args['potential.bounds_check'],
                                          usecols=args['potential.potfile_usecols'],
                                          masses=args['system.masses'],
                                          positions_eq=positions_eq,
                                          normalize=args['potential.normalize'],
                                          convert=True,
                                          verbose=verbose)

    # -----------------------
    # SETTING UP THE FRICTION
    # -----------------------

    if args['friction.type'] == 'interpolated':
        from MDsim.tools.createobjects import create_bivariatefrictioncoefficient
        from MDsim.friction.electronicfriction.ldfa.matrix.aim import AIMFrictionMatrix
        etas = []
        for etafile, usecols in zip(args['friction.etafiles'], args['friction.etafiles_usecols']):
            eta = create_bivariatefrictioncoefficient(datafile=etafile,
                                                      coordinates=args['friction.coordinates'],
                                                      bounds_check=args['friction.bounds_check'],
                                                      usecols=usecols,
                                                      masses=args['system.masses'],
                                                      positions_eq=positions_eq,
                                                      convert=True,
                                                      verbose=verbose)
            etas.append(eta)
        print('Combining atomic friction coefficients to friction matrix')
        friction_matrix=AIMFrictionMatrix(friction_coefficients=etas)

    elif args['friction.type'] == 'constant':
        from MDsim.friction.constantfriction import ConstantFriction
        #ok, here just diagonal friction
        etas = np.ones((Natoms, Ndim), dtype=np.float64) * args['friction.values'][:,None]
        friction_matrix = ConstantFriction(etas, convert = True)

    elif args['friction.type'] == 'tensorial':
        from MDsim.tools.createobjects import create_tensorialfriction
        #ok, here just diagonal friction
        friction_matrix = create_tensorialfriction(masses=args['system.masses'],
                                                   friction_matrix_mw=args['friction.tensor'],
                                                   datafile=args['friction.tensor_file'],
                                                   convert=True,
                                                   verbose=verbose)

    else:
        friction_matrix = None

    # ---------------------
    # SETTING UP THE SYSTEM
    # ---------------------
    from MDsim.systems.system import System

    print('Creating system instance')
    system = System(positions=positions_eq,
                    masses=args['system.masses'],
                    species=args['system.species'],
                    potential=potential,
                    friction=friction_matrix,
                    convert=True,
                    _verbose=False)


    # --------------------------
    # DO THE NORMALMODE ANALYSIS
    # --------------------------

    print(section_template.format('NORMALMODE ANALYSIS'))

    from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential as evaluate_normalmodes
    # this yields the true minimum in a.u. and the normal modes at the same time
    # Nelder-Mead a.k.a. Downhill Simplex is just fine for simple downhill search
    omegas, nm, positions_min, normalmodes = evaluate_normalmodes(potential=potential,
                                                                  positions=positions_eq,
                                                                  masses=args['system.masses'],
                                                                  minimize_first=False,
                                                                  verbose=verbose,
                                                                  convert=True,
                                                                  delta=args['potential.normalmodes_delta'],
                                                                  method='BFGS')


    # we start from the true minimum :)
    # note that positions_min is in Angstrom, so set the convert flag here
    #system.set_positions(positions_min, _convert=True)

    # ------------------
    # PUMPING THE SYSTEM
    # ------------------

    print(section_template.format('VIBRATIONAL EXCITATION'))
    velocities, info  = normalmodes.pump_modes_kinetic_energy(mode_idx=np.arange(system.get_Ndof()),
                                                              quanta=args['system.init_quanta'],
                                                              _au=True,
                                                              zpe=args['system.zpe'],
                                                              sign=args['system.init_velocities_sign'],
                                                              verbose=verbose)

    # note that he returned velocities from above are in au already
    system.set_velocities(velocities)
    system.add_additional_info(info)


    print(section_template.format('INITIALIZATION Part II'))

    # ---------------------------------
    # SETTING UP THE POTENTIAL, Part II
    # ---------------------------------

    # this is the time to add a harmonic potential if we wanted any...
    if args['potential.type'] == 'harmonic':
        print('Creating Harmonic potential')
        from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
        potential = NormalmodePotential(normalmodes=normalmodes,
                                        forces_method=args['potential.forces_method'],
                                        forces_stepwidth=args['potential.forces_stepwidth'],
                                        convert=True)
        print('Attaching harmonic potential to system instance')
        system.set_potential(potential)

    elif args['potential.type'] == 'diagonal_anharmonic':
        print('Creating diagonal anharmonic potential')
        from MDsim.potentials.atomic.diagonalanharmonicpotential import DiagonalAnharmonicPotential
        potential = DiagonalAnharmonicPotential(normalmodes=normalmodes,
                                                full_potential=potential,
                                                forces_method=args['potential.forces_method'],
                                                forces_stepwidth=args['potential.forces_stepwidth'],
                                                convert=True)
        print('Attaching diagonal anharmonic potential to system instance')
        system.set_potential(potential)

    # -------------------------
    # SETTING UP THE INTEGRATOR
    # -------------------------

    # get a random seed
    if args['misc.random_seed'] is None:
        from MDsim.tools import randomize_seed
        random_seed = randomize_seed()
    else:
        random_seed = args['misc.random_seed']

    tensorial_friction = args['friction.type'] == 'tensorial'

    integrator = create_integrator(method=args['propagation.integrator'],
                                   dt=args['propagation.timestep'],
                                   seed=random_seed,
                                   convert=True,
                                   tensorial_friction=tensorial_friction,
                                   verbose=verbose)


    # --------------------------------------------
    # PRE-EVALUATING THE NUMBER OF STEPS NECESSARY
    # --------------------------------------------

    # comparing against au saves us Nsteps multiplications of the system time
    t_end = args['propagation.simulation_time'] * units.FS_TO_AU
    iostep = args['io.iostep']

    tot_steps = np.ceil(args['propagation.simulation_time'] /  args['propagation.timestep'])
    tot_iosteps = int(np.floor(tot_steps / iostep) + 1)


    # ----------------------------------
    # SETTING UP THE ON-THE-FLY HANDLERS
    # ----------------------------------

    handlers = {}
    if args['io.debug']:
        debug_args = args
    else:
        debug_args = None

    for obs in args['io.observables']:
        handler = create_filehandler(system_inst=system,
                                     integrator_inst=integrator,
                                     observable=obs,
                                     iatom=args['io.iatom'],
                                     fileformat=args['io.fileformat'],
                                     seed=args['io.seed'],
                                     outfolder=args['io.outpath'],
                                     compress=args['io.compress'],
                                     convert=True,
                                     datatype=args['io.datatype'],
                                     verbose=verbose,
                                     normalmodes=normalmodes,
                                     Nsteps=tot_iosteps,
                                     args=debug_args)

        # init the handler with the information from t=0
        handlers[obs] = handler

    print('\n--> Initialization finished')
    # stdouthandler
    stdouthandler = StdoutHandler(system=system,
                                  integrator=integrator)

    try:
        times['init'] += time.time() - tmp
    except KeyError:
        times['init'] = time.time() - tmp

    tmp = time.time()

    print(stdouthandler.get_system_info(system))
    print(stdouthandler.get_integrator_info(integrator))
    if args['io.debug']:
        if verbose:
            print(stdouthandler.get_options(args))

    # --------------------------
    # THE ACTUAL SIMULATION LOOP
    # --------------------------

    print(section_template.format('SIMULATION'))

    # now that we have the handlers we can start with the actual propagation
    print('Simulating {} fs with dt = {} fs, ie., {} steps in total\n\t(--> {} io steps)'.format(
            args['propagation.simulation_time'],
            args['propagation.timestep'],
            int(tot_steps),
            int(tot_iosteps)))

    nsteps = 0

    if progressbar:
        updatestep = int(np.ceil(tot_steps*0.001))
        print()

    # walltime checking
    _start_loop = float(time.time())
    if args['misc.walltime'] is not None:
        _exit_loop = 0.95*args['misc.walltime']
    else:
        _exit_loop=False

    _interrupt = False

    # initialize the handlers
    for handler in handlers.values():
        handler.push()

    # aliasing the propagate function in order to check for effective
    # energy logging
    if 'effective_energy' in args['io.observables']:
        propagate = lambda system: integrator.propagate(system, _log_effective_energy=True)
    else:
        propagate = lambda system: integrator.propagate(system)

    while nsteps < tot_steps:
        try:
            propagate(system)
            nsteps += 1

            if nsteps%iostep == 0:
                for handler in handlers.values():
                    handler.push()
                if _exit_loop:
                    if (time.time() - _start) > _exit_loop:
                        print('Breaking simulation loop due to insufficient walltime')
                        print('\tsimulated steps : {}'.format(nsteps))
                        print('\tsimulated time  : {} fs'.format(int(system.get_time())))
                        break
            if progressbar:
                if nsteps%updatestep == 0:
                    update_progress(nsteps / tot_steps)
        except KeyboardInterrupt:
            print('Caught KeyboardInterrupt... Exiting')
            _interrupt = True
            break

    if not _interrupt:
        if progressbar:
            print()
        print('Simulation finished')

    try:
        times['simulation'] += time.time() - tmp
    except KeyError:
        times['simulation'] = time.time() - tmp

    tmp = time.time()

    print(section_template.format('FINALIZING'))
    print('Closing handlers:')
    for h, handler in handlers.items():
        print('\t{}'.format(h))
        handler.close()

    try:
        times['finalizing'] += time.time() - tmp
    except KeyError:
        times['finalizing'] = time.time() - tmp

    tmp = time.time()

    # -----------------
    # END OF SIMULATION
    # -----------------

    # ok, now we are done ;)
    times['runtime'] = time.time() - times['start']

    print(section_template.format('RUNTIME INFORMATION'))
    print('Initialization    : {}'.format(format_time(times['init'])))
    print('Time propagation  : {}'.format(format_time(times['simulation'])))
    print('Finalizing        : {}'.format(format_time(times['finalizing'])))
    print('-----------------   -----------')
    print('Total Runtime     : {}'.format(format_time(times['runtime'])))

    print(section_template.format('Program ended : {} -- Have a nice day :)'.format(get_date())))

    # restore standard out
    if not verbose:
        sys.stdout = _stdout

    if _interrupt:
        raise KeyboardInterrupt
