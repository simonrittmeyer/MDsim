# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os

from MDsim.tools.options import create_args_dict
from MDsim.observables import require_iatom


def check_sanity(options_dict, verbose=True):
    """
    Check if we have everything we need
    """
    args = create_args_dict(options_dict)

    # values that must not be None in any case
    required = ['system.species',
                'system.init_quanta',
                'system.positions_eq',
                'potential.type',
                'propagation.simulation_time',
                'propagation.timestep',
                'io.observables']

    complete = True

    if verbose:
        print('Checking for input completeness')

    msg = 'Incomplete input! The following arguments must not be None:'

    for req in required:
        if args[req] is None:
            msg += '\n\t* "{}"'.format(req)
            complete = False

    if not complete:
        if verbose:
            print(msg)
        raise ValueError(msg)

    if verbose:
        print('Checking for conditional requirements')

    msg = ''

    if args['system.ndof'] != 2:
        msg += 'More than two degrees of freedom are not (yet) supported.'

    elif args['system.ndof'] != len(args['system.init_quanta']):
        msg += 'Number of DOF does not match initial quanta'

    if args['system.masses'] is None:
        try:
            from MDsim.tools import species_to_masses
            species_to_masses(args['system.species'])
        except KeyError:
            msg += '\nNo masses given but species-to-mass conversion fails'


    # checking for the PES
    complete=True
    if args['potential.type'] == 'interpolated':
        err_msg = '\nInterpolation for potential but:'
        if args['potential.coordinates'] is None:
            err_msg += '\n\t* no coordinates specified'
            complete=False
        if args['potential.potfile'] is None:
            err_msg += '\n\t* no potfile given'
            complete = False
        if args['potential.potfile_usecols'] is None:
            err_msg += '\n\t* no potfile_usecols given'
            complete = False
        elif not os.path.isfile(args['potential.potfile']):
            raise IOError('Invalid potfile location')

        if not complete:
            msg += err_msg

    # checking for the friction
    complete=True
    if args['friction.type'] == 'interpolated':
        err_msg = '\nInterpolation for friction coefficients but:'
        if args['friction.coordinates'] is None:
            err_msg += '\n\t* no coordinates specified'
        if args['friction.etafiles'] is None:
            err_msg += '\n\t* no etafiles given'
            complete = False
        elif len(args['system.species']) != len(args['friction.etafiles']):
            err_msg += '\n\t* number of etafiles does not match number of species'
            complete = False
        else:
            for f in args['friction.etafiles']:
                if not os.path.isfile(f):
                    raise IOError('Invalid etafile location {}'.format(f))
        if args['friction.etafiles_usecols'] is None:
            err_msg += '\n\t* no etafiles_usecols given'
            complete = False
        elif len(args['friction.etafiles']) != len(args['friction.etafiles_usecols']):
            err_msg += '\n\t* number of etafiles_usecols does not match number of files'
            complete = False


    elif args['friction.type'] == 'constant':
        if args['friction.values'] is None:
            msg += '\nConstant friction chosen but no values given'
        elif len(args['system.species']) != len(args['friction.values']):
            err_msg += '\n\t* number of values does not match number of species'
            complete = False

    if not complete:
        msg += err_msg

    if args['friction.type'] == 'tensorial':
        s = ''
        for o in args['io.observables']:
            if 'eta' in o.lower():
                s += '\n\t * observable "{}" incompatible with tensorial friction'.format(o)
        if s:
            s = '\nTensorial friction chosen but no tensor/file given' + s
            msg += s
            complete=False

        if args['friction.tensor'] is None and args['friction.tensor_file'] is None:
            msg += '\nTensorial friction chosen but no tensor/file given'
            complete = False

        if args['friction.values'] is not None:
            msg += '\nTensorial friction chosen but "values" given. This is ambiguous -- use "tensor"!'
            complete = False

        if args['friction.tensor'] is not None and args['friction.tensor_file'] is not None:
            msg += '\nTensorial friction chosen but ambiguous input: tensor and file given'
            complete = False

        if args['friction.tensor_file'] is not None:
            if not os.path.isfile(f):
                raise IOError('Invalid tensor file location {}'.format(args['friction.tensor_file']))

        if args['friction.tensor'] is not None:
            if not len(args['friction.tensor'].shape) == 2:
                msg += '\nTensorial friction chosen but input tensor has invalid dimensionality'
                complete = False

    # checking for the observables
    complete =True
    err_msg = '\nThe following observables require a iatom specifiyer:'
    for o in args['io.observables']:
        if o in require_iatom and args['io.iatom'] is None:
            err_msg += '\n\t* "{}"'.format(o)
            complete = False
    if not complete:
        msg += err_msg

    if msg:
        msg = 'Incomplete/invalid input!' + msg
        if verbose:
            print(msg)
        raise ValueError(msg)
