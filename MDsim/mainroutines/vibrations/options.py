# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import copy

from MDsim.tools.options import Option
from MDsim.tools.options import create_options_dict
from MDsim.tools.options import create_options_set
from MDsim.tools.options import generic_options_dict
from MDsim.tools.options import generic_options_set


_options = [
            # the system variables
            Option(name='Ndof',
                   fmt='int',
                   category='system',
                   val=2,
                   info='Degrees of freedom in the simulation.'),

            Option(name='positions_eq',
                   category='system',
                   fmt='float_array',
                   val=None,
                   info='Equilibrium position of the system (in Angstrom), defaults to zeros.'),

            Option(name='species',
                   category='system',
                   fmt='str_list',
                   val=None,
                   info='Chemical symbols of the species or other identifyer of the vibrating particle.'),

            Option(name='masses',
                   category='system',
                   fmt='float_array',
                   val=None,
                   info='The masses of the vibrating species in AMU.'),

            Option(name='temperature',
                   fmt='float',
                   category='system',
                   val=0.,
                   info='System temperature in a Langevin-like description.'),

            Option(name='init_quanta',
                   category='system',
                   fmt='float_array',
                   val=None,
                   info='Initial excitation for each vibrational mode.'
                   ),

            Option(name='zpe',
                   category='system',
                   fmt='bool',
                   val=False,
                   info='Add zero point vibrational energy to *each* mode.'
                   ),

            Option(name='init_velocities_sign',
                   category='system',
                   fmt='str',
                   val='+',
                   choices=['+', '-', 'random'],
                   info='The direction of the velocities in normalmode space.'
                   ),
            # potential variables
            Option(name='type',
                   fmt='str',
                   category='potential',
                   val=None,
                   choices=['interpolated', 'harmonic', 'diagonal_anharmonic'],
                   info='Method to represent the potential energy surface.'),

            Option(name='bounds_check',
                   fmt='bool',
                   category='potential',
                   val=True,
                   info='Perform bounds checks for the interpolation function. Rather costly but failsafe.'),

            Option(name='coordinates',
                   category='potential',
                   fmt='str',
                   val=None,
                   choices=['atomic', 'generic-dof', 'diatomic-perp'],
                   info='Coordinates in which the potential is internally handled in case of interpolation.'),

            Option(name='forces_method',
                   category='potential',
                   fmt='str',
                   val='analytical',
                   choices=['analytical', 'numerical'],
                   info='How to evaluate forces on the interpolated potential.'),

            Option(name='forces_stepwidth',
                   category='potential',
                   fmt='float',
                   val=0.001,
                   info='Central differences stepwidth (in Angstrom) in case of numerical forces.'),

            Option(name='normalize',
                   fmt='bool',
                   category='potential',
                   val=True,
                   info='Normalize the energies for the potential such that Emin = 0 eV.'),

            Option(name='potfile',
                   fmt='str',
                   category='potential',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant energies in eV.'),

            Option(name='potfile_usecols',
                   fmt='int_list',
                   category='potential',
                   val=None,
                   info='Columns to be used from the potfile. First and second number are x and y, last number is E.'),

            Option(name='normalmodes_delta',
                   fmt='float',
                   category='potential',
                   val=0.001,
                   info='Stepwidth for the numerical evaluation of the Hessian matrix via central differences.'),

            # the friction-related variables
            Option(name='type',
                   fmt='str',
                   category='friction',
                   val=None,
                   choices=['interpolated', 'constant', 'tensorial'],
                   info='Type of (electronic) friction to be employed.'),

            Option(name='values',
                   fmt='float_array',
                   category='friction',
                   val=None,
                   info='Value of the atomic constant (electronic) friction in AMU per fs'),

            Option(name='coordinates',
                   category='friction',
                   fmt='str',
                   val=None,
                   choices=['atomic', 'generic-dof', 'diatomic-perp'],
                   info='Coordinates in which the friction coefficients are internally handled in case of interpolation.'),

            Option(name='etafiles',
                   fmt='str_list',
                   category='friction',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant friction coefficients in AMU per fs.'),

            Option(name='etafiles_usecols',
                   fmt='int_list',
                   category='friction',
                   val=None,
                   info='Columns to be used from the etafile. First and second number are x and y, last number is eta.'),

            Option(name='bounds_check',
                   fmt='bool',
                   category='friction',
                   val=True,
                   info='Perform bounds checks for the interpolation function. Rather costly but failsafe.'),

            Option(name='tensor',
                   fmt='float_array',
                   category='friction',
                   val=None,
                   case_sensitive=True,
                   info='The mass-weighted friction tensor in fs^-1.'),

            Option(name='tensor_file',
                   fmt='str',
                   category='friction',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the mass-weighted friction tensor in fs^-1.'),

            ]


options_dict = copy.deepcopy(generic_options_dict)
_options_dict = create_options_dict(_options)

for category in _options_dict.keys():
    if category not in options_dict:
        options_dict[category] = _options_dict[category]
    else:
        options_dict[category].update(_options_dict[category])

options_set = generic_options_set
options_set.update(create_options_set(_options))

def get_options_dict():
    return copy.deepcopy(options_dict)
