# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import sys
import re

import numpy as np
import time
import copy

from MDsim import units
from MDsim import __version__

from MDsim.prettyprint import format_time
from MDsim.prettyprint import get_date
from MDsim.prettyprint import update_progress

from MDsim.tools.options import update_options_values
from MDsim.tools.options import create_args_dict
from MDsim.tools.options import check_args_dict_unknown
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_filehandler
from MDsim.tools.createobjects import create_memoryhandler
from MDsim.tools.createobjects import create_postprocessingobservable


from MDsim.observables.implemented import require_delta_K

from MDsim.prettyprint.stdouthandler import StdoutHandler

from MDsim.mainroutines.diffusion.options import options_set
from MDsim.mainroutines.diffusion.options import get_options_dict
from MDsim.mainroutines.diffusion.options import check_options_dict_unknown
from MDsim.mainroutines.diffusion.sanity import check_sanity

logo=r"""
+------------------------------------------------------------------------------+
|                     ____              __ ____  _  __  __                     |
|                    / ___| _   _ _ __ / _|  _ \(_)/ _|/ _|                    |
|                    \___ \| | | | '__| |_| | | | | |_| |_                     |
|                     ___) | |_| | |  |  _| |_| | |  _|  _|                    |
|                    |____/ \__,_|_|  |_| |____/|_|_| |_|                      |
|                                                                              |
|                             Simon P. Rittmeyer                               |
|                         simon.rittmeyer(at)tum.de                            |
+------------------------------------------------------------------------------+
| Part of the MDsim package {0:<51}|
| Contains contributions by Patrick Guetlein                                   |
+------------------------------------------------------------------------------+
"""[1:-1:].format('(version {})'.format(__version__))



def main(infile=None, py_args={}, cmd_args={}, verbose=True, progressbar=True):
    """
    Diffusion main routine.

    We have the following order in case of conflicting arguments

        py_args > cmd_args > infile

    Parameters
    ----------
    infile : str, optional (default=None)
        Infile to be parsed.

    py_args : dictionary, optional (default = {})
        Options additionally passed on a python level.

    cmd_args : dictionary, optional (default = {})
        Options passed from the command line.

    verbose : boolena, optional (default = True)
        Print information to stdout. If False, then simply stdout is directed
        to /dev/null ;)

    progressbar : bool, optional (default=True)
        Show a progress bar that updates dynamically. Do *not* use this option
        if you pipe to a file.
    """

    # this is the dictionary in which we store the timings
    tmp = time.time()
    _start = tmp

    times = {'start': tmp}

    if not verbose:
        devnull = open(os.devnull, 'w')
        _stdout = sys.stdout
        sys.stdout = devnull

    section_template = StdoutHandler._section_template

    print(logo[:-1])
    print(section_template.format('Program started : {}'.format(get_date())))

    print(section_template.format('INITIALIZATION'))


    # ----------------------------------
    # READING INFILE AND PARSING OPTIONS
    # ----------------------------------

    options_dict = get_options_dict()

    if infile:
        from MDsim.tools.parser import parse_inputfile
        options_dict = parse_inputfile(infile, options_dict)
        if not check_options_dict_unknown(options_dict):
            raise ValueError
    else:
        print('No input file given')

    if cmd_args:
        print('Updating options with additional command line arguments')
        if check_args_dict_unknown(cmd_args, options_set):
            options_dict = update_options_values(cmd_args, options_dict)
        else:
            raise ValueError
    else:
        print('No additional command line arguments passed')

    if py_args:
        print('Updating options with additional python-level input arguments')
        if check_args_dict_unknown(cmd_args, options_set):
            options_dict = update_options_values(py_args, options_dict)
        else:
            raise ValueError
    else:
        print('No additional python-level arguments passed')

    check_sanity(options_dict, verbose=verbose)

    # ok, now we can change things
    args = create_args_dict(options_dict)

    # quick n dirty
    if args['io.observables'] is None:
        args['io.observables'] = []


    Natoms=args['system.natoms']
    Ndim=args['system.ndim']

    mode=args['propagation.mode']
    Nruns=args['propagation.nruns']

    # for numbering the individual runs in accumulation mode
    Ndigits=int(np.ceil(np.log10(Nruns+1)))
    run_template = '__run_{{:0{:d}d}}'.format(Ndigits)
    Ncount_runs = 1


    # this will become our dictionary for the postproc observables
    postproc_obs=None
    accumulation_verbose=verbose
    # ---------------------------------------------------------
    # THE LOOP OVER SEVERAL RUNS (in case of accumulation mode)
    # ---------------------------------------------------------

    while True:
        accumulation_verbose = (Ncount_runs == 1)
        if mode == 'accumulation':
            print(section_template.format('ACCUMULATION MODE (run {} / {})'.format(Ncount_runs, Nruns)))

        # get a random seed
        if args['misc.random_seed'] is None:
            from MDsim.tools import randomize_seed
            random_seed = randomize_seed()
        else:
            random_seed = args['misc.random_seed']

        # ------------------------
        # SETTING UP THE POTENTIAL
        # ------------------------

        if args['potential.type'] == 'interpolated':
            if args['potential.interpolation'] == 'fourier':
                if args['potential.basis'] == 'minimal':
                    from MDsim.tools.createobjects import create_minimumbasisfourierpotential
                    pes = create_minimumbasisfourierpotential(a=args['system.surface_lattice_constant'],
                                                              datafile=args['potential.potfile'],
                                                              forces_method='analytical',
                                                              surface=args['system.surface_facet'],
                                                              convert=True,
                                                              normalize=args['potential.normalize'],
                                                              verbose=accumulation_verbose)
                elif args['potential.basis'] == 'top-hollow':
                    from MDsim.tools.createobjects import create_tophollowfourierpotential
                    pes = create_tophollowfourierpotential(a=args['system.surface_lattice_constant'],
                                                           datafile=args['potential.potfile'],
                                                           forces_method='analytical',
                                                           surface=args['system.surface_facet'],
                                                           convert=True,
                                                           normalize=args['potential.normalize'],
                                                           verbose=accumulation_verbose)
                elif args['potential.basis'] == 'custom':
                    from MDsim.tools.createobjects import create_fourierpotential
                    pes = create_fourierpotential(a=args['system.surface_lattice_constant'],
                                                  real_order=args['potential.real_order'],
                                                  imag_order=args['potential.imag_order'],
                                                  datafile=args['potential.potfile'],
                                                  constant=args['potential.constant'],
                                                  usecols=args['potential.potfile_usecols'],
                                                  forces_method='analytical',
                                                  surface=args['system.surface_facet'],
                                                  convert=True,
                                                  normalize=args['potential.normalize'],
                                                  verbose=accumulation_verbose)


            elif args['potential.interpolation'] == 'cambridge':
                from MDsim.tools.createobjects import create_cambridgepotential
                pes = create_cambridgepotential(a=args['system.surface_lattice_constant'],
                                                datafile=args['potential.potfile'],
                                                forces_method='analytical',
                                                surface=args['system.surface_facet'],
                                                convert=True,
                                                verbose=accumulation_verbose)



            elif args['potential.interpolation'] == 'splines':
                from MDsim.tools.createobjects import create_bivariatepotential
                pes = create_bivariatepotential(datafile=args['potential.potfile'],
                                                periodic=True,
                                                coordinates='atomic',
                                                forces_method='analytical',
                                                usecols=args['potential.potfile_usecols'],
                                                convert=True,
                                                normalize=args['potential.normalize'],
                                                verbose=accumulation_verbose)
        else:
            pes = None

        try:
            pes._change_support(args['misc.support'])
        except AttributeError:
            pass

        # -----------------------
        # SETTING UP THE FRICTION
        # -----------------------

        if args['friction.type'] == 'interpolated':
            if args['friction.interpolation'] == 'fourier':
                if args['friction.basis'] == 'minimal':
                    from MDsim.tools.createobjects import create_minimumbasisfourierfrictioncoefficient
                    eta = create_minimumbasisfourierfrictioncoefficient(
                                a=args['system.surface_lattice_constant'],
                                datafile=args['friction.etafile'],
                                surface=args['system.surface_facet'],
                                convert=True,
                                verbose=accumulation_verbose)
                elif args['friction.basis'] == 'custom':
                    from MDsim.tools.createobjects import create_fourierfrictioncoefficient
                    eta = create_fourierfrictioncoefficient(
                                a=args['system.surface_lattice_constant'],
                                real_order=args['friction.real_order'],
                                imag_order=args['friction.imag_order'],
                                datafile=args['friction.etafile'],
                                constant=args['friction.constant'],
                                usecols=args['friction.etafile_usecols'],
                                surface=args['system.surface_facet'],
                                convert=True,
                                verbose=accumulation_verbose)

                eta._change_support(args['misc.support'])

            elif args['friction.interpolation'] == 'splines':
                from MDsim.tools.createobjects import create_bivariatefrictioncoefficient
                eta = create_bivariatefrictioncoefficient(
                            datafile=args['friction.etafile'],
                            periodic=True,
                            coordinates='atomic',
                            usecols=args['friction.etafile_usecols'],
                            convert=True,
                            verbose=accumulation_verbose)

            from MDsim.friction.electronicfriction.ldfa.matrix.homogeneousatoms import HomogeneousAtomsFrictionMatrix
            friction_matrix = HomogeneousAtomsFrictionMatrix(eta)

        elif args['friction.type'] == 'constant':
            from MDsim.friction.constantfriction import ConstantFriction
            etas = np.ones((Natoms, Ndim), dtype=np.float64) *args['friction.value']

            friction_matrix = ConstantFriction(etas, convert = True)


        else:
            friction_matrix = None


        # ---------------------
        # SETTING UP THE SYSTEM
        # ---------------------

        if (args['system.coverage'] > 0. and Natoms > 1):
            # in this situation a kohn lau system does make sense
            from MDsim.tools.createobjects import create_surfaceperiodicsystem

            system = create_surfaceperiodicsystem(
                           Natoms=Natoms,
                           Ndim=Ndim,
                           species=args['system.species'],
                           coverage=args['system.coverage'],
                           saturation_coverage=args['system.saturation_coverage'],
                           force_cutoff=args['pairwise.force_cutoff'],
                           list_cutoff=args['pairwise.list_cutoff'],
                           p0=args['system.dipole_moment_zero_coverage'],
                           polarizability=args['system.polarizability'],
                           surface_lattice_constant=args['system.surface_lattice_constant'],
                           surface_facet=args['system.surface_facet'],
                           initial_positions=args['system.initial_positions'],
                           pes=pes,
                           friction_matrix=friction_matrix,
                           temperature=args['system.temperature'],
                           masses=args['system.mass'],
                           random_seed=random_seed,
                           verbose=accumulation_verbose)


            if (args['system.dipole_moment_zero_coverage'] is not None and args['system.dipole_moment_zero_coverage'] > 0.):
                system._change_support(args['misc.support'])

                if args['pairwise.no_neighborlists']:
                    system.deactivate_neighbor_lists()


                if args['misc.show_system']:
                    system.show_neighbor_list()

            # supercell for the kvectors
            supercell = system.get_cell()

        else:
            # no pairwise interactions required
            # in this case we neither need periodicity nor a super cell nor any
            # pairwise interactions.
            from MDsim.systems.system import System

            if accumulation_verbose:
                print('Creating Generic System without pairwise interactions')
                print('\tCoverage and dipole moment equals zero')

            # distribute the positions over a unit cell
            if args['system.surface_facet'].lower() in ['fcc111', 'fcc100']:
                from MDsim.coordinates.diffusion import create_real_space_lattice
                from MDsim.coordinates.diffusion import create_sites
                primitive_cell = create_real_space_lattice(surface_facet=args['system.surface_facet'].lower(),
                                                           a=args['system.surface_lattice_constant'])
                sites = create_sites(surface_facet=args['system.surface_facet'].lower(),
                                     a=args['system.surface_lattice_constant'])

            else:
                msg = 'Surface facet "{}" not implemented'.format(args['surface_facet'])
                raise NotImplementedError(msg)


            if args['system.initial_positions'] == 'random':
                if accumulation_verbose:
                    print('\tPositions are randomly distributed across primitive unit cell')
                np.random.seed(random_seed)
                positions = np.einsum('ij,kj->ij', np.random.rand(Natoms, Ndim), primitive_cell)
            elif args['system.initial_positions'] == 'origin':
                if accumulation_verbose:
                    print('\tPositions are centered at origin')
                positions = np.zeros((Natoms, Ndim))
            elif args['system.initial_positions'] in sites.keys():
                if accumulation_verbose:
                    print('\tPositions are centered at {} site'.format(args['system.initial_positions']))
                positions = np.ones((Natoms, Ndim)) * sites[args['system.initial_positions']][None,:]


            system = System(positions=positions,
                            velocities=None,
                            masses=args['system.mass'],
                            species=args['system.species'],
                            potential=pes,
                            friction=friction_matrix,
                            temperature=args['system.temperature'],
                            convert=True,
                            _verbose=False)

            supercell=None
        # now we have everything for the simulation

        # -------------------------
        # SETTING UP THE INTEGRATOR
        # -------------------------

        integrator = create_integrator(method=args['propagation.integrator'],
                                       dt=args['propagation.timestep'],
                                       seed=random_seed,
                                       convert=True,
                                       verbose=accumulation_verbose)

        # --------------------------------------------
        # PRE-EVALUATING THE NUMBER OF STEPS NECESSARY
        # --------------------------------------------

        # comparing against au saves us Nsteps multiplications of the system time
        t_end = args['propagation.simulation_time'] * units.FS_TO_AU
        iostep = args['io.iostep']

        tot_steps = np.ceil(args['propagation.simulation_time'] /  args['propagation.timestep'])
        tot_iosteps = int(np.floor(tot_steps / iostep) + 1)


        # ----------------------------
        # PRE-EQUILIBRATING THE SYSTEM
        # ----------------------------

        # equilibration before attaching handlers speeds up things significantly
        eq_time = args['propagation.equilibration_time']
        eq_steps = int(np.ceil(eq_time /  args['propagation.timestep']))

        if eq_steps > 0:
            print('Equilibrating system without I/O ({} fs = {} steps)'.format(eq_time, eq_steps))
            nsteps = 0

            if progressbar:
                updatestep = int(np.ceil(eq_steps*0.001))
                print()

            while nsteps < eq_steps:
                integrator.propagate(system)
                nsteps += 1
                if progressbar:
                    if nsteps%updatestep == 0:
                        update_progress(float(nsteps)/ eq_steps)

            # reset system time
            system.set_time(0.)

        if progressbar:
            print()


        # ----------------------------------
        # SETTING UP THE ON-THE-FLY HANDLERS
        # ----------------------------------

        # create the io-related things
        handlers = {}
        if args['io.debug']:
            debug_args = args
        else:
            debug_args = None

        # check if we need postprocessing at all
        do_postprocessing = args['postprocessing.observables'] is not None

        if do_postprocessing:
            # check first which kind of scattering we need
            coherent_scattering = ('scattering_amplitudes' in args['io.observables'] or
                                     np.any(['incoherent' not in a for a in args['postprocessing.observables']]))
            incoherent_scattering = ('incoherent_scattering_amplitudes' in args['io.observables'] or
                                     np.any(['incoherent' in a for a in args['postprocessing.observables']]))
        else:
            # dummies
            coherent_scattering = False
            incoherent_scattering = False
        # in case we need the scattering amplitudes build up the vectors
        if (do_postprocessing or coherent_scattering or incoherent_scattering):
            from MDsim.tools.createobjects import create_kvectors
            dKs = {}
            Nkpts = args['io.nkpts']
            for direction in args['io.delta_k_directions']:
                dK = create_kvectors(surface_facet=args['system.surface_facet'],
                                     surface_direction=direction,
                                     Nkpts=Nkpts,
                                     max_amplitude=args['io.delta_k_max_amplitude'],
                                     # we will convert within the observable
                                     convert=False,
                                     supercell=supercell,
                                     verbose=accumulation_verbose)
                dKs[direction] = dK
        else:
            dKs = {}

        # this concerns the individual postproc handlers (will be set up only afterwards)
        if do_postprocessing:
            postproc_obs_individual_set = set([a.replace('_individual','') for a in args['postprocessing.observables'] if a.endswith('_individual')])
        else:
            postproc_obs_individual_set = set()

        # evaluating alternative friction coefficients along the way
        if ('alternative_average_eta' in args['io.observables']
                or 'alternative_eta' in args['io.observables']):

            if args['io.alternative_eta_interpolation'] == 'fourier':
                if args['io.alternative_eta_basis'] == 'minimal':
                    from MDsim.tools.createobjects import create_minimumbasisfourierfrictioncoefficient
                    eta = create_minimumbasisfourierfrictioncoefficient(
                                a=args['system.surface_lattice_constant'],
                                datafile=args['io.alternative_etafile'],
                                surface=args['system.surface_facet'],
                                convert=True,
                                verbose=accumulation_verbose)

                elif args['io.alternative_eta_basis'] == 'custom':
                    from MDsim.tools.createobjects import create_fourierfrictioncoefficient
                    eta = create_fourierfrictioncoefficient(
                                a=args['system.surface_lattice_constant'],
                                real_order=args['io.alternative_eta_real_order'],
                                imag_order=args['io.alternative_eta_imag_order'],
                                datafile=args['io.alternative_etafile'],
                                constant=args['io.alternative_eta_constant'],
                                usecols=args['io.alternative_etafile_usecols'],
                                surface=args['system.surface_facet'],
                                convert=True,
                                verbose=accumulation_verbose)

                eta._change_support(args['misc.support'])

            elif args['io.alternative_eta_interpolation'] == 'splines':
                from MDsim.tools.createobjects import create_bivariatefrictioncoefficient
                eta = create_bivariatefrictioncoefficient(
                            datafile=args['io.alternative_etafile'],
                            periodic=True,
                            coordinates='atomic',
                            usecols=args['io.alternative_etafile_usecols'],
                            convert=True,
                            verbose=accumulation_verbose)

            from MDsim.friction.electronicfriction.ldfa.matrix.homogeneousatoms import HomogeneousAtomsFrictionMatrix
            alternative_friction_matrix = HomogeneousAtomsFrictionMatrix(eta)

        else:
            alternative_friction_matrix = None

        # dynamical seed in accumulation mode
        if args['propagation.mode'] == 'accumulation':
            seed = args['io.seed'] + run_template.format(Ncount_runs)
        else:
            seed = args['io.seed']

        for obs in args['io.observables']:
            if obs in require_delta_K:
                for dK_name, dK in dKs.items():
                    handler = create_filehandler(system_inst=system,
                                                 integrator_inst=integrator,
                                                 observable=obs,
                                                 iatom=args['io.iatom'],
                                                 fileformat=args['io.fileformat'],
                                                 # avoid sqaure brackets in file names
                                                 seed=seed+'__dK__{}'.format(re.sub(r'[\[\]]', '', dK_name)),
                                                 delta_K=dK,
                                                 convert=True,
                                                 outfolder=args['io.outpath'],
                                                 compress=args['io.compress'],
                                                 datatype=args['io.datatype'],
                                                 verbose=verbose,
                                                 Nsteps=tot_iosteps,
                                                 args=debug_args)

                    # init the handler with the information from t=0
                    handlers[obs+'__{}'.format(dK_name)] = handler

            else:
                handler = create_filehandler(system_inst=system,
                                             integrator_inst=integrator,
                                             observable=obs,
                                             iatom=args['io.iatom'],
                                             fileformat=args['io.fileformat'],
                                             seed=seed,
                                             outfolder=args['io.outpath'],
                                             compress=args['io.compress'],
                                             datatype=args['io.datatype'],
                                             verbose=verbose,
                                             Nsteps=tot_iosteps,
                                             alternative_eta=alternative_friction_matrix,
                                             args=debug_args)

                handlers[obs] = handler

        # in case we have no explicit scattering amplitudes output, we store
        # them using a memory handler (and hope that memory will be alright ;))
        if do_postprocessing:
            amplitudes_handlers = []
            if (coherent_scattering and 'scattering_amplitudes' not in args['io.observables']):
                amplitudes_handlers.append('scattering_amplitudes')

            if (incoherent_scattering and 'incoherent_scattering_amplitudes' not in args['io.observables']):
                amplitudes_handlers.append('incoherent_scattering_amplitudes')

            for handler_str in amplitudes_handlers:
                for dK_name, dK in dKs.items():
                    handler = create_memoryhandler(system_inst=system,
                                                   integrator_inst=integrator,
                                                   observable=handler_str,
                                                   delta_K=dK,
                                                   datatype=args['io.datatype'],
                                                   convert=True,
                                                   verbose=False,
                                                   Nsteps=tot_iosteps,
                                                   args=debug_args)
                    handlers['{}__{}'.format(handler_str, dK_name)] = handler


        # stdouthandler
        stdouthandler = StdoutHandler(system=system,
                                      integrator=integrator)


        if accumulation_verbose:
            print()
            print(stdouthandler.get_system_info(system))
            print(stdouthandler.get_integrator_info(integrator))

        try:
            times['init'] += time.time() - tmp
        except KeyError:
            times['init'] = time.time() - tmp

        tmp = time.time()

        if args['io.debug']:
            if accumulation_verbose:
                print(stdouthandler.get_options(args))


        # --------------------------
        # THE ACTUAL SIMULATION LOOP
        # --------------------------
        print(section_template.format('SIMULATION'))

        # now that we have the handlers we can start with the actual propagation
        print('Simulating {} fs with dt = {} fs, ie., {} steps in total\n\t(--> {} io steps)'.format(
                args['propagation.simulation_time'],
                args['propagation.timestep'],
                int(tot_steps),
                int(tot_iosteps)))

        nsteps = 0

        if progressbar:
            updatestep = int(np.ceil(tot_steps*0.001))
            print()

        # walltime checking
        _start_loop = float(time.time())
        if args['misc.walltime'] is not None:
            _exit_loop = 0.95*args['misc.walltime']
        else:
            _exit_loop=False

        _interrupt = False

        # initialize the handlers
        for handler in handlers.values():
            handler.push()

        # aliasing the propagate function in order to check for effective
        # energy logging
        if 'effective_energy' in args['io.observables']:
            propagate = lambda system: integrator.propagate(system, _log_effective_energy=True)
        else:
            propagate = lambda system: integrator.propagate(system)

        while nsteps < tot_steps:
            try:
                propagate(system)
                nsteps += 1

                if nsteps%iostep == 0:
                    for handler in handlers.values():
                        handler.push()
                    if _exit_loop:
                        if (time.time() - _start) > _exit_loop:
                            print('Breaking simulation loop due to insufficient walltime')
                            print('\tsimulated steps : {}'.format(nsteps))
                            print('\tsimulated time  : {} fs'.format(int(system.get_time())))
                            break
                if progressbar:
                    if nsteps%updatestep == 0:
                        update_progress(nsteps / tot_steps)
            except KeyboardInterrupt:
                print('Caught KeyboardInterrupt... Exiting')
                _interrupt = True
                break

        if not _interrupt:
            if progressbar:
                print()
            print('Simulation finished ({})'.format(format_time(time.time() - tmp)))

        try:
            times['simulation'] += time.time() - tmp
        except KeyError:
            times['simulation'] = time.time() - tmp

        tmp = time.time()

        if (not args['pairwise.no_neighborlists']
                and args['system.coverage'] > 0.
                and args['system.dipole_moment_zero_coverage'] > 0
                and Natoms > 1):
            print()
            print('--> Updated neighbor lists every {:.2f} steps'.format(float(tot_steps + eq_steps) / system.get_neighbor_list_updates()))

        print(section_template.format('FINALIZING'))
        print('Closing handlers:')
        for h, handler in handlers.items():
            print('\t{}'.format(h))
            handler.close()

        try:
            times['finalizing'] += time.time() - tmp
        except KeyError:
            times['finalizing'] = time.time() - tmp

        # get out of the accumulation loop without postprocessing
        if _interrupt:
            break

        tmp = time.time()

        # ----------------------------------------------------------------
        # FIRST PART OF POSTPROCESSING  -- evaluate DSFs/ISFs at each run
        # ----------------------------------------------------------------

        # here we go with the post processing
        if do_postprocessing:
            print(section_template.format('POSTPROCESSING'))

            scattering_data = {'{}'.format(scat) : {} for scat in ['coherent', 'incoherent']}

            for scat_dict in scattering_data.values():
                scat_dict['times'] = {}
                scat_dict['amplitudes'] = {}
                scat_dict['files'] = {}

            # some crunches to do the looping
            scattering_data['coherent']['active'] = coherent_scattering
            scattering_data['incoherent']['active'] = incoherent_scattering

            # now we loop over coherent and incoherent scattering
            for scat, scat_dict in scattering_data.items():
                # skip if not active
                if not scat_dict['active']:
                    continue

                # identifiers
                if scat == 'coherent':
                    scat_str = ''
                else:
                    scat_str = 'INCOHERENT_'

                # do the scattering amplitudes handling / either from file or from memory
                if '{}scattering_amplitudes'.format(scat_str.lower()) in args['io.observables']:
                    for dK_name, dK in dKs.items():
                        # in this case we have a file!
                        # avoid the square brackets in file names
                        pattern = seed + r'__dK__{}__{}SCATTERING_AMPLITUDES\.(nc|dat|dat\.gz)'.format(re.sub(r'[\[\]]', '', dK_name),
                                                                                                       scat_str.upper())
                        for f in os.listdir(args['io.outpath']):
                            if re.search(pattern, f):
                                scat_dict['files'][dK_name] = os.path.join(args['io.outpath'], f)
                                break
                        print('Found {} scattering amplitudes file for dK direction "{}":'.format(scat, dK_name))
                        print('\t{}'.format(scat_dict['files'][dK_name]))
                        scat_dict['amplitudes'][dK_name] = None
                        scat_dict['times'][dK_name] = None
                else:
                    for dK_name, dK in dKs.items():
                        # in this case we had memory handling
                        handler = handlers['{}scattering_amplitudes__{}'.format(scat_str.lower(), dK_name)]

                        # you can only do this with a memoryhandler instance
                        scat_dict['files'][dK_name] = None

                        _data = handler.get_data()
                        scat_dict['times'][dK_name] = _data[:,0]

                        # ok, here we need to make a distinction between
                        # coherent and incoherent amplitudes due to the
                        # different dimensionality
                        if scat == 'coherent':
                            scat_dict['amplitudes'][dK_name] = _data[:,1::2] + _data[:,2::2]*1j
                        else:
                            _data = _data[0:,1::].reshape(-1, Natoms, Nkpts*2)
                            scat_dict['amplitudes'][dK_name] = _data[:,:,::2] + _data[:,:,1::2]*1j

                        print('Using in-memory stored {} scattering amplitudes for dK direction "{}"'.format(scat, dK_name))


            try:
                # -----------------------------------------------------
                # SETTING UP THE INDIVIDUAL POSTPROCESSING observables
                # that is those that are dumped for every run!
                # -----------------------------------------------------
                if postproc_obs_individual_set:
                    print('Evaluating and dumping individual run information to file for observable:')
                    for obs_name in postproc_obs_individual_set:
                        # TODO: switch for postproc observables without dK requirement
                        if 'incoherent' in obs_name:
                            scat_dict = scattering_data['incoherent']
                        else:
                            scat_dict = scattering_data['coherent']

                        for dK_name, dK in dKs.items():
                            # the observable
                            print('\t{} (dK direction "{}")'.format(obs_name, dK_name))
                            obs = create_postprocessingobservable(system_inst=system,
                                                                  observable=obs_name,
                                                                  verbose=False,
                                                                  scattering_amplitudes_file=scat_dict['files'][dK_name],
                                                                  times=scat_dict['times'][dK_name],
                                                                  scattering_amplitudes=scat_dict['amplitudes'][dK_name],
                                                                  optimize_length=args['postprocessing.fft_optimize_length'],
                                                                  autocorrelation=args['postprocessing.fft_autocorrelation'],
                                                                  delta_K=dK,
                                                                  normalize=False,
                                                                  convert=True)

                            handler = create_filehandler(system_inst=system,
                                                         integrator_inst=integrator,
                                                         observable=obs,
                                                         fileformat=args['io.fileformat'],
                                                         # avoid square brackets in file names
                                                         seed=seed + '__dK__{}'.format(re.sub(r'[\[\]]', '', dK_name)),
                                                         outfolder=args['io.outpath'],
                                                         compress=args['io.compress'],
                                                         datatype=args['io.datatype'],
                                                         delta_K=dK,
                                                         verbose=False,
                                                         args=debug_args)

                            handler.write()
                            handler.close()
                            # evaluating data

                # ----------------------------------------------------------------
                # SETTING UP THE ACCUMULATED POSTPROCESSING OBSERVABLES
                # that is those that are only dumped at the very end (accumulated)
                # ----------------------------------------------------------------

                # create the dictionary with the respective observables
                if postproc_obs is not None:
                    # we already have the observables
                    pass

                else:
                    postproc_obs = {}
                    postproc_obs_set = set([a for a in args['postprocessing.observables'] if not a.endswith('individual')])

                    # # no longer -- from now on we go for the direct ISFs
                    # # Here comes the swith finally ;)
                    # if args['postprocessing.isf_accumulated_quantity'] in ['both', 'dsf']:
                        # if ('isf' in postproc_obs_set and not 'dsf' in postproc_obs_set):
                            # # we need the dsf in any case...
                            # postproc_obs_set.add('dsf')

                        # # we remove the ISF here, as we only use the accumulated dsf from now on
                        # if args['postprocessing.isf_accumulated_quantity'] == 'dsf':
                            # try:
                                # postproc_obs_set.remove('isf')
                            # except KeyError:
                                # pass

                    # TODO: add switch for postproc obs that do not require dK values
                    for obs in postproc_obs_set:
                        # make the distinction between coherent and incoherent
                        if 'incoherent' in obs:
                            scat_dict = scattering_data['incoherent']
                        else:
                            scat_dict = scattering_data['coherent']

                        for dK_name, dK in dKs.items():
                            obs_id = obs + '__{}'.format(dK_name)

                            postproc_obs[obs_id] = create_postprocessingobservable(
                                                    system_inst=system,
                                                    observable=obs,
                                                    verbose=False,
                                                    scattering_amplitudes_file=scat_dict['files'][dK_name],
                                                    times=scat_dict['times'][dK_name],
                                                    scattering_amplitudes=scat_dict['amplitudes'][dK_name],
                                                    optimize_length=args['postprocessing.fft_optimize_length'],
                                                    autocorrelation=args['postprocessing.fft_autocorrelation'],
                                                    delta_K=dK,
                                                    convert=True)


                # -------------------------------
                # ADDING RUN IN ACCUMULATION MODE
                # -------------------------------

                if args['propagation.mode'] == 'accumulation':
                    print('Adding run to accumulated observables:')
                    for obs_name, obs in postproc_obs.items():
                        # TODO: add switch for observables that do not require dK values
                        # getting back the dK name
                        if 'incoherent' in obs_name:
                            scat_dict = scattering_data['incoherent']
                        else:
                            scat_dict = scattering_data['coherent']

                        pattern = r'(.*)__(.*)'
                        match_obj = re.search(pattern, obs_name)

                        if match_obj:
                            dK_name = match_obj.groups()[1]

                            print('\t{} (dK direction "{}")'.format(*match_obj.groups()))

                            obs.add(times=scat_dict['times'][dK_name],
                                    scattering_amplitudes=scat_dict['amplitudes'][dK_name],
                                    scattering_amplitudes_file=scat_dict['files'][dK_name])
                        else:
                            raise IOError("Something's gone wrong. Naming pattern of postproc observables is invalid")
                try:
                    times['postprocessing'] += time.time() - tmp
                except KeyError:
                    times['postprocessing'] = time.time() - tmp

                # -------------------------------------
                # END OF FIRST-PART POSTPROCESSING LOOP
                # -------------------------------------

            except KeyboardInterrupt:
                _interrupt = True
                break

        tmp = time.time()


        # ----------------------------------------------
        # CHECKING IF ACCUMULATION LOOP CAN BE ABANDONED
        # ----------------------------------------------

        # just in case a run has failed (remember we start with 1)
        if args['propagation.mode'] == 'accumulation':
            if args['postprocessing.observables'] is not None:
                    Ncount_runs = np.min([o.get_Naccumulated() for o in postproc_obs.values()])+1
            else:
                Ncount_runs += 1
            if Ncount_runs > Nruns:
                if postproc_obs is not None:
                    print(section_template.format('FINALIZING ACCUMULATION MODE'))
                break
        elif args['propagation.mode'] == 'single':
            # get out of the accumulation loop...
            break


    # ------------------------------------------------------------------------
    # SECOND PART OF POSTPROCESSING write the accumulated observables to files
    # ------------------------------------------------------------------------
    if postproc_obs is not None and not _interrupt:
        # now write the post processing stuff
        if args['propagation.mode'] == 'single':
            print(section_template.format('POSTPROCESSING'))

        # # evaluate ISF from DSF if asked for
        # if args['postprocessing.isf_accumulated_quantity'] in ['both', 'dsf']:
            # if ('isf' in args['postprocessing.observables']):

                # print('Evaluating ISF from (accumulated) DSFs')

                # for dK_name, dK in dKs.items():
                    # dsf_obs = postproc_obs['dsf__{}'.format(dK_name)]

                    # # do not sort, otherwise the ifft won't work as expected
                    # print('\tdK direction "{}"'.format(dK_name))
                    # data = postproc_obs['dsf__{}'.format(dK_name)].calc_isf()

                    # obs_id = 'isf__{}'.format(dK_name)

                    # postproc_obs[obs_id] = create_postprocessingobservable(
                                            # system_inst=system,
                                            # observable='isf',
                                            # verbose=False,
                                            # optimize_length=args['postprocessing.fft_optimize_length'],
                                            # autocorrelation=args['postprocessing.fft_autocorrelation'],
                                            # delta_K=dK,
                                            # convert=True)

                    # # crunches but quick and dirty
                    # Nacc = dsf_obs.get_Naccumulated()
                    # postproc_obs[obs_id].set_data(data*Nacc, Naccumulated=Nacc, accumulation_quantity='dsf')

        for obs_name, obs in postproc_obs.items():
            # if we do not want dsf output
            # split before the direction id
            if obs_name.split('__')[0] not in args['postprocessing.observables']:
                continue

            # TODO: add switch for observables that do not require dK values
            # getting back the dK name

            pattern = r'(.*)__(.*)'
            match_obj = re.search(pattern, obs_name)

            if match_obj:
                dK_name = match_obj.groups()[1]
                dK = dKs[dK_name]
                handler = create_filehandler(system_inst=system,
                                             integrator_inst=integrator,
                                             observable=obs,
                                             fileformat=args['io.fileformat'],
                                             # avoid square brackets infile names
                                             seed=args['io.seed']+'__dK__{}'.format(re.sub(r'[\[\]]', '', dK_name)),
                                             outfolder=args['io.outpath'],
                                             compress=args['io.compress'],
                                             datatype=args['io.datatype'],
                                             delta_K=dK,
                                             convert=True,
                                             verbose=verbose,
                                             args=debug_args)

                print('\tEvaluating and dumping information to file for "{}" (dK direction "{}")'.format(*match_obj.groups()))
                handler.write()
                handler.close()

        times['postprocessing'] += time.time() - tmp

        tmp = time.time()


    # -----------------
    # END OF SIMULATION
    # -----------------

    # ok, now we are done ;)
    times['runtime'] = time.time() - times['start']

    print(section_template.format('RUNTIME INFORMATION'))
    print('Initialization    : {}'.format(format_time(times['init'])))
    print('Time propagation  : {}'.format(format_time(times['simulation'])))
    print('Finalizing        : {}'.format(format_time(times['finalizing'])))
    if 'postprocessing' in times.keys():
        print('Post processing   : {}'.format(format_time(times['postprocessing'])))
    print('-----------------   -----------')
    print('Total Runtime     : {}'.format(format_time(times['runtime'])))

    print(section_template.format('Program ended : {} -- Have a nice day :)'.format(get_date())))

    # restore standard out
    if not verbose:
        sys.stdout = _stdout

    if _interrupt:
        raise KeyboardInterrupt
