# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os

from MDsim.tools.options import create_args_dict
from MDsim.observables import require_iatom


from MDsim.coordinates.diffusion.fcc111 import implemented_sites as implemented_sites_fcc111
from MDsim.coordinates.diffusion.fcc100 import implemented_sites as implemented_sites_fcc100


def check_sanity(options_dict, verbose=True):
    """
    Check if we have everything we need
    """

    args = create_args_dict(options_dict)

    # values that must not be None in any case
    required = ['system.species',
                'system.natoms',
                'system.surface_facet',
                'system.ndim',
                'system.surface_lattice_constant',
                'propagation.simulation_time',
                'propagation.timestep',
                ]

    complete = True

    if verbose:
        print('Checking for input completeness')

    msg = 'Incomplete input! The following arguments must not be None:'

    for req in required:
        if args[req] is None:
            msg += '\n\t* "{}"'.format(req)
            complete = False

    if not complete:
        if verbose:
            print(msg)
        raise ValueError(msg)

    if verbose:
        print('Checking for conditional requirements')

    msg = ''

    if args['propagation.simulation_time'] < 0:
        msg +='\nNegative simulation time'
    if args['propagation.timestep'] < 0:
        msg +='\nNegative time step'
    if args['io.iostep'] < 0:
        msg +='\nNegative I/O step'

    if args['system.dipole_moment_zero_coverage'] is None:
        args['system.dipole_moment_zero_coverage'] = 0.

    # checking for Kohn-Lau forces
    complete = True
    if args['system.coverage'] > 0.:
        err_msg = '\nSystem coverage > 0., ie., pairwise interactions but:'
        # if args['system.dipole_moment_zero_coverage'] is None:
            # err_msg += '\n\t* no zero coverage dipole moment given'
            # complete = False
        if args['system.polarizability'] is None:
            err_msg += '\n\t* no polarizability given'
            complete = False

        if args['pairwise.force_cutoff'] is None:
            err_msg += '\n\t* no force cutoff given'
            complete = False

        if not args['pairwise.no_neighborlists'] and args['pairwise.list_cutoff'] is None:
            err_msg += '\n\t* no neighbor list cutoff given'
            complete = False

        if not complete:
            msg += err_msg

    # check for the initial positions
    if args['system.initial_positions'] not in ['random', 'origin']:
        if ((args['system.surface_facet'] == 'fcc100' and args['system.initial_positions'] not in implemented_sites_fcc100)
            or (args['system.surface_facet'] =='fcc111' and args['system.initial_positions'] not in implemented_sites_fcc111)):
            msg += '\nInitial sites "{}" not implemented for surface facet "{}"'.format(args['system.initial_positions'],
                                                                                        args['system.surface_facet'])

    # patch : if we go for fcc(100), then only cambridge potential and constant friction
    complete=True
    if args['system.surface_facet'] == 'fcc100':
        err_msg += '\nSurface facet fcc(100) requires'
        if not args['potential.type'] == 'interpolated' and args['potential.interpolation'] == 'cambridge':
            err_msg += '\n\t* "cambridge" potential'
            complete = False
        if not args['friction.type'] == 'constant':
            err_msg += '\n\t* constant friction'
            complete = False

        if not complete:
            msg += err_msg

    # checking for the PES
    complete=True
    if args['potential.type'] == 'interpolated':
        if args['potential.interpolation'] == 'fourier':
            err_msg = '\nFourier interpolation for potential but:'
            if args['potential.potfile'] is None:
                err_msg += '\n\t* no potfile given'
                complete = False
            elif not os.path.isfile(args['potential.potfile']):
                raise IOError('Invalid potfile location')

            if args['potential.basis'] == 'custom':
                _msg = '\n\t* custom basis but no {} given'
                if args['potential.real_order'] is None:
                    err_msg += _msg.format('real expansion order')
                    complete = False
                if args['potential.imag_order'] is None:
                    err_msg += _msg.format('imaginay expansion order')
                    complete = False


            elif args['potential.basis'] == 'top-hollow':
                if not args['system.surface_facet'] == 'fcc111':
                    err_msg += '\n\t* "top-hollow" basis is available only for fcc111 surface facet'
                    complete = False

            if not complete:
                msg += err_msg

        elif args['potential.interpolation'] == 'cambridge':
            if not args['system.surface_facet'] == 'fcc100':
                msg += '"\ncambridge" interpolation is available only for fcc100 surface facet'

        elif args['potential.interpolation'] == 'splines':
            err_msg = '\nSpline interpolation for potential but:'
            if args['potential.potfile'] is None:
                err_msg += '\n\t* no potfile given'
                complete = False
            if not complete:
                msg += err_msg

    # checking for the friction
    complete=True
    if args['friction.type'] == 'interpolated':
        if args['friction.interpolation'] == 'fourier':
            err_msg = '\nFourier interpolation for friction but:'
            if args['friction.etafile'] is None:
                err_msg += '\n\t* no etafile given'
                complete = False
            elif not os.path.isfile(args['friction.etafile']):
                raise IOError('Invalid etafile location')
            if args['friction.basis'] == 'custom':
                _msg = '\n\t* custom basis but no {} given'
                if args['friction.real_order'] is None:
                    err_msg += _msg.format('real expansion order')
                    complete = False
                if args['friction.imag_order'] is None:
                    err_msg += _msg.format('imaginay expansion order')
                    complete = False
            if not complete:
                msg += err_msg

        elif args['friction.interpolation'] == 'splines':
            err_msg = '\nSpline interpolation for friction but:'
            if args['friction.potfile'] is None:
                err_msg += '\n\t* no potfile given'
                complete = False
            if not complete:
                msg += err_msg

    elif args['friction.type'] == 'constant':
        if args['friction.value'] is None:
            msg += '\nConstant friction chosen but no value given'

    # checking for the observables
    if (args['io.observables'] is None and
            args['postprocessing.observables'] is None):
        msg += '\nNeither on-the-fly nor post processing observables specified. Simulation is pointless!'

    if args['postprocessing.observables'] is None and args['propagation.mode'] == 'accumulation':
        msg += '\nAccumulation mode without post processing observables is pointless.'

    if args['io.observables'] is not None:
        complete=True
        if 'scattering_amplitudes' in args['io.observables']:
            err_msg = '\nObservable "scattering_amplitudes" chosen but'
            if args['io.delta_k_directions'] is None:
                err_msg += '\n\t* no dK directions given'
                complete = False
            if not complete:
                msg += err_msg

        complete =True
        err_msg = '\nThe following observables require a iatom specifiyer:'
        for o in args['io.observables']:
            if o in require_iatom and args['io.iatom'] is None:
                err_msg += '\n\t* "{}"'.format(o)
                complete = False
        if not complete:
            msg += err_msg

        if 'alternative_eta' in args['io.observables'] and arg['io.alternative_etafile'] is None:
            err_msg = '\nObservable "alternative_eta" chosen but'
            err_msg += '* no alternative etafile provided'
            complete = False

        if not complete:
            msg += err_msg
    complete=True
    if args['postprocessing.observables'] is not None:
        err_msg = '\nSurface correlation post-processing chosen but'
        if args['io.delta_k_directions'] is None:
            err_msg += '\n\t* no dK directions given'
            complete = False
        if not complete:
            msg += err_msg

    if args['io.observables'] is not None:
        if 'effective_energy' in args['io.observables']:
            if args['propagation.integrator'] != 'bussi parrinello':
                err_msg += '\nEffective energy can only be logged with Bussi-Parinello integrator'


# deprecated
#     # we need the scattering amplitudes for the ISFs
#     if args['postprocessing.observables'] is not None and 'scattering_amplitudes' not in args['io.observables']:
#         msg += '\nISF/DSF postprocessing requires observable "scattering_amplitudes"'

    # checking for the simulation mode
    if args['propagation.mode'] == 'accumulation' and args['propagation.nruns'] is None:
        msg += '\nChose accumulation mode, but no number of runs (Nruns) given.'

    if msg:
        msg = 'Incomplete input!' + msg
        if verbose:
            print(msg)
        raise ValueError(msg)

