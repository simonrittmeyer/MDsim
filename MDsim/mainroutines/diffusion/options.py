# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import copy

from MDsim.tools.options import Option
from MDsim.tools.options import create_options_dict
from MDsim.tools.options import create_options_set
from MDsim.tools.options import generic_options_dict
from MDsim.tools.options import generic_options_set
from MDsim.tools.options import check_options_dict_unknown as _check_options_dict_unknown

from MDsim.observables.implemented import postprocessing as implemented_observables_postproc

from MDsim.coordinates.diffusion import implemented_surfaces

from MDsim.coordinates.diffusion.fcc111 import implemented_directions as implemented_directions_fcc111
from MDsim.coordinates.diffusion.fcc100 import implemented_directions as implemented_directions_fcc100

from MDsim.coordinates.diffusion.fcc111 import implemented_sites as implemented_sites_fcc111
from MDsim.coordinates.diffusion.fcc100 import implemented_sites as implemented_sites_fcc100

implemented_directions = implemented_directions_fcc111 + implemented_directions_fcc100
implemented_sites = list(set(implemented_sites_fcc111 + implemented_sites_fcc100))

_options = [
            # the system variables
            Option(name='Natoms',
                   fmt='int',
                   category='system',
                   val=None,
                   info='Number of atoms in the simulation.'),

            Option(name='initial_positions',
                   fmt='str',
                   category='system',
                   val='random',
                   choices=['random', 'origin'] + implemented_sites,
                   info='How to initially distribute positions. Either completely random, or distributed over high symmetry sites.'),

            Option(name='Ndim',
                   fmt='int',
                   category='system',
                   val=2,
                   info='Number of Cartesian degrees of freedom per atom.'),

            Option(name='species',
                   fmt='str',
                   category='system',
                   val=None,
                   info='Chemical symbol of the diffusing species',
                   case_sensitive=True,
                   choices=['Na']),

            Option(name='mass',
                   fmt='float',
                   category='system',
                   val=None,
                   info='Mass of the diffusing species.'),

            Option(name='temperature',
                   fmt='float',
                   category='system',
                   val=0.,
                   info='System temperature in a Langevin-like description.'),

            Option(name='surface_facet',
                   fmt='str',
                   category='system',
                   val=None,
                   choices=implemented_surfaces,
                   info='Surface facet that defines all coordinates.'),

            Option(name='surface_lattice_constant',
                   fmt='float',
                   category='system',
                   val=None,
                   info='Surface lattice constant in Angstrom.'),

            Option(name='coverage',
                   fmt='float',
                   category='system',
                   val=0.,
                   info='The relative coverage with respect to one monolayer.'),

            Option(name='saturation_coverage',
                   fmt='float',
                   category='system',
                   val=1.,
                   info='Definition of one monolayer, ie. number of adsorbate atoms per surface atoms at saturation coverage.'),

            Option(name='dipole_moment_zero_coverage',
                   fmt='float',
                   category='system',
                   val=None,
                   info='Dipole moment at zero coverage in Debye.'),

            Option(name='polarizability',
                   fmt='float',
                   category='system',
                   val=None,
                   info='Dipole polarizability in 1/Angstrom**3'),

            # the potential-related variables
            Option(name='type',
                   fmt='str',
                   category='potential',
                   val=None,
                   choices=['interpolated'],
                   info='Method to represent the potential energy surface.'),

            Option(name='interpolation',
                   fmt='str',
                   category='potential',
                   val='fourier',
                   choices=['fourier', 'splines', 'cambridge'],
                   info='Interpolation method for the potential energy surface (PES).'),

            Option(name='basis',
                   fmt='str',
                   category='potential',
                   val='minimal',
                   choices=['minimal', 'top-hollow', 'custom'],
                   info='Basis for the Fourier interpolation.'),

            Option(name='real_order',
                   fmt='int',
                   category='potential',
                   val=None,
                   info='Order of the real-part Fourier potential expansion.'),

            Option(name='imag_order',
                   fmt='int',
                   category='potential',
                   val=None,
                   info='Order of the imaginary-part Fourier potential expansion.'),

            Option(name='constant',
                   fmt='bool',
                   category='potential',
                   val=True,
                   info='Use a constant contribution in the Fourier potential expansion.'),

            Option(name='normalize',
                   fmt='bool',
                   category='potential',
                   val=True,
                   info='Normalize the energies for the potential such that Emin = 0 eV.'),

            Option(name='potfile',
                   fmt='str',
                   category='potential',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant energies in eV.'),

            Option(name='potfile_usecols',
                   fmt='int_list',
                   category='potential',
                   val=[0, 1, 2],
                   info='Columns to be used from the potfile. First and second number are x and y, last number is E.'),

            # the friction-related variables
            Option(name='type',
                   fmt='str',
                   category='friction',
                   val=None,
                   choices=['interpolated', 'constant'],
                   info='Type of (electronic) friction to be employed.'),

            Option(name='value',
                   fmt='float',
                   category='friction',
                   val=None,
                   info='Value of the constant (electronic) friction in AMU per fs'),

            Option(name='interpolation',
                   fmt='str',
                   category='friction',
                   val='fourier',
                   choices=['fourier', 'splines'],
                   info='Interpolation method for the interpolated (electronic) friction.'),

            Option(name='basis',
                   fmt='str',
                   category='friction',
                   val='minimal',
                   choices=['minimal', 'custom'],
                   info='Basis for the Fourier interpolation of the (electronic) friction coefficient.'),

            Option(name='real_order',
                   fmt='int',
                   category='friction',
                   val=None,
                   info='Order of the real-part Fourier friction expansion.'),

            Option(name='imag_order',
                   fmt='int',
                   category='friction',
                   val=None,
                   info='Order of the imaginary-part Fourier friction expansion.'),

            Option(name='constant',
                   fmt='bool',
                   category='friction',
                   val=True,
                   info='Use a constant contribution in the Fourier friction expansion.'),

            Option(name='etafile',
                   fmt='str',
                   category='friction',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant friction coefficients in AMU per fs.'),

            Option(name='etafile_usecols',
                   fmt='int_list',
                   category='friction',
                   val=[0, 1, 2],
                   info='Columns to be used from the etafile. First and second number are x and y, last number is eta.'),

            # explicit pairwise parameters
            Option(name='force_cutoff',
                   fmt='float',
                   category='pairwise',
                   val=None,
                   info='Cutoff distance for the pairwise forces.'),

            Option(name='list_cutoff',
                   fmt='float',
                   category='pairwise',
                   val=None,
                   info='Cutoff distance for the neighbor lists'),

            Option(name='no_neighborlists',
                   fmt='bool',
                   category='pairwise',
                   val=False,
                   info='Do *not* use neighbor lists.'),

            Option(name='delta_k_directions',
                   fmt='str_list',
                   category='io',
                   val=None,
                   case_sensitive=True,
                   choices=implemented_directions,
                   info='The surface direction(s) along which to measure the ISF.'),

            Option(name='nkpts',
                   fmt='int',
                   category='io',
                   val=20,
                   info='Number of points along delta K at which to evaluate the ISF.'),

            Option(name='delta_k_max_amplitude',
                   fmt='float',
                   category='io',
                   val=4,
                   info='Maximum delta K value for the ISF evaluation.'),

            Option(name='alternative_eta',
                   fmt='float',
                   category='io',
                   val=4,
                   info='Alternative eta value'),

            Option(name='alternative_eta_interpolation',
                   fmt='str',
                   category='io',
                   val='fourier',
                   choices=['fourier', 'splines'],
                   info='Interpolation method for the alternative interpolated (electronic) friction.'),

            Option(name='alternative_eta_basis',
                   fmt='str',
                   category='io',
                   val='minimal',
                   choices=['minimal', 'custom'],
                   info='Basis for the Fourier interpolation of the alternative (electronic) friction coefficient.'),

            Option(name='alternative_eta_real_order',
                   fmt='int',
                   category='io',
                   val=None,
                   info='Order of the real-part Fourier alternative friction expansion.'),

            Option(name='alternative_eta_imag_order',
                   fmt='int',
                   category='io',
                   val=None,
                   info='Order of the imaginary-part Fourier alternative friction expansion.'),

            Option(name='alternative_eta_constant',
                   fmt='bool',
                   category='io',
                   val=True,
                   info='Use a constant contribution in the Fourier alternative friction expansion.'),

            Option(name='alternative_etafile',
                   fmt='str',
                   category='io',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant alternative friction coefficients in AMU per fs.'),

            Option(name='alternative_etafile_usecols',
                   fmt='int_list',
                   category='io',
                   val=[0, 1, 2],
                   info='Columns to be used from the etafile. First and second number are x and y, last number is eta.'),

            # equilibration
            Option(name='equilibration_time',
                   fmt='float',
                   category='propagation',
                   val=0,
                   info='Time (fs) to equilibrate the system before actual simulation starts.'),

            # post processing
            Option(name='observables',
                   fmt='str_list',
                   category='postprocessing',
                   val=None,
                   multvals=True,
                   # we add the individuals to it...
                   choices=implemented_observables_postproc+['isf_individual', 'dsf_individual'],
                   info='Post processing observables for which there will be an output.'),

            Option(name='fft_autocorrelation',
                   fmt='str',
                   category='postprocessing',
                   val='linear',
                   choices=['linear', 'cyclic', 'corrected'],
                   info='Consider the autocorrelation to be linear or cyclic'),

            Option(name='fft_optimize_length',
                   fmt='bool',
                   category='postprocessing',
                   val=False,
                   info='Extend the trajectory length by zero padding to the nearest power of two to evaluate the ISF/DSF.'),

            # simulation as such
            Option(name='mode',
                   category='propagation',
                   fmt='str',
                   choices=['accumulation', 'single'],
                   val='single',
                   info='The simulation mode. Either a single run, or multiple runs and accumulation.'),
            Option(name='Nruns',
                   category='propagation',
                   fmt='int',
                   val=1,
                   info='Number of runs in accumulation mode.'),

            Option(name='support',
                   category='misc',
                   fmt='str',
                   val='fortran',
                   choices=['fortran', 'cython', 'numba', 'fallback'],
                   info='Which extensions to be used to speedup the code.')
            ]




options_dict = copy.deepcopy(generic_options_dict)
_options_dict = create_options_dict(_options)

for category in _options_dict.keys():
    if category not in options_dict:
        options_dict[category] = _options_dict[category]
    else:
        options_dict[category].update(_options_dict[category])

options_set = generic_options_set
options_set.update(create_options_set(_options))

def get_options_dict():
    return copy.deepcopy(options_dict)

def check_options_dict_unknown(options_dict):
    return _check_options_dict_unknown(options_dict, options_set)
