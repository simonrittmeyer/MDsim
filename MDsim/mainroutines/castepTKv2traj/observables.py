# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim.observables import GenericObservable


class Casteptkv2Traj(GenericObservable):
    name = 'casteptkv2_traj'
    GenericObservable.register(name)

    def __init__(self, *args, **kwargs):
        omega = kwargs.pop('omega', None)
        kwargs['column_names'] = ['Qdot', 'Q']
        GenericObservable.__init__(self, *args, **kwargs)

        self.info = "Trajectory for castepTKv2"
        self.info += "\n\tInitial system energy: Einit = {:.6f} meV".format(self.system.get_total_energy()*1000)

        if omega:
            self.info += "\n\tharmonic frequency: omega = {:.6f} meV".format(omega*1000)
            self.info += "\n\t--> Einit = {:.1f} hbar*omega".format(self.system.get_total_energy() / omega)


    def get_observable_values(self):
        # the direct assignment avoids a copy
        storage = np.empty(2)
        storage[0] = self.system.get_velocities()[:,:]
        storage[1] = self.system.get_positions()[:,:]

        return storage

    def get_specific_info(self):
        return self.info
