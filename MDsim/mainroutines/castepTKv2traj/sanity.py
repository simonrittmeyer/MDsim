# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os

from MDsim.tools.options import create_args_dict
from MDsim.observables import require_iatom



def check_sanity(options_dict, verbose=True):
    """
    Check if we have everything we need
    """

    args = create_args_dict(options_dict)

    # values that must not be None in any case
    required = ['system.species',
                'system.masses',
                'system.initial_energy',
                'propagation.simulation_time',
                'propagation.timestep',
                'potential.potfile',
                'system.traj_type'
                ]

    complete = True

    if verbose:
        print('Checking for input completeness')

    msg = 'Incomplete input! The following arguments must not be None:'

    for req in required:
        if args[req] is None:
            msg += '\n\t* "{}"'.format(req)
            complete = False

    if not complete:
        if verbose:
            print(msg)
        raise ValueError(msg)

    if verbose:
        print('Checking for conditional requirements')

    msg = ''

    # checking for the PES
    if args['potential.type'] == 'interpolated':
        if not os.path.isfile(args['potential.potfile']):
            raise IOError('Invalid potfile location')

    # checking for the friction
    if args['friction.type'] == 'interpolated':
        if not os.path.isfile(args['friction.etafile']):
            raise IOError('Invalid etafile location')

    # traj type specifics
    if args['system.traj_type'] == 'vibration':
        if args['system.ndim'] == 2:
            if not len(args['potential.potfile_usecols']) == 3:
                msg += '\n2D potential requires 3 columns to be used'
            if not len(args['system.masses']) == 2:
                msg += '\nWrong masses dimensionality'
        pass
    elif args['system.traj_type'] == 'diffusion':
        if args['potential.type'] == 'harmonic':
            msg += '\nNo "harmonic" potential for diffusion trajectories'
        if args['system.ndim'] != 1:
            msg += '\nMulti dimensional diffusion is not (yet) supported'
    if msg:
        if verbose:
            print(msg)
        raise ValueError(msg)
