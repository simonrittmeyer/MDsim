# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import copy

from MDsim.tools.options import Option
from MDsim.tools.options import create_options_dict
from MDsim.tools.options import create_options_set
from MDsim.tools.options import generic_options_dict
from MDsim.tools.options import generic_options_set

from MDsim.observables.implemented import postprocessing as implemented_observables_postproc

_options = [
            Option(name='species',
                   fmt='str',
                   category='system',
                   val=None,
                   info='Name of the species/DOF',
                   case_sensitive=True,
                   ),

            Option(name='positions_eq',
                   fmt='float_array',
                   info='Absolute equilibrium positions.',
                   val=None,
                   case_sensitive=False
                   ),

            Option(name='ndim',
                   fmt='int',
                   category='system',
                   val=1,
                   info='Number of degrees of freedom in the simulation',
                   ),

            Option(name='masses',
                   fmt='float_array',
                   category='system',
                   val=None,
                   info='Mass of the species/DOF.'),

            Option(name='initial_energy',
                   fmt='float',
                   category='system',
                   val=None,
                   info='Initial energy. Either in eV or in vibrational quanta.'),

            Option(name='initial_energy_unit',
                   fmt='str',
                   category='system',
                   val='ev',
                   choices=['ev', 'quanta'],
                   info='Units of measure for the initial energy'),

            # the potential-related variables
            Option(name='type',
                   fmt='str',
                   category='potential',
                   val='interpolated',
                   choices=['interpolated', 'harmonic'],
                   info='Method to represent the potential energy surface.'),

            Option(name='normalize',
                   fmt='bool',
                   category='potential',
                   val=True,
                   info='Normalize the energies for the potential such that Emin = 0 eV.'),

            Option(name='potfile',
                   fmt='str',
                   category='potential',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant energies in eV.'),

            Option(name='potfile_usecols',
                   fmt='int_list',
                   category='potential',
                   val=[0, 1],
                   info='Columns to be used from the potfile. First and second number are x and y, last number is E.'),

            # the friction-related variables
            Option(name='type',
                   fmt='str',
                   category='friction',
                   val=None,
                   choices=['interpolated', 'constant'],
                   info='Type of (electronic) friction to be employed.'),

            Option(name='value',
                   fmt='float',
                   category='friction',
                   val=None,
                   info='Value of the constant (electronic) friction in AMU per fs'),

            Option(name='etafile',
                   fmt='str',
                   category='friction',
                   val=None,
                   case_sensitive=True,
                   info='Path to the file storing the relevant friction coefficients in AMU per fs.'),

            Option(name='etafile_usecols',
                   fmt='int_list',
                   category='friction',
                   val=[0, 1, 2],
                   info='Columns to be used from the etafile. First and second number are x and y, last number is eta.'),

            Option(name='normalmodes_delta',
                   fmt='float',
                   category='misc',
                   val=0.001,
                   info='Stepwidth (in Angstrom) of the finite difference algorithm evaluating the Hessian.'),

            Option(name='traj_type',
                   fmt='str',
                   category='system',
                   val=None,
                   choices=['vibration', 'diffusion'],
                   info='Type of motion to be simulated.')


            ]




options_dict = copy.deepcopy(generic_options_dict)
_options_dict = create_options_dict(_options)

for category in _options_dict.keys():
    if category not in options_dict:
        options_dict[category] = _options_dict[category]
    else:
        options_dict[category].update(_options_dict[category])

options_set = generic_options_set
options_set.update(create_options_set(_options))

def get_options_dict():
    return copy.deepcopy(options_dict)
