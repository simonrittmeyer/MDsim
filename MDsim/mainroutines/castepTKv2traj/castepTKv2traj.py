# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import sys

import numpy as np
import time

from MDsim import units
from MDsim import __version__

from MDsim.prettyprint import format_time
from MDsim.prettyprint import get_date
from MDsim.prettyprint import update_progress

from MDsim.tools.options import update_options_values
from MDsim.tools.options import create_args_dict
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_filehandler

from MDsim.prettyprint.stdouthandler import StdoutHandler

from MDsim.mainroutines.castepTKv2traj.options import get_options_dict
from MDsim.mainroutines.castepTKv2traj.sanity import check_sanity

logo=r"""
+------------------------------------------------------------------------------+
|                  _           _____ _  __      ____    _____           _      |
|     ___ __ _ ___| |_ ___ _ _|_   _| |/ /_   _|___ \  |_   _| __ __ _ (_)     |
|    / __/ _` / __| __/ _ \ '_ \| | | ' /\ \ / / __) |   | || '__/ _` || |     |
|   | (_| (_| \__ \ ||  __/ |_) | | | . \ \ V / / __/    | || | | (_| || |     |
|    \___\__,_|___/\__\___| .__/|_| |_|\_\ \_/ |_____|   |_||_|  \__,_|/ |     |
|                         |_|                                        |__/      |
|                                                                              |
|                             Simon P. Rittmeyer                               |
|                         simon.rittmeyer(at)tum.de                            |
+------------------------------------------------------------------------------+
| Part of the MDsim package {0:<51}|
+------------------------------------------------------------------------------+
"""[1:-1:].format('(version {})'.format(__version__))



def main(infile=None, py_args={}, cmd_args={}, verbose=True, progressbar=True):
    """
    Diffusion main routine.

    We have the following order in case of conflicting arguments

        py_args > cmd_args > infile

    Parameters
    ----------
    infile : str, optional (default=None)
        Infile to be parsed.

    py_args : dictionary, optional (default = {})
        Options additionally passed on a python level.

    cmd_args : dictionary, optional (default = {})
        Options passed from the command line.

    verbose : boolena, optional (default = True)
        Print information to stdout. If False, then simply stdout is directed
        to /dev/null ;)

    progressbar : bool, optional (default=True)
        Show a progress bar that updates dynamically. Do *not* use this option
        if you pipe to a file.
    """

    # this is the dictionary in which we store the timings
    tmp = time.time()
    _start = tmp

    times = {'start': tmp}

    if not verbose:
        devnull = open(os.devnull, 'w')
        _stdout = sys.stdout
        sys.stdout = devnull

    section_template = StdoutHandler._section_template

    print(logo[:-1])
    print(section_template.format('Program started : {}'.format(get_date())))

    print(section_template.format('INITIALIZATION'))


    # ----------------------------------
    # READING INFILE AND PARSING OPTIONS
    # ----------------------------------

    options_dict = get_options_dict()

    if infile:
        from MDsim.tools.parser import parse_inputfile
        options_dict = parse_inputfile(infile, options_dict)
    else:
        print('No input file given')

    if cmd_args:
        print('Updating options with additional command line arguments')
        options_dict = update_options_values(cmd_args, options_dict)
    else:
        print('No additional command line arguments passed')

    if py_args:
        print('Updating options with additional python-level input arguments')
        options_dict = update_options_values(py_args, options_dict)
    else:
        print('No additional python-level arguments passed')

    check_sanity(options_dict, verbose=verbose)

    # ok, now we can change things
    args = create_args_dict(options_dict)

    # ------------------------
    # SETTING UP THE POTENTIAL
    # ------------------------
    masses = np.asarray([args['system.masses']])
    # here comes the interpolation for the PES
    # TODO: switch for harmonic/morse potential

    Ndim = args['system.ndim']

    if args['system.traj_type'] == 'vibration':

        if Ndim == 1:
            from MDsim.tools.createobjects import create_univariatepotential
            potential = create_univariatepotential(datafile=args['potential.potfile'],
                                                   usecols=args['potential.potfile_usecols'],
                                                   periodic=False,
                                                   bounds_check=True,
                                                   forces_method='analytical',
                                                   verbose=verbose,
                                                   normalize=args['potential.normalize'],
                                                   convert=True,
                                                   )
        elif N == 2:
            from MDsim.tools.createobjects import create_bivariatepotential

        # well, now we need the frequencies
        from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential
        omegas, normalmodes, positions, coords = evaluate_normalmodes_on_potential(potential=potential,
                                                                                   positions=np.zeros((1,1)),
                                                                                   masses=masses,
                                                                                   # no mimization, otherwise out snapshot-coordinate mapping is off,
                                                                                   minimize_first=False,
                                                                                   delta=args['misc.normalmodes_delta'],
                                                                                   convert=True,
                                                                                   verbose=verbose)


        if args['potential.type'] == 'harmonic':
            from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
            if verbose:
                print('Creating NormalmodePotential instance')
            potential = NormalmodePotential(normalmodes=coords)

        omega = omegas[0]

    elif args['system.traj_type'] == 'diffusion':
        from MDsim.tools.createobjects import create_univariatepotential
        potential = create_univariatepotential(datafile=args['potential.potfile'],
                                               usecols=args['potential.potfile_usecols'],
                                               periodic=True,
                                               bounds_check=False,
                                               forces_method='analytical',
                                               verbose=verbose,
                                               normalize=args['potential.normalize'],
                                               convert=True,
                                               )

        positions=np.zeros((1,1))
        omega = None
    # -----------------------
    # SETTING UP THE FRICTION
    # -----------------------
    friction_matrix = None


    # ----------------------------------
    # EVALUATING INITIAL ENERGY/VELOCITY
    # ----------------------------------
    Einit = args['system.initial_energy']

    if args['system.initial_energy_unit'] == 'quanta':
        # omegas is in eV, see the convert switch above
        Einit *= omegas[0]

    # well, go to a.u. to evaluate the velocities and then back again (always use the positive velocity)
    velocities = np.sqrt(2*Einit*units.EV_TO_AU / (masses * units.AMU_TO_AU))*units.AU_TO_ANGSTROM_PER_FS
    velocities = np.asarray(velocities).reshape(1,1)


    # ---------------------
    # SETTING UP THE SYSTEM
    # ---------------------
    from MDsim.systems.system import System
    if verbose:
        print('Creating system instance')

    system = System(positions=positions,
                    velocities=velocities,
                    masses=masses,
                    # watch for dimensionality
                    species=np.asarray([args['system.species']]),
                    potential=potential,
                    friction=friction_matrix,
                    temperature=0,
                    convert=True,
                    _verbose=False)
    # now we have everything for the simulation

    # -------------------------
    # SETTING UP THE INTEGRATOR
    # -------------------------

    integrator = create_integrator(method=args['propagation.integrator'],
                                   dt=args['propagation.timestep'],
                                   convert=True,
                                   verbose=verbose)

    # --------------------------------------------
    # PRE-EVALUATING THE NUMBER OF STEPS NECESSARY
    # --------------------------------------------

    # comparing against au saves us Nsteps multiplications of the system time
    t_end = args['propagation.simulation_time'] * units.FS_TO_AU
    iostep = args['io.iostep']

    tot_steps = np.ceil(args['propagation.simulation_time'] /  args['propagation.timestep'])
    tot_iosteps = int(np.floor(tot_steps / iostep) + 1)

    # ----------------------------------
    # SETTING UP THE ON-THE-FLY HANDLERS
    # ----------------------------------

    # create the io-related things
    if args['io.debug']:
        debug_args = args
    else:
        debug_args = None

    from MDsim.mainroutines.castepTKv2traj.observables import Casteptkv2Traj

    obs = Casteptkv2Traj(system=system,
                         omega=omega)

    handlers = []

    for o in [obs, 'energy']:
        handler = create_filehandler(system_inst=system,
                                     integrator_inst=integrator,
                                     observable=o,
                                     fileformat=args['io.fileformat'],
                                     seed=args['io.seed'],
                                     outfolder=args['io.outpath'],
                                     compress=args['io.compress'],
                                     convert=True,
                                     datatype=args['io.datatype'],
                                     verbose=verbose,
                                     Nsteps=tot_iosteps,
                                     args=debug_args)
        handlers.append(handler)

    # stdouthandler
    stdouthandler = StdoutHandler(system=system,
                                  integrator=integrator)

    try:
        times['init'] += time.time() - tmp
    except KeyError:
        times['init'] = time.time() - tmp

    tmp = time.time()

    if args['io.debug']:
        print(stdouthandler.get_options(args))


    # --------------------------
    # THE ACTUAL SIMULATION LOOP
    # --------------------------

    print(section_template.format('SIMULATION'))

    # now that we have the handlers we can start with the actual propagation
    print('Simulating {} fs with dt = {} fs, ie., {} steps in total\n\t(--> {} io steps)'.format(
          args['propagation.simulation_time'],
          args['propagation.timestep'],
          int(tot_steps),
          int(tot_iosteps))
         )

    nsteps = 0

    if progressbar:
        updatestep = int(np.ceil(tot_steps*0.001))
        print()

    # walltime checking
    _start_loop = float(time.time())
    if args['misc.walltime'] is not None:
        _exit_loop = 0.95*args['misc.walltime']
    else:
        _exit_loop=False

    _interrupt = False

    # initialize the handlers
    for handler in handlers:
        handler.push()

    while nsteps < tot_steps:
        try:
            integrator.propagate(system)
            nsteps += 1

            if nsteps%iostep == 0:
                for handler in handlers:
                    handler.push()
                if _exit_loop:
                    if (time.time() - _start) > _exit_loop:
                        print('Breaking simulation loop due to insufficient walltime')
                        print('\tsimulated steps : {}'.format(nsteps))
                        print('\tsimulated time  : {} fs'.format(int(system.get_time())))
                        break
            if progressbar:
                if nsteps%updatestep == 0:
                    update_progress(nsteps / tot_steps)
        except KeyboardInterrupt:
            print('Caught KeyboardInterrupt... Exiting')
            _interrupt = True
            break

    if not _interrupt:
        print()
        print('Simulation finished')

    try:
        times['simulation'] += time.time() - tmp
    except KeyError:
        times['simulation'] = time.time() - tmp

    tmp = time.time()

    print(section_template.format('FINALIZING'))
    print('Closing handler:')
    for handler in handlers:
        handler.close()

    try:
        times['finalizing'] += time.time() - tmp
    except KeyError:
        times['finalizing'] = time.time() - tmp

    # -----------------
    # END OF SIMULATION
    # -----------------

    # ok, now we are done ;)
    times['runtime'] = time.time() - times['start']

    print(section_template.format('RUNTIME INFORMATION'))
    print('Initialization    : {}'.format(format_time(times['init'])))
    print('Time propagation  : {}'.format(format_time(times['simulation'])))
    print('Finalizing        : {}'.format(format_time(times['finalizing'])))
    print('-----------------   -----------')
    print('Total Runtime     : {}'.format(format_time(times['runtime'])))

    print(section_template.format('Program ended : {} -- Have a nice day :)'.format(get_date())))

    # restore standard out
    if not verbose:
        sys.stdout = _stdout

    if _interrupt:
        raise KeyboardInterrupt
