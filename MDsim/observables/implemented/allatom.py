# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements AllAtom observables.
"""

import numpy as np

from MDsim import units

from MDsim.observables import AllAtomObservable


# ---------------------
# all atoms observables
# ---------------------

class Positions(AllAtomObservable):
    name = 'positions'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'pos'
        AllAtomObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_positions().ravel()


class Velocities(AllAtomObservable):
    name = 'velocities'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'vel'
        AllAtomObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_velocities().ravel()


class Forces(AllAtomObservable):
    name = 'forces'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'force'
        AllAtomObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_forces().ravel()


class Etas(AllAtomObservable):
    name = 'etas'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'eta'
        AllAtomObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_etas().ravel()


class Momenta(AllAtomObservable):
    name = 'momenta'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'momentum'
        AllAtomObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_momenta().ravel()


class FomIntegrand(AllAtomObservable):
    # ------------------------------------------------------------------
    # this is the integrand of the Fourier transform within the forced
    # oscillator model (FOM) :
    #
    # A. C. Luntz, M. Persson and G. O. Sitz, J. Chem. Phys. 124, (2006).
    # http://www.dx.doi.org/10.1063/1.2177664
    # -------------------------------------------------------------------
    name = 'fom_integrand'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'sqrt(eta)*Rdot'
        AllAtomObservable.__init__(self, *args, **kwargs)

        self.stub = np.zeros(self.Ndof)
        self.unit_factor = np.sqrt(units.AU_TO_EV_PER_FS)

        if self.system.has_friction:
            if self.system.get_friction().is_diagonal():
                self.get_observable_values = self._get_observable_values_diagonal
            else:
                self.get_observable_values = self._get_observable_values_tensorial
        else:
            self.get_observable_values = self._get_zeros



    def _get_zeros(self):
        return self.stub

    def _get_observable_values_diagonal(self):
        return self.system.get_velocities(_au=True).ravel() * np.sqrt(self.system.get_etas(_au=True).ravel()) * self.unit_factor

    def _get_observable_values_tensorial(self):
        return np.dot(np.sqrt(self.system.get_etas(_au=True).reshape(self.Ndof, self.Ndof)),
                      self.system.get_velocities(_au=True).ravel()) * self.unit_factor
