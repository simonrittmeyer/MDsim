# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
How to implement new observables
--------------------------------

1) Choose one category: Atomic, AllAtom or Generic (or SurfaceCorrelation as
   defined in isf.py).

2) Derive a new class from the suitable parent class. While doing so, add
   "name" as class variable and do not forget to call the staticmethod
   "register()" of the parent class. This updates the list of implemented
   observables.

   The crucial routine to be implemented is "get_observable_values()". Have a
   look at the parent classes, they should provide you with all variables you
   require, most notably the self._value_storage and self.system variable.
"""

from MDsim.observables import AtomicObservable
from MDsim.observables import AllAtomObservable
from MDsim.observables import GenericObservable
from MDsim.observables import PostProcessingObservable

from MDsim.observables.implemented.atomic import Position
from MDsim.observables.implemented.atomic import Velocity
from MDsim.observables.implemented.atomic import Momentum
from MDsim.observables.implemented.atomic import Force
from MDsim.observables.implemented.atomic import Eta

from MDsim.observables.implemented.allatom import Positions
from MDsim.observables.implemented.allatom import Velocities
from MDsim.observables.implemented.allatom import Momenta
from MDsim.observables.implemented.allatom import Forces
from MDsim.observables.implemented.allatom import Etas
from MDsim.observables.implemented.allatom import FomIntegrand

from MDsim.observables.implemented.generic import Energy
from MDsim.observables.implemented.generic import Temperature
from MDsim.observables.implemented.generic import EffectiveEnergy
from MDsim.observables.implemented.generic import AverageEta
from MDsim.observables.implemented.generic import AlternativeAverageEta
from MDsim.observables.implemented.generic import RayleighFunction

from MDsim.observables.implemented.normalmodes import EnergyNormalmodes
from MDsim.observables.implemented.normalmodes import PositionsNormalmodes
from MDsim.observables.implemented.normalmodes import VelocitiesNormalmodes
from MDsim.observables.implemented.normalmodes import EhspecNormalmodes

from MDsim.observables.implemented.isf import ScatteringAmplitudes
from MDsim.observables.implemented.isf import IncoherentScatteringAmplitudes
from MDsim.observables.implemented.isf import SurfaceCorrelationObservable
from MDsim.observables.implemented.isf import Isf
from MDsim.observables.implemented.isf import IncoherentIsf
from MDsim.observables.implemented.isf import IsfViaDsf
from MDsim.observables.implemented.isf import IncoherentIsfViaDsf
from MDsim.observables.implemented.isf import Dsf
from MDsim.observables.implemented.isf import IncoherentDsf

from MDsim.observables.implemented.atomic import require_iatom
from MDsim.observables.implemented.generic import require_alternative_eta
from MDsim.observables.implemented.normalmodes import require_normalmodes
from MDsim.observables.implemented.isf import require_delta_K
from MDsim.observables.implemented.isf import require_fft
from MDsim.observables.implemented.isf import require_scattering_amplitudes
from MDsim.observables.implemented.isf import postprocessing



# convenient lists of availabe observables and those that require iatom
implemented = GenericObservable.get_implemented() \
                + AtomicObservable.get_implemented() \
                + AllAtomObservable.get_implemented() \
                + PostProcessingObservable.get_implemented() \
                + SurfaceCorrelationObservable.get_implemented()

on_the_fly = GenericObservable.get_implemented() \
                + AtomicObservable.get_implemented() \
                + AllAtomObservable.get_implemented()
