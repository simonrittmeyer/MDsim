# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements generic observables.
"""

import numpy as np

from MDsim import units

from MDsim.observables import GenericObservable

# -------------------
# generic observables
# -------------------

class Energy(GenericObservable):
    name = 'energy'
    GenericObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['column_names'] = ['E_pot', 'E_kin', 'E_tot']
        GenericObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        # the direct assignment avoids a copy
        storage = np.empty(3)
        storage[0] = self.system.get_potential_energy()
        storage[1] = self.system.get_kinetic_energy()
        storage[2] = np.sum(storage[0:2])

        return storage

class Temperature(GenericObservable):
    name = 'temperature'
    GenericObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['column_names'] = ['T_inst']
        GenericObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        # the direct assignment avoids a copy
        storage = np.ones(1) * self.system.get_instantaneous_temperature()
        return storage

class EffectiveEnergy(GenericObservable):
    """
    Observable that contains the effective energy changes as introduced by Bussi and Parinello.

    Initialization
    --------------
    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """

    name = 'effective_energy'
    GenericObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['column_names'] = ['E_pot', 'E_kin', 'E_tot', 'E_eff']
        GenericObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        # the direct assignment avoids a copy
        storage = np.empty(4)
        storage[0] = self.system.get_potential_energy()
        storage[1] = self.system.get_kinetic_energy()
        storage[2] = self.system.get_total_energy()
        storage[3] = self.system.get_effective_energy()

        return storage


class AverageEta(GenericObservable):
    """
    Observable that contains the average friction coefficient of the system,
    together with the respective standard deviation.

    Initialization
    --------------
    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'average_eta'
    GenericObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['column_names'] = ['mean(eta)', 'std(eta)']
        GenericObservable.__init__(self, *args, **kwargs)
    def get_observable_values(self):
        # the direct assignment avoids a copy
        # to the unit conversion only for the mean, not every element
        return [np.mean(self.system.get_etas(_au=True))*units.AU_TO_AMU_PER_FS,
                np.std(self.system.get_etas(_au=True))*units.AU_TO_AMU_PER_FS]



class AlternativeAverageEta(GenericObservable):
    """
    Observable that evaluates an alternative averaged friction coefficient by
    invoking the system positions and calling the alternative riction
    formulation with it. Note that there is no feedback to the actual
    trajectories.

    Initialization
    --------------
    alternative_eta : MDsim friction matrix instance
        The alternative friction matrix. Make sure that this function expects
        input in a.u.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'alternative_average_eta'
    GenericObservable.register(name)

    def __init__(self, alternative_eta, *args, **kwargs):
        # note that alternative eta must expect au input!
        kwargs['column_names'] = ['mean(alt_eta)', 'std(alt_eta)']
        GenericObservable.__init__(self, *args, **kwargs)
        self._Ndof = self.system.get_Ndof()
        self.alternative_eta = alternative_eta

    def get_observable_values(self):
        # the direct assignment avoids a copy
        alt_eta = self.alternative_eta(self.system.get_positions(_au=True))
        return [np.mean(alt_eta)*units.AU_TO_AMU_PER_FS,
                np.std(alt_eta)*units.AU_TO_AMU_PER_FS]

    def get_specific_info(self):
        info =  'Underlying friction interpolation:'
        info += '\n----------------------------------'
        info += '\n\n' + str(self.alternative_eta)
        return info

class RayleighFunction(GenericObservable):
    name = 'rayleigh_function'
    GenericObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['column_names'] = ['Rayleigh Function F']
        GenericObservable.__init__(self, *args, **kwargs)

        if self.system.has_friction:
            if self.system.get_friction().is_diagonal():
                self.get_observable_values = self._get_observable_values_diagonal
            else:
                self.get_observable_values = self._get_observable_values_tensorial
        else:
            self.get_observable_values = lambda : np.array([0.])

    def get_specific_info(self):
        info =  'Rayleigh Function as defined as:'
        info += '\n    F = 0.5*sum(Rdot.eta.Rdot)'
        info += '\nNote that dEdiss(t)/dt = 2*F'
        return info

    def _get_observable_values_diagonal(self):
        # the direct assignment avoids a copy
        return np.array([0.5 * np.sum(self.system.get_velocities(_au=True).ravel()**2 * self.system.get_etas(_au=True).ravel())*units.AU_TO_EV_PER_FS])

    def _get_observable_values_tensorial(self):
        # the direct assignment avoids a copy
        return np.array([0.5 * np.sum(np.dot(self.system.get_velocities(_au=True).ravel(), self.system.get_etas(_au=True).dot(self.system.get_velocities(_au=True)))) * units.AU_TO_EV_PER_FS])

require_alternative_eta = ['alternative_average_eta']
