# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

"""
Module that implements normalmode-transformation related output
"""

from __future__ import print_function
import numpy as np
from MDsim import units
from MDsim.observables import GenericObservable

class EnergyNormalmodes(GenericObservable):
    """
    Observable to follow the population of modes.

    Initialization
    --------------
    normalmodes : MDsim normalmodes instance
        The MDsim normalmodes instance that handles the coordinate
        transformations.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'energy_normalmodes'
    GenericObservable.register(name)

    def __init__(self, normalmodes, *args, **kwargs):
        self.normalmodes = normalmodes
        self.Nmodes = self.normalmodes.get_Nmodes()

        # here comes our harmonic potential
        # we do not care about forces here anyway, so no specific arguments
        # required
        from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
        self.harmonic_potential = NormalmodePotential(normalmodes=self.normalmodes)

        column_names = ['E_{:}(mode {:})'.format(i, j) for j in range(self.Nmodes)
                                                       for i in ['pot', 'kin', 'tot']]
        column_names += ['E_{:}(SYSTEM)'.format(i) for i in ['pot', 'kin', 'tot']]
        kwargs['column_names'] = column_names

        GenericObservable.__init__(self, *args, **kwargs)


    def get_observable_values(self):
        # the kinetic energy
        Ekin_nm = 0.5*(self.normalmodes.velocities_cartesian_to_normalmodes(self.system.get_velocities(_au=True)))**2

        # the potential energy
        Epot_nm = self.harmonic_potential.get_Epots_modes(self.system.get_positions(_au=True))

        # convert to eV
        Ekin_nm *= units.AU_TO_EV
        Epot_nm *= units.AU_TO_EV

        # the total energy
        Etot_nm = Ekin_nm + Epot_nm


        storage = np.empty((self.Nmodes+1)*3)

        # fill it
        storage[0:self.Nmodes*3:3] = Epot_nm
        storage[1:self.Nmodes*3:3] = Ekin_nm
        storage[2:self.Nmodes*3:3] = Etot_nm
        storage[-3] = self.system.get_potential_energy()
        storage[-2] = self.system.get_kinetic_energy()
        storage[-1] = self.system.get_total_energy()

        return storage


    def get_specific_info(self):
        info =  'The decomposition of the kinetic energy is exact.'
        info += '\nThe decomposition of the potential energy relies on the harmonic approximation though.'
        info += '\n'
        info += '\nTransformation matrix Uinv used to go from cartesian space to normalmode space\n'
        info += '\n    Uinv.R = Q and Uinv.Rdot = Qdot'
        info += '\n    ==== -   -     ==== ----   ----\n'
        info += '\nUnits are au'
        info += '\n---------------------------------------------------------------------------'
        info += '\n\n{}'.format(self.normalmodes.print_Uinv(_au=True))
        info += '\n'
        info += '\nHarmonic potential used:'
        info += '\n------------------------'
        info += '\n\n' + str(self.harmonic_potential)
        return info



class PositionsNormalmodes(GenericObservable):
    """
    Observable to monitor the positions vector in normalmode space.

    Initialization
    --------------
    normalmodes : MDsim normalmodes instance
        The MDsim normalmodes instance that handles the coordinate
        transformations.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'positions_normalmodes'
    GenericObservable.register(name)

    def __init__(self, normalmodes,  *args, **kwargs):
        self.normalmodes = normalmodes
        self.Nmodes = self.normalmodes.get_Nmodes()

        # here comes our harmonic potential
        # we do not care about forces here anyway, so no specific arguments
        # required
        from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
        self.harmonic_potential = NormalmodePotential(normalmodes=self.normalmodes)

        column_names = ['Q_{0} (mode {0})'.format(i) for i in range(self.Nmodes)]
        kwargs['column_names'] = column_names

        GenericObservable.__init__(self, *args, **kwargs)


    def get_observable_values(self):
        # the kinetic energy
        storage = np.empty((self.Nmodes))

        # fill it
        storage[:] = self.normalmodes.positions_cartesian_to_normalmodes(self.system.get_positions(_au=True)).ravel()
        #storage *= units.AU_TO
        return storage


    def get_specific_info(self):
        info =  'Projection of positions into normalmode space'
        info += '\n'
        info += '\nTransformation matrix Uinv used to go from cartesian space to normalmode space\n'
        info += '\n    Uinv.R = Q and Uinv.Rdot = Qdot'
        info += '\n    ==== -   -     ==== ----   ----\n'
        info += '\nUnits are au'
        info += '\n---------------------------------------------------------------------------'
        info += '\n\n{}'.format(self.normalmodes.print_Uinv(_au=True))
        info += '\n'
        info += '\nHarmonic potential used:'
        info += '\n------------------------'
        info += '\n\n' + str(self.harmonic_potential)
        return info


class VelocitiesNormalmodes(GenericObservable):
    """
    Observable to monitor the positions vector in normalmode space.

    Initialization
    --------------
    normalmodes : MDsim normalmodes instance
        The MDsim normalmodes instance that handles the coordinate
        transformations.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'velocities_normalmodes'
    GenericObservable.register(name)

    def __init__(self, normalmodes,  *args, **kwargs):
        self.normalmodes = normalmodes
        self.Nmodes = self.normalmodes.get_Nmodes()

        # here comes our harmonic potential
        # we do not care about forces here anyway, so no specific arguments
        # required
        from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
        self.harmonic_potential = NormalmodePotential(normalmodes=self.normalmodes)

        column_names = ['Qdot_{0} (d/dt mode {0})'.format(i) for i in range(self.Nmodes)]
        kwargs['column_names'] = column_names

        GenericObservable.__init__(self, *args, **kwargs)


    def get_observable_values(self):
        # the kinetic energy
        storage = np.empty((self.Nmodes))

        # fill it
        storage[:] = self.normalmodes.velocities_cartesian_to_normalmodes(self.system.get_velocities(_au=True)).ravel()
        #storage *= units.AU_TO_ANGSTROM_SQRT_AMU_PER_FS

        return storage


    def get_specific_info(self):
        info =  'Projection of velocities into normalmode space'
        info += '\n'
        info += '\nTransformation matrix Uinv used to go from cartesian space to normalmode space\n'
        info += '\n    Uinv.R = Q and Uinv.Rdot = Qdot'
        info += '\n    ==== -   -     ==== ----   ----\n'
        info += '\nUnits are au'
        info += '\n---------------------------------------------------------------------------'
        info += '\n\n{}'.format(self.normalmodes.print_Uinv(_au=True))
        info += '\n'
        info += '\nHarmonic potential used:'
        info += '\n------------------------'
        info += '\n\n' + str(self.harmonic_potential)
        return info


class EhspecNormalmodes(GenericObservable):
    """
    Observable to monitor the positions vector in normalmode space.

    Initialization
    --------------
    normalmodes : MDsim normalmodes instance
        The MDsim normalmodes instance that handles the coordinate
        transformations.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'ehspec_normalmodes'
    GenericObservable.register(name)

    def __init__(self, normalmodes,  *args, **kwargs):
        self.normalmodes = normalmodes
        self.Nmodes = self.normalmodes.get_Nmodes()

        # here comes our harmonic potential
        # we do not care about forces here anyway, so no specific arguments
        # required
        from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
        self.harmonic_potential = NormalmodePotential(normalmodes=self.normalmodes)

        column_names = ['Rdot_{0}'.format(i) for i in range(self.Nmodes)]
        column_names += ['R_{0}'.format(i) for i in range(self.Nmodes)]
        column_names +=  ['times_au']
        column_names += ['Qdot_{0} (d/dt mode {0})'.format(i) for i in range(self.Nmodes)]
        column_names += ['Q_{0} (mode {0})'.format(i) for i in range(self.Nmodes)]
        kwargs['column_names'] = column_names

        GenericObservable.__init__(self, *args, **kwargs)


    def get_observable_values(self):
        # the kinetic energy
        storage = np.empty(4*(self.Nmodes)+1)

        # fill it
        storage[:self.Nmodes] = self.system.get_velocities().ravel()
        storage[self.Nmodes:2*self.Nmodes] = self.system.get_positions().ravel()
        storage[2*self.Nmodes] = self.system.get_time(_au=True)
        storage[2*self.Nmodes+1:3*self.Nmodes+1] = self.normalmodes.velocities_cartesian_to_normalmodes(self.system.get_velocities(_au=True)).ravel()
        storage[3*self.Nmodes+1:] = self.normalmodes.positions_cartesian_to_normalmodes(self.system.get_positions(_au=True)).ravel()
        #storage *= units.AU_TO_ANGSTROM_SQRT_AMU_PER_FS

        return storage


    def get_specific_info(self):
        info =  ' Projection of positions and velocities into normalmode space'
        info += '\n   (note that times_au is the time in a.u.)'
        info += '\nTransformation matrix Uinv used to go from cartesian space to normalmode space\n'
        info += '\n    Uinv.R = Q and Uinv.Rdot = Qdot'
        info += '\n    ==== -   -     ==== ----   ----\n'
        info += '\nUnits are au'
        info += '\n---------------------------------------------------------------------------'
        info += '\n\n{}'.format(self.normalmodes.print_Uinv(_au=True))
        info += '\n'
        info += '\nHarmonic potential used:'
        info += '\n------------------------'
        info += '\n\n' + str(self.harmonic_potential)
        return info


require_normalmodes = ['energy_normalmodes', 'positions_normalmodes', 'velocities_normalmodes', 'ehspec_normalmodes']

