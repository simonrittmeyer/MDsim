# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Common helper routines
"""
from __future__ import print_function

import numpy as np

from MDsim import units

def get_kinfo(delta_K):
    """
    Pretty print for dK info. We expect dK in a.u.
    """
    delta_K = delta_K.T
    Nkpts, Ndim = delta_K.shape
    msg = '\n{:20s}'.format('Delta-K vectors')
    for i in range(Ndim):
        msg += '{:>20s}'.format(['dK_x','dK_y','dK_z'][i])
    msg += '{:>20s}'.format('|dK|')
    msg += '\n' + '-'*(20 + (Ndim + 1) * 20)
    for i in range(Nkpts):
        msg += '\n{:<20s}'.format('Delta-K_{}'.format(i))
        for j in range(Ndim):
            msg +='{:>20s}'.format('{:>12.8f} A**-1'.format(delta_K[i,j] / units.AU_TO_ANGSTROM))
        msg +='{:>20s}'.format('{:>12.8f} A**-1'.format(np.linalg.norm(delta_K[i]) / units.AU_TO_ANGSTROM))

    msg += '\n'
    msg += '\nNOTE:'
    msg += '\n-----'
    msg += '\nThe scattering amplitudes are stored separated in real and imaginary part!'

    return msg

