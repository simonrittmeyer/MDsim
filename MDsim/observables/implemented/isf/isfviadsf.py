# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements observables required to study surface diffusion.
"""

from __future__ import print_function

import numpy as np

from MDsim import units
from MDsim.analysis.isf import calc_isf_from_dsf

from MDsim.observables.implemented.isf import Dsf
from MDsim.observables.implemented.isf import IncoherentDsf

from MDsim.observables.implemented.isf.surfacecorrelation import SurfaceCorrelationObservable


class IsfViaDsf(Dsf):
    """
    Class to evaluate a single ISF directly from scattering amplitudes via
    evaluation of a DSF, or to accumulate many of them. If run in accumulation
    mode, you do not need to pass any of "times", "scattering_amplitudes" or
    "scattering_amplitudes_file" upon initialization. In single run mode, you
    need either "times" and "scattering_amplitudes" or
    "scattering_amplitudes_file".

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    normalize : boolean, optional (default=False)
        Normalize the result for each k-point. Only choose this option if you
        are sure that normalization at this point is appropriate.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """
    name = 'isf_via_dsf'
    SurfaceCorrelationObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['first_column_name'] = 'times'
        super(Dsf, self).__init__(*args, **kwargs)
        self._name = 'Intermediate Scattering Function (ISF) via accumulated Dynamical Structure Factors (DSFs)'

        self._accumulation_quantity = 'dsf'

        if self.autocorrelation == 'cyclic':
            self.zero_padding = False
        else:
            self.zero_padding = True

        self._coherent = True

    def get_all_data(self, sort=True):
        """
        Return all data at once, sorted anyway. Either we calculate on the fly
        or return the accumulated data.
        """
        data=super(IsfViaDsf, self).get_all_data(sort=False)

        freqs = data[:,0]
        dsf = data[:,1::]

        # evluate the ISF from the accumulated DSF
        data = np.c_[calc_isf_from_dsf(times=self.times,
                                       dsf=dsf,
                                       normalize=self._normalize)]

        if sort:
            data = data[data[:,0].argsort()]

        return data

# basically just a dummy
class IncoherentIsfViaDsf(Dsf):
    """
    Class to evaluate a single incoherent ISF directly from scattering
    amplitudes via evaluation of a DSF, or to accumulate many of them. If run
    in accumulation mode, you do not need to pass any of "times",
    "scattering_amplitudes" or "scattering_amplitudes_file" upon
    initialization. In single run mode, you need either "times" and
    "scattering_amplitudes" or "scattering_amplitudes_file".

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Natoms, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    normalize : boolean, optional (default=False)
        Normalize the result for each k-point. Only choose this option if you
        are sure that normalization at this point is appropriate.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """
    name = 'incoherent_isf_via_dsf'
    SurfaceCorrelationObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['first_column_name'] = 'times'
        super(Dsf, self).__init__(*args, **kwargs)
        self._name = 'Incoherent Intermediate Scattering Function (ISF) via accumulated Incoehrent Dynamical Structure Factors (DSFs)'

        self._accumulation_quantity = 'incoherent_dsf'

        if self.autocorrelation == 'cyclic':
            self.zero_padding = False
        else:
            self.zero_padding = True

        self._coherent = False

    def get_all_data(self, sort=True):
        """
        Return all data at once, sorted anyway. Either we calculate on the fly
        or return the accumulated data.
        """
        data=super(IncoherentIsfViaDsf, self).get_all_data(sort=False)

        freqs = data[:,0]
        dsf = data[:,1::]

        # evluate the ISF from the accumulated DSF
        data = np.c_[calc_isf_from_dsf(times=self.times,
                                       dsf=dsf,
                                       normalize=self._normalize)]

        if sort:
            data = data[data[:,0].argsort()]

        return data
