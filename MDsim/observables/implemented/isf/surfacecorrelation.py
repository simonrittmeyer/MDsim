# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements observables required to study surface diffusion.
"""
from __future__ import print_function

import numpy as np

from MDsim import units
from MDsim.observables import PostProcessingObservable
from MDsim.observables import check_observable_name

from MDsim.observables.implemented.isf.tools import get_kinfo

class SurfaceCorrelationObservable(PostProcessingObservable):
    """
    Base class for any Surface Correlation observable such as the dynamical
    structure factor (DSF) or the intermediate scattering function (ISF). Do
    not directly use this class but rather derived subclass thereof.

    Initialization
    --------------
    first_column_name : str
        The description of the first column.

    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    normalize : boolean, optional (default=False)
        Normalize the result for each k-point. Only choose this option if you
        are sure that normalization at this point is appropriate.

    zero_padding : bool, optinal (default=False)
        Whether we should zero pad for the FFT or not.

    optimize_length : bool, optinal (default=False)
        Truncate the input lenght to allow for efficient FFT (Radix2).

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """

    _implemented = []

    @staticmethod
    def register(name):
        """
        Register a new derived subclass to the common list of implemented
        methods.

        Parameters
        ----------
        name : string
            The name of the observable.
        """
        if check_observable_name(name):
            SurfaceCorrelationObservable._implemented.append(name)


    @staticmethod
    def get_implemented():
        """
        Return the list of implemented observables, ie. constructed subclasses.
        """
        return SurfaceCorrelationObservable._implemented


    def __init__(self, first_column_name, delta_K, *args, **kwargs):
        # make sure no one uses it
        if self.__class__.__name__ == 'SurfaceCorrelationObservable':
            raise NotImplementedError

        self.delta_K = np.array(delta_K.T)

        self._convert = kwargs.pop('convert', True)
        if self._convert:
            self.delta_K /= units.ANGSTROM_TO_AU

        self._kinfo = get_kinfo(self.delta_K)

        self.scattering_amplitudes_file = kwargs.pop('scattering_amplitudes_file', None)
        self.scattering_amplitudes = kwargs.pop('scattering_amplitudes', None)
        self.times = kwargs.pop('times', None)

        self._normalize = kwargs.pop('normalize', False)

        self.Naccumulated = 0
        self.accumulated_data = None

        self.autocorrelation = kwargs.pop('autocorrelation', 'linear').lower()
        self.optimize_length = kwargs.pop('optimize_length', False)

        kwargs['column_names'] = [first_column_name]
        kwargs['column_names'] += ['dK-vector {0}'.format(i)
                                     for i in range(1, delta_K.shape[0] + 1)
                                  ]

        PostProcessingObservable.__init__(self, *args, **kwargs)

        self._name = 'SurfaceCorrelationObservable'

        self._accumulation_quantity = 'N.A.'

        # default to the coherent case
        self._coherent = True


    def get_specific_info(self):
        """
        Return the observable specific information
        """
        info = self._name
        if self.accumulated_data is None:
            if self.scattering_amplitudes_file is not None:
                info += '\n\tusing scattering amplitudes from :\n\t\t{}'.format(self.scattering_amplitudes_file)
            else:
                info += '\n\tusing scattering amplitudes of a single run'
        else:
            info += '\n\tnumber of runs accumulated : {}'.format(self.Naccumulated)
            info += '\n\taccumulation based on : {}'.format(self._accumulation_quantity)
        info += '\n\tautocorrelation : {}'.format(self.autocorrelation)
        info += '\n\toptimized length : {}'.format(self.optimize_length)
        if self._kinfo:
            info += '\n' + self._kinfo
        return info


    def _get_args(self,
                  times=None,
                  scattering_amplitudes=None,
                  scattering_amplitudes_file=None,
                  coherent=True):
        """
        Interface to obtain the arguments for an isf evaluation either from
        file or directly passed.

        Parameters
        ----------
        scattering_amplitudes_file : str, optional (default=None)
            The location of the scattering amplitudes file that is to be read for
            the post processing. Note that you either have to pass the latter or
            *both* "times" and "scattering_amplitudes".

        scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
            Complexe valued array containing the scattering amplitudes.

        times : (Ntimes,) array, optional (default=None)
            Real-valued array containing the corresponding times.

        coherent : boolean, optional (default=True)
            Whether to bother with coherent or incoherent scattering. Basically
            this just affects how to read from a given file.

        Returns
        -------
        times : (Ntimes,) array
            The times corresponding to the scattering amplitudes.

        scattering_amplitudes : (Ntimes, Natoms, Nkpts) or (Ntimes, Nkpts) array
            The scattering amplitudes. For coherent scattering there is no
            dimension for individual atoms as the amplitudes are already
            averaged.
        """
        if (times is None and scattering_amplitudes is None):
            if scattering_amplitudes_file is None:
                msg = 'You have to pass either "time" and "scattering_amplitudes"'
                msg += '\nor "scattering_amplitudes_file"'
                raise ValueError(msg)
            else:
                if coherent:
                    from MDsim.analysis.isf import read_file_ScatteringAmplitude
                    times, scattering_amplitudes = read_file_ScatteringAmplitude(scattering_amplitudes_file)
                else:
                    from MDsim.analysis.isf import read_file_IncoherentScatteringAmplitude
                    times, scattering_amplitudes = read_file_IncoherentScatteringAmplitude(scattering_amplitudes_file,
                                                                                           Natoms=self.Natoms)

        elif scattering_amplitudes_file is None:
            if (times is None or scattering_amplitudes is None):
                msg = 'You have to pass either "time" *and* "scattering_amplitudes"'
                msg += '\nor "scattering_amplitudes_file"'
                raise ValueError(msg)
            else:
                pass
        else:
            msg = 'You cannot pass all three of "time", "scattering_amplitudes"'
            msg += '\nand "scattering_amplitudes_file"'
            raise ValueError(msg)

        return times, scattering_amplitudes

    def get_Naccumulated(self):
        return self.Naccumulated
