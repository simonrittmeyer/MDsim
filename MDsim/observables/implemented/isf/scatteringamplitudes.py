# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Scattering amplitudes, foundation for any surface correlation simulation
"""

import numpy as np

from MDsim import units

from MDsim.observables import GenericObservable
from MDsim.observables.implemented.isf.tools import get_kinfo

from MDsim.analysis.isf import calc_scattering_amplitudes
from MDsim.analysis.isf import calc_incoherent_scattering_amplitudes

class ScatteringAmplitudes(GenericObservable):
    """
    The scattering amplitudes as defined via

        A(dK, t) = sum_k(exp(-i * pos_k .dot. dK))

    These can be evaluated on-the-fly.

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes shall be
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected. Note that for the actual
        evaluation we ask for system positions in a.u.!
    """

    name = 'scattering_amplitudes'
    GenericObservable.register(name)

    def __init__(self, delta_K, *args, **kwargs):
        # Delta K are in au!


        # well, we expect delta_K as (Nkpts, Ndim) but the further routines
        # require the transpose. Do it only once at this position.
        self.delta_K = np.array(delta_K.T)

        self._convert = kwargs.pop('convert', True)

        if self._convert:
            self.delta_K = self.delta_K / units.ANGSTROM_TO_AU

        kwargs['column_names'] = ['{0}(dK-vector {1})'.format(j,i)
                                    for i in range(0, self.delta_K.shape[1])
                                    for j in ['Re', 'Im']]
        GenericObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        # we go for the unconverted positions here, this saves unncessary multiplications
        _A = calc_scattering_amplitudes(positions = self.system.get_positions(_au=True),
                                        delta_K = self.delta_K)

        amplitudes = np.empty(_A.shape[0]*2)
        amplitudes[0::2] = _A.real
        amplitudes[1::2] = _A.imag

        return amplitudes


    def get_specific_info(self):
        return get_kinfo(self.delta_K)

    @staticmethod
    def get_implemented():
        """
        Return the name of the observable only
        """
        return ['scattering_amplitudes']


class IncoherentScatteringAmplitudes(GenericObservable):
    """
    The incoherent scattering amplitudes as defined via

        A_k(dK, t) = exp(-i * pos_k .dot. dK)

    These can be evaluated on-the-fly.

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes shall be
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u.

    system : MDsim system instance
        The system which shall be observed.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected. Note that for the actual
        evaluation we ask for system positions in a.u.!
    """

    name = 'incoherent_scattering_amplitudes'

    GenericObservable.register(name)

    def __init__(self, delta_K, *args, **kwargs):
        # Delta K are in au!

        # well, we expect delta_K as (Nkpts, Ndim) but the further routines
        # require the transpose. Do it only once at this position.
        self.delta_K = np.array(delta_K.T)

        self._convert = kwargs.pop('convert', True)

        if self._convert:
            self.delta_K = self.delta_K / units.ANGSTROM_TO_AU

        kwargs['column_names'] = []

        GenericObservable.__init__(self, *args, **kwargs)

        # time is always of the column names
        column_names = ['times'] + ['atom_{0} -- {1}(dK-vector {2})'.format(n,j,i)
                                    for n in range(self.Natoms)
                                    for i in range(0, self.delta_K.shape[1])
                                    for j in ['Re', 'Im']
                                    ]

        # write that new stuff conveniently to member variables
        self.__dict__.update(self._init_columns(column_names=column_names))


    def get_observable_values(self):
        # we go for the unconverted positions here, this saves unncessary multiplications
        _A = calc_incoherent_scattering_amplitudes(positions = self.system.get_positions(_au=True),
                                                   delta_K = self.delta_K)

        amplitudes = np.empty((_A.shape[0], _A.shape[1]*2))
        amplitudes[:,0::2] = _A.real
        amplitudes[:,1::2] = _A.imag

        return amplitudes.ravel()


    def get_specific_info(self):
        return get_kinfo(self.delta_K)

    @staticmethod
    def get_implemented():
        """
        Return the name of the observable only
        """
        return ['incoherent_scattering_amplitudes']
