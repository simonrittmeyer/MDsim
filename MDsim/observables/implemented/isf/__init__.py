# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements observables required to study surface diffusion.
"""
from __future__ import print_function

from MDsim.observables.implemented.isf.tools import get_kinfo

from MDsim.observables.implemented.isf.scatteringamplitudes import ScatteringAmplitudes
from MDsim.observables.implemented.isf.scatteringamplitudes import IncoherentScatteringAmplitudes

from MDsim.observables.implemented.isf.surfacecorrelation import SurfaceCorrelationObservable

from MDsim.observables.implemented.isf.isf import Isf
from MDsim.observables.implemented.isf.isf import IncoherentIsf

from MDsim.observables.implemented.isf.dsf import Dsf
from MDsim.observables.implemented.isf.dsf import IncoherentDsf

from MDsim.observables.implemented.isf.isfviadsf import IsfViaDsf
from MDsim.observables.implemented.isf.isfviadsf import IncoherentIsfViaDsf

# lists for inclusions
require_delta_K = ScatteringAmplitudes.get_implemented()\
                   + IncoherentScatteringAmplitudes.get_implemented()\
                   + SurfaceCorrelationObservable.get_implemented()

postprocessing = SurfaceCorrelationObservable.get_implemented()
require_fft = SurfaceCorrelationObservable.get_implemented()
require_scattering_amplitudes = SurfaceCorrelationObservable.get_implemented()


