# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements observables required to study surface diffusion.
"""
from __future__ import print_function

import numpy as np

from MDsim import units

from MDsim.observables.implemented.isf.surfacecorrelation import SurfaceCorrelationObservable

from MDsim.analysis.isf import calc_dsf
from MDsim.analysis.isf import calc_incoherent_dsf

class Dsf(SurfaceCorrelationObservable):
    """
    Class to evaluate a single DSF directly from scattering amplitudes, or to
    accumulate many of them. If run in accumulation mode, you do not need to
    pass any of "times", "scattering_amplitudes" or
    "scattering_amplitudes_file" upon initialization. In single run mode, you
    need either "times" and "scattering_amplitudes" or
    "scattering_amplitudes_file".

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """
    name = 'dsf'
    SurfaceCorrelationObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['first_column_name'] = 'omega'
        super(Dsf, self).__init__(*args, **kwargs)
        self._name = 'Dynamical Structure Factor (DSF)'
        self._accumulation_quantity = 'dsf'
        if self.autocorrelation == 'cyclic':
            self.zero_padding = False
        else:
            self.zero_padding = True

        self._coherent = True


    def _evaluate_dsf(self, times=None,
                      scattering_amplitudes=None,
                      scattering_amplitudes_file=None):
        """
        Wrapper-routine to evaluate a DSF, based either on an times and
        scattering_amplitudes array, or a respective file.

        Times must come in FS!
        """

        self.times, scattering_amplitudes = self._get_args(times,
                                                           scattering_amplitudes,
                                                           scattering_amplitudes_file)

        if self._coherent:
            freqs, dsf = calc_dsf(self.times * units.FS_TO_AU,
                                  scattering_amplitudes,
                                  optimize_length=self.optimize_length,
                                  zero_padding=self.zero_padding,
                                  verbose=False)
        else:
            freqs, dsf = calc_incoherent_dsf(self.times * units.FS_TO_AU,
                                             scattering_amplitudes,
                                             optimize_length=self.optimize_length,
                                             zero_padding=self.zero_padding,
                                             verbose=False)
        dsf = dsf.real
        freqs *= units.AU_TO_EV
        return freqs, dsf


    def get_all_data(self, sort=True):
        """
        Return all data at once, sorted anyway. Either we calculate on the fly
        or return the accumulated data.
        """
        if self.accumulated_data is None:
            freqs, dsf = self._evaluate_dsf(scattering_amplitudes=self.scattering_amplitudes,
                                            times=self.times,
                                            scattering_amplitudes_file=self.scattering_amplitudes_file)

            data = np.c_[freqs, dsf]

        else:
            data = self.accumulated_data
            data[:,1::] /= self.Naccumulated

        if sort:
            data = data[data[:,0].argsort()]

        return data


    def add(self, *args, **kwargs):
        """
        Add a run to accumulate data. Pass either "scattering_amplitudes_file"
        or *BOTH* "scattering_amplitudes" and "times". A succesful DSF will be
        accumulated and self.Naccumulated will be raised by one.

        Parameters
        ----------
        scattering_amplitudes_file : str, optional (default=None)
            The location of the scattering amplitudes file that is to be read for
            the post processing. Note that you either have to pass the latter or
            *both* "times" and "scattering_amplitudes".

        scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
            Complexe valued array containing the scattering amplitudes.

        times : (Ntimes,) array, optional (default=None)
            Real-valued array containing the corresponding times..

        Returns
        -------
        <True> if a valid DSF could be evaluated, <False> else.
        """
        freqs, dsf = self._evaluate_dsf(*args, **kwargs)

        if self.accumulated_data is None:
            self.accumulated_data = np.c_[freqs, dsf]
        else:
            # no need to accumulate the time
            self.accumulated_data[:,1:] += dsf

        self.Naccumulated +=1

        return True


class IncoherentDsf(Dsf):
    """
    Class to evaluate a single incoherent DSF directly from incoheren
    scattering amplitudes, or to accumulate many of them. If run in
    accumulation mode, you do not need to pass any of "times",
    "scattering_amplitudes" or "scattering_amplitudes_file" upon
    initialization. In single run mode, you need either "times" and
    "scattering_amplitudes" or "scattering_amplitudes_file".

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """
    name = 'incoherent_dsf'
    SurfaceCorrelationObservable.register(name)

    def __init__(self, *args, **kwargs):
        Dsf.__init__(self, *args, **kwargs)
        self._name = 'Incoherent Dynamical Structure Factor (DSF)'
        self._coherent = False
