# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements observables required to study surface diffusion.
"""
from __future__ import print_function

import numpy as np

from MDsim import units

from MDsim.observables.implemented.isf.surfacecorrelation import SurfaceCorrelationObservable

from MDsim.analysis.isf import calc_isf
from MDsim.analysis.isf import calc_incoherent_isf

class Isf(SurfaceCorrelationObservable):
    """
    Class to evaluate a single ISF directly from scattering amplitudes, or to
    accumulate many of them. If run in accumulation mode, you do not need to
    pass any of "times", "scattering_amplitudes" or
    "scattering_amplitudes_file" upon initialization. In single run mode, you
    need either "times" and "scattering_amplitudes" or
    "scattering_amplitudes_file".

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    normalize : boolean, optional (default=False)
        Normalize the result for each k-point. Only choose this option if you
        are sure that normalization at this point is appropriate.

    zero_padding : bool, optinal (default=False)
        Whether we should zero pad for the FFT or not.

    optimize_length : bool, optinal (default=False)
        Truncate the input lenght to allow for efficient FFT (Radix2).

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """
    name = 'isf'
    SurfaceCorrelationObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['first_column_name'] = 'time'
        SurfaceCorrelationObservable.__init__(self, *args, **kwargs)

        self.coherent = True
        self._name = 'Intermediate Scattering Function (ISF)'
        self._accumulation_quantity = 'isf'


    # functionality to add a run in accumulation mode
    def add(self, *args, **kwargs):
        """
        Add a run to accumulate data. Pass either "scattering_amplitudes_file"
        or *BOTH* "scattering_amplitudes" and "times". A succesful ISF will be
        accumulated and self.Naccumulated will be raised by one.

        Parameters
        ----------
        scattering_amplitudes_file : str, optional (default=None)
            The location of the scattering amplitudes file that is to be read for
            the post processing. Note that you either have to pass the latter or
            *both* "times" and "scattering_amplitudes".

        scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
            Complexe valued array containing the scattering amplitudes.

        times : (Ntimes,) array, optional (default=None)
            Real-valued array containing the corresponding times..

        Returns
        -------
        <True> if a valid ISF could be evaluated, <False> else.
        """

        kwargs['coherent'] = self.coherent

        times, isf = self._evaluate_isf(*args, **kwargs)

        # if np.any(np.isnan(isf)) or isf[1,1] < 0.9:
            # # seems corrupt...
            # return False

        if self.accumulated_data is None:
            self.accumulated_data = np.c_[times, isf]
        else:
            # no need to accumulate the time
            self.accumulated_data[:,1:] += isf
        # why did I do that??? Each one is individually normalized, that is it!
        #    self.accumulated_data[:,1:] /= np.max(self.accumulated_data[:,1:], axis = 0)

        self.Naccumulated +=1

        return True


    def _evaluate_isf(self,
                      times=None,
                      scattering_amplitudes=None,
                      scattering_amplitudes_file=None,
                      coherent=True):
        """
        Wrapper-routine to evaluate an ISF, based either on an times and
        scattering_amplitudes array, or a respective file.

        Parameters
        ----------
        scattering_amplitudes_file : str, optional (default=None)
            The location of the scattering amplitudes file that is to be read for
            the post processing. Note that you either have to pass the latter or
            *both* "times" and "scattering_amplitudes".

        scattering_amplitudes : (Ntimes, Nkpts) array, optional (default=None)
            Complexe valued array containing the scattering amplitudes.

        times : (Ntimes,) array, optional (default=None)
            Real-valued array containing the corresponding times.

        coherent : boolean, optional (default=True)
            Whether we evaluate a coherent ISF or not. The difference is that a
            coherent ISF invokes already averaged scattering amplitudes while
            an incoherent ISF just averages the individual intensities.

        Returns
        -------
        times : (N,) array
            Appropriate times array. Lenght depends on zero_padding and
            optimized length for FFT.

        isf : (N, Nkpts) array
            Real part of the ISF. Length again depends on zero_padding and
            optimize_length.
        """

        times, scattering_amplitudes = self._get_args(times=times,
                                                          scattering_amplitudes=scattering_amplitudes,
                                                          scattering_amplitudes_file=scattering_amplitudes_file,
                                                          coherent=coherent)
        # normalization first comes with get_all_data()
        if coherent:
            times_isf, isf = calc_isf(times,
                                      scattering_amplitudes,
                                      optimize_length=self.optimize_length,
                                      autocorrelation=self.autocorrelation,
                                      verbose=False,
                                      normalize=False)
        else:
            times_isf, isf = calc_incoherent_isf(times,
                                                 scattering_amplitudes,
                                                 optimize_length=self.optimize_length,
                                                 autocorrelation=self.autocorrelation,
                                                 verbose=False,
                                                 normalize=False)

        isf = isf.real
        return times_isf, isf


    def get_all_data(self, sort=True):
        """
        Return all data at once, sorted anyway. Either we calculate on the fly
        or return the accumulated data.
        """
        if self.accumulated_data is None:
            times, isf = self._evaluate_isf(scattering_amplitudes=self.scattering_amplitudes,
                                            times=self.times,
                                            scattering_amplitudes_file=self.scattering_amplitudes_file,
                                            coherent=self.coherent)

            data = np.c_[times, isf]

        else:
            data = self.accumulated_data
            # do the averaging, ie. divide by number of runs
            data[:,1::] /= self.Naccumulated

        if sort:
            data = data[data[:,0].argsort()]

        # let's normalize at this particular point to avoid averaging issues
        if self._normalize:
            norm=np.max(data[:,1::], axis=0)
            data[:,1::] /= norm[None,:]

        return data


    def get_Naccumulated(self):
        return self.Naccumulated


    def set_data(self, data, Naccumulated=1, accumulation_quantity=None):
        """
        Set data from the outside.
        """
        self.accumulated_data = data
        self.Naccumulated = Naccumulated
        if accumulation_quantity is not None:
            self._accumulation_quantity = accumulation_quantity



class IncoherentIsf(Isf):
    """
    Class to evaluate a single *incoherent* ISF directly from scattering
    amplitudes, or to accumulate many of them. If run in accumulation mode, you
    do not need to pass any of "times", "scattering_amplitudes" or
    "scattering_amplitudes_file" upon initialization. In single run mode, you
    need either "times" and "scattering_amplitudes" or
    "scattering_amplitudes_file".

    Initialization
    --------------
    delta_K : (Nkpts, Ndim) array
        The delta_K values for which the scattering amplitudes where
        calculated. Note that input is expected in inverse Angstrom if
        convert=<True>, else in a.u. This array will no longer be actively
        used, however, information will be drawn therefrom.

    system : MDsim system instance
        The system which shall be observed.

    scattering_amplitudes_file : str, optional (default=None)
        The location of the scattering amplitudes file that is to be read for
        the post processing. Note that you either have to pass the latter or
        *both* "times" and "scattering_amplitudes".

    scattering_amplitudes : (Ntimes, Natoms, Nkpts) array, optional (default=None)
        Complexe valued array containing the scattering amplitudes.

    times : (Ntimes,) array, optional (default=None)
        Real-valued array containing the corresponding times..

    normalize : boolean, optional (default=False)
        Normalize the result for each k-point. Only choose this option if you
        are sure that normalization at this point is appropriate.

    zero_padding : bool, optinal (default=False)
        Whether we should zero pad for the FFT or not.

    optimize_length : bool, optinal (default=False)
        Truncate the input lenght to allow for efficient FFT (Radix2).

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.

    convert : boolean, optional (default=True)
        Convert the input delta_K values from inverse Angstrom to a.u.. If
        <False> then input in a.u. is expected.
    """
    name = 'incoherent_isf'
    SurfaceCorrelationObservable.register(name)

    def __init__(self, *args, **kwargs):
        Isf.__init__(self, *args, **kwargs)
        self._name = 'Incoherent Intermediate Scattering Function (ISF)'
        self.coherent = False

