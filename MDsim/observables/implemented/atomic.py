# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module that implements atomic observables.
"""

import numpy as np

from MDsim import units

from MDsim.observables import AtomicObservable

# ------------------
# atomic observables
# ------------------

class Position(AtomicObservable):
    # register the variable
    name = 'position'
    AtomicObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'pos'
        AtomicObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_positions()[self.iatom]


class Velocity(AtomicObservable):
    name = 'velocity'
    AtomicObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'vel'
        AtomicObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_velocities()[self.iatom]


class Force(AtomicObservable):
    name = 'force'
    AtomicObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'force'
        AtomicObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_forces()[self.iatom]


class Eta(AtomicObservable):
    name = 'eta'
    AtomicObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'eta'
        AtomicObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_etas()[self.iatom]


class Momentum(AtomicObservable):
    name = 'momentum'
    AtomicObservable.register(name)

    def __init__(self, *args, **kwargs):

        kwargs['prefix'] = 'momentum'
        AtomicObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_momenta()[self.iatom]


require_iatom = AtomicObservable.get_implemented()
