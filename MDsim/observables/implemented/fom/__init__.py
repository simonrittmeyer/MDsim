# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim import units

from MDsim.observables import AllAtomObservable


# this is the one the logs permanently

class FomIntegrand(AllAtomObservable):
    # ------------------------------------------------------------------
    # this is the integrand of the Fourier transform within the forced
    # oscillator model (FOM) :
    #
    # A. C. Luntz, M. Persson and G. O. Sitz, J. Chem. Phys. 124, (2006).
    # http://www.dx.doi.org/10.1063/1.2177664
    # -------------------------------------------------------------------
    name = 'fom_integrand'
    AllAtomObservable.register(name)

    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'from_integrand'
        AllAtomObservable.__init__(self, *args, **kwargs)

    def get_observable_values(self):
        return self.system.get_velocities().ravel() * self.system.get_etas().ravel()

# here goes the post processing variable that does all the FFT magic
