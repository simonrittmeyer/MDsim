# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import re

def check_observable_name(name):
    """
    Check whether an obervable name is ok or not.
    """
    pattern = r'[a-zA-z]+[\s\w]*'
    if not re.match(pattern, name):
        raise ValueError('Invalid observable name "{}"'.format(name))
    else:
        return True


class _Observable(object):
    """
    This is just a common basis! Do not use it, but rather go for the derived
    subclasses

    Initialization
    --------------
    system : MDsim system instance
        The system we want to observe.

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    name = 'n.a'

    def __init__(self, system,
                 min_width = 20,
                 min_spacing = 2,
                 fmt = '{}.9E'):

        # save the system and important properties
        self.system = system
        self.Natoms = system.get_Natoms()
        self.Ndim = system.get_Ndim()

        # formatting flags
        self._min_width = min_width
        self._min_spacing = min_spacing
        self._fmt = fmt

    def __str__(self):
        return 'Observable "{}"'.format(self.name)


    # helpers
    def _get_column_widths(self, column_names, min_width = 20, min_spacing = 2):
        """
        Routine that determines the optimal column widths. A minimum width can
        be specified as well as the minimal spacing between different columns.

        Parameters
        ----------
        column_names : list of strings
            The column names.

        min_width : integer, optional (default = 20)
            Minimum column width.

        min_spacing : integer, optinal (default = 2)
            Minimum whitespace-spacing between columns in the string.

        Returns
        -------
        column_widths : list of integers
            The optimal column widths.
        """

        column_widths = []
        for n in column_names:
            if len(n) < (min_width - min_spacing):
                column_widths.append(min_width)
            else:
                column_widths.append(len(n) + min_spacing)
        return column_widths


    def _format_headline(self, column_names, column_widths):
        """
        Function that returns a readily formatted headline.

        Parameters
        ----------
        column_names : list of strings
            List of properly formatted column names.

        column_widths : list of integers
            The column widths.

        Returns
        -------
        head : string
            Properly formatted headline incl. lines above and below the column names.
        """

        head = ''
        for i, name in enumerate(column_names):
            head += '{{:>{:d}s}}'.format(column_widths[i]).format(name)

        limiter = '-'*(len(head))

        head = limiter + '\n' + head + '\n' + limiter

        return head


    def _get_data_template(self, column_widths, min_spacing = 2, fmt = '{}.9E'):
        """
        This function creates a template which then needs to be filled with the
        actual observables. We do this, as it can be done once and for all.
        Hence we do not need many many string concatenation operations which are
        extremely slow.

        Parameters
        ----------
        column_widths : list of integers
            The column widths. See "_get_column_widths()".

        min_spacing : integer
            The minimal number of white space between columns.

        fmt : string, optional (default = "{}.9E")
            The formatting. Note that the total number of digits incl. comma
            will be <col_widths> - <min_spacing>.

        Returns
        -------
        template : string
            Ready to use template.
        """
        template = ''

        for i, w in enumerate(column_widths):
            template += ' '*min_spacing + '{{:>{:s}}}'.format(fmt.format(w-min_spacing))

        return template


    def _init_columns(self, column_names,
                      min_width=20,
                      min_spacing=2,
                      fmt='{}.9E'):
        """
        Initialize columns, ie. write on system properties once you have the
        column_names... then we have everything!

        This is basically just a wrapping routine. But we put it in a separate
        function such that derived classes can call it in their constructor.

        Parameters
        ----------
        column_names : list of strings
            List of properly formatted column names. The formatting is
            something that has be to implemented by subclasses.

        min_width : integer, optional (default = 20)
            Minimum column width.

        min_spacing : integer
            The minimal number of white space between columns.

        fmt : string, optional (default = "{}.9E")
            The formatting. Note that the total number of digits incl. comma
            will be <col_widths> - <min_spacing>.

        Returns
        -------
        init_dict : dictionary
            Dictionary with the following keys:

            "_column_widths" : list of integers
                The column widths.

            "_headline" : string
                The properly formatted headline.

            "_data_template" : string
                The data template

            "_column_names" : List of strings
                The column names as given as input. Returned for convenience
                only.

            "_Ncolumns" : integer
                The number of columns. Crucial for the netcdf support (we need
                to know the dimensions).

            You should use this dictionary to update the instance __dict__.
        """
        column_widths = self._get_column_widths(column_names=column_names,
                                               min_width=min_width,
                                               min_spacing=min_spacing)

        headline = self._format_headline(column_names=column_names,
                                               column_widths=column_widths)

        data_template = self._get_data_template(column_widths=column_widths,
                                                min_spacing=min_spacing,
                                                fmt=fmt)

        return {'_column_widths' : column_widths,
                '_headline' : headline,
                '_data_template' : data_template,
                '_column_names' : column_names,
                '_Ncolumns' : len(column_names)}

    def get_headline(self):
        """
        Return the readily formatted headline.
        """
        return self._headline

    def get_dataline(self):
        """
        Obtain the readily formatted dataline corresponding to the current
        state of the system.
        """
        return self._data_template.format(self.system.get_time(),
                                          *self.get_observable_values())


    def get_observable_values(self):
        """
        Function that has to be implemented by final observables. Cannot be abstracted.
        """
        raise NotImplementedError

    def get_Ncolumns(self):
        """
        Return the number of columns - crucial for netCDF support
        """
        return self._Ncolumns

    def get_column_names(self):
        """
        Return column names, crucial for netCDF support
        """
        return self._column_names

    def get_idstring(self):
        """
        Function that returns an observable id, which can be nicely used by
        filehandlers to identify files.
        """
        return re.sub('\s', '_', self.name.strip())

    def get_specific_info(self):
        return ""

    def get_all_data(self):
        """
        Return all data at once
        """
        msg = 'This is not a PostProcessingObservable!'
        raise AttributeError(msg)


    def get_all_datalines(self):
        """
        Return all data strings at once
        """
        msg = 'This is not a PostProcessingObservable!'
        raise AttributeError(msg)

class GenericObservable(_Observable):
    """
    Base class for generic observables. This can be anything. For more
    specialized observables that basically just return a positions array or
    such, see AllAtomsObservables or AtomicObservable.

    Note that this base class carries a list of all derived subclasses. This
    way, we can conveniently use the static method "get_implemented()" to
    obtain a list of all implemented observables. To register to this list, a
    subclass has to call "register()" once (not in constructor but on
    class-level).

    Note moreover that sublasses do have to implement "get_observable_values()"
    TIME IS AUTOMATICALLY ADDED, don't do it in this routine!

    Initialization
    --------------
    system : MDsim system instance
        The system which shall be observed.

    column_names : list of strings
        Your column names. "time" will be automatically added, so do not include
        it here!

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    _implemented = []

    @staticmethod
    def register(name):
        """
        Register a new derived subclass to the common list of implemented
        methods.

        Parameters
        ----------
        name : string
            The name of the observable.
        """
        if check_observable_name(name):
            GenericObservable._implemented.append(name)

    @staticmethod
    def get_implemented():
        """
        Return the list of implemented observables, ie. constructed subclasses.
        """
        return GenericObservable._implemented

    def __init__(self, system, column_names, **kwargs):
        _Observable.__init__(self,
                             system=system,
                             **kwargs)

        # time is always of the column names
        column_names = ['times'] + column_names

        # write that new stuff conveniently to member variables
        self.__dict__.update(self._init_columns(column_names=column_names))


class AtomicObservable(_Observable):
    """
    Base class for atomic observables, eg. the postion of a single atom.

    Note that this base class carries a list of all derived subclasses. This
    way, we can conveniently use the static method "get_implemented()" to
    obtain a list of all implemented observables. To register to this list, a
    subclass has to call "register()" once (not in constructor but on
    class-level).

    Note moreover that sublasses do have to implement "get_observable_values()"
    TIME IS AUTOMATICALLY ADDED, don't do it in this routine!

    Initialization
    --------------
    system : MDsim system instance
        The system which shall be observed.

    iatom : integer
        The ID of the atom you want to observe.

    prefix : string
        The prefix of the observable that will appear in the columns.
        Formatting will be <prefix>_(<iatom>,<x,y,z>).

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """

    _implemented = []

    @staticmethod
    def register(name):
        """
        Register a new derived subclass to the common list of implemented
        methods.

        Parameters
        ----------
        name : string
            The name of the observable.
        """
        if check_observable_name(name):
            AtomicObservable._implemented.append(name)

    @staticmethod
    def get_implemented():
        """
        Return the list of implemented observables, ie. constructed subclasses.
        """
        return AtomicObservable._implemented

    def __init__(self, system, iatom, prefix, **kwargs):
        _Observable.__init__(self,
                             system=system,
                             **kwargs)

        self.iatom = iatom
        self.prefix = prefix

        column_names = self._get_column_names(prefix=self.prefix,
                                              iatom=self.iatom,
                                              Ndim=self.Ndim)

        # write that new stuff conveniently to member variables
        self.__dict__.update(self._init_columns(column_names=column_names,
                                                min_width=self._min_width,
                                                min_spacing=self._min_spacing,
                                                fmt=self._fmt))

    def _get_column_names(self, prefix, iatom, Ndim):
        """
        Routine that creates column names based on a prefix.
        Formatting will be <prefix>_(<iatom>,<x,y,z>).

        Parameters
        ----------
        prefix : string
            The prefix of the observable that will appear in the columns.
            Formatting will be <prefix>_(<iatom>,<x,y,z>).

        iatom : integer
            The ID of the atom to be observed.

        Ndim : integer
            Cartesian dimensionality of the system.

        Returns
        -------
        column_names : list of strings
            The column names
        """
        column_names  = ['time']
        column_names += ['{}_({},{})'.format(prefix, iatom, ['x','y','z'][i]) for i in range(Ndim)]
        return column_names

    def get_idstring(self):
        """
        Function that returns an observable id, which can be nicely used by
        filehandlers to identify files.
        """
        return '{}-atom-{}'.format(self.name, self.iatom)


class AllAtomObservable(_Observable):
    """
    Base class for all-atom observables, eg. all postions in the system. Note
    that dumping everything into a single file may be problematic for larger
    systems, as the number of columns will be (Natoms * Ndim + 1).

    Note that this base class carries a list of all derived subclasses. This
    way, we can conveniently use the static method "get_implemented()" to
    obtain a list of all implemented observables. To register to this list, a
    subclass has to call "register()" once (not in constructor but on
    class-level).

    Note moreover that sublasses do have to implement "get_observable_values()"
    TIME IS AUTOMATICALLY ADDED, don't do it in this routine!

    Initialization
    --------------
    system : MDsim system instance
        The system which shall be observed.

    prefix : string
        The prefix of the observable that will appear in the columns.
        Formatting will be <prefix>_(<iatom>,<x,y,z>).

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """
    _implemented = []

    @staticmethod
    def register(name):
        """
        Register a new derived subclass to the common list of implemented
        methods.

        Parameters
        ----------
        name : string
            The name of the observable.
        """
        if check_observable_name(name):
            AllAtomObservable._implemented.append(name)

    @staticmethod
    def get_implemented():
        """
        Return the list of implemented observables, ie. constructed subclasses.
        """
        return AllAtomObservable._implemented

    def __init__(self, system, prefix, **kwargs):
        _Observable.__init__(self,
                             system=system,
                             **kwargs)

        self.prefix = prefix

        column_names = self._get_column_names(prefix=self.prefix,
                                              Ndim=self.Ndim)

        # write that new stuff conveniently to member variables
        self.__dict__.update(self._init_columns(column_names=column_names,
                                                min_width=self._min_width,
                                                min_spacing=self._min_spacing,
                                                fmt=self._fmt))
        self.Ndof = self.system.get_Ndof()

    def _get_column_names(self, prefix, Ndim):
        """
        Routine that creates column names based on a prefix.
        Formatting will be <prefix>_(<iatom>,<x,y,z>).

        Parameters
        ----------
        prefix : string
            The prefix of the observable that will appear in the columns.
            Formatting will be <prefix>_(<iatom>,<x,y,z>).

        Ndim : integer
            Cartesian dimensionality of the system.

        Returns
        -------
        column_names : list of strings
            The column names
        """
        column_names  = ['time']
        for iatom in range(self.Natoms):
            column_names += ['{}_({},{})'.format(prefix, iatom, ['x','y','z'][i]) for i in range(Ndim)]
        return column_names


class PostProcessingObservable(_Observable):
    """
    Base class of observables that are only accessable after the simulation.

    Initialization
    --------------
    system : MDsim system instance
        The system which shall be observed.

    column_names : list of strings
        Your column names. "time" will *NOT* be automatically added, so do
        include it here!

    min_width : integer, optional (default = 20)
        The minimal width of the respective columns in the formatted string
        output.

    min_spacing : integer, optional (default = 2)
        The minimal whitespacing between to adjacent columns.

    fmt : string, optional (default = "{}.9E")
        The format for the floating point numbers. Note that you only have to
        specify the decimal numbers, the total length of the number is
        determined by som e column_width-routines, which is why we need the
        empty formatter.
    """

    _implemented = []

    @staticmethod
    def register(name):
        """
        Register a new derived subclass to the common list of implemented
        methods.

        Parameters
        ----------
        name : string
            The name of the observable.
        """
        if check_observable_name(name):
            PostProcessingObservable._implemented.append(name)

    @staticmethod
    def get_implemented():
        """
        Return the list of implemented observables, ie. constructed subclasses.
        """
        return PostProcessingObservable._implemented

    def __init__(self, system, column_names, **kwargs):
        _Observable.__init__(self,
                             system=system,
                             **kwargs)

        # time is always of the column names
        column_names = column_names

        # write that new stuff conveniently to member variables
        self.__dict__.update(self._init_columns(column_names=column_names))

    def get_dataline(self):
        raise AttributeError

    def get_observable_values(self):
        raise AttributeError

    def get_all_data(self, sort=True):
        """
        Return all data at once. Sort flag is necessary since the results of
        Fourier transforms are not necessarily ordered as we would expect.
        """
        raise NotImplementedError

    def get_all_datalines(self):
        """
        Return all data strings at once
        """
        return [self._data_template.format(*data)
                for data in self.get_all_data(sort=True)]


# convenience imports
from implemented import implemented as implemented_observables
from implemented import require_iatom
from implemented import require_delta_K
from implemented import postprocessing
