# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import time
import numpy as np
import warnings

from scipy.optimize import curve_fit
from scipy.optimize import minimize


def fit_generic(x, y, fitfunc, p0,
                 method='Nelder-Mead',
                 measure = 'absolute',
                 show = False,
                 _verbose=False,
                 **kwargs):
    """
    Fit a generic function by minimizing the measure

        M_absolute(param) = sum((y - fitfunc(x, param))**2),

    or

        M_relative(param) = sum((y - fitfunc(x, param)/y)**2).

    This is hence a least (relative) square optimization yet without using the
    unflexible Levenberg Marquardt algorithm. Can be as well used to do
    non-linear regression.


    Parameters
    ----------
    x : (N,) array-like
        Input x data.

    y : (N,) array-like
        Input x data.

    fitfunc : function
        Target function that is callable via fitfunc(x, *param).

    p0 : (Nparam,) array-like
        Initial parameter guess.

    method : string, optional (default = "Nelder-Mead")
        Any of scipy's minimizing routines. Have a look at
        scipy.optimize.minimize. Shortened excerpt:

        "Nelder-Mead" uses the Simplex algorithm.

        "Powell" is a modification of Powell's method which is a conjugate
            direction method.

        "CG" uses a nonlinear conjugate gradient algorithm.

        "BFGS" uses the quasi-Newton method of Broyden, Fletcher, Goldfarb, and
            Shanno (BFGS).

        "Newton-CG" uses a Newton-CG algorithm (also known as the truncated
            Newton method).

        "Anneal" uses simulated annealing, which is a probabilistic
            metaheuristic algorithm for global optimization.

    measure : string, optional ({*"absolute"*, "relative"})
        Defines the measure to be optimized. Either absolute or relative
        errors.

    show: boolean, optional (default = False)
        Visualize fit result.

    _verbose : boolean, optional (default = True)
        Print some information to stdout.

    **kwargs : Further arguments that will be directly passed to
        scipty.optimize.minimize()

    Returns
    -------
    opt_param : (Nparam,) array
        The optimized parameters.

    opt_fitfunc : function
        The readily-parametrized fit function

    res : OptimizeResult object
        The result as provided by scipy.optimize.minimize()
    """

    # this is our measure. We may want to add a second measure if the data is
    # heteroscedastic.
    if measure == 'absolute':
        def _errfunc(param, x, y):
            return np.sum((y - fitfunc(x, *param))**2)

    elif measure == 'relative':
        def _errfunc(param, x, y):
            return np.sum((y - fitfunc(x, *param)/y)**2)
    else:
        msg = 'Undefined measure "{}"'.format(measure)
        raise ValueError(msg)

    options = {'disp' : _verbose}

    if not 'options' in kwargs.keys():
        kwargs['options'] = options
    else:
        kwargs['options'].update(options)

    res = minimize(fun = _errfunc,
                   x0 = p0,
                   method = method,
                   args = (x, y),
                   **kwargs)

    if res['success']:
        opt_param = res['x']
    else:
        opt_param = np.ones_like(p0) * np.nan

    opt_fitfunc = lambda x : fitfunc(x, *opt_param)

    if show:
        import matplotlib.pyplot as plt
        ax = plt.figure().add_subplot(111)

        ax.plot(x,y, color = 'black', ls = '', marker = 'o', label = 'data points')
        ax.plot(x, opt_fitfunc(x), color = 'red', lw = 2, label = 'fit function')

        ax.set_xlabel('x')
        ax.set_ylabel('y')

        plt.show()

    return opt_param, opt_fitfunc, res


def fit_exponential(x, y, linearize=False, **kwargs):
    """
    Fit to

        y = p[0]*exp(-p[1] * x)

    if possible, linearize the model.
    """

    x = np.asarray(x)
    y = np.asarray(y)

    def _fitfunc(x, A, alpha):
        return A*np.exp(-alpha*x)

    if linearize:
        if np.any(y <= 0.):
            msg = 'Negative values make linearization impossible'
            raise ValueError(msg)

        # caution: These are the linear paramenters
        p, opt_fitfunc = fit_linear(x, np.log(y), **kwargs)

        # convert these parameters to the exponential ones:
        opt_p = [np.exp(p[0]), -p[1]]
        opt_fitfunc = lambda x: _fitfunc(x, *opt_p)

    else:
        # estimating parameters
        for i in range(len(y)):
            inv_alpha_guess = x[i]
            if y[i] < y[0]/np.e:
                break

        p0 = [y[0], 1./inv_alpha_guess]
        opt_p, opt_fitfunc, res = fit_generic(x, y, _fitfunc, p0, **kwargs)

    return opt_p, opt_fitfunc


def fit_linear(x, y, **kwargs):
    """
    Fit to

        y = p[0]*x + p[1]
    """
    x = np.asarray(x)
    y = np.asarray(y)

    def _fitfunc(x, A, alpha):
        return alpha*x + A

    # initial guess... assume we start from x=0
    p0 = [y[0], (y[-1]-y[0])/(x[-1]-x[0])]

    opt_p, opt_fitfunc, res = fit_generic(x, y, _fitfunc, p0, **kwargs)

    return opt_p, opt_fitfunc


def fit_exponential_plus_constant(x,y, **kwargs):
    """
    Fit to

        y = p[0]*exp(-p[1] * x) + p[2]
    """
    x = np.asarray(x)
    y = np.asarray(y)

    def _fitfunc(x, A, alpha, B):
        return A*np.exp(-alpha*x) + B

    # estimating parameters
    for i in range(len(y)):
        inv_alpha_guess = x[i]
        if y[i] < y[0]/np.e:
            break

    p0 = [y[0], 1./inv_alpha_guess, y[-1]]

    opt_p, opt_fitfunc, res = fit_generic(x, y, _fitfunc, p0, **kwargs)

    return opt_p, opt_fitfunc


def tune_cutoff(times,
                signal,
                fitfunc='exponential_plus_constant',
                max_cutoff=None,
                end=None,
                stepsize=1,
                logfile=None,
                show=False,
                verbose=False,
                ):
    """
    Function to tune the lower limit cutoff by monitoring the R^2 parameter,
    ie., the coefficient of determination. "end" is the maximum time that is
    considered.

    Parameters
    ----------
    times : (Ntimes,) array
        The time series corresponding to the signal.

    signal : (Ntimes,) array
        The corresponding signal.

    fitfunc : string, optional ({*'exponential_plus_constant*', 'exponential'})
        Defines the fit function to be used.

    max_cutoff : float, optional (default=None)
        The maximum value that is disregareded, ie. the maximum lower bound of
        the fitting data. Note that this is not the index but the time value.
        Defaults to end_idx-5, i.e. at least 5 datapoints will constitue the
        fit.

    end : float, optinal (default=None)
        The maximum time value that is included in the fitting procedure, i.e.
        the fied upper bound of data range. Note again, that this is the actual
        time value and not the index.

    stepsize : int, optional (default=1)
        The stepsize in the optimization procedure. Note that this is not time
        but stepszie in indices. If you have non-equally spaced time series,
        this may be fishy...

    logfile : str, optional (default=None)
        Path to which a logfile is written. No logfile if None is given.

    show : boolean, optional (default=False)
        Visualize the optimization results.

    verbose : boolean, optional (default=False)
        Print additional info to stdout.
    """
    # make sure the data is sorted
    sort_idx = np.argsort(times)
    times = times[sort_idx]
    signal = signal[sort_idx]

    if end is None or end >= times[-1]:
        if end >= times[-1]:
            warnings.warn('Caution: your specified end is behind the last data point')
        end_idx = len(times)-1
    else:
        end_idx = np.arange(0, len(times))[end <= times][0]

    if max_cutoff is None:
        # at least 5 data points are necessary
        max_cutoff_idx = end_idx - 5
    else:
        max_cutoff_idx = np.arange(0, len(times))[max_cutoff <= times][0]
        if max_cutoff_idx > end_idx -5:
            raise ValueError('max_cutoff_idx > end_idx - 5')

    fitfuncs = {'exponential_plus_constant' : fit_exponential_plus_constant,
                'exponential' : fit_exponential}
    # throws an error for illegal fitfuncs
    try:
        fitfunc_callable = fitfuncs[fitfunc]
    except KeyError:
        raise NotImplementedError('Fitfunc "{}" not available'.format(fitfunc))

    fitfunc_info = 'Fit function used:\n'
    if fitfunc == 'exponential':
        fitfunc_info += '   f(t) = A*exp(-alpha*t)'
        Nparam = 2
    elif fitfunc == 'exponential_plus_constant':
        fitfunc_info += '   f(tx) = A*exp(-alpha*t)+B'
        Nparam = 3

    if verbose:
        print('Starting Fit')
    msg = '-'*70
    msg += '\n{}'.format(time.strftime('%c'))
    msg += '\n'+'-'*70
    msg += '\n' + fitfunc_info
    msg += '\n'+'-'*70
    msg += '\ninput data range : {:.3E} to {:.3E}'.format(times[0], times[-1])
    msg += '\nmax_cutoff_time : {:.3E}'.format(times[max_cutoff_idx])
    msg += '\nend_time : {:.3E}'.format(times[end_idx])
    msg += '\nstepwidth : {:d}'.format(stepsize)
    if verbose:
        print(msg)

    # conveniently store the data in a dictionary
    data = {}

    for cutoff_idx in range(0, max_cutoff_idx, stepsize):
        p_fitted, opt_fitfunc = fitfunc_callable(times[cutoff_idx:end_idx], signal[cutoff_idx:end_idx])
        # if verbose:
            # print('\tFitting for cutoff time = {:.3E}'.format(times[cutoff_idx]))
        # evaluate the adjusted R^2
        Rsquare = calc_adjusted_Rsquare(times[cutoff_idx:end_idx], signal[cutoff_idx:end_idx], opt_fitfunc, Nparam=Nparam)
        alpha = p_fitted[1]

        data[cutoff_idx] = {'Rsquare': Rsquare,
                            'cutoff_idx' : cutoff_idx,
                            'cutoff_time' : times[cutoff_idx],
                            'alpha' : alpha}

    # create arrays to work on from the dictionaries
    cutoff_idcs = np.array([d['cutoff_idx'] for d in data.values()])
    cutoff_times = np.array([d['cutoff_time'] for d in data.values()])
    Rsquares = np.array([d['Rsquare'] for d in data.values()])
    alphas = np.array([d['alpha'] for d in data.values()])

    # ignore failed fits
    try:
        best_idx = np.nanargmax(Rsquares)
    except ValueError:
        best_idx=0

    msg2 = '-'*70
    msg2 += '\nbest_cutoff_time : {:.2E}'.format(cutoff_times[best_idx])
    msg2 += '\nalpha : {:.2E}'.format(alphas[best_idx])
    msg2 += '\nR^2_adjusted : {:.6f}'.format(Rsquares[best_idx])
    msg2 += '\n'+ '-'*70
    if verbose:
        print(msg2)

    if logfile is not None:
        if verbose:
            print('Writing logfile: \n\t{}'.format(logfile))
        with open(logfile, 'w') as f:
            f.write('# ' + msg.replace('\n', '\n# '))
            f.write('\n# ' + msg2.replace('\n', '\n# '))
            f.write('\n#\n# Individual fits')
            f.write('\n# {:>18s}{:>20s}{:>20s}'.format('cutoff_time', 'R^2_adj', 'alpha'))
            sort_idx = np.argsort(cutoff_times)
            for t, R, a in zip(cutoff_times[sort_idx], Rsquares[sort_idx], alphas[sort_idx]):
                f.write('\n{:>20.6E}{:>20.8f}{:>20.6E}'.format(t, R, a))

    if show:

        import matplotlib.pyplot as plt
        fig = plt.figure()
        # the R^2
        ax1 = fig.add_subplot(1,2,1)
        # the alpha
        ax2 = ax1.twinx()

        ax1.set_xlabel(r'cutoff time')
        ax1.set_ylabel(r'$\overline{R}^2$')
        ax1.plot(cutoff_times[best_idx],
                 Rsquares[best_idx],
                 ls='',
                 marker='*',
                 markersize=20,
                 markeredgecolor='darkblue',
                 color = 'darkblue')
        ax1.plot(cutoff_times,
                 Rsquares,
                 ls='',
                 marker='o',
                 color = 'cornflowerblue',
                 markeredgecolor='cornflowerblue',
                 label = r'$\overline{R}^2$')

        ax2.set_xlabel(r'cutoff time')
        ax2.set_ylabel(r'$\alpha$')
        ax2.plot(cutoff_times[best_idx],
                 alphas[best_idx],
                 ls='',
                 marker='*',
                 markersize=20,
                 color = 'crimson',
                 markeredgecolor='crimson')
        ax2.plot(cutoff_times,
                 alphas,
                 ls='',
                 marker='s',
                 color = 'lightpink',
                 markeredgecolor='lightpink',
                 label = r'decay $\alpha$')


        (h1, l1) = ax1.get_legend_handles_labels()
        (h2, l2) = ax2.get_legend_handles_labels()
        leg = ax2.legend(h1+h2, l1+l2, loc='center')
        leg.get_frame().set_linewidth(0.5)

        # best values with respect to the entire times array
        best_idx_entire_array = cutoff_idcs[best_idx]

        ax = fig.add_subplot(1,2,2)
        ax.plot(times[:best_idx_entire_array],
                signal[:best_idx_entire_array],
                color='gray',
                ls='',
                marker='o',
                markeredgecolor='gray',
                )
        ax.plot(times[end_idx::],
                signal[end_idx::],
                color='gray',
                ls='',
                marker='o',
                markeredgecolor='gray',
                label='unsed data points')

        ax.plot(times[best_idx_entire_array:end_idx],
                signal[best_idx_entire_array:end_idx],
                color='black',
                ls='',
                marker='o',
                markeredgecolor='black',
                label='used data points')

        p_fitted, opt_fitfunc = fitfunc_callable(times[best_idx_entire_array:end_idx],
                                                 signal[best_idx_entire_array:end_idx])
        ax.plot(times,
                opt_fitfunc(times),
                color='crimson',
                lw=2,
                label='fit function')

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.legend()
        plt.show()

    return alphas[best_idx]


def calc_Rsquare(times, signal, fitfunc):
    # residual sum of squares
    SSres = np.sum((signal - fitfunc(times))**2)

    # total sum of squares
    SStot = np.sum((signal - np.mean(signal))**2)

    # coefficient of determination
    Rsquare = 1. - (SSres/SStot)

    return Rsquare


def calc_adjusted_Rsquare(times, signal, fitfunc, Nparam):
    # "korrigiertes Bestimmtheitsmass"

    # residual sum of squares
    SSres = np.sum((signal - fitfunc(times))**2)

    # total sum of squares
    SStot = np.sum((signal - np.mean(signal))**2)
    Npoints = float(len(times))

    Rsquare_adjusted = 1. - ((SSres / (Npoints-Nparam)) / (SStot / (Npoints-1)))

    return Rsquare_adjusted


def fit_lifetime(times,
                 signal,
                 fitfunc='exponential_plus_constant',
                 end=None,
                 logfile=None,
                 show=False,
                 verbose=False,
                 ):
    """
    Function to evaluate the lifetime associated with an exponential decay.

    Parameters
    ----------
    times : (Ntimes,) array
     The time series corresponding to the signal.

    signal : (Ntimes,) array
     The corresponding signal.

    fitfunc : string, optional ({*'exponential_plus_constant*', 'exponential'})
     Defines the fit function to be used.

    end : float, optinal (default=None)
     The maximum time value that is included in the fitting procedure, i.e.
     the fixed upper bound of data range. Note again, that this is the actual
     time value and not the index.

    logfile : str, optional (default=None)
     Path to which a logfile is written. No logfile if None is given.

    show : boolean, optional (default=False)
     Visualize the optimization results.

    verbose : boolean, optional (default=False)
     Print additional info to stdout.
    """

    # make sure the data is sorted
    sort_idx = np.argsort(times)
    times = times[sort_idx]
    signal = signal[sort_idx]

    if end is None or end >= times[-1]:
        if end >= times[-1]:
            warnings.warn('Caution: your specified end is behind the last data point')
        end_idx = len(times)-1
    else:
        end_idx = np.arange(0, len(times))[end <= times][0]

    fitfuncs = {'exponential_plus_constant' : fit_exponential_plus_constant,
                'exponential' : fit_exponential}

    # throws an error for illegal fitfuncs
    try:
        fitfunc_callable = fitfuncs[fitfunc]
    except KeyError:
        raise NotImplementedError('Fitfunc "{}" not available'.format(fitfunc))

    fitfunc_info = 'Fit function used:\n'
    if fitfunc == 'exponential':
        fitfunc_info += '   f(t) = A*exp(-alpha*t)'
        Nparam = 2
    elif fitfunc == 'exponential_plus_constant':
        fitfunc_info += '   f(tx) = A*exp(-alpha*t)+B'
        Nparam = 3

    if verbose:
        print('Starting Fit')

    msg = '-'*70
    msg += '\n{}'.format(time.strftime('%c'))
    msg += '\n'+'-'*70
    msg += '\n' + fitfunc_info
    msg += '\n'+'-'*70
    msg += '\ninput data range : {:.3E} to {:.3E}'.format(times[0], times[-1])
    msg += '\nend_time : {:.3E}'.format(times[end_idx])
    if verbose:
        print(msg)

    p_fitted, opt_fitfunc = fitfunc_callable(times[:end_idx], signal[:end_idx], show=show)
    Rsquare = calc_adjusted_Rsquare(times[:end_idx], signal[:end_idx], opt_fitfunc, Nparam=Nparam)
    alpha = p_fitted[1]

    msg2 = '-'*70
    msg2 += '\n1/alpha : {:.6E}'.format(1./alpha)
    msg2 += '\nR^2_adjusted : {:.6f}'.format(Rsquare)
    msg2 += '\n'+ '-'*70
    if verbose:
        print(msg2)

    if logfile is not None:
        if verbose:
            print('Writing logfile: \n\t{}'.format(logfile))
        with open(logfile, 'w') as f:
            f.write('# ' + msg.replace('\n', '\n# '))
            f.write('\n# ' + msg2.replace('\n', '\n# '))


    return 1./alpha
