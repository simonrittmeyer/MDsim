# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Module to convert frcitional energy losses to an electron-excitation spectrum
within the forced oscillator model.
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt

from scipy import fftpack
from scipy.integrate import cumtrapz

from MDsim import units
from MDsim.analysis.isf import _get_next_power_of_two

def calc_Ps(times, etas, velocities, tensorial_eta=False, min_freq=None, optimize_length=True, zero_padding=True, convert=True, verbose=True):
    """
    Calculate Ps as defined

    Ps = (hbar * omega * pi)^-1 |int{sqrt[etas(t)].velocities(t)*exp(-i*omega*t) dt}|^2

    in Eq. (3) of:

    J. R. Trail et al., Phys. Rev. Lett. 88, 166802 (2002).
    http://www.dx.doi.org/10.1103/PhysRevLett.88.166802

    From the paper above:

        The normalized Ps "can be interpreted as the probability that a single
        electron-hole excitation event is of energy hbar*omega."

    Note that we do not resolve different spin channels here.

    Parameters
    ----------
    times : (Ntimes,) array
        The actual times corresponding to the trajectory.
        Unit if convert=<True> is fs, else a.u.

    etas : (Ntimes, Natoms*Ndim) array
        The atomic friction coefficients corresponding to the trajectory.
        Note that the (Natoms, Ndof) dimension from within the code should be
        flattended already here. Otherwise we will do so. Also, check the
        "tensorial_eta" flag.
        Unit if convert=<True> is amu/fs, else a.u.

    velocities : (Ntimes, Natoms*Ndim) array
        The velocities corresponding to the trajectory.
        Note that the (Natoms, Ndim) dimension from within the code should be
        flattended already here. Otherwise we will do so.
        Unit if convert=<True> is angstrom/fs, else a.u.

    tensorial_eta : boolean, optional (default=False)
        If you want to evaluate FOM spectra within a tensorial friction
        approach. Note that in this case, "etas" is required to be an
        (Natoms*Ndim, Natoms*Ndim) matrix. POnly constant tensors supported so
        far.

    min_freq : float, optional (default=5 meV)
        The minimal frequency for wich the spectrum is evaluated. Getting
        closer to zero brings you closer to division by zero-errors.
        Unit if convert=<True> is eV, else a.u.

    zero_padding : bool, optional (default = True)
        Apply zero padding or not to interpolate in frequency space. Be aware
        that this may introduce boundary effects if you trajectory has not
        properply decayed to zero.

    optimize_length : boolean, optional (default=False)
        FFT is fastest if the number of elements is a power of two (see
        Radix2). Hence, activiating this option will zero-pad the input such that
        its lenght is actually the next power of two.

    convert : bool, optional (default=True)
        Convert flag, defined above.

    verbose : boolean, optional (default = True)
        Print some status information to stdout.

    Returns
    -------
    freqs : (N,) array
        The frequencies of the spectrum. Length depends on whether
        zero-padding has been applied or not, whether the input has been
        trunkated to optimize FFT speed, and on the minimial frequency chosen.
        Unit if convert=<True> is eV, else a.u.

    Ps : (N,) array
        The the spectrum. Length depends on whether zero-padding has
        been applied or not, whether the input has been trunkated to
        optimize FFT speed, and on the minimal frequency chosen.
        Unit if convert=<True> is fs, else a.u.
    """

    Ntimes = len(times)

    if not tensorial_eta:
        np.testing.assert_equal(etas.shape, velocities.shape, err_msg='Incompatible shape of "etas" and "velocities"')
        np.testing.assert_equal(etas.shape[0], Ntimes, err_msg='Incompatible shape of "etas"/"velocities" and "times"')

        if len(etas.shape) > 2:
            # flattening the (Natoms, Ndof) structure of eta and vel makes life easier
            etas = np.reshape(etas, (Ntimes, -1))
            velocities = np.reshape(velocities, (Ntimes, -1))
    else:
        velocities = np.reshape(velocities, (Ntimes, -1))
        etas = np.reshape(etas, (velocities.shape[1], velocities.shape[1]))

    if zero_padding:
        n = 2*Ntimes-1
    else:
        n = Ntimes

    if optimize_length:
        # tremendous boost for the FFT (Radix2 algorithms)
        Nnext = _get_next_power_of_two(n)
        if Nnext != Ntimes:
            msg  = 'Info : Zero padded an additional {} elements to allow for most efficient FFT.'.format(Nnext-Ntimes)
            if verbose:
                print(msg)
        n = Nnext

    if convert:
        if verbose:
            print('Converting times: fs --> a.u.')
        times = times * units.FS_TO_AU
        if verbose:
            print('Converting etas: amu/fs --> a.u.')
        etas = etas * units.AMU_PER_FS_TO_AU
        if verbose:
            print('Converting velocities : angstrom/fs --> a.u.')
        velocities = velocities * units.ANGSTROM_PER_FS_TO_AU

    dt = times[1]-times[0]


    # here comes the FFT and the multiplication with the complex conjugate
    if not tensorial_eta:
        arg = np.sqrt(etas)*velocities
    else:
        arg = np.einsum('ij,ti->ti', np.sqrt(etas), velocities)
    FFT = fftpack.fft(arg,n=n, axis=0)
    Ps = np.sum((FFT*np.conjugate(FFT)).real, axis=1)

    # we want the \omega, but fft freq yields the \nu
    freqs = fftpack.fftfreq(n=n, d=dt) * 2 * np.pi

    # create the mask
    if min_freq is None:
        min_freq = 5e-3 * units.EV_TO_AU
    else:
        if convert:
            min_freq *= units.EV_TO_AU

    mask = freqs > min_freq

    Ps = Ps[mask]
    freqs = freqs[mask]

    # the final division here. Note that we are still in au, where hbar = 1
    Ps /= (np.pi*freqs)

    if convert:
        if verbose:
            print('Converting Ps : a.u. --> fs')
        Ps *= units.AU_TO_FS
        if verbose:
            print('Converting freqs : a.u. --> eV')
        freqs *= units.AU_TO_EV

    return freqs, Ps


def _calc_Pe(freqs, Ps, convert=True, verbose=True):
    """
    Evaluate the electron-hole pair spectrum acc.

    Pe(omega) = 1/hbar * int_omega^infty[Ps(omega')/omega' d(omega')]

    as defined in Eq. (23) of

    J. R. Trail et al., J. Chem. Phys. 119, 4539 (2003).
    http://www.dx.doi.org/10.1063/1.1593631

    * This routine is intended to continue with the output of calc_Ps! *

    While you can also use it as a standalone routine, you'll have to take care
    of division-by-zero issues and unit conversions yourself! In this case,
    consider using "calc_Pe".

    Parameters
    ----------
    freqs : (Nfreqs,) array
        The frequencies associated with the single excitation spectrum Ps.
        Unit if convert=<True> is eV, else a.u.

    Ps : (Nfreqs,) array
        The single excitation spectrum in a.u., no conversion switch here so
        far.
        Unit if convert=<True> is fs, else a.u.

    convert : bool, optional (default=True)
        Convert flag, defined above.

    verbose : boolean, optional (default = True)
        Print some status information to stdout.

    Returns
    -------
    energies : (Nfreqs,) array
        The energies associated with the electron excitation spectrum.
        Unit if convert=<True> is eV, else a.u.

    Pe: (Nfreqs,) array
        The electron excitation spectrum.
        Unit if convert=<True> is eV^-1, else a.u.
    """

    if convert:
        if verbose:
            print('Converting Ps : fs --> a.u.')
        Ps *= units.FS_TO_AU
        if verbose:
            print('Converting freqs : eV --> a.u.')
        freqs *= units.EV_TO_AU

    if verbose:
        print('Numerically integrating Pe', end='')

    # we simply use scipy's cumtrapz
    # the -1 accounts for the fact that we have decreasing
    # freqs over the integral
    Pe = cumtrapz((Ps/freqs)[::-1], freqs[::-1], initial=0)[::-1]*-1

    if verbose:
        print(' (done)')

    if convert:
        if verbose:
            print('Converting Pe : a.u. --> eV^-1')
        Pe /= units.AU_TO_EV
        if verbose:
            print('Converting freqs : a.u. --> eV')
        freqs *= units.AU_TO_EV

    return freqs, Pe


def calc_Pe(*args, **kwargs):
    """
    Evaluate the electron-hole pair spectrum acc. to

        Pe(omega) = int_omega^infty[Ps(omega')/omega' d(omega')]

    where

        Ps = (omega * pi)^-1 |int{sqrt[etas(t)].velocities(t)*exp(-i*omega*t) dt}|^2

    see:
        J. R. Trail et al., J. Chem. Phys. 119, 4539 (2003).
        http://www.dx.doi.org/10.1063/1.1593631

        M. Lindenblatt and E. Pehlke, Surf. Sci. 600, 5068 (2006).
        http://www.dx.doi.org/10.1016/j.susc.2006.08.034


    Parameters
    ----------
    times : (Ntimes,) array
        The actual times corresponding to the trajectory.
        Unit if convert=<True> is fs, else a.u.

    etas : (Ntimes, Natoms*Ndim) array
        The atomic friction coefficients corresponding to the trajectory.
        Note that the (Natoms, Ndof) dimension from within the code should be
        flattended already here. Otherwise we will do so. Also, check the
        "tensorial_eta" flag.
        Unit if convert=<True> is amu/fs, else a.u.

    velocities : (Ntimes, Natoms*Ndim) array
        The velocities corresponding to the trajectory.
        Note that the (Natoms, Ndim) dimension from within the code should be
        flattended already here. Otherwise we will do so.
        Unit if convert=<True> is angstrom/fs, else a.u.

    tensorial_eta : boolean, optional (default=False)
        If you want to evaluate FOM spectra within a tensorial friction
        approach. Note that in this case, "etas" is required to be an
        (Natoms*Ndim, Natoms*Ndim) matrix. POnly constant tensors supported so
        far.

    min_freq : float, optional (default=5 meV)
        The minimal frequency for wich the spectrum is evaluated. Getting
        closer to zero brings you closer to division by zero-errors.
        Unit if convert=<True> is eV, else a.u.

    zero_padding : bool, optional (default = True)
        Apply zero padding or not to interpolate in frequency space. Be aware
        that this may introduce boundary effects if you trajectory has not
        properply decayed to zero.

    optimize_length : boolean, optional (default=False)
        FFT is fastest if the number of elements is a power of two (see
        Radix2). Hence, activiating this option will zero-pad the input such that
        its lenght is actually the next power of two.

    convert : bool, optional (default=True)
        Convert flag, defined above.

    verbose : boolean, optional (default = True)
        Print some status information to stdout.

    Returns
    -------
    energies : (Nfreqs,) array
        The energies associated with the electron-hole pair excitation
        spectrum.
        Unit if convert=<True> is eV, else a.u.

    Pe: (Nfreqs,) array
        The electron excitation spectrum.
        Unit if convert=<True> is eV^-1, else a.u.
    """
    energies, Pe = _calc_Pe(*calc_Ps(*args, **kwargs),
                            convert=kwargs.get('convert', True),
                            verbose=kwargs.get('verbose', True))

    return energies, Pe
