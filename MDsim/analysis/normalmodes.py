# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Simple tools to avoid using the ASE normalmode analysis tools
"""
from __future__ import print_function

import numpy as np
import numpy.linalg
from scipy.optimize import minimize

from MDsim import units
from MDsim.derivatives import hessian_central_differences

from MDsim.coordinates.normalmodes import Normalmodes

# -----------------
# The core routines
# -----------------

def minimize_potential_energy(calc_Epot, guess, convert=True, verbose=True, **kwargs):
    """
    Find the minimum energy configuration of a potential. In contrast to
    minimize_energy() this routine does not require a system instance but only
    a potential.

    Parameters
    ----------
    calc_Epot : Callable
        Function that actually returns forces upon being called with a
        positions array. This is very generic, you can for instance also pass
        system.calc_Epot here.

        Required interface is

            ``calc_Epot(positions)``

        and <positions> is expected in a.u..

    guess : (Natoms, Ndim) array
        A guess for the minimum energy configuration. Note that this one needs
        to be in a.u., if <convert> = False, in Angstrom elsewise.

    convert : boolean, optional (default=True)
        Convert input guess from Angstrom to a.u. and output configuration back
        to Angstrom again.

    verbose : boolean, optional (default=True)
        Print status information to stdout.

    **kwargs : Directly passed to scipy.optimize.minimize()

    Returns
    -------
    pos_min : (Natoms, Ndim) array
        The minimum energy configuration.
    """
    guess = np.asarray(guess)

    Natoms, Ndim = guess.shape

    if convert:
        guess = guess * units.ANGSTROM_TO_AU

    # alias the force call
    def func(pos):
        return calc_Epot(pos.reshape(Natoms, Ndim))

    if verbose:
        print('Minimizing potential energy')

    try:
        minimizer = minimize(func, guess.ravel(), options = {'disp' : False}, **kwargs)
        pos_min = minimizer['x'].reshape(Natoms, Ndim)
        if convert:
            pos_min *= units.AU_TO_ANGSTROM
            guess *= units.AU_TO_ANGSTROM

    except ValueError as err:
        print('\tMinimization did not suceed (minimizer running out of bounds)')
        raise ValueError(err)

    if verbose:
        if convert:
            unit = 'Angstrom'
        else:
            unit = 'a.u.'

        msg='Found minimum at (coordinates in {})'.format(unit)
        msg+='\n\t{}'.format(str(pos_min).replace('\n','\n\t'))
        msg+='\nOffset from initial guess'
        msg+='\n\t{}'.format(str(pos_min-guess).replace('\n','\n\t'))
        print(msg)

    return pos_min


def evaluate_normalmodes(calc_Epot,
                         positions,
                         masses,
                         delta=None,
                         convert=True,
                         verbose=True, **kwargs):
    """
    Most generic routine to evaluate normalmodes based on a numerical
    evaluation of the Hessian.

    Parameters
    ----------
    calc_Epot : Callable
        Function that actually returns forces upon being called with a
        positions array. This is very generic, you can for instance also pass
        system.calc_Epot here.

        Required interface is

            ``calc_Epot(positions)``

        and <positions> is expected in a.u.. Energy is returned in a.u. (MDsim
        convention).

    positions : (Natoms, Ndim) array
        Positions at equilibrium, ie., where to evaluate the normalmodes.

    masses : (Natoms,) array
        The masses associated with the atoms, or DOF more generally.


    delta : float, optional (default = None)
        Stepwidth for the numerical evaluation of the Hessian. If <None>,
        stepwidth will be 0.001 a.u. Also applies for numerical forces, if
        central differences are invoked.

    convert : boolean, optional (default = True)
        Whether to convert from Angstrom/amu to a.u. or not. If <False> all
        input expected in a.u. (includes stepwidth as well!). Moreover, also
        output will be converted to eV and Angstrom if <True>.

    verbose : boolean, optional (default=True)
        Print status information to stdout.

    **kwargs : Directly passed to scipy.optimize.minimize()

    Returns
    -------
    omegas : (Nmodes = Natoms*Ndim, 1) array
        The frequencies of the corresponding vibrations. Units depend
        on the input "convert" flag.

    displacements : (Nmodes, Natoms, Ndim) array
        Normalmodes. First index is the mode, second its displacements in
        mass-weighted coordinates.

    positions : (Natoms, Ndim) array
        The positions at which the normalmodes where evaluated.

    coords : Normalmodes coordinates instance
        A properly defined Normalmodes coordinates instance, ready to be used.
    """
    if convert:
        positions = positions * units.ANGSTROM_TO_AU
        masses = masses *units.AMU_TO_AU

    if delta is None:
        delta = 0.001

    elif convert:
        delta = delta * units.ANGSTROM_TO_AU

    if verbose:
        print('Numerically evaluating Hessian (a.k.a. force constant matrix)')

    Hessian = hessian_central_differences(func=calc_Epot,
                                          positions=positions,
                                          d=delta)
    if not np.allclose(Hessian, Hessian.T, rtol=1e-6):
        print('Warning, "_get_Hessian": Hessian matrix is not symmetric. Are your forces alright?')
        print(Hessian)

    Natoms, Ndim = positions.shape

    # inverse masses
    # we repeat it such that we end up with correct dimensionality
    masses_inv = np.repeat(1./masses, Ndim)

    # get the mass weighted Hessian
    Hessian_mw = np.sqrt(masses_inv)[:,None]*Hessian*np.sqrt(masses_inv)

    if verbose:
        print('Diagonalizing Hessian... ', end='')

    omegas_square, displacements =  np.linalg.eigh(Hessian_mw)
    omegas = np.sqrt(omegas_square)



    if verbose:
        print('Found the following eigenfrequencies')
        for i, o in enumerate(omegas):
            msg = '\tomega_{}'.format(i)
            msg+= ' = {:9.3E} a.u.'.format(o)
            msg+= ' = {:9.3f} meV'.format(o*units.AU_TO_EV*1000)
            msg+= ' = {:9.3f} cm^-1'.format(o*units.AU_TO_KAYSER)
            msg+= ' = {:9.3f} THz'.format(o*units.AU_TO_THZ)
            print(msg)

    # Note that numpy.linalg.eigh returns the modes index as second
    # index, ie. in columns. Hence, what we call normalmodes is Q
    # avoid converting issues, just do it in a.u. all the way
    coords = Normalmodes(displacements=displacements,
                         omegas=omegas,
                         masses=masses,
                         positions_eq=positions,
                         convert=False)

    if convert:
        omegas *= units.AU_TO_EV
        # normalmodes are dimensionless

    return omegas, displacements, coords


# --------------------
# The wrapper routines
# --------------------

def minimize_energy_of_system(system, guess, convert=True, verbose=True, **kwargs):
    """
    Find the minimum energy configuration of the system. Basically just a
    wrapper for `minimize_potential_energy`.

    Parameters
    ----------
    system : MDsim system instance
        The system to be minimized.

    guess : (Natoms, Ndim) array
        A guess for the minimum energy configuration. Note that this one needs
        to be in a.u., if <convert> = False, in Angstrom elsewise.

    convert : boolean, optional (default=True)
        Convert input guess from Angstrom to a.u. and output configuration back
        to Angstrom again.

    verbose : boolean, optional (default=True)
        Print status information to screen.

    **kwargs : Directly passed to scipy.optimize.minimize()

    Returns
    -------
    pos_min : (Natoms, Ndim) array
        The minimum energy configuration. Note that this is the same units as
        guess, depending on the convert flag.
    """
    return minimize_potential_energy(calc_Epot=system.calc_potential_energy,
                                     guess=guess,
                                     convert=convert,
                                     verbose=verbose,
                                     **kwargs)


def evaluate_normalmodes_of_system(system,
                                   positions=None,
                                   delta=None,
                                   convert=True,
                                   verbose=True,
                                   **kwargs):
    """
    Routine to evaluate normalmodes based on a numerical evaluation of the
    Hessian.

    Parameters
    ----------
    system : MDsim system
        System for which to evaluate the normalmodes

    positions : (Natoms, Ndim) array, optional (default = None)
        Positions at equilibrium. If <None> the initial positions will be
        used.

    delta : float, optional (default = None)
        Stepwidth for the numerical evaluation of the Hessian. If <None>,
        stepwidth will be 0.001 a.u.

    convert : boolean, optional (default = True)
        Whether to convert from Angstrom to a.u. or not. If <False> all
        input expected in a.u. Moreover, also output will be converted to
        eV and A / sqrt(a.m.u.) if <True>.

    verbose : boolean, optional (default=True)
        Print status information to stdout.

    **kwargs : Directly passed to scipy.optimize.minimize()

    Returns
    -------
    omegas : (Nmodes = Natoms*Ndim, 1) array
        The frequencies of the corresponding vibrations. Units depend
        on the input "convert" flag.

    displacements : (Nmodes, Natoms, Ndim) array
        Normalmodes. First index is the mode, second its displacements in
        mass-weighted coordinates.

    positions : (Natoms, Ndim) array
        The positions at which the normalmodes where evaluated.

    coords : Normalmodes coordinates instance
        A properly defined Normalmodes coordinates instance, ready to be used.
    """
    if positions is None:
        # use the a.u. version, independent of the _convert flag
        positions = minimize_energy_of_system(system,
                                              guess=system.get_positions(_au=True),
                                              convert=False,
                                              verbose=verbose,
                                              **kwargs)
    elif convert:
        positions = positions * units.ANGSTROM_TO_AU

    if convert:
        if delta is not None:
            delta *= units.ANGSTROM_TO_AU

    # positions is in a.u. now
    omegas, displacements, coords = evaluate_normalmodes(calc_Epot=system.calc_potential_energy,
                                                         positions=positions,
                                                         masses=system.get_masses(_au=True),
                                                         verbose=verbose,
                                                         delta=delta,
                                                         convert=False)

    if convert:
        omegas = omegas * units.AU_TO_EV

    return omegas, displacements, positions, coords



def minimize_on_potential(potential, guess, convert=True, verbose=True, **kwargs):
    """
    Find the minimum energy configuration of a potential. In contrast to
    minimize_energy() this routine does not require a system instance but only
    a potential.

    Parameters
    ----------
    potential : MDsim potential instance
        The potential to be minimized.

    guess : (Natoms, Ndim) array
        A guess for the minimum energy configuration. Note that this one needs
        to be in a.u., if <convert> = False, in Angstrom elsewise.

    convert : boolean, optional (default=True)
        Convert input guess from Angstrom to a.u. and output configuration back
        to Angstrom again.

    verbose : boolean, optional (default=True)
        Print status information to stdout.

    **kwargs : Directly passed to scipy.optimize.minimize()

    Returns
    -------
    pos_min : (Natoms, Ndim) array
        The minimum energy configuration.
    """
    return minimize_potential_energy(calc_Epot=potential.get_Epot,
                                     guess=guess,
                                     convert=convert,
                                     verbose=verbose,
                                     **kwargs)


def evaluate_normalmodes_on_potential(potential,
                                      positions,
                                      masses,
                                      minimize_first=True,
                                      delta=None,
                                      verbose=True,
                                      convert=True,
                                      **kwargs):
    """
    Routine to evaluate normalmodes based on a numerical evaluation of the
    Hessian.

    Parameters
    ----------
    system : MDsim system
        System for which to evaluate the normalmodes

    positions : (Natoms, Ndim) array
        Positions at which to evaluate the normalmodes

    masses : (Natoms,) array
        The masses associated with the atoms, or DOF more generally.

    minimize_first : boolean, optional (default = True)
        Positions will be minimized prior to evaluation of the Hesse matrix.
        Starting point is the positions provided as input.

    delta : float, optional (default = None)
        Stepwidth for the numerical evaluation of the Hessian. If <None>,
        stepwidth will be 0.001 a.u.

    convert : boolean, optional (default = True)
        Whether to convert from Angstrom/amu to a.u. or not. If <False> all
        input expected in a.u. Moreover, also output will be converted to
        eV if <True>.

    verbose : boolean, optional (default=True)
        Print status information to stdout.

    **kwargs : Directly passed to scipy.optimize.minimize()

    Returns
    -------
    omegas : (Nmodes = Natoms*Ndim, 1) array
        The frequencies of the corresponding vibrations. Units depend
        on the input "convert" flag.

    displacements : (Nmodes, Natoms, Ndim) array
        Normalmodes. First index is the mode, second its displacements in
        mass-weighted coordinates.

    positions : (Natoms, Ndim) array
        The positions at which the normalmodes where evaluated.

    coords : Normalmodes coordinates instance
        A properly defined Normalmodes coordinates instance, ready to be used.
    """
    if minimize_first:
        positions = minimize_on_potential(potential,
                                          guess=positions,
                                          convert=convert,
                                          verbose=verbose,
                                          **kwargs)
    calc_Epot = potential.get_Epot

    # positions is in a.u. now
    omegas, displacements, coords = evaluate_normalmodes(calc_Epot=calc_Epot,
                                                         positions=positions,
                                                         masses=masses,
                                                         delta=delta,
                                                         verbose=verbose,
                                                         convert=convert)
    return omegas, displacements, positions, coords
