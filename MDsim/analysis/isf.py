# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy import fftpack

"""
Routines to evaluate an ISF (intermediate scattering function)
from MD simulations
"""

# routine by SPR
def _get_next_power_of_two(n):
    """
    Helper routine to yield the next higher power of two.
    """
    return 1<<(n-1).bit_length()

def _get_previous_power_of_two(n):
    """
    Helper routine to yield the next lower power of two.
    """
    return 1<<n.bit_length()- 1


def calc_incoherent_scattering_amplitudes(positions, delta_K):
    """
    Evaluate the per-particle scattering amplitudes which do *NOT* include
    correlation.

    Parameters
    ----------
    positions : (Natoms, Ndim) array
        The system positions

    delta_K : (Ndim, Nkpts) array
        The vectors of momentum change for which to evaluate the scattering
        amplitudes.

    Returns
    -------
    amplitudes : (Natoms, Nkpts) array
        The scattering amplitudes for the respective Delta-K vectors as a
        function of time. Note that this is a complex valued array.
    """

    Natoms, Ndim = positions.shape

    # sanity check
    if not delta_K.shape[0] == Ndim:
        raise ValueError('Dimensions of "trajectory" and "delta_K" are inconsistent')

    # get the scalar products first, this is of dimension (Natoms, Nkpts)
    scalar_products = np.dot(positions, delta_K)

    # calculate the amplitudes (may very well be complex-valued)
    amplitudes = np.exp(-np.complex(0,1)*scalar_products)

    return amplitudes


def calc_scattering_amplitudes(positions, delta_K):
    """
    Calculate the scattering amplitues for a given set of delta_K and positions
    vectors. This is the coherent scattering amplitudes, ie. thos that include
    correlation.

    Contains contributions from Patrick Guetlein.

    Parameters
    ----------
    positions : (Natoms, Ndim) array
        The system positions

    delta_K : (Ndim, Nkpts) array
        The vectors of momentum change for which to evaluate the scattering
        amplitudes.

    Returns
    -------
    amplitudes : (Nkpts,) array
        The scattering amplitudes for the respective Delta-K vectors as a
        function of time. Note that this is a complex valued array.
    """

    return np.sum(calc_incoherent_scattering_amplitudes(positions, delta_K), axis=0)


def calc_dsf(times, amplitudes, zero_padding=True, optimize_length=True, sort=False, verbose=True):
    """
    Evaluate the dynamical structure factor (DSF) which is defined as

        S(dK, dw) = |A(dK, dw)|^2

    where A(dK, dw) is the time-Fourier transform of the scattering amplitudes
    A(dK, t):

        A(dK, dw) = int(A(dK, t) * exp(-i*dw*t) dt)

    Note that we require the full scattering amplitudes for the entire run to
    do the Fourier transform. This is hence a post-processing routine.

    Parameters
    ----------
    times : (Ntimes,) array
        The times array corresponding to the input amplitudes. Will be used to
        return a properly formatted frequency array.

    amplitudes : (Ntimes, Nkpts) array
        Complex valued array of scattering amplitudes.

    zero_padding : boolean, optional (default=True)
        Whether zero padding will be used for the FFT. Actually, without zero
        padding, you will obtain the cyclic autocorrelation. In constrast,
        zero-padding will give you the ("correct") linear autocorrelation.

    optimize_length : boolean, optional (default=False)
        FFT is fastest if the number of elements is a power of two (see
        Radix2). Hence, activiating this option will zero-pad the input such that
        its lenght is actually the next power of two.

    sort : boolean, optional (default=False)
        It <True> the datapoints will be sorted according to increasing
        frequencies. Note that you should *NOT* choose this option if you want
        to backtransform the DSF to obtain ISFs as this messes up the inverse
        FFT.

    verbose : boolean, optional (default = True)
        Print some status information to stdout.

    Returns
    -------
    freqs : (N,) array
        The frequency bins corresponding to the DSF.

    DSFs : (N, Nkpts) array
        The DSFs for the delta_K. Length of first dimension depends on whether
        zero-padding has been applied or not, and whether the input has been
        trunkated to optimize FFT speed.
    """
    try:
        Ntimes, Nkpts = amplitudes.shape
    except ValueError:
        # most likely, we have a 1D amplitudes array
        amplitudes = amplitudes[:,np.newaxis]
        Ntimes, Nkpts = amplitudes.shape

    if zero_padding:
        n = 2*Ntimes-1
    else:
        n = Ntimes

    if optimize_length:
        # tremendous boost for the FFT (Radix2 algorithms)
        Nnext = _get_next_power_of_two(n)
        if Nnext != Ntimes:
            msg  = 'Info : Zero padded an additional {} elements to allow for most efficient FFT.'.format(Nnext-Ntimes)
            if verbose:
                print(msg)
        n = Nnext

    amplitudes_FT = fftpack.fft(amplitudes[:,:], n=n, axis=0)

    # do not need to correct for the phase factor as we are interested in the
    # absolute value only. Else, see
    #       http://stackoverflow.com/a/24077914
    DSF = (amplitudes_FT * np.conjugate(amplitudes_FT)).real

    # assume equally spaced times, otherwise the FFT will crash anyway
    dt = times[1] - times[0]

    # we want the \omega, but fft freq yields the \nu
    freqs = fftpack.fftfreq(n=n, d=dt) * 2 * np.pi

    if sort:
        idx = np.argsort(freqs)
        freqs = freqs[idx]
        DSF = DSF[idx]

    return freqs, DSF


def calc_incoherent_dsf(times, amplitudes, **kwargs):
    """
    Evaluate the incoherent dynamical structure factor (DSF) which is defined as

        S(dK, dw) = sum_i[|A_i(dK, dw)|^2]

    where A_i(dK, dw) is the single particle time-Fourier transform of the
    single particle scattering amplitudes A_i(dK, t):

        A_i(dK, dw) = int(A_i(dK, t) * exp(-i*dw*t) dt)

    Note that we require the full scattering amplitudes for the entire run to
    do the Fourier transform. This is hence a post-processing routine.

    Parameters
    ----------
    times : (Ntimes,) array
        The times array corresponding to the input amplitudes. Will be used to
        return a properly formatted frequency array.

    amplitudes : (Ntimes, Nkpts) array
        Complex valued array of scattering amplitudes.

    zero_padding : boolean, optional (default=True)
        Whether zero padding will be used for the FFT. Actually, without zero
        padding, you will obtain the cyclic autocorrelation. In constrast,
        zero-padding will give you the ("correct") linear autocorrelation.

    optimize_length : boolean, optional (default=False)
        FFT is fastest if the number of elements is a power of two (see
        Radix2). Hence, activiating this option will zero-pad the input such that
        its lenght is actually the next power of two.

    sort : boolean, optional (default=False)
        It <True> the datapoints will be sorted according to increasing
        frequencies. Note that you should *NOT* choose this option if you want
        to backtransform the DSF to obtain ISFs as this messes up the inverse
        FFT.

    verbose : boolean, optional (default = True)
        Print some status information to stdout.

    Returns
    -------
    freqs : (N,) array
        The frequency bins corresponding to the DSF.

    DSFs : (N, Nkpts) array
        The DSFs for the delta_K. Length of first dimension depends on whether
        zero-padding has been applied or not, and whether the input has been
        trunkated to optimize FFT speed.
    """
    Ntimes, Natoms, Nkpts = amplitudes.shape

    for i in range(Natoms):
        # this is the per-atom ISF
        freqs, _dsf = calc_dsf(times, amplitudes[:,i,:].reshape(Ntimes, Nkpts), **kwargs)
        if i==0:
            DSF = _dsf.copy()
        else:
            DSF += _dsf

    # do the average
    DSF /= float(Natoms)

    return freqs, DSF


def calc_isf_from_dsf(dsf, times, normalize=False):
    """
    Calculate inverse Fourier transform of the dynamical structure factor
    (DSF). This is just the ISF.

    Parameters
    ----------
    dsf : (N, Nkpts) array
        Complex/real valued array of dynamical structure factors. If complex
        input is given, only the real part will be considered, as by definition
        of the DSF, it should be entirely real.

        *Do pass the DSF as directly obtained from the FFT routine, as otherwise
        you'll end up in a big mess!*

    times : (Ntimes,) array
        The times array of the underlying scattering amplitudes. Required to
        properly re-shape the output.

    normalize : boolean, optional (default=True)
        Normalize the ISF. Only choose this option if you are sure that
        normalization at this point is appropriate.

    Returns
    -------
    times : (Ntimes/2,) array
        The appropriate times array. Due to evaluation via Fourier transform,
        we lose half of the time information (sampling from -nyquist/2 to
        nyquist/2).

    ISFs : (Ntimes/2, Nkpts) array
        The ISFs for the delta_K. Length of first dimension depends on whether
        zero-padding has been applied or not, and whether the input has been
        trunkated to optimize FFT speed.
    """

    #ISF = np.fft.ifft(dsf, axis=0, n=Ntimes)
    ISF = fftpack.ifft(dsf.real, axis=0).real

    # normalization
    if normalize:
        norm=np.max(ISF.real, axis=0)
        ISF /= norm[np.newaxis,:]

    # center around 0, but only shift along the right axis...
    # actually, we do not need it... just want to have one branch anyway
    #ISF = np.fft.fftshift(ISF, axes=0)

    Ntimes = times.shape[0]

    # only return the positive-frequency branch
    return  times[:Ntimes/2], ISF[:Ntimes/2]



# Routine by SPR
def calc_isf(times, amplitudes, normalize=False, autocorrelation='linear', optimize_length=False, verbose=True):
    """
    Calculate the autocorrelation function of the scattering amplitudes via
    Wiener-Khinchin. This is just the ISF.

    Parameters
    ----------
    times : (Ntimes,) array
        The times array corresponding to the input amplitudes. Will be used to
        return a properly formatted frequency array.

    amplitudes : (Ntimes, Nkpts) array
        Complex valued array of scattering amplitudes.

    normalize : boolean, optional (default=True)
        Normalize the ISF. Only choose this option if you are sure that
        normalization at this point is appropriate.

    autocorrelation : str, optional ({*linear*, cyclic, corrected})
        Whether to treat the amplitudes to be periodic or pulsed in time. This
        essentially determines, whether we compute the cyclic or linear
        autocorrelation. In case of 'linear' the amplitudes series will be zero
        padded from (Ntimes, Nkpts) to (2*Ntimes-1, Nkpts).

        In case of 'corrected' we will divide by the autocorrelation of a
        heaviside step function. This effectively corrects for the number of
        pairs of points that are allowed to contribute to the correlation
        function at each time.

        Please note that evaluating an autocorrelation from a finite size
        sequence is always approximative if the data is not periodic with
        respect to the sampled time range. This means, the smaller the tau, the
        better the approximation for the autocorrelation. As tau grows, so do
        errors and boundary effects (clipping/periodically extending data).
        Hence, for large tau, neither of linear or cyclic autocorrelation
        should be correct for pulsed data.

        Hence, your simulations must last long enough for the autocorrelation
        to die out without being affected by boundary effects too much!

    optimize_length : boolean, optional (default=False)
        FFT is fastest if the number of elements is a power of two (see
        Radix2). Hence, activiating this option will zero-pad the input such that
        its lenght is actually the next power of two.

    verbose : boolean, optional (default=True)
        Print some status information to stdout.

    Returns
    -------
    times : (Ntimes/2,) array
        The appropriate times array. Due to evaluation via Fourier transform,
        we lose half of the time information (sampling from -nyquist/2 to
        nyquist/2).

    ISFs : (Ntimes/2, Nkpts) array
        The ISFs for the delta_K. Note that this is a complex valued array!
    """

    try:
        Ntimes, Nkpts = amplitudes.shape
    except ValueError:
        # most likely, we have a 1D amplitudes array
        amplitudes = amplitudes[:,np.newaxis]
        Ntimes, Nkpts = amplitudes.shape

    autocorrelation = autocorrelation.lower()

    if autocorrelation in ['linear', 'corrected']:
        n = 2*Ntimes-1
    elif autocorrelation == 'cyclic':
        n = Ntimes
    else:
        raise ValueError('calc_isf() : "autocorrelation" must be "linear", "corrected" or "cyclic"')

    if optimize_length:
        # tremendous boost for the FFT (Radix2 algorithms)
        Nnext = _get_next_power_of_two(n)
        if Nnext != Ntimes:
            msg  = 'Info : Zero padded an additional {} elements to allow for most efficient FFT.'.format(Nnext-Ntimes)
            if verbose:
                print(msg)
        n = Nnext


    # the amplitudes
    amplitudes_FT = fftpack.fft(amplitudes, n=n, axis=0)

    # the inverse FFT of the DSF // no zero padding necessary any more in addition
    ISF = fftpack.ifft((amplitudes_FT * np.conjugate(amplitudes_FT)).real, axis=0)

    if autocorrelation == 'corrected':
        ones_FT = fftpack.fft(np.ones(amplitudes.shape, dtype=np.float64), n=n, axis=0)
        correction = fftpack.ifft((ones_FT * np.conjugate(ones_FT)).real, axis=0)
        ISF /= correction

    # only return the positive-frequency branch
    ISF = ISF[:Ntimes/2]
    times = times[:Ntimes/2]

    # normalize only here as otherwise the corrected values will blow up
    if normalize:
        norm=np.max(ISF.real, axis=0)[None,:]
        ISF /= norm

    return times, ISF


def calc_incoherent_isf(times, amplitudes, **kwargs):
    """
    Calculate the autocorrelation function of the incoherent scattering
    amplitudes via Wiener-Khinchin. This is just the *incoherent* ISF.

    Parameters
    ----------
    times : (Ntimes,) array
        The times array corresponding to the input amplitudes. Will be used to
        return a properly formatted frequency array.

    amplitudes : (Ntimes, Natoms, Nkpts) array
        Complex valued array of scattering amplitudes.

    normalize : boolean, optional (default=True)
        Normalize the ISF. Only choose this option if you are sure that
        normalization at this point is appropriate.

    autocorrelation : str, optional ({*linear*, cyclic, corrected})
        Whether to treat the amplitudes to be periodic or pulsed in time. This
        essentially determines, whether we compute the cyclic or linear
        autocorrelation. In case of 'linear' the amplitudes series will be zero
        padded from (Ntimes, Nkpts) to (2*Ntimes-1, Nkpts).

        In case of 'corrected' we will divide by the autocorrelation of a
        heaviside step function. This effectively corrects for the number of
        pairs of points that are allowed to contribute to the correlation
        function at each time.

        Please note that evaluating an autocorrelation from a finite size
        sequence is always approximative if the data is not periodic with
        respect to the sampled time range. This means, the smaller the tau, the
        better the approximation for the autocorrelation. As tau grows, so do
        errors and boundary effects (clipping/periodically extending data).
        Hence, for large tau, neither of linear or cyclic autocorrelation
        should be correct for pulsed data.

        Hence, your simulations must last long enough for the autocorrelation
        to die out without being affected by boundary effects too much!

    optimize_length : boolean, optional (default=False)
        FFT is fastest if the number of elements is a power of two (see
        Radix2). Hence, activiating this option will zero-pad the input such that
        its lenght is actually the next power of two.

    verbose : boolean, optional (default=True)
        Print some status information to stdout.

    Returns
    -------
    times : (Ntimes/2,) array
        The appropriate times array. Due to evaluation via Fourier transform,
        we lose half of the time information (sampling from -nyquist/2 to
        nyquist/2).

    ISFs : (Ntimes/2, Nkpts) array
        The ISFs for the delta_K. Note that this is a complex valued array!
    """
    Ntimes, Natoms, Nkpts = amplitudes.shape

    for i in range(Natoms):
        # this is the per-atom ISF
        times, _isf = calc_isf(times, amplitudes[:,i,:].reshape(Ntimes, Nkpts), **kwargs)
        if i==0:
            ISF = _isf.copy()
        else:
            ISF += _isf

    ISF /= float(Natoms)

    return times, ISF


# routine by PG
def read_file_ScatteringAmplitude(datafile):
    """
    Function to read in the scattering amplitude from the MD-output file
    of scattering-amplitude observable

    Parameters
    ----------
    datafile : string
        path to file with data of scattering amplitude appropriately
        formatted according to observablervable output, ie columns of
        time, k-vectors (real and imag part alternating)

    Returns
    -------
    times : (len(times) x 1) array
        time series of simulation trajectory

    amplitude : (len(times) x dim_K) array (complex!)
        complex valued scattering amplitudes
    """

    if datafile.endswith('.nc'):
        from MDsim.tools.netcdf import load_netcdf
        data = load_netcdf(fname=datafile)
    else:
        data = np.loadtxt(fname=datafile, dtype=np.float64)

    times = data[:,0]

    Ntimes = data.shape[0]
    Nkpts = (data.shape[1] - 1) / 2
    amplitudes = np.empty((Ntimes, Nkpts), dtype=np.complex128)
    amplitudes[:,:].real = data[:,1::2]
    amplitudes[:,:].imag = data[:,2::2]

    return times, amplitudes


def read_file_IncoherentScatteringAmplitude(datafile, Natoms):
    """
    Function to read in the incoherent scattering amplitude from the MD-output
    file of scattering-amplitude observable

    Parameters
    ----------
    datafile : string
        path to file with data of scattering amplitude appropriately
        formatted according to observablervable output, ie columns of
        time, k-vectors (real and imag part alternating)

    Natoms : int
        The number of atoms for which the incoherent scattering amplitudes were
        evaluated. How else would you get this number?

    Returns
    -------
    times : (len(times) x 1) array
        time series of simulation trajectory

    amplitudes : (len(times) x dim_K) array (complex!)
        complex valued scattering amplitudes
    """

    if datafile.endswith('.nc'):
        from MDsim.tools.netcdf import load_netcdf
        data = load_netcdf(fname=datafile)
    else:
        data = np.loadtxt(fname=datafile, dtype=np.float64)

    times = data[:,0]

    Ntimes = data.shape[0]
    Nkpts = (data.shape[1] - 1) / (2*Natoms)

    data = data[:,1::].reshape(Ntimes, Natoms, -1)
    amplitudes = np.empty((Ntimes, Natoms, Nkpts), dtype=np.complex128)

    amplitudes[:,:,:].real = data[:,:,0::2]
    amplitudes[:,:,:].imag = data[:,:,1::2]

    return times, amplitudes


# Routine by PG
def _calc_isf_one_by_one(amplitude):
    """
    Function to calculate the intermediate scattering function from
    scattering amplitudes, ie. to evaluate the autocorrelation of the
    scattering amplitudes.

    Parameters
    ----------
    amplitude : (timesteps x 1) array
                scattering amplitudes for a single delta-K vector over time t
                with a given number of timesteps

    Returns
    -------
    ISF : (timesteps x 1) array
          intermediate scattering function
    """
    #ISF = np.fft.ifft(np.fft.fft(amplitude) * np.conj(np.fft.fft(amplitude)))
    ISF = fftpack.ifft(fftpack.fft(amplitude) * np.conj(fftpack.fft(amplitude)))
    #ISF = np.fft.fftshift(ISF.T / max(ISF))
    ISF = fftpack.fftshift(ISF.T / max(ISF))

    return ISF

## Routine by SPR
#def calc_amplitudes_autocorrelation(amplitudes, zero_padding = True):
#    """
#    Calculate the autocorrelation function of the scattering amplitudes via
#    Wiener-Khinchin. This is just the ISF.

#    Parameters
#    ----------
#    amplitudes : (Nks x Ntimes) array
#        Complex valued array of scattering amplitudes.

#    zero_padding : boolean, optional (default = True)
#        Whether to apply zero padding or not.

#    Returns
#    -------
#    ISFs : (Nk x Ntimes) array
#        The ISFs for the delta_K
#    """
#    # avoid any call by reference confusion
#    Nk, Ntimes = amplitudes.shape

#    normalization = np.ones(amplitudes.shape[1], dtype = np.float64)

#    if zero_padding:
#        amplitudes = np.hstack((amplitudes, np.zeros_like(amplitudes)))
#        normalization = np.hstack((normalization, np.zeros_like(normalization)))

#    ISFs = np.empty(amplitudes.shape, dtype = np.float64)

#    # normalize ones
#    norm = ifft(fft(normalization)*np.conj(fft(normalization)))

#    # may be vectorized at some point
#    for k in range(Nk):
#        # do the fft only once
#        A_fft = fft(amplitudes[k,:])

#        ISFs[k,:] = np.real(ifft(A_fft*np.conj(A_fft)))

#    # only the non-mirrored part
#    return ISFs[:,0:Ntimes]



#    if zero_padding is True:
#        amplitude = np.hstack((amplitude, np.zeros(np.shape(amplitude))))

#        ISF = np.fft.ifft(np.fft.fft(amplitude) * np.conj(np.fft.fft(amplitude)))
#        ISF_real = np.real(ISF[0:len(amplitude)/2.].T / max(ISF[0:len(amplitude)/2.]))

#    elif zero_padding is False:
#        ISF = np.fft.ifft(np.fft.fft(amplitude) * np.conj(np.fft.fft(amplitude)))
#        ISF_real = np.real(ISF.T / max(ISF))

#    return ISF_real





#def get_scattering_amplitude(pos, Natoms, Ndim, Delta_K):
#    """

#    Arguments:
#    ---------

#    pos: position of atoms
#         shape np.array(Natoms,Ndim)

#    Natoms: number of atoms
#            int/float

#    Ndim: degrees of freedom
#          int/float

#    Delta_K: vectors of reciprocal lattice to compute scattering amplitudes
#             shape np.array(Ndim,N_Kgrid)

#    Returns:
#    --------
#    amplitude: scattering amplitude summed over atoms
#               shape np.array(len(Delta_K),1)
#    """

#    # check shape of position array
#    if pos.shape != (Natoms,Ndim):
#        print 'wrong shape of positions'

#    # calculate scattering amplitude A
#    amplitude = np.empty([Delta_K.shape[1],1], dtype = 'complex')

#    # loop over different atoms and immediate summation
#    for n, pos_atom in enumerate(pos):
#        #print 'scalar product: ', n, '\n', np.dot(k.T,pos_atom[:,np.newaxis])
#        #print 'exp:', n, '\n', np.exp(-1j * np.dot(k.T,pos_atom[:,np.newaxis]))
#        amplitude += np.exp(-1j * np.dot(k.T,pos_atom[:,np.newaxis]))

#    # get amplitude as row-vector output
#    return amplitude.T[0]
