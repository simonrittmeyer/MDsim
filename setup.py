# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
# setup.py
import setuptools
from distutils.core import setup
from distutils.core import Extension

import numpy as np
from Cython.Build import cythonize

import MDsim

name="MDsim"
version = MDsim.__version__
description = "MD code to do friction MD"
author = "Simon P. Rittmeyer (TUM)"
author_email = "simon.rittmeyer@tum.de"

cython_extensions = [
    Extension(
        name = 'MDsim.speedup.cythonsupport.fourier',
        sources = ['MDsim/speedup/cythonsupport/src/fourier.pyx'],
        ),
    Extension(
        name = 'MDsim.speedup.cythonsupport.kohnlau',
        sources = ['MDsim/speedup/cythonsupport/src/kohnlau.pyx'],
        ),
    Extension(
        name = 'MDsim.speedup.cythonsupport.neighbor',
        sources = ['MDsim/speedup/cythonsupport/src/neighbors.pyx'],
        ),
    Extension(
        name = 'MDsim.speedup.cythonsupport.cambridge',
        sources = ['MDsim/speedup/cythonsupport/src/cambridge.pyx'],
        ),
    Extension(
        name = 'MDsim.speedup.cythonsupport.distances',
        sources = ['MDsim/speedup/cythonsupport/src/distances.pyx'],
        )
    ]

setup(
    name=name,
    version=version,
    description=description,
    author=author,
    author_email=author_email,
    packages = setuptools.find_packages(),
    scripts=[# here we would have the binaries
             ],
    ext_modules = cythonize(cython_extensions),
    include_dirs=[np.get_include()]
)


from numpy.distutils.core import Extension
from numpy.distutils.core import setup

f90_extensions = [
    Extension(
        name = 'MDsim.speedup.f90support.kohnlau',
        sources = ['MDsim/speedup/f90support/src/kohnlau.f90']
        ),
    Extension(
        name = 'MDsim.speedup.f90support.fourier',
        sources = ['MDsim/speedup/f90support/src/fourier.f90'],
        ),
    Extension(
        name = 'MDsim.speedup.f90support.cambridge',
        sources = ['MDsim/speedup/f90support/src/cambridge.f90'],
        ),
    Extension(
        name = 'MDsim.speedup.f90support.neighbors',
        sources = ['MDsim/speedup/f90support/src/neighbors.f90'],
        ),
    Extension(
        name = 'MDsim.speedup.f90support.distances',
        sources = ['MDsim/speedup/f90support/src/distances.f90'],
        )
    ]

setup(
    name=name,
    version=version,
    description=description,
    author=author,
    author_email=author_email,
    packages = setuptools.find_packages(),
    scripts=[# here we would have the binaries
             ],
    ext_modules = f90_extensions,
    include_dirs=[np.get_include()]
)

# to build with intel compiler use
# python setup.py build_ext --fcompiler=intelem
