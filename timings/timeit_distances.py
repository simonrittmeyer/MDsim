# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np
from MDsim.tools import timeit_auto
from MDsim.tools.periodic import Distances

def timing(funcs, Natoms, setup):
    # some formatting...
    headline ='| {{:<40s}} | {{:^{:d}s}} |'.format(len(Natoms * 10)).format('', 'Natoms')
    lim = ' '+ '-'* (len(headline)-2) + ' '
    print(lim)
    print(headline)
    headline = '| {:<40s} | '.format('function')
    for N in Natoms:
        headline += '{:^10}'.format(N)
    headline += ' |'

    print(headline)
    print(lim)

    # the actual benchmarks
    for func in funcs:
        info = '| {:<40} | '.format(func.replace('dist.','').replace('pos','').replace(', unit_cell',''))
        for N in Natoms:
            runs, time = timeit_auto(func, setup=setup.format(N))
            suffix = 'us'
            if time < 0:
                suffix = 'ns'
                time *= 1000
            if time > 1000:
                suffix = 'ms'
                time /= 1000
            if time > 1000:
                suffix = 's'
                time /= 1000
            info += '{:>7.2f}{:<3s}'.format(time, suffix)
        info += ' |'
        print(info)

    print(lim)



funcs=['d._calc_distances_mic_python(pos, unit_cell)']

try:
    from MDsim.speedup import f90support
    print('f90support available')
    funcs.append('d._calc_distances_mic_fortran(pos, unit_cell)')
except ImportError:
    print('f90 support *not* available')

try:
    from MDsim.speedup import numbasupport
    print('numba support available')
    funcs.append('d._calc_distances_mic_numba(pos, unit_cell)')
except ImportError:
    print('numba support *not* available')

try:
    from MDsim.speedup import cythonsupport
    print('cython support available')
    funcs.append('d._calc_distances_mic_cython(pos, unit_cell)')
except ImportError:
    print('cython support *not* available')

print('Running timings')

setup="""
import numpy as np
from MDsim.tools.periodic import Distances
from MDsim import units
Natoms={}
pos = np.random.random((Natoms, 2))
cell_length = 100.
unit_cell = cell_length*np.array([[1, 0],[0,1]])
pos[:,0] *= unit_cell[0,0]
pos[:,1] *= unit_cell[1,1]
unit_cell = np.diag(unit_cell)
d = Distances()
"""

Natoms = [10,100,1000]
msg='| TIMINGS FOR DISTANCE EVALUATIONS |'
print('+'+'-'*(len(msg)-2)+'+')
print(msg)
print('+'+'-'*(len(msg)-2)+'+')
timing(Natoms=Natoms, funcs=funcs, setup=setup)
