# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np
from MDsim.tools import timeit_auto
from MDsim.potentials.atomic.periodic.diffusion.fcc100 import CambridgePotentialFCC100

def timing(funcs, Natoms, setup):
    # some formatting...
    headline ='| {{:<20s}} | {{:^{:d}s}} |'.format(len(Natoms * 10)).format('', 'Natoms')
    lim = ' '+ '-'* (len(headline)-2) + ' '
    print(lim)
    print(headline)
    headline = '| {:<20s} | '.format('function')
    for N in Natoms:
        headline += '{:^10}'.format(N)
    headline += ' |'

    print(headline)
    print(lim)

    # the actual benchmarks
    for func in funcs:
        info = '| {:<20} | '.format(func.replace('pos','').replace('pot.',''))
        for N in Natoms:
            runs, time = timeit_auto(func, setup=setup.format(N))
            suffix = 'us'
            if time < 0:
                suffix = 'ns'
                time *= 1000
            if time > 1000:
                suffix = 'ms'
                time /= 1000
            if time > 1000:
                suffix = 's'
                time /= 1000
            info += '{:>7.2f}{:<3s}'.format(time, suffix)
        info += ' |'
        print(info)

    print(lim)



a = 3.610 / np.sqrt(2)
energies_dict = {'site1' : 0.050,
                 'site2' : 0.045}
pot = CambridgePotentialFCC100(a=a,
                               energies_dict=energies_dict,
                               convert=True,
                               )
pos = np.random.random((5,2))

print('+----------------------------------------------+')
print('| TIMINGS FOR CAMBRIDGE FCC100 EXPANSION CALLS |')
print('+----------------------------------------------+')

funcs = [
         'pot._python_call(pos)',
         'pot._fortran_call(pos)',
         'pot._numba_call(pos)',
         'pot._cython_call(pos)'
         ]

print('')
print('Consistency check for 5 random atomic positions')
print('-----------------------------------------------')
for func in funcs:
    print(func.replace('pos','').replace('pot.',''))
    print('\t', eval(func))

print()

print('Running timings')

setup="""
import numpy as np
a = 3.610 / np.sqrt(2)
from MDsim.potentials.atomic.periodic.diffusion.fcc100 import CambridgePotentialFCC100
energies_dict = {{'site1' : 0.050,
                 'site2' : 0.045}}
pot = CambridgePotentialFCC100(a=a,
                               energies_dict=energies_dict,
                               convert=True,
                               )
pos = np.random.random(({}, 2))
"""
Natoms = [10,100,1000, 10000]

# timing formatter
timing(Natoms=Natoms, funcs=funcs, setup=setup)

print('')
print('+-------------------------------------------------------+')
print('| TIMINGS FOR CAMBRIDGE FCC100 EXPANSION GRADIENT CALLS |')
print('+-------------------------------------------------------+')

funcs = [
         'pot._python_gradient(pos)',
         'pot._fortran_gradient(pos)',
         'pot._numba_gradient(pos)',
         'pot._cython_gradient(pos)',
         ]

print('')
print('Consistency check for 5 random atomic positions')
print('-----------------------------------------------')
for func in funcs:
    print(func.replace('pos','').replace('pot.',''))
    gradient = eval(func)
    for g in gradient:
        print('\t', g)

print()

timing(Natoms=Natoms, funcs=funcs, setup=setup)
