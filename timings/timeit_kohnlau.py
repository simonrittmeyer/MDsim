# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np
from MDsim.tools import timeit_auto
from MDsim.potentials.atomic.periodic.diffusion.fcc111 import MinimumBasisFourierPotentialFCC111

def timing(funcs, Natoms, setup):
    # some formatting...
    headline ='| {{:<40s}} | {{:^{:d}s}} |'.format(len(Natoms * 10)).format('', 'Natoms')
    lim = ' '+ '-'* (len(headline)-2) + ' '
    print(lim)
    print(headline)
    headline = '| {:<40s} | '.format('function')
    for N in Natoms:
        headline += '{:^10}'.format(N)
    headline += ' |'

    print(headline)
    print(lim)

    # the actual benchmarks
    for func in funcs:
        info = '| {:<40} | '.format(func.replace('pot.','').replace('pos','').replace(', neighbor_lists, num_neighbors',''))
        for N in Natoms:
            runs, time = timeit_auto(func, setup=setup.format(N))
            suffix = 'us'
            if time < 0:
                suffix = 'ns'
                time *= 1000
            if time > 1000:
                suffix = 'ms'
                time /= 1000
            if time > 1000:
                suffix = 's'
                time /= 1000
            info += '{:>7.2f}{:<3s}'.format(time, suffix)
        info += ' |'
        print(info)

    print(lim)



e_funcs=['pot._calc_Epot_bruteforce_python(pos)']
f_funcs=['pot._calc_forces_bruteforce_python(pos)']
e_funcs.append('pot._calc_Epot_neighborlist_python(pos, neighbor_lists, num_neighbors)')
f_funcs.append('pot._calc_forces_neighborlist_python(pos, neighbor_lists, num_neighbors)')

#e_funcs=[]
#f_funcs=[]
try:
    from MDsim.speedup import f90support
    print('f90support available')
    e_funcs.append('pot._calc_Epot_bruteforce_fortran(pos)')
    e_funcs.append('pot._calc_Epot_neighborlist_fortran(pos, neighbor_lists, num_neighbors)')
    f_funcs.append('pot._calc_forces_bruteforce_fortran(pos)')
    f_funcs.append('pot._calc_forces_neighborlist_fortran(pos, neighbor_lists, num_neighbors)')
except ImportError:
    print('f90 support *not* available')

try:
    from MDsim.speedup import numbasupport
    e_funcs.append('pot._calc_Epot_bruteforce_numba(pos)')
    e_funcs.append('pot._calc_Epot_neighborlist_numba(pos, neighbor_lists, num_neighbors)')
    f_funcs.append('pot._calc_forces_bruteforce_numba(pos)')
    f_funcs.append('pot._calc_forces_neighborlist_numba(pos, neighbor_lists, num_neighbors)')
except ImportError:
    print('numba support *not* available')

try:
    from MDsim.speedup import cythonsupport
    print('cython support available')
    e_funcs.append('pot._calc_Epot_bruteforce_cython(pos)')
    e_funcs.append('pot._calc_Epot_neighborlist_cython(pos, neighbor_lists, num_neighbors)')
    f_funcs.append('pot._calc_forces_bruteforce_cython(pos)')
    f_funcs.append('pot._calc_forces_neighborlist_cython(pos, neighbor_lists, num_neighbors)')
except ImportError:
    print('cython support *not* available')

print('Running timings')

setup="""
import numpy as np
from MDsim.neighbors import Neighbors
from MDsim.potentials.pairwise.kohnlaupotential import KohnLauPotential
from MDsim import units
Natoms={}
pos = np.random.random((Natoms, 2))
cell_length = 10.
cell = cell_length*np.array([[1, 0],[0,1]])
pos[:,0] *= cell[0,0]
pos[:,1] *= cell[1,1]
nn = Neighbors(unit_cell=cell)
list_cutoff = cell_length*0.2
neighbor_lists, num_neighbors = nn.construct_neighbor_lists(pos, list_cutoff*units.ANGSTROM_TO_AU)
dipoles = np.ones(Natoms) * 1.22
pot = KohnLauPotential(force_cutoff = list_cutoff*0.8,
                       Natoms=Natoms,
                       Ndim=2,
                       dipoles=dipoles,
                       unit_cell=cell)
"""

Natoms = [10,100,1000,10000]
for name, funcs in [('energy', e_funcs), ('forces', f_funcs)]:
        msg='| TIMINGS FOR KOHN-LAU EVALUATIONS ({}) |'.format(name)
        print('+'+'-'*(len(msg)-2)+'+')
        print(msg)
        print('+'+'-'*(len(msg)-2)+'+')
        timing(Natoms=Natoms, funcs=funcs, setup=setup)
