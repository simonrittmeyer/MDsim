MDsim
=====

MD code to run molecular dynamics simulations including full-tensor electronic friction. A
proper readme is yet to be done once time permits. However, most functionality 
is properly documented within the code. 

Following the spirit of a modern and flexible library for any kind of MD simulations, 
this code is organized in a highly object-oriented and modular way. It is thus 
straightforward to tailor an application adressing your specific needs with 
little additional effort. Following the fast prototyping paradigm, the bulk code 
is written in Python, with performance critical tasks being outsourced to 
properly interfaced Fortran and/or Cython routines. Bindings to electronic structure 
codes and highly parallelized external MD codes exist on the basis of 
the [ase](https://wiki.fysik.dtu.dk/ase/) package. Main applications exist for several tasks that emerged 
in the context of my PhD thesis.

## Short list of featured simulations
* Surface diffusion events with pairwise interactions between adsorbates based
on a Langevin-MD with spatially varying friction. This includes the evaluation
of He3-SE signatures. Results of this project have been published in this 
[paper](http://www.dx.doi.org/10.1103/PhysRevLett.117.196001).

* Vibrational damping simulations, also based on Langevin MD. There are tools to
project into normalmode space and to handle harmonic approximations etc.
Results of this project have been published in this
[paper](http://dx.doi.org/10.1103/PhysRevLett.115.046102). In particular, 
this code allows to propagate Langevin-MD with a full non-diagonal friction tensor. 
Respective details are shown in the SI of my latest paper currently under review.

* AIMD+EF (yet unpublished) with ``CASTEP`` via the respective ASE interface using the
castep2cube and ``castep_Hirshfeld`` utilities.

* QM/Me+EF (together with Vanessa J. Bukas, yet unpublished) with ``CASTEP`` and ``LAMMPS`` via their ase
interfaces.


## Quick installation

The installation of this package follows the usual distutils way via the ``setup.py`` file.

```bash
git clone git@gitlab.com:simonrittmeyer/MDsim.git
cd MDsim
python setup.py install --user
# done
# (optional) run the tests
nosetest tests
```

## How to cite
If you think that this code (or parts of it) is useful for your project, 
please consider citing my work on 
[non-adiabatic vibrational damping](http://dx.doi.org/10.1103/PhysRevLett.115.046102) 
and [surface diffusion](http://www.dx.doi.org/10.1103/PhysRevLett.117.196001). 
If you further plan to use the QM/Me+EF functionality (with analysis tools by 
Vanessa J. Bukas, Standford), please contact me directly to see whether we have
published a respective article in the meantime.
