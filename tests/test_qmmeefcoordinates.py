# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

from MDsim.tools.status import Status
from MDsim.qmmeef.coordinates import Coordinates

def test_qmmeefcoords():
    status = Status()

    ratio=np.random.rand()
    Natoms=10

    positions = np.random.random((Natoms,3))
    coords = Coordinates(qm_lattice_scale=ratio)

    status.ok = np.allclose(positions*ratio, coords.points_me_to_qm(positions))
    print('Transforming points Me --> QM : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.allclose(positions/ratio, coords.points_qm_to_me(positions))
    print('Transforming points Me <-- QM : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.allclose(positions/ratio, coords.gradients_me_to_qm(positions))
    print('Transforming gradients Me --> QM : {}'.format(status))
    if not status.ok:
        print(coords.gradients_me_to_qm(positions))
        print(positions/ratio)
        raise AssertionError

    status.ok = np.allclose(positions*ratio, coords.gradients_qm_to_me(positions))
    print('Transforming gradients Me <-- QM : {}'.format(status))
    if not status.ok:
        raise AssertionError

if __name__ == '__main__':
    test_qmmeefcoords()
