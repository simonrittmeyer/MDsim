# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase

from ase.io.castep import read_castep

from MDsim import units

from MDsim.tools.status import Status
from MDsim.bindings.castep import installed
from MDsim.bindings.castep import ExtendedCastep
from MDsim.bindings.castep import CastepSystem
from MDsim.qmmeef.qm import CastepQM
from MDsim.qmmeef.qmme import QMMe

from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_trajectoryhandler

def test_qmmeef(steps=2, remove=True, show=False):
    try:
        installed()
    except Exception:
        print('Some of castep/castep_hirshfeld/castep2cube are not installed, skipping test...')
        return
    status = Status()

    verbose = False

    # this is to suppress the warnings (shouldn't propagate this instance! But
    # this one is for testing.)

    import warnings
    warnings.filterwarnings("ignore")

    qm_atoms = ase.Atoms('H3', [[0,0,3.], [0,0,4.], [0,0,5.]], cell=[5, 5, 10])
    calc = ExtendedCastep(verbose = verbose)

    me_atoms = ase.Atoms('H4', [[0,0,1], [0,0,2.], [0,0,3.], [0,0,4.]], cell=[5, 5, 10])

    calc._rename_existing_dir = False
    qm_atoms.set_calculator(calc)
    qm_atoms.calc.set_pspot(pspot = 'OTF')
    qm_atoms.calc.cell.kpoints_mp_grid = '1 1 1'
    qm_atoms.set_momenta(np.random.rand(3,3))

    folder = 'output/qm'
    qm_system = CastepQM(qm_atoms,
                calc_folder=folder,
                adsorbate_idx = 2,
                _verbose=verbose,
                friction='aim',
                _eval_parallel = True)

    calc = ExtendedCastep(verbose = verbose)
    calc._rename_existing_dir = False
    me_atoms.set_calculator(calc)
    me_atoms.calc.set_pspot(pspot = 'OTF')
    me_atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    folder = 'output/me'
    me_system = CastepSystem(me_atoms,
                             calc_folder=folder,
                             _verbose=verbose,
                             _init_forces = False,
                             _init_etas = False
                             )


    qmme_system = QMMe(me_system=me_system,
                       qm_system=qm_system,
                       _verbose=verbose)

    idx = qmme_system.get_idx()

    me_idx = qmme_system.get_me_idx()
    qm_idx = qmme_system.get_qm_idx()

    status.ok = qmme_system.get_Natoms()==5
    print('QM/Me system is correctly assembled w.r.t. number of atoms : {}'.format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-9
    status.ok = (abs(qm_system.get_kinetic_energy() - qmme_system.get_kinetic_energy()) < eps
                 # well, different CODATA versions
                 and abs(qm_system.get_kinetic_energy() - qm_atoms.get_kinetic_energy()) < eps)
    print('Kinetic energy is properly composed : {}'.format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-5
    status.ok = np.all(np.abs(qmme_system.get_positions()[idx['me_part']] - me_system.get_positions()) < eps)
    print('Positions reordering for Me system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_positions()[idx['qm_part']] - qm_system.get_positions()) < eps)
    print('Positions reordering for QM system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-5
    status.ok = np.all(np.abs(qmme_system.get_velocities()[idx['bath']] - me_system.get_velocities()[idx['bath']]) < eps)
    print('Velocities reordering for Me-exclusive system (bath) : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_velocities()[idx['qm_part']] - qm_system.get_velocities()) < eps)
    print('Velocities reordering for QM system : {}'.format(status))
    if not status.ok:
        raise AssertionError
    status.ok = np.all(np.abs(qmme_system.get_forces()[idx['adsorbate']]
                              - qm_system.get_forces()[qm_system.get_adsorbate_idx()]) < eps)

    print('Adsorbate forces are purely QM : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_forces()[idx['bath']]
                              - me_system.get_forces()[idx['bath']]) < eps)
    print('Bath forces are purely elastic (Me) : {}'.format(status))
    if not status.ok:
        raise AssertionError


    dummy = np.zeros_like(qmme_system.get_forces())
    dummy[idx['overlap'],:] = me_system.get_forces()[me_idx['overlap']]
    dummy[idx['overlap'],:] += qm_system.get_forces_qm()[qm_system.get_slab_idx()]

    status.ok = np.all(np.abs(qmme_system.get_forces()[idx['overlap']] - dummy[idx['overlap']]) < eps)
    print('Overlap forces are superposition of pure-QM and elastic forces : {}'.format(status))
    if not status.ok:
        raise AssertionError

    castep_forces = read_castep(os.path.join(qm_system.get_calc_folder(), qm_system.atoms.calc._seed + '.castep')).get_forces()

    status.ok = np.allclose(qm_system.get_forces(), castep_forces)
    print('QM forces are actually the forces from castep : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_etas()[idx['adsorbate']] > eps))
    print('Friction coefficients are non-zero for adsorbate atoms : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_etas()[idx['substrate']] < eps))
    print('Friction coefficients are zero for substrate atoms : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = (qm_system.atoms.calc._calls == 1,
                 qm_system.slab_atoms.calc._calls == 1)
    print('QM calculations are re-used : {}'.format(status))
    if not status.ok:
        raise AssertionError

    new_pos = qmme_system.get_positions() + np.random.rand(*qmme_system.get_dimensions())
    qmme_system.set_positions(new_pos * units.ANGSTROM_TO_AU)
    status.ok = np.allclose(qmme_system.get_positions(), new_pos)
    print('Setting positions for full system : {}'.format(status))
    if not status.ok:
        raise AssertionError


    status.ok = np.all(np.abs(qmme_system.get_positions()[idx['me_part']] - me_system.get_positions()) < eps)
    print('Setting positions for Me system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.allclose(qmme_system.get_positions()[idx['qm_part']], qm_system.get_positions())
    status.ok = status.ok and np.allclose(qmme_system.get_positions()[idx['qm_part']], qm_system.atoms.get_positions())
    print('Setting positions for QM system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    slab_idx = qm_system.get_slab_idx()
    status.ok = np.allclose(qmme_system.get_positions()[idx['qm_part']][slab_idx], qm_system.get_positions()[slab_idx])
    status.ok = status.ok and np.allclose(qm_system.get_positions()[slab_idx], qm_system.slab_atoms.get_positions())
    print('Setting positions for QM slab-subsystem : {}'.format(status))
    if not status.ok:
        raise AssertionError

    Ediss = qmme_system.get_dissipated_energy()
    status.ok = Ediss == 0.
    print('No initial energy dissipation : {}'.format(status))
    if not status.ok:
        raise AssertionError

    # avoid confusion
    qmme_system.atoms.set_positions((qmme_system.get_positions() - 1.75) * units.ANGSTROM_TO_AU)

    # the propagation part
    integrator = create_integrator(method='liouville', dt=1, seed=None, verbose=False)
    folder = os.path.join('output','trajectories')
    handlers = []
    handlers.append(create_trajectoryhandler(system_inst = qmme_system, verbose=False, outfolder=folder, seed='QMMe'))
    handlers.append(create_trajectoryhandler(system_inst = me_system, verbose=False, outfolder=folder, seed='Me'))
    handlers.append(create_trajectoryhandler(system_inst = qm_system, verbose=False, outfolder=folder, seed='QM'))

    # init
    for handler in handlers:
        handler.push()

    # positions are stored only with rather limited precision
    pos_eps = 1e-5
    eps = 1e-10

    pos = qmme_system.get_positions().copy()
    vel = qmme_system.get_velocities().copy()
    forces = qmme_system.get_forces().copy()
    etas = qmme_system.get_etas().copy()
    Epot = qmme_system.get_potential_energy()


    status.ok = (qm_system.atoms.calc._calls == 2
                 and qm_system.atoms.calc._continuation_calls == 2
                 and qm_system.slab_atoms.calc._calls == 2)
    print('New QM calculation required for new positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = (qm_system.atoms.calc._calls == 2
                 and qm_system.atoms.calc._continuation_calls == 2
                 and qm_system.slab_atoms.calc._calls == 2)
    print('New QM calculation required for new positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    nstep=0
    for step in range(steps):
        nstep += 1
        integrator.propagate(qmme_system)
        status.ok = (not np.all(np.abs(pos - qmme_system.get_positions()) < pos_eps)
                     and not np.all(np.abs(vel - qmme_system.get_velocities()) < eps)
                     and not np.all(np.abs(forces - qmme_system.get_forces()) < eps)
                     and not np.all(np.abs(etas - qmme_system.get_etas()) < eps)
                     and not np.abs(Epot - qmme_system.get_potential_energy()) < eps)


        print('System properties all change after propagation at step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        _Ediss = Ediss
        Ediss = qmme_system.get_dissipated_energy()
        status.ok = Ediss > _Ediss
        print('Dissipated energy increases at step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        status.ok = (qm_system.atoms.calc._calls == 2 + nstep
                     and qm_system.atoms.calc._continuation_calls == 2 + nstep
                     and qm_system.slab_atoms.calc._calls == 2 + nstep)
        print('New QM calculation required for step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        pos = qmme_system.get_positions().copy()
        vel = qmme_system.get_velocities().copy()
        forces = qmme_system.get_forces().copy()
        etas = qmme_system.get_etas().copy()
        Epot = qmme_system.get_potential_energy()

        for handler in handlers:
            handler.push()

    for handler in handlers:
        handler.close()

    if show:
        for system in ['QM', 'Me', 'QMMe']:
            os.system('ase-gui {}/{}__ase.traj'.format(folder, system))

    if remove:
        shutil.rmtree(folder)

if __name__ == '__main__':
    test_qmmeef(steps=5,remove=False, show=False)
