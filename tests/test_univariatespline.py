# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.interpolation.univariatespline import UnivariateSpline
from MDsim.interpolation.univariatespline import PeriodicUnivariateSpline

import scipy
from distutils.version import LooseVersion

def test_univariatespline(show=False):
    # some random data to interpolate

    data = np.loadtxt('./data/PES_11x11x1_kpoints.pot')
    points = data[:,1]
    values = data[:,-1]

    points_periodic = np.linspace(0, 2*np.pi, 10)
    values_periodic = np.sin(points_periodic)
    for method in ['univariatespline', 'interpolatedunivariatespline']:
        usp = UnivariateSpline(points, values, function=method, smoothing=1e-6)
        print(usp)
        if show:
            usp.show()


        # bounds checking...
        if (LooseVersion(scipy.__version__) >= LooseVersion('0.16.0')):
            try:
                usp.get_value(max(points)+1)
                usp.get_gradient(max(points)+1)
                raise AssertionError
            except ValueError:
                pass

        usp = PeriodicUnivariateSpline(points_periodic, values_periodic, function=method, smoothing=1e-6)
        print(usp)
        if show:
            usp.show()
if __name__ == '__main__':
    test_univariatespline(show=True)


