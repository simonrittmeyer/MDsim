# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase

from MDsim.bindings.castep import installed
from MDsim.bindings.castep import ExtendedCastep


def test_extendedcastep(show=False, remove=True):
    try:
        installed()
    except Exception:
        print('Some of castep/castep_hirshfeld/castep2cube are not installed, skipping test...')
        return

    atoms = ase.Atoms('NCH', [[0, 0, 0], [0, 0, 1.156], [0,0,1.064+1.156]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep()

    calc._label = 'HCN'
    calc._directory = 'output/castep_run'
    if os.path.isdir(calc._directory):
        shutil.rmtree(calc._directory)
    os.makedirs(calc._directory)
    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    atoms.calc.get_scf_density(atoms)
    atoms.get_potential_energy()
    atoms.get_forces()

    if show:
        import matplotlib.pyplot as plt
        from ase.data.colors import jmol_colors
        jmol_colors[1] = np.array((60,179,113))/255.

        x = np.linspace(0,10,500)
        points = np.array([np.append(atoms.get_positions()[0][:2], xi) for xi in x])
        density = atoms.calc._interpolated_scf_density(points)
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.plot(x, density, lw=2, c='red')
        for p, atom in zip(atoms.get_positions(), atoms):
            ax.axvline(x=p[2], ls='--', lw=2, c=jmol_colors[atom.number])
            ax.text(p[2]-.3, max(density), atom.symbol, color=jmol_colors[atom.number], rotation=90, fontsize='large', weight='bold')
        ax.set_xlabel(r'$z$ coordinate ($\AA$)')
        ax.set_ylabel(r'$\rho$ (e/$\AA^3$)')
        plt.show()

    # second run to see if reuse works properly
    atoms = ase.Atoms('NCH', [[0, 0, 0], [0, 0, 1.156], [0,0,1.064+1.156]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep()

    calc._label = 'HCN'
    calc._directory = 'output/castep_run2'
    if os.path.isdir(calc._directory):
        shutil.rmtree(calc._directory)
    os.makedirs(calc._directory)

    atoms.set_calculator(calc)
    atoms.calc.reuse_previous_calculation('output/castep_run')
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'


    if show:
        import matplotlib.pyplot as plt
        from ase.data.colors import jmol_colors
        jmol_colors[1] = np.array((60,179,113))/255.

        x = np.linspace(0,10,500)
        points = np.array([np.append(atoms.get_positions()[0][:2], xi) for xi in x])
        atomic_embedding_density = atoms.calc.get_aim_densities(atoms)
        rho = atoms.calc.get_scf_density(atoms)
        density = rho(points)

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.plot(x, density, lw=2, ls='--', c='red', label='scf')
        for i, atom in enumerate(atoms):
            density_i = atoms.calc._interpolated_hirshfeld_weights[i](points)*density
            ax.plot(x, density_i, lw=2, c=jmol_colors[atom.number], label=atom.symbol)

        for p, atom in zip(atoms.get_positions(), atoms):
            ax.axvline(x=p[2], ls='--', lw=2, c=jmol_colors[atom.number])
            ax.text(p[2]-.3, max(density), atom.symbol, color=jmol_colors[atom.number], rotation=90, fontsize='large', weight='bold')

        ax.legend()
        ax.set_xlabel(r'$z$ coordinate ($\AA$)')
        ax.set_ylabel(r'$\rho$ (e/$\AA^3$)')
        plt.show()


        fig = plt.figure()

        for i, atom in enumerate(atoms):
            ax = fig.add_subplot(3,1,i+1)
            density_i = atoms.calc._interpolated_hirshfeld_weights[i](points)*density
            density_aim = (1.-atoms.calc._interpolated_hirshfeld_weights[i](points))*density
            ax.plot(x, density, lw=2, ls='--', c='red', label=r'$\rho^{\mathrm{SCF}}$')
            ax.plot(x, density_i, lw=2, ls=':', c=jmol_colors[atom.number], label=r'$\rho^{\mathrm{ba}}_{\mathrm{%s}}$'%atom.symbol)
            ax.plot(x, density_aim, lw=2, ls='-', c=jmol_colors[atom.number], label=r'$\rho^{\mathrm{AIM}}_{\mathrm{%s}}$'%atom.symbol)
            ax.plot(atom.position[2], atomic_embedding_density[atom.index], ls='', lw=2, markersize=6, marker='o', color=jmol_colors[atom.number])

            for p, atom in zip(atoms.get_positions(), atoms):
                ax.axvline(x=p[2], ls='--', lw=2, c=jmol_colors[atom.number])
                ax.text(p[2]-.3, max(density), atom.symbol, color=jmol_colors[atom.number], rotation=90, fontsize='large', weight='bold')
            ax.legend()
            ax.set_xlabel(r'$z$ coordinate ($\AA$)')
            ax.set_ylabel(r'$\rho$ (e/$\AA^3$)')

        plt.show()

    print('+'*80)

    # third and fourth run to see if high-level routines work properly
    atoms = ase.Atoms('NCH', [[0, 0, 0], [0, 0, 1.156], [0,0,1.064+1.156]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep()

    calc._label = 'HCN'
    calc._directory = 'output/castep_run3'
    if os.path.isdir(calc._directory):
        shutil.rmtree(calc._directory)

    os.makedirs(calc._directory)
    atoms.set_calculator(calc)
    atoms.calc.reuse_previous_calculation('output/castep_run')
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    atoms.get_forces()
    atoms.get_total_energy()

    atoms.calc.get_aim_densities(atoms)
    atoms.calc.get_scf_density(atoms)
    if remove:
        shutil.rmtree('output/castep_run')
        shutil.rmtree('output/castep_run2')
        shutil.rmtree('output/castep_run3')

    print('+'*80)
    calc._directory = 'output/castep_run4'
    if os.path.isdir(calc._directory):
        shutil.rmtree(calc._directory)
    atoms.set_calculator(calc)
    atoms.set_positions(atoms.get_positions()+1)
    atoms.calc.get_aim_densities(atoms)
    atoms.get_forces()

    if remove:
        shutil.rmtree('output')
if __name__ == '__main__':
    test_extendedcastep(show=True,remove=False)
