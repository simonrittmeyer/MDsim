# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import os

import ase
from ase.lattice.surface import fcc100
from ase.calculators.lammpsrun import LAMMPS
from ase.optimize import FIRE

from MDsim.tools.status import Status
from MDsim.bindings.lammps import LammpsSystem
from MDsim.bindings.lammps import installed

def test_lammpssystem():
    try:
        installed()
    except Exception:
        print('Either lammps or ase is not installed, skipping test...')
        return

    status = Status()

    # taken from VJB's original QMMe-script -- thanks!
    a_Pd_fcc = 3.93# MEAMopt
    a_Pd_100 =  a_Pd_fcc / np.sqrt(2.0)

    size = (5,5,5) # *** always use even numbers !!! ***
    vacuum = 30.0

    atoms = fcc100(symbol='Pd', a=a_Pd_fcc, size=size, vacuum=vacuum)

    pair_style = 'meam'
    # necessary to use an absolute path here, otherwise the calculator will
    # crash (uses some tmp dir)
    Pd_eam_file = os.path.abspath('data/Pd-00PBE.meam')
    pair_coeff = ['* * ' + Pd_eam_file + ' Pd NULL Pd']
    parameters = {'pair_style' : pair_style,
                  'pair_coeff' : pair_coeff}
    files = [Pd_eam_file]

    calc = LAMMPS(parameters=parameters,
                  tmp_dir='output',
                  keep_tmp_files=True,
                  keep_alive=False,
                  files=files)
    atoms.set_pbc(True)
    atoms.set_calculator(calc)
    forces = atoms.get_forces()

    system = LammpsSystem(atoms=atoms)

    eps = 1e-10
    status.ok = np.all(np.abs(system.get_forces() - forces) <  eps)
    print('Forces are transferred nicely : {}'.format(status))
    if not status.ok:
        raise AssertionError


if __name__ == '__main__':
    test_lammpssystem()
