# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_kdirections(show=False):
    import numpy as np
    from MDsim import units
    from MDsim.potentials.atomic.periodic.diffusion.fcc111 import MinimumBasisFourierPotentialFCC111
    from MDsim.coordinates.diffusion.fcc111 import create_surface_direction
    from MDsim.coordinates.diffusion.fcc111 import create_sites

    a = 3.610 / np.sqrt(2)
    # numbers from J. M. Carlsson and B. Hellsing, Phys. Rev. B 61, 13973 (2000)
    # for 2x2 cells
    energies = {'top' : -1.897,
                'bridge' : -1.939,
                'fcc' : -1.940,
                'hcp' : -1.944}

    pot = MinimumBasisFourierPotentialFCC111(a, energies, convert = True)

    plot_dict = pot._create_lattice_plot()

    ax = plot_dict['ax']
    repeats = plot_dict['repeats']
    _lim, lim  = plot_dict['lim']
    print(plot_dict)

    from MDsim.coordinates.diffusion.fcc111 import implemented_directions
    for direction in implemented_directions:
        v = create_surface_direction(direction)

        # scale the things
        if '2' in direction:
            v *= 2*a
        else:
            v *= a*units.ANGSTROM_TO_AU
        ax.arrow(0, 0, v[0], v[1],
                 head_width=0.1,
                 linewidth = 2,
                 length_includes_head = True,
                 head_length=0.1,
                 zorder = 5,
                 linestyle='dotted',
                 color = 'black'
                 )
        ax.text(v[0], v[1], direction)

    sites = create_sites(a*units.ANGSTROM_TO_AU)
    markers = ['s', 'o', 'v', '^']

    for i, (site, coords) in enumerate(sites.items()):
        ax.plot(*coords,
                marker=markers[i],
                color = 'purple',
                markersize=10,
                ls ='',
                label=site)


    ax.legend()

    if show:
        import matplotlib.pyplot as plt
        plt.show()

if __name__ == '__main__':
    test_kdirections(show=True)

