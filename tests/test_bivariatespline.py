# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_bivariatespline(show = False):
    import numpy as np

    from MDsim.tools.status import Status

    from MDsim.interpolation.bivariatespline.atomicbivariatespline import AtomicBivariateSpline
    from MDsim.interpolation.bivariatespline.periodic.periodicatomicbivariatespline import PeriodicAtomicBivariateSpline

    status = Status()

    # making up some function...
    npoints = 20

    x = np.linspace(-5,5, npoints)
    y = np.linspace(-7,13, npoints)

    points = np.empty((npoints**2, 2))
    z = np.empty(npoints**2)

    c = 0
    for xi in x:
        for yj in y:
            points[c,0] = xi
            points[c,1] = yj
            z[c] = np.sin(np.pi/5.*xi) * 1.45*np.cos(np.pi/10. *yj)
            c+=1

    bs = AtomicBivariateSpline(points, z)
    if show:
        bs.show()

    pbs = PeriodicAtomicBivariateSpline(points, z)

    status.ok =  np.all(bs.get_value(points) - z < 1e-10)
    print('Interpolation matches input points : {}'.format(status))

    try:
        status.ok =  False
        bs.get_value(np.array([[123,41343]]))
    except ValueError:
        status.ok =  True

    print('Out of bounds check : {}'.format(status))
    if not status.ok:
        raise AssertionError

    pos = np.array([[5.75,-7.88]])

    status.ok =   np.all(np.abs(pbs._wrap_positions(pos) - np.array([[-5.+0.75, 13.-.88]])) < 1e-10)
    print('Position wrap works for periodic spline : {}'.format(status))

    if not status.ok:
        raise AssertionError

    points_shift = points.copy()
    points_shift[:,0] += 10
    points_shift[:,1] += 20


    status.ok =   np.all(np.abs(pbs.get_value(points) - pbs.get_value(points_shift)) < 1e-10)
    print('Values remain unchanged under position wrap : {}'.format(status))

    if not status.ok:
        raise AssertionError

    status.ok =   np.all(np.abs(bs.get_value(points) - bs.get_value(points)) < 1e-10)
    print('Values remain unchanged under call aliasing : {}'.format(status))

    from scipy.optimize import minimize

    def func(pos):
        pos = np.array([pos])
        return pbs.get_value(pos)

    tol = 1e-10
    minimizer = minimize(func, x0=[0,0], options = {'disp' : False}, tol = tol)
    pos_min = [minimizer['x']]

    status.ok =   np.all(np.abs(pbs.get_gradient(pos_min)) < 1e-7)
    print('Gradient vanishes at minimum : {}'.format(status))

    if not status.ok:
        raise AssertionError

    if show:
        pbs.show(_repeats = 3)

if __name__ == '__main__':
    test_bivariatespline(show = True)
