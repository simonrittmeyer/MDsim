# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_diffusioncoordinates(show=False):
    import numpy as np
    from MDsim.potentials.atomic.periodic.diffusion.fcc111 import MinimumBasisFourierPotentialFCC111
    import MDsim.coordinates.diffusion as coords

    a = 3.610 / np.sqrt(2)
    # numbers from J. M. Carlsson and B. Hellsing, Phys. Rev. B 61, 13973 (2000)
    # for 2x2 cells
    energies = {'top' : -1.897,
                'bridge' : -1.939,
                'fcc' : -1.944,
                'hcp' : -1.944}

    pot = MinimumBasisFourierPotentialFCC111(a, energies, convert = False)

    lattice = coords.fcc111.create_rectangular_real_space_lattice(a)

    if show:
        import matplotlib.pyplot as plt
        import matplotlib.patches as patches
        plot_dict = pot._create_lattice_plot(surf_plot=False)

        ax = plot_dict['ax']
        repeats = plot_dict['repeats']

        print(plot_dict)

        from MDsim.coordinates.diffusion.fcc111 import implemented_directions as directions
        line_styles = ['--', ':', '-.']
        colors = ['crimson', 'cornflowerblue', 'darkorange']

        for i in range(2):
            direction = directions[i]
            ls = line_styles[i]
            color = colors[i]
            v = coords.fcc111.create_surface_direction(direction)
            ax.plot([-v[0]*repeats*10, v[0]*repeats*10],
                    [-v[1]*repeats*10, v[1]*repeats*10],
                    lw=3,
                    ls=ls,
                    color=color,
                    label=direction)

        sites = ['top', 'hcp', 'fcc', 'bridge']
        sites_pos = coords.fcc111.create_sites(a)
        markers = ['o', 's', 'v', '^']
        for i in range(len(sites)):
            marker=markers[i]
            site = sites[i]
            ax.plot(*sites_pos[site].T,
                    marker=marker,
                    markersize=10,
                    color='gold',
                    ls='',
                    label=site)



        ax.add_patch(patches.Rectangle(xy=(0,0),
                                       width=lattice[0, 0],
                                       height=lattice[1, 1],
                                       color='red',
                                       alpha=0.2))

        ax.legend()
        plt.show()

if __name__ == '__main__':
    test_diffusioncoordinates(show=True)
