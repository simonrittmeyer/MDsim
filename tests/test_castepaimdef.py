# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase

from ase.calculators.castep import Castep

from MDsim.bindings.castep import CastepEFSystem
from MDsim.bindings.castep import ExtendedCastep
from MDsim.tools.createobjects.integrator import create_integrator
from MDsim.tools.createobjects.asebindings import create_trajectoryhandler

from MDsim.tools.status import Status

def test_castepaimdef(remove=True, show=False, steps=1):
    try:
        from ase.test.castep import installed
        installed()
    except Exception:
        print('Either castep is not installed, skipping test...')
        return

    status = Status()
    atoms = ase.Atoms('H2', [[0, 0, 0], [0, 0, 1.2]], cell=[5, 5, 10])
    calc = Castep()
    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')

    try:
        CastepEFSystem(atoms=atoms, friction='aim', _verbose=False)
        status.ok = False
    except ValueError:
        status.ok = True

    print('CastepSystemEF rejects wrong calculator: {}'.format(status))
    if not status.ok:
        raise AssertionError


    try:
        shutil.rmtree('output')
    except OSError:
        pass


    for friction in ['wrong', 'iaa', 'aim']:
        atoms = ase.Atoms('H2', [[0, 0, 0], [0, 0, 1.2]], cell=[5, 5, 10])
        atoms.center()
        calc = ExtendedCastep(verbose=False)

        calc._label = 'H2'
        calc._rename_existing_dir = False
        atoms.set_calculator(calc)
        atoms.calc.set_pspot(pspot = 'OTF')
        atoms.calc.cell.kpoints_mp_grid = '1 1 1'

        integrator = create_integrator(method='liouville', dt=1, seed=None, verbose=False)

        if friction == 'wrong':
            try:
                CastepEFSystem(atoms=atoms, friction=friction, _verbose=False)
                status.ok = False
            except NotImplementedError:
                status.ok = True

            print('CastepSystemEF rejects wrong friction: {}'.format(status))
            if not status.ok:
                raise AssertionError

            continue

        folder = 'output/{}'.format(friction)

        system = CastepEFSystem(atoms=atoms, friction=friction, adsorbate_idx=1, _verbose=False, calc_folder=folder)
        f = '{} model'.format(friction.upper())

        handler = create_trajectoryhandler(system_inst = system, verbose=False, outfolder=folder)
        status.ok = system.atoms.calc.param.reuse.value is None
        if friction == 'iaa':
            status.ok = status.ok and system.slab_atoms.calc.param.reuse.value is None

        print("{} : Initial step is *not* a reuse-calculation: {}".format(f, status))
        if not status.ok:
            raise AssertionError

        if friction == 'iaa':
            status.ok = (system.atoms.calc._continuation_calls == 0
                         and system.atoms.calc._calls == 1
                         and system.slab_atoms.calc._calls == 1
                         and system.slab_atoms.calc._continuation_calls == 1)
        else:
            status.ok = (system.atoms.calc._continuation_calls == 1
                         and system.atoms.calc._calls == 1
                         and system.slab_atoms.calc._calls == 0
                         and system.slab_atoms.calc._continuation_calls == 0)

        print("{} : init_etas runs continuation run as expected: {}".format(f, status))
        if not status.ok:
            raise AssertionError

        etas = system.get_etas()

        if friction == 'iaa':
            status.ok = (system.atoms.calc._continuation_calls == 0
                         and system.atoms.calc._calls == 1
                         and system.slab_atoms.calc._calls == 1
                         and system.slab_atoms.calc._continuation_calls == 1)
        else:
            status.ok = (system.atoms.calc._continuation_calls == 1
                         and system.atoms.calc._calls == 1
                         and system.slab_atoms.calc._calls == 0
                         and system.slab_atoms.calc._continuation_calls == 0)

            print("{} : re-using etas without new scf/continuation call: {}".format(f, status))
        if not status.ok:
            raise AssertionError

        status.ok = not np.all(etas == 0.)
        print("{} : non-zero friction as expected: {}".format(f, status))
        if not status.ok:
            raise AssertionError

        nstep = 0

        # positions are stored only with rather limited precision
        pos_eps = 1e-5
        eps = 1e-10

        pos = system.get_positions().copy()
        vel = system.get_velocities().copy()
        forces = system.get_forces().copy()
        etas = system.get_etas().copy()
        Epot = system.get_potential_energy()

        for step in range(steps):
            nstep+=1
            integrator.propagate(system)
            status.ok = (not np.all(np.abs(pos - system.get_positions()) < pos_eps)
                         and not np.all(np.abs(vel - system.get_velocities()) < eps)
                         and not np.all(np.abs(forces - system.get_forces()) < eps)
                         and not np.all(np.abs(etas - system.get_etas()) < eps)
                         and not np.abs(Epot - system.get_potential_energy()) < eps)

            print('{} : System properties all change after propagation at step {} : {}'.format(f, nstep, status))
            if not status.ok:
                raise AssertionError

            pos = system.get_positions().copy()
            vel = system.get_velocities().copy()
            forces = system.get_forces().copy()
            etas = system.get_etas().copy()
            Epot = system.get_potential_energy()


            handler.push()

        # the '+' accounts for the initial evaluation
        status.ok = system.get_Ncalls() == (steps+1)
        print('{} : Each integration step calls force routine once : {}'.format(f, status))
        if not status.ok:
            raise AssertionError

        # the '+' accounts for the initial evaluation
        status.ok = (system.atoms.calc._continuation_calls == (steps+1)
                     and system.slab_atoms.calc._continuation_calls == 0)
        if friction == 'iaa':
            status.ok = (system.slab_atoms.calc._continuation_calls == (steps+1)
                         and system.atoms.calc._continuation_calls == 0)

        print('{} : Each integration step calls eta routine once : {}'.format(f, status))
        if not status.ok:
            raise AssertionError

        if friction == 'iaa':
            status.ok = system.atoms.calc._continuation_calls == 0
            print('{} : No calls to atoms continuation calculations even after propagation : {}'.format(f, status))
        else:
            status.ok = system.slab_atoms.calc._continuation_calls == 0 and system.slab_atoms.calc._calls == 0
            print('{} : No calls to slab atoms (continuation) calculations even after propagation : {}'.format(f, status))

        if not status.ok:
            raise AssertionError

        status.ok = system.atoms.calc.param.reuse.value is not None
        if friction == 'iaa':
            status.ok = status.ok and system.slab_atoms.calc.param.reuse.value is not None

        print("{} : Sequential steps reuse previous steps: {}".format(f, status))
        if not status.ok:
            raise AssertionError

        handler.close()

        system = CastepEFSystem(friction = friction, adsorbate_idx=1, restart_traj='{}/MDsim__ase.traj'.format(folder), _verbose=False, calc_folder=folder)

        handler = create_trajectoryhandler(system_inst = system, verbose=False, outfolder=folder)
        # positions are stored only with rather limited precision
        pos_eps = 1e-5
        eps = 1e-10

        status.ok = (np.all(np.abs(pos - system.get_positions()) < pos_eps)
                     and np.all(np.abs(vel - system.get_velocities()) < eps)
                     and np.all(np.abs(forces - system.get_forces()) < eps)
                     and np.all(np.abs(etas - system.get_etas()) < eps)
                     and np.abs(Epot - system.get_potential_energy()) < eps)

        print('{} : System properties are properly restored upon restart : {}'.format(f, status))
        if not status.ok:
            raise AssertionError

        status.ok = system.atoms.calc._calls == steps
        if friction == 'iaa':
            status.ok = (status.ok and system.slab_atoms.calc._calls == steps
                         and system.slab_atoms.calc._continuation_calls == steps)
        else:
            status.ok = status.ok and system.atoms.calc._continuation_calls == steps
        print('{} : Restarting reuses previous information : {}'.format(f, status))
        if not status.ok:
            raise AssertionError

        for step in range(steps):
            nstep+=1
            integrator.propagate(system)
            status.ok = (not np.all(np.abs(pos - system.get_positions()) < pos_eps)
                         and not np.all(np.abs(vel - system.get_velocities()) < eps)
                         and not np.all(np.abs(forces - system.get_forces()) < eps)
                         and not np.all(np.abs(etas - system.get_etas()) < eps)
                         and not np.abs(Epot - system.get_potential_energy()) < eps)

            print('{} : System properties all change after propagation at step {} : {}'.format(f, nstep, status))
            if not status.ok:
                raise AssertionError

            pos = system.get_positions().copy()
            vel = system.get_velocities().copy()
            forces = system.get_forces().copy()
            etas = system.get_etas().copy()
            Epot = system.get_potential_energy()


            handler.push()

        handler.close()

        if show:
            os.system('ase-gui {}/*.traj'.format(folder))

        if remove:
            shutil.rmtree(folder)


if __name__ == '__main__':
    test_castepaimdef(remove=False, show=False, steps=1)
