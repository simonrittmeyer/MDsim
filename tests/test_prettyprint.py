# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
from MDsim.potentials.atomic.harmonicpotential import HarmonicPotential

def test_prettyprint_harmonic():

    Natoms = 2
    Ndim = 2

    masses = np.random.rand(Natoms)
    omegas = np.random.rand(Natoms, Ndim)

    for m in ['analytical', 'numerical']:
        potential = HarmonicPotential(omegas = omegas,
                                      masses = masses,
                                      forces_method = m ,
                                      convert = True)

        print('+'*80)
        print(potential)
        print('+'*80)


def test_prettyprint_bivariatepotential():
    from MDsim.tools.createobjects import create_bivariatepotential

    pes = create_bivariatepotential(datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
                                    verbose = False,
                                    usecols = (0,1,4))

    print('+'*80)
    print(pes)
    print('+'*80)

    pes = create_bivariatepotential(datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
                                    verbose = False,
                                    periodic = True,
                                    usecols = (0,1,4))
    print('+'*80)
    print(pes)
    print('+'*80)

    masses = np.array([12.,16.])
    positions_eq=np.array([[21.22994700],[22.38415200]])
    pes = create_bivariatepotential(datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
                                    coordinates = "diatomic-perp",
                                    periodic = False,
                                    convert=False,
                                    normalize=True,
                                    positions_eq=positions_eq,
                                    masses=masses,
                                    usecols = (0,1,4))
    print('+'*80)
    print(pes)
    print('+'*80)

def test_prettyprint_bivariatefriction():
    from MDsim.tools.createobjects import create_bivariatefrictioncoefficient as create_bivariatefriction

    eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                   verbose = False,
                                   usecols = (0,1,4))

    print('+'*80)
    print(eta)
    print('+'*80)

    eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                   verbose = False,
                                   periodic = True,
                                   usecols = (0,1,4))
    print('+'*80)
    print(eta)
    print('+'*80)

    masses = np.array([12.,16.])
    positions_eq=np.array([[21.22994700],[22.38415200]])

    eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                   coordinates = "diatomic-perp",
                                   periodic = False,
                                   convert=False,
                                   positions_eq=positions_eq,
                                   masses=masses,
                                   usecols = (0,1,4),
                                   bounds_check = False)
    print('+'*80)
    print(eta)
    print('+'*80)

def test_prettyprint_fourierpotential():
    from MDsim.tools.createobjects import create_minimumbasisfourierpotential

    a = 3.610 * np.sqrt(2)

    pes = create_minimumbasisfourierpotential(a = a,
                      datafile = 'data/Na_on_Cu111_adsorption_sites.pot')

    print('+'*80)
    print(pes)
    print('+'*80)

def test_prettyprint_systems():
    from MDsim.systems.system import System
    from MDsim.systems.periodicsystem import PeriodicSystem
    from MDsim.systems.pairwise import PairwiseSystem
    from MDsim.systems.pairwise.kohnlausystem import KohnLauSystem

    Natoms = 10
    Ndim = 2

    positions = np.zeros((Natoms, Ndim))
    velocities = np.zeros_like(positions)
    velocities = None

    species = ['Xe' for i in range(Natoms)]

    # initiate system
    system = System(positions = positions,
                    velocities = velocities,
                    species = species,
                    temperature = abs(float(np.random.randint(1e3))),
                    convert = True)

    print('+'*80)
    print(system)
    print('+'*80)

    # initiate system
    system = PeriodicSystem(positions = positions,
                            velocities = velocities,
                            unit_cell = [[1.2,0],[0,7.43]],
                            species = species,
                            temperature = abs(float(np.random.randint(1e3))),
                            convert = True)

    print('+'*80)
    print(system)
    print('+'*80)

    # initiate system
    system = PairwiseSystem(positions = positions,
                            velocities = velocities,
                            unit_cell = [[120,0],[0,743]],
                            species = species,
                            temperature = abs(float(np.random.randint(1e3))),
                            list_cutoff = 12,
                            force_cutoff = 6,
                            _init_forces = False,
                            convert = True)

    print('+'*80)
    print(system)
    print('+'*80)

    # initiate system
    dipoles = np.ones(Natoms)
    system = KohnLauSystem(positions = positions,
                           velocities = velocities,
                           dipoles = dipoles,
                           unit_cell = [[120,0],[0,743]],
                           species = species,
                           temperature = abs(float(np.random.randint(1e3))),
                           list_cutoff = 12,
                           force_cutoff = 6,
                           _init_forces = False,
                           convert = True)

    print('+'*80)
    print(system)
    print('+'*80)

    # initiate system
    dipoles = np.ones(Natoms)*1.43574
    system = KohnLauSystem(positions = positions,
                           velocities = velocities,
                           dipoles = dipoles,
                           unit_cell = [[120,0],[0,743]],
                           species = species,
                           temperature = abs(float(np.random.randint(1e3))),
                           list_cutoff = 12,
                           force_cutoff = 6,
                           _init_forces = False,
                           convert = True)

    print('+'*80)
    print(system)
    print('+'*80)

def test_prettyprint_integrators():
    from MDsim.tools.createobjects import create_integrator

    ints = ['velocityverlet',
            'liouville',
            'gjf',
            #'bbk',
            'vec',
            'bp']
    for i in ints:
        integrator = create_integrator(method = i, dt = float(abs(np.random.randn())))

        print('+'*80)
        print(integrator)
        print('+'*80)


def test_prettyprint(remove=True):
    from MDsim.prettyprint.interface import IOInterface
    # legacy workaround
    from MDsim.fileio.asciihandler import ASCIIHandler as FileHandler
    from MDsim.observables.implemented import implemented
    from MDsim.tools.createobjects import create_integrator
    from MDsim.tools.createobjects import create_filehandler
    from MDsim.tools.createobjects import create_observable
    from MDsim.friction.constantfriction import ConstantFriction
    from MDsim.systems.pairwise.kohnlausystem import KohnLauSystem


    Natoms = 2
    Ndim = 2

    positions = np.random.randn(Natoms, Ndim)
    velocities = np.zeros_like(positions)
    velocities = None

    masses = np.random.rand(Natoms)
    species = ['Na']*Natoms
    omegas = np.random.rand(Natoms, Ndim)

    potential = HarmonicPotential(omegas = omegas,
                                  masses = masses,
                                  forces_method = 'analytical',
                                  convert = True)
    # initiate system
    dipoles = np.ones(Natoms)*1.43574

    etas = np.abs(np.random.randn(Natoms, Ndim))
    friction = ConstantFriction(etas)
    system = KohnLauSystem(positions = positions,
                           velocities = velocities,
                           dipoles = dipoles,
                           unit_cell = [[120,0],[0,743]],
                           species = species,
                           masses = masses,
                           friction=friction,
                           temperature = abs(float(np.random.randint(1e3))),
                           list_cutoff = 12,
                           force_cutoff = 6,
                           convert = True)


    integrator = create_integrator(method = 'bp',
                                   dt = float(abs(np.random.randn())))

    IO = IOInterface()

    print IO.get_system_info(system)
    print IO.get_integrator_info(integrator)
    print IO.get_units_info()


    for obs in implemented:
        if np.any(o in obs for o in ['scattering_amplitudes', 'isf', 'dsf', 'eta', 'alternative_eta']):
            continue
        O = create_observable(system_inst=system, observable=obs, iatom = 1)
        print O
        print O.get_idstring()
        print O.get_headline()
        print O.get_dataline()

    handlers=[]
    for o in implemented:
        if obs in ['scattering_amplitudes', 'isf', 'dsf', 'alternative_eta']:
            continue
        try:
            handler = create_filehandler(system_inst = system,
                                         integrator_inst = integrator,
                                         observable = o,
                                         iatom = 1,
                                         )

            handler.push()
            handlers.append(handler)
        except ValueError:
            pass
    for step in range(100):
        integrator.propagate(system)
        for handler in handlers:
            handler.push()
    for handler in handlers:
        handler.close()

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass
if __name__ == '__main__':
    test_prettyprint_harmonic()
    test_prettyprint_bivariatepotential()
    test_prettyprint_fourierpotential()
    test_prettyprint_systems()
    test_prettyprint_integrators()
    test_prettyprint(remove=False)
    test_prettyprint_bivariatefriction()
