# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# this test compares the well known solutions for a (damped) harmonic
# oscillator against numerical solutions provided by the integrators.
import numpy as np
from MDsim.tools.createobjects import create_integrator
from MDsim.potentials.atomic.harmonicpotential import HarmonicPotential
from MDsim.systems.system import System
from MDsim.tools.status import Status

def test_harmonic(show=False, Nsteps=1e4):

    omega = 1.23456789e-2
    mass = 9.87654321
    potential = HarmonicPotential(omegas = [[omega]],
                                  masses = [[mass]],
                                  convert = False)
    # force constant
    k = float(potential._pref)
    # amplitude of the oscillation a.k.a. starting position
    A = 4.236

    position = np.array([[A]])
    Nsteps = int(Nsteps)

    dt = .523

    integrators = [#'Gronbech-Jensen/Farago',
                   # our BBK implementation seems broken
                   #'Bruenger/Brooks/Karplus',
                   #'Vanden-Eijnden/Ciccotti',
                   #'Bussi/Parrinello',
                   #'Liouville',
                   'VelocityVerlet'
                   ]
    status = Status()

    for iname in integrators:
        system = System(positions = position,
                        masses = [mass],
                        species = ['dummy'],
                        velocities = np.zeros_like(position),
                        temperature = 0.,
                        friction = None,
                        potential = potential,
                        convert = False,
                        _verbose = False)

        integrator = create_integrator(method=iname,
                                       dt=dt,
                                       verbose=False,
                                       convert=False)
        time = np.zeros(Nsteps)
        exact = np.zeros(Nsteps)
        numerical = np.zeros(Nsteps)
        energy = np.zeros(Nsteps)

        for step in range(Nsteps):
            time[step] = system.get_time(_au=True)
            exact[step] = A * np.cos(omega * time[step])
            numerical[step] = system.get_positions(_au=True).ravel()
            energy[step] = system.get_total_energy(_au=True)

            integrator.propagate(system)
        devs = np.abs(exact-numerical)

        eps = 1e-3
        status.ok = np.max(devs) < eps
        print('{} // harmonic oscillator numerical trajectory stability : {}'.format(iname, status))

        eps = 1e-6
        status.ok = np.max(np.abs(energy-energy[0])) < eps
        print('{} // harmonic oscillator numerical total energy stability : {}'.format(iname, status))
        if not status.ok:
            raise AssertionError

        if show:
            import matplotlib.pyplot as plt
            plt.plot(time, exact, label='exact')
            plt.plot(time, numerical, label='numerical')
            plt.title(iname)
            plt.xlabel('time')
            plt.ylabel('amplitude')
            plt.legend()
            plt.show()

def test_harmonic_damped(show=False, Nsteps=1e4):
    omega = 1.23456789e-2
    mass = 9.87654321
    potential = HarmonicPotential(omegas = [[omega]],
                                  masses = [[mass]],
                                  convert = False)
    # force constant
    k = float(potential._pref)
    # amplitude of the oscillation a.k.a. starting position
    A = 4.236
    position = np.array([[A]])
    Nsteps = int(Nsteps)

    dt = 0.523

    integrators = ['Gronbech-Jensen/Farago',
                   # our BBK implementation seems broken
                   #'Bruenger/Brooks/Karplus',
                   'Vanden-Eijnden/Ciccotti',
                   'Bussi/Parrinello',
                   'Liouville',
                   ]

    status = Status()

    for iname in integrators:
        for zeta in [2.5, 1., 5.0e-5, 0.]:
            eta = 2 * zeta * omega * mass
            system = System(positions = position,
                            masses = [mass],
                            species = ['dummy'],
                            velocities = np.zeros_like(position),
                            temperature = 0.,
                            friction = eta,
                            potential = potential,
                            convert = False,
                            _verbose = False)

            integrator = create_integrator(method=iname,
                                           dt=dt,
                                           verbose=False,
                                           convert=False)
            time = np.zeros(Nsteps)
            exact = np.zeros(Nsteps)
            numerical = np.zeros(Nsteps)
            energy = np.zeros(Nsteps)

            # initiate the exact solutions

            # just lazyness, as the lower part is legacy code
            pos = float(position)
            vel = 0
            # see MDsim v1... yes the ancient one (HarmonicFrictionParticle.py)
            if zeta > 1.:
                gamma_plus = 0.5*(-2*zeta*omega + np.sqrt(4*omega**2*zeta**2 - 4*omega**2))
                gamma_minus = 0.5*(-2*zeta*omega - np.sqrt(4*omega**2*zeta**2 - 4*omega**2))
                A = pos + (pos*gamma_plus - vel) / (gamma_minus - gamma_plus)
                B = - (pos*gamma_plus - vel) / (gamma_minus - gamma_plus)
                info = 'overdamped system'

                def pos_exact(t):
                    return A * np.exp(gamma_plus*t) + B * np.exp(gamma_minus * t)

            elif zeta == 1.0:
                A = pos
                B = vel + omega*pos
                info = 'critically damped system'

                def pos_exact(t):
                    return (A + B*t)*np.exp(-omega*t)

            elif zeta < 1e-20:
                A = pos
                info = 'free system'

                def pos_exact(t):
                    return A * np.cos(omega * t)

            elif zeta < 1.:
                omega_d = omega * np.sqrt(1-zeta**2)
                A = pos
                B = 1. / omega_d*(zeta * omega * pos + vel)
                info = 'underdamped system'

                def pos_exact(t):
                    return (A*np.cos(omega_d * t)+B*np.sin(omega_d * t)) * np.exp(-zeta * omega*t)

            Ediss = 0
            for step in range(Nsteps):
                time[step] = system.get_time(_au=True)
                numerical[step] = system.get_positions(_au=True).ravel()
                energy[step] = system.get_total_energy(_au=True) + Ediss
                integrator.propagate(system)
                Ediss += float(system.get_velocities(_au=True))**2*eta * dt

            exact = pos_exact(time)
            devs = np.abs(exact-numerical)

            eps = 1e-3
            status.ok = np.max(devs) < eps
            print('{} // harmonic oscillator ({}) numerical trajectory stability : {}'.format(iname, info, status))

            eps = 5e-5
            status.ok = np.max(np.abs(energy-energy[0])) < eps
            print('{} // harmonic oscillator ({}) numerical (Ediss + Etot) stability : {}'.format(iname, info, status))
            if not status.ok:
                raise AssertionError

            if show:
                import matplotlib.pyplot as plt
                plt.plot(time, exact, label='exact')
                plt.plot(time, numerical, label='numerical')
                plt.title('{} ({})'.format(iname, info))
                plt.xlabel('time')
                plt.ylabel('amplitude')
                plt.show()

if __name__ == '__main__':
    test_harmonic(show=False)
    test_harmonic_damped(show=False)
