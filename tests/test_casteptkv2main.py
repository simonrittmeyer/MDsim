# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil

def test_casteptkv2traj_main(show=False, remove=True, time=1000):
    from MDsim.mainroutines.castepTKv2traj import main
    from MDsim.mainroutines.castepTKv2traj.options import get_options_dict
    from MDsim.tools.options import create_args_dict

    args = create_args_dict(get_options_dict())
#    for arg in sorted(args.keys()):
#        print(arg, args[arg])

    args['system.species'] = 'ddist'
    args['system.masses'] = [12.]
    args['system.initial_energy'] = 1
    args['system.initial_energy_unit'] = 'quanta'
    args['system.traj_type'] = 'vibration'
    args['potential.type'] = 'interpolated'
    args['potential.potfile'] = 'data/PES_11x11x1_kpoints.pot'
    args['potential.potfile_usecols'] = [1,2]

    args['io.debug'] = True
    args['io.fileformat'] = 'ascii'
    args['io.iostep'] = 1
    args['io.outpath'] = 'output'

    args['propagation.simulation_time'] = time
    args['propagation.timestep'] = 1

    main(infile=None,
         py_args=args)

    if show:
        data = np.loadtxt('output/MDsim__CASTEPTKV2_TRAJ.dat')
        import matplotlib.pyplot as plt
        plt.plot(data.T[0], data.T[1], label='Qdot')
        plt.plot(data.T[0], data.T[2], label='Q')
        plt.show()

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass

def test_casteptkv2traj_main_diffusion(show=False, remove=True, time=1000):
    from MDsim.mainroutines.castepTKv2traj import main
    from MDsim.mainroutines.castepTKv2traj.options import get_options_dict
    from MDsim.tools.options import create_args_dict

    args = create_args_dict(get_options_dict())
#    for arg in sorted(args.keys()):
#        print(arg, args[arg])

    args['system.species'] = 'ddist'
    args['system.masses'] = [22.989769]
    args['system.initial_energy'] = 0.11
    args['system.initial_energy_unit'] = 'ev'
    args['system.traj_type'] = 'diffusion'
    args['potential.type'] = 'interpolated'
    args['potential.potfile'] = 'data/PES_hollow-bridge-hollow.pot'
    args['potential.potfile_usecols'] = [0,5]

    args['io.debug'] = True
    args['io.fileformat'] = 'ascii'
    args['io.iostep'] = 1
    args['io.outpath'] = 'output'

    args['propagation.simulation_time'] = time
    args['propagation.timestep'] = 1

    main(infile=None,
         py_args=args)

    if show:
        data = np.loadtxt('output/MDsim__CASTEPTKV2_TRAJ.dat')
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(2,1,2)
        ax.plot(data.T[0], data.T[2], label='Q')
        ax2.plot(data.T[0], data.T[1], label='Qdot')
        ax.set_ylabel('Q')
        ax2.set_ylabel('Qdot')
        for a in [ax, ax2]:
            a.set_xlabel(time)

        plt.show()

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass
if __name__ == '__main__':
    test_casteptkv2traj_main_diffusion(remove=False, show=True)
    test_casteptkv2traj_main(remove=False, show=True)
