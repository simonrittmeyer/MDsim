# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

def test_supercell(show=False):
    import numpy as np
    from MDsim.potentials.atomic.periodic.diffusion.fcc111 import MinimumBasisFourierPotentialFCC111
    import MDsim.coordinates.diffusion.fcc111 as coords

    from MDsim import tools
    from MDsim.tools.status import Status

    status = Status()

    a = 3.610 / np.sqrt(2)
    # numbers from J. M. Carlsson and B. Hellsing, Phys. Rev. B 61, 13973 (2000)
    # for 2x2 cells
    energies = {'top' : -1.897,
                'bridge' : -1.939,
                'fcc' : -1.944,
                'hcp' : -1.944}

    pot = MinimumBasisFourierPotentialFCC111(a, energies, convert = False)

    coverage = 0.0025
    saturation_coverage = 4./9

    Natoms = 10000

    supercell_dict = coords.create_surface_supercell(a=a,
                                                     coverage=coverage,
                                                     saturation_coverage=saturation_coverage,
                                                     fix_Natoms=Natoms)


    # check if periodicity is compatible with underlying potential
    supercell = supercell_dict['supercell']

    # place positions also outside the box
    positions = np.random.rand(Natoms, 2)*np.diag(supercell)[None,:]*2

    # wrap them back
    positions_pbc = tools.periodic.wrap_positions_orthorhombic(positions, supercell)

    eps =1e-15
    status.ok = np.allclose(pot.get_forces(positions), pot.get_forces(positions_pbc), eps)
    print('Supercell periodicity is compatible with primitive unit cell : {}'.format(status))

    if not status.ok:
        raise AssertionError

    if show:
        import matplotlib.pyplot as plt
        import matplotlib.patches as patches

        # plotting
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        Natomss = [5,10,20,30,40,50]

        Natoms = Natomss[-1]
        coverage = 0.5
        supercell_dict = coords.create_surface_supercell(a=a,
                                                         coverage=coverage,
                                                         saturation_coverage=saturation_coverage,
                                                         fix_Natoms=Natoms)


        # check if periodicity is compatible with underlying potential
        supercell = supercell_dict['supercell']

        npts=1000

        xlim = [-0.5 * supercell[0, 0], 1.5*supercell[0, 0]]
        ylim = [-0.5 * supercell[1, 1], 1.5*supercell[1, 1]]

        # creating the x/y points
        x = np.linspace(xlim[0], xlim[1], npts)
        y = np.linspace(ylim[0], ylim[1], npts)

        X, Y = np.meshgrid(x, y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        # evaluating using vectorized call ;)
        Z = pot.get_Epots(points).reshape(npts, npts)


        cmap = plt.get_cmap("Blues")
        surf = ax.pcolormesh(X, Y, Z, cmap=cmap)
        # color bar
        cbar = plt.colorbar(surf, format="%.6f")
        cbar.set_label(r'f(x,y) (a.u.)')

        #cont = ax.contour(X, Y, Z, colors='lightgray')


        # axes limits and labels
        ax.set_xlim(*xlim)
        ax.set_ylim(*ylim)
        ax.set_xlabel('x')
        ax.set_ylabel('y')

        ax.set_aspect('equal')

        for Natoms in Natomss:
            supercell_dict = coords.create_surface_supercell(a=a,
                                                             coverage=coverage,
                                                             saturation_coverage=saturation_coverage,
                                                             fix_Natoms=Natoms)


            # check if periodicity is compatible with underlying potential
            supercell = supercell_dict['supercell']
            ax.add_patch(patches.Rectangle(xy=(0,0),
                                           width=supercell[0, 0],
                                           height=supercell[1, 1],
                                           color='red',
                                           alpha=0.2))


        plt.show()
if __name__ == '__main__':
    test_supercell(show=True)
