# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Tests for the tensorial friction propagation implemented in Nov. 2016

We now actually use the unittest framework.
"""

import os
import unittest
import shutil
import tempfile

import numpy as np

from MDsim.friction.frictiontensor import FrictionTensor
from MDsim.coordinates.frictiontensor import FrictionModes

from MDsim.tools import is_diagonal_matrix

from MDsim import units

class TestFrictionTensor(unittest.TestCase):
    """
    Test functionality of coordinate transforms and friction tensor behavior
    """

    # hard coded masses from CASTEP
    m_C = 12.0107000
    m_O = 15.9994000

    # first coordinate is dRcom, second is ddist
    masses = np.array([m_C, m_O])

    # these are the Cz and Oz elements of Askerka's friction tensor in mass-weighted coordinates
    # M. Askerka, R. J. Maurer, V. S. Batista and J. C. Tully, Phys. Rev. Lett. 116, 217601 (2016). i
    # http://www.dx.doi.org/10.1103/PhysRevLett.116.217601
    friction_tensor_mw = np.array([[0.245, -0.075],
                                   [-0.075,0.034]])*1e-3


    def test_coordinates_init(self):
        coords = FrictionModes(self.friction_tensor_mw, self.masses, convert=True)

    def test_coordinates_eigenbasis(self):
        coords = FrictionModes(self.friction_tensor_mw, self.masses, convert=True)
        self.assertTrue(is_diagonal_matrix(coords.gamma_mw))

        # compare the values along the friction modes
        Askerka_vals = np.array([108., 3.70])
        FM = 1./np.diag(coords.gamma_mw) * units.AU_TO_FS * 1e-3

        # increased tolerance as we only have the reduced two dimensional version
        self.assertTrue(np.allclose(FM, Askerka_vals, rtol = 0.10))

    def test_tensor_init(self):
        tensor = FrictionTensor(self.friction_tensor_mw, self.masses, convert=True)

        self.assertTrue(tensor.Ndim == 1)
        self.assertTrue(tensor.Natoms == 2)

    def test_tensor_call(self):
        tensor = FrictionTensor(self.friction_tensor_mw, self.masses, convert=True)

        self.assertTrue(is_diagonal_matrix(tensor.get_friction_tensor_eigenbasis_mw()))
        self.assertTrue(is_diagonal_matrix(tensor.get_friction_tensor_eigenbasis()))
        self.assertTrue(not is_diagonal_matrix(tensor.get_friction_tensor_mw()))
        self.assertTrue(not is_diagonal_matrix(tensor.get_friction_tensor()))

        # compare the values along the friction modes
        Askerka_vals = np.array([108., 3.70])
        FM = 1./np.diag(tensor.get_friction_tensor_eigenbasis_mw(_au=True)) * units.AU_TO_FS * 1e-3

        # increased tolerance as we only have the reduced two dimensional version
        self.assertTrue(np.allclose(FM, Askerka_vals, rtol = 0.10))

    def test_tensor_velocity_transform(self):
        tensor = FrictionTensor(self.friction_tensor_mw, self.masses, convert=True)
        velocities = np.random.rand(2,1)

        q=tensor.velocities_cartesian_to_frictionmodes(velocities)
        v=tensor.velocities_frictionmodes_to_cartesian(q)

        self.assertTrue(np.allclose(velocities, v))


if __name__ == '__main__':
    unittest.main()
