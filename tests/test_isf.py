# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil
import random

from MDsim import units
from MDsim.systems.system import System

from MDsim.friction.scalarfriction import ScalarFriction

from MDsim.tools.createobjects import create_minimumbasisfourierpotential
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_filehandler


from MDsim.analysis.isf import calc_scattering_amplitudes
from MDsim.analysis.isf import calc_isf
from MDsim.analysis.isf import read_file_ScatteringAmplitude

from MDsim.analysis.fitting import tune_cutoff

from MDsim.tools.status import Status
from MDsim.tools.netcdf import load_netcdf


def test_ISF(nsteps = 1e3, Natoms=2, Nkpts=5, show_traj=False, show_fit=False,
             show_ISF=False, show_signature=False, remove=True):
    verbose=True
    status = Status()

    print('Running simple test simulation...', end='')
    a = 3.610 / np.sqrt(2)
    pes = create_minimumbasisfourierpotential(a=a,
                                              datafile='data/Na_on_Cu111_adsorption_sites.pot',
                                              verbose=verbose)

    Ndim = 2

    positions = np.random.randn(Natoms, Ndim)
    species = ['Na' for i in range(Natoms)]

    velocities = np.zeros_like(positions)
    eta = ScalarFriction(eta = .005, convert = False)
    T = 155
    #pes=None

    system = System(positions = positions,
               species = species,
               velocities = velocities,
               temperature = T,
               friction = eta,
               potential = pes,
               convert = True,
               _verbose=verbose)

    # hard coded from DJW's mail to maintain comparability
    delta_K = np.vstack((np.linspace(0,2,Nkpts), np.linspace(0,2,Nkpts))).T
    angle = np.pi/3
    delta_K[:,0] *= np.cos(angle)
    delta_K[:,1] *= np.sin(angle)

    integrator = create_integrator(method='bussi parrinello',
                                   dt=10,
                                   verbose=verbose)

    handlers=[]
    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='netcdf',
                                       observable='positions',
                                       verbose=verbose))
    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='netcdf',
                                       observable='scattering_amplitudes',
                                       delta_K = delta_K,
                                       convert = True,
                                       verbose=verbose))

    for n in range(int(nsteps)):
        integrator.propagate(system)
        for handler in handlers:
            handler.push()

    for handler in handlers:
        handler.close()

    print('done')

    filename = 'output/MDsim__POSITIONS.nc'

    print('Reading trajectory data...'.format(filename), end='')
#    data = np.loadtxt(filename)
    data = load_netcdf(filename)
    print('done')

    time = data[:,0]
    positions = data[:,1::]

    Ntimes = data.shape[0]
    Nparticles = positions.shape[1] / 2

    # reshaping things a bit
    positions = positions.reshape((len(time), Nparticles, 2))

    if show_traj:
        print('Rendering trajectories plot')
        import matplotlib.pyplot as plt
        for i in range(Nparticles):
            plt.plot(positions[:,i,0], positions[:,i,1])
        plt.show()


    print('Reading on-the-fly scattering amplitudes...', end='')
    fname = 'output/MDsim__SCATTERING_AMPLITUDES.nc'
    times, otf_amplitudes = read_file_ScatteringAmplitude(fname)
    print('done')

    print('Evaluating ISFs...', end='')

    zero_padding = True
    chop = True

    times = np.arange(len(otf_amplitudes[:,0]))
    time_zp, ISFs_zp = calc_isf(times, otf_amplitudes, autocorrelation='linear', optimize_length=chop, verbose=True)
    time_nzp, ISFs_nzp = calc_isf(times, otf_amplitudes, autocorrelation='cyclic', optimize_length=chop, verbose=True)


    print('done')

    # fit the exponential decay
    alphas = np.zeros((Nkpts))
    skip=0
    print('Fitting decay rates (skipping first {} entries)...'.format(skip), end='')
    for k in range(Nkpts):
        # alphas[k] = fit_exponential_decay(time[skip:], ISFs[skip:,k].real, end = len(time[skip:])-20, show=show_fit)
        alphas[k] = tune_cutoff(time_zp[skip:], ISFs_zp[skip:,k].real, end=len(time_zp[skip:])-20, max_cutoff = 20, stepsize=10, show = False)
    print('done')

    if show_ISF:
        import matplotlib
        import matplotlib.pyplot as plt
        print('Rendering ISF plot')
        #time -= max(time)/2.
        real_map = plt.get_cmap('Reds')
        imag_map = plt.get_cmap('Blues')

        norm = matplotlib.colors.Normalize(-1, Nkpts+1)

        for k in range(Nkpts):
            ISF = ISFs_zp[:,k]
            time = time_zp
            plt.plot(time, ISF.real, color=real_map(norm(k)))
            plt.plot(time, ISF.imag, color=imag_map(norm(k)))

            ISF = ISFs_nzp[:,k]
            time = time_nzp
            plt.plot(time, ISF.real, color=real_map(norm(k)), ls = '--', lw=2)
            plt.plot(time, ISF.imag, color=imag_map(norm(k)), ls = '--', lw=2)

            plt.xlabel('time / fs')
            plt.ylabel('ISF(dK, t) / fs')

            plt.show()

    # if show_signature:
        # import matplotlib
        # import matplotlib.pyplot as plt
        # print('Rendering signature plot')

        # plt.plot(range(Nkpts), alphas, color = 'red', lw = 2)
        # plt.show()

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass


if __name__ == '__main__':
    test_ISF(nsteps = 2**15, Natoms=50, Nkpts=10, remove=False, show_traj=False, show_fit=True, show_ISF=True, show_signature=True)
