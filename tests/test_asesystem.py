# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase


from MDsim import units

from MDsim.bindings.castep import ExtendedCastep
from MDsim.bindings.ase.asetomdsim import ASESystem

from MDsim.tools.status import Status

def test_asesystem():
    try:
        from ase.test.castep import installed
        installed()
    except Exception:
        print('Either ase or castep are not installed, skipping test...')
        return
    status = Status()

    atoms = ase.Atoms('H2', [[0, 0, 0], [0, 0, 1.1]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep(verbose = False)

    calc._label = 'H2'
    calc._directory = 'output/castep_run'

    if os.path.isdir(calc._directory):
        shutil.rmtree(calc._directory)
    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'
    system = ASESystem(atoms)

    eps = 1e-10

    pos = system.get_positions()
    status.ok =  np.all(np.abs(pos - atoms.get_positions()) < eps)
    print('Initial system positions == atoms positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    mom = system.get_momenta(_au=True)*units.MOMENTUM_AU_TO_ASE
    status.ok =  np.all(np.abs(mom - atoms.get_momenta()) < eps)
    print('Initial system momenta == atoms momenta : {}'.format(status))
    if not status.ok:
        raise AssertionError

    try:
        forces = system.get_forces()
        status.ok =  True
    except StandardError:
        forces = np.empty_like(pos)
        status.ok =  False

    print('Evaluating forces: {}'.format(status))
    if not status.ok:
        raise AssertionError

    try:
        Epot = system.get_potential_energy()
        status.ok =  True
    except StandardError:
        Epot = -1234567890
        status.ok =  False

    print('Evaluating energy: {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = system.atoms.calc._calls == 1
    print('Force and energy evaluation use same underlying QM calculation : {}'.format(status))
    if not status.ok:
        raise AssertionError


    status.ok =  not np.all([system._update_Epot, system._update_forces, system._update_etas])
    print('Evaluating energies/forces sets reuse flag : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok =  abs(system.calc_potential_energy(system.get_positions()*units.ANGSTROM_TO_AU)*units.AU_TO_EV - Epot) < eps
    status.ok =  status.ok and system.atoms.calc._calls == 1
    print('Reusing previously calculated energies : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok =  np.all(np.abs(system.calc_forces(system.get_positions()*units.ANGSTROM_TO_AU)*units.AU_TO_EV_PER_ANGSTROM - forces) < eps)
    status.ok =  status.ok and system.atoms.calc._calls == 1
    print('Reusing previously calculated forces : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.set_positions(system.get_positions()*units.ANGSTROM_TO_AU + 1.)

    status.ok =  np.all([system._update_Epot, system._update_forces, system._update_etas])
    print('Updating positions updates reuse flags : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.set_velocities(system.get_velocities() * units.ANGSTROM_PER_FS_TO_AU + 1.)
    status.ok =  np.all([system._update_Ekin])
    print('Updating velocities updates reuse flags : {}'.format(status))
    if not status.ok:
        raise AssertionError

    pos = system.get_positions()
    status.ok =  np.all(np.abs(pos - system.atoms.get_positions()) < eps)
    print('Updated system positions == atoms positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    mom = system.get_momenta(_au=True)*units.MOMENTUM_AU_TO_ASE
    status.ok =  np.all(np.abs(mom - system.atoms.get_momenta()) < eps)
    print('Updated system momenta == atoms momenta : {}'.format(status))
    if not status.ok:
        raise AssertionError

    forces = system.get_forces()
    Epot = system.get_potential_energy()
    status.ok =  system.atoms.calc._calls == 2

    print('Changing positions requires new calculator call : {}'.format(status))
    if not status.ok:
        raise AssertionError

if __name__=='__main__':
    test_asesystem()

