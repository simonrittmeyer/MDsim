# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from MDsim import tools
from MDsim.tools.status import Status

def test_check_kwargs():
    kwargs = {'one' : 1,
              'two' : 2,
              'three' : 3}

    param = {'one' : 3,
             'three' : 1}

    status = Status()

    try:
        tools.check_kwargs(kwargs, param)
        status.ok = False
    except ValueError:
        status.ok = True

    print('check_kwargs // undefined arguments are rejected : {}'.format(status))

    kwargs.pop('two')

    status.ok = tools.check_kwargs(kwargs, param)

    print('check_kwargs // defined arguments are correctly transfered : {}'.format(status))


def test_walltime():
    status = Status()
    for i in ['24:24:24', 87864, "87864", 87864.0]:
        walltime = tools.convert_walltime(i)
        status.ok = (isinstance(walltime, float) and walltime == 87864.)
        print('convert_walltime // walltime conversion: {}'.format(status))

    for i in ['', None, False]:
        walltime = tools.convert_walltime(i)
        status.ok = walltime is None

        print('convert_walltime // no walltime: {}'.format(status))

    for i in ['23:0', True]:
        try:
            walltime = tools.convert_walltime(i)
            status.ok = False
        except ValueError:
            status.ok = True
        print('convert_walltime // wrong arguments are rejected : {}'.format(status))



if __name__ == '__main__':
    test_check_kwargs()
    test_walltime()
