# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

def test_normalmodepumping(show=True):
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential
    from MDsim import units
    from MDsim.tools.status import Status
    # masses from CASTEP
    m_C = 12.01070000
    m_O = 15.9994

    masses = np.array([m_C, m_O])
    positions = np.zeros((2,1))
    datafile = 'data/CO_on_Cu100_c2x2.pot'

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "generic-dof",
                                    periodic = False,
                                    convert=True,
                                    normalize=True,
                                    verbose=False,
                                    forces_method = 'analytical',
                                    masses=masses.copy(),
                                    usecols = (0,2,4))
    Natoms = 2
    Ndim = 1


    omega, nm, pos_min, Modes = evaluate_normalmodes_on_potential(potential=pot,
                                                                  positions=positions,
                                                                  masses=masses,
                                                                  minimize_first=True,
                                                                  delta = 0.01,
                                                                  verbose=False,
                                                                  convert = True)

    idx = [0,1]
    quanta = np.array([2,5])

    velocities, info = Modes.pump_modes_kinetic_energy(idx, quanta, verbose=False, _au=True)

    status = Status()
    status.ok = np.allclose(np.sum(velocities.ravel()**2 * masses.ravel()*units.AMU_TO_AU * 0.5), np.sum(quanta*omega*units.EV_TO_AU))
    print('Kinetic energy is conserved upon transformations : {}'.format(status))
    if not status.ok:
        raise AssertionError

    print(Modes.print_Q())

if __name__ == '__main__':
    test_normalmodepumping()
