# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil

def test_vibdamp_main(show=False, remove=True, time=1000):
    from MDsim.mainroutines.vibrations import main
    from MDsim.mainroutines.vibrations.options import get_options_dict
    from MDsim.tools.options import create_args_dict

    args = create_args_dict(get_options_dict())
    args = {'system.species' : ['C', 'O'],
            'system.init_quanta' : [0, 1],
            'system.init_velocities_sign' : 'random',
            'system.positions_eq' : np.array([21.22994700, 22.38415200]),
            'io.observables' : ['positions', 'energy', 'energy_normalmodes'],
            'potential.type' : 'harmonic',
            'potential.potfile' : 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
            'potential.potfile_usecols' : [0,1,4],
            'potential.normalmodes_stepwidth' : 0.1,
            'potential.coordinates' : 'diatomic-perp',
            'io.debug' : True,
            'propagation.simulation_time': time,
            'propagation.timestep' : .1,
            'friction.type' : 'interpolated',
            'friction.coordinates' : 'diatomic-perp',
            'friction.etafiles' : ['data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top-AIM.eta',
                                   'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top-AIM.eta'],
            'friction.etafiles_usecols' : [[0,1,6],[0,1,7]]
            }
    main(py_args=args)

if __name__ == '__main__':
    test_vibdamp_main(show=False, time=1e4)
