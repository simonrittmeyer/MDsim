# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.tools.createobjects import create_univariatepotential
from MDsim.systems.system import System
from MDsim.analysis.normalmodes import evaluate_normalmodes_of_system
from MDsim.coordinates.normalmodes import Normalmodes


def test_univariatepotential(show=False):
    potential = create_univariatepotential(datafile='data/PES_11x11x1_kpoints.pot',
                                     usecols=(1,2))

    if show:
        potential.show()

    # now try to find the minimum, for this reason, we create a temp system
    system = System(positions=np.zeros((1,1)),
                    velocities=np.zeros((1,1)),
                    masses=[6.8606],
                    species=['ddist'],
                    potential=potential,
                    friction=None,
                    convert=True,
                    _verbose=True)


    # this yields the true minimum in a.u. and the normal modes at the same time
    # Nelder-Mead a.k.a. Downhill Simplex is just fine for simple downhill search
    omegas, nm, positions_min, Modes = evaluate_normalmodes_of_system(system=system,
                                                                      # makes the routine find the true minimum
                                                                      positions=None,
                                                                      method='BFGS')

if __name__ == '__main__':
    test_univariatepotential(show=True)
