# This file is part of MDsim.
#
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
#
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_maxwellboltzmann(show = False):
    import numpy as np
    from MDsim.tools.maxwellboltzmann import draw_velocities
    from MDsim.tools.maxwellboltzmann import MaxwellBoltzmann
    from MDsim import units
    # mimic helium
    m_He = 4.002602*units.AMU_TO_AU

    T=300*units.KELVIN_TO_AU

    c = 1

    if show:
        import matplotlib.pyplot as plt
        fig = plt.figure()

    for Natoms in [1e2, 1e3, 1e6]:
        for Ndim in [1,2,3]:
            masses = np.ones(int(Natoms))*m_He
            vv = draw_velocities(masses, Ndim, T, convert=False)

            # the magnitudes
            v = np.sqrt(np.sum(vv**2, axis =1))
            mb = MaxwellBoltzmann(m_He, T, Ndim, convert=False)

            if show:
                ax = fig.add_subplot(3,3, c)
                bins = np.arange(0,2e-3,5e-5)
                # normed = True gives density values
                ax.hist(v, bins,
                         normed = True,
                         color = 'CornflowerBlue')

                # the continuous function
                vs = np.linspace(0,2e-3, 100)

                ax.plot(vs, mb(vs),
                         lw = 2,
                         color = 'red')

                ax.set_xlabel('velocity / a.u.')
                ax.set_ylabel('f(v)')

                velocities = vv
                Ekin = 0.5 * np.einsum('i,i->', masses, np.einsum('ij,ij->i', velocities, velocities))
                Tinst = 2*Ekin/(Ndim*Natoms)*units.AU_TO_KELVIN
                ax.text(0.5,0.7,
                        'Ndim = {0:d}\nNatoms = {1:.0e}\nT = {2:.2f}'.format(Ndim, Natoms, Tinst),
                        transform = ax.transAxes,
                        ha = 'left',
                        va = 'top')
                c +=1
    if show:
        plt.show()

if __name__ == '__main__':
    test_maxwellboltzmann(show = True)
