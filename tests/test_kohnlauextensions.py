# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np
import time
from MDsim.neighbors import Neighbors
from MDsim.potentials.pairwise.kohnlaupotential import KohnLauPotential
from MDsim.tools.status import Status
from MDsim import units

def test_kohnlauextension(Natoms=3, show=False):
    pos = np.random.randn(Natoms, 2)

    cell_length = 10.
    cell = cell_length*np.array([[1, 0],[0,1]])

    pos[:,0] *= cell[0,0]
    pos[:,1] *= cell[1,1]


    status = Status()

    nn = Neighbors(unit_cell=cell)

    list_cutoff = cell_length*0.2

    dipoles = np.ones(Natoms) * 1.22
    pot = KohnLauPotential(force_cutoff = list_cutoff*0.8,
                           Natoms=Natoms,
                           Ndim=2,
                           dipoles=dipoles,
                           unit_cell=cell)

    neighbor_lists, num_neighbors = nn.construct_neighbor_lists(pos, list_cutoff*units.ANGSTROM_TO_AU)


    e_ref='pot._calc_Epot_bruteforce_python(pos)'
    f_ref='pot._calc_forces_bruteforce_python(pos)'

    e_funcs = ['pot._calc_Epot_neighborlist_python(pos, neighbor_lists, num_neighbors)']
    f_funcs = ['pot._calc_forces_neighborlist_python(pos, neighbor_lists, num_neighbors)']

    try:
        from MDsim.speedup import f90support
        print('f90support available')
        e_funcs.append('pot._calc_Epot_bruteforce_fortran(pos)')
        e_funcs.append('pot._calc_Epot_neighborlist_fortran(pos, neighbor_lists, num_neighbors)')
        f_funcs.append('pot._calc_forces_bruteforce_fortran(pos)')
        f_funcs.append('pot._calc_forces_neighborlist_fortran(pos, neighbor_lists, num_neighbors)')
    except ImportError:
        print('f90 support *not* available')

    try:
        from MDsim.speedup import numbasupport
        e_funcs.append('pot._calc_Epot_bruteforce_numba(pos)')
        e_funcs.append('pot._calc_Epot_neighborlist_numba(pos, neighbor_lists, num_neighbors)')
        f_funcs.append('pot._calc_forces_bruteforce_numba(pos)')
        f_funcs.append('pot._calc_forces_neighborlist_numba(pos, neighbor_lists, num_neighbors)')
    except ImportError:
        print('numba support *not* available')

    try:
        from MDsim.speedup import cythonsupport
        print('cython support available')
        e_funcs.append('pot._calc_Epot_bruteforce_cython(pos)')
        e_funcs.append('pot._calc_Epot_neighborlist_cython(pos, neighbor_lists, num_neighbors)')
        f_funcs.append('pot._calc_forces_bruteforce_cython(pos)')
        f_funcs.append('pot._calc_forces_neighborlist_cython(pos, neighbor_lists, num_neighbors)')
    except ImportError:
        print('cython support *not* available')



    eps = 1e-10

    for f, funclist, reffunc in [('energy', e_funcs, e_ref),
                                 ('forces', f_funcs, f_ref)]:
        print('Checking against plain python reference implementation ({})'.format(f))
        ref = eval(reffunc)
        for func in funclist:
            val = eval(func)
            time.sleep(1)
            status.ok = np.allclose(ref, val, eps)
            print('\t{} : {}'.format(func.replace('pos.','').replace('pos','').replace(', neighbor_lists, num_neighbors',''), status))
            if not status.ok:
                mask = np.abs(ref - val) < eps
                print('Reference: \n\t', ref[mask])
                print('Obtained: \n\t', val[mask])
                raise AssertionError
    if show:
        nn.show_neighbor_list(positions=pos, iatom=None, list_cutoff=list_cutoff)

if __name__ == '__main__':
    test_kohnlauextension(Natoms=100, show=True)
