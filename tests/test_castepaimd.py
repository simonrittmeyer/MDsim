# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase

from ase.calculators.aims import Aims
from ase.calculators.castep import Castep

from MDsim.bindings.castep import CastepSystem
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_trajectoryhandler

from MDsim.tools.status import Status

def test_castepaimd(remove=True, show=False, steps=1):
    castep_folder = 'output/castep_runs/'
    try:
        from ase.test.castep import installed
        installed()
    except Exception:
        print('Either castep or ase is not installed, skipping test...')
        return
    status = Status()
    atoms = ase.Atoms('H2')
    calc = Aims()
    atoms.set_calculator(calc)

    try:
        CastepSystem(atoms=atoms, calc_folder=castep_folder)
        status.ok = False
    except ValueError:
        status.ok = True

    print('CastepSystem rejects wrong calculator: {}'.format(status))
    if not status.ok:
        raise AssertionError

    try:
        shutil.rmtree('output')
    except OSError:
        pass

    atoms = ase.Atoms('H2', [[0, 0, 0], [0, 0, 1.2]], cell=[5, 5, 10])
    atoms.center()
    calc = Castep()

    calc._label = 'H2'
    calc._rename_existing_dir = False
    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    integrator = create_integrator(method='velocity verlet', dt=1, seed=None, verbose=True)
    system = CastepSystem(atoms=atoms, _verbose=True, calc_folder=castep_folder)
    handler = create_trajectoryhandler(system_inst = system, verbose=True)


    status.ok = system.atoms.calc.param.reuse.value is None
    print("Initial step is *not* a reuse-calculation: {}".format(status))
    if not status.ok:
        raise AttributeError

    nstep = 0

    for step in range(steps):
        nstep+=1
        integrator.propagate(system)
        handler.push()

    # the '+' accounts for the initial evaluation
    status.ok = system.get_Ncalls() == (steps+1)
    print('Each integration step calls force routine once : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = system.atoms.calc.param.reuse.value is not None
    print("Sequential steps reuse previous steps: {}".format(status))
    if not status.ok:
        raise AttributeError

    pos = system.get_positions().copy()
    vel = system.get_velocities().copy()
    forces = system.get_forces().copy()
    Epot = system.get_potential_energy()

    system = CastepSystem(restart_traj='output/MDsim__ase.traj', _verbose=True, calc_folder=castep_folder)

    # positions are stored only with rather limited precision
    pos_eps = 1e-5
    eps = 1e-10

    status.ok = (np.all(np.abs(pos - system.get_positions()) < pos_eps)
                 and np.all(np.abs(vel - system.get_velocities()) < eps)
                 and np.all(np.abs(forces - system.get_forces()) < eps)
                 and np.abs(Epot - system.get_potential_energy()) < eps)

    print('Positions/velocities/forces/potential energy are properly restored upon restart : {}'.format(status))
    if not status.ok:
        raise AttributeError

    status.ok = system.atoms.calc._calls == steps
    print('Restarting reuses previous information : {}'.format(status))
    if not status.ok:
        raise AttributeError

    for step in range(steps):
        nstep += 1
        integrator.propagate(system)
        handler.push()

    handler.close()

    if show:
        os.system('ase-gui output/*.traj')

    if remove:
        shutil.rmtree('output')


if __name__ == '__main__':
    test_castepaimd(remove=False, show=False, steps=2)
