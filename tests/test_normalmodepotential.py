# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_normalmodepotential(show = False):
    import numpy as np
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.systems.system import System

    from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential
    from MDsim.coordinates.normalmodes import Normalmodes
    from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp

    from MDsim.potentials.atomic.normalmodepotential import NormalmodePotential
    from MDsim import units

    # hard coded masses from CASTEP
    m_C = 12.0107000
    m_O = 15.9994000


    # first coordinate is dRcom, second is ddist
    masses = np.array([m_C, m_O])

    datafile = 'data/CO_on_Cu100_c2x2.pot'

    positions=np.zeros((2,1))

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "generic-dof",
                                    periodic = False,
                                    convert=True,
                                    normalize=True,
                                    verbose=False,
                                    forces_method = 'analytical',
                                    masses=masses.copy(),
                                    usecols = (0,2,4))
    Natoms = 2
    Ndim = 1

    omega, nm, pos_min, Modes = evaluate_normalmodes_on_potential(potential=pot,
                                                                  positions=positions,
                                                                  masses=masses,
                                                                  minimize_first=True,
                                                                  delta = 0.01,
                                                                  convert = True)
    pot_nm = NormalmodePotential(Modes)
    if show:
        lim = (0,1)
        npts = 200


        data = np.loadtxt(datafile, usecols=(0,1,4)).T
        data[2,:] -= np.min(data[2,:])


        xy_range = np.array([[np.min(data[0]), np.max(data[0])],
                             [np.min(data[1]), np.max(data[1])]])*units.ANGSTROM_TO_AU

        xy_range[0,:] = np.array([-0.3, 0.3])
        xy_range[1,:] = np.array([-0.30, 0.30])

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)


        X,Y = np.meshgrid(x,y)

        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.zeros_like(X)
        Znm = np.zeros_like(X)
        Fx = np.zeros_like(X)
        Fy = np.zeros_like(X)

        print('Evaluating plot data')
        for i in range(X.shape[0]):
            for j in range(Y.shape[1]):
                p = np.array([X[i,j], Y[i,j]]).reshape(2,1)
                Z[i,j] = pot.get_Epot(p) * units.AU_TO_EV
                Znm[i,j] = pot_nm.get_Epot(p*units.ANGSTROM_TO_AU) * units.AU_TO_EV

        Z = np.ma.masked_invalid(Z)
        X = np.ma.array(X, mask=Z.mask)
        Y = np.ma.array(Y, mask=Z.mask)

        import matplotlib.pyplot as plt
        from matplotlib import rcParams
        import matplotlib
        rcParams['mathtext.fontset'] = 'stixsans'
        rcParams['font.family'] = 'Arial'

        fig, axes = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True)
        cmap = plt.get_cmap('plasma')
        cmap_nm = cmap

        print('Rendering plot')

        levels = np.linspace(*lim, num = 21)

        cmap_adjusts = []

        ax = axes[0][0]
        # surface plot
        ax.set_title('DFT potential')
        cont = ax.contourf(X, Y, Z,
                           cmap=cmap,
                           extend="both",
                           levels = levels)

        cmap_adjusts += [cont]

        ax = axes[0][1]
        ax.set_title('harmonic potential')
        # surface plot
        cont = ax.contourf(X, Y, Znm,
                           cmap=cmap_nm,
                           extend="both",
                           levels = levels)

        cmap_adjusts += [cont]

        ax = axes[1][0]
        # surface plot
        cont1 = ax.contourf(X, Y, Z,
                           cmap=cmap,
                           extend="both",
                           levels = levels)
        cont2 = ax.contour(X, Y, Znm,
                           cmap=cmap_nm,
                           extend="both",
                           levels = levels)
        ax.set_title('Comparison')
        cmap_adjusts += [cont1, cont2]

        ax = axes[1][1]
        # surface plot
        cont = ax.contourf(X, Y, np.abs(Z-Znm),
                           cmap=cmap,
                           extend="both",
                           levels = levels)
        ax.set_title('Delta')
        cmap_adjusts += [cont]

        for i in cmap_adjusts:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)


        for ax in axes.ravel():
            ax.set_xlim(*xy_range[0])
            ax.set_ylim(*xy_range[1])

            ax.set_xlabel(r'$\Delta\mathbf{R}_{\mathrm{C}}$ / a.u.')
            ax.set_ylabel(r'$\Delta\mathbf{R}_{\mathrm{O}}$ / a.u.')

#             ax.clabel(cont, fmt ='%.2f eV')

        # set a colorbar
        # cbar = fig.colorbar(surf, orientation='vertical')
        # cbar.set_label('V / eV')

            npts = 300
            modes = np.zeros((2,npts,2))
            forces = np.zeros_like(modes)
            for i, a in enumerate(np.linspace(-1e2,1e2,npts)):
                modes[0,i,:] = Modes.positions_normalmodes_to_cartesian(a*np.array([1,0])).ravel()
                modes[1,i,:] = Modes.positions_normalmodes_to_cartesian(a*np.array([0,1])).ravel()
                forces[0,i,:] = Modes.gradient_cartesian_to_normalmodes(pot.get_forces(modes[0,i,:].reshape(2,1))).ravel()
                forces[1,i,:] = Modes.gradient_cartesian_to_normalmodes(pot.get_forces(modes[1,i,:].reshape(2,1))).ravel()

            # go along the mode and evaluate forces

            ax.plot(modes[0,:,0], modes[0,:,1], color='black', lw=2)
            # ax.plot(*modes[1].T, color='SlateBlue', lw=2)
            ax.plot(modes[1,:,0], modes[1,:,1], color='gray', lw=2, ls = '--')

            ax.set_aspect('equal')

        cax, kw = matplotlib.colorbar.make_axes([ax for ax in axes.flat])
        plt.colorbar(cmap_adjusts[0], cax=cax, **kw)
        plt.show()


if __name__ == '__main__':
    test_normalmodepotential(show = True)
