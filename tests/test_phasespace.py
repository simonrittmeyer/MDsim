# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import time
import numpy as np


from MDsim.systems.system import System
from MDsim.potentials import Potential
from MDsim.potentials.atomic.harmonicpotential import HarmonicPotential
from MDsim.friction.scalarfriction import ScalarFriction
from MDsim.friction.constantfriction import ConstantFriction
from MDsim import units

from MDsim.tools.createobjects import create_integrator


def test_phase_space_coverage(Nsteps = 1e3, show = False):
    ### ### ### ### ### ### ### ### ### ### ###
    ###  choose integrator to test here!!!  ###
    ### ### ### ### ### ### ### ### ### ### ###

    integrators = [
                   'Gronbech-Jensen/Farago',
                   # our BBK implementation seems broken
                   #'Bruenger/Brooks/Karplus',
                   'Vanden-Eijnden/Ciccotti',
                   'Bussi/Parrinello']

    for integrator_method in integrators:
        # # # # # # # # # # # # # # # # # # # # #
        # TUCKERMAN'S HARMONIC OSCILLATOR TESTS #
        # # # # # # # # # # # # # # # # # # # # #

        Natoms = 2
        Ndim = 1

        positions = np.zeros((Natoms, Ndim))
        velocities = None
        pos_off = None
        masses = np.ones(Natoms)

        temperature = 1.

        omega = np.array([[1], [1] ])
        etas = np.array([[0.5],[8.]])


        timestep = .1
        simulation_time = 2000 * 2*np.pi
        Nsteps = int(Nsteps)

        # create the Integrator instance
        integrator = create_integrator(method = integrator_method,
                                       dt = timestep,
                                       convert = False)

        print('Testing harmonic oscillator with different eta values')
        # create potential object (now includes force function)
        potential = HarmonicPotential(omegas = omega,
                                      masses = masses,
                                      positions_eq = pos_off,
                                      convert = False)

        etaMatrix = ConstantFriction(friction_matrix = etas, convert = False)

        # initiate system
        system = System(positions = positions,
                        velocities = velocities,
                        masses = masses,
                        potential = potential,
                        friction = etaMatrix,
                        temperature = temperature,
                        convert = False)

        dt = timestep
        if Nsteps is None:
            Nsteps = int(np.ceil(simulation_time/dt))
        else:
            Nsteps += 1

        steps = np.arange(Nsteps)
        times = dt * steps
        Ekin = np.empty(Nsteps)
        Epot = np.empty(Nsteps)
        pos = np.empty(shape = (Nsteps,Natoms, Ndim), dtype = float, order = 'C')
        vels = np.empty(shape = (Nsteps,Natoms, Ndim), dtype = float, order = 'C')
        momentum = np.empty(shape = (Nsteps,Natoms, Ndim), dtype = float, order = 'C')

        T = 2*np.pi
        Ttimes = times / T

        # actual MD simulation loop
        for step in range(0,int(Nsteps)):
            Ekin[step] = system.get_kinetic_energy()
            Epot[step] = system.get_potential_energy()
            pos[step,:] = system.get_positions()
            momentum[step,:] = system.get_momenta()
            vels[step,:] = system.get_velocities()
            # propagate
            integrator.propagate(system)

        if show:
            import matplotlib.pyplot as plt
            from matplotlib import rcParams
            rcParams['mathtext.fontset'] = 'stixsans'
            rcParams['font.family'] = 'Arial'

            # plot integrator characteristics
            fig, axs = plt.subplots(3,Natoms)

            # plot trajectory of particles in harmonic potential
            for n,ax in enumerate(axs[0]):
                ax.plot(Ttimes,
                        pos[:,n],
                        color = 'k')

                ax.set_xlabel(r'$t$ $(T)$')
                ax.set_ylabel(r'$q(t)$ (a.u.)')
                ax.set_ylim(-3,3)
                ax.set_xlim(0,20)
                ax.text(.1, .9, r'$\eta = {}$ a.u.'.format(etas[n,0]),
                        ha = 'left',
                        va = 'top',
                        transform = ax.transAxes)

            # plot phase space diagram of particles
            for n,ax in enumerate(axs[1]):
                ax.plot(pos[:,n],
                        momentum[:,n],
                        linestyle = '',
                        marker = 'o',
                        markersize = 1.5,
                        markeredgecolor = 'none',
                        color = 'k')

                ax.set_xlabel(r'position $p$ (a.u.)')
                ax.set_ylabel(r'momentum $q$ (a.u.)')
                ax.set_aspect('equal')
                ax.set_xlim(-4,4)
                ax.set_ylim(-4,4)

            # probabilities
            bins = np.linspace(-5,5,30)
            width = bins[-1]-bins[-2]
            bins_cont = np.linspace(bins[0], bins[-1], 200)

            def gaussian(x, mean, sigma):
                return 1./(sigma * np.sqrt(2.*np.pi)) * np.exp(-(x-mean)**2 / (2*sigma**2))

            # only use equilibrated positions for statistics
            neq = 1000
            for n, ax in enumerate(axs[2]):
                # the gaussian probaility distribution
                sigma = np.std(pos[neq:,n])**2
                mean = np.mean(pos[neq:,n])
                ax.plot(bins_cont, gaussian(bins_cont, mean, sigma), color = 'black')

                # the histogram
                hist, bin_edges = np.histogram(pos[neq:,n], bins, density = True)
                ax.plot(bins[:-1]+width/2., hist, marker = 'o', ls = '', color = 'black', ms = 6)

                ax.set_xlim(-4,4)
                ax.set_ylim(0.,0.4)

                ax.set_ylabel(r'$P(q)$')
                ax.set_xlabel(r'$q$ (a.u.)')


            fig.suptitle('Integrator method : "{}"'.format(integrator_method))
            #plt.tight_layout()
            plt.show()

if __name__ == '__main__':
    test_phase_space_coverage(Nsteps = 1e4, show = True)


