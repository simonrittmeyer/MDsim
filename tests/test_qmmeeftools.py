# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np

from ase.visualize import view
from ase.build import fcc111, fcc100

from MDsim.qmmeef.tools import cut_qm_from_me
from MDsim.qmmeef.coordinates import Coordinates

from MDsim.tools.status import Status

def test_tools(show=False):
    size = [11,22,100]
    qm_size = [2,10,5]
    status = Status()
    for name, func in zip(['fcc(100)', 'fcc(111)'], [fcc100, fcc111]):
        print('Testing for {}'.format(name))
        atoms=func('Cu', size = size, vacuum=10)
        scale = 1.542
        maxZ = np.max(atoms.get_positions()[:2])
        pos = atoms.get_positions()
        pos[:,2] -= maxZ
        atoms.set_positions(pos)

        me_vacuum = atoms.get_cell()[-1,-1] - (np.max(atoms.get_positions()[:,-1]) - np.min(atoms.get_positions()[:,-1]))

        qm_atoms, idx=cut_qm_from_me(atoms,
                                     me_size=size,
                                     qm_size=qm_size,
                                     _qm_lattice_scale=scale,
                                     _return_idx=True)

        qm_vacuum = qm_atoms.get_cell()[-1,-1] - (np.max(qm_atoms.get_positions()[:,-1]) - np.min(qm_atoms.get_positions()[:,-1]))


        eps = 1e-3
        status.ok = np.abs(qm_vacuum - me_vacuum) < eps


        print('Vacuum is cut properly: {}'.format(status))
        if not status.ok:
            raise AssertionError

        status.ok = len(qm_atoms) == np.prod(qm_size)
        print('Number of QM atoms is correct : {}'.format(status))
        if not status.ok:
            raise AssertionError

        coords = Coordinates(qm_lattice_scale=scale)

        status.ok = np.allclose(qm_atoms.get_positions(),
                                coords.points_me_to_qm(atoms.get_positions()[idx]))
        print('Positions of QM atoms are correct (incl. scaling): {}'.format(status))
        if not status.ok:
            raise AssertionError

        if show:
            view(atoms)
            view(qm_atoms)
            qm_atoms.set_positions(qm_atoms.get_positions(wrap=True))
            view(qm_atoms)

if __name__ == '__main__':
    test_tools(show=False)
