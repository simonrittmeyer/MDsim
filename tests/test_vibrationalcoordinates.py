# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

from MDsim import units
from MDsim.tools.status import Status

def test_diatomicperp(show=False, verbose=False):
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.analysis.normalmodes import evaluate_normalmodes
    from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp

    masses = np.array([12.,16.])
    datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot'

    positions_eq=np.array([[21.22994700],[22.38415200]])

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "diatomic-perp",
                                    periodic = False,
                                    convert=False,
                                    normalize=True,
                                    positions_eq=positions_eq,
                                    verbose=False,
                                    masses=masses,
                                    usecols = (0,1,4))

    pot._bounds_check = False
    lim = (0,3)
    coords = InternalCoordsDiatomicPerp(masses=masses,
                                        positions_eq=positions_eq,
                                        convert=False)

    if show:
        pot.show(lim = lim)


    Npos = 100
    r = pot._interp_range.copy()
    scale = r - r[:,0][:,None]

    # now we have positions in the interpolation range
    internals = np.random.rand(Npos,2) * scale[:,1][None,:] * 0.5
    internals += r[:,0][None,:] * 0.5

    pos = np.zeros_like(internals)
    for i, internal in enumerate(internals):
        pos[i,:] = coords.positions_internals_to_cartesian(internal).ravel()

    if show:
        npts = 400


        data = np.loadtxt(datafile, usecols=(2,3,4)).T
        data[2,:] -= np.min(data[2,:])


        xy_range = np.array([[np.min(data[0]), np.max(data[0])],
                             [np.min(data[1]), np.max(data[1])]])

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)


        X,Y = np.meshgrid(x,y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.zeros_like(points[:,0])


        for i in range(npts**2):
            try:
                Z[i] = pot.get_value(points[i].reshape(2,1))
            except ValueError:
                Z[i] = np.nan

        Z = Z.reshape(npts, npts)

        Z = np.ma.masked_invalid(Z)
        X = np.ma.array(X, mask=Z.mask)
        Y = np.ma.array(Y, mask=Z.mask)

        import matplotlib.pyplot as plt
        from matplotlib import rcParams
        rcParams['mathtext.fontset'] = 'stixsans'
        rcParams['font.family'] = 'Arial'

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)


        cmap = plt.get_cmap('coolwarm')

        # surface plot
        surf = ax.pcolormesh(X, Y, Z,
                            cmap=cmap)

        # contour plot
        levels = np.linspace(*lim, num = 10)
        cont = ax.contour(X, Y, Z,
                          colors = 'black',
                          extend="both",
                          levels = levels)


        # scatter plot
        scat = ax.scatter(x = data[0], y = data[1],
                          c = data[2], s = 20, cmap = cmap,
                          label = 'sampled points')

        for i in [surf, cont, scat]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel(r'$\mathbf{R}_{\mathrm{C},z}$ / $\AA$')
        ax.set_ylabel(r'$\mathbf{R}_{\mathrm{O},z}$ / $\AA$')

        ax.clabel(cont, fmt ='%.2f eV')

        # set a colorbar
        cbar = fig.colorbar(surf, orientation='vertical')
        cbar.set_label('V / eV')

        # checking the forces
        ax.scatter(x = pos[:,0], y= pos[:,1], color='black')


        plt.show()

    from MDsim.derivatives import gradient_central_differences as central_differences
    devs = np.zeros(Npos)

    for i,p in enumerate(pos):
        f_analytical = pot.get_forces(p.reshape(2,1))
        func = lambda x: pot.get_Epot(x.reshape(2,1))
        f_numerical = -central_differences(func, p.reshape(2,1), d=0.00001)

        devs[i] = np.linalg.norm(f_analytical - f_numerical)

        if verbose:
            print(np.linalg.norm(f_analytical-f_numerical)/np.linalg.norm(f_numerical)*100)
            print('\tanalyical [{} {}]'.format(*f_analytical))
            print('\tnumerical [{} {}]'.format(*f_numerical))

    status = Status()
    eps = 1e-6
    status.ok = np.all(devs < eps)
    print('Numerical and analytical forces agree: {}'.format(status))
    if not status.ok:
        raise AssertionError

if __name__ == '__main__':
    test_diatomicperp(show=True)
