# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
def test_dK():
    from MDsim.tools.createobjects import create_kvectors
    from MDsim.tools.status import Status

    s = Status()

    supercell = np.array([[220., 0],
                          [0., 235.]
                          ])

    try:
        dKs = create_kvectors(surface_facet='fcc111',
                              surface_direction='[11-2]',
                              Nkpts=50,
                              min_amplitude=0.,
                              max_amplitude=3.,
                              verbose=False,
                              convert=False)
        s.ok = True
    except Exception:
        s.ok = False

    print('Standard usage works as before: {}'.format(s))
    if not s.ok:
        raise AssertionError

    dKs2 = create_kvectors(surface_facet='fcc111',
                           surface_direction='[11-2]',
                           Nkpts=50,
                           min_amplitude=0.,
                           max_amplitude=3.,
                           supercell=supercell,
                           verbose=False,
                           convert=False)

    g0_divider = 2 * np.pi / np.diagonal(supercell)
    eps = 1e-10

    s.ok = True
    for dK in dKs2[1::]:
        delta = (dK / g0_divider)[1]
        rem=delta%1
        if np.abs(rem) > eps:
            if not (rem - 1.) <eps:
                s.ok = False
    print('Adjusting to scattering conditions of supercell: {}'.format(s))
    if not s.ok:
        raise AssertionError

    try:
        dKs = create_kvectors(surface_facet='fcc111',
                              surface_direction='[112]',
                              Nkpts=50,
                              min_amplitude=0.,
                              max_amplitude=3.,
                              supercell=supercell,
                              verbose=False,
                              convert=False)
        s.ok = False
    except NotImplementedError:
        s.ok = True
    print('Rejecting incompatible dK vectors: {}'.format(s))
    if not s.ok:
        raise AssertionError

    N=200
    dKs = create_kvectors(surface_facet='fcc111',
                           surface_direction='[11-2]',
                           Nkpts=N,
                           min_amplitude=0.,
                           max_amplitude=3.,
                           supercell=supercell,
                           verbose=False,
                           convert=False)
    s.ok = dKs.shape[1] != N

    print('Reducing to unique k-points: {}'.format(s))
    if not s.ok:
        raise AssertionError

if __name__ == '__main__':
    test_dK()

