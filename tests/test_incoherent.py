# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil

from MDsim.tools.status import Status


def test_incoherent(*args, **kwargs):
    def run(show=False, time=10000, accumulate=False, Natoms=1):
        from MDsim.mainroutines.diffusion import main
        from MDsim.mainroutines.diffusion.options import get_options_dict
        from MDsim.tools.options import create_args_dict

        args = create_args_dict(get_options_dict())
    #    for arg in sorted(args.keys()):
    #        print(arg, args[arg])

        args['system.natoms'] = Natoms
        args['system.species'] = 'Na'
        args['system.surface_facette'] = 'fcc111'
        args['system.initial_positions'] = 'bridge'
        args['system.temperature'] = 155
        args['system.polarizability'] = 17
        args['system.surface_lattice_constant'] = 3.610 / np.sqrt(2)
        args['system.surface_facet'] = 'fcc111'
        args['potential.type'] = 'interpolated'
        args['potential.potfile'] = 'data/Na_on_Cu111_adsorption_sites.pot'

        args['friction.value'] = 0.010
        args['friction.type'] = 'constant'

        args['system.coverage'] = 0.05
        args['system.saturation_coverage'] = 4./9.
        args['system.polarizability'] = 13.8
        args['system.dipole_moment_zero_coverage'] = 3.48

        args['misc.support'] = 'fortran'

        args['pairwise.force_cutoff'] = 20.
        args['pairwise.list_cutoff'] = 40.

        args['misc.show_system'] = show
        args['io.delta_k_directions'] = ['[110]', '[11-2]']
        args['io.delta_k_max_amplitude'] = 4.

        args['io.alternative_etafile'] = 'data/Na_on_Cu111_adsorption_sites.eta'
        args['io.debug'] = True
        args['io.fileformat'] = 'netcdf4'
        args['io.iostep'] = 10
        args['io.seed'] = 'incoherent_test'
        args['io.outpath'] = 'output'

        args['propagation.simulation_time'] = time
        args['propagation.timestep'] = 10
        args['propagation.equilibration_time'] = 100000
        if accumulate:
            args['propagation.mode'] = 'accumulation'
            args['propagation.nruns'] = 3
        else:
            args['propagation.mode'] = 'single'

        args['postprocessing.observables'] = ['isf', 'incoherent_isf', 'dsf', 'incoherent_dsf', 'isf_via_dsf', 'incoherent_isf_via_dsf']
        args['postprocessing.fft_optimize_length'] = True
        args['postprocessing.fft_autocorrelation'] = 'linear'

        main(infile='../infiles/diffusion.cfg',
             py_args=args,
             verbose=False)

    remove = kwargs.pop('remove', True)

    run(Natoms = 1, **kwargs)
    # comparing the isf
    f1 = 'output/incoherent_test__dK__11-2__INCOHERENT_ISF.nc'
    f2 = 'output/incoherent_test__dK__11-2__ISF.nc'

    from MDsim.tools.netcdf import load_netcdf
    d1 = load_netcdf(f1)
    d2 = load_netcdf(f2)

    status = Status()

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass
    status.ok = np.allclose(d1, d2)
    print('Incoherent and coherent ISF coincide for single particle problem : {}'.format(status))
    if not status.ok:
        raise AssertionError


    run(Natoms = 50, **kwargs)
    # comparing the isf
    f1 = 'output/incoherent_test__dK__11-2__INCOHERENT_ISF.nc'
    f2 = 'output/incoherent_test__dK__11-2__ISF.nc'

    from MDsim.tools.netcdf import load_netcdf
    d1 = load_netcdf(f1)
    d2 = load_netcdf(f2)

    status = Status()
    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass

    status.ok = not np.allclose(d1, d2)
    print('Incoherent and coherent ISF do not coincide for multi particle problem : {}'.format(status))
    if not status.ok:
        raise AssertionError

if __name__ == '__main__':
    test_incoherent(remove=False, show=False, time=2**14, accumulate=True)
