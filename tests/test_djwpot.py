# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.potentials.atomic.periodic.diffusion.fcc111 import DJWPotential
from MDsim.potentials.atomic.periodic.diffusion.fcc111 import CambridgePotentialFCC111

def test_djwpot(show=False):
    a = 3.610 / np.sqrt(2)
    pot = DJWPotential(a, p2 = 0.055)
    if show:
        print(pot)
        pot.show()
        pot.show_cuts()

def test_cambridgepot(show=False):
    a = 3.610 / np.sqrt(2)
    pot = CambridgePotentialFCC111(a=a,
                                   height=.55)
    if show:
        print(pot)
        pot.show()
        pot.show_cuts()

if __name__ == '__main__':
    test_djwpot(show=True)
    test_cambridgepot(show=True)
