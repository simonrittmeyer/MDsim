# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim import units

def test_scalar_friction():
    from MDsim.friction.scalarfriction import ScalarFriction
    eta = ScalarFriction(eta = 1.2, convert = True)

    print eta

    pos = np.zeros((12,3))
    i = eta(pos)
    j = eta(pos)

    assert i is j

    eta = ScalarFriction(1.2, *pos.shape, convert = True)
    k = eta(pos)

    assert np.all(j==k)

def test_constant_friction_matrix():
    from MDsim.friction.constantfriction import ConstantFriction

    etas = np.random.randn(12,3)
    eta = ConstantFriction(etas, convert = True)

    assert np.all(eta(None) == etas)

    print eta


def test_homogeneousatomsfriction():
    from MDsim.tools.createobjects import create_minimumbasisfourierfrictioncoefficient as create_minimumbasisfourierfriction
    a = 3.610 / np.sqrt(2)
    eta = create_minimumbasisfourierfriction(a=a ,
                                             datafile='data/Na_on_Cu111_adsorption_sites.eta',
                                             verbose=False)

    from MDsim.friction.electronicfriction.ldfa.matrix.homogeneousatoms import HomogeneousAtomsFrictionMatrix
    from MDsim.tools.status import Status

    status = Status()

    friction = HomogeneousAtomsFrictionMatrix(eta)
    print(friction)
    status.ok = not friction._created_etas

    print("Etas not initially set: {}".format(status))
    if not status.ok:
        raise AssertionError

    Natoms = 100
    Ndim = 3
    pos = np.random.randn(Natoms, Ndim)

    try:
        etas = friction(pos).copy()
        status.ok = True
    except StandardError:
        status.ok = False

    print("Etas can be called: {}".format(status))
    if not status.ok:
        raise AssertionError


    status.ok = friction._created_etas

    print("Etas set after first call: {}".format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-10
    status.ok = np.all(np.abs(etas - friction(pos))<eps)
    print("Calling with identical positions yields identical results: {}".format(status))
    if not status.ok:
        raise AssertionError

    pos = np.random.randn(Natoms, Ndim)
    status.ok = not np.all(np.abs(etas - friction(pos))<eps)
    print("Calling with different positions yields different results: {}".format(status))
    if not status.ok:
        raise AssertionError

    pos = np.ones((Natoms, Ndim))
    etas = friction(pos)
    status.ok = np.all(np.abs(etas - etas[1]) < eps)

    print("Calling with same positions for all DOF yields identical friction for all DOF: {}".format(status))
    if not status.ok:
        raise AssertionError

    Natoms = 101
    Ndim = 3
    pos = np.random.randn(Natoms, Ndim)

    try:
        etas = friction(pos)
        status.ok = False
    except StandardError:
        status.ok = True

    print("Changing Natoms yields error: {}".format(status))
    if not status.ok:
        raise AssertionError

if __name__ == '__main__':
#    test_scalar_friction()
#    test_constant_friction_matrix()
    test_homogeneousatomsfriction()
