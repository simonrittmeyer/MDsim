# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import os
import numpy as np
import ase
import shutil

from MDsim.mainroutines.qmmeef import run
from MDsim.mainroutines.qmmeef import restart

def attach_me_calc(me_atoms):
    from MDsim.bindings.lammps import ExtendedLAMMPS

    # setting up the lammps calculator
    pair_style = 'meam'

    Pd_eam_file = os.path.abspath('data/Pd-00PBE.meam')
    pair_coeff = ['* * ' + Pd_eam_file + ' Pd NULL Pd']
    parameters = {'pair_style' : pair_style,
                  'pair_coeff' : pair_coeff}
    files = [Pd_eam_file]

    calc = ExtendedLAMMPS(parameters=parameters,
                          files=files)

    me_atoms.set_calculator(calc)

    return me_atoms

def add_adsorbate(qm_atoms):
    from MDsim import units
    # This routine should as well assign momenta!
    # dummy routine to add some H on top
    pos = qm_atoms.get_positions()[np.lexsort(qm_atoms.get_positions().T)][-1]
    #slight shift to see lateral motion of the adsorbates
    pos[:2] += 0.1
    height = 1.5
    pos[-1] += height
    # kinetic energy in eV
    Ekin = 5
    mass = 1

    mom = -np.sqrt(2 * Ekin * units.EV_TO_AU * mass * units.AMU_TO_AU)*units.MOMENTUM_AU_TO_ASE
    adsorbate_element='H'
    qm_atoms.extend(ase.Atoms(adsorbate_element, positions=[pos],
                              momenta=[[0,0,mom]]))

    return qm_atoms

def attach_qm_calc(qm_atoms):
    from MDsim.bindings.castep import ExtendedCastep
    calc = ExtendedCastep(verbose=True)
    calc._castep_command = "mpirun.local -np 4 /data/rittmeyer/bin/wheezy/castep-8.0-openmpi_dm_edft"
    calc._rename_existing_dir = False
    calc._copy_pspots = False
    calc._link_pspots = True

    calc.spin_polarized = False

    qm_atoms.set_calculator(calc)

    calc.set_pspot(pspot = 'OTF')
    calc.cell.fix_com = False
    calc.cell.kpoints_mp_grid = '2 2 1'

    return qm_atoms

if __name__ == '__main__':
    restart_test=True

    try:
        shutil.rmtree('output')
    except OSError:
        pass

    # starting from scratch
    run(me_element='Pd',
         me_facette='fcc100',
         me_size=[10,10,10],
         me_a=3.93,
         attach_me_calc = attach_me_calc,
         vacuum=5.,
         qm_size=[1,1,3],
         add_adsorbate=add_adsorbate,
         friction_model='iaa',
         integrator='liouville',
         timestep=1,
         simulation_time=5,
         observed_quantities=['forces', 'energy'],
         qm_update_step = 1,
         qmme_update_step = 1,
         walltime='00:00:40',
         preview=False,
         dryrun=False,
         attach_qm_calc=attach_qm_calc,)

    # analyzing trajectory information
    from ase.io import read
    from MDsim.tools.status import Status
    status = Status()

    print("Analyzing trajectories...")
    qm_traj = read('./output/trajectories/QMMeEF-MDsim__QM__ase.traj', index=':')
    qmme_traj = read('./output/trajectories/QMMeEF-MDsim__QMMe__ase.traj', index = ':')

    N = len(qm_traj)
    for i in range(len(qm_traj)):
        status.ok = qm_traj[i].info['dissipated_energy'] == qmme_traj[i].info['dissipated_energy']
        print('Consistent dissipated energy for step {} : {}'.format(i, status))
        if not status.ok:
            raise AssertionError

    if restart_test:
        # testing restart
        restart(attach_me_calc = attach_me_calc,
             friction_model='iaa',
             integrator='liouville',
             timestep=1,
             simulation_time=5,
             observed_quantities=['forces', 'energy'],
             qm_update_step = 1,
             qmme_update_step = 1,
             walltime=None,
             restart_folder='restart',
             attach_qm_calc=attach_qm_calc)


    qm_traj_res = read('./output/trajectories/QMMeEF-MDsim__QM__ase.traj', index=':')
    qmme_traj_res = read('./output/trajectories/QMMeEF-MDsim__QMMe__ase.traj', index = ':')

    key = 'dissipated_energy'
    for i in range(N, len(qm_traj_res)):
        status.ok = qm_traj_res[i].info[key] == qmme_traj_res[i].info[key]
        print('Consistent dissipated energy for step {} : {}'.format(i, status))
        if not status.ok:
            raise AssertionError
        status.ok = qm_traj_res[i].info[key] > qm_traj_res[i-1].info[key]
        print('Increasing dissipated energy for step {} : {}'.format(i, status))
