# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import copy
import numpy as np
from MDsim.tools.status import Status
def test_diffusion_sanity(show=False):
    from MDsim.mainroutines.diffusion.options import get_options_dict
    from MDsim.mainroutines.diffusion.options import options_set
    from MDsim.tools.options import create_args_dict
    from MDsim.tools.options import check_args_dict_unknown

    from MDsim.mainroutines.diffusion.sanity import check_sanity

    options_dict = get_options_dict()
    s = Status()

    verbose=True
    try:
        check_sanity(options_dict, verbose=verbose)
        s.ok = False
    except ValueError:
        s.ok = True

    print('Incomplete input is rejected : {}'.format(s))
    if not s.ok:
        raise AssertionError

    options_dict['io']['observables'].val = 'positions'
    options_dict['system']['species'].val = 'Na'
    options_dict['system']['surface_facet'].val = 'fcc111'
    options_dict['system']['natoms'].val = 1000
    options_dict['system']['surface_lattice_constant'].val = 3.61/np.sqrt(2)
    options_dict['propagation']['simulation_time'].val = 1000
    options_dict['propagation']['timestep'].val = 1
    try:
        print(options_dict['friction']['type'].val)
        check_sanity(options_dict, verbose=verbose)
        s.ok = True
    except ValueError:
        s.ok = False

    print('Complete input is accepted : {}'.format(s))
    if not s.ok:
        raise AssertionError

    options_dict['io']['observables'].val = 'position'
    try:
        check_sanity(options_dict, verbose=verbose)
        s.ok = False
    except ValueError:
        s.ok = True
    print('Complete but insane input is rejected : {}'.format(s))

    if not s.ok:
        raise AssertionError

    options_dict['io']['observables'].val = 'positions'
    args_dict = create_args_dict(options_dict)

    print('Known options are accepted via args : {}'.format(s))
    s.ok = check_args_dict_unknown(args_dict, options_set)
    if not s.ok:
        raise AssertionError

    args_dict['iio.observables'] = 'positions'
    s.ok = not check_args_dict_unknown(args_dict, options_set)
    print('Unknown options are rejected via args: {}'.format(s))
    if not s.ok:
        raise AssertionError

if __name__ == '__main__':
    test_diffusion_sanity(show=True)
