# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import shutil
import os


def test_asebindings(remove=True, show=False, steps=1e3):
    from MDsim.tools.createobjects import create_integrator
    from MDsim.friction.scalarfriction import ScalarFriction
    from MDsim.tools.createobjects import create_minimumbasisfourierpotential
    from MDsim.systems.system import System
    from MDsim.tools.createobjects import create_trajectoryhandler

    try:
        shutil.rmtree('output')
    except OSError:
        pass

    Natoms = 100
    Ndim = 2

    positions = np.random.randn(Natoms, Ndim)*100
    velocities = None

    masses = np.random.rand(Natoms)
    species = ['Na']*Natoms

    a = 3.610 / np.sqrt(2)
    pes = create_minimumbasisfourierpotential(a = a, datafile = 'data/Na_on_Cu111_adsorption_sites.pot')

    velocities = np.zeros_like(positions)
    eta = ScalarFriction(eta = .1, convert = False)
    T = 300
    system = System(positions = positions,
                    species = species,
                    velocities = velocities,
                    temperature = T,
                    friction = eta,
                    potential = pes,
                    convert = True)

    integrator = create_integrator(method = 'bp',
                                   dt = 10)

    handler = create_trajectoryhandler(system_inst = system)

    handler.push()

    for step in range(int(steps)):
        integrator.propagate(system)
        if step%10==0:
            handler.push()

    if show:
        os.system('ase-gui output/*traj')

    handler.close()

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass

if __name__ == '__main__':
    test_asebindings(remove=True, show=True, steps=1e5)
