# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from MDsim.tools.status import Status

def test_cambridgepotential(show=False):
    from MDsim.potentials.atomic.periodic.diffusion.fcc100 import CambridgePotentialFCC100
    a = 3.610 / np.sqrt(2)

    energies_dict = {'site1' : 0.050,
                     'site2' : 0.045}
    pot = CambridgePotentialFCC100(a=a,
                                   energies_dict=energies_dict,
                                   convert=True,
                                   )
    status = Status()

    try:
        pot.get_Epot(np.array([[0,0]]))
        status.ok = True
    except Exception:
        status.ok = False

    print('Potential is callable : {}'.format(status))
    if not status.ok:
        raise AssertionError


    positions = np.random.rand(100,2)
    f_analytic = pot.get_forces(positions)
    pot._forces_method = 'numerical'
    f_numeric = pot.get_forces(positions)

    status.ok = np.allclose(f_analytic, f_numeric)

    print('Numerical forces and analytical forces agree : {}'.format(status))
    if not status.ok:
        raise AssertionError

    if show:
        pot.show()
        pot.show_cuts()

if __name__ == '__main__':
    test_cambridgepotential(show=True)
