# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil

import ase
from ase.lattice.surface import fcc100
from ase.optimize import FIRE
from ase.visualize import view

from MDsim import units

from MDsim.tools.status import Status
from MDsim.bindings.lammps import installed as lammps_installed
from MDsim.bindings.castep import installed as castep_installed
from MDsim.bindings.castep import ExtendedCastep
from MDsim.bindings.lammps import ExtendedLAMMPS

from MDsim.qmmeef import CastepQM
from MDsim.qmmeef import LammpsMe
from MDsim.qmmeef import QMMe
from MDsim.qmmeef import cut_qm_from_me

from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_trajectoryhandler


def test_qmmeef(steps=2, remove=True, show=False, skip=True):

    if skip:
        return

    try:
        castep_installed()
    except Exception:
        print('Some of castep/castep_hirshfeld/castep2cube are not installed, skipping test...')
        return

    try:
        lammps_installed()
    except Exception:
        print('LAMMPS is not installed, skipping test...')
        return

    status = Status()

    verbose = False

    a_Pd_fcc = 3.93 # MEAMopt
    a_Pd_100 =  a_Pd_fcc / np.sqrt(2.0)

    _qm_lattice_scale = 1.0

    size = (10,10,10)
    vacuum = 10.0

    # divide by 2 here, since ase understands something different about vacuum
    # than we do
    me_atoms = fcc100(symbol='Pd', a=a_Pd_fcc, size=size, vacuum=vacuum/2.)

    # setting up the lammps calculator
    pair_style = 'meam'
    # necessary to use an absolute path here, otherwise the calculator will
    # crash (uses some tmp dir)
    Pd_eam_file = os.path.abspath('data/Pd-00PBE.meam')
    pair_coeff = ['* * ' + Pd_eam_file + ' Pd NULL Pd']
    parameters = {'pair_style' : pair_style,
                  'pair_coeff' : pair_coeff}
    files = [Pd_eam_file]
    calc = ExtendedLAMMPS(parameters=parameters,
                  keep_tmp_files=True,
                  keep_alive=False,
                  files=files)
    me_atoms.set_pbc(True)
    me_atoms.set_calculator(calc)

#    # optimize the geometry
#    opt = FIRE(me_atoms)
#    opt.run(fmax=1e-2, steps=100)

    # momenta

    me_atoms.set_momenta(np.zeros((np.prod(size), 3)))

    folder = 'output/me'
    me_system = LammpsMe(atoms=me_atoms,
                         calc_folder=folder,
                         _verbose=verbose)

    qm_atoms = cut_qm_from_me(me_atoms,
                              me_size=size,
                              qm_size = [1,1,3],
                              _qm_lattice_scale=_qm_lattice_scale,
                              _verbose=verbose)

    # add some hydrogen
    top = np.sort(qm_atoms.get_positions(), axis=-1)[-1]
    height = 2.0
    top[-1] += height
    adsorbate_element='Kr'
    qm_atoms.extend(ase.Atoms(adsorbate_element, positions=[top]))

    # set up the calculator
    calc = ExtendedCastep(verbose = verbose)
    calc._castep_command = 'mpiexec.openmpi -np 4 castep'
    calc._rename_existing_dir = False
    calc._copy_pspots = False
    calc._link_pspots = True

    calc.spin_polarized = False

    if os.path.isdir('output'):
        shutil.rmtree('output')

    qm_atoms.set_calculator(calc)

    # can be set only after the atoms object is known to the calculator
    qm_atoms.calc.set_pspot(pspot = 'OTF')
    qm_atoms.calc.cell.fix_com = False
    qm_atoms.calc.cell.kpoints_mp_grid = '4 4 1'

    # add some velocity to the adsorbate
    adsorbate_idx = [atom.index for atom in qm_atoms if atom.symbol == adsorbate_element]
    momenta = qm_atoms.get_momenta()
    qm_atoms.set_momenta(momenta)
    masses=qm_atoms.get_masses()

    # kinetic energy in eV
    Ekin = 5
    momenta[adsorbate_idx, 2] = -np.sqrt(2 * Ekin * units.EV_TO_AU * masses[adsorbate_idx] * units.AMU_TO_AU)*units.AU_TO_ANGSTROM_AMU_PER_FS
    qm_atoms.set_momenta(momenta)

    folder = 'output/qm'
    qm_system = CastepQM(qm_atoms,
                         calc_folder=folder,
                         adsorbate_idx = adsorbate_idx,
                         friction='none',
                         _verbose=verbose,
                         _eval_parallel = False)

    qmme_system = QMMe(me_system=me_system,
                       qm_system=qm_system,
                       _init_forces=False,
                       _init_etas=False,
                       _verbose=verbose)

    idx = qmme_system.get_idx()

    me_idx = qmme_system.get_me_idx()
    qm_idx = qmme_system.get_qm_idx()

    status.ok = qmme_system.get_Natoms()== me_system.get_Natoms() + 1
    print('QM/Me system is correctly assembled w.r.t. number of atoms : {}'.format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-5
    status.ok = np.all(np.abs(qmme_system.get_positions()[idx['me_part']] - me_system.get_positions()) < eps)
    print('Positions reordering for Me system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_positions()[idx['qm_part']] - qm_system.get_positions()) < eps)
    print('Positions reordering for QM system : {}'.format(status))
    if not status.ok:
        raise AssertionError


    status.ok = np.all(np.abs(qmme_system.get_forces()[idx['adsorbate']]
                              - qm_system.get_forces()[qm_system.get_adsorbate_idx()]) < eps)
    print('Adsorbate forces are purely QM : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(qmme_system.get_forces()[idx['bath']]
                              - me_system.get_forces()[idx['bath']]) < eps)
    print('Bath forces are purely elastic (Me) : {}'.format(status))
    if not status.ok:
        raise AssertionError

    dummy = np.zeros_like(qmme_system.get_forces())
    dummy[idx['overlap'],:] = me_system.get_forces()[me_idx['overlap']]
    dummy[idx['overlap'],:] += qm_system.get_forces_qm()[qm_system.get_slab_idx()]

    status.ok = np.all(np.abs(qmme_system.get_forces()[idx['overlap']] - dummy[idx['overlap']]) < eps)
    print('Overlap forces are superposition of pure-QM and elastic forces : {}'.format(status))
    if not status.ok:
        raise AssertionError

    if qmme_system.has_friction:
        status.ok = np.all(np.abs(qmme_system.get_etas()[idx['adsorbate']] > eps))
        print('Friction coefficients are non-zero for adsorbate atoms : {}'.format(status))
        if not status.ok:
            raise AssertionError

        status.ok = np.all(np.abs(qmme_system.get_etas()[idx['substrate']] < eps))
        print('Friction coefficients are zero for substrate atoms : {}'.format(status))
        if not status.ok:
            raise AssertionError

    status.ok = (qm_system.atoms.calc._calls == 1,
                 qm_system.slab_atoms.calc._calls == 1)
    print('QM calculations are re-used : {}'.format(status))
    if not status.ok:
        raise AssertionError


    # the propagation part
    integrator = create_integrator(method='liouville', dt=5, seed=None, verbose=False)

    folder = os.path.join('output','trajectories')
    handlers = []
    handlers.append(create_trajectoryhandler(system_inst = qmme_system, verbose=False, outfolder=folder, seed='QMMe'))
    handlers.append(create_trajectoryhandler(system_inst = me_system, verbose=False, outfolder=folder, seed='Me'))
    handlers.append(create_trajectoryhandler(system_inst = qm_system, verbose=False, outfolder=folder, seed='QM'))

    # positions are stored only with rather limited precision
    pos_eps = 1e-5
    eps = 1e-10

    pos = qmme_system.get_positions().copy()
    vel = qmme_system.get_velocities().copy()
    forces = qmme_system.get_forces().copy()
    etas = qmme_system.get_etas().copy()
    Epot = qmme_system.get_potential_energy()

    nstep=0


    for step in range(steps):
        nstep += 1
        integrator.propagate(qmme_system)
        status.ok = (not np.all(np.abs(pos - qmme_system.get_positions()) < pos_eps)
                     and not np.all(np.abs(vel - qmme_system.get_velocities()) < eps)
                     and not np.all(np.abs(forces - qmme_system.get_forces()) < eps)
                     and not np.abs(Epot - qmme_system.get_potential_energy()) < eps)
        if qmme_system.has_friction:
            status.ok = status.ok and not np.all(np.abs(etas - qmme_system.get_etas()) < eps)


        print('System properties all change after propagation at step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        status.ok = (qm_system.atoms.calc._calls == 1 + nstep
                     and qm_system.slab_atoms.calc._calls == 1 + nstep)
        if qmme_system.has_friction:
            status.ok = status.ok and qm_system.atoms.calc._continuation_calls == 1 + nstep
        print('New QM calculation required for step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        pos = qmme_system.get_positions().copy()
        vel = qmme_system.get_velocities().copy()
        forces = qmme_system.get_forces().copy()
        etas = qmme_system.get_etas().copy()
        Epot = qmme_system.get_potential_energy()

        for handler in handlers:
            handler.push()

    for handler in handlers:
        handler.close()

    if show:
        for system in ['QM', 'Me', 'QMMe']:
            os.system('ase-gui {}/{}__ase.traj'.format(folder, system))

    if remove:
        shutil.rmtree(folder)

if __name__ == '__main__':
    test_qmmeef(steps=2, skip=False, remove=False, show=False)
