# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

def test_effective_energy(show = False, steps = 10):
    import numpy as np
    import random

    from MDsim import units
    from MDsim.systems.system import System

    from MDsim.friction.scalarfriction import ScalarFriction

    from MDsim.tools.createobjects import create_minimumbasisfourierpotential
    from MDsim.tools.createobjects import create_integrator

    if show:
        import matplotlib.pyplot as plt
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

    a = 3.610 / np.sqrt(2)
    pes = create_minimumbasisfourierpotential(a = a, datafile = 'data/Na_on_Cu111_adsorption_sites.pot', verbose=False)

    Natoms = 500
    Ndim = 2
    positions = np.random.randn(Natoms, Ndim)
    species = ['Na' for i in range(Natoms)]


    velocities = np.random.randn(*positions.shape)
    eta=0.05*units.AU_TO_AMU_PER_FS
    T = 300

    print('----------------------------------------------------------------')
    print('Testing the Bussi-Parrinello integrator for conservation of the ')
    print('                      EFFECTIVE ENERGY                          ')
    print('----------------------------------------------------------------')
    print('System details:')
    print('---------------')
    print('Natoms = {} (non-interacting)'.format(Natoms))
    print('Ndim   = {}'.format(Ndim))
    print('eta    = {} a.u.'.format(eta))
    print('T      = {} K'.format(T))
    print('')
    print('PES:')
    print('----')
    print(pes)
    print('')
    print('-----------------------------------------------------------------')
    print('Simulating for {} steps'.format(int(steps)))
    print('-----------------------------------------------------------------')

    for dt in [10.,
               1.,
               0.1,
               ]:
        S = System(positions = positions,
                   species = species,
                   velocities = velocities,
                   friction = eta,
                   temperature = T,
                   potential = pes,
                   convert = True,
                   _verbose=False)

        integrator = create_integrator(method = 'Bussi Parrinello',
                                       dt = dt,
                                       verbose=False)

        steps = int(steps / dt)

        Eeff = np.zeros(steps)
        pos = np.zeros((steps,2))
        #S.set_effective_energy(S.get_total_energy())
        for step in range(0,steps):
            integrator.propagate(S, _log_effective_energy=True)
            Eeff[step] = S.get_effective_energy()

        Eeff -= Eeff[0]

        head = 'Time step = {} fs'.format(dt)
        print(head)
        print('-'*len(head))
        print('<Eeff-Eeff_0> / dof      = {:+.3E} meV'.format(np.mean(Eeff) / (Natoms*Ndim) * 1e3))
        print('sigma(Eeff-Eeff_0) / dof = {:+.3E} meV'.format(np.std(Eeff) / (Natoms*Ndim) * 1e3))
        print('')
        if show:
            ax.plot(np.linspace(0,1,steps), (Eeff-Eeff[0])/(Natoms*Ndim), label = 'dt = {} fs'.format(dt), lw = 2)
            ax.set_xlabel('step')
            ax.set_ylabel('E_eff / dof (eV)')

    if show:
        ax.legend()
        plt.show()
if __name__ == '__main__':
    test_effective_energy(show = True, steps = 1e4)
