# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

def test_ldfa(show = False):
    from MDsim.friction.electronicfriction.ldfa import LDFA

    for atom in LDFA.implemented:
        eta = LDFA(element = atom)
        print eta
        if show:
            eta.show()

    if show:
        LDFA.show_all()

def test_ldfa_matrices(show = False):
    # ok, first a "realistic" friction coeffient
    from MDsim.tools.createobjects import create_bivariatefrictioncoefficient as create_bivariatefriction
    from MDsim.friction.electronicfriction.ldfa.matrix.aim import AIMFrictionMatrix

    masses = np.array([12.,16.])
    positions_eq=np.array([[21.22994700],[22.38415200]])

    eta1 = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                    coordinates = "diatomic-perp",
                                    periodic = False,
                                    convert=False,
                                    positions_eq=positions_eq,
                                    masses=masses,
                                    usecols = (0,1,4),
                                    bounds_check = False)

    eta2 = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                    coordinates = "diatomic-perp",
                                    periodic = False,
                                    convert=False,
                                    positions_eq=positions_eq,
                                    masses=masses,
                                    usecols = (0,1,5),
                                    bounds_check = False)
    eta_matrix = AIMFrictionMatrix([eta1,eta2])

    print eta_matrix
    while True:
        try:
            print eta_matrix(np.random.randn(2,1))
            break
        except ValueError as err:
            raise AssertionError('Eta matrix not callable')
    # now second a diffusive friction coefficient
if __name__ == '__main__':
    test_ldfa(show=True)
    test_ldfa_matrices()
