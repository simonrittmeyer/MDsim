# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np
from MDsim.potentials.atomic.periodic.diffusion.fcc100 import CambridgePotentialFCC100
from MDsim.tools.status import Status

def test_cambridgeextension(Natoms=1000):

    a = 3.610 / np.sqrt(2)
    energies_dict = {'site1' : 0.050,
                     'site2' : 0.045}
    pot = CambridgePotentialFCC100(a=a,
                                   energies_dict=energies_dict,
                                   convert=True,
                                   )

    pos = np.random.random((Natoms,2))

    val_funcs = []
    grad_funcs = []

    status = Status()

    try:
        from MDsim.speedup import f90support
        print('f90support available')
        val_funcs.append('pot._fortran_call(pos)')
        grad_funcs.append('pot._fortran_gradient(pos)')
    except ImportError:
        print('f90 support *not* available')

    try:
        from MDsim.speedup import numbasupport
        print('numba support available')
        val_funcs.append('pot._numba_call(pos)')
        grad_funcs.append('pot._numba_gradient(pos)')
    except ImportError:
        print('numba support *not* available')

    try:
        from MDsim.speedup import cythonsupport
        print('cython support available')
        val_funcs.append('pot._cython_call(pos)')
        grad_funcs.append('pot._cython_gradient(pos)')
    except ImportError:
        print('cython support *not* available')


    eps = 1e-10
    val_ref = pot._python_call(pos)

    print('Checking against plain python reference implementation')
    for func in val_funcs:
        val = eval(func)
        status.ok = np.all(np.abs(val_ref - val) < eps)
        print('\t{} : {}'.format(func.replace('pos','').replace('pot.',''), status))
        # if not status.ok:
            # raise AssertionError

    grad_ref = pot._python_gradient(pos)
    for func in grad_funcs:
        grad = eval(func)
        status.ok = np.all(np.abs(grad_ref - grad) < eps)
        print('\t{} : {}'.format(func.replace('pos','').replace('pot.',''), status))
        # if not status.ok:
            # raise AssertionError


if __name__ == '__main__':
    test_cambridgeextension(Natoms=1000)
