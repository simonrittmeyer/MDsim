# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Tests for the new constraints implemented in Sept. 2016

We now actually use the unittest framework.
"""

import os
import unittest
import shutil
import tempfile

import numpy as np

from MDsim.systems.system import System

try:
    from MDsim.bindings.ase.asetomdsim import ASESystem
    from ase.constraints import FixAtoms
    from ase.constraints import FixCartesian
    _ase_avail = True
except ImportError:
    _ase_avail = False

class TestConstraintSystem(unittest.TestCase):
    """
    Test functionality on the base system class
    """
    Ndim=3
    Natoms = 10

    verbose = False
    # a minimal setup
    kwargs = {'positions' : np.random.rand(Natoms, Ndim),
              'species' : ['O']*2+['Pd']*(Natoms-2),
              'potential' : None,
              'friction' : None,
              'temperature' : 0.,
              'fix_atoms' : [i for i in range(3)],
              'convert' : True,
              '_init_forces' : True,
              '_init_etas' : True,
              '_random_seed' : None,
              '_verbose' : verbose}

    def test_init_old(self):
        # test if old init woorks
        kwargs=self.kwargs.copy()
        kwargs.pop('fix_atoms')
        system = System(**kwargs)

        self.assertTrue(not system._constrained)

    def test_correct_init(self):
        system = System(**self.kwargs)
        self.assertTrue(system._constrained)

    def test_fix_clash_init(self):
        kwargs=self.kwargs.copy()
        kwargs['fix_coordinates'] = np.ones((self.Natoms,self.Ndim), dtype=bool)
        with self.assertRaises(ValueError):
            system = System(**kwargs)

    def test_init_wrong_index(self):
        # test if old init works
        kwargs=self.kwargs.copy()
        kwargs['fix_atoms'].append(self.Natoms)

        # invalid indices
        with self.assertRaises(IndexError):
            system = System(**kwargs)

        # double indices
        kwargs['fix_atoms'][-1] = 0
        with self.assertRaises(IndexError):
            system = System(**kwargs)

    def test_constrained_atoms(self):
        system = System(**self.kwargs)
        self.assertTrue(system._constrained)

        forces = np.random.rand(self.Natoms, self.Ndim)
        self.assertTrue(np.all(forces!=0.))

        system.set_forces(forces)

        # test for the zeros
        self.assertTrue(np.all(system.get_forces()[self.kwargs['fix_atoms'],:]==0.))

    def test_wrong_constrained_coordinates(self):
        kwargs=self.kwargs.copy()
        kwargs['fix_atoms'] = None
        fix_coords = np.random.randint(2, size=(self.Natoms+1, self.Ndim-1), dtype=bool)
        kwargs['fix_coordinates']= fix_coords

        with self.assertRaises(IndexError):
            system = System(**kwargs)


    def test_constrained_coordinates(self):
        kwargs=self.kwargs.copy()
        kwargs['fix_atoms'] = None
        fix_coords = np.random.randint(2, size=(self.Natoms, self.Ndim), dtype=bool)
        kwargs['fix_coordinates'] = fix_coords
        system = System(**kwargs)
        self.assertTrue(system._constrained)

        forces = np.random.rand(self.Natoms, self.Ndim)
        self.assertTrue(np.all(forces!=0.))

        system.set_forces(forces)

        # test for the zeros
        self.assertTrue(np.all(system.get_forces()[fix_coords]==0.))

    def test__str__(self):
        system = System(**self.kwargs)
        print(system)

        kwargs=self.kwargs.copy()
        kwargs['fix_atoms'] = None
        fix_coords = np.random.randint(2, size=(self.Natoms, self.Ndim), dtype=bool)
        kwargs['fix_coordinates'] = fix_coords
        system = System(**kwargs)
        print(system)


@unittest.skipIf(not _ase_avail, "ASE not available")
class TestConstraintASESystem(unittest.TestCase):
    """
    Test functionality on the base system class
    """

    from ase import Atoms
    from ase.build import fcc111
    from ase.build import add_adsorbate

    atoms = fcc111('Cu', size = [1,1,5])
    add_adsorbate(atoms, Atoms('O'), height=2, position='fcc')

    verbose = False

    # a minimal setup
    kwargs = {'atoms' : atoms,
              'friction' : None,
              'temperature' : 0.,
              '_init_forces' : False,
              '_init_etas' : False,
              '_random_seed' : None,
              '_verbose' : verbose}

    def test_init_no_constraints(self):
        kwargs=self.kwargs.copy()
        system = ASESystem(**kwargs)

    def test_FixAtoms_and_FixCartesian(self):
        atoms = self.atoms.copy()

        # fix the bulk
        constraints = [FixAtoms(indices=[atom.index for atom in atoms if atom.symbol == 'Cu'])]

        # fix the z of oxygen, ie. optimize the bond distance
        for i in [atom.index for atom in atoms if atom.symbol == 'O']:
            constraints.append(FixCartesian(i, mask = (0,0,1)))

        atoms.set_constraint(constraints)

        kwargs = self.kwargs.copy()
        kwargs['atoms'] = atoms
        system = ASESystem(**kwargs)

        # test for the zeros
        self.assertTrue(system._constrained)

        self.assertTrue(system.get_fixed_coordinates()[-1,-1])
        self.assertTrue(np.all(system.get_fixed_coordinates()[-1,:2]==False))

        print(system)
if __name__ == '__main__':
    unittest.main()
