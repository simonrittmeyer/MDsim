# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_diffusion(show = False, steps = 1e3, show_details=False):
    import numpy as np
    import random

    from MDsim import units
    from MDsim.systems.system import System

    from MDsim.friction.scalarfriction import ScalarFriction

    from MDsim.tools.createobjects import create_minimumbasisfourierpotential
    from MDsim.tools.createobjects import create_integrator

    a = 3.610 / np.sqrt(2)
    pes = create_minimumbasisfourierpotential(a = a, datafile = 'data/Na_on_Cu111_adsorption_sites.pot')

    Natoms = 100
    Ndim = 2
    positions = np.random.randn(Natoms, Ndim)
    species = ['Na' for i in range(Natoms)]


    velocities = np.zeros_like(positions)
    eta = ScalarFriction(eta = 10, convert = False)
    T = 300

#    T = 0
#    eta = None

    S = System(positions = positions,
               species = species,
               velocities = velocities,
               temperature = T,
               friction = eta,
               potential = pes,
               convert = True)

    ints = ['gjf', 'bbk', 'vec', 'bp']
    ints = ['bp']
    integrator = create_integrator(method = random.choice(ints),
                                   dt = 1)

    steps = int(steps)

    Ekin = np.zeros(steps)
    Epot = np.zeros(steps)
    Eeff = np.zeros(steps)
    Etot = np.zeros(steps)
    pos = np.zeros((steps,2))
    for step in range(0,steps):
        integrator.propagate(S)
        pos[step,:] = S.get_positions()[0]
        Ekin[step] = S.get_kinetic_energy()
        Epot[step] = S.get_potential_energy()
        Eeff[step] = S.get_effective_energy()
        Etot[step] = S.get_total_energy()


    # get the root mean square of kinetic energy
    RMS = np.sqrt(np.mean(np.square(Ekin)))
    kT = S.get_kT() * units.KELVIN_TO_EV

    if show:
        update_frequency = 1

        import matplotlib.pyplot as plt
        # get the range in x and y
        x_range = (np.min(pos[:,0]), np.max(pos[:,0]))
        y_range = (np.min(pos[:,1]), np.max(pos[:,1]))

        npts = 400
        x = np.linspace(*x_range, num = npts)
        y = np.linspace(*y_range, num = npts)

        X,Y = np.meshgrid(x,y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        # evaluating using vectorized call ;)
        Z = pes.get_Epots(points*units.ANGSTROM_TO_AU).reshape(npts, npts)*units.AU_TO_EV

        # plotting
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        #cmap = plt.get_cmap('coolwarm')
        #surf = ax.pcolormesh(X,Y,Z, cmap = cmap)
        cont = ax.contour(X,Y,Z, colors = 'gray')

        ax.plot(pos[::update_frequency,0], pos[::update_frequency,1], lw = 2, color = 'black')
        ax.plot(pos[0,0], pos[0,1], marker = 'o', color = 'black')
        ax.set_xlim(*x_range)
        ax.set_ylim(*y_range)
        ax.set_aspect('equal')

        plt.show()

    if show_details:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        kT = S.get_kT()

        ax.plot(np.arange(steps), Ekin, label = 'Ekin')
        ax.plot(np.arange(steps), Epot, label = 'Epot')
        ax.plot(np.arange(steps), Etot, label = 'Etot')
        ax.plot(np.arange(steps), Eeff/(Natoms*Ndim), label = 'Eeff', lw = 2)
        ax.axhline(y = Natoms*Ndim*0.5*kT, color = 'black', lw = 2, ls = '--')
        ax.set_xlabel('step')
        ax.set_ylabel('E')

        ax.legend()

        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        mean_dist = np.sqrt((pos[0,0] - pos[::update_frequency,0])**2 + (pos[0,1] - pos[::update_frequency,1])**2)
        ax.plot(np.arange(len(mean_dist))*update_frequency, mean_dist, color = 'black')
        ax.set_ylabel('Mean distance from origin')
        ax.set_xlabel('Step')

        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        T = 2*Ekin / (Natoms * Ndim) * units.EV_TO_KELVIN
        kT = S.get_kT() * units.EV_TO_KELVIN
        ax.plot(np.arange(len(T)), T, color = 'black')
        ax.axhline(y = kT, color = 'black', lw = 2, ls = '--')
        ax.set_ylabel('Temperature / K')
        ax.set_xlabel('Step')

        plt.show()
if __name__ == '__main__':
    test_diffusion(show = True, steps = 1e5)
