# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

def test_normalmodes_harmonic(show = False):
    import numpy as np

    from MDsim.potentials.atomic.harmonicpotential import HarmonicPotential
    from MDsim.systems.system import System

    from MDsim.analysis.normalmodes import evaluate_normalmodes_of_system
    from MDsim.tools.status import Status

    status = Status()

    Natoms = 2
    Ndim = 2

    positions = np.zeros((Natoms, Ndim))
    velocities = np.zeros_like(positions)

    masses = np.random.rand(Natoms)
    omegas = np.random.rand(Natoms, Ndim)

    potential = HarmonicPotential(omegas = omegas,
                                  masses = masses,
                                  forces_method = 'numerical',
                                  convert = True)


    # initiate system
    system = System(positions = positions,
                    velocities = velocities,
                    masses = masses,
                    potential = potential,
                    convert = True)

    w, nm, pos_min, coords = evaluate_normalmodes_of_system(system, convert = True)

    eps = 1e-10

    # for the harmonic potential the frequencies should be extremely good
    status.ok = np.all(abs(np.unique(w) - np.unique(omegas) < eps))

    print('Harmonic frequencies reproduced (analytical forces) : {}'.format(status))

    if not status.ok:
        raise AssertionError

    # ok, this is a bit dirty
    system.potential._forces_method = 'numerical'

    w_num, nm, pos_min, coords = evaluate_normalmodes_of_system(system, convert = True)

    eps = 1e-8

    # for the harmonic potential the frequencies should be extremely good
    status.ok = np.all(abs(np.unique(w) - np.unique(w_num) < eps))

    print('Harmonic frequencies reproduced (numerical forces) : {}'.format(status))

    if not status.ok:
        raise AssertionError

def test_normalmodes_diatomic(show = False):
    import numpy as np
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.systems.system import System

    from MDsim.analysis.normalmodes import evaluate_normalmodes_of_system
    from MDsim.coordinates.normalmodes import Normalmodes
    from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp

    from MDsim import units
    # masses from CASTEP
    m_C = 12.01070000
    m_O = 15.9994
    masses = np.array([m_C, m_O])

    datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot'

    positions_eq=np.array([[21.22994700],[22.38415200]])

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "diatomic-perp",
                                    periodic = False,
                                    convert=True,
                                    normalize=True,
                                    positions_eq=positions_eq.copy(),
                                    verbose=False,
                                    forces_method = 'numerical',
                                    masses=masses.copy(),
                                    usecols = (0,1,4))

    Natoms = 2
    Ndim = 1

    positions = np.zeros((Natoms, Ndim))
    positions = positions_eq.copy()
    velocities = np.zeros_like(positions)

    # initiate system
    system = System(positions = positions,
                    velocities = velocities,
                    masses = masses.copy(),
                    potential = pot,
                    convert = True)

    omega, nm, pos_min, Modes = evaluate_normalmodes_of_system(system,
                                                              delta = 0.01,
                                                              convert = True)

    if show:

        lim = (0,1)
        coords = InternalCoordsDiatomicPerp(masses=masses,
                                            positions_eq=positions_eq,
                                            convert=False)
        npts = 300


        data = np.loadtxt(datafile, usecols=(2,3,4)).T
        data[2,:] -= np.min(data[2,:])


        xy_range = np.array([[np.min(data[0]), np.max(data[0])],
                             [np.min(data[1]), np.max(data[1])]])*units.ANGSTROM_TO_AU

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)


        X,Y = np.meshgrid(x,y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.zeros_like(points[:,0])


        for i in range(npts**2):
            try:
                Z[i] = pot.get_value(points[i].reshape(2,1)) * units.AU_TO_EV
            except ValueError:
                Z[i] = np.nan

        Z = Z.reshape(npts, npts)

        Z = np.ma.masked_invalid(Z)
        X = np.ma.array(X, mask=Z.mask)
        Y = np.ma.array(Y, mask=Z.mask)

        import matplotlib.pyplot as plt
        from matplotlib import rcParams
        rcParams['mathtext.fontset'] = 'stixsans'
        rcParams['font.family'] = 'Arial'

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        ax.set_aspect('equal')

        cmap = plt.get_cmap('Reds')

        # surface plot
        surf = ax.pcolormesh(X, Y, Z,
                            cmap=cmap)

        # contour plot
        levels = np.linspace(*lim, num = 10)
        cont = ax.contour(X, Y, Z,
                          colors = 'black',
                          extend="both",
                          levels = levels)



        for i in [surf, cont]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel(r'$\mathbf{R}_{\mathrm{C},z}$ / a.u.')
        ax.set_ylabel(r'$\mathbf{R}_{\mathrm{O},z}$ / a.u.')

        ax.clabel(cont, fmt ='%.2f eV')

        # set a colorbar
        cbar = fig.colorbar(surf, orientation='vertical')
        cbar.set_label('V / eV')

        modes = np.zeros((2,3,2))
        for i, a in enumerate(np.linspace(-1e4,1e4,3)):
            modes[0,i] = Modes.positions_normalmodes_to_cartesian(a*np.array([1,0])).ravel()
            modes[1,i] = Modes.positions_normalmodes_to_cartesian(a*np.array([0,1])).ravel()

        ax.plot(*modes[0].T, color='SeaGreen', lw=2)
        ax.plot(*modes[1].T, color='SlateBlue', lw=2)

        ax.text(1.012*modes[0,-1,0], 1.007*modes[0,-1,1], r'$\omega_1={:.1f}\,\mathrm{{meV}}$'.format(omega[0]*1000),
                va='center',
                ha='left',
                color='SeaGreen')
        ax.text(1.01*modes[1,-1,0], 0.997*modes[1,-1,1], r'$\omega_2={:.1f}\,\mathrm{{meV}}$'.format(omega[1]*1000),
                va='center',
                ha='left',
                color='SlateBlue')
        plt.show()

    return Modes


def test_normalmodes_diatomic_internal_coordinates(show = False):
    import numpy as np
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.systems.system import System

    from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential
    from MDsim.coordinates.normalmodes import Normalmodes
    from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp

    from MDsim import units

    # hard coded masses from CASTEP
    m_C = 12.0107000
    m_O = 15.9994000

    # total mass
    M = m_C + m_O

    # first coordinate is dRcom, second is ddist
    masses = np.array([M, (m_C * m_O) / M])

    datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot'

    positions=np.zeros((2,1))

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "generic-dof",
                                    periodic = False,
                                    convert=True,
                                    normalize=True,
                                    verbose=False,
                                    forces_method = 'analytical',
                                    masses=masses.copy(),
                                    usecols = (0,1,4))
    Natoms = 2
    Ndim = 1

    omega, nm, pos_min, Modes = evaluate_normalmodes_on_potential(potential=pot,
                                                                  positions=positions,
                                                                  masses=masses,
                                                                  minimize_first=True,
                                                                  delta = 0.01,
                                                                  convert = True)

    if show:

        lim = (0,.25)
        npts = 300


        data = np.loadtxt(datafile, usecols=(0,1,4)).T
        data[2,:] -= np.min(data[2,:])


        xy_range = np.array([[np.min(data[0]), np.max(data[0])],
                             [np.min(data[1]), np.max(data[1])]])*units.ANGSTROM_TO_AU

        xy_range[0,:] = np.array([-0.15, 0.15])
        xy_range[1,:] = np.array([-0.20, 0.30])

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)


        X,Y = np.meshgrid(x,y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.zeros_like(points[:,0])
        Fx = np.zeros_like(points[:,0])
        Fy = np.zeros_like(points[:,0])

        print('Evaluating plot data')
        for i in range(npts**2):
            try:
                Z[i] = pot.get_value(points[i].reshape(2,1)) * units.AU_TO_EV
                Fx[i], Fy[i] = pot.get_forces(points[i].reshape(2,1))
            except ValueError:
                Z[i] = Fx[i] = Fy[i] = np.nan

        Z = Z.reshape(npts, npts)
        Fx = Fx.reshape(npts, npts)
        Fy = Fy.reshape(npts, npts)

        Z = np.ma.masked_invalid(Z)
        X = np.ma.array(X, mask=Z.mask)
        Y = np.ma.array(Y, mask=Z.mask)

        import matplotlib.pyplot as plt
        from matplotlib import rcParams
        rcParams['mathtext.fontset'] = 'stixsans'
        rcParams['font.family'] = 'Arial'

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)


        cmap = plt.get_cmap('Reds')

        print('Rendering plot')
        # surface plot
#         surf = ax.pcolormesh(X, Y, Z,
#                             cmap=cmap)

        # contour plot
        levels = np.linspace(*lim, num = 20)
        cont = ax.contour(X, Y, Z,
                          colors = 'gray',
                          extend="both",
                          levels = levels)

        ax.streamplot(x=x,
                      y=y,
                      u=Fx,
                      v=Fy,
                      density = 5,
                      color='black')

        for i in [cont]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel(r'$\Delta\mathbf{R}_{\mathrm{com}}$ / a.u.')
        ax.set_ylabel(r'$\Delta\mathbf{R}_{\mathrm{dist}}$ / a.u.')

        ax.clabel(cont, fmt ='%.2f eV')

        # set a colorbar
        # cbar = fig.colorbar(surf, orientation='vertical')
        # cbar.set_label('V / eV')



        modes = np.zeros((2,3,2))
        for i, a in enumerate(np.linspace(-1e4,1e4,3)):
            modes[0,i] = Modes.positions_normalmodes_to_cartesian(a*np.array([1,0])).ravel()
            modes[1,i] = Modes.positions_normalmodes_to_cartesian(a*np.array([0,1])).ravel()

        ax.plot(*modes[0].T, color='SeaGreen', lw=2)
        ax.plot(*modes[1].T, color='SlateBlue', lw=2)

        ax.set_aspect('equal')

        ax.text(1.012*modes[0,-1,0], 1.007*modes[0,-1,1], r'$\omega_1={:.1f}\,\mathrm{{meV}}$'.format(omega[0]*1000),
                va='center',
                ha='left',
                color='SeaGreen')
        ax.text(1.01*modes[1,-1,0], 0.997*modes[1,-1,1], r'$\omega_2={:.1f}\,\mathrm{{meV}}$'.format(omega[1]*1000),
                va='center',
                ha='left',
                color='SlateBlue')
        plt.show()

    return Modes


def test_normalmodes_cartesian(show = False):
    import numpy as np
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.systems.system import System

    from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential
    from MDsim.coordinates.normalmodes import Normalmodes
    from MDsim.coordinates.vibrations.diatomic.perp import InternalCoordsDiatomicPerp

    from MDsim import units

    # hard coded masses from CASTEP
    m_C = 12.0107000
    m_O = 15.9994000


    # first coordinate is dRcom, second is ddist
    masses = np.array([m_C, m_O])

    datafile = 'data/CO_on_Cu100_c2x2.pot'

    positions=np.zeros((2,1))

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "generic-dof",
                                    periodic = False,
                                    convert=True,
                                    normalize=True,
                                    verbose=False,
                                    forces_method = 'analytical',
                                    masses=masses.copy(),
                                    usecols = (0,2,4))
    Natoms = 2
    Ndim = 1

    omega, nm, pos_min, Modes = evaluate_normalmodes_on_potential(potential=pot,
                                                                  positions=positions,
                                                                  masses=masses,
                                                                  minimize_first=True,
                                                                  delta = 0.01,
                                                                  convert = True)

    if show:
        lim = (0,1.)
        npts = 300


        data = np.loadtxt(datafile, usecols=(0,1,4)).T
        data[2,:] -= np.min(data[2,:])


        xy_range = np.array([[np.min(data[0]), np.max(data[0])],
                             [np.min(data[1]), np.max(data[1])]])*units.ANGSTROM_TO_AU

        xy_range[0,:] = np.array([-0.4, 0.4])
        xy_range[1,:] = np.array([-0.40, 0.40])

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)


        X,Y = np.meshgrid(x,y)



        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.zeros_like(X)
        Fx = np.zeros_like(X)
        Fy = np.zeros_like(X)

        print('Evaluating plot data')
        for i in range(X.shape[0]):
            for j in range(Y.shape[1]):
                p = np.array([X[i,j], Y[i,j]])
                Z[i,j] = pot.get_value(p) * units.AU_TO_EV
                try:
                    Fx[i,j], Fy[i,j] = pot.get_forces(p) * units.AU_TO_EV_PER_ANGSTROM
                except ValueError:
                    Z[i,j] = Fx[i,j] = Fy[i,j] = np.nan

        Z = np.ma.masked_invalid(Z)
        X = np.ma.array(X, mask=Z.mask)
        Y = np.ma.array(Y, mask=Z.mask)

        import matplotlib.pyplot as plt
        from matplotlib import rcParams
        rcParams['mathtext.fontset'] = 'stixsans'
        rcParams['font.family'] = 'Arial'

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)


        cmap = plt.get_cmap('Reds')

        print('Rendering plot')
        # surface plot
#         surf = ax.pcolormesh(X, Y, Z,
#                             cmap=cmap)

        # contour plot
        levels = np.linspace(*lim, num = 20)
        cont = ax.contour(X, Y, Z,
                          colors = 'gray',
                          extend="both",
                          levels = levels)

        ax.streamplot(x=x,
                      y=y,
                      u=Fx,
                      v=Fy,
                      density = 5,
                      color='black')

        for i in [cont]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel(r'$\Delta\mathbf{R}_{\mathrm{C}}$ / a.u.')
        ax.set_ylabel(r'$\Delta\mathbf{R}_{\mathrm{O}}$ / a.u.')

        ax.clabel(cont, fmt ='%.2f eV')

        # set a colorbar
        # cbar = fig.colorbar(surf, orientation='vertical')
        # cbar.set_label('V / eV')

        npts = 300
        modes = np.zeros((2,npts,2))
        forces = np.zeros_like(modes)
        for i, a in enumerate(np.linspace(-1e2,1e2,npts)):
            modes[0,i,:] = Modes.positions_normalmodes_to_cartesian(a*np.array([1,0])).ravel()
            modes[1,i,:] = Modes.positions_normalmodes_to_cartesian(a*np.array([0,1])).ravel()
            forces[0,i,:] = Modes.gradient_cartesian_to_normalmodes(pot.get_forces(modes[0,i,:].reshape(2,1))).ravel()
            forces[1,i,:] = Modes.gradient_cartesian_to_normalmodes(pot.get_forces(modes[1,i,:].reshape(2,1))).ravel()

        # go along the mode and evaluate forces

        ax.plot(modes[0,:,0], modes[0,:,1], color='SeaGreen', lw=2)
        # ax.plot(*modes[1].T, color='SlateBlue', lw=2)
        ax.plot(modes[1,:,0], modes[1,:,1], color='SlateBlue', lw=2)

        ax.set_aspect('equal')

        ax.text(1.012*modes[0,-1,0], 1.007*modes[0,-1,1], r'$\omega_1={:.1f}\,\mathrm{{meV}}$'.format(omega[0]*1000),
                va='center',
                ha='left',
                color='SeaGreen')
        ax.text(1.01*modes[1,-1,0], 0.997*modes[1,-1,1], r'$\omega_2={:.1f}\,\mathrm{{meV}}$'.format(omega[1]*1000),
                va='center',
                ha='left',
                color='SlateBlue')
        plt.show()

        x = np.linspace(-1e1, 1e1, npts)
        fig = plt.figure()
        ax = fig.add_subplot(1,2,1)
        ax.plot(x, forces[0,:,0], color = 'SeaGreen', ls = '-')
        ax.plot(x, forces[0,:,1], color = 'SeaGreen', ls = '--')
        ax = fig.add_subplot(1,2,2)
        ax.plot(x, forces[1,:,0], color = 'SlateBlue', ls = '--')
        ax.plot(x, forces[1,:,1], color = 'SlateBlue', ls = '-')
        plt.show()
    return Modes


if __name__ == '__main__':
#     test_normalmodes_harmonic(show = False)
#     Modes=test_normalmodes_diatomic(show = True)
    Modes=test_normalmodes_diatomic_internal_coordinates(show = True)
#    Modes=test_normalmodes_cartesian(show = True)
