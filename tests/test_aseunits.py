# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Tests for the new constraints implemented in Sept. 2016

We now actually use the unittest framework.
"""

import os
import unittest
import shutil
import tempfile

import numpy as np


try:
    import ase
    import ase.units
    from MDsim.bindings.ase.asetomdsim import ASESystem
    _ase_avail = True
except ImportError:
    _ase_avail = False


@unittest.skipIf(not _ase_avail, "ASE not available")
class TestConstraintASESystem(unittest.TestCase):
    """
    Test functionality on the base system class
    """

    # create a test atoms object

    atoms = ase.Atoms('H2', [[0, 0, 0], [0, 0, 1.1]], cell=[5, 5, 10])
    momenta = np.random.rand(2, 3)
    atoms.set_momenta(momenta)

    verbose = False
    # a minimal setup
    kwargs = {'atoms' : atoms,
              'friction' : None,
              'temperature' : 0.,
              '_init_forces' : False,
              '_init_etas' : False,
              '_random_seed' : None,
              '_verbose' : verbose}

    eps = 1e-6
    system = ASESystem(**kwargs)

    def test_masses(self):
        self.assertTrue(np.all(self.system.get_masses() == self.atoms.get_masses()))

    def test_positions(self):
        self.assertTrue(np.all(self.system.get_positions() == self.atoms.get_positions()))

    def test_momenta(self):
        self.assertTrue(np.allclose(self.system.get_momenta(), (self.atoms.get_momenta() * ase.units.fs), atol=self.eps))

    def test_kinetic_energy(self):
        # different codata versions
        self.assertAlmostEqual(self.system.get_kinetic_energy(), self.atoms.get_kinetic_energy())

    def test_set_momenta_and_kinetic_energy(self):
        momenta = np.random.rand(*self.system.get_dimensions())
        self.system.set_velocities(momenta / self.system.get_masses()[:,None])

        self.assertTrue(np.allclose(self.system.get_momenta(), (self.system.atoms.get_momenta() * ase.units.fs), atol=self.eps))
        self.assertAlmostEqual(self.system.get_kinetic_energy(), self.system.atoms.get_kinetic_energy())


if __name__ == '__main__':
    unittest.main()
