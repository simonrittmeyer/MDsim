# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

def test_neighborlists(nsteps=1e3, show=False):
    from MDsim import units
    from MDsim.systems.pairwise.kohnlausystem import KohnLauSystem

    from MDsim.tools.createobjects import create_integrator

    from MDsim.tools.status import Status
    Nparticles = 2
    Ndim = 2

    cell_len = 20.
    cell = np.array([[cell_len,0],[0,cell_len]])

    positions = np.array([[1.,2], [6.5, 1]])
    velocities = np.array([[.1, 0], [-.1, 0]])

    dipoles = np.zeros(Nparticles)

    species = ['H' for i in range(Nparticles)]

    # set them the same value to always update neighbors lists
    lcutoff = 5
    fcutoff = 5

    T = 0

    S1 = KohnLauSystem(dipoles = dipoles.copy(),
                      force_cutoff = fcutoff,
                      list_cutoff = lcutoff,
                      unit_cell = cell.copy(),
                      positions = positions.copy(),
                      friction = None,
                      velocities = velocities.copy(),
                      potential = None,
                      _verbose=False,
                      temperature = 0,
                      species = species)

    S2 = KohnLauSystem(dipoles = dipoles.copy(),
                      force_cutoff = fcutoff,
                      list_cutoff = lcutoff,
                      unit_cell = cell.copy(),
                      positions = positions.copy(),
                      friction = None,
                      velocities = velocities.copy(),
                      potential = None,
                      _verbose=False,
                      temperature = 0,
                      species = species)

    dipoles = np.ones(Nparticles)

    S3 = KohnLauSystem(dipoles = dipoles.copy(),
                      force_cutoff = fcutoff,
                      list_cutoff = lcutoff,
                      unit_cell = cell.copy(),
                      positions = positions.copy(),
                      friction = None,
                      velocities = velocities.copy(),
                      potential = None,
                      _verbose=False,
                      temperature = 0,
                      species = species)

    integrator = create_integrator(method = 'velocity verlet', dt = 1, verbose=False)

    nsteps = int(nsteps)
    steps = range(nsteps)

    for info, S in [('not wrapped', S1), ('wrapped', S2), ('dipoles', S3)]:

        pos = np.empty((nsteps, Nparticles, Ndim))
        forces = np.empty((nsteps, Nparticles, Ndim))
        pos_wrapped = np.empty((nsteps, Nparticles, Ndim))
        num_neighbors=np.empty((nsteps, Nparticles))
        dist = np.empty((nsteps, Ndim))
        mic_dist=np.empty((nsteps))
        check=np.ones((nsteps), dtype=bool)
        for step in steps:
            integrator.propagate(S)
            num_neighbors[step] = np.count_nonzero(S.get_neighbor_masks())
            pos[step] = S.get_positions()
            forces[step] = S.get_forces()
            pos_wrapped[step] = S.get_positions()%cell_len
            dist[step] = (pos[step,0,:] - pos[step,1,:])
            mic_dist[step] = np.linalg.norm(dist[step] - cell_len * np.round(dist[step]/cell_len))

            if mic_dist[step] < lcutoff:
                if num_neighbors[step,0] == 0:
                    check[step] = False
            else:
                if num_neighbors[step,0] == 1:
                    check[step] = False

        status = Status()
        status.ok = np.all(check)
        print(' Neighbour lists are in sync with MIC distances ({}) : {}'.format(info, status))
        if not status.ok:
            raise AssertionError

        if show:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            N=4
            fig.suptitle('{} : Drawn lines are $x$ components, dotted lines are $y$ components'.format(info.upper()))

            if 'dipole' in info:
                N=5

            ax = fig.add_subplot(N,1,1)
            ax.fill_between(steps, 0, cell_len, color = 'lavender')
            ax.text(nsteps, cell_len, 'unit cell',  ha = 'right', color = 'mediumpurple')
            ax.plot(steps, pos[:,0,0], color = 'royalblue', label='x component', lw = 2)
            ax.plot(steps, pos[:,0,1], color = 'royalblue', label='y component', lw = 2, ls = ':')
            ax.plot(steps, pos[:,1,0], color = 'crimson', lw = 2)
            ax.plot(steps, pos[:,1,1], color = 'crimson', lw = 2, ls = ':')
            ax.set_ylabel('pos')
            ax.set_xticklabels([])
            ax.set_title('Positions directly from simulation')

            ax = fig.add_subplot(N,1,2)
            ax.plot(steps, pos_wrapped[:,0,0], color='royalblue', lw = 2)
            ax.plot(steps, pos_wrapped[:,0,1], color='royalblue', lw = 2, ls = ':')
            ax.plot(steps, pos_wrapped[:,1,0], color='crimson', lw =2)
            ax.plot(steps, pos_wrapped[:,1,1], color='crimson', lw =2, ls = ':')
            ax.set_xticklabels([])
            ax.set_ylabel('pos')
            ax.set_title('Positions wrapped only after simulation')

            ax = fig.add_subplot(N,1,3)
            ax.fill_between(steps, 0, lcutoff, color = 'lavender')
            ax.text(nsteps, lcutoff, 'list cutoff',  ha = 'right', color = 'mediumpurple')
            ax.plot(steps, mic_dist, color = 'teal', lw = 2)
            ax.set_ylabel('dist')
            ax.set_xticklabels([])
            ax.set_title('MIC distance (calculated afterwards)')

            ax = fig.add_subplot(N,1,4)

            ax.set_title('Number of neighbors of atom 1 (directly from simulation)')
            ax.set_ylim(-0.1,1.1)
            ax.set_xlabel('step')
            ax.set_ylabel('neighbors')
            ax.plot(steps, num_neighbors[:,0], color = 'purple', lw=2)
            ax.fill_between(steps, 0, num_neighbors[:,0], color = 'lavender')

            if N > 4:
                ax.set_xticklabels([])
                ax.set_xlabel('')

                ax = fig.add_subplot(N,1,5)

                ax.set_title('Forces on atom 1 (directly from simulation)')
                ax.set_ylim(-2.5,2.5)
                ax.set_xlabel('step')
                ax.set_ylabel('force')
                ax.plot(steps, forces[:,0,0], color = 'royalblue', lw=2)
                ax.plot(steps, forces[:,0,1], color = 'royalblue', lw=2, ls = ':')
                ax.plot(steps, forces[:,1,0], color = 'crimson', lw=2)
                ax.plot(steps, forces[:,1,1], color = 'crimson', lw=2, ls = ':')
                ax.fill_between(steps, -2.5*num_neighbors[:,0] , 2.5*num_neighbors[:,0], color = 'lavender')

            plt.show()

    if show:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set_title('With dipoles: Wrapped trajectories')
        import matplotlib.colors as colors
        import matplotlib.cm as cmx
        cm1 = plt.get_cmap('Blues')
        cm2 = plt.get_cmap('Reds')
        cNorm  = colors.Normalize(vmin=0, vmax=1)
        scalarMap1 = cmx.ScalarMappable(norm=cNorm, cmap=cm1)
        scalarMap2 = cmx.ScalarMappable(norm=cNorm, cmap=cm2)
        for i in range(nsteps-9):
            ax.plot(pos_wrapped[i:i+9,0,0], pos_wrapped[i:i+9,0,1], color = cm1(float(i)/nsteps), lw=2)
            ax.plot(pos_wrapped[i:i+9,1,0], pos_wrapped[i:i+9,1,1], color = cm2(float(i)/nsteps), lw=2)
        ax.plot(pos_wrapped[0,0,0], pos_wrapped[0,0,1], color = 'royalblue', marker='o', ms = 5)
        ax.plot(pos_wrapped[0,1,0], pos_wrapped[0,1,1], color = 'crimson', marker = 'o', ms =5 )
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        plt.show()
if __name__ == '__main__':
    test_neighborlists(show=True)
