# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

from MDsim.tools.status import Status
from MDsim.tools.options import Option

def test_options_int():
    status = Status()
    print('----------------------------------')
    print('Testing value setter for fmt="int"')
    print('----------------------------------')
    o = Option(name='test',
               fmt='int')

    print('[POSITIVE TESTS]')
    for testval in [1, 1., '1', '1.0', '   1 ', ' 1.0  ' ]:
        t = type(testval).__name__
        try:
            o.val=testval
            status.ok = (o.val == 1 and isinstance(o.val, int))
        except ValueError:
            status.ok = False
        print('\tInput "{}" type("{}") works : {}'.format(testval, t, status))
        if not status.ok:
            raise AssertionError

    print('[NEGATIVE TESTS]')
    for testval in [[1], [1.], ['1'], ['1.0']]:
        a = testval
        b = np.array(testval)

        for i in [a, b]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok=False
            except ValueError:
                status.ok=True
            print('\tInput "{}" type("{}") is rejected : {}'.format(testval, t, status))
            if not status.ok:
                raise AssertionError

def test_options_float():
    status = Status()
    print('------------------------------------')
    print('Testing value setter for fmt="float"')
    print('------------------------------------')
    o = Option(name='test',
               fmt='float')

    print('[POSITIVE TESTS]')
    for testval in [1, 1., '1', '1.0', '   1 ', ' 1.0  ' ]:
        t = type(testval).__name__
        try:
            o.val=testval
            status.ok = (o.val == 1 and isinstance(o.val, float))
        except ValueError:
            status.ok = False
        print('\tInput "{}" type("{}") works : {}'.format(testval, t, status))
        if not status.ok:
            raise AssertionError

    print('[NEGATIVE TESTS]')
    for testval in [[1], [1.], ['1'], ['1.0']]:
        a = testval
        b = np.array(testval)

        for i in [a, b]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok=False
            except ValueError:
                status.ok=True
            print('\tInput "{}" type("{}") is rejected : {}'.format(testval, t, status))
            if not status.ok:
                raise AssertionError


def test_options_str():
    status = Status()
    print('----------------------------------')
    print('Testing value setter for fmt="str"')
    print('----------------------------------')
    o = Option(name='test',
               fmt='str')

    for testval in ['1', '1.0', '   1 ', ' 1.0  ', [1], [1.], ['1'], ['1.0']]:
        a = testval
        b = np.array(testval)
        for i in [a, b]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = (isinstance(o.val, str))
            except ValueError:
                status.ok = False
            print('\tInput "{}" type("{}") works and yields "{}" : {}'.format(testval, t, o.val, status))
        if not status.ok:
            raise AssertionError

def test_options_bool():
    status = Status()
    print('-----------------------------------')
    print('Testing value setter for fmt="bool"')
    print('-----------------------------------')
    o = Option(name='test',
               fmt='bool')

    print('[POSITIVE TESTS]')
    for testval in [False, 'false', ' FALSE ', 'False', '0', 0]:
        t = type(testval).__name__
        try:
            o.val=testval
            status.ok = (o.val is False and isinstance(o.val, bool))
        except ValueError:
            status.ok = False
        print('\tInput "{}" type("{}") works : {}'.format(testval, t, status))
        if not status.ok:
            raise AssertionError

    print('[NEGATIVE TESTS]')
    for testval in [0.0, [0], [0.], ['0'], ['0.0']]:
        a = testval
        b = np.array(testval)

        for i in [a, b]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = (o.val is False and isinstance(o.val, bool))
                status.ok=False
            except ValueError:
                status.ok=True
            print('\tInput "{}" type("{}") is rejected : {}'.format(testval, t, status))
            if not status.ok:
                raise AssertionError

def test_options_choices():
    status = Status()
    print('---------------------------------------')
    print('Testing choice validation for fmt="str"')
    print('---------------------------------------')
    o = Option(name='test',
               fmt='str',
               choices=['one', 'two'])

    print('[POSITIVE TESTS]')
    for testval in ['one', 'two']:
        a = testval
        b = np.array(testval)
        for i in [a, b]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = (isinstance(o.val, str) and o.val == testval)
            except ValueError:
                status.ok = False
            print('\tInput "{}" type("{}") works and yields "{}" : {}'.format(testval, t, o.val, status))
        if not status.ok:
            raise AssertionError

    print('[NEGATIVE TESTS]')
    for testval in ['three', 1, 2, 1., 2.]:
        a = testval
        b = np.array(testval)
        c = [testval]
        for i in [a, b,c]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = False
            except ValueError:
                status.ok = True
            print('\tInput "{}" type("{}") is rejected : {}'.format(testval, t, status))
        if not status.ok:
            raise AssertionError

def test_options_choices_mult_list():
    status = Status()
    print('--------------------------------------------')
    print('Testing choice validation for fmt="str_list"')
    print('--------------------------------------------')
    o = Option(name='test',
               fmt='str_list',
               choices=['one', 'two'])

    print('[POSITIVE TESTS]')
    for testval, result in [('one', ['one']),
                            ('two', ['two']),
                            ('one, two', ['one', 'two']),
                            (['one', 'two'], ['one', 'two']),
                            (['one', 'one '], ['one', 'one'])]:
        a = testval
        for i in [a]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = (isinstance(o.val, list) and (o.val == result))
            except ValueError:
                status.ok = False
            print('\tInput "{}" type("{}") works and yields "{}" : {}'.format(testval, t, o.val, status))
    if not status.ok:
        raise AssertionError

    print('[NEGATIVE TESTS]')
    for testval in ['three', ['one', 1], ['one', 'three'], 1, 2, 1., 2.]:
        a = testval
        b = np.array(testval)
        c = [testval]
        for i in [a, b, c]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = False
            except ValueError:
                status.ok = True
            print('\tInput "{}" type("{}") is rejected : {}'.format(testval, t, status))
        if not status.ok:
            raise AssertionError


def test_options_choices_mult_array():
    status = Status()
    print('---------------------------------------------')
    print('Testing choice validation for fmt="str_array"')
    print('---------------------------------------------')
    o = Option(name='test',
               fmt='str_array',
               choices=['one', 'two'])

    print('[POSITIVE TESTS]')
    for testval, result in [('one', np.array(['one'])),
                            ('two', np.array(['two'])),
                            (['one', 'two'], np.array(['one', 'two'])),
                            (['one', 'one '], np.array(['one', 'one']))]:
        a = testval
        for i in [a]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = (isinstance(o.val, np.ndarray) and (np.array_equal(o.val,result)))
            except ValueError:
                status.ok = False
            print('\tInput "{}" type("{}") works and yields "{}" : {}'.format(testval, t, result, status))
        if not status.ok:
            raise AssertionError

    print('[NEGATIVE TESTS]')
    for testval in ['three', ['one', 1], ['one', 'three'], 1, 2, 1., 2.]:
        a = testval
        b = np.array(testval)
        c = [testval]
        for i in [a, b, c]:
            t = type(i).__name__
            try:
                o.val=i
                status.ok = False
            except ValueError:
                status.ok = True
            print('\tInput "{}" type("{}") is rejected : {}'.format(testval, t, status))
        if not status.ok:
            raise AssertionError

if __name__ == '__main__':
    test_options_int()
    test_options_float()
    test_options_str()
    test_options_bool()
    test_options_choices()
    test_options_choices_mult_list()
    test_options_choices_mult_array()
