# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python

import numpy as np

import ase
from ase.constraints import FixAtoms

from MDsim.bindings.castep import ExtendedCastep
from MDsim.mainroutines.aimdef import main

from MDsim.tools.status import Status

def test_aimdefmain(show=False):
    try:
        from ase.test.castep import installed
        installed()
    except Exception:
        print('Either castep is not installed, skipping test...')
        return

    status = Status()
    for friction in [None, 'iaa', 'aim']:
        atoms = ase.Atoms('H3', [[0, 0, 0], [0, 0, 1.2], [0, 0, 2.4]], cell=[5, 5, 10])

        constraints = FixAtoms(indices=[2])
        atoms.set_constraint(constraints)


        atoms.center()
        calc = ExtendedCastep(verbose=False)

        calc._label = 'H3'
        calc._rename_existing_dir = False
        atoms.set_calculator(calc)
        atoms.calc.set_pspot(pspot = 'OTF')
        atoms.calc.cell.kpoints_mp_grid = '1 1 1'

        adsorbate_idx = [0,1]

        if friction is None:
            _f = 'none'
        else:
            _f = friction

        folder = 'output/{}'.format(_f)


        print(folder)
        try:
            kwargs = {
                      'atoms':None,
                      'adsorbate_idx':None,
                      'friction_model':None,
                      'electronic_temperature':0.,
                      'simulation_time':3,
                      'timestep':1,
                      'integrator':'liouville',
                      'update_step':1,
                      'walltime':'01:00:00',
                      'seed':'AIMD+EF-MDsim',
                      'outfolder':folder,
                      'observed_quantities':['forces', 'energy'],
                      'preview':show,
                      'random_seed':None,
                      'verbose':False,
                      'restart_traj':None,
                      'keep_checkfiles':2}

            main(**kwargs)
            status.ok = False
        except ValueError:
            status.ok = True

        print("{} : Incomplete input is rejected {}".format(friction, status))

        kwargs.update({'atoms' : atoms})
        main(**kwargs)

if __name__ == '__main__':
    test_aimdefmain(show=False)
