# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from MDsim.bindings.castep.castepnormalmodes import CastepNormalmodes

def test_castepnormalmodes():
    phononfile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top_vib-2D.phonon'

    NM = CastepNormalmodes(phononfile=phononfile)

if __name__ == '__main__':
    test_castepnormalmodes()
