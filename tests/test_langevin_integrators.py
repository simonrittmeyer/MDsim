# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.


def test_langevin_integrators(show = False, steps = 1e2, free=True, drag=True, harmonic=True):
    import numpy as np
    import random
    import time

    import scipy.signal

    from MDsim import units
    from MDsim.systems.system import System

    from MDsim.friction.scalarfriction import ScalarFriction

    from MDsim.tools.createobjects import create_integrator

    from MDsim.analysis.fitting import fit_exponential
    from MDsim.potentials.atomic.dragpotential import DragPotential
    from MDsim.potentials.atomic.harmonicpotential import HarmonicPotential
    steps = int(steps)

    integrators = [#'Gronbech-Jensen/Farago',
                   # our BBK implementation seems broken
                   #'Bruenger/Brooks/Karplus',
                   #'Vanden-Eijnden/Ciccotti',
                   'Bussi/Parrinello'
                   ]

    etas = [1.34, 0.23, 0.0079]
    T = 317
    dt = 1.
    Natoms = int(2.4e3)
    # hardcode Ndim=1; makes tests easier
    Ndim = 1

    omega =24.67e-3

    force=1.32543e-2

    print('-'*80)
    print('LANGEVIN INTEGRATOR TEST')
    print('-'*80)
    print('test run on  : {}'.format(time.strftime('%c')))
    print('-'*80)
    print('Thermostatted temperature : {} K'.format(T))
    print('Time steps : {}'.format(steps))
    print('Time step size : {} fs'.format(dt))
    print('Natoms : {}'.format(Natoms))
    print('Ndim : {}'.format(Ndim))
    print('')


    for integrator in integrators:
        Ekin = np.zeros((steps, len(etas)))
        temp = np.zeros_like(Ekin)
        msd = np.zeros((steps, len(etas)))
        msd_harm = np.zeros((steps, len(etas)))
        drift = np.zeros((steps, len(etas)))
        drift_lims = np.zeros(len(etas))
        msv = np.zeros((steps, len(etas)))
        msv_harm = np.zeros((steps, len(etas)))
        v = np.zeros((steps, len(etas), Natoms, Ndim))
        Ds = np.zeros(len(etas))
        decays = np.zeros(len(etas))
        decay_lims = np.zeros(len(etas))
        T_sys = np.zeros(len(etas))
        # velocity autocorrelation
        vacf=np.zeros((steps, len(etas)))
        vacf_fft=np.zeros((steps, len(etas)))
        propagator = create_integrator(method = integrator,
                                       dt = dt, verbose = False)

        head = 'Integrator method : {}'.format(integrator)

        print('')
        print('+'*len(head))
        print(head)
        print('+'*len(head))

        for i, eta in enumerate(etas):
            steps_range = np.arange(0,steps)
            times = steps_range * dt
            head = 'eta = {:.3f} amu/fs'.format(float(eta))
            print('='*len(head))
            print(head)
            print('='*len(head))
            if free:
                positions = np.zeros((Natoms, Ndim))
                species = ['Na' for j in range(Natoms)]

                velocities = None

                S = System(positions = positions,
                           species = species,
                           velocities = velocities,
                           temperature = T,
                           friction = eta,
                           potential = None,
                           convert = True,
                           _verbose = False)

                for step in steps_range:
                    Ekin[step,i] = S.get_kinetic_energy()
                    temp[step,i] = S.get_instantaneous_temperature()
                    msd[step,i] = np.mean(np.sum(S.get_positions()**2, axis = 1))
                    # velocity auto correlation
                    v[step,i,:,:] = S.get_velocities()
                    vacf[step,i] = S.get_velocity_autocorrelation()
                    # mean qsuared velocities
                    msv[step,i] = np.mean(np.sum(S.get_velocities()**2, axis = 1))
                    propagator.propagate(S)



                # mean kinetic energy (ensemble average)
                Ekin_mean = np.mean(Ekin) / (Natoms * Ndim)

                # lim_t-->infty <x**2(t)> / t = 2*D
                # where D = (kT) / (m**2*eta)
                # See Pastor et al., MolPhys 1988
                # note: gamma = "our eta" / m and hence "our eta" = gamma * m

                # as we do not have the conversion of a diffusion constant...
                D = (S.get_kT() / S.get_etas()[0,0])*units.EV_FS_PER_AMU_TO_AU*units.AU_TO_ANGSTROM**2/units.AU_TO_FS

                Ds[i] = D

                D_inst = msd[1:,i]/(times[1:]*2*Ndim)

                T_sys[i] = np.mean(temp[:,i])

                # mean squared velocity
                # explicite Pastor et al. tests
                msd_mean = np.mean(msd[1:,i]/times[1:])
                msd_sigma = np.std(msd[1:,i]/times[1:])
                msd_lim = 2* Ndim * D

                msv_mean = np.mean(msv[:,i])
                msv_sigma = np.std(msv[:,i])
                msv_lim = S.get_kT()*units.EV_TO_AU / (S.get_masses()[0]*units.AMU_TO_AU) * units.AU_TO_ANGSTROM_PER_FS**2
                msv_lim *= Ndim

#                # get the autocorrelation function via scipy's fft
#                for n in range(Natoms):
#                    vacf_fft[:,i] += scipy.signal.fftconvolve(v[:,i,n].flatten(), v[::-1,i,n].flatten(), mode='full')[steps-1::]

#                vacf_fft[:,i] /= Natoms

                # continouos limit of velocity autocorrelation is given by
                # kT / m * exp(- eta/m  * t)
                # see for instance Tuckerman book on stat mech, eq (15.3.8)
                opt_p, fit_func = fit_exponential(x=times, y=vacf[:,i], show=False)
                decay = opt_p[1]
                decays[i] = decay
                decay_lim = (S.get_etas()[0,0]) / (S.get_masses()[0])
                decay_lims[i] = decay_lim

                print('* Temperature')
                print('  -----------')
                print('\t<T>_t    = {:.3f} K'.format(T_sys[i]))
                print('\tsigma(T) = {:.3f} K'.format(np.std(temp[:,i])))
                print('')
                print('* Diffusion')
                print('  ---------')
                print('\tD_inst(t_end)      = {:.6f} mA**2/fs'.format(D_inst[-1]*1000))
                print('\t<D_inst(t)>_t      = {:.6f} mA**2/fs'.format(np.mean(D_inst)*1000))
                print('\tsigma(D_inst(t))_t = {:.6f} mA**2/fs'.format(np.std(D_inst)*1000))
                print('\t-----------')
                print('\tD_Einstein         = {:.6f} mA**2/fs'.format(D*1000))
                print('')
                print('\tN.B.')
                print('\t----')
                print('\tDef: MSD(t) = <(pos(t) - pos(0))**2>_ensemble')
                print('\tDef: D_inst(t) = MSD(t)/(2*Ndim*t)')
                print('')

                print('* Tests proposed by Pastor et al.')
                print('  -------------------------------')
                print('  FREE BROWNIAN PARTICLE')
                print('  ----------------------')
                print('\t(a) Mean squared displacements')
                print('\t------------------------------')
                print('\t\t<<(R(t)-R(0))**2> /t _ensemble>_t = {:.6f} mA**2/fs'.format(msd_mean*1000))
                print('\t\tsigma_t                           = {:.6f} mA**2/fs'.format(msd_sigma*1000))
                print('\t\t----------------')
                print('\t\tcontinuous limit                  = {:.6f} mA**2/fs'.format(msd_lim*1000))
                print('')
                print('\t(b) Mean squared velocity')
                print('\t-------------------------')
                print('\t\t<<v(t)**2 / t >_ensemble>_t = {:.6f} mA**2/fs**2'.format(msv_mean*1000))
                print('\t\tsigma_t                     = {:.6f} mA**2/fs**2'.format(msv_sigma*1000))
                print('\t\t----------------')
                print('\t\tcontinuous limit            = {:.6f} mA**2/fs**2'.format(msv_lim*1000))
                print('')
                print('\t(c) Velocity autocorrelation')
                print('\t----------------------------')
                print('\t\tdecay rate (fitted) = {:.6f} ps**-1'.format(decay*1000))
                print('\t\t----------------')
                print('\t\tcontinuous limit    = {:.6f} ps**-1'.format(decay_lim*1000))
                print('')

            if drag:
                drag_forces = np.ones((Natoms, Ndim))*force
                potential = DragPotential(drag_forces=drag_forces,
                                          convert = False)

                # the harmonic oscillator properties
                positions = np.zeros((Natoms, Ndim))
                species = ['Na' for j in range(Natoms)]

                velocities = None
                S = System(positions = positions,
                           species = species,
                           velocities = velocities,
                           temperature = T,
                           friction = eta,
                           potential = potential,
                           convert = True,
                           _verbose = False)

                pos_old=None
                for step in steps_range:
                    if step == 0:
                        pos_old = S.get_positions().flatten()
                        continue
                    drift[step,i]= np.mean((S.get_positions().flatten() - pos_old) / dt)
                    pos_old=S.get_positions().flatten()
                    propagator.propagate(S)

                drift_mean = np.mean(drift[1:,i])
                drift_sigma = np.std(drift[1:,i])

                drift_lim = (potential.get_forces(None)[0,0]/(S.get_etas()[0,0]*units.AMU_PER_FS_TO_AU))*units.AU_TO_ANGSTROM_PER_FS
                drift_lims[i] = drift_lim

                print('  DRAGGED BROWNIAN PARTICLE')
                print('  --------------------------')
                print('\t(d) Terminal Drift')
                print('\t------------------')
                print('\t\t<terminal drift>    = {:.6f} A/ps'.format(drift_mean*1000))
                print('\t\tsigma_t             = {:.6f} A/ps'.format(drift_sigma*1000))
                print('\t\t----------------')
                print('\t\tcontinuous limit    = {:.6f} A/ps'.format(drift_lim*1000))
                print('')

            if harmonic:
                omegas = np.ones((Natoms, Ndim))*omega
                potential = HarmonicPotential(omegas = omegas,
                                              masses = S.get_masses(),
                                              convert = True)

                # the harmonic oscillator properties
                positions = np.zeros((Natoms, Ndim))
                species = ['Na' for j in range(Natoms)]

                velocities = None
                S = System(positions = positions,
                           species = species,
                           velocities = velocities,
                           temperature = T,
                           friction = eta,
                           potential = potential,
                           convert = True,
                           _verbose = False)
                for step in steps_range:
                    msd_harm[step,i] = np.mean(np.sum(S.get_positions()**2, axis = 1))
                    msv_harm[step,i] = np.mean(np.sum(S.get_velocities()**2, axis = 1))
                    propagator.propagate(S)

                # mean squared velocity
                # explicite Pastor et al. tests
                msd_harm_mean = np.mean(msd_harm[1:,i])
                msd_harm_sigma = np.std(msd_harm[1:,i])
                msd_harm_lim = Ndim*(S.get_kT()*units.EV_TO_AU) / (potential.get_spring_constants()[0,0]*units.EV_PER_ANGSTROM_SQUARED_TO_AU)*units.AU_TO_ANGSTROM**2

                msv_harm_mean = np.mean(msv_harm[1:,i])
                msv_harm_sigma = np.std(msv_harm[1:,i])
                msv_harm_lim = Ndim*(S.get_kT()*units.EV_TO_AU) / (S.get_masses()[0]*units.AMU_TO_AU)*units.AU_TO_ANGSTROM_PER_FS**2

                virial = ((S.get_masses()[0]*units.AMU_TO_AU * msv_harm_mean * units.ANGSTROM_PER_FS_TO_AU**2)
                         -(potential.get_spring_constants()[0,0] * units.EV_PER_ANGSTROM_SQUARED_TO_AU * msd_harm_mean * units.ANGSTROM_TO_AU**2))*units.AU_TO_EV

                print('  HARMONIC BROWNIAN PARTICLE')
                print('  --------------------------')
                print('\t(e) Mean squared displacements')
                print('\t------------------------------')
                print('\t\t<<(R(t))**2>_ensemble>_t = {:.6f} mA**2'.format(msd_harm_mean*1000))
                print('\t\tsigma_t                  = {:.6f} mA**2'.format(msd_harm_sigma*1000))
                print('\t\t----------------')
                print('\t\tcontinuous limit         = {:.6f} mA**2'.format(msd_harm_lim*1000))
                print('')
                print('\t(f) Mean squared velocity')
                print('\t-------------------------')
                print('\t\t<<(V(t))**2>_ensemble>_t = {:.6f} (A/fs)**2'.format(msd_harm_mean*1000))
                print('\t\tsigma_t                  = {:.6f} (A/fs)**2'.format(msd_harm_sigma*1000))
                print('\t\t----------------')
                print('\t\tcontinuous limit         = {:.6f} (A/fs)**2'.format(msd_harm_lim*1000))
                print('')
                print('\t(g) Virial theorem')
                print('\t------------------')
                print('\t\tm<v**2(t)> - k<x**2(t)>  = {:.6f} eV'.format(virial))
                print('\t\t-----------------------')
                print('\t\tcontinuous limit         = {:.6f} eV'.format(0))
        if show:
            update_frequency = 1

            def color(index):
                colors = ['red', 'blue', 'green', 'orange', 'purple']
                l = len(colors)
                return colors[i%l]


            import matplotlib.pyplot as plt
            from matplotlib import rcParams
            rcParams['mathtext.fontset'] = 'stixsans'
            rcParams['font.family'] = 'Arial'

            if free:
                fig = plt.figure()
                T_target = S.get_temperature()

                for i, eta in enumerate(etas):
                    ax = fig.add_subplot(len(etas),1,i+1)

                    Tinst = temp[:,i]
                    ax.plot(times/1000., Tinst,
                            color = color(i),
                            label = r'$\eta = {}$ amu/fs'.format(eta))

                    ax.legend(loc = 'lower right')

                    ax.axhline(y = T_target, color = 'black', lw = 2, ls = '--')
                    ax.axhline(y = T_sys[i], color = color(i), lw = 2, ls = '--')

                    ax.set_ylabel(r'temperature $T$ (K)')
                    ax.set_xlabel(r'time $t$ (ps)')

                fig.suptitle('Free Brownian; Temperature; Integrator method : "{}"'.format(integrator))
                plt.show()

                fig = plt.figure()

                for i, eta in enumerate(etas):
                    ax = fig.add_subplot(len(etas),1,i+1)
                    D = Ds[i]
                    ax.plot(times[1:]/1000.,
                            msd[1:,i]/(times[1:]),
                            color = color(i),
                            lw = 2,
                            label = r'$\eta = {}$ amu/fs'.format(eta))
                    ax.axhline(y = 2*Ndim*D, color = 'black', lw = 2, ls = '--')
                    ax.legend(loc = 'lower right')

                    ax.set_ylabel(r'$\langle(\mathbf{R}(t)-\mathbf{R}(0))^2\rangle_\mathrm{ensemble}$ ($\mathrm{\AA}^2$)')
                    ax.set_xlabel(r'Time $t$ (ps)')

                fig.suptitle('Free Brownian; Diffusion Constant; Integrator method : "{}"'.format(integrator))
                plt.show()

                fig = plt.figure()

                for i, eta in enumerate(etas):
                    ax = fig.add_subplot(len(etas),1,i+1)
                    ax.plot(times/1000.,
                            vacf[:,i],
                            color = color(i),
                            lw = 2,
                            label = r'$\eta = {}$ amu/fs'.format(eta))

                    # continouos limit is given by
                    # kT / m * exp(- eta * m * t)
                    # see for instance Tuckerman book on stat mech, eq (15.3.8)
                    kT_over_m = (S.get_kT() * units.EV_TO_AU) /(S.get_masses()[0] * units.AMU_TO_AU) * units.AU_TO_ANGSTROM_PER_FS**2
                    gamma = decay_lims[i]

                    def lim(t):
                        return Ndim *kT_over_m *  np.exp(-gamma*t)

                    ax.plot(times/1000.,
                            lim(times),
                            color = 'black',
                            lw = 2,
                            ls ='--')

                    ax.legend(loc = 'lower right')

                    ax.set_ylabel(r'$\langle \mathbf{v}_i(0)\cdot\mathbf{v}_i(t)\rangle$ ($\AA^2/\mathrm{fs}^2$)')
                    ax.set_xlabel(r'Time $t$ (ps)')

                fig.suptitle('Free Brownian; Velocity Autocorrelation; Integrator method : "{}"'.format(integrator))
                plt.show()

            if drag:
                fig = plt.figure()
                for i, eta in enumerate(etas):
                    ax = fig.add_subplot(len(etas),1,i+1)

                    ax.plot(times/1000., drift[:,i]*1000,
                            color = color(i),
                            lw=2,
                            label = r'$\eta = {}$ amu/fs'.format(eta))

                    ax.legend(loc = 'lower right')

                    ax.axhline(y = np.mean(drift[:,i])*1000, color = color(i), lw = 2, ls = '--')
                    ax.axhline(y = drift_lims[i]*1000, color = 'black', lw = 2, ls = '--')

                    ax.set_ylabel(r'$\langle \mathbf{R}(t)-\mathbf{R}(t-\Delta t) \rangle / \Delta t$ ($\AA/\mathrm{ps}$)')
                    ax.set_xlabel(r'time $t$ (ps)')
                fig.suptitle('Drag Force; Terminal Drift; Integrator method : "{}"'.format(integrator))
                plt.show()

            if harmonic:
                fig = plt.figure()
                for i, eta in enumerate(etas):
                    ax = fig.add_subplot(len(etas),1,i+1)

                    ax.plot(times/1000., msd_harm[:,i]*1000,
                            lw=2,
                            color = color(i),
                            label = r'$\eta = {}$ amu/fs'.format(eta))

                    ax.legend(loc = 'lower right')

                    ax.axhline(y = msd_harm_lim*1000, color = 'black', lw = 2, ls = '--')
                    ax.axhline(y = np.mean(msd_harm[:,i]*1000), color = color(i), lw = 2, ls = '--')

                    ax.set_ylabel(r'$\langle \mathbf{R}(t)^2\rangle$ ($\mathrm{m}\AA^2$)')
                    ax.set_xlabel(r'time $t$ (ps)')
                fig.suptitle('Harmonic Oscillator; Mean Square Displacements; Integrator method : "{}"'.format(integrator))
                plt.show()

                fig = plt.figure()
                for i, eta in enumerate(etas):
                    ax = fig.add_subplot(len(etas),1,i+1)

                    ax.plot(times/1000., msv_harm[:,i],
                            color = color(i),
                            lw=2,
                            label = r'$\eta = {}$ amu/fs'.format(eta))

                    ax.legend(loc = 'lower right')

                    ax.axhline(y = msv_harm_lim, color = 'black', lw = 2, ls = '--')
                    ax.axhline(y = np.mean(msv_harm[:,i]), color = color(i), lw = 2, ls = '--')

                    ax.set_ylabel(r'$\langle \mathbf{v}(t)^2\rangle$ ($\AA/\mathrm{fs})^2$)')
                    ax.set_xlabel(r'time $t$ (ps)')
                fig.suptitle('Harmonic Oscillator; Mean Square Velocities; Integrator method : "{}"'.format(integrator))
                plt.show()

if __name__ == '__main__':
    test_langevin_integrators(show=True,free=True, drag=True, harmonic=True, steps = 1e3)
