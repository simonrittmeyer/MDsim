# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
def test_observables():
    from MDsim.tools.createobjects import create_observable
    from MDsim.tools.createobjects import create_dummy_system
    from MDsim.observables.implemented import implemented

    system = create_dummy_system(Natoms = 2, Ndim = 2)

    for obs in implemented:
        if np.any(o in obs for o in ['scattering_amplitudes', 'isf', 'dsf', 'eta','normalmodes']):
            continue

        O = create_observable(system_inst=system, observable=obs, iatom = 1)
        print O
        print O.get_idstring()
        print O.get_headline()
        print O.get_dataline()
        print '+'*80

if __name__ == '__main__':
    test_observables()
