# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

from MDsim.fileio.netcdfhandler import NetCDFHandler

import numpy as np
import shutil
import random

from MDsim import units
from MDsim.systems.system import System

from MDsim.friction.scalarfriction import ScalarFriction

from MDsim.tools.createobjects import create_minimumbasisfourierpotential
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_filehandler

from MDsim.tools.status import Status

def test_netcdfsupport(nsteps = 1e3, Natoms=2, remove=True):

    verbose=True
    status = Status()

    print('Running simple test simulation...', end='')
    a = 3.610 / np.sqrt(2)
    pes = create_minimumbasisfourierpotential(a=a,
                                              datafile='data/Na_on_Cu111_adsorption_sites.pot',
                                              verbose=verbose)

    Ndim = 2

    positions = np.random.randn(Natoms, Ndim)
    species = ['Na' for i in range(Natoms)]

    velocities = np.zeros_like(positions)
    eta = ScalarFriction(eta = .1, convert = False)
    T = 500
    #pes=None

    system = System(positions = positions,
               species = species,
               velocities = velocities,
               temperature = T,
               friction = eta,
               potential = pes,
               convert = True,
               _verbose=verbose)

    # hard coded from DJW's mail to maintain comparability
    integrator = create_integrator(method='bussi parrinello',
                                   dt=10,
                                   verbose=verbose)


    handlers=[]
    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='netcdf4',
                                       seed='netcdf4',
                                       observable='positions',
                                       compress=False,
                                       verbose=verbose))

    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='netcdf4_inmemory',
                                       seed='netcdf4_inmemory',
                                       observable='positions',
                                       Nsteps=int(nsteps)+1,
                                       compress=False,
                                       verbose=verbose))

    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='ascii',
                                       seed='ascii',
                                       observable='positions',
                                       compress=False,
                                       verbose=verbose))
    for n in range(int(nsteps)):
        integrator.propagate(system)
        for handler in handlers:
            handler.push()

    for handler in handlers:
        handler.close()

    print('done')

    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass
if __name__ == '__main__':
    test_netcdfsupport(nsteps = 2**12, Natoms = 50, remove=False)
