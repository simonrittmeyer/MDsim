# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase

from MDsim import units

from ase.calculators.aims import Aims

from MDsim.bindings.castep import installed
from MDsim.bindings.castep import ExtendedCastep
from MDsim.bindings.castep import CastepEFSystem

from MDsim.tools.status import Status


def test_castepsystem(remove=True):
    try:
        installed()
    except Exception:
        print('Some of castep/castep_hirshfeld/castep2cube are not installed, skipping test...')
        return
    status = Status()
    verbose = False
    atoms = ase.Atoms('H2')
    calc = Aims()
    atoms.set_calculator(calc)

    try:
        CastepEFSystem(atoms=atoms)
        status.ok = False
    except ValueError:
        status.ok = True

    print('CastepEFSystem rejects wrong calculator: {}'.format(status))
    if not status.ok:
        raise AssertionError


    atoms = ase.Atoms('H2', [[0, 0, 0], [0, 0, 1.]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep(verbose = verbose)

    calc._label = 'H2'
    calc._rename_existing_dir = False
    calc._copy_pspots = False
    calc._link_pspots = True

    if os.path.isdir('output'):
        shutil.rmtree('output')


    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    try:
        system = CastepEFSystem(atoms, adsorbate_idx = 1, _verbose=True, friction=1)
        ok = False
    except ValueError:
        ok = True

    try:
        system = CastepEFSystem(atoms, adsorbate_idx = 1, _verbose=False, friction='asfdf')
        status.ok = False
    except NotImplementedError:
        status.ok = True and ok

    print('CastepEFSystem rejects wrong friction: {}'.format(status))
    if not status.ok:
        raise AssertionError


    system = CastepEFSystem(atoms, calc_folder='output/aim', adsorbate_idx = 1, _verbose=verbose, friction='aim', _init_forces=True, _init_etas=True)

    eps = 1e-10

    pos = system.get_positions()
    status.ok = np.all(np.abs(pos - atoms.get_positions()) < eps)
    print('Initial system positions == atoms positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    slab_mask = system.get_slab_mask()

    status.ok = np.all(np.abs(pos[slab_mask,:] - system.slab_atoms.get_positions()) < eps)
    print('Subset of initial system positions == slab atoms positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    mom = system.get_momenta(_au=True)*units.MOMENTUM_AU_TO_ASE
    status.ok = np.all(np.abs(mom - atoms.get_momenta()) < eps)
    print('Initial system momenta == atoms momenta : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(mom[slab_mask,:] - system.slab_atoms.get_momenta()) < eps)
    print('Subset of initial system momenta == slab atoms momenta : {}'.format(status))
    if not status.ok:
        raise AssertionError

    try:
        forces = system.get_forces()
        status.ok = True
    except StandardError:
        forces = np.empty_like(pos)
        status.ok = False

    print('Evaluating forces: {}'.format(status))
    if not status.ok:
        raise AssertionError

    try:
        Epot = system.get_potential_energy()
        status.ok = True
    except StandardError:
        Epot = -1234567890
        status.ok = False

    print('Evaluating energy: {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = system.atoms.calc._calls == 1
    print('Force and energy evaluation use same underlying QM calculation : {}'.format(status))
    if not status.ok:
        raise AssertionError


    status.ok = not np.all([system._update_Epot, system._update_forces, system._update_etas])
    print('Evaluating energies/forces sets reuse flag : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = abs(system.calc_potential_energy(system.get_positions()*units.ANGSTROM_TO_AU)*units.AU_TO_EV - Epot) < eps
    status.ok = status.ok and system.atoms.calc._calls == 1
    print('Reusing previously calculated energies : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(system.calc_forces(system.get_positions()*units.ANGSTROM_TO_AU)*units.AU_TO_EV_PER_ANGSTROM - forces) < eps)
    status.ok = status.ok and system.atoms.calc._calls == 1
    print('Reusing previously calculated forces : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.set_positions(system.get_positions()*units.ANGSTROM_TO_AU + 1.)
    status.ok = np.all([system._update_Epot, system._update_forces, system._update_etas])
    print('Updating positions updates reuse flags : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.set_velocities(system.get_velocities() * units.ANGSTROM_PER_FS_TO_AU + 1.)
    status.ok = np.all([system._update_Ekin])
    print('Updating velocities updates reuse flags : {}'.format(status))
    if not status.ok:
        raise AssertionError

    pos = system.get_positions()
    status.ok = np.all(np.abs(pos - system.atoms.get_positions()) < eps)
    print('Updated system positions == atoms positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(pos[slab_mask,:] - system.slab_atoms.get_positions()) < eps)
    print('Subset of updated system positions == slab atoms positions : {}'.format(status))
    if not status.ok:
        raise AssertionError

    mom = system.get_momenta(_au=True)*units.MOMENTUM_AU_TO_ASE
    status.ok = np.all(np.abs(mom - system.atoms.get_momenta()) < eps)
    print('Updated system momenta == atoms momenta : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(mom[slab_mask,:] - system.slab_atoms.get_momenta()) < eps)
    print('Subset of updated system momenta == slab atoms momenta : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = system.atoms.calc.calculation_required(system.atoms)
    print('Changing positions requires new calculator call : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = system.slab_atoms.calc.calculation_required(system.slab_atoms)
    print('Changing positions requires new slab calculator call : {}'.format(status))
    if not status.ok:
        raise AssertionError

    forces = system.get_forces()
    status.ok = not(system.atoms.calc.calculation_required(system.atoms))
    print('Evaluating forces set "calculation_required" to False : {}'.format(status))
    if not status.ok:
        raise AssertionError

    etas = system.get_etas()
    status.ok = system.atoms.calc._continuation_calls == 2 and system.atoms.calc._calls == 2
    print('New positions require new AIM-density calculation but SCF is reused : {}'.format(status))
    if not status.ok:
        raise AssertionError

    etas = system.get_etas()
    status.ok = (system.atoms.calc._continuation_calls == 2 and
                 system.atoms.calc._calls == 2)
    print('AIM densities are reused : {}'.format(status))
    if not status.ok:
        raise AssertionError

    # the iaa tests
    calc = ExtendedCastep(verbose = verbose)
    calc._label = 'H3'
    calc._rename_existing_dir = False
    calc._copy_pspots = False
    calc._copy_pspots = True
    calc._directory = 'castep_runs'

    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    system = CastepEFSystem(atoms, calc_folder='output/iaa', adsorbate_idx = 1, _verbose=verbose, friction='iaa', _init_forces=True, _init_etas=True)
    etas = system.get_etas()


    status.ok = system.slab_atoms.calc._continuation_calls == 1 and system.slab_atoms.calc._calls == 1
    print('Initially slab atoms calculator is in sync w.r.t. scf and continuation : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.set_positions(system.get_positions()*units.ANGSTROM_TO_AU + 1.)

    status.ok = system.atoms.calc.calculation_required(system.atoms)
    print('Updating positions requires new calculation in atoms: {}'.format(status))
    if not status.ok:
        raise AssertionError


    status.ok = system.slab_atoms.calc.calculation_required(system.slab_atoms) and system._update_etas
    print('Updating positions requires new calculation in slab atoms: {}'.format(status))
    if not status.ok:
        raise AssertionError

    etas = system.get_etas()
    status.ok = (system.slab_atoms.calc._continuation_calls == 2
                 and system.slab_atoms.calc._calls == 2)
    print('Slab atoms calculator remains in sync w.r.t. scf and continuation after position update: {}'.format(status))
    if not status.ok:
        raise AssertionError


    etas = system.get_etas()
    etas = system.get_etas()
    status.ok = (system.slab_atoms.calc._continuation_calls == 2 and
                 system.slab_atoms.calc._calls == 2)
    print('IAA densities are reused : {}'.format(status))

    if not status.ok:
        raise AssertionError
    if remove:
        shutil.rmtree('output/iaa')
        shutil.rmtree('output/aim')

if __name__=='__main__':
    test_castepsystem(remove=False)

