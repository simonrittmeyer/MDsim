# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

# test the create routines
import numpy as np
import shutil

def test_create_integrator():
    from MDsim.tools.createobjects import create_integrator
    ints = ['velocityverlet',
            'liouville',
            'gjf',
            #'bbk',
            'vec',
            'bp']
    for i in ints:
        res = create_integrator(method = i, dt = 5.1243, seed = 132)
        if res is None:
            raise AssertionError
    try:
        create_integrator(method = 'fgjkhfd')
    except NotImplementedError:
        pass


def test_create_bivariatefriction(show = False):
    from MDsim.tools.createobjects import create_bivariatefrictioncoefficient as create_bivariatefriction

    eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                   usecols = (0,1,4))

    if show:
        eta.show()

    eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                   periodic = True,
                                   usecols = (0,1,4))
    if show:
        eta.show()

    masses = np.array([12.,16.])
    positions_eq=np.array([[21.22994700],[22.38415200]])

    eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                   coordinates = "diatomic-perp",
                                   periodic = False,
                                   convert=False,
                                   positions_eq=positions_eq,
                                   masses=masses,
                                   usecols = (0,1,4),
                                   bounds_check = False)
    if show:
        eta.show()

    try:
        eta = create_bivariatefriction(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                       coordinates = "diatomic-perp",
                                       periodic = True,
                                       convert=False,
                                       positions_eq=positions_eq,
                                       verbose=False,
                                       masses=masses,
                                       usecols = (0,1,4),
                                       bounds_check = False)
        raise AssertionError
    except NotImplementedError:
        pass

def test_create_bivariatepotential(show = False):
    from MDsim.tools.createobjects import create_bivariatepotential

    pes = create_bivariatepotential(datafile = 'data/CN_on_Pt111_2x2x5_top_LDFA-AIM.eta',
                                    usecols = (0,1,4))

    if show:
        pes.show()

    pes = create_bivariatepotential(datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
                                    periodic = True,
                                    usecols = (0,1,4))
    if show:
        pes.show()

    masses = np.array([12.,16.])
    positions_eq=np.array([[21.22994700],[22.38415200]])
    pes = create_bivariatepotential(datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
                                    coordinates = "diatomic-perp",
                                    periodic = False,
                                    convert=False,
                                    normalize=True,
                                    positions_eq=positions_eq,
                                    masses=masses,
                                    usecols = (0,1,4))

    if show:
        pes.show()

    try:
        pes = create_bivariatepotential(datafile = 'data/CO_on_Pt111_5layers_sqrt3xsqrt3_R30_top.pot',
                                        coordinates = "diatomic-perp",
                                        periodic = True,
                                        convert=False,
                                        normalize=True,
                                        positions_eq=positions_eq,
                                        masses=masses,
                                        usecols = (0,1,4))

        raise AssertionError
    except NotImplementedError:
        pass

def test_create_minimumbasisfourierpotential(show = False):
    from MDsim.tools.createobjects import create_minimumbasisfourierpotential

    a = 3.610 / np.sqrt(2)

    pes = create_minimumbasisfourierpotential(a = a,
                                              datafile = 'data/Na_on_Cu111_adsorption_sites.pot')


    if show:
        pes.show()
        pes.show_cuts()

def test_create_tophollowfourierpotential(show = False):
    from MDsim.tools.createobjects import create_tophollowfourierpotential

    a = 3.610 / np.sqrt(2)

    pes = create_tophollowfourierpotential(a = a,
                                           datafile = 'data/test.pot')


    if show:
        pes.show()
        pes.show_cuts()

def test_create_minimumbasisfourierfriction(show = False):
    from MDsim.tools.createobjects import create_minimumbasisfourierfrictioncoefficient as create_minimumbasisfourierfriction
    a = 3.610 / np.sqrt(2)
    eta = create_minimumbasisfourierfriction(a = a,
                                             datafile = 'data/Na_on_Cu111_adsorption_sites.eta')


    if show:
        eta.show()
        eta.show_cuts()

# def test_create_observables():
    # from MDsim.tools.createobjects import create_observable
    # from MDsim.tools.createobjects import create_dummy_system
    # from MDsim.observables.implemented import implemented

    # system = create_dummy_system()
    # for obs in implemented:
        # if obs in ['scattering_amplitudes', 'isf', 'dsf', ]:
            # continue
        # O = create_observable(system_inst=system, observable=obs, iatom = 1)

# def test_create_handlers(remove=True):
    # from MDsim.tools.createobjects import create_dummy_system
    # from MDsim.observables.implemented import implemented
    # from MDsim.tools.createobjects import create_filehandler
    # from MDsim.tools.createobjects import create_stdouthandler
    # from MDsim.tools.createobjects import create_integrator

    # system = create_dummy_system()
    # integrator = create_integrator(dt=1)

    # handlers=[]
    # for o in implemented:
        # if o in ['scattering_amplitudes', 'isf', 'dsf']:
            # continue
        # handler = create_filehandler(system_inst = system,
                                     # integrator_inst = integrator,
                                     # observable = o,
                                     # iatom = 1,
                                     # )

        # handler.push()
        # handlers.append(handler)

        # handler = create_stdouthandler(system_inst=system,
                                       # integrator_inst=integrator,
                                       # observable=o,
                                       # iatom=1)
        # handler.push_logo()
        # handler.push_system_info()
        # handler.push_units_info()
        # handler.push()
    # for handler in handlers:
        # handler.close()

    # if remove:
        # shutil.rmtree('output')


if __name__ == '__main__':
    test_create_integrator()
    test_create_bivariatepotential(show = False)
    test_create_bivariatefriction(show = False)
    test_create_tophollowfourierpotential(show = False)
    test_create_minimumbasisfourierpotential(show = False)
    # test_create_observables()
    # test_create_handlers()
    test_create_minimumbasisfourierfriction(show = False)
