# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

from MDsim.neighbors import Neighbors
from MDsim.tools.status import Status

def test_neighborextension(Natoms=10, show=False):
    pos = np.random.randn(Natoms, 2)

    cell_length = 100.
    cell = cell_length*np.array([[1, 0],[0,1]])

    pos[:,0] *= cell[0,0]
    pos[:,1] *= cell[1,1]


    status = Status()

    nn = Neighbors(unit_cell=cell)

    list_cutoff = cell_length*0.4

    funcs = []
    funcs.append('nn._construct_neighbor_lists_numpy(pos, list_cutoff, mic={})')

    try:
        from MDsim.speedup import f90support
        print('f90support available')
        funcs.append('nn._construct_neighbor_lists_fortran(pos, list_cutoff, mic={})')
    except ImportError:
        print('f90 support *not* available')

    try:
        from MDsim.speedup import numbasupport
        print('numba support available')
        funcs.append('nn._construct_neighbor_lists_numba(pos, list_cutoff, mic={})')
    except ImportError:
        print('numba support *not* available')

    try:
        from MDsim.speedup import cythonsupport
        print('cython support available')
        funcs.append('nn._construct_neighbor_lists_cython(pos, list_cutoff, mic={})')
    except ImportError:
        print('cython support *not* available')



    eps = 1e-10

    for mic in [False, True]:
        print('Checking against plain python reference implementation (MIC={})'.format(mic))
        ref,rlist = nn._construct_neighbor_lists_python(pos, list_cutoff, mic=mic)
        for func in funcs:
            nlist, nneigh = eval(func.format(mic))
            status.ok = np.all(np.abs(ref - nlist) == 0)
            print('\t{} : {}'.format(func.replace('nn.','').replace('pos, list_cutoff, mic={}',''), status))
            if not status.ok:
                raise AssertionError
    if show:
        nn.show_neighbor_list(positions=pos, iatom=None, list_cutoff=list_cutoff)

if __name__ == '__main__':
    test_neighborextension(Natoms=100, show=True)
