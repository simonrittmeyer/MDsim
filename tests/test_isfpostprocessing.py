# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil
import random

from MDsim import units
from MDsim.systems.system import System

from MDsim.friction.scalarfriction import ScalarFriction

from MDsim.tools.createobjects import create_minimumbasisfourierpotential
from MDsim.tools.createobjects import create_integrator
from MDsim.tools.createobjects import create_filehandler
from MDsim.tools.createobjects import create_postprocessingobservable


from MDsim.tools.status import Status
from MDsim.tools.netcdf import load_netcdf


def test_ISF(nsteps = 1e3, Natoms=2, Nkpts=5, show_traj=False, show_fit=False,
             show_ISF=False, show_signature=False, remove=True):
    verbose=False
    status = Status()

    print('Running simple test simulation...', end='')
    a = 3.610 / np.sqrt(2)
    pes = create_minimumbasisfourierpotential(a=a,
                                              datafile='data/Na_on_Cu111_adsorption_sites.pot',
                                              verbose=verbose)

    Ndim = 2

    positions = np.random.randn(Natoms, Ndim)
    species = ['Na' for i in range(Natoms)]

    velocities = np.zeros_like(positions)
    eta = ScalarFriction(eta = .005, convert = False)
    T = 155
    system = System(positions = positions,
               species = species,
               velocities = velocities,
               temperature = T,
               friction = eta,
               potential = pes,
               convert = True,
               _verbose=verbose)

    # hard coded from DJW's mail to maintain comparability
    delta_K = np.vstack((np.linspace(0,2,Nkpts), np.linspace(0,2,Nkpts))).T
    angle = np.pi/3
    delta_K[:,0] *= np.cos(angle)
    delta_K[:,1] *= np.sin(angle)

    integrator = create_integrator(method='bussi parrinello',
                                   dt=10,
                                   verbose=verbose)

    handlers=[]
    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='netcdf',
                                       observable='positions',
                                       verbose=verbose))
    handlers.append(create_filehandler(system_inst=system,
                                       integrator_inst=integrator,
                                       fileformat='netcdf',
                                       observable='scattering_amplitudes',
                                       delta_K = delta_K,
                                       convert=True,
                                       verbose=verbose))

    for n in range(int(nsteps)):
        integrator.propagate(system)
        for handler in handlers:
            handler.push()

    for handler in handlers:
        handler.close()

    print('done')

    filename = 'output/MDsim__POSITIONS.nc'

    print('Reading trajectory data...'.format(filename), end='')
#    data = np.loadtxt(filename)
    data = load_netcdf(filename)
    print('done')

    time = data[:,0]
    positions = data[:,1::]

    Ntimes = data.shape[0]
    Nparticles = positions.shape[1] / 2

    # reshaping things a bit
    positions = positions.reshape((len(time), Nparticles, 2))

    if show_traj:
        print('Rendering trajectories plot')
        import matplotlib.pyplot as plt
        for i in range(Nparticles):
            plt.plot(positions[:,i,0], positions[:,i,1])
        plt.show()

    fname = 'output/MDsim__SCATTERING_AMPLITUDES.nc'
    obs = create_postprocessingobservable(
                            system_inst=system,
                            observable='isf',
                            verbose=False,
                            scattering_amplitudes_file=fname,
                            optimize_length=True,
                            delta_K=delta_K,
                            convert=True)
    handler = create_filehandler(system_inst=system,
                                 integrator_inst=integrator,
                                 fileformat='netcdf4',
                                 observable=obs,
                                 )
    handler.write()
    handler.close()

    handler = create_filehandler(system_inst=system,
                                 integrator_inst=integrator,
                                 fileformat='ascii',
                                 observable=obs,
                                 )
    handler.write()
    handler.close()

    if show_ISF:
        import matplotlib
        import matplotlib.pyplot as plt
        data = load_netcdf('output/MDsim__ISF.nc').T
        time = data[0]
        ISF = data[1::]
        print('Rendering ISF plot')
        #time -= max(time)/2.
        real_map = plt.get_cmap('Reds')

        norm = matplotlib.colors.Normalize(-1, Nkpts+1)

        for k, i in enumerate(ISF):
            plt.plot(time, i, color=real_map(norm(k)))

        plt.xlabel('time / fs')
        plt.ylabel('ISF(dK, t) / fs')

        plt.show()


if __name__ == '__main__':
    test_ISF(nsteps = 2**12, Natoms=50, Nkpts=10, remove=False, show_traj=False, show_fit=False, show_ISF=True, show_signature=True)
