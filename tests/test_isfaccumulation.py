# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import numpy as np
import shutil
import os
import re
import matplotlib
import matplotlib.pyplot as plt

from MDsim.tools.netcdf import load_netcdf
from MDsim.tools.status import Status

def test_isfaccumulation(show=False, remove=True, verbose=False, time=10000):
    def run():
        from MDsim.mainroutines.diffusion import main
        from MDsim.mainroutines.diffusion.options import get_options_dict
        from MDsim.tools.options import create_args_dict

        args = create_args_dict(get_options_dict())

        args['system.natoms'] = 50
        args['system.species'] = 'Na'
        args['system.surface_facette'] = 'fcc111'
        args['system.initial_positions'] = 'bridge'
        args['system.temperature'] = 155
        args['system.polarizability'] = 17
        args['system.surface_lattice_constant'] = 3.610 / np.sqrt(2)
        args['system.surface_facet'] = 'fcc111'
        args['potential.type'] = 'interpolated'
        args['potential.potfile'] = 'data/Na_on_Cu111_adsorption_sites.pot'

        args['friction.value'] = 0.010
        args['friction.type'] = 'constant'

        args['system.coverage'] = 0.05
        args['system.saturation_coverage'] = 4./9.
        args['system.polarizability'] = 13.8
        args['system.dipole_moment_zero_coverage'] = 3.48

        args['misc.support'] = 'fortran'

        args['pairwise.force_cutoff'] = 20.
        args['pairwise.list_cutoff'] = 40.

        args['misc.show_system'] = False
        args['io.observables'] = None #['scattering_amplitudes']
        args['io.delta_k_directions'] = ['[11-2]']
        args['io.delta_k_max_amplitude'] = 4.

        args['io.alternative_etafile'] = 'data/Na_on_Cu111_adsorption_sites.eta'
        args['io.debug'] = True
        args['io.fileformat'] = 'netcdf4'
        args['io.iostep'] = 1
        args['io.outpath'] = 'tmp'

        args['propagation.simulation_time'] = time
        args['propagation.timestep'] = 10
        args['propagation.equilibration_time'] = 100000
        args['propagation.mode'] = 'accumulation'
        args['propagation.nruns'] = 3

        args['postprocessing.observables'] = ['isf', 'dsf', 'isf_via_dsf']
        args['postprocessing.fft_optimize_length'] = True
        args['postprocessing.fft_autocorrelation'] = 'linear'

        main(py_args=args,
             verbose=verbose)

    run()

    via_isf = load_netcdf('tmp/MDsim__dK__11-2__ISF.nc').T
    via_dsf = load_netcdf('tmp/MDsim__dK__11-2__ISF_VIA_DSF.nc').T

    if show:
        for data, cmap_col, ls, lw in zip([via_isf, via_dsf], ['rainbow', 'rainbow'], ['-', '--'], [1,2]):
            time = data[0] / 1000.
            ISF = data[1::]

            Nkpts = len(ISF)
            cmap = plt.get_cmap(cmap_col)
            norm = matplotlib.colors.Normalize(-1, Nkpts+1)

            for k, i in enumerate(ISF):
                plt.plot(time, i, ls=ls, lw=lw, color=cmap(norm(k)))
        plt.xlabel('time / ps')
        plt.ylabel('ISF(dK, t)/ISF(dK,0)')
        plt.show()

    if remove:
        try:
            shutil.rmtree('tmp')
        except OSError:
            pass

    status = Status()

    status.ok = np.allclose(via_isf, via_dsf)
    print('Accumulation via ISF and DSF coincides with runtime accumulation : {}'.format(status))
    if not status.ok:
        raise AssertionError

if __name__ == '__main__':
    test_isfaccumulation(remove=False, show=True, verbose=True)
