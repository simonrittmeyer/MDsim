# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import os
import shutil
import ase

from MDsim import units

from MDsim.tools.status import Status
from MDsim.bindings.castep import installed
from MDsim.bindings.castep import ExtendedCastep
from MDsim.qmmeef.qm import CastepQM

from MDsim.tools.createobjects.integrator import create_integrator
from MDsim.tools.createobjects.asebindings import create_trajectoryhandler

def test_castepqm(steps=2,remove=True):
    try:
        installed()
    except Exception:
        print('Some of castep/castep_hirshfeld/castep2cube are not installed, skipping test...')
        return
    status = Status()
    verbose = False

    integrator = create_integrator(method='liouville', dt=1, seed=None, verbose=False)

    atoms = ase.Atoms('H3', [[0, 0, 0], [0, 0, 1.], [0,0,2]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep(verbose = verbose)

    calc._rename_existing_dir = False
    calc._copy_pspots = False
    calc._link_pspots = True
    if os.path.isdir('output'):
        shutil.rmtree('output')

    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    folder = 'output/qm'
    system = CastepQM(atoms,
                calc_folder=folder,
                adsorbate_idx = 1,
                _verbose=verbose,
                friction='aim',
                _init_forces=True,
                _init_etas=True,
                _eval_parallel = True)



    forces = system.get_forces()
    status.ok = np.all(forces.shape == atoms.get_positions().shape)
    print('Correct dimensionality of force matrix : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.get_forces_qm()
    system.get_forces_slab()
    status.ok = system.atoms.calc._calls == 1 and system.slab_atoms.calc._calls == 1
    print('Forces calls require one castep run each if positions remain the same : {}'.format(status))
    if not status.ok:
        raise AssertionError

    etas = system.get_etas()
    aim_etas = etas.copy()

    status.ok = np.all(etas.shape == atoms.get_positions().shape)
    print('Correct dimensionality of eta tensor : {}'.format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-10

    status.ok = np.all(np.abs(etas[system.get_slab_idx(),:]) < eps)
    print('AIM-Friction only on adsorbate atoms : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(etas[system.get_adsorbate_idx(),:] > eps))
    print('Non-zero AIM-friction on center atom (== adsorbate) : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = system.atoms.calc._calls == 1 and system.atoms.calc._continuation_calls == 1
    print('AIM-eta call requires no additional castep SCF run : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.set_positions(system.get_positions() * units.ANGSTROM_TO_AU + 1.2)
    system.get_etas()
    status.ok = system.atoms.calc._calls == 2 and system.atoms.calc._continuation_calls == 2
    print('AIM-eta call before forces call requires new SCF calculation for full system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = (system.atoms.calc._calls == 2
                 and system.atoms.calc._continuation_calls == 2
                 and system.slab_atoms.calc._calls == 1)
    print('AIM-eta call before forces call does not run slab_atoms SCF call : {}'.format(status))
    if not status.ok:
        raise AssertionError

    system.get_forces()
    system.get_forces_qm()
    system.get_forces_slab()
    status.ok = (system.atoms.calc._calls == 2
                 and system.atoms.calc._continuation_calls == 2
                 and system.slab_atoms.calc._calls == 2)
    print('Subsequent forces call requires new SCF calculation only for the slab : {}'.format(status))
    if not status.ok:
        raise AssertionError


    # the propagation part
    handler = create_trajectoryhandler(system_inst = system, verbose=False, outfolder=folder)

    # positions are stored only with rather limited precision
    pos_eps = 1e-5
    eps = 1e-10

    pos = system.get_positions().copy()
    vel = system.get_velocities().copy()
    forces = system.get_forces().copy()
    etas = system.get_etas().copy()
    Epot = system.get_potential_energy()

    nstep=0

    # this is to suppress the warnings (shouldn't propagate this instance! But
    # this one is for testing.)

    import warnings
    warnings.filterwarnings("ignore")

    for step in range(steps):
        nstep += 1
        integrator.propagate(system)
        status.ok = (not np.all(np.abs(pos - system.get_positions()) < pos_eps)
                     and not np.all(np.abs(vel - system.get_velocities()) < eps)
                     and not np.all(np.abs(forces - system.get_forces()) < eps)
                     and not np.all(np.abs(etas - system.get_etas()) < eps)
                     and not np.abs(Epot - system.get_potential_energy()) < eps)

        print('System properties all change after propagation at step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        status.ok = (system.atoms.calc._calls == 2 + nstep
                     and system.atoms.calc._continuation_calls == 2 + nstep
                     and system.slab_atoms.calc._calls == 2 + nstep)
        print('New calculation required for step {} : {}'.format(nstep, status))
        if not status.ok:
            raise AssertionError

        pos = system.get_positions().copy()
        vel = system.get_velocities().copy()
        forces = system.get_forces().copy()
        etas = system.get_etas().copy()
        Epot = system.get_potential_energy()


        handler.push()

    handler.close()

    # Here comes the IAA test part
    atoms = ase.Atoms('H3', [[0, 0, 0], [0, 0, 1.], [0,0,2]], cell=[5, 5, 10])
    atoms.center()
    calc = ExtendedCastep(verbose = verbose)

    calc._rename_existing_dir = False
    calc._copy_pspots = False
    calc._link_pspots = True
    if os.path.isdir('output'):
        shutil.rmtree('output')

    atoms.set_calculator(calc)
    atoms.calc.set_pspot(pspot = 'OTF')
    atoms.calc.cell.kpoints_mp_grid = '1 1 1'

    folder = 'output/qm'
    system = CastepQM(atoms,
                calc_folder=folder,
                adsorbate_idx = 1,
                _verbose=verbose,
                friction='IAA',
                _init_forces=True,
                _init_etas=True,
                _eval_parallel = True)

    forces = system.get_forces()
    system.get_forces_qm()
    system.get_forces_slab()
    status.ok = system.atoms.calc._calls == 1 and system.slab_atoms.calc._calls == 1
    print('Forces calls require one castep run each if positions remain the same : {}'.format(status))
    if not status.ok:
        raise AssertionError

    etas = system.get_etas()
    status.ok = np.all(etas.shape == atoms.get_positions().shape)
    print('Correct dimensionality of eta tensor : {}'.format(status))
    if not status.ok:
        raise AssertionError

    eps = 1e-10

    status.ok = np.all(np.abs(etas[system.get_slab_idx(),:]) < eps)
    print('IAA-Friction only on adsorbate atoms : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(etas[system.get_adsorbate_idx(),:] > eps))
    print('Non-zero IAA-friction on center atom (== adsorbate) : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = (system.atoms.calc._calls == 1
                 and system.slab_atoms.calc._calls == 1
                 and system.slab_atoms.calc._continuation_calls == 1)
    print('IAA-eta call requires no additional castep SCF/slab run : {}'.format(status))
    if not status.ok:
        raise AssertionError

    status.ok = np.all(np.abs(etas - aim_etas) < 0.001)
    print('IAA and AIM friction is reasonably consistent for this system : {}'.format(status))
    if not status.ok:
        raise AssertionError

    if remove:
        shutil.rmtree(folder)

if __name__ == '__main__':
    test_castepqm(steps=2, remove=False)

