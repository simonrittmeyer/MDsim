# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import numpy as np

def test_normalmodetrafo(show=False):
    from MDsim.tools.createobjects import create_bivariatepotential
    from MDsim.analysis.normalmodes import evaluate_normalmodes_on_potential
    from MDsim import units
    # masses from CASTEP
    m_C = 12.01070000
    m_O = 15.9994

    masses = np.array([m_C, m_O])
    positions = np.zeros((2,1))
    datafile = 'data/CO_on_Cu100_c2x2.pot'

    pot = create_bivariatepotential(datafile = datafile,
                                    coordinates = "generic-dof",
                                    periodic = False,
                                    convert=True,
                                    normalize=True,
                                    verbose=True,
                                    forces_method = 'analytical',
                                    masses=masses.copy(),
                                    usecols = (0,2,4))
    Natoms = 2
    Ndim = 1


    omega, nm, pos_min, Modes = evaluate_normalmodes_on_potential(potential=pot,
                                                                  positions=positions,
                                                                  masses=masses,
                                                                  minimize_first=True,
                                                                  delta = 0.01,
                                                                  convert = True)

    # do the transformations by hand first
    # the normalmodes in columns is just Q
    Q = nm.copy()

    # the masses is simple
    Msqrt = np.diag(np.sqrt(masses))
    Msqrtinv = np.diag(1/np.sqrt(masses))

    # transform the normalmodes to cartesian basis
    nm = np.dot(Msqrtinv, Q)
    nm = Q.T

    # now the columns are the modes and the rows are the cartesian coordinates
    if show:

        lim = (0,.25)
        npts = 300


        data = np.loadtxt(datafile, usecols=(0,1,4)).T
        data[2,:] -= np.min(data[2,:])


        xy_range = np.array([[np.min(data[0]), np.max(data[0])],
                             [np.min(data[1]), np.max(data[1])]])*units.ANGSTROM_TO_AU

        xy_range[0,:] = np.array([-0.15, 0.15])
        xy_range[1,:] = np.array([-0.20, 0.30])

        x = np.linspace(*xy_range[0], num = npts)
        y = np.linspace(*xy_range[1], num = npts)


        X,Y = np.meshgrid(x,y)
        points = np.vstack((X.flatten(), Y.flatten())).T

        Z = np.zeros_like(points[:,0])
        Fx = np.zeros_like(points[:,0])
        Fy = np.zeros_like(points[:,0])

        print('Evaluating plot data')
        for i in range(npts**2):
            try:
                Z[i] = pot.get_value(points[i].reshape(2,1)) * units.AU_TO_EV
                Fx[i], Fy[i] = pot.get_forces(points[i].reshape(2,1))
            except ValueError:
                Z[i] = Fx[i] = Fy[i] = np.nan

        Z = Z.reshape(npts, npts)
        Fx = Fx.reshape(npts, npts)
        Fy = Fy.reshape(npts, npts)

        Z = np.ma.masked_invalid(Z)
        X = np.ma.array(X, mask=Z.mask)
        Y = np.ma.array(Y, mask=Z.mask)

        import matplotlib.pyplot as plt
        from matplotlib import rcParams
        rcParams['mathtext.fontset'] = 'stixsans'
        rcParams['font.family'] = 'Arial'

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)


        cmap = plt.get_cmap('Reds')

        print('Rendering plot')

        # ok, let's do the mass weighting here
        from ase.data import atomic_masses
        X *= np.sqrt(atomic_masses[6])
        x *= np.sqrt(atomic_masses[6])

        Y *= np.sqrt(atomic_masses[8])
        y *= np.sqrt(atomic_masses[8])

        xy_range[0] *= np.sqrt(atomic_masses[6])
        xy_range[1] *= np.sqrt(atomic_masses[8])

        # surface plot
        surf = ax.pcolormesh(X, Y, Z,
                            cmap=cmap)

        # contour plot
        levels = np.linspace(*lim, num = 20)
        cont = ax.contour(X, Y, Z,
                          colors = 'gray',
                          extend="both",
                          levels = levels)

#         ax.streamplot(x=x,
#                       y=y,
#                       u=Fx,
#                       v=Fy,
#                       density = 2,
#                       color='black')

        for i in [surf, cont]:
            i.cmap.set_under('white')
            i.cmap.set_over('white')
            i.set_clim(*lim)

        ax.set_xlim(*xy_range[0])
        ax.set_ylim(*xy_range[1])

        ax.set_xlabel(r'$\Delta\mathbf{X}_{\mathrm{C}}$ / a.u.')
        ax.set_ylabel(r'$\Delta\mathbf{X}_{\mathrm{O}}$ / a.u.')

        ax.clabel(cont, fmt ='%.2f eV')

        # add the modes on top
        lines = np.empty((2,2,2))
        disp = [-1e2,1e2]
        for i, m in enumerate(nm.T):
            for j, d in enumerate(disp):
                lines[i,j,:] = d*m + pos_min.ravel()

        # the soft mode
        ax.plot(lines[0,:,0], lines[0,:,1])

        # the hard mode
        ax.plot(lines[1,:,0], lines[1,:,1])

        ax.set_aspect('equal')

        plt.show()

        fig = plt.figure()


if __name__ == '__main__':
    test_normalmodetrafo()
