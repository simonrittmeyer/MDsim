# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

def test_kohnlaudipolesystem(show = False, steps = 1e3):
    import numpy as np
    import random

    from MDsim import units
    from MDsim.systems.pairwise.kohnlausystem import KohnLauSystem
    from MDsim.tools.createobjects import create_minimumbasisfourierpotential
    from MDsim.tools.createobjects import create_integrator
    from MDsim.friction.scalarfriction import ScalarFriction

    a = 3.610 / np.sqrt(2)
    pot = create_minimumbasisfourierpotential(a = a, datafile = 'data/Na_on_Cu111_adsorption_sites.pot')
    pot._change_support('cython')
    Nparticles = 50

    cell = 10*np.array([[2*a*np.cos(np.pi/6.), 0],[0,a]])

    positions = np.random.rand(Nparticles,2)
    positions[:,0] *= cell[0,0]
    positions[:,1] *= cell[1,1]

    velocities = None

    species = ['Na' for i in range(Nparticles)]

    # fake masses and positions
    dipoles = 1.4*np.ones(Nparticles)
    dipoles = np.zeros(Nparticles)

    fcutoff = 10
    lcutoff = fcutoff *1.5

    eta = ScalarFriction(eta = 0.01, convert = False)
    T = 300
    S = KohnLauSystem(dipoles = dipoles,
                      force_cutoff = fcutoff,
                      list_cutoff = lcutoff,
                      unit_cell = cell,
                      positions = positions,
                      friction = eta,
                      velocities = velocities,
                      potential = pot,
                      temperature = T,
                      species = species)

    #exit()
    # let's do some time integration... simplest velocity verlet


    dt = 10
    method = 'bussi parrinello'
    integrator = create_integrator(method = method,
                                   seed = None,
                                   dt = dt)


    update_frequency = 100

    if show:
        S.show_neighbor_list(iatom = 1)

    steps = int(steps)
    Ekin = np.zeros(steps/update_frequency)
    Epot = np.zeros(steps/update_frequency)

    positions = np.zeros((steps, Nparticles, 2))
    for step in range(steps):
        if step%update_frequency==0:
            Ekin[step/update_frequency] = S.get_kinetic_energy()
            Epot[step/update_frequency] = S.get_potential_energy()

        integrator.propagate(S)
        positions[step,:,:] = S.get_positions()

    print()
    print('steps                 : {}'.format(steps))
    print('neighbor list updates : {}'.format(S.get_neighbor_list_updates()))
    print('---------------------')
    print('efficiency            : {}%'.format((1.-(S.get_neighbor_list_updates()/float(steps)))*100.))
    if show:
        import random
        import matplotlib.pyplot as plt
        from matplotlib.colors import ColorConverter
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        Nplotparticles = Nparticles
        for i in np.random.choice(np.arange(Nparticles), size = Nplotparticles):
            color = random.choice(ColorConverter.cache.keys())
            ax.plot(positions[::10,i,0], positions[::10,i,1], c = color)

        ax.set_aspect('equal')

        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)

        times = np.arange(len(Epot))*dt*update_frequency/1000.

        ax.plot(times, Epot, label = 'E_pot')
        ax.plot(times, Ekin, label = 'E_kin')
        ax.plot(times, Ekin + Epot, label = 'E_tot')

        ax.set_xlabel('time (ps)')
        ax.set_ylabel('energy (eV)')
        kT = S.get_kT()

        ax.axhline(y = Nparticles*kT, lw = 2, color = 'black')

        ax.legend()
        plt.show()

if __name__== '__main__' :
    test_kohnlaudipolesystem(show = True, steps = 1e4)
