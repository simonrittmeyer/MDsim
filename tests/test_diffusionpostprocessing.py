# This file is part of MDsim.
# 
# MDsim is a simulation package for full-tensorial Langevin MD.
# Copyright (C) 2014-2017  Simon P. Rittmeyer
#
# MDsim is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3.0 of the License, or
# (at your option) any later version.
# 
# MDsim is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with MDsim. If not, see <http://www.gnu.org/licenses/>.

"""
Test following the simulations from

J. Ellis, A. P. Graham, F. Hofmann and J. P. Toennies, Phys. Rev. B 63, 195408 (2001).
"""

from __future__ import print_function

import numpy as np
import shutil

def test_diffusion_main(show=False, remove=True, time=1000, accumulate=None, show_analysis=False):
    from MDsim.mainroutines.diffusion import main
    from MDsim.mainroutines.diffusion.options import get_options_dict
    from MDsim.tools.options import create_args_dict
    from MDsim.tools.netcdf import load_netcdf

    import netCDF4
    args = create_args_dict(get_options_dict())
#    for arg in sorted(args.keys()):
#        print(arg, args[arg])

    args['system.natoms'] = 50
    args['system.species'] = 'Na'
    args['system.temperature'] = 300
    args['system.surface_lattice_constant'] = 3.610 / np.sqrt(2)
    args['system.surface_facet'] = 'fcc100'
    args['potential.type'] = 'interpolated'
    args['potential.potfile'] = 'data/cambridge.pot'
    args['potential.interpolation'] = 'cambridge'

    args['friction.type'] = 'constant'
    args['friction.value'] = 0.015

    args['system.coverage'] = 0.028
    args['system.saturation_coverage'] = 1.
    args['system.polarizability'] = 17.7
    args['system.dipole_moment_zero_coverage'] = 4

    args['misc.support'] = 'fortran'

    args['pairwise.force_cutoff'] = 20.
    args['pairwise.list_cutoff'] = 40.

    args['misc.show_system'] = show
#     args['io.observables'] = ['positions']#, 'scattering_amplitudes']
    args['io.delta_k_directions'] = ['[110]']
    args['io.nkpts'] = 50
    args['io.delta_k_max_amplitude'] = 3

    args['io.debug'] = True
    args['io.fileformat'] = 'netcdf4'
    args['io.iostep'] = 1
    args['io.outpath'] = 'output'

    args['propagation.simulation_time'] = time
    args['propagation.timestep'] = 30
    args['propagation.equilibration_steps'] = 10000
    if accumulate is not None:
        args['propagation.mode'] = 'accumulation'
        args['propagation.nruns'] = accumulate
    else:
        args['propagation.mode'] = 'single'

    args['postprocessing.observables'] = ['dsf', 'isf']
    args['postprocessing.fft_optimize-length'] = True

    main(infile='../infiles/diffusion.cfg',
         py_args=args)

    if show_analysis:
        import matplotlib
        import matplotlib.pyplot as plt
        from MDsim.analysis.fitting import tune_cutoff
        fname = 'output/MDsim__dK__110__ISF.nc'
        data = load_netcdf(fname).T
        time = data[0]
        ISF = data[1::]

        # read the delta K info
        ncfile = netCDF4.Dataset(fname, 'r')
        kinfo = ncfile.observable
        ncfile.close()

        kpts = []
        for line in kinfo.split('\n'):
            if 'Delta-K_' in line:
                kpts.append(float(line.split()[-2]))
        kpts = np.asarray(kpts)
        print('Rendering ISF plot')
        #time -= max(time)/2.
        real_map = plt.get_cmap('Reds')

        norm = matplotlib.colors.Normalize(-1, len(kpts)+1)

        for k, i in enumerate(ISF):
            plt.plot(time, i, color=real_map(norm(k)))

        plt.xlabel('time / fs')
        plt.ylabel('ISF(dK, t) / fs')

        plt.show()

        alphas = np.zeros_like(kpts)*np.nan
        Nkpts = len(kpts)

        show_fit=False
    #     show_fit=True
        print('-'*80)
        print('Tuning cutoff ({})'.format(fname))
        print('-'*80)

        max_time = 4
        for k in range(0, Nkpts):
            print('\t{} / {}'.format(k+1, Nkpts))
            alphas[k] = tune_cutoff(time, ISF[k], show=show_fit, stepsize=int(100), max_cutoff=int(2*max_time*1000./30))

        plt.plot(kpts, alphas)
        plt.xlabel('dK / A^-1')
        plt.ylabel('alpha(dK) / ps^-1')

        plt.show()
    if remove:
        try:
            shutil.rmtree('output')
        except OSError:
            pass

if __name__ == '__main__':
    test_diffusion_main(remove=False, show=False, time=2**14*30, accumulate=50, show_analysis=True)
